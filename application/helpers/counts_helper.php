<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('count_cheques'))
{
    function count_cheques($where_data)
    {
        $ci=& get_instance();
        $ci->load->database();
        $ci->db->where($where_data)
            ->get('cheques')
            ->result();
        $rs=$ci->db->affected_rows();
        return $rs;
    }
}

/*Created For Sub Head In Account List*/
if ( ! function_exists('read_subHead'))
{
    function read_subHead($id)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select('*')
            ->where('A_ID',$id)
            ->get('accounts')
            ->row();
    }
}

/*Read Accounts Head By Head Id*/
if(!function_exists('account_head_by_subId')){
    function account_head_by_subId($id)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select('*')
            ->from('accounts_head')
            ->where('SUB_HEAD_ID',$id)
            ->get()->result();
    }
}

/*Read Accounts By Head Id*/
if(!function_exists('account_by_head')){
    function account_by_head($id)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select('*')
            ->from('accounts a')
            ->join('accounts_head ah','a.H_ID=ah.H_ID')
            ->where('a.H_ID',$id)
            ->get()->result();
    }
}

/*Read Accounts By Sub Head Id*/
if(!function_exists('account_by_sub_head')){
    function account_by_sub_head($id)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select('*')
            ->from('accounts a')
            ->join('accounts_head ah','a.H_ID=ah.H_ID')
            ->where('a.SUB_HEAD_ID',$id)
            ->get()->result();
    }
}

/*Read Sub Type By Head Id*/
if( ! function_exists('sub_type_by_id'))
{
    function sub_type_by_id($id)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->where('SUB_HEAD_ID',$id)
            ->where('SUB_STATUS', 1)->get('sub_heads')->row();
    }
}

/*Read Sum of Opening Balance By Sub Type*/
if( ! function_exists('opening_bal_by_sub_head'))
{
    function opening_bal_by_sub_head($sub_head_id,$type)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select('sum(A_OPENINGBALANCE) as opening_balance')
            ->where(array(
                'SUB_HEAD_ID'   => $sub_head_id,
                'A_OPENINGTYPE' => $type
            ))
            ->get('accounts')->row();
    }
}

/*Read Sum of Opening Balance By Account Head*/
if( ! function_exists('opening_bal_by_head'))
{
    function opening_bal_by_head($head_id,$type)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select('sum(A_OPENINGBALANCE) as opening_balance')
            ->where(array(
                'H_ID'   => $head_id,
                'A_OPENINGTYPE' => $type
            ))
            ->get('accounts')->row();
    }
}

/*Created  for Debit Transaction Data*/
if ( ! function_exists('read_debit_transactions'))
{
    function read_debit_transactions($tr_id)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select("*")
            ->from('transactions_meta tm')
            ->join("accounts a","tm.A_ID=a.A_ID")
            ->where(array(
                'T_ID'=>$tr_id,
                'TM_TYPE'=>"Debit"
            ))
            ->get()->result();
    }
}

/*Created  for Credit Transaction Data*/
if ( ! function_exists('read_credit_transactions'))
{
    function read_credit_transactions($tr_id)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select("*")
            ->from('transactions_meta tm')
            ->join("accounts a","tm.A_ID=a.A_ID")
            ->where(array(
                'T_ID'=>$tr_id,
                'TM_TYPE'=>"Credit"
            ))
            ->get()->result();
    }
}

/*Created  for Transaction Sum By Date to Date on Account Id*/
if ( ! function_exists('transactions_between_date'))
{
    function transactions_between_date($account_id,$start_date,$end_date,$type)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select("sum(TM_AMOUNT) as amount")
            ->from('transactions_meta tm')
            ->join('transactions t','tm.T_ID=t.T_ID')
            // ->join("accounts a","tm.A_ID=a.A_ID")
            ->where(array(
                'tm.A_ID'      => $account_id,
                'tm.TM_TYPE'  => $type,
                't.T_DATE >='     => $start_date,
                't.T_DATE <='     => $end_date
            ))
            ->get()->row();
    }
}

/*Created  for Transaction Sum By Date to Date on Sub Type*/
if ( ! function_exists('transactions_by_sub_type'))
{
    function transactions_by_sub_type($sub_head_id,$start_date,$end_date,$type)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select("sum(TM_AMOUNT) as amount")
            ->from('transactions_meta tm')
            ->join('transactions t','tm.T_ID=t.T_ID')
            ->join("accounts a","tm.A_ID=a.A_ID")
            ->where(array(
                'a.SUB_HEAD_ID'   => $sub_head_id,
                'tm.TM_TYPE'      => $type,
                't.T_DATE >='     => $start_date,
                't.T_DATE <='     => $end_date
            ))
            ->get()->row();
    }
}

/*Created  for Transaction Sum By Date to Date on Account Head*/
if ( ! function_exists('transactions_by_account_head'))
{
    function transactions_by_account_head($head_id,$start_date,$end_date,$type)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select("sum(TM_AMOUNT) as amount")
            ->from('transactions_meta tm')
            ->join('transactions t','tm.T_ID=t.T_ID')
            ->join("accounts a","tm.A_ID=a.A_ID")
            ->where(array(
                'a.H_ID'          => $head_id,
                'tm.TM_TYPE'      => $type,
                't.T_DATE >='     => $start_date,
                't.T_DATE <='     => $end_date
            ))
            ->get()->row();
    }
}

/*Created  for  Transaction Sum till specific Date*/
if ( ! function_exists('transaction_date'))
{
    function transaction_date($account_id,$date,$type)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select("sum(TM_AMOUNT) as amount")
            ->from('transactions_meta tm')
            ->join('transactions t','tm.T_ID=t.T_ID')
            ->join("accounts a","tm.A_ID=a.A_ID")
            /* ->group_start()
                ->where('a.A_ID',$account_id)
                ->or_where('a.A_SELFHEAD_ID',$account_id)
                ->or_where_in('a.A_SELFHEAD_ID',"select A_ID from accounts where A_SELFHEAD_ID=$account_id",false)
            ->group_end() */
            ->where('(a.A_ID ='.$account_id.' OR a.A_SELFHEAD_ID = '.$account_id.' OR a.A_SELFHEAD_ID IN(select A_ID from accounts where A_SELFHEAD_ID='.$account_id.'))', NULL, FALSE)
            ->where(array(
                'tm.TM_TYPE'   => $type,
                't.T_DATE <='   => $date
            ))
            ->get()->row();
    }
}

/*Created  for  Transaction Sum till specific Date By Sub Head*/
if ( ! function_exists('transaction_date_by_subhead'))
{
    function transaction_date_by_subhead($sub_head_id,$date,$type)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select("sum(TM_AMOUNT) as amount")
            ->from('transactions_meta tm')
            ->join('transactions t','tm.T_ID=t.T_ID')
            ->join("accounts a","tm.A_ID=a.A_ID")
            ->where(array(
                'a.SUB_HEAD_ID'=> $sub_head_id,
                'tm.TM_TYPE'   => $type,
                't.T_DATE <'   => $date
            ))
            ->get()->row();
    }
}

/*Created  for  Transaction Sum till specific Date By Account Head*/
if ( ! function_exists('transaction_date_by_head'))
{
    function transaction_date_by_head($head_id,$date,$type)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select("sum(TM_AMOUNT) as amount")
            ->from('transactions_meta tm')
            ->join('transactions t','tm.T_ID=t.T_ID')
            ->join("accounts a","tm.A_ID=a.A_ID")
            ->where(array(
                'a.H_ID'=> $head_id,
                'tm.TM_TYPE'   => $type,
                't.T_DATE <'   => $date
            ))
            ->get()->row();
    }
}

/*Read Transaction Date to Date By Self Head Id*/
if ( ! function_exists('transaction_date_by_self'))
{
    function transaction_date_by_self($account_id,$start_date,$end_date,$type)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select("sum(TM_AMOUNT) as amount")
            ->from('transactions_meta tm')
            ->join('transactions t','tm.T_ID=t.T_ID')
            ->join("accounts a","tm.A_ID=a.A_ID")
            /* ->group_start()
                ->where('a.A_ID',$account_id)
                ->or_where('a.A_SELFHEAD_ID',$account_id)
                ->or_where_in('a.A_SELFHEAD_ID',"select A_ID from accounts where A_SELFHEAD_ID=$account_id",false)
            ->group_end() */
            ->where('(a.A_ID ='.$account_id.' OR a.A_SELFHEAD_ID = '.$account_id.' OR a.A_SELFHEAD_ID IN(select A_ID from accounts where A_SELFHEAD_ID='.$account_id.'))', NULL, FALSE)
            ->where(array(
                'tm.TM_TYPE'   => $type,
                't.T_DATE >='   => $start_date,
                't.T_DATE <='   => $end_date
            ))
            ->get()->row();
    }
}


/*Read Account Detail By Account Id or Self Head Id*/
if ( ! function_exists('accounts_by_ids'))
{
    function accounts_by_ids($id,$type)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select('sum(A_OPENINGBALANCE) as opening_balance')
            /* ->group_start()
                ->where('A_ID',$id)
                ->or_where('A_SELFHEAD_ID',$id)
                ->or_where_in('A_SELFHEAD_ID',"select A_ID from accounts where A_SELFHEAD_ID=$id",false)
            ->group_end() */
            ->where('(A_ID ='.$id.' OR A_SELFHEAD_ID = '.$id.' OR A_SELFHEAD_ID IN(select A_ID from accounts where A_SELFHEAD_ID='.$id.'))', NULL, FALSE)
            ->where('A_OPENINGTYPE',$type)
            ->get('accounts')->row();
    }
}

/*Read Active Sub Type For Balance Sheet*/
if(!function_exists('sub_type_for_balance'))
{
    function sub_type_for_balance($sub_name)
    {
        $ci=& get_instance();
        return $ci->db->where('HEAD_TYPE',$sub_name)
            ->where('SUB_STATUS', 1)->get('sub_heads')->result();
    }
}
/* ACCOUNT CLIENT ID*/
if ( ! function_exists('client_name'))
{
    function client_name($id)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select('name')
            ->where('client_id',$id)
            ->get('tbl_client')
            ->row()->name;
    }
}

/* USER ACCOUNT DETAIL*/
if ( ! function_exists('user_account_detail'))
{
    function user_account_detail($id)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select('*')
            ->where('user_id',$id)
            ->get('tbl_account_details')
            ->row();
    }
}

/* COUNTRIES */
if ( ! function_exists('origin'))
{
    function origin($id)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select('value')
            ->where('id',$id)
            ->get('tbl_countries')
            ->row()->value;
    }
}
/* CITY */
if ( ! function_exists('city'))
{
    function city($id)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select('name')
            ->where('id',$id)
            ->get('tbl_cities')
            ->row()->name;
    }
}

if (!function_exists('accounts_by_self')){
    function accounts_by_self($self_head_id){
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select('A_ID,A_NAME,CLIENT_ID')
            ->where('A_SELFHEAD_ID',$self_head_id)
            ->get('accounts')->result();
    }
}

if ( ! function_exists('job_expense_id'))
{
    function job_expense_id($invoice_id,$title)
    {
        $ci =& get_instance();
        $ci->load->database();
        $query = $ci->db->select('A_ID')
            ->from('accounts a')
            ->join('tbl_client cl','cl.client_id=a.CLIENT_ID')
            ->join('tbl_invoices inv','inv.client_id=cl.client_id')
            ->where('inv.invoices_id', $invoice_id)
            ->where('A_NAME', $title)
            ->get()->row();
        if( !empty($query)){
            return $query->A_ID;
        }else{
            return 0;
        }
    }
}

if ( ! function_exists('job_info'))
{
    function job_info($id)
    {
        $ci =& get_instance();
        $ci->load->database();
        return $ci->db->select('*')
            ->where('invoices_id', $id)
            ->get('tbl_invoices')
            ->row();
    }
}

if ( ! function_exists('job_activity'))
{
    function job_activity($id)
    {
        $ci =& get_instance();
        $ci->load->database();
        return $ci->db->select('*')
            ->where('job_status_id', $id)
            ->get('tbl_job_status')
            ->row()->job_status_title;
    }
}
if ( ! function_exists('commodity_type'))
{
    function commodity_type($id)
    {
        $ci =& get_instance();
        $ci->load->database();
        return $ci->db->select('commodity_type')
            ->where('commodity_type_id', $id)
            ->get('tbl_commodity_types')
            ->row()->commodity_type;
    }
}
if (!function_exists('petty_cash_details')){
    function petty_cash_details($id){
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->select('pcd.*,a.A_NAME')
            ->from('petty_cash_details pcd')
            ->join('accounts a','pcd.A_ID=a.A_ID')
            ->where('PC_ID',$id)
            ->get()->result();
    }
}

if ( ! function_exists('job_expense_title'))
{
    function job_expense_title($id)
    {
        $ci =& get_instance();
        $ci->load->database();
        return $ci->db->select('job_expense_title')
            ->where('job_expense_id', $id)
            ->get('tbl_job_expenses')
            ->row()->job_expense_title;
    }
}
if ( ! function_exists('shipping_line'))
{
    function shipping_line($id)
    {
        $ci =& get_instance();
        $ci->load->database();
        return $ci->db->select('shipping_name')
            ->where('shipping_id', $id)
            ->get('tbl_shipping_line')
            ->row()->shipping_name;
    }
}

if ( ! function_exists('field_staff_details'))
{
    function field_staff_details($id)
    {
        $ci =& get_instance();
        $ci->load->database();
        return $ci->db->select('*')
            ->where('account_id', $id)
            ->get('tbl_staff')
            ->row();
    }
}

/*Get Voucher Type and No*/
if (!function_exists('get_voucher_no')){
    function get_voucher_no($voucher_type,$transaction_id){
        $ci=& get_instance();
        $ci->load->database();
        $table='';
        $column='';
        $where_column='';
        if ($voucher_type=='Job Expenses'){
            $table='tbl_vouchers';
            $column='voucher_no';
            $where_column='transaction_id';
        }elseif ($voucher_type=='Payment Voucher'){
            $table='payment_vouchers';
            $column='PV_VOUCHER_NO';
            $where_column='T_ID';
        }elseif ($voucher_type=='Receipt Voucher'){
            $table='tbl_advance_payments';
            $column='voucher_no';
            $where_column='transaction_id';
        }elseif ($voucher_type=='General Expense'){
            $table='petty_cash';
            $column='PC_VOUCHER_NO';
            $where_column='T_ID';
        }
        if ($table==''||$column==''||$where_column==''){
            return '';
        }else{
            $result=$ci->db->select("$column as voucher_no")->where($where_column,$transaction_id)->get($table)->row();
            return (!empty($result))?$result->voucher_no:'';
        }
    }
}

/*Read Accounts By Sub Head Id*/
if(!function_exists('count_accounts_by_sub_head')){
    function count_accounts_by_sub_head($id)
    {
        $ci=& get_instance();
        $ci->load->database();
        return $ci->db->from('accounts')->where('SUB_HEAD_ID',$id)->count_all_results();
    }
}

/*** YARD ***/
if ( ! function_exists('yard'))
{
    function yard($id)
    {
        $ci =& get_instance();
        $ci->load->database();
        return $ci->db->select('*')
            ->where('yard_id', $id)
            ->get('tbl_yards')
            ->row()->yard;
    }
}

/*** INCOTERM ***/
if ( ! function_exists('incoterm'))
{
    function incoterm($id)
    {
        $ci =& get_instance();
        $ci->load->database();
        return $ci->db->select('*')
            ->where('incoterm_id', $id)
            ->get('tbl_incoterms')
            ->row()->incoterm;
    }
}
/*** WEIGHT UNIT ***/
if ( ! function_exists('weight_unit'))
{
    function weight_unit($id)
    {
        $ci =& get_instance();
        $ci->load->database();
        return $ci->db->select('*')
            ->where('weight_unit_id', $id)
            ->get('tbl_weight_units')
            ->row()->weight_unit;
    }
}
/*** GD TYPE ***/
if ( ! function_exists('gd_type'))
{
    function gd_type($id)
    {
        $ci =& get_instance();
        $ci->load->database();
        return $ci->db->select('*')
            ->where('gd_type_id', $id)
            ->get('tbl_gd_types')
            ->row()->gd_type;
    }
}
/*** JOB DOCUMENT TITLE ***/
if ( ! function_exists('document_title'))
{
    function document_title($id)
    {
        $ci =& get_instance();
        $ci->load->database();
        return $ci->db->select('*')
            ->where('document_type_id', $id)
            ->get('tbl_document_types')
            ->row()->document_title;
    }
}
    /*** JOB TYPE ***/
    if ( ! function_exists('job_type'))
    {
        function job_type($id)
        {
            $ci =& get_instance();
            $ci->load->database();
            return $ci->db->select('*')
                ->where('Iid', $id)
                ->get('stbjobtype')
                ->row()->VCJobType;
        }
    }
/*** SHED ***/
if ( ! function_exists('sheds'))
{
    function sheds($id)
    {
        $ci =& get_instance();
        $ci->load->database();
        return $ci->db->select('*')
            ->where('shed_id', $id)
            ->get('tbl_sheds')
            ->row()->shed;
    }
}

?>