<?php

class Menu {

    public function dynamicMenu() {
        $CI = & get_instance();
        $CI->load->database();
        $user_id = $CI->session->userdata('user_id');
        $user_type = $CI->session->userdata('user_type');
        if ($user_type != 1) {
            $user_menu =  $CI->db->select('*')
                ->from('tbl_user_role')
                ->join('tbl_menu', 'tbl_user_role.menu_id=tbl_menu.menu_id')
                ->where('tbl_user_role.user_id', $user_id)
                ->order_by('sort', 'ASC')
                ->get()->result_array();
        } else {
            $user_menu =  $CI->db->select('*')->order_by('sort', 'ASC')->get('tbl_menu')->result_array();
        }

        $menu = array(
            'items' => array(),
            'parents' => array()
        );

        foreach($user_menu as $items){
            $menu['items'][$items['menu_id']] = $items;
            $menu['parents'][$items['parent']][] = $items['menu_id'];
        }
        return $output = $this->buildMenu(0, $menu);
    }

    public function buildMenu($parent, $menu, $sub = NULL) {

        $html = "";

        if (isset($menu['parents'][$parent])) {
            if (!empty($sub)) {
                $html .= "<ul class='treeview-menu'>\n";
                $a = "";
                $i = "";
            } else {
                $html .= "<ul class='sidebar-menu'>\n";
                $a = "sidebar-margin";
                $i = "sidebar-icon ";
            }
            foreach ($menu['parents'][$parent] as $itemId) {
                $result = $this->active_menu_id($menu['items'][$itemId]['menu_id']);
                if ($result) {
                    $active = 'active';
                } else {
                    $active = '';
                }

                if (!isset($menu['parents'][$itemId])) { //if condition is false only view menu
                    $html .= "<li class='" . $active . "' >\n  <a href='" . base_url() . $menu['items'][$itemId]['link'] . "' class='".$a."'> <i class='" .$i. $menu['items'][$itemId]['icon'] . "'></i><span>" . lang($menu['items'][$itemId]['label']) . "</span></a>\n</li> \n";
                }

                if (isset($menu['parents'][$itemId])) { //if condition is true show with submenu
                    $html .= "<li class='treeview " . $active . "'>\n  <a href='" . base_url() . $menu['items'][$itemId]['link'] . "' class='sidebar-margin'> <i class='sidebar-icon " . $menu['items'][$itemId]['icon'] . "'></i><span>" . lang($menu['items'][$itemId]['label']) . "</span><i class='fa fa-angle-right pull-right'></i></a>\n";
                    $html .= self::buildMenu($itemId, $menu, true);
                    $html .= "</li> \n";
                }
            }
            $html .= "</ul> \n";
        }
        return $html;
    }

    public function active_menu_id($id) {
        $CI = & get_instance();
        $activeId = $CI->session->userdata('menu_active_id');

        if (!empty($activeId)) {
            foreach ($activeId as $v_activeId) {
                if ($id == $v_activeId) {
                    return TRUE;
                }
            }
        }
        return FALSE;
    }

}
