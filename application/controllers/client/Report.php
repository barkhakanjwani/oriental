<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Report extends Client_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('report_model');
        $this->load->model('accounts_model');
        $this->load->model('invoice_model');

        $this->load->helper('ckeditor');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "99.8%",
                'height' => "400px"
            )
        );
    }

    /*** My Reporting ***/
    /*** JOB REPORTS ***/
    public function job_report() {
        check_url();
        $data['title'] = "Job Report";
        $data['page'] = lang('job_report');
        $subview = 'job_report';

        if($_POST){
            $start_date = date('Y-m-d',strtotime($this->input->post('start_date')));
            $end_date = date('Y-m-d',strtotime($this->input->post('end_date')));
            $data['start_date'] = $start_date;
            $data['end_date'] = $end_date;
            $this->db->select('*')
                ->from('tbl_invoices inv')
                ->join('tbl_saved_commodities comm','inv.invoices_id = comm.invoices_id')
                ->where(array(
                    'inv.created_date >='     => $start_date,
                    'inv.created_date <='     => $end_date
                ));
            foreach($_POST as $coulmn=>$value) {
                $data[$coulmn] = $value;
                if ($coulmn != 'start_date' && $coulmn != 'end_date') {
                    if(!empty($value)){
                        if($coulmn == 'commodity' || $coulmn == 'hs_code'){
                            $a= 'comm.';
                            $this->db->where($a.$coulmn, $value);
                        }
                        else{
                            $a= 'inv.';
                            $this->db->where($a.$coulmn, $value);
                        }
                    }
                }
            }
            $this->db->where('inv.client_id',$this->session->userdata('client_id'));
            $this->db->group_by('inv.invoices_id');
            $invoices = $this->db->get()->result();
            $data['invoices'] = $invoices;
        }


        $this->report_model->_table_name = 'tbl_job_status';
        $this->report_model->_order_by = 'job_status_id';
        $data['all_status'] = $this->report_model->get();
        /*** GET CLIENTS ***/
        $this->report_model->_table_name = 'tbl_client';
        $this->report_model->_order_by = 'name';
        $data['all_clients'] = $this->report_model->get();
        /*** GET COUNTRIES ***/
        $this->report_model->_table_name = 'tbl_countries';
        $this->report_model->_order_by = 'id';
        $data['all_countries'] = $this->report_model->get();

        $data['sheds'] = $this->report_model->check_groupby_all('tbl_invoices', '', array('port'), array('port'));
        $data['subview'] = $this->load->view('client/report/' . $subview, $data, TRUE);
        $this->load->view('client/_layout_main', $data);
    }
    /*** END JOB REPORTS ***/
}