<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Invoice extends Client_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('invoice_model');

        $this->load->helper('ckeditor');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "99.8%",
                'height' => "400px"
            )
        );
    }

    public function all_invoices() {
        check_url();
        $data['title'] = "All Jobs";
        $data['page'] = lang("all_jobs");
        $subview = 'all_invoices';

        $user_id = $this->session->userdata('user_id');
        $user_info = $this->invoice_model->check_by(array('user_id' => $user_id), 'tbl_users');
        $data['role'] = $user_info->role_id;

        $this->invoice_model->_table_name = 'tbl_invoices';
        $this->invoice_model->_order_by = 'reference_no';
        $data['all_invoices'] = $this->invoice_model->get_by(array('invoices_id !=' => '0', 'client_id' => $user_info->client_id), FALSE);



        $data['subview'] = $this->load->view('client/invoice/' . $subview, $data, TRUE);
        $this->load->view('client/_layout_main', $data);
    }
}
