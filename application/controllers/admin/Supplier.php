<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Supplier extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('invoice_model');
        $this->load->model('client_model');
    }

    /*     * * Create New Supplier ** */
    public function manage_supplier($action = NULL, $id = NULL) {
        check_url();
        if ($action == 'edit_supplier') {
            $id = decode($id);
            $data['active'] = 2;
            $data['supplier_info'] = $this->user_model->check_by(array('supplier_id'=>$id),'tbl_supplier');
        } else {
            $data['active'] = 1;
        }
        $data['title'] = 'Supplier List';
        $this->user_model->_table_name = 'tbl_supplier';
        $this->user_model->_order_by = 'supplier_id';
        $data['all_suppliers'] = $this->user_model->get();

        $data['subview'] = $this->load->view('admin/supplier/supplier_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    public function manage_suppliers()
    {
        $search_array=array();
        $rows = array();
        $search_value=$_POST['search']['value'];
        if ($search_value){
            $search_array=array(
                'su.supplier_name'  => $search_value
            );
        }
        $length = 10; $start = 1;
        if($_POST['length'] != -1){
            $length = $_POST['length'];
            $start = $_POST['start'];
        }
        $supplier_list = $this->client_model->supplier_searching_list($search_array,$length,$start);
        $supplier_list_rows = $this->client_model->supplier_searching_list_rows($search_array);
        $i = $_POST['start']+1;
        foreach($supplier_list as $supplier){
            if ($supplier->supplier_status == 1){
                $status = '<a data-toggle="tooltip" data-placement="top" title="Click to Deactive" href="'.base_url('admin/supplier/change_status/0/'.encrypt($supplier->supplier_id)).'">'.lang('active').'</a>';
            }else{
                $status = '<a data-toggle="tooltip" data-placement="top" title="Click to Active "  href="'.base_url('admin/supplier/change_status/1/'.encrypt($supplier->supplier_id)).'">'.lang('deactive').'</a>';
            }

            $rows[] = array(
                strftime(config_item('date_format'), strtotime($supplier->supplier_date)),
                ucfirst($supplier->supplier_name),
                $supplier->supplier_email,
                $supplier->supplier_contact,
                $status,
                btn_edit('admin/supplier/manage_supplier/edit_supplier/' . encode($supplier->supplier_id))
            );
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $supplier_list_rows,
            "recordsFiltered" => $supplier_list_rows,
            "data" => $rows
        );
        echo json_encode($output);
    }

    public function check_supplier_email($id=NULL){
        $email = $_POST['email'];
        $get_email = array();
        if(!empty($id)){
            $id = decrypt($id);
            $get_email = $this->db->select('*')
                ->from('tbl_supplier')
                ->where('supplier_id != '.$id)
                ->where('supplier_email',$email)
                ->get()->result();
        }
        else{
            $get_email = $this->user_model->check_by(array('supplier_email' => $email), 'tbl_supplier');
        }
        if(!empty($get_email)){
            echo json_encode(false);
            die();
        }
        else{
            echo json_encode(true);
            die();
        }
    }

    public function change_status($flag, $id) {
        check_url();
        if ($flag == 1) {
            $msg = 'Active';
        } else {
            $msg = 'Deactive';
        }
        $id = decrypt($id);
        $where = array('supplier_id' => $id);
        $action = array('supplier_status' => $flag, 'updated_by'=>$this->session->userdata('user_id'));
        $this->user_model->set_action($where, $action, 'tbl_supplier');

        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'Supplier',
            'module_field_id' => $id,
            'activity' => lang('activity_change_supplier_status'),
            'icon' => 'fa-user',
            'value1' => $msg,
        );
        $this->user_model->_table_name = 'tbl_activities';
        $this->user_model->_primary_key = "activities_id";
        $this->user_model->save($activities);

        $type = "success";
        $message = lang('supplier')." " . $msg . " Successfully!";
        set_message($type, $message);
        redirect('admin/supplier/manage_supplier'); //redirect page
    }
    
    /*** Save New Supplier ***/
    public function save_supplier($id = null) {
        $login_data = $this->user_model->array_from_post(array('supplier_name', 'supplier_email', 'supplier_contact'));
        $id = (!empty($id))?decrypt($id):NULL;
        $message = (!empty($id))?lang('update_supplier_info'):lang('save_supplier_info');
        $act = (!empty($id))?lang('activity_update_supplier'):lang('activity_new_supplier');
        
        if(empty($id)){
            $account_head = $this->user_model->check_by(array('H_TYPE'=>'Liabilities', 'H_NAME'=>'Account Payables'), 'accounts_head');
            $this->load->model('accounts_model');
            $rs = $this->accounts_model->account_no_by_head($account_head->H_ID);
            if($rs->A_NO!=""){
                $pos=strlen($rs->A_NO)-1;
                $account_no = substr_replace($rs->A_NO,substr($rs->A_NO,$pos)+1,$pos);
            }else{
                $rs = $this->accounts_model->select_edit_head($account_head->H_ID);
                $account_no=$rs->H_NO.".".(1);
            }
            $account = array(
                'H_ID'          => $account_head->H_ID,
                'SUB_HEAD_ID'   => $account_head->SUB_HEAD_ID,
                'A_NAME'        => $this->input->post('supplier_name'). " (".$account_head->H_NAME.")",
                'A_DESC'        => '',
                'A_OPENINGTYPE' => '',
                'A_OPENINGBALANCE'=>'',
                'A_SELFHEAD_ID' => 0,
                'A_NO'          => $account_no,
                'A_STATUS'      => 1,
                'CREATED_BY'    => $this->session->userdata('user_id')
            );
            $this->user_model->_table_name = 'accounts';
            $this->user_model->_primary_key = "A_ID";
            $account_id=$this->user_model->save($account);
            
            $login_data['created_by'] = $this->session->userdata('user_id');
            $login_data['account_id'] = $account_id;
            $this->user_model->_table_name = 'tbl_supplier';
            $this->user_model->_primary_key = "supplier_id";
            $id = $this->user_model->save($login_data);
        } else{
            $supplier_info=$this->user_model->check_by(array('supplier_id'=>$id),'tbl_supplier');
            $account = array(
                'A_NAME' => $this->input->post('supplier_name'). " (Account Payables)",
                'UPDATED_BY'    => $this->session->userdata('user_id')
            );
            $this->user_model->_table_name = 'accounts';
            $this->user_model->_primary_key = "A_ID";
            $this->user_model->save($account,$supplier_info->account_id);
            
            $login_data['updated_by'] = $this->session->userdata('user_id');
            $this->user_model->_table_name = 'tbl_supplier';
            $this->user_model->_primary_key = "supplier_id";
            $this->user_model->save($login_data, $id);
        }
        
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'Supplier',
            'module_field_id' => $id,
            'activity' => $act,
            'icon' => 'fa-user',
            'value1' => $login_data['supplier_name']
        );
        $this->user_model->_table_name = 'tbl_activities';
        $this->user_model->_primary_key = "activities_id";
        $this->user_model->save($activities);
        $type = 'success';
        set_message($type, $message);
        redirect('admin/supplier/manage_supplier'); //redirect page
    }
}
