<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Po extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('invoice_model');

        $this->load->helper('ckeditor');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "99.8%",
                'height' => "400px"
            )
        );
    }

    public function index(){
        $subview = 'manage_deposit';
        $data['subview'] = 'admin/security_deposit/'.$subview;
        $this->load->view('admin/_layout_main2', $data);
    }

    public function manage_po($action = NULL, $id = NULL) {
        $data['page'] = lang('pay_orders');
        $data['title'] = "Create Pay Order";
        if (!empty($id)) {
            $data['title'] = "Manage Pay Order";
            $this->db->select('*');
            $this->db->from('tbl_invoices');
            $this->db->join('tbl_pay_orders', 'tbl_invoices.invoices_id = tbl_pay_orders.invoices_id');
            $this->db->where('tbl_pay_orders.po_id', $id);
            $po_info = $this->db->get()->row();
            $data['po_info'] = $po_info;
            $data['account_id'] = $this->db->select('A_ID')
                ->from('transactions_meta')
                ->where('T_ID', $po_info->transaction_id)
                ->order_by('TM_ID', 'DESC')
                ->get()->row();
        }
        /** ALL JOBS **/
        $this->invoice_model->_table_name = 'tbl_invoices';
        $this->invoice_model->_order_by = 'invoices_id';
        $data['all_invoice_info'] = $this->invoice_model->get();
        if ($action == 'po_details') {
            $data['title'] = "Pay Order Details";
            $this->db->select('*');
            $this->db->from('tbl_invoices');
            $this->db->join('tbl_pay_orders', 'tbl_invoices.invoices_id = tbl_pay_orders.invoices_id');
            $this->db->join('accounts', 'accounts.A_ID = tbl_pay_orders.a_id');
            $this->db->where('tbl_pay_orders.po_id', $id);
            $data['po_info'] = $this->db->get()->row();
            $subview = 'po_details';
        }
        elseif ($action == 'pdf_po') {
            $data['title'] = "PO PDF";
            $this->load->helper('dompdf');
            $this->db->select('*');
            $this->db->from('tbl_invoices');
            $this->db->join('tbl_pay_orders', 'tbl_invoices.invoices_id = tbl_pay_orders.invoices_id');
            $this->db->join('accounts', 'accounts.A_ID = tbl_pay_orders.a_id');
            $this->db->where('tbl_pay_orders.po_id', $id);
            $data['po_info'] = $this->db->get()->row();

            $viewfile = $this->load->view('admin/pay_orders/pdf_po2', $data, TRUE);
            pdf_create($viewfile, 'PO # ' . $data['po_info']->reference_no);
        }
        else{
            $subview = 'manage_po';
        }

        $user_id = $this->session->userdata('user_id');

        $user_info = $this->invoice_model->check_by(array('user_id' => $user_id), 'tbl_users');
        $data['role'] = $user_info->role_id;

        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'client_id';
        $data['all_client'] = $this->invoice_model->get();
        $this->invoice_model->_table_name = 'banks';
        $this->invoice_model->_order_by = 'B_ID';
        $data['banks'] = $this->invoice_model->get();

        $data['subview'] = 'admin/pay_orders/'.$subview;
        $this->load->view('admin/_layout_main2', $data);
    }

    public function get_client_jobs($client_id)
    {
        $jobs = $this->invoice_model->check_by_all(array('client_id'=>$client_id), 'tbl_invoices');
        echo json_encode($jobs);
        die();
    }

    public function get_advance_accounts($job_id)
    {
        $jobs = $this->invoice_model->check_by(array('invoices_id'=>$job_id), 'tbl_invoices');
        $requisition = $this->invoice_model->check_by(array('invoices_id'=>$job_id), 'tbl_requisitions');
        $advances_accounts = array();
        if(!empty($requisition)){
            $ap_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Liabilities', 'NAME' => 'Current Liabilities'), 'sub_heads');
            $ap_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_head->SUB_HEAD_ID, 'H_NAME' => 'Advances from clients'), 'accounts_head');
            $advances_accounts = $this->invoice_model->check_by_all(array('SUB_HEAD_ID' => $ap_sub_head->SUB_HEAD_ID, 'H_ID' => $ap_sub_head->H_ID, 'CLIENT_ID' => $jobs->client_id), 'accounts');
        }

        echo json_encode($advances_accounts);
        die();
    }

    public function search_po(){
        if($_POST){
            if(isset($_POST['client_id'])){
                $client_id = $_POST['client_id'];
                $data['client_id'] = $client_id;
                $this->db->select('*');
                $this->db->from('tbl_invoices');
                $this->db->join('tbl_client', 'tbl_client.client_id = tbl_invoices.client_id');
                $this->db->join('tbl_pay_orders', 'tbl_invoices.invoices_id = tbl_pay_orders.invoices_id');
                $this->db->join('accounts', 'accounts.A_ID = tbl_pay_orders.a_id');
                $this->db->where('tbl_pay_orders.client_id',$_POST['client_id']);
                $data['po_info'] = $this->db->get()->result();
            }
            if(isset($_POST['reference_no'])) {
                $reference_no = $_POST['reference_no'];
                $data['reference_no'] = $reference_no;
                $invoice_info = $this->invoice_model->check_by(array('reference_no' => $reference_no), 'tbl_invoices');

                if (!empty($invoice_info)){
                    $this->db->select('*');
                    $this->db->from('tbl_invoices');
                    $this->db->join('tbl_client', 'tbl_client.client_id = tbl_invoices.client_id');
                    $this->db->join('tbl_pay_orders', 'tbl_invoices.invoices_id = tbl_pay_orders.invoices_id');
                    $this->db->join('accounts', 'accounts.A_ID = tbl_pay_orders.a_id');
                    $this->db->where('tbl_invoices.invoices_id', $invoice_info->invoices_id);
                    $invoice_info = $this->db->get()->result();
                }
                $data['po_info'] = $invoice_info;
            }
        }
        $this->invoice_model->_table_name = 'tbl_invoices';
        $this->invoice_model->_order_by = 'invoices_id';
        $data['all_invoices'] = $this->invoice_model->get();
        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'name';
        $data['all_client'] = $this->invoice_model->get();
        $this->invoice_model->_table_name = 'tbl_bills';
        $this->invoice_model->_order_by = 'bill_id';
        $data['all_bill'] = $this->invoice_model->get();

        $data['title'] = "Search Pay Order";
        $subview = 'search_po';

        $data['subview'] = $this->load->view('admin/pay_orders/' . $subview, $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_po($id = NULL)
    {
        $client_id = $this->input->post('client_id', TRUE);
        $invoices_id = $this->input->post('invoices_id', TRUE);
        $a_id = $this->input->post('a_id', TRUE);
        $po_amount = $this->input->post('po_amount', TRUE);
        $po_date = $this->input->post('po_date', TRUE);
        $branch_id = $this->input->post('branch_id', TRUE);
        $account_id = $this->input->post('account_id', TRUE);
        $po_no = $this->input->post('po_no', TRUE);
        $po_note = $this->input->post('po_note', TRUE);
        $po_month = date("m", strtotime($po_date));
        $po_year = date("Y", strtotime($po_date));
        $inv_info = $this->invoice_model->check_by(array('invoices_id' => $invoices_id), 'tbl_invoices');
        if (!empty($id)) {
            $data = array(
                'invoices_id' => $invoices_id,
                'client_id' => $client_id,
                'branch_id' => $branch_id,
                'a_id' => $a_id,
                'po_amount' => $po_amount,
                'po_date' => $po_date,
                'po_no' => $po_no,
                'po_note' => $po_note,
                'po_month' => $po_month,
                'po_year' => $po_year,
            );
            $this->invoice_model->_table_name = 'tbl_pay_orders';
            $this->invoice_model->_primary_key = 'po_id';
            $this->invoice_model->save($data, $id);
            $po_id = $id;

            $trans_account_id = $_POST['trans_account_id'];
            $bank_and_cash_id = $_POST['bank_and_cash_id'];
            $data = array(
                'TM_AMOUNT'=>$po_amount
            );
            $this->invoice_model->_table_name = 'transactions_meta';
            $this->invoice_model->_primary_key = 'TM_ID';
            $this->invoice_model->save($data, $trans_account_id);

            $data = array(
                'A_ID'=>$account_id,
                'TM_AMOUNT'=>$po_amount
            );
            $this->invoice_model->_table_name = 'transactions_meta';
            $this->invoice_model->_primary_key = 'TM_ID';
            $this->invoice_model->save($data, $bank_and_cash_id);

            $act = lang('update_po');
            $msg = lang('update_po_activity');
        } else {
                $data = array(
                    "T_DATE"=>date('Y-m-d'),
                    "T_PAY"=>"",
                    "T_TYPE"=>"Pay Order"
                );
                $this->invoice_model->_table_name = 'transactions';
                $this->invoice_model->_primary_key = 'T_ID';
                $transaction_id = $this->invoice_model->save($data);

                $data = array(
                    'invoices_id' => $invoices_id,
                    'client_id' => $client_id,
                    'branch_id' => $branch_id,
                    'a_id' => $a_id,
                    'transaction_id' => $transaction_id,
                    'po_amount' => $po_amount,
                    'po_date' => $po_date,
                    'po_no' => $po_no,
                    'po_note' => $po_note,
                    'po_month' => $po_month,
                    'po_year' => $po_year,
                );
                $this->invoice_model->_table_name = 'tbl_pay_orders';
                $this->invoice_model->_primary_key = 'po_id';
                $po_id = $this->invoice_model->save($data);

                $data = array(
                    "T_ID"=>$transaction_id,
                    "A_ID"=>$a_id,
                    "TM_AMOUNT"=>$po_amount,
                    "TM_TYPE"=>'Credit',
                    "IS_ACTIVE"=>1
                );
                $this->invoice_model->_table_name = 'transactions_meta';
                $this->invoice_model->_primary_key = 'TM_ID';
                $this->invoice_model->save($data);

                $data = array(
                    'T_ID'=>$transaction_id,
                    'A_ID'=>$account_id,
                    'TM_AMOUNT'=>$po_amount,
                    'TM_TYPE'=>'Debit',
                    'IS_ACTIVE'=>1
                );
                $this->invoice_model->_table_name = 'transactions_meta';
                $this->invoice_model->_primary_key = 'TM_ID';
                $this->invoice_model->save($data);
                $title = read_subHead($a_id)->A_NAME;
                if($title == 'Security Deposit'){
                    $deposit_data = array(
                        'invoices_id'=>$invoices_id,
                        'deposit_amount'=>$po_amount,
                        'payorder_creation_date'=>$po_date
                    );
                    $this->invoice_model->_table_name = 'tbl_security_deposit';
                    $this->invoice_model->_primary_key = 'deposit_id';
                    $this->invoice_model->save($deposit_data);
                }

                $act = lang('create_po');
                $msg = lang('create_po_activity');
            }

        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'Pay Order',
            'module_field_id' => $po_id,
            'activity' => $act,
            'icon' => 'fa-usd',
            'value1' => $inv_info->reference_no,
            'value2' => 'PKR ' . $po_amount,
        );
        $this->invoice_model->_table_name = 'tbl_activities';
        $this->invoice_model->_primary_key = 'activities_id';
        $this->invoice_model->save($activity);
        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/po/search_po/');
    }

}
