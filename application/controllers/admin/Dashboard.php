<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->model('invoice_model');
    }

    public function index($action = NULL) {
        $data['title'] = config_item('company_name');
        $data['page'] = lang('dashboard');

        $user_id = $this->session->userdata('user_id');
        $user_info = $this->admin_model->check_by(array('user_id' => $user_id), 'tbl_users');
        $data['role'] = $user_info->role_id;

        if (!empty($action) && $action == 'payments') {
            $data['yearly'] = $this->input->post('yearly', TRUE);
        } else {
            $data['yearly'] = date('Y');
        }
        if ($this->input->post('year', TRUE)) {
            $data['year'] = $this->input->post('year', TRUE);
        } else {
            $data['year'] = date('Y');
        }

        $data['yearly_overview'] = $this->get_yearly_overview($data['yearly']);
        $job_status = $this->db->get('tbl_job_status')->result();
        $activity_count = array();
        foreach($job_status as $status){
            $job_acitivity = $this->admin_model->pieChart($status->job_status_id);
            if(!empty($job_acitivity)){
                $activity_count[] = array(
                    'count'=>$job_acitivity[0]['invoices_count'],
                    'job_status_title'=>$status->job_status_title,
                );
            }
            else{
                $activity_count[] = array(
                    'count'=>0,
                    'job_status_title'=>$status->job_status_title,
                );
            }
        }

        $all_invoices = $this->db->get('tbl_invoices')->result();
        $total_overall_amount = 0;
        $total_due_amount = 0;
        $total_paid_amount = 0;
        if(!empty($all_invoices)){
            foreach($all_invoices as $invoice){
                $bill_info = $this->invoice_model->check_by(array('invoices_id'=>$invoice->invoices_id), 'tbl_bills');
                if(!empty($bill_info)){
                    $total_overall_amount2 = $this->invoice_model->calculate_bill('bill_grand_total', $bill_info->bill_id);
                    $total_paid_amount2 = $this->invoice_model->calculate_to('paid_amount', $bill_info->invoices_id);
                    $total_due_amount2 = $this->invoice_model->calculate_to('invoice_due', $bill_info->invoices_id);

                    $total_overall_amount += (float)$total_overall_amount2;
                    $total_due_amount += (float)$total_due_amount2;
                    $total_paid_amount += (float)$total_paid_amount2;
                }
            }
        }
        $data['total_overall_amount'] = $total_overall_amount;
        $data['total_due_amount'] = $total_due_amount;
        $data['total_paid_amount'] = $total_paid_amount;
        $data['pie_chart'] = $activity_count;

        $data['subview'] = $this->load->view('admin/main_content', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function get_yearly_overview($year) {// this function is to create get monthy recap report
        for ($i = 1; $i <= 12; $i++) { // query for months
            if ($i >= 1 && $i <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
                $month = '0' . $i;
            } else {
                $month = $i;
            }
            $yearly_report[$i] = $this->admin_model->calculate_amount($year, $month); // get all report by start date and in date
        }
        return $yearly_report; // return the result
    }
}
