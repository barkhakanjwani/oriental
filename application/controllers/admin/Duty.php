<?php
    /***
        Date: 11-12-2019
        Developer : Shahbaz Ahmed
        Change Owner: Usama Shakeel
        Comment:
        - Invoice Value calculate per item, also insurance and landing charges based on per commodity
        - Form Layout change
        - Print layout change
        - no. of units * unit value = total taxable value
        - total taxable value * { Rate of exchange (Manual) } % {(percentage or amount manaual) insurance}  % {Landing Charges} = Invoice Total per commodity
        - Invoice Total per commodity % CD .....
     ***/

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Duty extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('invoice_model');

        $this->load->helper('ckeditor');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "99.8%",
                'height' => "400px"
            )
        );
    }

    public function manage_duty($action = NULL, $id = NULL) {
        check_url();
        $data['page'] = lang('duty');
        $this->db->select('*')->from('tbl_invoices');
        $this->db->where('`invoices_id` NOT IN (SELECT `invoices_id` FROM `tbl_requisitions`)', NULL, FALSE);
        $data['all_invoices'] = $this->db->get()->result();
        $data['search_value'] = "";
        $data['title'] = "Create Requisition";
        /** ALL JOBS **/
        $this->invoice_model->_table_name = 'tbl_invoices';
        $this->invoice_model->_order_by = 'invoices_id';
        $data['all_invoice_info'] = $this->invoice_model->get();
        /** ALL REQUISITION EXPENSES **/
        $data['requisition_expenses'] = $this->invoice_model->check_by_all(array('requisition_expense_status'=>1),'tbl_requisition_expenses');
        if ($action == 'edit_duty') {
            $id = decode($id);
            $data['title'] = "Manage Requisition";
            $data['search_value'] = TRUE;
            $this->db->select('*');
            $this->db->from('tbl_requisitions');
            $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
            $this->db->where('tbl_requisitions.requisition_id', $id);
            $data['requisition_info'] = $this->db->get()->row();
            $requisition_info = $data['requisition_info'];
            $data['invoices_id'] = $requisition_info->invoices_id;
            $subview = 'manage_duty';
        }
        else if ($action == 'duty_details') {
            $id = decode($id);
            $data['title'] = "Requisition Details";
            $this->db->select('*');
            $this->db->from('tbl_requisitions');
            $this->db->join('tbl_invoices', 'tbl_invoices.invoices_id = tbl_requisitions.invoices_id');
            $this->db->where('tbl_requisitions.requisition_id', $id);
            $data['requisition_info'] = $this->db->get()->row();
            
            $data['financial_info'] = $this->invoice_model->check_by(array('invoices_id'=>$data['requisition_info']->invoices_id), 'tbl_financial_information');

            $this->db->select('*');
            $this->db->from('tbl_requisitions');
            $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
            $this->db->join('tbl_saved_commodities', 'tbl_saved_commodities.id = tbl_custom_duties.commodity_id');
            $this->db->where('tbl_requisitions.requisition_id', $id);
            $data['custom_duty_info'] = $this->db->get()->result();
            $subview = 'duty_details';
        }
        elseif ($action == 'pdf_duty') {
            $id = decode($id);
            $data['title'] = "Requisition PDF";
            $this->load->helper('dompdf');
            $this->db->select('*');
            $this->db->from('tbl_requisitions');
            $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
            $this->db->join('tbl_invoices', 'tbl_invoices.invoices_id = tbl_requisitions.invoices_id');
            $this->db->where('tbl_requisitions.requisition_id', $id);
            $data['requisition_info'] = $this->db->get()->row();

            $viewfile = $this->load->view('admin/duty/pdf_duty2', $data, TRUE);
            pdf_create($viewfile, 'Requisition # ' . $data['requisition_info']->reference_no);
        }
        else{
            $data['invoices_id'] = decode($id);
            $subview = 'manage_duty';
        }

        $user_id = $this->session->userdata('user_id');

        $user_info = $this->invoice_model->check_by(array('user_id' => $user_id), 'tbl_users');
        $data['role'] = $user_info->role_id;

        $data['subview'] = 'admin/duty/'.$subview;
        $this->load->view('admin/_layout_main2', $data);
    }

    public function create_duty(){
        check_url();
        $this->invoice_model->_table_name = 'tbl_invoices';
        $this->invoice_model->_order_by = 'invoices_id';
        $data['all_invoices'] = $this->invoice_model->get();
        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'name';
        $data['all_client'] = $this->invoice_model->get();
        if($_POST){
            if(isset($_POST['client_id'])){
                $data['client_id'] = $_POST['client_id'];
                $this->db->select('*')->from('tbl_invoices');
                $this->db->where('`invoices_id` NOT IN (SELECT `invoices_id` FROM `tbl_requisitions`)', NULL, FALSE);
                $this->db->where('client_id',$_POST['client_id']);
                $data['job_info'] = $this->db->get()->result();
            }
            if(isset($_POST['reference_no'])) {
                $reference_no = $_POST['reference_no'];
                $data['reference_no'] = $reference_no;
                $invoice_info = $this->invoice_model->check_by(array('reference_no' => $reference_no), 'tbl_invoices');
                if (!empty($invoice_info)){
                    $this->db->select('*')->from('tbl_invoices');
                    $this->db->where('`invoices_id` NOT IN (SELECT `invoices_id` FROM `tbl_requisitions`)', NULL, FALSE);
                    $this->db->where('invoices_id',$invoice_info->invoices_id);
                    $invoice_info = $this->db->get()->result();
                }
                $data['job_info'] = $invoice_info;
            }
        }
        $data['subview'] = 'admin/duty/create_duty';
        $this->load->view('admin/_layout_main2', $data);
    }

    public function search_duty(){
        check_url();
        if($_POST){
            if(isset($_POST['client_id'])){
                $data['client_id'] = $_POST['client_id'];
                $this->db->select('*');
                $this->db->from('tbl_requisitions');
                $this->db->join('tbl_invoices', 'tbl_invoices.invoices_id = tbl_requisitions.invoices_id');
                $this->db->where('tbl_invoices.client_id',$_POST['client_id']);
                $data['requisition_info'] = $this->db->get()->result();
            }
            if(isset($_POST['reference_no'])) {
                $reference_no = $_POST['reference_no'];
                $data['reference_no'] = $reference_no;
                $invoice_info = $this->invoice_model->check_by(array('reference_no' => $reference_no), 'tbl_invoices');
                if (!empty($invoice_info)){
                    $this->db->select('*');
                    $this->db->from('tbl_requisitions');
                    $this->db->join('tbl_invoices', 'tbl_invoices.invoices_id = tbl_requisitions.invoices_id');
                    $this->db->where('tbl_invoices.invoices_id', $invoice_info->invoices_id);
                    $invoice_info = $this->db->get()->result();
                }
                $data['requisition_info'] = $invoice_info;
            }
        }
        $this->invoice_model->_table_name = 'tbl_invoices';
        $this->invoice_model->_order_by = 'invoices_id';
        $data['all_invoices'] = $this->invoice_model->get();
        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'name';
        $data['all_client'] = $this->invoice_model->get();
        $data['title'] = "Search Requisition";
        $subview = 'search_duty';

        $data['subview'] = $this->load->view('admin/duty/'.$subview, $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_duty($id = NULL) {
        $data = $this->invoice_model->array_from_post(array('invoices_id','requisition_note'));
        $commodity_id=$this->input->post('commodity_id');
        $insurance_value=$this->input->post('insurance_value');
        $insurance_type=$this->input->post('insurance_type');
        $landing_charges=$this->input->post('landing_charges');
        $duty_cd=$this->input->post('duty_cd');
        $duty_fed=$this->input->post('duty_fed');
        $duty_acd=$this->input->post('duty_acd');
        $duty_rd=$this->input->post('duty_rd');
        $duty_st=$this->input->post('duty_st');
        $duty_ast=$this->input->post('duty_ast');
        $duty_it=$this->input->post('duty_it');
        if(!empty($id)){
            $req_id = decrypt($id);
            $data['updated_by'] = $this->session->userdata('user_id');
            $this->invoice_model->_table_name = 'tbl_requisitions';
            $this->invoice_model->_primary_key = 'requisition_id';
            $this->invoice_model->save($data, $req_id);
            /*** DELETE PREVIOUS ENTRIES ***/
            $this->invoice_model->_table_name = 'tbl_custom_duties';
            $this->invoice_model->delete_multiple(array('requisition_id' => $req_id));
            $this->invoice_model->_table_name = 'tbl_other_expenses';
            $this->invoice_model->delete_multiple(array('requisition_id' => $req_id));
            /*** CUSTOM DUTIES ***/
            for ($i = 0; $i < count($commodity_id); $i++) {
                $insurance_type2 = (!empty($insurance_type[$i])) ? $insurance_type[$i] : 'Amount';
                $landing_charges2 = (!empty($landing_charges[$i])) ? $landing_charges[$i] : 'No';
                $data_duties = array(
                    'requisition_id' => $req_id,
                    'commodity_id' => $commodity_id[$i],
                    'insurance_value' => $insurance_value[$i],
                    'insurance_type' => $insurance_type2,
                    'landing_charges' => $landing_charges2,
                    'duty_cd' => $duty_cd[$i],
                    'duty_fed' => $duty_fed[$i],
                    'duty_acd' => $duty_acd[$i],
                    'duty_rd' => $duty_rd[$i],
                    'duty_st' => $duty_st[$i],
                    'duty_ast' => $duty_ast[$i],
                    'duty_it' => $duty_it[$i]
                );
                $this->invoice_model->_table_name = 'tbl_custom_duties';
                $this->invoice_model->_primary_key = 'custom_duty_id';
                $this->invoice_model->save($data_duties);
            }
            /*** OTHER EXPENSES ***/
            /*$TM_ID = $this->input->post('TM_ID');
            $TM_AMOUNT = $this->input->post('TM_AMOUNT');
            for ($i = 0; $i < count($TM_ID); $i++) {
                $this->db->where('advance_id', $TM_ID[$i]);
                $this->db->update("tbl_advances", array('advance_amount' => $TM_AMOUNT[$i]));
            }*/

            $requisition_expense_id = $this->input->post('requisition_expense_id');
            $other_expense_amount = $this->input->post('other_expense_amount');

            for ($i = 0; $i < count($requisition_expense_id); $i++) {
                $expense_data = array(
                    'requisition_id' => $req_id,
                    'invoices_id' => $this->input->post('invoices_id'),
                    'requisition_expense_id' => $requisition_expense_id[$i],
                    'other_expense_amount' => $other_expense_amount[$i]
                );
                $this->invoice_model->_table_name = 'tbl_other_expenses';
                $this->invoice_model->_primary_key = 'other_expense_id';
                $this->invoice_model->save($expense_data);
            }

            $action = lang('activity_duty_updated');
            $msg = lang('duty_updated');
        }
        else{
            $data['created_by'] = $this->session->userdata('user_id');
            $this->invoice_model->_table_name = 'tbl_requisitions';
            $this->invoice_model->_primary_key = 'requisition_id';
            $req_id = $this->invoice_model->save($data);
            /*** CUSTOM DUTIES ***/
            for ($i = 0; $i < count($commodity_id); $i++) {
                $insurance_type2 = (!empty($insurance_type[$i])) ? $insurance_type[$i] : 'Amount';
                $landing_charges2 = (!empty($landing_charges[$i])) ? $landing_charges[$i] : 'No';
                $data_duties = array(
                    'requisition_id' => $req_id,
                    'commodity_id' => $commodity_id[$i],
                    'insurance_value' => $insurance_value[$i],
                    'insurance_type' => $insurance_type2,
                    'landing_charges' => $landing_charges2,
                    'duty_cd' => $duty_cd[$i],
                    'duty_fed' => $duty_fed[$i],
                    'duty_acd' => $duty_acd[$i],
                    'duty_rd' => $duty_rd[$i],
                    'duty_st' => $duty_st[$i],
                    'duty_ast' => $duty_ast[$i],
                    'duty_it' => $duty_it[$i]
                );
                $this->invoice_model->_table_name = 'tbl_custom_duties';
                $this->invoice_model->_primary_key = 'custom_duty_id';
                $this->invoice_model->save($data_duties);
            }
            /*** OTHER EXPENSES ***/
            $requisition_expense_id = $this->input->post('requisition_expense_id');
            $other_expense_amount = $this->input->post('other_expense_amount');

            for ($i = 0; $i < count($requisition_expense_id); $i++) {
                $expense_data = array(
                    'requisition_id' => $req_id,
                    'invoices_id' => $this->input->post('invoices_id'),
                    'requisition_expense_id' => $requisition_expense_id[$i],
                    'other_expense_amount' => $other_expense_amount[$i]
                );
                $this->invoice_model->_table_name = 'tbl_other_expenses';
                $this->invoice_model->_primary_key = 'other_expense_id';
                $this->invoice_model->save($expense_data);
            }
            
            $action = lang('activity_duty_created');
            $msg = lang('duty_created');
        }
        
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'requisition',
            'module_field_id' => $req_id,
            'activity' => $action,
            'icon' => 'fa-circle-o',
            'value1' => $this->invoice_model->job_no_creation($_POST['invoices_id']),
            'value2' => "PKR ".number_format($this->invoice_model->calculate_duty('declared_grand_total', $req_id),2)
        );
        $this->invoice_model->_table_name = 'tbl_activities';
        $this->invoice_model->_primary_key = 'activities_id';
        $this->invoice_model->save($activity);

        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/duty/search_duty');
    }
    
    /*** Server Side Listing ***/
    public function all_duties() {

        check_url();
        $data['title'] = "All Requisition";
        $subview = 'all_requisitions';

        if($_POST) {
            $data['client_id']=$_POST['client_id'];
            $data['reference_no']=$_POST['reference_no'];
            $data['commodities']=$_POST['commodity'];
            $data['bl_no']=$_POST['bl_no'];
        }

        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'name';
        $data['all_client'] = $this->invoice_model->get();

        $user_id = $this->session->userdata('user_id');
        $user_info = $this->invoice_model->check_by(array('user_id' => $user_id), 'tbl_users');
        $data['role'] = $user_info->role_id;

        $data['subview'] = $this->load->view('admin/duty/' . $subview, $data, TRUE);
        $this->load->view('admin/_layout_main', $data);

    }

    public function all_duty($client_id=null,$reference_no=null,$bl_no=null,$commodity=null)
    {
        $search_array=array();
        $rows = array();
        $search_value=$_POST['search']['value'];
        if ($search_value){
            $search_array=array(
                'cl.client_id'  => $search_value,
                'inv.reference_no'  => $search_value,
                'inv.bl_no'  => $search_value,
                'comm.commodity'  => $search_value
            );
        }
        $length = 10; $start = 1;
        if($_POST['length'] != -1){
            $length = $_POST['length'];
            $start = $_POST['start'];
        }

        $requisition_search_list = $this->invoice_model->requisition_searching_list($search_array,$length,$start,$client_id,$reference_no,$bl_no,$commodity);
        $requisition_search_list_rows = $this->invoice_model->requisition_searching_list_rows($search_array,$client_id,$reference_no,$bl_no,$commodity);
        $i = $_POST['start']+1;
        foreach($requisition_search_list as $requisition){
            if($requisition->requisition_type == 'Declared') {
                $grand_total_duties = number_format($this->invoice_model->calculate_duty('declared_grand_total_duties', $requisition->requisition_id), 2);
            }else{
                $grand_total_duties = number_format($this->invoice_model->calculate_duty('assessable_grand_total_duties', $requisition->requisition_id),2);
            }
            $other_expenses_total = number_format($this->invoice_model->calculate_duty('other_expenses_total', $requisition->invoices_id),2);
            if($requisition->requisition_type == 'Declared'){
                $grand_total = number_format($this->invoice_model->calculate_duty('declared_grand_total', $requisition->requisition_id),2);
            }else{
                $grand_total = number_format($this->invoice_model->calculate_duty('assessable_grand_total', $requisition->requisition_id), 2);
            }
            $action = btn_view('admin/duty/manage_duty/duty_details/' . encode($requisition->requisition_id))." ".btn_edit('admin/duty/manage_duty/edit_duty/' . encode($requisition->requisition_id));

            $rows[] = array(
                (!empty($requisition->invoices_id))?$this->invoice_model->job_no_creation($requisition->invoices_id):'',
                (!empty($requisition->created_date))?strftime(config_item('date_format'), strtotime($requisition->created_date)):'',
                (!empty($this->invoice_model->get_invoice_value($requisition->invoices_id)))?$requisition->requisition_type:'',
                (!empty($this->invoice_model->get_invoice_value($requisition->invoices_id)))?$requisition->currency." ".number_format($requisition->invoice_value):'',
                (!empty($requisition->exchange_rate))?number_format($requisition->exchange_rate):'',
                $grand_total_duties,
                $other_expenses_total,
                $grand_total,
                $action
            );
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $requisition_search_list_rows,
            "recordsFiltered" => $requisition_search_list_rows,
            "data" => $rows
        );

        echo json_encode($output);
    }

}
