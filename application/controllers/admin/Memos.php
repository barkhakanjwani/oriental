<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Memos extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('invoice_model');
    }

    public function index(){
        $subview = 'manage_memo';
        $data['subview'] = 'admin/memos/'.$subview;
        $this->load->view('admin/_layout_main2', $data);
    }

    public function manage_memo($action = NULL, $id = NULL) {
        check_url();
        $id = decode($id);
        $data['page'] = lang('memos');
        $this->db->select('*')->from('tbl_invoices');
        $this->db->where('`invoices_id` IN (SELECT `invoices_id` FROM `tbl_bills`)', NULL, FALSE);
        $data['all_invoices'] = $this->db->get()->result();
        $data['search_value'] = "";
        $data['title'] = "Create Memo";
        if (!empty($id)) {
            $data['title'] = "Manage Memo";
            $data['search_value'] = TRUE;
            $this->db->select('*');
            $this->db->from('tbl_memos');
            $this->db->join('tbl_bills', 'tbl_bills.invoices_id = tbl_memos.invoices_id');
            $this->db->join('tbl_invoices', 'tbl_invoices.invoices_id = tbl_memos.invoices_id');
            $this->db->where('tbl_memos.memo_id', $id);
            $memo_info = $this->db->get()->row();
            $data['memo_info'] = $memo_info;
            $data['memo_charges'] = $this->invoice_model->check_by_all(array('memo_id'=>$memo_info->memo_id), 'tbl_memo_charges');
            $this->db->select('*')->from('tbl_invoices');
            $this->db->where('`invoices_id` IN (SELECT `invoices_id` FROM `tbl_bills`)', NULL, FALSE);
            $this->db->where('client_id',$memo_info->client_id);
            $data['all_jobs'] = $this->db->get()->result();
        }
        /** ALL JOBS **/
        $this->invoice_model->_table_name = 'tbl_invoices';
        $this->invoice_model->_order_by = 'invoices_id';
        $data['all_invoice_info'] = $this->invoice_model->get();
        if ($action == 'memo_details') {
            $data['title'] = "Memo Details";
            $this->db->select('*');
            $this->db->from('tbl_memos');
            $this->db->join('tbl_bills', 'tbl_bills.invoices_id = tbl_memos.invoices_id');
            $this->db->join('tbl_invoices', 'tbl_invoices.invoices_id = tbl_memos.invoices_id');
            $this->db->where('tbl_memos.memo_id', $id);
            $data['memo_info'] = $this->db->get()->row();
            $subview = 'memo_details';
        }
        else{
            $subview = 'manage_memo';
        }

        $user_id = $this->session->userdata('user_id');

        $user_info = $this->invoice_model->check_by(array('user_id' => $user_id), 'tbl_users');
        $data['role'] = $user_info->role_id;
        /*Search Result*/
        $reference_no = $this->input->post('reference_no', TRUE);
        if (!empty($reference_no)) {
            $invoices_info = $this->invoice_model->check_by(array('reference_no'=>$reference_no), 'tbl_invoices');
            if(!empty($invoices_info)){
                $data['invoices_id'] = $invoices_info->invoices_id;
                $check_invoice = $this->db->where('invoices_id', $data['invoices_id'])->get('tbl_bills')->result();
                if(!empty($check_invoice)){
                    $data['search_value'] = TRUE;
                }
                else{
                    $data['search_value'] = '';
                    $data['bill_not_exist'] = TRUE;
                }
            }
            else{
                $data['search_value'] = '';
                $data['job_not_exist'] = TRUE;
                $data['reference_no'] = $reference_no;
            }
        }

        $this->db->select('*');
        $this->db->from('tbl_invoices');
        $this->db->join('tbl_client', 'tbl_invoices.client_id = tbl_client.client_id');
        $this->db->join('tbl_bills', 'tbl_bills.invoices_id = tbl_invoices.invoices_id');
        $this->db->group_by('tbl_invoices.client_id');
        $data['all_clients'] = $this->db->get()->result();

        $data['subview'] = 'admin/memos/'.$subview;
        $this->load->view('admin/_layout_main2', $data);
    }

    public function search_memo(){
        check_url();
        if($_POST){
            if(isset($_POST['client_id'])){
                $client_id = $_POST['client_id'];
                $data['client_id'] = $client_id;
                $this->db->select('*');
                $this->db->from('tbl_invoices');
                $this->db->join('tbl_bills', 'tbl_bills.invoices_id = tbl_invoices.invoices_id');
                $this->db->join('tbl_memos', 'tbl_invoices.invoices_id = tbl_memos.invoices_id');
                $this->db->where('tbl_invoices.client_id',$_POST['client_id']);
                $data['memo_info'] = $this->db->get()->result();
            }
            if(isset($_POST['reference_no'])) {
                $reference_no = $_POST['reference_no'];
                $data['reference_no'] = $reference_no;
                $invoice_info = $this->invoice_model->check_by(array('reference_no' => $reference_no), 'tbl_invoices');

                if (!empty($invoice_info)){
                    $this->db->select('*');
                    $this->db->from('tbl_security_deposit');
                    $this->db->join('tbl_bills', 'tbl_bills.invoices_id = tbl_security_deposit.invoices_id');
                    $this->db->join('tbl_memos', 'tbl_invoices.invoices_id = tbl_memos.invoices_id');
                    $this->db->where('tbl_invoices.invoices_id', $invoice_info->invoices_id);
                    $invoice_info = $this->db->get()->result();
                }
                $data['memo_info'] = $invoice_info;
            }
        }
        $this->invoice_model->_table_name = 'tbl_invoices';
        $this->invoice_model->_order_by = 'invoices_id';
        $data['all_invoices'] = $this->invoice_model->get();
        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'name';
        $data['all_client'] = $this->invoice_model->get();
        $this->invoice_model->_table_name = 'tbl_bills';
        $this->invoice_model->_order_by = 'bill_id';
        $data['all_bill'] = $this->invoice_model->get();

        $data['title'] = "Search Memos";
        $subview = 'search_memo';

        $data['subview'] = $this->load->view('admin/memos/' . $subview, $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_memo($id = NULL) {
        $job_info = $this->invoice_model->check_by(array('invoices_id'=>$_POST['invoices_id']), 'tbl_invoices');
        if (!empty($id)) {
            $id = decrypt($id);
            $data = $this->invoice_model->array_from_post(array('memo_type','client_id', 'invoices_id'));
            $check_type = $this->invoice_model->get_any_field('tbl_memos',array('memo_id'=>$id), 'memo_type');
            if($_POST['memo_type'] != $check_type){
                $data['memo_no'] = $this->invoice_model->generate_memo_no($_POST['memo_type']);
            }
            $this->invoice_model->_table_name = 'tbl_memos';
            $this->invoice_model->_primary_key = 'memo_id';
            $this->invoice_model->save($data, $id);

            $memo_id = $id;
            $mc_id = $this->input->post('mc_id', TRUE);
            $mc_title_update = $this->input->post('mc_title_update', TRUE);
            $mc_amount_update = $this->input->post('mc_amount_update', TRUE);
            $mc_title = $this->input->post('mc_title', TRUE);
            $mc_amount = $this->input->post('mc_amount', TRUE);

            for($i=0; $i<count($mc_id); $i++){
                $this->db->where('mc_id', $mc_id[$i]);
                $this->db->update("tbl_memo_charges", array('mc_title' => $mc_title_update[$i], 'mc_amount'=> $mc_amount_update[$i]));
            }

            if(!empty($mc_title)){
                for($x=0; $x<count($mc_title); $x++) {
                    $title_amount = array(
                        'memo_id' => $memo_id,
                        'mc_title' => $mc_title[$x],
                        'mc_amount' => $mc_amount[$x]
                    );
                    $this->invoice_model->_table_name = 'tbl_memo_charges';
                    $this->invoice_model->_primary_key = 'mc_id';
                    $this->invoice_model->save($title_amount);
                }
            }

            $action = lang('activity_memo_updated');
            $msg = lang('memo_updated');

        } else {
            $data = $this->invoice_model->array_from_post(array('memo_type', 'client_id', 'invoices_id'));
            $data['memo_no'] = $this->invoice_model->generate_memo_no($_POST['memo_type']);
            $this->invoice_model->_table_name = 'tbl_memos';
            $this->invoice_model->_primary_key = 'memo_id';
            $memo_id =$this->invoice_model->save($data);

            $mc_title = $this->input->post('mc_title', TRUE);
            $mc_amount = $this->input->post('mc_amount', TRUE);
             for($x=0; $x<count($mc_title); $x++) {
                $title_amount = array(
                    'memo_id' => $memo_id,
                    'mc_title' => $mc_title[$x],
                    'mc_amount' => $mc_amount[$x]
                );
                $this->invoice_model->_table_name = 'tbl_memo_charges';
                $this->invoice_model->_primary_key = 'mc_id';
                $this->invoice_model->save($title_amount);
             }
            $action = lang('activity_memo_created');
            $msg = lang('memo_created');
        }

        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => $_POST['memo_type']." ".'Memo',
            'module_field_id' => $memo_id,
            'activity' => $action,
            'icon' => 'fa-circle-o',
            'value1' => $job_info->reference_no
        );
        $this->invoice_model->_table_name = 'tbl_activities';
        $this->invoice_model->_primary_key = 'activities_id';
        $this->invoice_model->save($activity);

        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/memos/search_memo');
    }

    public function ajax_get_client_jobs($client_id)
    {
        $reference  = $this->invoice_model->get_client_jobs($client_id);
        echo json_encode($reference);
        die();
    }

    public function delete($memo_id, $id){
        check_url();
        $id = decrypt($id);
        $memo_id = decrypt($memo_id);
        $this->invoice_model->_table_name = 'tbl_memo_charges';
        $this->invoice_model->_primary_key = 'mc_id';
        $this->invoice_model->delete($id);
        $type = "success";
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'invoice',
            'module_field_id' => $memo_id,
            'activity' => 'Memo Charges has been removed',
            'icon' => 'fa-circle-o',
            'value1' => $id,
        );
        $this->invoice_model->_table_name = 'tbl_activities';
        $this->invoice_model->_primary_key = 'activities_id';
        $this->invoice_model->save($activity);
        $text = "Record has been removed";
        set_message($type, $text);
        redirect('admin/memos/manage_memo/create_memo/' . encode($memo_id));
    }

}
