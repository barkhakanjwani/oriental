<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        ini_set('max_input_vars', '3000');
        $this->load->model('settings_model');
        $this->load->model('invoice_model');
        $this->load->model('accounts_model');
        $this->auth_key = config_item('api_key');

        $this->load->helper('ckeditor');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "100%",
                'height' => "400px"
            )
        );
    }
    
    public function empty_tables(){
        
        /*** CHILD TABLES ***/
        /*$this->db->truncate('');*/
        /*** END CHILD TABLES ***/
        
        /*$this->db->empty_table('tbl_bills');
        $this->db->query("ALTER TABLE tbl_bills AUTO_INCREMENT = 1");*/
    
        /*$query = $this->db->query("SHOW TABLES");
        $name = $this->db->database;
        foreach ($query->result_array() as $row)
        {
            $table = $row['Tables_in_' . $name];
            $this->db->empty_table($table);
            //$this->db->query("TRUNCATE " . $table);
            $this->db->query("ALTER TABLE ".$table." AUTO_INCREMENT = 1");
        }
        $this->output->set_output("Database emptyed");*/
    }

    public function index() {
        check_url();
        $settings = $this->input->get('settings', TRUE) ? $this->input->get('settings', TRUE) : 'general';

        $data['title'] = lang('company_details');

        $data['load_setting'] = $settings;

        $data['page'] = lang('settings');

        $this->settings_model->_table_name = "tbl_countries";
        $this->settings_model->_order_by = "id";
        $data['countries'] = $this->settings_model->get();

        $data['subview'] = $this->load->view('admin/settings/settings', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_settings() {
        check_url();
        $input_data = $this->settings_model->array_from_post(array('company_name', 'company_legal_name',
            'contact_person', 'company_address','currency','company_city', 'company_zip_code',
            'company_country', 'company_phone', 'company_email', 'company_domain', 'company_vat'));

        foreach ($input_data as $key => $value) {
            $data = array('value' => $value);
            $this->db->where('config_key', $key)->update('tbl_config', $data);
            $exists = $this->db->where('config_key', $key)->get('tbl_config');
            if ($exists->num_rows() == 0) {
                $this->db->insert('tbl_config', array("config_key" => $key, "value" => $value));
            }
        }
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $this->session->userdata('user_id'),
            'activity' => lang('activity_save_general_settings'),
            'value1' => $input_data['company_name']
        );
        $this->settings_model->_table_name = 'tbl_activities';
        $this->settings_model->_primary_key = 'activities_id';
        $this->settings_model->save($activity);
        $type = "success";
        $message = lang('save_general_settings');
        set_message($type, $message);
        redirect('admin/settings');
    }

    public function theme() {
        check_url();
        $data['page'] = lang('settings');
        $data['load_setting'] = 'theme';
        $data['title'] = lang('theme_settings'); //Page title
        $data['subview'] = $this->load->view('admin/settings/settings', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }
    
    public function save_theme() {
        check_url();
        $input_data = $this->settings_model->array_from_post(array('website_name', 'logo_or_icon', 'sidebar_theme'));
        if (!empty($_FILES['company_logo']['name'])) {
            $val = $this->settings_model->uploadImage('company_logo');
            $val == TRUE || redirect('admin/settings/theme');
            $input_data['company_logo'] = $val['path'];
        }
        foreach ($input_data as $key => $value) {
            $data = array('value' => $value);
            $this->db->where('config_key', $key)->update('tbl_config', $data);
            $exists = $this->db->where('config_key', $key)->get('tbl_config');
            if ($exists->num_rows() == 0) {
                $this->db->insert('tbl_config', array("config_key" => $key, "value" => $value));
            }
        }
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $this->session->userdata('user_id'),
            'activity' => lang('activity_save_theme_settings'),
            'value1' => $input_data['website_name']
        );
        $this->settings_model->_table_name = 'tbl_activities';
        $this->settings_model->_primary_key = 'activities_id';
        $this->settings_model->save($activity);
        // messages for user
        $type = "success";
        $message = lang('save_theme_settings');
        set_message($type, $message);
        redirect('admin/settings/theme');
    }
    
    public function invoice() {
        check_url();
        $data['page'] = lang('settings');
        $data['load_setting'] = 'invoice';
        $data['title'] = lang('invoice_settings');
        $this->settings_model->_table_name = 'tbl_invoices';
        $this->settings_model->_order_by = 'invoices_id';
        $data['invoices'] = $this->settings_model->get();
        $this->settings_model->_table_name = 'stbjobtype';
        $this->settings_model->_order_by = 'Iid';
        $data['job_info'] = $this->settings_model->get();
        $data['subview'] = $this->load->view('admin/settings/settings', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
    
    public function save_invoice() {
        $input_data = $this->settings_model->array_from_post(array('job_number_auto'));
        
        foreach ($input_data as $key => $value) {
            if($key == 'job_number_auto' && $value == ''){
                $value = 'No';
            }
            
            $data = array('value' => $value);
            $this->db->where('config_key', $key)->update('tbl_config', $data);
            $exists = $this->db->where('config_key', $key)->get('tbl_config');
            if ($exists->num_rows() == 0) {
                $this->db->insert('tbl_config', array("config_key" => $key, "value" => $value));
            }
        }
        
        /*** JOB TYPES ***/
        
        $this->settings_model->_table_name = 'tbl_invoices';
        $this->settings_model->_order_by = 'invoices_id';
        $jobs = $this->settings_model->get();
        if(empty($jobs)) {
            $jobid = $this->input->post('job_id', TRUE);
            $jobtypeother = $this->input->post('job_type', TRUE);
            $prefixother = $this->input->post('job_prefix', TRUE);
            $separatorother = $this->input->post('job_separator', TRUE);
            $startother = $this->input->post('job_start', TRUE);
            $jobtypeother_update = $this->input->post('job_type_update', TRUE);
            $prefixother_update = $this->input->post('job_prefix_update', TRUE);
            $separatorother_update = $this->input->post('job_separator_update', TRUE);
            $startother_update = $this->input->post('job_start_update', TRUE);
            for ($i = 0; $i < count($jobid); $i++) {
                $this->db->where('Iid', decrypt($jobid[$i]));
                $data = array(
                    'Iid' => $jobid[$i],
                    'VCJobType' => $jobtypeother_update[$i],
                    'VCPrefix' => $prefixother_update[$i],
                    'VCSeparator' => $separatorother_update[$i],
                    'VCStartFrom' => $startother_update[$i],
                    'IUpdatedBy' => $this->session->userdata('user_id')
                );
                
                $this->db->where('Iid', $jobid[$i]);
                $this->db->update("stbjobtype", $data);
                
            }
            if (!empty($jobtypeother)) {
                for ($i = 0; $i < count($jobtypeother); $i++) {
                    $jobtype = array(
                        'VCJobType' => $jobtypeother[$i],
                        'VCPrefix' => $prefixother[$i],
                        'VCSeparator' => $separatorother[$i],
                        'VCStartFrom' => $startother[$i],
                        'ICreatedBy' => $this->session->userdata('user_id')
                    );
                    
                    $this->settings_model->_table_name = 'stbjobtype';
                    $this->settings_model->_primary_key = 'Iid';
                    $this->settings_model->save($jobtype);
                }
            }
        }
        /*** END JOB TYPES ***/
        
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $this->session->userdata('user_id'),
            'activity' => lang('activity_save_invoice_settings'),
            'value1' => $input_data['invoice_prefix']
        );
        $this->settings_model->_table_name = 'tbl_activities';
        $this->settings_model->_primary_key = 'activities_id';
        $this->settings_model->save($activity);
        // messages for user
        $type = "success";
        $message = lang('save_invoice_settings');
        set_message($type, $message);
        redirect('admin/settings/invoice');
    }
    
    public function delete_job_type($id) {
        check_url();
        $id = (!empty($id))?($id):NULL;
        $job_info = $this->settings_model->check_by(array('Iid' => $id), 'stbjobtype');
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $id,
            'activity' => lang('activity_delete_a_job_type'),
            'value1' => $job_info->VCPrefix,
        );
        $this->settings_model->_table_name = 'tbl_activities';
        $this->settings_model->_primary_key = 'activities_id';
        $this->settings_model->save($activity);
        
        $this->settings_model->_table_name = 'stbjobtype';
        $this->settings_model->_primary_key = 'Iid';
        $this->settings_model->delete($id);
        // messages for user
        $type = "success";
        $message = lang('job_type_deleted');
        set_message($type, $message);
        redirect('admin/settings/invoice');
    }
    
    /*public function save_invoice() {
        check_url();
        $input_data = $this->settings_model->array_from_post(array('invoice_prefix_import','invoice_start_import','invoice_prefix_export','invoice_start_export', 'invoice_number_separator_import', 'invoice_number_separator_export', 'default_terms'));
        
        $this->invoice_model->_table_name= 'tbl_invoices';
        $jobs = $this->invoice_model->get();
        if(!empty($jobs)){
            $input_data = $this->settings_model->array_from_post(array('default_terms'));
        }else{
            $input_data = $this->settings_model->array_from_post(array('job_number_auto', 'invoice_prefix_import','invoice_start_import','invoice_prefix_export','invoice_start_export', 'invoice_number_separator_import', 'invoice_number_separator_export', 'default_terms'));
        }
        
        if (!empty($_FILES['invoice_logo']['name'])) {
            $val = $this->settings_model->uploadImage('invoice_logo');
            $val == TRUE || redirect('admin/settings/invoice');
            $input_data['invoice_logo'] = $val['path'];
        }

        foreach ($input_data as $key => $value) {
            if($key == 'job_number_auto' && $value == ''){
                $value = 'No';
            }
            
            $data = array('value' => $value);
            $this->db->where('config_key', $key)->update('tbl_config', $data);
            $exists = $this->db->where('config_key', $key)->get('tbl_config');
            if ($exists->num_rows() == 0) {
                $this->db->insert('tbl_config', array("config_key" => $key, "value" => $value));
            }
        }
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $this->session->userdata('user_id'),
            'activity' => lang('activity_save_invoice_settings'),
            'value1' => $input_data['invoice_prefix']
        );
        $this->settings_model->_table_name = 'tbl_activities';
        $this->settings_model->_primary_key = 'activities_id';
        $this->settings_model->save($activity);

        $type = "success";
        $message = lang('save_invoice_settings');
        set_message($type, $message);
        redirect('admin/settings/invoice');
    }*/
    
    public function bill() {
        check_url();
        $data['page'] = lang('settings');
        $data['load_setting'] = 'bill';
        $data['title'] = lang('bill_settings');
        $this->settings_model->_table_name = 'tbl_invoices';
        $this->settings_model->_order_by = 'invoices_id';
        $data['invoices'] = $this->settings_model->get();
        $data['subview'] = $this->load->view('admin/settings/settings', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
    
    public function save_bill() {
        check_url();
        $input_data = $this->settings_model->array_from_post(array('default_terms'));
        
        if (!empty($_FILES['invoice_logo']['name'])) {
            $val = $this->settings_model->uploadImage('invoice_logo');
            $val == TRUE || redirect('admin/settings/bill');
            $input_data['invoice_logo'] = $val['path'];
        }
        
        foreach ($input_data as $key => $value) {
            if($key == 'job_number_auto' && $value == ''){
                $value = 'No';
            }
            
            $data = array('value' => $value);
            $this->db->where('config_key', $key)->update('tbl_config', $data);
            $exists = $this->db->where('config_key', $key)->get('tbl_config');
            if ($exists->num_rows() == 0) {
                $this->db->insert('tbl_config', array("config_key" => $key, "value" => $value));
            }
        }
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $this->session->userdata('user_id'),
            'activity' => lang('activity_save_bill_settings'),
            'value1' => $input_data['invoice_prefix']
        );
        $this->settings_model->_table_name = 'tbl_activities';
        $this->settings_model->_primary_key = 'activities_id';
        $this->settings_model->save($activity);
        // messages for user
        $type = "success";
        $message = lang('save_bill_settings');
        set_message($type, $message);
        redirect('admin/settings/bill');
    }

    public function department($action = NULL, $id = NULL) {
        check_url();
        $data['page'] = lang('settings');
        if ($action == 'edit_dept') {
            $data['active'] = 2;
            if (!empty($id)) {
                $id = decode($id);
                $data['dept_info'] = $this->settings_model->check_by(array('departments_id' => $id), 'tbl_departments');
            }
        } else {
            $data['active'] = 1;
        }
        $data['page'] = lang('settings');
        $data['sub_active'] = lang('department');
        if ($action == 'update_dept') {
            $dept_data['deptname'] = $this->input->post('deptname', TRUE);
            $id = (!empty($id))?decrypt($id):NULL;
            $msg = (!empty($id))?lang('department_updated'):lang('department_added');
            $act = (!empty($id))?lang('activity_updated_a_department'):lang('activity_added_a_department');
            $this->settings_model->_table_name = 'tbl_departments';
            $this->settings_model->_primary_key = 'departments_id';
            $id = $this->settings_model->save($dept_data, $id);

            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'settings',
                'module_field_id' => $id,
                'activity' => $act,
                'value1' => $dept_data['deptname']
            );
            $this->settings_model->_table_name = 'tbl_activities';
            $this->settings_model->_primary_key = 'activities_id';
            $this->settings_model->save($activity);

            // messages for user
            $type = "success";
            $message = $msg;
            set_message($type, $message);
            redirect('admin/settings/department');
        } else {
            $data['title'] = lang('department'); //Page title
            $data['load_setting'] = 'department';
        }

        $this->settings_model->_table_name = 'tbl_departments';
        $this->settings_model->_order_by = 'departments_id';
        $data['all_dept_info'] = $this->settings_model->get();

        $user_id = $this->session->userdata('user_id');
        $user_info = $this->settings_model->check_by(array('user_id' => $user_id), 'tbl_users');
        $data['role'] = $user_info->role_id;

        $data['subview'] = $this->load->view('admin/settings/settings', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function delete_dept($id) {
        check_url();
        $id = (!empty($id))?decrypt($id):NULL;
        $dept_info = $this->settings_model->check_by(array('departments_id' => $id), 'tbl_departments');
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $id,
            'activity' => lang('activity_delete_a_department'),
            'value1' => $dept_info->deptname,
        );
        $this->settings_model->_table_name = 'tbl_activities';
        $this->settings_model->_primary_key = 'activities_id';
        $this->settings_model->save($activity);

        $this->settings_model->_table_name = 'tbl_departments';
        $this->settings_model->_primary_key = 'departments_id';
        $this->settings_model->delete($id);
        
        $type = "success";
        $message = lang('department_deleted');
        set_message($type, $message);
        redirect('admin/settings/department');
    }

    public function update_profile() {
        $data['title'] = lang('update_profile');
        $data['subview'] = $this->load->view('admin/settings/update_profile', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function profile_updated() {
        $user_id = $this->session->userdata('user_id');
        $profile_data = $this->settings_model->array_from_post(array('fullname', 'phone'));

        if (!empty($_FILES['avatar']['name'])) {
            $val = $this->settings_model->uploadImage('avatar');
            $val == TRUE || redirect('admin/settings/update_profile');
            $profile_data['avatar'] = $val['path'];
        }

        $this->settings_model->_table_name = 'tbl_account_details';
        $this->settings_model->_primary_key = 'user_id';
        $this->settings_model->save($profile_data, $user_id);

        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $user_id,
            'activity' => lang('activity_update_profile'),
            'value1' => $profile_data['fullname'],
        );
        $this->settings_model->_table_name = 'tbl_activities';
        $this->settings_model->_primary_key = 'activities_id';
        $this->settings_model->save($activity);

        $client_id = $this->input->post('client_id', TRUE);
        if (!empty($client_id)) {
            $client_data = $this->settings_model->array_from_post(array('name', 'email', 'address'));
            $this->settings_model->_table_name = 'tbl_client';
            $this->settings_model->_primary_key = 'client_id';
            $this->settings_model->save($client_data, $client_id);
        }
        $type = "success";
        $message = lang('profile_updated');
        set_message($type, $message);
        redirect('admin/settings/update_profile'); //redirect page
    }

    public function set_password() {
        $user_id = $this->session->userdata('user_id');
        $password = $this->hash($this->input->post('old_password', TRUE));
        $check_old_pass = $this->admin_model->check_by(array('password' => $password), 'tbl_users');
        $user_info = $this->admin_model->check_by(array('user_id' => $user_id), 'tbl_users');
        if (!empty($check_old_pass)) {
            $data['password'] = $this->hash($this->input->post('new_password'));
            $this->settings_model->_table_name = 'tbl_users';
            $this->settings_model->_primary_key = 'user_id';
            $this->settings_model->save($data, $user_id);
            $type = "success";
            $message = lang('password_updated');
            $action = lang('activity_password_update');
        } else {
            $type = "error";
            $message = lang('password_error');
            $action = lang('activity_password_error');
        }
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $user_id,
            'activity' => $action,
            'value1' => $user_info->username,
        );
        $this->settings_model->_table_name = 'tbl_activities';
        $this->settings_model->_primary_key = 'activities_id';
        $this->settings_model->save($activity);
        set_message($type, $message);
        redirect('admin/settings/update_profile'); //redirect page
    }

    public function change_email() {
        $user_id = $this->session->userdata('user_id');
        $password = $this->hash($this->input->post('password', TRUE));
        $check_old_pass = $this->settings_model->check_by(array('password' => $password), 'tbl_users');
        $user_info = $this->admin_model->check_by(array('user_id' => $user_id), 'tbl_users');
        if (!empty($check_old_pass)) {
            $new_email = $this->input->post('email', TRUE);
            if ($check_old_pass->email == $new_email) {
                $type = 'error';
                $message = lang('current_email');
                $action = lang('trying_update_email');
            } elseif ($this->is_email_available($new_email)) {
                $data = array(
                    'new_email' => $new_email,
                    'new_email_key' => md5(rand() . microtime()),
                );

                $this->settings_model->_table_name = 'tbl_users';
                $this->settings_model->_primary_key = 'user_id';
                $this->settings_model->save($data, $user_id);
                $data['user_id'] = $user_id;
                $this->send_email_change_email($new_email, $data);
                $type = "success";
                $message = lang('succesffuly_change_email');
                $action = lang('activity_updated_email');
            } else {
                $type = "error";
                $message = lang('duplicate_email');
                $action = lang('trying_update_email');
            }
        } else {
            $type = "error";
            $message = lang('password_error');
            $action = lang('trying_update_email');
        }
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $user_id,
            'activity' => $action,
            'value1' => $user_info->email,
            'value2' => $new_email,
        );
        $this->settings_model->_table_name = 'tbl_activities';
        $this->settings_model->_primary_key = 'activities_id';
        $this->settings_model->save($activity);
        set_message($type, $message);
        redirect('admin/settings/update_profile'); //redirect page
    }

    function send_email_change_email($email, $data) {
        $email_template = $this->settings_model->check_by(array('email_group' => 'change_email'), 'tbl_email_templates');
        $message = $email_template->template_body;
        $subject = $email_template->subject;

        $email_key = str_replace("{NEW_EMAIL_KEY_URL}", base_url() . 'login/reset_email/' . $data['user_id'] . '/' . $data['new_email_key'], $message);
        $new_email = str_replace("{NEW_EMAIL}", $data['new_email'], $email_key);
        $site_url = str_replace("{SITE_URL}", base_url(), $new_email);
        $message = str_replace("{SITE_NAME}", config_item('company_name'), $site_url);

        $params['recipient'] = $email;

        $params['subject'] = '[ ' . config_item('company_name') . ' ]' . ' ' . $subject;
        $params['message'] = $message;

        $params['resourceed_file'] = '';
        $this->settings_model->send_email($params);
    }

    function is_email_available($email) {
        $this->db->select('1', FALSE);
        $this->db->where('LOWER(email)=', strtolower($email));
        $this->db->or_where('LOWER(new_email)=', strtolower($email));
        $query = $this->db->get('tbl_users');
        return $query->num_rows() == 0;
    }

    public function hash($string) {
        return hash('sha512', $string . config_item('encryption_key'));
    }

    public function change_username() {
        $user_id = $this->session->userdata('user_id');
        $password = $this->hash($this->input->post('password', TRUE));
        $check_old_pass = $this->admin_model->check_by(array('password' => $password), 'tbl_users');
        $user_info = $this->admin_model->check_by(array('user_id' => $user_id), 'tbl_users');
        if (!empty($check_old_pass)) {
            $data['username'] = $this->input->post('username');
            $this->settings_model->_table_name = 'tbl_users';
            $this->settings_model->_primary_key = 'user_id';
            $this->settings_model->save($data, $user_id);
            $type = "success";
            $message = lang('username_updated');
            $action = lang('activity_username_updated');
        } else {
            $type = "error";
            $message = lang('password_error');
            $action = lang('username_changed_error');
        }
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $user_id,
            'activity' => $action,
            'value1' => $user_info->username,
            'value2' => $this->input->post('username'),
        );
        $this->settings_model->_table_name = 'tbl_activities';
        $this->settings_model->_primary_key = 'activities_id';
        $this->settings_model->save($activity);
        set_message($type, $message);
        redirect('admin/settings/update_profile'); //redirect page
    }

    public function database_backup() {
        check_url();
        $this->load->dbutil();
        $prefs = array(
            'format' => 'zip', // gzip, zip, txt
            'filename' => 'CLEARING_FORWARDING_Backup' . date('Y-m-d') . '.zip',
            'add_drop' => TRUE, // Whether to add DROP TABLE statements to backup file
            'add_insert' => TRUE, // Whether to add INSERT data to backup file
            'newline' => "\n"               // Newline character used in backup file
        );
        $backup = & $this->dbutil->backup($prefs);
        $this->load->helper('download');
        force_download('CLEARING_FORWARDING_Backup' . date('Y-m-d') . '.zip', $backup);

        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $this->session->userdata('user_id'),
            'activity' => 'activity_database_backup',
            'value1' => $prefs['filename']
        );
        $this->settings_model->_table_name = 'tbl_activities';
        $this->settings_model->_primary_key = 'activities_id';
        $this->settings_model->save($activity);
    }

    public function activities() {
        $data['title'] = lang('activities');
        $data['activities_info'] = $this->db->where(array('user' => $this->session->userdata('user_id')))->order_by('activity_date', 'DESC')->get('tbl_activities')->result();

        $data['subview'] = $this->load->view('admin/settings/activities', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function clear_activities() {
        check_url();
        $this->db->where(array('user' => $this->session->userdata('user_id')))->delete('tbl_activities');
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $this->session->userdata('user_id'),
            'activity' => 'activity_deleted',
            'value1' => lang('all_activity') . date('Y-m-d')
        );
        $this->settings_model->_table_name = 'tbl_activities';
        $this->settings_model->_primary_key = 'activities_id';
        $this->settings_model->save($activity);

        $type = "success";
        $message = lang('activities_deleted');
        set_message($type, $message);
        redirect('admin/dashboard');
    }

    /*** JOB ACTIVITY STATUS ***/
    public function job_status($action = NULL, $id = NULL) {
        check_url();
        $data['page'] = lang('settings');
        if ($action == 'edit_job_status') {
            $data['active'] = 2;
            if (!empty($id)) {
                $id = decode($id);
                $data['job_status_info'] = $this->settings_model->check_by(array('job_status_id' => $id), 'tbl_job_status');
            }
        } else {
            $data['active'] = 1;
        }
        $data['page'] = lang('settings');
        $data['sub_active'] = lang('job_status');
        if ($action == 'update_job_status') {
            $job_status_data['job_status_title'] = $this->input->post('job_status_title', TRUE);
            $id = (!empty($id))?decrypt($id):NULL;
            $msg = (!empty($id))?lang('job_status_updated'):lang('job_status_added');
            $act = (!empty($id))?lang('activity_updated_a_job_status'):lang('activity_added_a_job_status');
            if(!empty($id)){
                $job_status_data['updated_by'] = $this->session->userdata('user_id');
            }
            else{
                $job_status_data['created_by'] = $this->session->userdata('user_id');
            }
            $this->settings_model->_table_name = 'tbl_job_status';
            $this->settings_model->_primary_key = 'job_status_id';
            $id = $this->settings_model->save($job_status_data, $id);

            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'settings',
                'module_field_id' => $id,
                'activity' => $act,
                'value1' => $job_status_data['job_status_title']
            );
            $this->settings_model->_table_name = 'tbl_activities';
            $this->settings_model->_primary_key = 'activities_id';
            $this->settings_model->save($activity);

            /*messages for user*/
            $type = "success";
            $message = $msg;
            set_message($type, $message);
            redirect('admin/settings/job_status');
        } else {
            $data['title'] = lang('job_status');
            $data['load_setting'] = 'job_status';
        }

        $this->settings_model->_table_name = 'tbl_job_status';
        $this->settings_model->_order_by = 'job_status_id';
        $data['all_job_status_info'] = $this->settings_model->get();

        $data['subview'] = $this->load->view('admin/settings/settings', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
    /*** END JOB ACTIVITY STATUS ***/
    /*** GENERAL EXPENSES HEADS ***/
    public function general_expenses($action = NULL, $id = NULL) {
        check_url();
        $data['page'] = lang('settings');
        if ($action == 'edit_expense') {
            $data['active'] = 2;
            if (!empty($id)) {
                $id = decode($id);
                $data['expense_info'] = $this->settings_model->check_by(array('A_ID' => $id), 'accounts');
            }
        } else {
            $data['active'] = 1;
        }
        $data['sub_active'] = lang('general_expenses');
        if ($action == 'update_expense') {
            $check_ac_no = $this->accounts_model->max_account_no_by_head($_POST['head_id']);
            if (!empty($check_ac_no)) {
                $a = explode('.', $check_ac_no->A_NO);
                $arr=array();
                for ($k=0;$k<count($a);$k++){

                    if ($k==(count($a)-1)){
                        $arr[]=$a[$k]+1;
                    }else{
                        $arr[]=$a[$k];
                    }
                }
                $account_no=implode('.',$arr);
            } else {
                $check_ac_no = $this->accounts_model->select_edit_head($_POST['head_id']);
                $account_no = $check_ac_no->H_NO . "." . (1);
            }
            $postData=array(
                'H_ID'          => $this->input->post('head_id'),
                'SUB_HEAD_ID'   => 10,
                'A_NAME'        => $this->input->post('h_name'),
                'A_DESC'        => '',
                'A_NO'          => $account_no,
                'A_STATUS'      => 1,
                'CREATED_AT'    => date('Y-m-d H:i:s')
            );
            $id = (!empty($id))?decrypt($id):NULL;
            $msg = (!empty($id))?lang('general_expense_updated'):lang('general_expense_added');
            $act = (!empty($id))?lang('activity_updated_a_general_expense'):lang('activity_added_a_general_expense');
            if(!empty($id)){
                $postData['UPDATED_BY'] = $this->session->userdata('user_id');
            }
            else{
                $postData['CREATED_BY'] = $this->session->userdata('user_id');
            }
            $this->settings_model->_table_name = 'accounts';
            $this->settings_model->_primary_key = 'A_ID';
            $id = $this->settings_model->save($postData, $id);
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'settings',
                'module_field_id' => $id,
                'activity' => $act,
                'icon' => 'fa-usd',
                'value1' => $_POST['h_name'],
            );
            $this->settings_model->_table_name = 'tbl_activities';
            $this->settings_model->_primary_key = 'activities_id';
            $this->settings_model->save($activity);
            $type = "success";
            $message = $msg;
            set_message($type, $message);
            redirect('admin/settings/general_expenses');
        }else{
            $data['title'] = lang('general_expenses');
            $data['load_setting'] = 'general_expenses';
        }

        $ap_head = $this->settings_model->check_by(array('HEAD_TYPE' => 'Expense', 'NAME' => 'Operating Expense'), 'sub_heads');
        $ap_sub_head = $this->settings_model->check_by(array('SUB_HEAD_ID' => $ap_head->SUB_HEAD_ID, 'H_NAME'=>'General Expenses'), 'accounts_head');
        $data['head_id'] = $ap_sub_head->H_ID;
        $data['expenses'] = $this->settings_model->check_by_all(array('SUB_HEAD_ID' => $ap_head->SUB_HEAD_ID, 'H_ID'=>$ap_sub_head->H_ID), 'accounts');

        $data['subview'] = $this->load->view('admin/settings/settings', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
    /*** END GENERAL EXPENSES HEADS ***/
    /*** JOB EXPENSES HEADS ***/
    public function job_expenses($action = NULL, $id = NULL) {
        $data['page'] = lang('settings');
        if ($action == 'edit_expense') {
            $data['active'] = 2;
            if (!empty($id)) {
                $id = decode($id);
                $data['expense_info'] = $this->settings_model->check_by(array('job_expense_id' => $id), 'tbl_job_expenses');
            }
        } else {
            $data['active'] = 1;
        }
        $data['sub_active'] = lang('job_expenses');
        if ($action == 'update_expense') {
            $id = (!empty($id))?decrypt($id):NULL;
            $msg = (!empty($id))?lang('job_expense_updated'):lang('job_expense_added');
            $act = (!empty($id))?lang('activity_updated_a_job_expense'):lang('activity_added_a_job_expense');
            $job_expense_data['job_expense_title'] = $this->input->post('job_expense_title', TRUE);
            $job_expense_data['job_expense_type'] = $this->input->post('job_expense_type', TRUE);
            if(!empty($id)){
                $job_expense_data['updated_by'] = $this->session->userdata('user_id');
            }
            else{
                $job_expense_data['created_by'] = $this->session->userdata('user_id');
            }
            $this->settings_model->_table_name = 'tbl_job_expenses';
            $this->settings_model->_primary_key = 'job_expense_id';
            $id = $this->settings_model->save($job_expense_data, $id);
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'settings',
                'module_field_id' => $id,
                'activity' => $act,
                'icon' => 'fa-usd',
                'value1' => $_POST['job_expense_title'],
            );
            $this->settings_model->_table_name = 'tbl_activities';
            $this->settings_model->_primary_key = 'activities_id';
            $this->settings_model->save($activity);
            $type = "success";
            $message = $msg;
            set_message($type, $message);
            redirect('admin/settings/job_expenses');
        }else{
            $data['title'] = lang('job_expenses');
            $data['load_setting'] = 'job_expenses';
        }

        $this->settings_model->_table_name = "tbl_job_expenses";
        $this->settings_model->_order_by = "job_expense_id";
        $data['expenses'] = $this->settings_model->get();

        $data['subview'] = $this->load->view('admin/settings/settings', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
    /*** END JOB EXPENSES HEADS ***/
    /*** REQUISITION EXPENSES HEADS ***/
    public function requisition_expenses($action = NULL, $id = NULL) {
        check_url();
        $data['page'] = lang('settings');
        if ($action == 'edit_expense') {
            $data['active'] = 2;
            if (!empty($id)) {
                $id = decode($id);
                $data['requisition_expense_info'] = $this->settings_model->check_by(array('requisition_expense_id' => $id), 'tbl_requisition_expenses');
            }
        } else {
            $data['active'] = 1;
        }
        $data['page'] = lang('settings');
        $data['sub_active'] = lang('requisition_status');
        if ($action == 'update_expense') {
            $expense_data['requisition_expense_title'] = $this->input->post('requisition_expense_title', TRUE);
            $id = (!empty($id))?decrypt($id):NULL;
            $msg = (!empty($id))?lang('requisition_expense_updated'):lang('requisition_expense_added');
            $act = (!empty($id))?lang('activity_updated_a_requisition_expense'):lang('activity_added_a_requisition_expense');
            if(!empty($id)){
                $expense_data['updated_by'] = $this->session->userdata('user_id');
            }
            else{
                $expense_data['created_by'] = $this->session->userdata('user_id');
            }
            $this->settings_model->_table_name = 'tbl_requisition_expenses';
            $this->settings_model->_primary_key = 'requisition_expense_id';
            $id = $this->settings_model->save($expense_data, $id);

            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'settings',
                'module_field_id' => $id,
                'activity' => $act,
                'value1' => $expense_data['requisition_expense_title']
            );
            $this->settings_model->_table_name = 'tbl_activities';
            $this->settings_model->_primary_key = 'activities_id';
            $this->settings_model->save($activity);

            /*messages for user*/
            $type = "success";
            $message = $msg;
            set_message($type, $message);
            redirect('admin/settings/requisition_expenses');
        } else {
            $data['title'] = lang('requisition_expense');
            $data['load_setting'] = 'requisition_expenses';
        }

        $this->settings_model->_table_name = 'tbl_requisition_expenses';
        $this->settings_model->_order_by = 'requisition_expense_id';
        $data['all_requisition_expenses'] = $this->settings_model->get();

        $data['subview'] = $this->load->view('admin/settings/settings', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
    /*** END REQUISITION EXPENSES HEADS ***/
    /*** DOCUMENT TYPES ***/
    public function document_type($action = NULL, $id = NULL) {
        check_url();
        $data['page'] = lang('settings');
        if ($action == 'edit_document_type') {
            $data['active'] = 2;
            if (!empty($id)) {
                $id = decode($id);
                $data['document_type_info'] = $this->settings_model->check_by(array('document_type_id' => $id), 'tbl_document_types');
            }
        } else {
            $data['active'] = 1;
        }
        $data['page'] = lang('settings');
        $data['sub_active'] = lang('document_type');
        if ($action == 'update_document_type') {
            $document_type_data['document_title'] = $this->input->post('document_title', TRUE);
            $id = (!empty($id))?decrypt($id):NULL;
            $msg = (!empty($id))?lang('document_type_updated'):lang('document_type_added');
            $act = (!empty($id))?lang('activity_updated_a_document_type'):lang('activity_added_a_document_type');
            if(!empty($id)){
                $document_type_data['updated_by'] = $this->session->userdata('user_id');
            }
            else{
                $document_type_data['created_by'] = $this->session->userdata('user_id');
            }
            $this->settings_model->_table_name = 'tbl_document_types';
            $this->settings_model->_primary_key = 'document_type_id';
            $id = $this->settings_model->save($document_type_data, $id);

            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'settings',
                'module_field_id' => $id,
                'activity' => $act,
                'value1' => $document_type_data['document_title']
            );
            $this->settings_model->_table_name = 'tbl_activities';
            $this->settings_model->_primary_key = 'activities_id';
            $this->settings_model->save($activity);

            /*messages for user*/
            $type = "success";
            $message = $msg;
            set_message($type, $message);
            redirect('admin/settings/document_type');
        } else {
            $data['title'] = lang('document_type');
            $data['load_setting'] = 'document_type';
        }

        $this->settings_model->_table_name = 'tbl_document_types';
        $this->settings_model->_order_by = 'document_type_id';
        $data['all_document_types'] = $this->settings_model->get();

        $user_id = $this->session->userdata('user_id');
        $user_info = $this->settings_model->check_by(array('user_id' => $user_id), 'tbl_users');
        $data['role'] = $user_info->role_id;

        $data['subview'] = $this->load->view('admin/settings/settings', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
    /*** END DOCUMENT TYPES ***/
    /*** COMMODITIES TYPES ***/
    public function commodity_type($action = NULL, $id = NULL) {
        check_url();
        $data['page'] = lang('settings');
        if ($action == 'edit_commodity_type') {
            $data['active'] = 2;
            if (!empty($id)) {
                $id = decode($id);
                $data['commodity_type_info'] = $this->settings_model->check_by(array('commodity_type_id' => $id), 'tbl_commodity_types');
            }
        } else {
            $data['active'] = 1;
        }
        $data['page'] = lang('settings');
        $data['sub_active'] = lang('commodity_type');
        if ($action == 'update_commodity_type') {
            $commodity_type_data['commodity_type'] = $this->input->post('commodity_type', TRUE);
            $id = (!empty($id))?decrypt($id):NULL;
            $msg = (!empty($id))?lang('commodity_type_updated'):lang('commodity_type_added');
            $act = (!empty($id))?lang('activity_updated_a_commodity_type'):lang('activity_added_a_commodity_type');
            if(!empty($id)){
                $commodity_type_data['updated_by'] = $this->session->userdata('user_id');
            }
            else{
                $commodity_type_data['created_by'] = $this->session->userdata('user_id');
            }
            $this->settings_model->_table_name = 'tbl_commodity_types';
            $this->settings_model->_primary_key = 'commodity_type_id';
            $id = $this->settings_model->save($commodity_type_data, $id);

            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'settings',
                'module_field_id' => $id,
                'activity' => $act,
                'value1' => $commodity_type_data['commodity_type']
            );
            $this->settings_model->_table_name = 'tbl_activities';
            $this->settings_model->_primary_key = 'activities_id';
            $this->settings_model->save($activity);

            /*messages for user*/
            $type = "success";
            $message = $msg;
            set_message($type, $message);
            redirect('admin/settings/commodity_type');
        } else {
            $data['title'] = lang('commodity_type');
            $data['load_setting'] = 'commodity_type';
        }

        $this->settings_model->_table_name = 'tbl_commodity_types';
        $this->settings_model->_order_by = 'commodity_type_id';
        $data['all_commodity_types'] = $this->settings_model->get();

        $data['subview'] = $this->load->view('admin/settings/settings', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
    /*** END COMMODITIES TYPES ***/

    /*** START SHIPPING LINE ***/

    public function shipping_line($action = NULL, $id = NULL) {
        check_url();
        $data['page'] = lang('settings');
        if ($action == 'edit_shipping') {
            $data['active'] = 2;
            if (!empty($id)) {
                $id = decode($id);
                $data['shipping_info'] = $this->settings_model->check_by(array('shipping_id' => $id), 'tbl_shipping_line');
            }
        } else {
            $data['active'] = 1;
        }
        $data['page'] = lang('settings');
        $data['sub_active'] = lang('shipping_line');
        if ($action == 'update_shipping') {
            $shipping_data['shipping_name'] = $this->input->post('shipping_name', TRUE);
            $id = (!empty($id))?decrypt($id):NULL;
            $msg = (!empty($id))?lang('shipping_line_updated'):lang('shipping_line_added');
            $act = (!empty($id))?lang('activity_updated_a_shipping_line'):lang('activity_added_a_shipping_line');
            if(!empty($id)){
                $shipping_data['updated_by'] = $this->session->userdata('user_id');
            }
            else{
                $shipping_data['created_by'] = $this->session->userdata('user_id');
            }
            $this->settings_model->_table_name = 'tbl_shipping_line';
            $this->settings_model->_primary_key = 'shipping_id';
            $id = $this->settings_model->save($shipping_data, $id);

            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'settings',
                'module_field_id' => $id,
                'activity' => $act,
                'value1' => $shipping_data['shipping_name']
            );
            $this->settings_model->_table_name = 'tbl_activities';
            $this->settings_model->_primary_key = 'activities_id';
            $this->settings_model->save($activity);
            
            $type = "success";
            $message = $msg;
            set_message($type, $message);
            redirect('admin/settings/shipping_line');
        } else {
            $data['title'] = lang('shipping_line'); //Page title
            $data['load_setting'] = 'shipping_line';
        }

        $this->settings_model->_table_name = 'tbl_shipping_line';
        $this->settings_model->_order_by = 'shipping_id';
        $data['all_shipping_info'] = $this->settings_model->get();

        $user_id = $this->session->userdata('user_id');
        $user_info = $this->settings_model->check_by(array('user_id' => $user_id), 'tbl_users');
        $data['role'] = $user_info->role_id;

        $data['subview'] = $this->load->view('admin/settings/settings', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function delete_shipping($id) {
        check_url();
        $id = (!empty($id))?decrypt($id):NULL;
        $shipping_info = $this->settings_model->check_by(array('shipping_id' => $id), 'tbl_shipping_line');
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'settings',
            'module_field_id' => $id,
            'activity' => lang('activity_delete_a_shipping_line'),
            'value1' => $shipping_info->shipping_name,
        );
        $this->settings_model->_table_name = 'tbl_activities';
        $this->settings_model->_primary_key = 'activities_id';
        $this->settings_model->save($activity);

        $this->settings_model->_table_name = 'tbl_shipping_line';
        $this->settings_model->_primary_key = 'shipping_id';
        $this->settings_model->delete($id);
        // messages for user
        $type = "success";
        $message = lang('shipping_deleted');
        set_message($type, $message);
        redirect('admin/settings/shipping_line');
    }

    /*** END SHIPPING LINE ***/
    
    /*** START YARD ***/
    public function yard($action = NULL, $id = NULL) {
        check_url();
        $data['page'] = lang('settings');
        if ($action == 'edit_yard') {
            $data['active'] = 2;
            if (!empty($id)) {
                $id = decode($id);
                $data['yard_info'] = $this->settings_model->check_by(array('yard_id' => $id), 'tbl_yards');
            }
        } else {
            $data['active'] = 1;
        }
        $data['page'] = lang('settings');
        $data['sub_active'] = lang('yard');
        if ($action == 'update_yard') {
            $yard_data['yard'] = $this->input->post('yard', TRUE);
            $id = (!empty($id))?decrypt($id):NULL;
            $msg = (!empty($id))?lang('yard_updated'):lang('yard_added');
            $act = (!empty($id))?lang('activity_updated_a_yard'):lang('activity_added_a_yard');
            if(!empty($id)){
                $yard_data['updated_by'] = $this->session->userdata('user_id');
            }
            else{
                $yard_data['created_by'] = $this->session->userdata('user_id');
            }
            $this->settings_model->_table_name = 'tbl_yards';
            $this->settings_model->_primary_key = 'yard_id';
            $id = $this->settings_model->save($yard_data, $id);
            
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'settings',
                'module_field_id' => $id,
                'activity' => $act,
                'value1' => $yard_data['yard']
            );
            $this->settings_model->_table_name = 'tbl_activities';
            $this->settings_model->_primary_key = 'activities_id';
            $this->settings_model->save($activity);
            
            $type = "success";
            $message = $msg;
            set_message($type, $message);
            redirect('admin/settings/yard');
        } else {
            $data['title'] = lang('yard');
            $data['load_setting'] = 'yard';
        }
        
        $this->settings_model->_table_name = 'tbl_yards';
        $this->settings_model->_order_by = 'yard_id';
        $data['all_yard_info'] = $this->settings_model->get();
        
        $data['subview'] = $this->load->view('admin/settings/settings', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
    /*** END YARD ***/
    /*** START SHED ***/
    public function shed($action = NULL, $id = NULL) {
        check_url();
        $data['page'] = lang('settings');
        if ($action == 'edit_shed') {
            $data['active'] = 2;
            if (!empty($id)) {
                $id = decode($id);
                $data['shed_info'] = $this->settings_model->check_by(array('shed_id' => $id), 'tbl_sheds');
            }
        } else {
            $data['active'] = 1;
        }
        $data['page'] = lang('settings');
        $data['sub_active'] = lang('shed');
        if ($action == 'update_shed') {
            $shed_data['shed'] = $this->input->post('shed', TRUE);
            $id = (!empty($id))?decrypt($id):NULL;
            $msg = (!empty($id))?lang('shed_updated'):lang('shed_added');
            $act = (!empty($id))?lang('activity_updated_a_shed'):lang('activity_added_a_shed');
            if(!empty($id)){
                $shed_data['updated_by'] = $this->session->userdata('user_id');
            }
            else{
                $shed_data['created_by'] = $this->session->userdata('user_id');
            }
            $this->settings_model->_table_name = 'tbl_sheds';
            $this->settings_model->_primary_key = 'shed_id';
            $id = $this->settings_model->save($shed_data, $id);
            
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'settings',
                'module_field_id' => $id,
                'activity' => $act,
                'value1' => $shed_data['shed']
            );
            $this->settings_model->_table_name = 'tbl_activities';
            $this->settings_model->_primary_key = 'activities_id';
            $this->settings_model->save($activity);
            
            $type = "success";
            $message = $msg;
            set_message($type, $message);
            redirect('admin/settings/shed');
        } else {
            $data['title'] = lang('shed');
            $data['load_setting'] = 'shed';
        }
        
        $this->settings_model->_table_name = 'tbl_sheds';
        $this->settings_model->_order_by = 'shed_id';
        $data['all_shed_info'] = $this->settings_model->get();
        
        $data['subview'] = $this->load->view('admin/settings/settings', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
    /*** END SHED ***/
    /*** START INCOTERM ***/
    public function incoterm($action = NULL, $id = NULL) {
        check_url();
        $data['page'] = lang('settings');
        if ($action == 'edit_incoterm') {
            $data['active'] = 2;
            if (!empty($id)) {
                $id = decode($id);
                $data['incoterm_info'] = $this->settings_model->check_by(array('incoterm_id' => $id), 'tbl_incoterms');
            }
        } else {
            $data['active'] = 1;
        }
        $data['page'] = lang('settings');
        $data['sub_active'] = lang('incoterm');
        if ($action == 'update_incoterm') {
            $incoterm_data['incoterm'] = $this->input->post('incoterm', TRUE);
            $id = (!empty($id))?decrypt($id):NULL;
            $msg = (!empty($id))?lang('incoterm_updated'):lang('incoterm_added');
            $act = (!empty($id))?lang('activity_updated_a_incoterm'):lang('activity_added_a_incoterm');
            if(!empty($id)){
                $incoterm_data['updated_by'] = $this->session->userdata('user_id');
            }
            else{
                $incoterm_data['created_by'] = $this->session->userdata('user_id');
            }
            $this->settings_model->_table_name = 'tbl_incoterms';
            $this->settings_model->_primary_key = 'incoterm_id';
            $id = $this->settings_model->save($incoterm_data, $id);
            
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'settings',
                'module_field_id' => $id,
                'activity' => $act,
                'value1' => $incoterm_data['incoterm']
            );
            $this->settings_model->_table_name = 'tbl_activities';
            $this->settings_model->_primary_key = 'activities_id';
            $this->settings_model->save($activity);
            
            $type = "success";
            $message = $msg;
            set_message($type, $message);
            redirect('admin/settings/incoterm');
        } else {
            $data['title'] = lang('incoterm');
            $data['load_setting'] = 'incoterm';
        }
        
        $this->settings_model->_table_name = 'tbl_incoterms';
        $this->settings_model->_order_by = 'incoterm_id';
        $data['all_incoterm_info'] = $this->settings_model->get();
        
        $data['subview'] = $this->load->view('admin/settings/settings', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
    /*** END INCOTERM ***/
    /*** START WEIGHT UNIT ***/
    public function weight_unit($action = NULL, $id = NULL) {
        check_url();
        $data['page'] = lang('settings');
        if ($action == 'edit_weight_unit') {
            $data['active'] = 2;
            if (!empty($id)) {
                $id = decode($id);
                $data['weight_unit_info'] = $this->settings_model->check_by(array('weight_unit_id' => $id), 'tbl_weight_units');
            }
        } else {
            $data['active'] = 1;
        }
        $data['page'] = lang('settings');
        $data['sub_active'] = lang('weight_unit');
        if ($action == 'update_weight_unit') {
            $weight_unit_data['weight_unit'] = $this->input->post('weight_unit', TRUE);
            $id = (!empty($id))?decrypt($id):NULL;
            $msg = (!empty($id))?lang('weight_unit_updated'):lang('weight_unit_added');
            $act = (!empty($id))?lang('activity_updated_a_weight_unit'):lang('activity_added_a_weight_unit');
            if(!empty($id)){
                $weight_unit_data['updated_by'] = $this->session->userdata('user_id');
            }
            else{
                $weight_unit_data['created_by'] = $this->session->userdata('user_id');
            }
            $this->settings_model->_table_name = 'tbl_weight_units';
            $this->settings_model->_primary_key = 'weight_unit_id';
            $id = $this->settings_model->save($weight_unit_data, $id);
            
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'settings',
                'module_field_id' => $id,
                'activity' => $act,
                'value1' => $weight_unit_data['weight_unit']
            );
            $this->settings_model->_table_name = 'tbl_activities';
            $this->settings_model->_primary_key = 'activities_id';
            $this->settings_model->save($activity);
            
            $type = "success";
            $message = $msg;
            set_message($type, $message);
            redirect('admin/settings/weight_unit');
        } else {
            $data['title'] = lang('weight_unit');
            $data['load_setting'] = 'weight_unit';
        }
        
        $this->settings_model->_table_name = 'tbl_weight_units';
        $this->settings_model->_order_by = 'weight_unit_id';
        $data['all_weight_unit_info'] = $this->settings_model->get();
        
        $data['subview'] = $this->load->view('admin/settings/settings', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
    /*** END WEIGHT UNIT ***/
    /*** START GD TYPE ***/
    public function gd_type($action = NULL, $id = NULL) {
        check_url();
        $data['page'] = lang('settings');
        if ($action == 'edit_gd_type') {
            $data['active'] = 2;
            if (!empty($id)) {
                $id = decode($id);
                $data['gd_type_info'] = $this->settings_model->check_by(array('gd_type_id' => $id), 'tbl_gd_types');
            }
        } else {
            $data['active'] = 1;
        }
        $data['page'] = lang('settings');
        $data['sub_active'] = lang('gd_type');
        if ($action == 'update_gd_type') {
            $gd_type_data['gd_type'] = $this->input->post('gd_type', TRUE);
            $id = (!empty($id))?decrypt($id):NULL;
            $msg = (!empty($id))?lang('gd_type_updated'):lang('gd_type_added');
            $act = (!empty($id))?lang('activity_updated_a_gd_type'):lang('activity_added_a_gd_type');
            if(!empty($id)){
                $gd_type_data['updated_by'] = $this->session->userdata('user_id');
            }
            else{
                $gd_type_data['created_by'] = $this->session->userdata('user_id');
            }
            $this->settings_model->_table_name = 'tbl_gd_types';
            $this->settings_model->_primary_key = 'gd_type_id';
            $id = $this->settings_model->save($gd_type_data, $id);
            
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'settings',
                'module_field_id' => $id,
                'activity' => $act,
                'value1' => $gd_type_data['gd_type']
            );
            $this->settings_model->_table_name = 'tbl_activities';
            $this->settings_model->_primary_key = 'activities_id';
            $this->settings_model->save($activity);
            
            $type = "success";
            $message = $msg;
            set_message($type, $message);
            redirect('admin/settings/gd_type');
        } else {
            $data['title'] = lang('gd_type');
            $data['load_setting'] = 'gd_type';
        }
        
        $this->settings_model->_table_name = 'tbl_gd_types';
        $this->settings_model->_order_by = 'gd_type_id';
        $data['all_gd_type_info'] = $this->settings_model->get();
        
        $data['subview'] = $this->load->view('admin/settings/settings', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
    /*** END GD TYPE ***/
}
