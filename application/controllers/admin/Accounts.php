<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends Admin_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('accounts_model');
        $this->load->model('invoice_model');
    }
	
    #---------------Start Bank Section-------------------#
    public function banks()
    {
        check_url();
        $data['banks'] = $this->accounts_model->read_banks();
		$data['subview'] = $this->load->view('admin/accounts/banks', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function branches()
    {
        check_url();
        $data['branches'] = $this->accounts_model->read_branches();
        $data['banks']  = $this->accounts_model->read_bank_where('B_STATUS',1);
        $this->invoice_model->_table_name = 'tbl_cities';
        $this->invoice_model->_order_by = 'id';
        $data['cities'] = $this->invoice_model->get();
        /*$data['banks']  = $this->accounts_model->cash_andBank_accounts();*/
        $data['subview'] = $this->load->view('admin/accounts/branches', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function add_banks()
    {
        if($this->input->post()){
            $img='';
            $config['upload_path']          = './upload/';
            $config['allowed_types']        = 'gif|jpg|png';
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('ba_image'))
            {
                $error = array('error' => $this->upload->display_errors());
            }else
            {
                $data = $this->upload->data();
                $img =$data['file_name'];
            }

            $account_no=null;
            $result=$this->accounts_model->account_no_by_selfHead(2);
            if($result->A_NO!=""){
                $pos=strlen($result->A_NO)-1;
                $account_no = substr_replace($result->A_NO,substr($result->A_NO,$pos)+1,$pos);
            }else{
                $result=$this->accounts_model->read_account_by_id(2);
                $account_no=$result->A_NO.".".(1);
            }

            $accountData=array(
                'H_ID'              => 4,
                'SUB_HEAD_ID'       => 1,
                'A_NAME'            => $this->input->post('ba_name'),
                'A_DESC'            => "",
                'A_OPENINGTYPE'     => "",
                'A_OPENINGBALANCE'  => "",
                'A_SELFHEAD_ID'     => 2,
                'A_NO'              => $account_no,
                'A_STATUS'          => $this->input->post('ba_status'),
                'CREATED_BY'        => $this->session->userdata('user_id')
            );
            $this->accounts_model->create_account($accountData);
            $insert_id = $this->db->insert_id();
            $postData=array(
                'B_NAME'    => $this->input->post('ba_name'),
                'B_LOGO'    => $img,
                'B_STATUS'  => $this->input->post('ba_status'),
                'B_DATETIME'=> date('Y-m-d H:i:s'),
                'CREATED_BY'=> $this->session->userdata('user_id'),
                'A_ID'      => $insert_id
            );
            /*$result = $this->accounts_model->create_bank($postData);*/
            $this->invoice_model->_table_name = 'banks';
            $this->invoice_model->_primary_key = 'B_ID';
            $bank_id = $this->invoice_model->save($postData);
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'bank & cash',
                'module_field_id' => $bank_id,
                'activity' => lang('activity_added_new_bank'),
                'icon' => 'fa-bank',
                'value1' => $_POST['ba_name'],
            );
            $this->invoice_model->_table_name = 'tbl_activities';
            $this->invoice_model->_primary_key = 'activities_id';
            $this->invoice_model->save($activity);
            $type = "success";
            $message = lang('bank_added');
            set_message($type, $message);
            redirect('admin/accounts/banks');
        }
    }

    public function ajax_edit_bank($bank_id)
    {
        $bank_id = decrypt($bank_id);
        $output=$this->accounts_model->select_edit_bank($bank_id);
        echo json_encode($output);
		die();
    }

    public function edit_bank()
    {
        if($this->input->post()){
            $img=$this->input->post('hdn_logo_name');
            $config['upload_path']          = './upload/';
            $config['allowed_types']        = 'gif|jpg|png';
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('B_LOGO'))
            {
                $error = array('error' => $this->upload->display_errors());
            }else
            {
                $data = $this->upload->data();
                $img =$data['file_name'];
            }
            $postData=array(
                'B_ID'      => $this->input->post('B_ID'),
                'B_NAME'    => $this->input->post('B_NAME'),
                'B_LOGO'    => $img,
                'B_STATUS'  => $this->input->post('B_STATUS'),
                'UPDATED_BY' => $this->session->userdata('user_id')
            );

            $accountData=array(
                'A_ID'              => $this->input->post('A_ID'),
                'H_ID'              => 4,
                'SUB_HEAD_ID'       => 1,
                'A_NAME'            => $this->input->post('B_NAME'),
                'A_DESC'            => "",
                'A_OPENINGTYPE'     => "",
                'A_OPENINGBALANCE'  => "",
                'A_SELFHEAD_ID'     => 2,
                'A_STATUS'          => $this->input->post('B_STATUS'),
                'UPDATED_BY'        => $this->session->userdata('user_id')
            );
            $this->accounts_model->update_account($accountData);
           if($this->accounts_model->update_bank($postData)) {
               $activity = array(
                   'user' => $this->session->userdata('user_id'),
                   'module' => 'bank & cash',
                   'module_field_id' => $_POST['B_ID'],
                   'activity' => lang('activity_updated_new_bank'),
                   'icon' => 'fa-bank',
                   'value1' => $_POST['B_NAME'],
               );
               $this->invoice_model->_table_name = 'tbl_activities';
               $this->invoice_model->_primary_key = 'activities_id';
               $this->invoice_model->save($activity);
               $type = "success";
               $message = lang('bank_updated');
               set_message($type, $message);
               redirect('admin/accounts/banks');
           }
           else{
               $type = "error";
               $message = 'OOPS! Something went wrong';
               set_message($type, $message);
               redirect('admin/accounts/banks');
           }
        }
    }

    public function delete_bank($bank_id)
    {
        $account=$this->accounts_model->select_edit_bank($bank_id);
        $this->accounts_model->delete_account($account->A_ID);
        $status=$this->accounts_model->delete_bank($bank_id);
        echo json_encode($status);
		die();
    }
    
    public function add_branch()
    {
        if($this->input->post()){
            $account_no=null;
            $bank = $this->invoice_model->check_by(array('B_ID' => $this->input->post('bank_name')), 'banks');
            $result=$this->accounts_model->account_no_by_selfHead($bank->A_ID);
            if($result->A_NO!=""){
                $pos=strlen($result->A_NO)-1;
                $account_no = substr_replace($result->A_NO,substr($result->A_NO,$pos)+1,$pos);
            }else{
                $result=$this->accounts_model->read_account_by_id($bank->A_ID);
                $account_no=$result->A_NO.".".(1);
            }
            
            $accountData=array(
                'H_ID'          => 4,
                'SUB_HEAD_ID'   => 1,
                'A_NAME'        => $bank->B_NAME." - ".$this->input->post('branch_name'),
                'A_DESC'        => "",
                'A_OPENINGTYPE' => $this->input->post('opening_type'),
                'A_OPENINGBALANCE'=>$this->input->post('opening_balance'),
                'A_SELFHEAD_ID' => $bank->A_ID,
                'A_NO'          => $account_no,
                'A_STATUS'      => $this->input->post('branch_status'),
                'CREATED_BY'    => $this->session->userdata('user_id')
            );
            $this->accounts_model->create_account($accountData);
            $insert_id=$this->db->insert_id();
            
            $postData=array(
                'B_ID'        => $this->input->post('bank_name'),
                'BR_NAME'     => $this->input->post('branch_name'),
                'BR_CODE'     => $this->input->post('branch_code'),
                'BR_ADDRESS'  => $this->input->post('branch_address'),
                'BR_CITY'     => $this->input->post('branch_city'),
                'BR_OPENINGTYPE'=> $this->input->post('opening_type'),
                'BR_OPENINGBALANCE'=>$this->input->post('opening_balance'),
                'BR_STATUS'   => $this->input->post('branch_status'),
                'CREATED_BY'  => $this->session->userdata('user_id'),
                'A_ID'        => $insert_id
            );
            $this->invoice_model->_table_name = 'branches';
            $this->invoice_model->_primary_key = 'BR_ID';
            $branch_id = $this->invoice_model->save($postData);
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'bank & cash',
                'module_field_id' => $branch_id,
                'activity' => lang('activity_added_new_branch'),
                'icon' => 'fa-usd',
                'value1' => $_POST['branch_name'],
            );
            $this->invoice_model->_table_name = 'tbl_activities';
            $this->invoice_model->_primary_key = 'activities_id';
            $this->invoice_model->save($activity);
            $type = "success";
            $message = lang('branch_added');
            set_message($type, $message);
            redirect('admin/accounts/branches');
        }
    }

    public function ajax_edit_branch($branch_id)
    {
        $branch_id = decrypt($branch_id);
        $output=$this->accounts_model->select_edit_branch($branch_id);
        echo json_encode($output);
		die();
    }

    public function ajax_get_branch_aid($branch_id)
    {
        $output=$this->accounts_model->select_edit_branch($branch_id);
        echo json_encode($output);
        die();
    }
    
    public function edit_branch()
    {
        if($this->input->post()){
            $account_no=null;
            $bank = $this->invoice_model->check_by(array('B_ID' => $this->input->post('bank_name')), 'banks');
            $result=$this->accounts_model->account_no_by_selfHead($bank->A_ID,$this->input->post('account_id'));
            if($result->A_NO!=""){
                $pos=strlen($result->A_NO)-1;
                $account_no = substr_replace($result->A_NO,substr($result->A_NO,$pos)+1,$pos);
            }else{
                $result=$this->accounts_model->read_account_by_id($bank->A_ID);
                $account_no=$result->A_NO.".".(1);
            }
            
            $postData=array(
                'BR_ID'       => $this->input->post('branch_id'),
                'B_ID'        => $this->input->post('bank_name'),
                'BR_NAME'     => $this->input->post('branch_name'),
                'BR_CODE'     => $this->input->post('branch_code'),
                'BR_ADDRESS'  => $this->input->post('branch_address'),
                'BR_CITY'     => $this->input->post('branch_city'),
                'BR_OPENINGTYPE'=> $this->input->post('opening_type'),
                'BR_OPENINGBALANCE'=>$this->input->post('opening_balance'),
                'BR_STATUS'   => $this->input->post('branch_status'),
                'UPDATED_BY'  => $this->session->userdata('user_id')
            );
            
            $accountData=array(
                'A_ID'          => $this->input->post('acc_id'),
                'H_ID'          => 4,
                'SUB_HEAD_ID'   => 1,
                'A_NAME'        => $bank->B_NAME." - ".$this->input->post('branch_name'),
                'A_DESC'        => "",
                'A_OPENINGTYPE' => $this->input->post('opening_type'),
                'A_OPENINGBALANCE'=>$this->input->post('opening_balance'),
                'A_SELFHEAD_ID' => $bank->A_ID,
                'A_STATUS'      => $this->input->post('branch_status'),
                'A_NO'          => $account_no,
                'UPDATED_BY'    => $this->session->userdata('user_id')
            );
            $this->accounts_model->update_account($accountData);
            if($this->accounts_model->update_branch($postData)){
                $activity = array(
                    'user' => $this->session->userdata('user_id'),
                    'module' => 'bank & cash',
                    'module_field_id' => $_POST['branch_id'],
                    'activity' => lang('activity_updated_new_branch'),
                    'icon' => 'fa-bank',
                    'value1' => $_POST['branch_name'],
                );
                $this->invoice_model->_table_name = 'tbl_activities';
                $this->invoice_model->_primary_key = 'activities_id';
                $this->invoice_model->save($activity);
                $type = "success";
                $message = lang('branch_updated');
                set_message($type, $message);
                redirect('admin/accounts/branches');
            }else{
                $type = "error";
                $message = 'OOPS! Something went wrong';
                set_message($type, $message);
                redirect('admin/accounts/branches');
            }
        }
    }

    public function delete_branch($branch_id)
    {
        $output=$this->accounts_model->select_edit_branch($branch_id);
        $status1=$this->accounts_model->delete_account($output->A_ID);
        $status=$this->accounts_model->delete_branch($branch_id);
        echo json_encode($status);
		die();
    }
	
	public function ajax_select_branches($bank_id)
    {
        $branches  = $this->accounts_model->read_branch_where($bank_id);
        echo json_encode($branches);
		die();
    }
    #---------------End Bank Section-------------------#
    #---------------Start Accounts Head Section-------------------#
    public function accountHead()
    {
		$data['accountsHead']=$this->accounts_model->read_heads();
		$data['subview'] = $this->load->view('admin/accounts/account_head', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function select_sub_head($type){
        $output=$this->accounts_model->select_sub_heads($type);
        echo json_encode($output);
		die();
    }

    public function add_head()
    {
        if($this->input->post()){
            $head_no=null;
            $result=$this->accounts_model->sub_heads_no($this->input->post('SUB_HEAD_ID'));
            if($result->H_NO!=""){
                $head_no =$result->H_NO+0.1;
            }else{
                $rs = $this->accounts_model->sub_heads_by_id($this->input->post('SUB_HEAD_ID'));
                $head_no =$rs->SUB_NO+0.1;
            }
            $postData=array(
                'H_NAME'     => $this->input->post('h_name'),
                'H_TYPE'     => $this->input->post('h_type'),
                'SUB_HEAD_ID'=> $this->input->post('SUB_HEAD_ID'),
                'H_NO'       => $head_no,
                'H_STATUS'   => $this->input->post('h_status'),
                'CREATED_BY'   => $this->session->userdata('user_id')
            );
            $this->invoice_model->_table_name = 'accounts_head';
            $this->invoice_model->_primary_key = 'H_ID';
            $id = $this->invoice_model->save($postData);
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'chart of account',
                'module_field_id' => $id,
                'activity' => lang('activity_added_account_head'),
                'icon' => '',
                'value1' => $_POST['h_name'],
            );
            $this->invoice_model->_table_name = 'tbl_activities';
            $this->invoice_model->_primary_key = 'activities_id';
            $this->invoice_model->save($activity);
            $type = "success";
            $message = lang('account_head_added');
            set_message($type, $message);
            redirect('admin/accounts/accountHead');
        }
    }

    public function ajax_edit_head($head_id)
    {
        $output=$this->accounts_model->select_edit_head($head_id);
        $subHeads=$this->accounts_model->select_sub_heads($output->H_TYPE);
        $rs=array(
          'edit_data' => $output,
           'dropdown'=> $subHeads
        );
        echo json_encode($rs);
		die();
    }

    public function edit_head()
    {
        if($this->input->post()){
            $postData=array(
                'H_ID'     => $this->input->post('H_ID'),
                'H_NAME'     => $this->input->post('H_NAME'),
                'H_TYPE'     => $this->input->post('H_TYPE'),
                'SUB_HEAD_ID'=> $this->input->post('SUB_HEAD_ID'),
                'H_STATUS'   => $this->input->post('H_STATUS'),
                'UPDATED_BY'   => $this->session->userdata('user_id')
            );
            if($this->accounts_model->update_head($postData)){
                $activity = array(
                    'user' => $this->session->userdata('user_id'),
                    'module' => 'chart of account',
                    'module_field_id' => $_POST['H_ID'],
                    'activity' => lang('activity_updated_account_head'),
                    'icon' => 'fa-usd',
                    'value1' => $_POST['H_NAME'],
                );
                $this->invoice_model->_table_name = 'tbl_activities';
                $this->invoice_model->_primary_key = 'activities_id';
                $this->invoice_model->save($activity);
                $type = "success";
                $message = lang('account_head_updated');
                set_message($type, $message);
                redirect('admin/accounts/accountHead');
            }else{
                $type = "error";
                $message = 'OOPS! Something went wrong';
                set_message($type, $message);
                redirect('admin/accounts/accountHead');
            }
        }
    }

    public function delete_head($head_id)
    {
        $status=$this->accounts_model->delete_head($head_id);
        echo json_encode($status);
		die();
    }

    #---------------End Accounts Head Section-------------------#

    #---------------Start Accounts Section-------------------#
    public function index()
    {
        $data['accountsHead']= $this->accounts_model->read_heads();
		$data['subview'] = $this->load->view('admin/accounts/account', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    /*** SUB ACCOUNTS FOR SEPERATE PAGE ***/
    public function manage_sub_accounts($id = NULL){
        if(!empty($id)){
            $data['title'] = 'Edit Sub Account';
        }
        else{
            $data['title'] = 'Create Sub Account';
        }
        $data['accountsHead']= $this->accounts_model->read_heads();
        $data['subview'] = $this->load->view('admin/accounts/manage_sub_accounts', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_account($id = NULL){
        if(!empty($id)){
            $account_no=null;
            if($this->input->post('a_subHead')!=0)
            {
                $result=$this->accounts_model->account_no_by_selfHead($this->input->post('a_subHead'));
                if($result->A_NO!=""){
                    $pos=strlen($result->A_NO)-1;
                    $account_no = substr_replace($result->A_NO,substr($result->A_NO,$pos)+1,$pos);
                }else{
                    $result=$this->accounts_model->read_account_by_id($this->input->post('a_subHead'));
                    $account_no=$result->A_NO.".".(1);
                }
            }elseif ($this->input->post('a_head')!=''){
                $rs = $this->accounts_model->account_no_by_head($this->input->post('a_head'));
                if($rs->A_NO!=""){
                    $pos=strlen($rs->A_NO)-1;
                    $account_no = substr_replace($rs->A_NO,substr($rs->A_NO,$pos)+1,$pos);
                }else{
                    $rs = $this->accounts_model->select_edit_head($this->input->post('a_head'));
                    $account_no=$rs->H_NO.".".(1);
                }
            }
            $postData=array(
                'A_ID'          => $this->input->post('a_id'),
                'H_ID'          => $this->input->post('a_head'),
                'SUB_HEAD_ID'   => $this->input->post('sub_type'),
                'A_NAME'        => $this->input->post('a_name'),
                'A_DESC'        => $this->input->post('a_desc'),
                'A_OPENINGTYPE' => $this->input->post('a_openingType'),
                'A_OPENINGBALANCE'=>$this->input->post('a_openingBalance'),
                'A_SELFHEAD_ID' => $this->input->post('a_subHead'),
                'A_STATUS'      => $this->input->post('a_status'),
                'A_NO'          => $account_no,
            );
            if($this->accounts_model->update_account($postData)){
                $action = 'Edit Sub account';
                $activities = array(
                    'user' => $this->session->userdata('user_id'),
                    'module' => 'Sub Account',
                    'module_field_id' => '',
                    'activity' => $action,
                    'icon' => 'fa-user',
                    'value1' => $this->input->post('a_name')
                );
                $this->invoice_model->_table_name = 'tbl_activities';
                $this->invoice_model->_primary_key = "activities_id";
                $this->invoice_model->save($activities);
                /*messages for user*/
                $type = "success";
                $message = 'Sub Account successfully Updated !';
                set_message($type, $message);
                redirect('admin/accounts/manage_sub_accounts/'.$id);
            }
        }
        else{
            $account_no=null;
            if($this->input->post('a_subHead')!=0)
            {
                $result=$this->accounts_model->account_no_by_selfHead($this->input->post('a_subHead'));
                if($result->A_NO!=""){
                    $pos=strlen($result->A_NO)-1;
                    $account_no = substr_replace($result->A_NO,substr($result->A_NO,$pos)+1,$pos);
                }else{
                    $result=$this->accounts_model->read_account_by_id($this->input->post('a_subHead'));
                    $account_no=$result->A_NO.".".(1);
                }
            }elseif ($this->input->post('a_head')!=''){
                $rs = $this->accounts_model->account_no_by_head($this->input->post('a_head'));
                if($rs->A_NO!=""){
                    $pos=strlen($rs->A_NO)-1;
                    $account_no = substr_replace($rs->A_NO,substr($rs->A_NO,$pos)+1,$pos);
                }else{
                    $rs = $this->accounts_model->select_edit_head($this->input->post('a_head'));
                    $account_no=$rs->H_NO.".".(1);
                }
            }
            $postData=array(
                'H_ID'          => $this->input->post('a_head'),
                'SUB_HEAD_ID'   => $this->input->post('sub_type'),
                'A_NAME'        => $this->input->post('a_name'),
                'A_DESC'        => $this->input->post('a_desc'),
                'A_OPENINGTYPE' => $this->input->post('a_openingType'),
                'A_OPENINGBALANCE'=>$this->input->post('a_openingBalance'),
                'A_SELFHEAD_ID' => $this->input->post('a_subHead'),
                'A_NO'          => $account_no,
                'A_STATUS'      => $this->input->post('a_status'),
                'CREATED_AT'    => date('Y-m-d H:i:s')
            );
            if($this->accounts_model->create_account($postData))
            {
                $this->invoice_model->_table_name = 'tbl_client';
                $this->invoice_model->_order_by = 'client_id';
                $all_clients = $this->invoice_model->get();
                if ($this->input->post('client_account') == 'on') {
                    if (!empty($all_clients)) {
                        foreach ($all_clients as $client) {
                            $client_account = array(
                                'H_ID' => $this->input->post('a_head'),
                                'SUB_HEAD_ID' => $this->input->post('sub_type'),
                                'CLIENT_ID' => $client->client_id,
                                'A_NAME' => $this->input->post('a_name'),
                                'A_DESC' => $this->input->post('a_desc'),
                                'A_OPENINGTYPE' => $this->input->post('a_openingType'),
                                'A_OPENINGBALANCE' => $this->input->post('a_openingBalance'),
                                'A_SELFHEAD_ID' => $this->input->post('a_subHead'),
                                'A_NO' => $account_no,
                                'A_STATUS' => $this->input->post('a_status'),
                                'CREATED_AT' => date('Y-m-d H:i:s')
                            );
                            $this->invoice_model->_table_name = 'accounts';
                            $this->invoice_model->_primary_key = 'A_ID';
                            $this->invoice_model->save($client_account);
                        }
                    }
                }
                $action = 'Add Sub account';
                $activities = array(
                    'user' => $this->session->userdata('user_id'),
                    'module' => 'Sub Account',
                    'module_field_id' => '',
                    'activity' => $action,
                    'icon' => 'fa-user',
                    'value1' => $this->input->post('a_name')
                );
                $this->invoice_model->_table_name = 'tbl_activities';
                $this->invoice_model->_primary_key = "activities_id";
                $this->invoice_model->save($activities);
                /*messages for user*/
                $type = "success";
                $message = 'New Sub Account successfully Created !';
                set_message($type, $message);
                redirect('admin/accounts/manage_sub_accounts');
            }
        }
    }
    /*** END SUB ACCOUNTS FOR SEPERATE PAGE ***/

    public function add_account()
    {
        if($this->input->post()){
            $account_no=null;
            if($this->input->post('a_subHead')!=0)
            {
                $result=$this->accounts_model->account_no_by_selfHead($this->input->post('a_subHead'));
                if($result->A_NO!=""){
                    $pos=strlen($result->A_NO)-1;
                    $account_no = substr_replace($result->A_NO,substr($result->A_NO,$pos)+1,$pos);
                }else{
                    $result=$this->accounts_model->read_account_by_id($this->input->post('a_subHead'));
                    $account_no=$result->A_NO.".".(1);
                }
            }elseif ($this->input->post('a_head')!=''){
                $rs = $this->accounts_model->account_no_by_heads($this->input->post('a_head'));
                if($rs->A_NO!=""){
                    $a = explode('.', $rs->A_NO);
                    $arr=array();
                    for ($k=0;$k<count($a);$k++){

                        if ($k==(count($a)-1)){
                            $arr[]=$a[$k]+1;
                        }else{
                            $arr[]=$a[$k];
                        }
                    }
                    $account_no=implode('.',$arr);
                }else{
                    $rs = $this->accounts_model->select_edit_head($this->input->post('a_head'));
                    $account_no=$rs->H_NO.".".(1);
                }
            }
            $postData=array(
                'H_ID'          => $this->input->post('a_head'),
                'SUB_HEAD_ID'   => $this->input->post('sub_type'),
                'A_NAME'        => $this->input->post('a_name'),
                'A_DESC'        => $this->input->post('a_desc'),
                'A_OPENINGTYPE' => $this->input->post('a_openingType'),
                'A_OPENINGBALANCE'=>$this->input->post('a_openingBalance'),
                'A_SELFHEAD_ID' => $this->input->post('a_subHead'),
                'A_NO'          => $account_no,
                'A_STATUS'      => $this->input->post('a_status'),
                'CREATED_BY'    => $this->session->userdata('user_id')
            );
            $this->invoice_model->_table_name = 'accounts';
            $this->invoice_model->_primary_key = 'A_ID';
            $id = $this->invoice_model->save($postData);
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'chart of account',
                'module_field_id' => $id,
                'activity' => lang('activity_added_sub_account'),
                'icon' => 'fa-usd',
                'value1' => $_POST['a_name'],
            );
            $this->invoice_model->_table_name = 'tbl_activities';
            $this->invoice_model->_primary_key = 'activities_id';
            $this->invoice_model->save($activity);
            $type = "success";
            $message = lang('account_sub_account_added');
            set_message($type, $message);
            redirect('admin/accounts');
        }
    }

    public function edit_account()
    {
        
        if($this->input->post()){
            $account_no=null;
            if($this->input->post('a_subHead')!=0)
            {
                $result=$this->accounts_model->account_no_by_selfHead($this->input->post('a_subHead'));
                if($result->A_NO!=""){
                    $pos=strlen($result->A_NO)-1;
                    $account_no = substr_replace($result->A_NO,substr($result->A_NO,$pos)+1,$pos);
                }else{
                    $result=$this->accounts_model->read_account_by_id($this->input->post('a_subHead'));
                    $account_no=$result->A_NO.".".(1);
                }
            }elseif ($this->input->post('a_head')!=''){
                $rs = $this->accounts_model->account_no_by_head($this->input->post('a_head'));
                if($rs->A_NO!=""){
                    $pos=strlen($rs->A_NO)-1;
                    $account_no = substr_replace($rs->A_NO,substr($rs->A_NO,$pos)+1,$pos);
                }else{
                    $rs = $this->accounts_model->select_edit_head($this->input->post('a_head'));
                    $account_no=$rs->H_NO.".".(1);
                }
            }
            $postData=array(
                'A_ID'          => $this->input->post('a_id'),
                'H_ID'          => $this->input->post('a_head'),
                'SUB_HEAD_ID'   => $this->input->post('sub_type'),
                'A_NAME'        => $this->input->post('a_name'),
                'A_DESC'        => $this->input->post('a_desc'),
                'A_OPENINGTYPE' => $this->input->post('a_openingType'),
                'A_OPENINGBALANCE'=>$this->input->post('a_openingBalance'),
                'A_SELFHEAD_ID' => $this->input->post('a_subHead'),
                'A_STATUS'      => $this->input->post('a_status'),
                'A_NO'          => $account_no,
                'UPDATED_BY'    => $this->session->userdata('user_id')
            );
            if($this->accounts_model->update_account($postData)){
                $activity = array(
                    'user' => $this->session->userdata('user_id'),
                    'module' => 'chart of account',
                    'module_field_id' => $this->input->post('a_id'),
                    'activity' => lang('activity_updated_sub_account'),
                    'icon' => 'fa-usd',
                    'value1' => $_POST['a_name'],
                );
                $this->invoice_model->_table_name = 'tbl_activities';
                $this->invoice_model->_primary_key = 'activities_id';
                $this->invoice_model->save($activity);
                $type = "success";
                $message = lang('account_sub_account_updated');
                set_message($type, $message);
                redirect('admin/accounts');
            }else{
                $type = "error";
                $message = 'OOPS! Something went wrong';
                set_message($type, $message);
                redirect('admin/accounts');
            }
        }
    }

    public function ajax_edit_account($account_id)
    {
        $account=$this->accounts_model->select_edit_account($account_id);
        $subType=$this->accounts_model->select_sub_heads($account->H_TYPE);
        $head=$this->accounts_model->read_head_where($account->SUB_HEAD_ID);
        $subHead=$this->accounts_model->ajax_select_accounts(array('H_ID'=>$account->H_ID,'A_STATUS'=>1));
        $output=array(
          'account'  => $account,
            'subType'=> $subType,
            'head'      => $head,
            'subHead'   => $subHead
        );
        echo json_encode($output);
		die();
    }

    public function delete_account($account_id)
    {
        $status=$this->accounts_model->delete_account($account_id);
        echo json_encode($status);
		die();
    }

    public function ajax_select_head($sub_id)
    {
        $heads  = $this->accounts_model->read_head_where($sub_id);
        echo json_encode($heads);
		die();
    }

    public function ajax_select_accounts($head_id)
    {
        $accounts  = $this->accounts_model->ajax_select_accounts(array('H_ID'=>$head_id,'A_STATUS'=>1));
        echo json_encode($accounts);
		die();
    }

    #---------------End Accounts Section-------------------#
    
    #---------------Start General Journal Section-------------------#

    public function general_journal()
    {
        if($this->input->post('date')!=""){
            $date=$this->input->post('date');
            $data['date'] = $date;
            $data['entries'] = $this->accounts_model->read_transaction_by_date($date);
        }else{
            $data['entries'] = $this->accounts_model->read_transaction();
        }
        $data['accounts']= $this->accounts_model->ajax_select_accounts(array('A_STATUS'=>1));
        $data['subview'] = $this->load->view('admin/accounts/general_journal', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function general_journal_lists($date = NULL)
    {
        $search_array = array();
        $rows = array();
        $search_value = $_POST['search']['value'];
        if ($search_value) {
            $search_array = array(
                'pv.PV_VOUCHER_NO' => $search_value
            );
        }
        $length = 10;
        $start = 1;
        if ($_POST['length'] != -1) {
            $length = $_POST['length'];
            $start = $_POST['start'];
        }
        $read_transaction_list = $this->accounts_model->read_transaction_searching_list($search_array, $length, $start, $date);
        $read_transaction_list_rows = $this->accounts_model->read_transaction_list_rows($search_array, $date);
        $i = $_POST['start'] + 1;
        foreach ($read_transaction_list as $read_transaction) {
            $debit_transactions=read_debit_transactions($read_transaction->T_ID);
            $credit_transactions=read_credit_transactions($read_transaction->T_ID);
            $desc='';$credit='';
            foreach ($debit_transactions as $d){
                $desc .= '<span style="float:left;">'. $d->A_NAME .'</span><br>';
            }
            foreach ($credit_transactions as $c){
                $desc .= '<span style="float:right;">'. $c->A_NAME .'</span><br>';
            }
            foreach ($debit_transactions as $d){
                if($d->PARTICULARS!=""){
                    $desc .= '<span style="float:left;">'. $d->PARTICULARS .'</span><br>';
                }
            }
            foreach ($credit_transactions as $c){
                if($c->PARTICULARS!=""){
                    $desc .= '<span style="float:left;">'. $c->PARTICULARS .'</span><br>';
                }
            }
            foreach ($debit_transactions as $d){
                $debit = '<span style="float:left;">'. number_format($d->TM_AMOUNT,2) .'</span><br>';
            }
            foreach ($debit_transactions as $d){
                $credit .= '<br>';
                }
            foreach ($credit_transactions as $c){
                $credit .= '<span style="float:right;">'. number_format($c->TM_AMOUNT,2) .'</span><br>';
            }
            $btn =  '<button class="btn btn-xs btn-primary" onclick="getEditEntries('. $read_transaction->T_ID .')" ><i class="fa fa-edit"></i></button>';
            $rows[] = array(
                date('d-m-Y',strtotime($read_transaction->T_DATE)),
                $desc,
                $debit,
                $credit,
                $btn
            );
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $read_transaction_list_rows,
            "recordsFiltered" => $read_transaction_list_rows,
            "data" => $rows
        );
        echo json_encode($output);
    }
    
    public function add_entries()
    {
        if($_POST){
            $this->accounts_model->create_transaction(array(
                'T_DATE'=>$this->input->post('date'),
                'T_TYPE'=>"General Journal",
                'CREATED_BY'=>$this->session->userdata('user_id')
            ));
            $tr_id=$this->db->insert_id();
            $debit_account=$this->input->post('debit_account');
            $debit_particular=$this->input->post('debit_particular');
            $debit_amount=$this->input->post('debit_amount');
            $credit_account=$this->input->post('credit_account');
            $credit_particular=$this->input->post('credit_particular');
            $credit_amount=$this->input->post('credit_amount');
            
            $tr_data=array();
            for ($i=0;$i<count($debit_amount);$i++) {
                if(isset($debit_amount[$i])){
                    $tr_data[]=array(
                        'T_ID'          => $tr_id,
                        'A_ID'          => $debit_account[$i],
                        'PARTICULARS'   => $debit_particular[$i],
                        'TM_AMOUNT'     => $debit_amount[$i],
                        'TM_TYPE'       => "Debit",
                        'IS_ACTIVE'     => 1
                    );
                }
            }
            for ($t=0;$t<count($credit_amount);$t++){
                if(isset($credit_amount[$t])){
                    $tr_data[]=array(
                        'T_ID'          => $tr_id,
                        'A_ID'          => $credit_account[$t],
                        'PARTICULARS'   => $credit_particular[$t],
                        'TM_AMOUNT'     => $credit_amount[$t],
                        'TM_TYPE'       => "Credit",
                        'IS_ACTIVE'     => 1
                    );
                }
            }
            if($this->accounts_model->create_transaction_meta($tr_data)) {
                $activity = array(
                    'user' => $this->session->userdata('user_id'),
                    'module' => 'entry book',
                    'module_field_id' => $tr_id,
                    'activity' => lang('activity_general_journal_created'),
                    'icon' => 'fa-usd',
                    'value1' => "PKR ".$_POST['total_credit_amount']
                );
                $this->invoice_model->_table_name = 'tbl_activities';
                $this->invoice_model->_primary_key = 'activities_id';
                $this->invoice_model->save($activity);
                $type = "success";
                $message = lang('general_journal_created');
                set_message($type, $message);
                redirect('admin/accounts/general_journal');
            }
            else{
                $type = "error";
                $message = 'OOPS! Something went wrong';
                set_message($type, $message);
                redirect('admin/accounts/general_journal');
            }
        }
    }
    
    public function ajax_edit_entries($tr_id)
    {
        $transaction = $this->accounts_model->transaction_by_id($tr_id);
        $tr_debit = read_debit_transactions($tr_id);
        $tr_credit = read_credit_transactions($tr_id);
        $data = array(
            'tr'     => $transaction,
            'tr_debit'=> $tr_debit,
            'tr_credit'=> $tr_credit
        );
        echo json_encode($data);
        die();
    }
    
    public function update_entries()
    {
        if($_POST){
            $tr_id= $this->input->post('tr_id');
            $transaction_data = array(
                'T_ID'  => $tr_id,
                'T_DATE'=>$this->input->post('date'),
                'T_TYPE'=>"General Journal",
                'UPDATED_BY'=>$this->session->userdata('user_id')
            );
            $trmd_id = $this->input->post('trmd_id');
            $trmc_id = $this->input->post('trmc_id');
            $debit_account=$this->input->post('debit_account');
            $debit_particular=$this->input->post('debit_particular');
            $debit_amount=$this->input->post('debit_amount');
            $credit_account=$this->input->post('credit_account');
            $credit_particular=$this->input->post('credit_particular');
            $credit_amount=$this->input->post('credit_amount');
            $tr_data_update=array();
            $tr_data_insert=array();
            for ($i=0;$i<count($debit_amount);$i++) {
                if(!empty($debit_amount[$i])){
                    if(!empty($trmd_id[$i])) {
                        $tr_data_update[] = array(
                            'TM_ID'         => $trmd_id[$i],
                            'A_ID'          => $debit_account[$i],
                            'PARTICULARS'   => $debit_particular[$i],
                            'TM_AMOUNT'     => $debit_amount[$i],
                            'TM_TYPE'       => "Debit"
                        );
                    }elseif(empty($trmd_id[$i])) {
                        $tr_data_insert[] = array(
                            'T_ID'          => $tr_id,
                            'A_ID'          => $debit_account[$i],
                            'PARTICULARS'   => $debit_particular[$i],
                            'TM_AMOUNT'     => $debit_amount[$i],
                            'TM_TYPE'       => "Debit",
                            'IS_ACTIVE'     => 1
                        );
                    }
                }
            }
            for ($t=0;$t<count($credit_amount);$t++){
                if(!empty($credit_amount[$t])){
                    if(!empty($trmc_id[$i])) {
                        $tr_data_update[] = array(
                            'TM_ID'         => $trmc_id[$i],
                            'A_ID'          => $credit_account[$t],
                            'PARTICULARS'   => $credit_particular[$t],
                            'TM_AMOUNT'     => $credit_amount[$t],
                            'TM_TYPE'       => "Credit"
                        );
                    }elseif(empty($trmc_id[$t])) {
                        $tr_data_insert[] = array(
                            'T_ID'          => $tr_id,
                            'A_ID'          => $credit_account[$t],
                            'PARTICULARS'   => $credit_particular[$t],
                            'TM_AMOUNT'     => $credit_amount[$t],
                            'TM_TYPE'       => "Credit",
                            'IS_ACTIVE'     => 1
                        );
                    }
                }
            }
            
            if(!empty($tr_data_insert))
            {
                $this->accounts_model->create_transaction_meta($tr_data_insert);
            }
            $this->accounts_model->update_trm_by_id($tr_data_update);
            
            if($this->accounts_model->update_transaction($transaction_data)){
                $activity = array(
                    'user' => $this->session->userdata('user_id'),
                    'module' => 'entry book',
                    'module_field_id' => $tr_id,
                    'activity' => lang('activity_general_journal_updated'),
                    'icon' => 'fa-usd',
                    'value1' => "PKR ".$_POST['total_credit_amount']
                );
                $this->invoice_model->_table_name = 'tbl_activities';
                $this->invoice_model->_primary_key = 'activities_id';
                $this->invoice_model->save($activity);
                $type = "success";
                $message = lang('general_journal_updated');
                set_message($type, $message);
                redirect('admin/accounts/general_journal');
            }
            else{
                $type = "error";
                $message = 'OOPS! Something went wrong';
                set_message($type, $message);
                redirect('admin/accounts/general_journal');
            }
        }
    }
    
    public function delete_trm($trm_id)
    {
        $status=$this->accounts_model->delete_transaction_meta($trm_id);
        echo json_encode($status);
        die();
    }
    
    public function delete_transaction($tr_id)
    {
        $status=$this->accounts_model->delete_transaction($tr_id);
        echo json_encode($status);
        die();
    }
    
    #---------------End General Journal Section-------------------#

    #---------------Start General Ledgers Section-------------------#
    public function general_ledgers ()
    {
        $data['ac']=array();
        if($_POST){
            $account = $this->input->post('accounts');
            $data['account_id'] = $account;
            $start_date = date('Y-m-d',strtotime($this->input->post('start_date')));
            $end_date = date('Y-m-d',strtotime($this->input->post('end_date')));
            $data['start_date'] = $start_date;
            $data['end_date'] = $end_date;
            if($this->input->post('opening')){
                $data['ac'] = $this->accounts_model->account_categories_wise($account);
            }
           
            $data['general_ledgers'] = $this->accounts_model->general_ledgers_data($start_date,$end_date,$account);
        }
        $data['accounts']= $this->accounts_model->ajax_select_accounts(array('A_STATUS'=>1));
        $data['subview'] = $this->load->view('admin/accounts/general_ledgers', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function client_ledgers ()
    {
        $data['ac']=array();
        if($_POST){
            $client_id = $this->input->post('client_id');
            $start_date = date('Y-m-d',strtotime($this->input->post('start_date')));
            $end_date = date('Y-m-d',strtotime($this->input->post('end_date')));
            $data['start_date'] = $start_date;
            $data['end_date'] = $end_date;
            $data['client_id'] = $client_id;
            /*$data['general_ledgers_clients'] = $this->accounts_model->client_ledgers_data($start_date,$end_date,$client_id);*/
            $data['client_ledgers'] = $this->accounts_model->client_ledgers($start_date,$end_date,$client_id);
        }
        $data['all_clients']= $this->accounts_model->ajax_select_client(array('client_status'=>1));
        $data['subview'] = $this->load->view('admin/accounts/general_ledgers_client', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function job_ledgers ()
    {
        $data['ac']=array();
        if($_POST){
            $client_id = $this->input->post('client_id');
            $invoices_id = $this->input->post('invoices_id');
            $data['client_id'] = $client_id;
            $data['invoices_id']= $invoices_id;
            /*$data['advance'] = $this->accounts_model->advance_amount_by_invoice($invoices_id);*/
            /*$data['pre_job_advance'] = $this->accounts_model->pre_advance_by_invoice($invoices_id);
            $data['security_deposit'] = $this->accounts_model->security_deposit_by_invoice($invoices_id);
            $data['bill'] = $this->accounts_model->total_bill_by_invoice($invoices_id);
            $data['receiveds'] = $this->accounts_model->received_amount_by_invoice($invoices_id);
            $data['memos'] = $this->accounts_model->memos_amount_by_invoice($invoices_id);
            $data['expenses'] = $this->accounts_model->expenses_amount_by_invoice($invoices_id);
            $data['job_expenses']=$this->accounts_model->job_expenses_by_invoice($invoices_id);*/
            $data['job_ledgers'] = $this->accounts_model->job_ledgers($invoices_id);
            $data['client_invoices']=$this->invoice_model->check_by_all(array('client_id'=>$client_id), 'tbl_invoices');
        }
        $data['all_clients']= $this->accounts_model->ajax_select_client(array('client_status'=>1));
        $data['subview'] = $this->load->view('admin/accounts/general_ledgers_job', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
    #---------------End General Ledgers  Section-------------------#
    #---------------Start Trial Balance Section-------------------#
    
    public function trial_balance()
    {
        if($_POST){
            $data['start_date'] = date('Y-m-d',strtotime($this->input->post('start_date')));
            $data['end_date'] = date('Y-m-d',strtotime($this->input->post('end_date')));
            $data['opening_value'] = $this->input->post('opening');
        }else{
            $data['start_date'] = '';
            $data['end_date'] = '';
            $data['opening_value'] = '';
        }
        $data['sub_types'] = $this->accounts_model->read_sub_type();
        $data['accounts'] = $this->accounts_model->ajax_select_accounts(array('A_STATUS'=>1));
        $data['subview'] = $this->load->view('admin/accounts/trial_balance', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }
    
    #---------------Start Trial Balance Section-------------------#
    
    #---------------Start Balance Sheet Section-------------------#
    
    public function balance_sheet()
    {
        if($_POST){
            $data['start_date'] = date('Y-m-d',strtotime($this->input->post('start_date')));
            $data['end_date'] = date('Y-m-d',strtotime($this->input->post('end_date')));
            $data['opening_value'] = $this->input->post('opening');
        }else{
            $data['start_date'] = '';
            $data['end_date'] = '';
            $data['opening_value'] = '';
        }
         $data['subview'] = $this->load->view('admin/accounts/balance_sheet', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }
    
    #---------------End Balance Sheet Section-------------------#

    #---------------Start Income Statement Section-------------------#

    public function income_statement()
    {
        
        if($_POST){
            $data['start_date'] = date('Y-m-d',strtotime($this->input->post('start_date')));
            $data['end_date'] = date('Y-m-d',strtotime($this->input->post('end_date')));
        }else{
            $data['start_date'] = '';
            $data['end_date'] = '';
        }
        $data['accounts'] = $this->accounts_model->ajax_select_accounts(array('A_STATUS'=>1));
		$data['subview'] = $this->load->view('admin/accounts/income_statement', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    #---------------Start Income Statement Section-------------------#

    #---------------Start Cash And Bank Book Section-------------------#
    #---------------Start Cash In hand Section-------------------#
    public function cash_inHand()
    {
        $data['accounts']= $this->accounts_model->ajax_select_accounts(array('A_STATUS'=>1));
		$data['subview'] = $this->load->view('admin/accounts/cash_in_hand', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function add_inHand()
    {
        if($_POST) {
            $debit_account=""; $credit_account="";
            if($this->input->post('type')=='Debit'){
                $debit_account =1;
                $credit_account=$this->input->post('account');
            }elseif ($this->input->post('type')=='Credit'){
                $debit_account =$this->input->post('account');
                $credit_account =1;
            }
            $this->accounts_model->create_transaction(array(
                'T_DATE'=>date('Y-m-d',strtotime($this->input->post('date'))),
                'T_PAY' => $this->input->post('pay'),
                'T_TYPE'=>"Cash in Hand",
                'CREATED_BY'=>$this->session->userdata('user_id'),
            ));
            $tr_id=$this->db->insert_id();
            $tr_data=array();
            $tr_data[]=array(
                'T_ID'          => $tr_id,
                'A_ID'          => $debit_account,
                'PARTICULARS'   => $this->input->post('particular'),
                'TM_AMOUNT'     => $this->input->post('amount'),
                'TM_TYPE'       => "Debit",
                'IS_ACTIVE'     => 1
            );
            $tr_data[]=array(
                'T_ID'          => $tr_id,
                'A_ID'          => $credit_account,
                'PARTICULARS'   => $this->input->post('particular'),
                'TM_AMOUNT'     => $this->input->post('amount'),
                'TM_TYPE'       => "Credit",
                'IS_ACTIVE'     => 1
            );
            $this->accounts_model->create_transaction_meta($tr_data);
            $postData = array(
                'CB_DATE'           => date('Y-m-d',strtotime($this->input->post('date'))),
                'CB_DEBITACCOUNT'   => $debit_account,
                'CB_CREDITACCOUNT'  => $credit_account,
                'CB_PAY'            => $this->input->post('pay'),
                'CB_AMOUNT'         => $this->input->post('amount'),
                'CB_PARTICULARS'    => $this->input->post('particular'),
                'CB_TYPE'           => $this->input->post('type'),
                'IS_ACTIVE'         => 1,
                'T_ID'              => $tr_id,
                'CREATED_BY'=>$this->session->userdata('user_id'),
            );
            $this->invoice_model->_table_name = 'cash_and_bank';
            $this->invoice_model->_primary_key = 'CB_ID';
            $id = $this->invoice_model->save($postData);
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'bank & cash',
                'module_field_id' => $id,
                'activity' => lang('activity_added_cash_in_hand'),
                'icon' => 'fa-usd',
                'value1' => $_POST['amount'],
            );
            $this->invoice_model->_table_name = 'tbl_activities';
            $this->invoice_model->_primary_key = 'activities_id';
            $this->invoice_model->save($activity);
            $type = "success";
            $message = lang('cash_in_hand_added');
            set_message($type, $message);
            redirect('admin/accounts/cash_inHand');
        }
    }

    public function edit_cash_inHand($id)
    {
        $result= $this->accounts_model->ajax_select_accounts(array('A_STATUS'=>1));
        $list = "";
        foreach ($result as $value) {
            $client = ($value->CLIENT_ID == "")?'':" (".client_name($value->CLIENT_ID).")";
            $list[$value->A_ID] = $value->A_NAME;
        }
        $data['accounts'] = $list;

        $data['edit_cb'] = $this->accounts_model->read_cashAndBank_id($id);
		$data['subview'] = $this->load->view('admin/accounts/edit_cash_in_hand', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function update_inHand($id)
    {
        $id = decrypt($id);
        if($_POST) {
            $debit_account=""; $credit_account="";
            if($this->input->post('type')=='Debit'){
                $debit_account =1;
                $credit_account=$this->input->post('account');
            }elseif ($this->input->post('type')=='Credit'){
                $debit_account =$this->input->post('account');
                $credit_account =1;
            }
            $this->accounts_model->update_transaction(array(
                'T_ID'  => $this->input->post('tr_id'),
                'T_DATE'=>date('Y-m-d',strtotime($this->input->post('date'))),
                'T_PAY' => $this->input->post('pay'),
                'UPDATED_BY' => $this->session->userdata('user_id'),
            ));
            $tr_data_debit=array(
                'T_ID'          => $this->input->post('tr_id'),
                'A_ID'          => $debit_account,
                'PARTICULARS'   => $this->input->post('particular'),
                'TM_AMOUNT'     => $this->input->post('amount'),
                'TM_TYPE'       => "Debit",
                'IS_ACTIVE'     => 1
            );
            $this->accounts_model->update_transaction_meta($tr_data_debit);
            $tr_data_credit=array(
                'T_ID'          => $this->input->post('tr_id'),
                'A_ID'          => $credit_account,
                'PARTICULARS'   => $this->input->post('particular'),
                'TM_AMOUNT'     => $this->input->post('amount'),
                'TM_TYPE'       => "Credit",
                'IS_ACTIVE'     => 1
            );
            $this->accounts_model->update_transaction_meta($tr_data_credit);
            $postData = array(
                'CB_ID'             => $id,
                'CB_DATE'           => date('Y-m-d',strtotime($this->input->post('date'))),
                'CB_DEBITACCOUNT'   => $debit_account,
                'CB_CREDITACCOUNT'  => $credit_account,
                'CB_PAY'            => $this->input->post('pay'),
                'CB_AMOUNT'         => $this->input->post('amount'),
                'CB_PARTICULARS'    => $this->input->post('particular'),
                'CB_TYPE'           => $this->input->post('type'),
                'UPDATED_BY'           => $this->session->userdata('user_id'),
                'IS_ACTIVE'         => 1
            );
            if($this->accounts_model->update_cashAndBank($postData)) {
                $activity = array(
                    'user' => $this->session->userdata('user_id'),
                    'module' => 'cash & bank',
                    'module_field_id' => $id,
                    'activity' => lang('activity_updated_cash_in_hand'),
                    'icon' => 'fa-usd',
                    'value1' => $_POST['amount'],
                );
                $this->invoice_model->_table_name = 'tbl_activities';
                $this->invoice_model->_primary_key = 'activities_id';
                $this->invoice_model->save($activity);
                $type = "success";
                $message = lang('cash_in_hand_updated');
                set_message($type, $message);
                redirect('admin/accounts/edit_cash_inHand/'.$id);
            }
            else{
                $type = "success";
                $message = 'OOPS! Something went wrong';
                set_message($type, $message);
                redirect('admin/accounts/cash_inHand');
            }
        }
    }

    public function delete_inHand($id)
    {
        $rs=$this->accounts_model->read_cashAndBank_id($id);
        $this->accounts_model->delete_cashAndBank($id);
        $this->accounts_model->delete_transaction($rs->T_ID);
        redirect('admin/accounts/cash_inHand');
    }
    #---------------End Cash In hand Section-------------------#

    #---------------Start Cash & Bank Section-------------------#
    public function cash_atBank()
    {
        $data['accounts']= $this->accounts_model->ajax_select_accounts(array('A_STATUS'=>1));
        $data['banks']= $this->accounts_model->read_bank_where('B_STATUS',1);
		$data['subview'] = $this->load->view('admin/accounts/cash_at_bank', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function add_atBank()
    {
        if($_POST) {
            $debit_account=""; $credit_account="";
            if($this->input->post('type')=='Debit'){
                $debit_account =$this->input->post('cast_atBank_Account');
                $credit_account=$this->input->post('account');
            }elseif ($this->input->post('type')=='Credit'){
                $debit_account =$this->input->post('account');
                $credit_account =$this->input->post('cast_atBank_Account');
            }
            $this->accounts_model->create_transaction(array(
                'T_DATE'=>date('Y-m-d',strtotime($this->input->post('date'))),
                'T_PAY' => $this->input->post('pay'),
                'T_TYPE'=>"Cash at Bank",
                'CREATED_BY'=>$this->session->userdata('user_id'),
            ));
            $tr_id=$this->db->insert_id();
            $tr_data=array();
            $tr_data[]=array(
                'T_ID'          => $tr_id,
                'A_ID'          => $debit_account,
                'PARTICULARS'   => $this->input->post('particular'),
                'TM_AMOUNT'     => $this->input->post('amount'),
                'TM_TYPE'       => "Debit",
                'IS_ACTIVE'     => 1
            );
            $tr_data[]=array(
                'T_ID'          => $tr_id,
                'A_ID'          => $credit_account,
                'PARTICULARS'   => $this->input->post('particular'),
                'TM_AMOUNT'     => $this->input->post('amount'),
                'TM_TYPE'       => "Credit",
                'IS_ACTIVE'     => 1
            );
            $this->accounts_model->create_transaction_meta($tr_data);
            $postData = array(
                'B_ID'              => $this->input->post('bank_name'),
                'CB_DATE'           => date('Y-m-d',strtotime($this->input->post('date'))),
                'CB_DEBITACCOUNT'   => $debit_account,
                'CB_CREDITACCOUNT'  => $credit_account,
                'CB_CHEQUENO'       => $this->input->post('chq_no'),
                'CB_PAY'            => $this->input->post('pay'),
                'CB_AMOUNT'         => $this->input->post('amount'),
                'CB_PARTICULARS'    => $this->input->post('particular'),
                'CB_TYPE'           => $this->input->post('type'),
                'IS_ACTIVE'         => 1,
                'T_ID'              => $tr_id,
                'CREATED_BY'=>$this->session->userdata('user_id'),
            );
            $this->invoice_model->_table_name = 'cash_and_bank';
            $this->invoice_model->_primary_key = 'CB_ID';
            $id = $this->invoice_model->save($postData);
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'cash & bank',
                'module_field_id' => $id,
                'activity' => lang('activity_added_cash_at_bank'),
                'icon' => 'fa-usd',
                'value1' => $_POST['amount'],
            );
            $this->invoice_model->_table_name = 'tbl_activities';
            $this->invoice_model->_primary_key = 'activities_id';
            $this->invoice_model->save($activity);
            $type = "success";
            $message = lang('cash_at_bank_added');
            set_message($type, $message);
            redirect('admin/accounts/cash_atBank');
        }
    }

    public function edit_cash_atBank($id)
    {
        $data['acc']= $this->accounts_model->ajax_select_accounts(array('A_STATUS'=>1));
        $list = "";
        foreach ( $data['acc'] as $value) {
            $client = ($value->CLIENT_ID == "")?'':" (".client_name($value->CLIENT_ID).")";
            $list[$value->A_ID] = $value->A_NAME;
        }
        $data['accounts'] = $list;

        $data['banks']= $this->accounts_model->read_bank_where('B_STATUS',1);

        $data['edit_cb'] = $this->accounts_model->read_cashAndBank_id($id);
		$data['subview'] = $this->load->view('admin/accounts/edit_cash_atBank', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function update_atBank($id)
    {
        $id = decrypt($id);
        if($_POST) {
            $debit_account=""; $credit_account="";
            if($this->input->post('type')=='Debit'){
                $debit_account =$this->input->post('cast_atBank_Account');
                $credit_account=$this->input->post('account');
            }elseif ($this->input->post('type')=='Credit'){
                $debit_account =$this->input->post('account');
                $credit_account =$this->input->post('cast_atBank_Account');
            }
            $this->accounts_model->update_transaction(array(
                'T_ID'  => $this->input->post('tr_id'),
                'T_DATE'=>date('Y-m-d',strtotime($this->input->post('date'))),
                'T_PAY' => $this->input->post('pay'),
                'UPDATED_BY' => $this->session->userdata('user_id'),
            ));
            $tr_data_debit=array(
                'T_ID'          => $this->input->post('tr_id'),
                'A_ID'          => $debit_account,
                'PARTICULARS'   => $this->input->post('particular'),
                'TM_AMOUNT'     => $this->input->post('amount'),
                'TM_TYPE'       => "Debit",
                'IS_ACTIVE'     => 1
            );
            $this->accounts_model->update_transaction_meta($tr_data_debit);
            $tr_data_credit=array(
                'T_ID'          => $this->input->post('tr_id'),
                'A_ID'          => $credit_account,
                'PARTICULARS'   => $this->input->post('particular'),
                'TM_AMOUNT'     => $this->input->post('amount'),
                'TM_TYPE'       => "Credit",
                'IS_ACTIVE'     => 1
            );
            $this->accounts_model->update_transaction_meta($tr_data_credit);
            $postData = array(
                'B_ID'              => $this->input->post('bank_name'),
                'CB_ID'             => $id,
                'CB_DATE'           => date('Y-m-d',strtotime($this->input->post('date'))),
                'CB_DEBITACCOUNT'   => $debit_account,
                'CB_CREDITACCOUNT'  => $credit_account,
                'CB_PAY'            => $this->input->post('pay'),
                'CB_AMOUNT'         => $this->input->post('amount'),
                'CB_PARTICULARS'    => $this->input->post('particular'),
                'CB_TYPE'           => $this->input->post('type'),
                'UPDATED_BY' => $this->session->userdata('user_id'),
                'IS_ACTIVE'         => 1
            );
            if($this->accounts_model->update_cashAndBank($postData)) {
                $activity = array(
                    'user' => $this->session->userdata('user_id'),
                    'module' => 'cash & bank',
                    'module_field_id' => $id,
                    'activity' => lang('activity_updated_cash_at_bank'),
                    'icon' => 'fa-usd',
                    'value1' => $_POST['amount'],
                );
                $this->invoice_model->_table_name = 'tbl_activities';
                $this->invoice_model->_primary_key = 'activities_id';
                $this->invoice_model->save($activity);
                $type = "success";
                $message = lang('cash_at_bank_updated');
                set_message($type, $message);
                redirect('admin/accounts/edit_cash_atBank/'.$id);
            }
            else{
                $type = "success";
                $message = 'OOPS! Something went wrong';
                set_message($type, $message);
                redirect('admin/accounts/edit_cash_atBank');
            }
        }
    }

    public function delete_atBank($action, $id)
    {
        $rs=$this->accounts_model->read_cashAndBank_id($id);
        $this->accounts_model->delete_cashAndBank($id);
        $this->accounts_model->delete_transaction($rs->T_ID);
        redirect('admin/accounts/cash_atBank');
    }
    #---------------End Cash & Bank Section-------------------#

    #---------------End Cash And Bank Book Section-------------------#

    public function select_opening($account,$c_date)
    {
        $date = date('Y-m-d',strtotime($c_date));
        $next_date = date('Y-m-d',strtotime($c_date.' +1 day'));

        #------Opening Balance Section---#
        $opening_balance=0;
        $closing_balance = 0;
        $account_data=$this->accounts_model->read_account_by_id($account);
        if($account_data->A_OPENINGTYPE=='Debit'){
            $opening_balance+= $account_data->A_OPENINGBALANCE;
            $closing_balance+= $account_data->A_OPENINGBALANCE;
        }elseif($account_data->A_OPENINGTYPE=='Credit'){
            $opening_balance-= $account_data->A_OPENINGBALANCE;
            $closing_balance-= $account_data->A_OPENINGBALANCE;
        }
        $debit_transactions = transaction_date($account,$date,"Debit");
        $credit_transactions = transaction_date($account,$date,"Credit");

        $opening_balance=$opening_balance+$debit_transactions->amount-$credit_transactions->amount;
        #------Opening Balance Section---#

        #------Closing Balance Section---#
        $closing_debit_transactions = transaction_date($account,$next_date,"Debit");
        $closing_credit_transactions = transaction_date($account,$next_date,"Credit");

        $closing_balance=$closing_balance+$closing_debit_transactions->amount-$closing_credit_transactions->amount;
        #------Closing Balance Section---#

        $cash_data = $this->accounts_model->read_cashAndBank($account,$date);
        $expense = $this->accounts_model->read_expense_by_account($account,$date);
        $cheques = $this->accounts_model->read_cheques_by_account($account,$date);
        $data=array(
          'opening_balance' => $opening_balance,
          'closing_balance' => $closing_balance,
            'data'          => $cash_data ,
            'expense_data'  => $expense,
            'ch'           => $cheques
        );
        echo json_encode($data);
		die();
    }
    #---------------Start Voucher Section-------------------#

    /*** PRE SHIPMENT VOUCHER ***/
    public function pre_voucher_list()
    {
        $data['vouchers'] = $this->invoice_model->check_by_all(array('voucher_type'=>'Pre'), 'tbl_vouchers');
        $data['accounts']= $this->accounts_model->ajax_select_accounts(array('A_STATUS'=>1));
        $data['subview'] = $this->load->view('admin/accounts/pre_voucher_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function get_pre_client_accounts(){
        $job_id = $_POST['invoice_id'];
        $job = $this->invoice_model->check_by(array('invoices_id'=>$job_id), 'tbl_invoices');
        $requisition = $this->invoice_model->check_by(array('invoices_id'=>$job_id), 'tbl_requisitions');
        $client = $this->invoice_model->check_by(array('client_id'=>$job->client_id), 'tbl_client');
        if(!empty($requisition)){
            $ap_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Liabilities', 'NAME' => 'Current Liabilities'), 'sub_heads');
            $ap_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_head->SUB_HEAD_ID, 'H_NAME' => 'Advances from clients'), 'accounts_head');
            $advances_accounts = $this->invoice_model->check_by_all(array('SUB_HEAD_ID' => $ap_sub_head->SUB_HEAD_ID, 'H_ID' => $ap_sub_head->H_ID, 'CLIENT_ID' => $client->client_id,'A_NAME'=>client_name($client->client_id). ' (Advances from clients)'), 'accounts');
            echo json_encode(array('client_name'=>$client->name, 'client_accounts'=>$advances_accounts));
            die();
        }
        else {
            echo json_encode("");
            die();
        }
    }

    public function manage_pre_voucher($action = NULL, $id = NULL) {
        $id = decode($id);
        $this->db->select('*');
        $this->db->from('tbl_invoices');
        $this->db->join('tbl_requisitions', 'tbl_requisitions.invoices_id = tbl_invoices.invoices_id');
        $this->db->order_by('tbl_invoices.created_date', 'DESC');
        $this->db->group_by('tbl_requisitions.invoices_id');
        $data['all_invoices'] = $this->db->get()->result();
        $data['banks'] = $this->accounts_model->read_bank_where('B_STATUS',1);
        $data['vouchers'] = $this->accounts_model->read_expense();
        $data['accounts']= $this->accounts_model->ajax_select_accounts(array('A_STATUS'=>1));
        if($action == 'edit_voucher'){
            $this->db->select('*');
            $this->db->from('tbl_vouchers');
            $this->db->where('voucher_id', $id);
            $data['voucher_info'] = $this->db->get()->row();
            $subview = "manage_pre_voucher";
        }
        elseif($action == 'voucher_detail'){
            $data['voucher'] = $this->invoice_model->check_by(array('voucher_id'=>$id), 'tbl_vouchers');
            $subview = "pre_voucher";
        }
        else{
            $subview = "manage_pre_voucher";
        }
        $data['title'] = "Pre Shipment Voucher";
        $data['subview'] = 'admin/accounts/'.$subview;
        $this->load->view('admin/_layout_main2', $data);
    }

    public function save_pre_voucher($id = NULL){
        if(!empty($id)){
            $voucher_id = decrypt($id);
            /*** TRANSACTION ***/
            $t_id = decrypt($this->input->post('transaction_id'));
            $t_data = array(
                'T_PAY'=>$this->input->post('paid_to'),
                'T_DATE'=>$this->input->post('date')
            );

            $this->invoice_model->_table_name = 'transactions';
            $this->invoice_model->_primary_key = 'T_ID';
            $this->invoice_model->save($t_data, $t_id);
            /*** END TRANSACTION ***/

            $account_id = NULL;
            if($this->input->post('payment_type') == 1){
                $account_id = $this->input->post('payment_type');
            }
            else{
                $account_id = $this->input->post('account_id');
            }
            $voucherData = array(
                'credit_account' => $account_id,
                'payment_method' => $this->input->post('payment_type'),
                'voucher_type' => 'Pre',
                'paid_to' => $this->input->post('paid_to'),
                'cheque_no' => $this->input->post('chq_no'),
                'description' => $this->input->post('desc'),
                'payment_date' => $this->input->post('date'),
                'transaction_id'  => $t_id,
                'created_by'=> $this->session->userdata('user_id')
            );


            $this->invoice_model->_table_name = 'tbl_vouchers';
            $this->invoice_model->_primary_key = 'voucher_id';
            $this->invoice_model->save($voucherData, $voucher_id);

            $this->invoice_model->_table_name = 'tbl_voucher_details';
            $this->invoice_model->delete_multiple(array('voucher_id'=>$voucher_id));

            $this->invoice_model->_table_name = 'transactions_meta';
            $this->invoice_model->delete_multiple(array('T_ID'=>$t_id));

            /*** TRANSACTION META ***/
            $invoices_id = $this->input->post('invoice_id');
            $debit_account = $this->input->post('debit_account');
            $amount = $this->input->post('amount');
            if (!empty($invoices_id)) {
                for($x=0; $x<count($invoices_id); $x++){
                    $voucher_detail = array(
                        'voucher_id'=>$voucher_id,
                        'invoices_id'=>$invoices_id[$x],
                        'debit_account'=>$debit_account[$x],
                        'amount'=>$amount[$x]
                    );

                    $this->invoice_model->_table_name = 'tbl_voucher_details';
                    $this->invoice_model->_primary_key = 'voucher_detail_id';
                    $this->invoice_model->save($voucher_detail);
                    $tr_data = array();
                    $tr_data[] = array(
                        'T_ID'          => $t_id,
                        'A_ID'          => $account_id,
                        'PARTICULARS'   => $this->input->post('desc'),
                        'TM_AMOUNT'     => $amount[$x],
                        'TM_TYPE'       => "Credit",
                        'IS_ACTIVE'     => 1
                    );


                    $tr_data[] = array(
                        'T_ID'          => $t_id,
                        'A_ID'          => $debit_account[$x],
                        'PARTICULARS'   => $this->input->post('desc'),
                        'TM_AMOUNT'     => $amount[$x],
                        'TM_TYPE'       => "Debit",
                        'IS_ACTIVE'     => 1
                    );

                    $this->accounts_model->create_transaction_meta($tr_data);
                }
            }
            /*** END TRANSACTION META ***/

            $action = lang('activity_voucher_created');
            $msg = lang('voucher_updated');
        }
        else{
            $this->accounts_model->create_transaction(array(
                'T_DATE'=>$this->input->post('date'),
                'T_PAY' => $this->input->post('paid_to'),
                'T_TYPE'=>"Pre Shipment Voucher"
            ));
            $tr_id = $this->db->insert_id();

            $account_id = NULL;
            if($this->input->post('payment_type') == 1){
                $account_id = $this->input->post('payment_type');
            }
            else{
                $account_id = $this->input->post('account_id');
            }

            $voucherData = array(
                'credit_account' => $account_id,
                'payment_method' => $this->input->post('payment_type'),
                'voucher_type' => 'Pre',
                'paid_to' => $this->input->post('paid_to'),
                'cheque_no' => $this->input->post('chq_no'),
                'description' => $this->input->post('desc'),
                'payment_date' => $this->input->post('date'),
                'transaction_id'  => $tr_id,
                'created_by'=> $this->session->userdata('user_id')
            );
            $this->invoice_model->_table_name = 'tbl_vouchers';
            $this->invoice_model->_primary_key = 'voucher_id';
            $voucher_id = $this->invoice_model->save($voucherData);

            $invoices_id = $this->input->post('invoice_id');
            $debit_account = $this->input->post('debit_account');
            $amount = $this->input->post('amount');

            if (!empty($invoices_id)) {
                for($x=0; $x<count($invoices_id); $x++){
                    $voucher_detail = array(
                        'voucher_id'=>$voucher_id,
                        'invoices_id'=>$invoices_id[$x],
                        'debit_account'=>$debit_account[$x],
                        'amount'=>$amount[$x]
                    );
                    $this->invoice_model->_table_name = 'tbl_voucher_details';
                    $this->invoice_model->_primary_key = 'voucher_detail_id';
                    $this->invoice_model->save($voucher_detail);
                    $tr_data = array();
                    $tr_data[] = array(
                        'T_ID'          => $tr_id,
                        'A_ID'          => $account_id,
                        'PARTICULARS'   => $this->input->post('desc'),
                        'TM_AMOUNT'     => $amount[$x],
                        'TM_TYPE'       => "Credit",
                        'IS_ACTIVE'     => 1
                    );

                    $tr_data[] = array(
                        'T_ID'          => $tr_id,
                        'A_ID'          => $debit_account[$x],
                        'PARTICULARS'   => $this->input->post('desc'),
                        'TM_AMOUNT'     => $amount[$x],
                        'TM_TYPE'       => "Debit",
                        'IS_ACTIVE'     => 1
                    );

                    $this->accounts_model->create_transaction_meta($tr_data);
                }
            }
            $action = lang('activity_voucher_created');
            $msg = lang('voucher_created');
        }
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'Voucher',
            'module_field_id' => $voucher_id,
            'activity' => $action,
            'icon' => 'fa-circle-o',
            'value1' => "Pre Shipment Voucher"
        );
        $this->invoice_model->_table_name = 'tbl_activities';
        $this->invoice_model->_primary_key = 'activities_id';
        $this->invoice_model->save($activity);

        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/accounts/pre_voucher_list');
    }
    /*** END PRE SHIPMENT VOUCHER ***/

    /*** POST SHIPMENT VOUCHER ***/
    public function pos_voucher_list()
    {
        $data['vouchers'] = $this->accounts_model->read_expense('Post');
        $data['subview'] = $this->load->view('admin/accounts/pos_voucher_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function manage_pos_voucher($action = NULL, $id = NULL) {
        $id = decode($id);
        $this->db->select('*');
        $this->db->from('tbl_invoices');
        $this->db->join('tbl_bills', 'tbl_bills.invoices_id = tbl_invoices.invoices_id');
        $this->db->order_by('tbl_invoices.created_date', 'DESC');
        $data['all_invoices'] = $this->db->get()->result();
        $data['banks'] = $this->accounts_model->read_bank_where('B_STATUS',1);
        $data['vouchers'] = $this->accounts_model->read_expense();
        $data['accounts']= $this->accounts_model->ajax_select_accounts(array('A_STATUS'=>1));
        if($action == 'edit_voucher'){
            $this->db->select('*');
            $this->db->from('expense');
            $this->db->join('tbl_invoices', 'tbl_invoices.invoices_id = expense.INVOICES_ID');
            $this->db->join('tbl_client', 'tbl_invoices.client_id = tbl_client.client_id');
            $this->db->where('expense.E_ID', $id);
            $data['voucher_info'] = $this->db->get()->row();
            $subview = "manage_pos_voucher";
        }
        elseif($action == 'voucher_detail'){
            $data['voucher'] = $this->accounts_model->read_expense_by_id($id);
            $subview = "pos_voucher";
        }
        else{
            $subview = "manage_pos_voucher";
        }
        $data['title'] = "Post Shipment Voucher";
        $data['subview'] = 'admin/accounts/'.$subview;
        $this->load->view('admin/_layout_main2', $data);
    }

    public function get_post_client_accounts(){
        $invoice_id = $_POST['invoice_id'];
        $bill_info = $this->invoice_model->check_by(array('invoices_id'=>$invoice_id), 'tbl_bills');
        if(!empty($bill_info)){
            $get_invoice_info = $this->invoice_model->check_by(array('invoices_id' => $invoice_id), 'tbl_invoices');
            $get_client_info = $this->invoice_model->check_by(array('client_id' => $get_invoice_info->client_id), 'tbl_client');
            $get_t_id = $this->invoice_model->check_by(array('invoices_id' => $invoice_id), 'tbl_bills');
            $ap_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Liabilities', 'NAME' => 'Current Liabilities'), 'sub_heads');
            $ap_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_head->SUB_HEAD_ID, 'H_NAME' => 'Account Payables'), 'accounts_head');
            $get_client_accounts = $this->invoice_model->check_by_all(array('SUB_HEAD_ID' => $ap_sub_head->SUB_HEAD_ID, 'H_ID' => $ap_sub_head->H_ID, 'CLIENT_ID' => $get_invoice_info->client_id, 'A_NAME'=>client_name($get_invoice_info->client_id). ' (Account Payables)'), 'accounts');
            if(!empty($get_client_accounts)){
                $value = $get_client_accounts;
                echo json_encode(array('client_name'=>$get_client_info->name, 'client_accounts'=>$value));
                die();
            }
            else{
                echo json_encode("");
                die();
            }
        }
        else{
            echo json_encode("");
            die();
        }
    }

    public function save_pos_voucher($id = NULL)
    {
        if(!empty($id)) {
            $id = decrypt($id);
            $expense_id = $id;
            $account_id = NULL;
            if($this->input->post('payment_type') == 1){
                $account_id = $this->input->post('payment_type');
            }
            else{
                $account_id = $this->input->post('account_id');
            }
            $expenseData = array(
                'INVOICES_ID' => $this->input->post('invoice_id'),
                'DEBIT_ACCOUNT' => $this->input->post('debit_account'),
                'CREDIT_ACCOUNT' => $account_id,
                'EXPENSE_TYPE' => $this->input->post('payment_type'),
                'PAID_TO' => $this->input->post('paid_to'),
                'CHEQUE_NO' => $this->input->post('chq_no'),
                'DESCRIPTION' => $this->input->post('desc'),
                'AMOUNT' => $this->input->post('amount'),
                'AMOUNT_IN_WORDS' => $this->input->post('amountWords'),
                'E_DATE' => $this->input->post('date'),
            );

            $this->invoice_model->_table_name = 'expense';
            $this->invoice_model->_primary_key = 'E_ID';
            $this->invoice_model->save($expenseData, $id);
            /*** TRANSACTION ***/
            $t_id = decrypt($this->input->post('transaction_id'));
            $t_data = array(
                'T_PAY'=>$this->input->post('paid_to'),
                'T_DATE'=>$this->input->post('date')
            );

            $this->invoice_model->_table_name = 'transactions';
            $this->invoice_model->_primary_key = 'T_ID';
            $this->invoice_model->save($t_data, $t_id);
            /*** END TRANSACTION ***/
            /*** TRANSACTION META ***/
            $trans_account_id = decrypt($this->input->post('trans_account_id'));
            $bank_and_cash_id = decrypt($this->input->post('bank_and_cash_id'));
            $tm_data = array(
                'A_ID'          => $this->input->post('debit_account'),
                'PARTICULARS'   => $this->input->post('desc'),
                'TM_AMOUNT'     => $this->input->post('amount')
            );

            $this->invoice_model->_table_name = 'transactions_meta';
            $this->invoice_model->_primary_key = 'TM_ID';
            $this->invoice_model->save($tm_data, $trans_account_id);
            $tm_data = array(
                'A_ID'          => $account_id,
                'PARTICULARS'   => $this->input->post('desc'),
                'TM_AMOUNT'     => $this->input->post('amount')
            );

            $this->invoice_model->_table_name = 'transactions_meta';
            $this->invoice_model->_primary_key = 'TM_ID';
            $this->invoice_model->save($tm_data, $bank_and_cash_id);
            /*** END TRANSACTION META ***/

            $action = lang('activity_voucher_created');
            $msg = lang('voucher_updated');
        }
        else{
            $this->accounts_model->create_transaction(array(
                'T_DATE'=>$this->input->post('date'),
                'T_PAY' => $this->input->post('paid_to'),
                'T_TYPE'=>"Post Shipment Voucher"
            ));
            $tr_id = $this->db->insert_id();
            $tr_data = array();
            $tr_data[] = array(
                'T_ID'          => $tr_id,
                'A_ID'          => $this->input->post('debit_account'),
                'PARTICULARS'   => $this->input->post('desc'),
                'TM_AMOUNT'     => $this->input->post('amount'),
                'TM_TYPE'       => "Debit",
                'IS_ACTIVE'     => 1
            );
            $account_id = NULL;
            if($this->input->post('payment_type') == 1){
                $account_id = $this->input->post('payment_type');
            }
            else{
                $account_id = $this->input->post('account_id');
            }
            $tr_data[]=array(
                'T_ID'          => $tr_id,
                'A_ID'          => $account_id,
                'PARTICULARS'   => $this->input->post('desc'),
                'TM_AMOUNT'     => $this->input->post('amount'),
                'TM_TYPE'       => "Credit",
                'IS_ACTIVE'     => 1
            );

            $this->accounts_model->create_transaction_meta($tr_data);

            $expenseData = array(
                'INVOICES_ID' => $this->input->post('invoice_id'),
                'DEBIT_ACCOUNT' => $this->input->post('debit_account'),
                'CREDIT_ACCOUNT' => $account_id,
                'EXPENSE_TYPE' => $this->input->post('payment_type'),
                'VOUCHER_TYPE' => 'Post',
                'PAID_TO' => $this->input->post('paid_to'),
                'CHEQUE_NO' => $this->input->post('chq_no'),
                'DESCRIPTION' => $this->input->post('desc'),
                'AMOUNT' => $this->input->post('amount'),
                'AMOUNT_IN_WORDS' => $this->input->post('amountWords'),
                'E_DATE' => $this->input->post('date'),
                'T_ID'  => $tr_id,
                'E_STATUS' => "Approved",
                'IS_ACTIVE' => 1,
                'CREATED_BY'=> $this->session->userdata('user_id')
            );


            $this->invoice_model->_table_name = 'expense';
            $this->invoice_model->_primary_key = 'E_ID';
            $expense_id = $this->invoice_model->save($expenseData);

            $action = lang('activity_voucher_created');
            $msg = lang('voucher_created');
        }
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'Voucher',
            'module_field_id' => $expense_id,
            'activity' => $action,
            'icon' => 'fa-circle-o',
            'value1' => "Post Shipment Voucher",
            'value2' => $this->input->post('amount')
        );
        $this->invoice_model->_table_name = 'tbl_activities';
        $this->invoice_model->_primary_key = 'activities_id';
        $this->invoice_model->save($activity);

        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/accounts/pos_voucher_list');
    }
    /*** END POST SHIPMENT VOUCHER ***/

    /*** EXPENDITURE VOUCHER ***/
    public function exp_voucher_list()
    {
        $data['vouchers'] = $this->accounts_model->read_petty_expense();
        $data['subview'] = $this->load->view('admin/accounts/exp_voucher_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function manage_exp_voucher($action = NULL, $id = NULL) {
        $data['banks'] = $this->accounts_model->read_bank_where('B_STATUS',1);
        $data['vouchers'] = $this->accounts_model->read_expense();

        $ap_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Expense', 'NAME' => 'Operating Expense'), 'sub_heads');
        $ap_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_head->SUB_HEAD_ID), 'accounts_head');
        $data['expenses'] = $this->invoice_model->check_by_all(array('SUB_HEAD_ID' => $ap_sub_head->SUB_HEAD_ID, 'CLIENT_ID' => NULL), 'accounts');

        $this->invoice_model->_table_name = 'tbl_staff';
        $this->invoice_model->_order_by = 'staff_id';
        $data['all_staffs'] = $this->invoice_model->get();

        if($action == 'edit_voucher'){
            $this->db->select('*');
            $this->db->from('petty_cash');
            $this->db->where('PC_ID', $id);
            $data['voucher_info'] = $this->db->get()->row();
            $data['voucher_details']=petty_cash_details($id);
            $subview = "manage_exp_voucher";
        }
        elseif($action == 'voucher_detail'){
            $data['voucher'] = $this->accounts_model->read_petty_cash_by_id($id);
            $subview = "exp_voucher";
        }
        else{
            $subview = "manage_exp_voucher";
        }

        $data['title'] = "Expense Voucher";
        $data['subview'] = 'admin/accounts/'.$subview;
        $this->load->view('admin/_layout_main2', $data);
    }

    public function exp_voucher()
    {
        $search_array=array();
        $rows = array();
        $search_value=$_POST['search']['value'];
        if ($search_value){
            $search_array=array(
                'PC_VOUCHER_NO'  => $search_value
            );
        }
        $length = 10; $start = 1;
        if($_POST['length'] != -1){
            $length = $_POST['length'];
            $start = $_POST['start'];
        }
        $exp_voucher_list = $this->accounts_model->exp_voucher_list($search_array,$length,$start);
        $exp_voucher_list_rows = $this->accounts_model->exp_voucher_list_rows($search_array);
        $i = $_POST['start']+1;
        foreach($exp_voucher_list as $voucher){
            $rows[] = array(
                $voucher->PC_VOUCHER_NO,
                ($voucher->staff_name == 'Cash In Hand')? 'CASH':'BANK',
                $voucher->staff_name,
                $voucher->A_NAME,
                date('d-m-Y',strtotime($voucher->PC_DATE)),
                number_format($voucher->PCD_AMOUNT,2),
                $voucher->username,
                btn_view('admin/accounts/manage_exp_voucher/voucher_detail/'.$voucher->PC_ID)
            );
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $exp_voucher_list_rows,
            "recordsFiltered" => $exp_voucher_list_rows,
            "data" => $rows
        );
        echo json_encode($output);
    }

    public function save_exp_voucher($id = NULL)
    {
        if(!empty($id))
        {
            $pc_id = $id;
            $payment_mode=$this->input->post('payment_type');
            $tr_id = $this->input->post('transaction_id');
            $tr_id1 = $this->input->post('transaction_id1');
            if ($payment_mode==1){
                $credit_account=1;
            }else{
                $credit_account=$this->input->post('account_id');
            }

            $petty_cash = array(
                'PC_ID'                 => $pc_id,
                'PC_DATE'               => date('Y-m-d',strtotime($this->input->post('date'))),
                'PC_PAYMENT_MODE'       => 1,
                'PC_PAID_TO'            => $credit_account,
                'PC_PAID_BY'            => $this->session->userdata('user_id'),
                'PC_DESC'               => $this->input->post('desc'),
                'T_ID'                  => $tr_id,
                'T_ID1'                 => $tr_id1
            );

            $this->invoice_model->_table_name = 'petty_cash';
            $this->invoice_model->_primary_key = 'PC_ID';
            $this->invoice_model->save($petty_cash, $id);

            $pcd_id=$this->input->post('pcd_id');
            $expense_account=$this->input->post('expense_account');
            $amount=$this->input->post('amount');
            $total_amount=array_sum($amount);
            $this->db->trans_begin();
            $this->invoice_model->_table_name = 'petty_cash_details';
            $this->invoice_model->delete_multiple(array('PC_ID'=>$id));
            for ($j=0;$j<count($amount);$j++){
                $petty_cash_detail=array(
                    'PC_ID'         => $id,
                    'A_ID'          => $expense_account[$j],
                    'PCD_AMOUNT'    => $amount[$j],
                    'PCD_STATUS'    => 1,
                    'PCD_DATETIME'  => date('Y-m-d H:i:s')
                );
                $this->invoice_model->_table_name = 'petty_cash_details';
                $this->invoice_model->_primary_key = 'PCD_ID';
                $this->invoice_model->save($petty_cash_detail);
            }

            /*** TRANSACTION ***/
            $t_data = array(
                'T_PAY'=>'',
                'T_DATE'=>$this->input->post('date')
            );

            $this->invoice_model->_table_name = 'transactions';
            $this->invoice_model->_primary_key = 'T_ID';
            $this->invoice_model->save($t_data, $tr_id);

            $this->invoice_model->_table_name = 'transactions';
            $this->invoice_model->_primary_key = 'T_ID';
            $this->invoice_model->save($t_data, $tr_id1);
            /*** END TRANSACTION ***/
            /*** TRANSACTION META ***/
            $this->invoice_model->_table_name = 'transactions_meta';
            $this->invoice_model->delete_multiple(array('T_ID'=>$tr_id));

            $tr_data = array();
            $tr_data[]=array(
                'T_ID'          => $tr_id,
                'A_ID'          => $this->input->post('payment_type'),
                'PARTICULARS'   => $this->input->post('desc'),
                'TM_AMOUNT'     => $total_amount,
                'TM_TYPE'       => "Credit",
                'IS_ACTIVE'     => 1
            );

            $tr_data[] = array(
                'T_ID'          => $tr_id,
                'A_ID'          => $credit_account,
                'PARTICULARS'   => $this->input->post('desc'),
                'TM_AMOUNT'     => $total_amount,
                'TM_TYPE'       => "Debit",
                'IS_ACTIVE'     => 1
            );
            $this->accounts_model->create_transaction_meta($tr_data);

            $this->invoice_model->_table_name = 'transactions_meta';
            $this->invoice_model->delete_multiple(array('T_ID'=>$tr_id1));
            $tr_data1 = array();
            $tr_data1[] = array(
                'T_ID'          => $tr_id1,
                'A_ID'          => $credit_account,
                'PARTICULARS'   => $this->input->post('desc'),
                'TM_AMOUNT'     => $total_amount,
                'TM_TYPE'       => "Credit",
                'IS_ACTIVE'     => 1
            );

            $expense_account=$this->input->post('expense_account');
            for($k=0;$k<count($amount);$k++){
                $tr_data1[] = array(
                    'T_ID'          => $tr_id1,
                    'A_ID'          => $expense_account[$k],
                    'PARTICULARS'   => $this->input->post('desc'),
                    'TM_AMOUNT'     => $amount[$k],
                    'TM_TYPE'       => "Debit",
                    'IS_ACTIVE'     => 1
                );
            }
            $this->accounts_model->create_transaction_meta($tr_data1);
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            }
            /*** END TRANSACTION META ***/

            $action = lang('activity_updated_a_general_expense');
            $msg = lang('general_expense_updated');
        }
        else{
            #--Voucher No--#
            $payment_mode=$this->input->post('payment_type');
            $last_ref = $this->accounts_model->last_pcref_no();
            $no = $last_ref->PC_ID;
            $ref_no="100".($no+1);

            if ($payment_mode==1){
                $credit_account=1;
            }else{
                $credit_account=$this->input->post('account_id');
            }
            $staff_account=$this->input->post('staff_account');
            $amount=$this->input->post('amount');
            $total_amount=array_sum($amount);
            $this->accounts_model->create_transaction(array(
                'T_DATE'=>$this->input->post('date'),
                'T_PAY' => '',
                'T_TYPE'=>"General Expense",
                'CREATED_BY'=> $this->session->userdata('user_id'),
                'CREATED_AT' => date('Y-m-d H:i:s')
            ));
            $tr_id = $this->db->insert_id();
            $tr_data = array();
            $tr_data[] = array(
                'T_ID'          => $tr_id,
                'A_ID'          => $credit_account,
                'PARTICULARS'   => $this->input->post('desc'),
                'TM_AMOUNT'     => $total_amount,
                'TM_TYPE'       => "Credit",
                'IS_ACTIVE'     => 1
            );

            $expense_account=$this->input->post('expense_account');
            for($k=0;$k<count($amount);$k++){
                $tr_data[] = array(
                    'T_ID'          => $tr_id,
                    'A_ID'          => $expense_account[$k],
                    'PARTICULARS'   => $this->input->post('desc'),
                    'TM_AMOUNT'     => $amount[$k],
                    'TM_TYPE'       => "Debit",
                    'IS_ACTIVE'     => 1
                );
            }
            $this->accounts_model->create_transaction_meta($tr_data);

            $petty_cash = array(
                'PC_REF_NO'             => $ref_no,
                'PC_VOUCHER_NO'         => 'GEV'.$ref_no,
                'PC_DATE'               => date('Y-m-d',strtotime($this->input->post('date'))),
                'PC_PAYMENT_MODE'       => 1,
                'PC_PAID_TO'            => $credit_account,
                'PC_PAID_BY'            => $this->session->userdata('user_id'),
                'PC_DESC'               => $this->input->post('desc'),
                'T_ID'                  => $tr_id,
                'PC_STATUS'             => "Approved",
                'IS_ACTIVE'             => 1,
                'PC_DATETIME'           => date('Y-m-d H:i:s')
            );
            $this->invoice_model->_table_name = 'petty_cash';
            $this->invoice_model->_primary_key = 'PC_ID';
            $pc_id = $this->invoice_model->save($petty_cash);

            for ($j=0;$j<count($amount);$j++){
                $petty_cash_detail=array(
                    'PC_ID'         => $pc_id,
                    'A_ID'          => $expense_account[$j],
                    'PCD_AMOUNT'    => $amount[$j],
                    'PCD_STATUS'    => 1,
                    'PCD_DATETIME'  => date('Y-m-d H:i:s')
                );
                $this->invoice_model->_table_name = 'petty_cash_details';
                $this->invoice_model->_primary_key = 'PCD_ID';
                $this->invoice_model->save($petty_cash_detail);
            }

            $action = 'GEV'.$ref_no.' '.lang('activity_added_a_general_expense');
            $msg = lang('general_expense_added');
        }
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'entry book',
            'module_field_id' => $pc_id,
            'activity' => $action,
            'icon' => 'fa-circle-o',
            'value1' => "PKR ".$total_amount
        );
        $this->invoice_model->_table_name = 'tbl_activities';
        $this->invoice_model->_primary_key = 'activities_id';
        $this->invoice_model->save($activity);

        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/accounts/exp_voucher_list');
    }
    /*** END EXPENDITURE VOUCHER ***/
    
    /*** START GENERAL VOUCHER ***/
    public function voucher_list()
    {
        $data['vouchers'] = $this->accounts_model->read_general_voucher('Gen');
        $data['accounts']= $this->accounts_model->ajax_select_accounts(array('A_STATUS'=>1));
        $data['subview'] = $this->load->view('admin/accounts/voucher_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function vouchers_list()
    {
        $search_array=array();
        $rows = array();
        $search_value=$_POST['search']['value'];
        if ($search_value){
            $search_array=array(
                'PAID_TO'  => $search_value
            );
        }
        $length = 10; $start = 1;
        if($_POST['length'] != -1){
            $length = $_POST['length'];
            $start = $_POST['start'];
        }
        $vouchers_list = $this->accounts_model->vouchers_list($search_array,$length,$start);
        $vouchers_list_rows = $this->accounts_model->vouchers_list_rows($search_array);
        $i = $_POST['start']+1;
        foreach($vouchers_list as $vouchers){
            $action = '<button type="button" onclick="getEditVoucher(this.value)" value="<?php echo $voucher->E_ID;?>" data-popup="tooltip" data-container="body" data-original-title="Edit Voucher" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#" ><i class="fa fa-edit"></i></button>
                       <a target="_blank" href="'. base_url('admin/accounts/view_voucher/'.encode($vouchers->E_ID)).'" type="button" data-popup="tooltip" title="" data-container="body" data-original-title="View Voucher" class="btn btn-info btn-xs"><i class="fa fa-list-alt"></i></a>';
            $rows[] = array(
                $vouchers->DEBIT_NAME,
                $vouchers->CREDIT_NAME,
                $vouchers->PAID_TO,
                $vouchers->CHEQUE_NO,
                date('d-m-Y',strtotime($vouchers->E_DATE)),
                number_format($vouchers->AMOUNT,2),
                $vouchers->E_STATUS,
                $action
            );
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $vouchers_list_rows,
            "recordsFiltered" => $vouchers_list_rows,
            "data" => $rows
        );
        echo json_encode($output);
    }
    
    public function add_voucher()
    {
        if($this->input->post()) {
            $this->accounts_model->create_transaction(array(
                'T_DATE'=>date('Y-m-d',strtotime($this->input->post('date'))),
                'T_PAY' => $this->input->post('paid_to'),
                'T_TYPE'=>"voucher",
                'CREATED_BY'=>$this->session->userdata('user_id')
            ));
            $tr_id=$this->db->insert_id();
            $tr_data=array();
            $tr_data[]=array(
                'T_ID'          => $tr_id,
                'A_ID'          => $this->input->post('debit_account'),
                'PARTICULARS'   => $this->input->post('desc'),
                'TM_AMOUNT'     => $this->input->post('amount'),
                'TM_TYPE'       => "Debit",
                'IS_ACTIVE'     => 1
            );
            $tr_data[]=array(
                'T_ID'          => $tr_id,
                'A_ID'          => $this->input->post('credit_account'),
                'PARTICULARS'   => $this->input->post('desc'),
                'TM_AMOUNT'     => $this->input->post('amount'),
                'TM_TYPE'       => "Credit",
                'IS_ACTIVE'     => 1
            );
            
            $this->accounts_model->create_transaction_meta($tr_data);
            
            $expenseData = array(
                'VOUCHER_TYPE'  => 'Gen',
                'EXPENSE_TYPE'  => 0,
                'DEBIT_ACCOUNT' => $this->input->post('debit_account'),
                'CREDIT_ACCOUNT' => $this->input->post('credit_account'),
                'PAID_TO' => $this->input->post('paid_to'),
                'CHEQUE_NO' => $this->input->post('chq_no'),
                'DESCRIPTION' => $this->input->post('desc'),
                'AMOUNT' => $this->input->post('amount'),
                'AMOUNT_IN_WORDS' => $this->input->post('amountWords'),
                'E_DATE' => $this->input->post('date'),
                'T_ID'  => $tr_id,
                'E_STATUS' => "Approved",
                'IS_ACTIVE' => 1,
                'CREATED_BY'=> $this->session->userdata('user_id')
            );
            $this->invoice_model->_table_name = 'expense';
            $this->invoice_model->_primary_key = 'E_ID';
            $id = $this->invoice_model->save($expenseData);
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'entry book',
                'module_field_id' => $id,
                'activity' => lang('activity_general_voucher_created'),
                'icon' => 'fa-usd',
                'value1' => "PKR ".$_POST['amount'],
            );
            $this->invoice_model->_table_name = 'tbl_activities';
            $this->invoice_model->_primary_key = 'activities_id';
            $this->invoice_model->save($activity);
            $type = "success";
            $message = lang('general_voucher_created');
            set_message($type, $message);
            redirect('admin/accounts/voucher_list');
        }
    }
    
    public function ajax_edit_voucher($voucher_id)
    {
        $expense=$this->accounts_model->ajax_select_expense_by_id($voucher_id);
        echo json_encode($expense);
        die();
    }
    
    public function edit_voucher()
    {
        if($_POST){
            $tr_id=$this->input->post('tr_id');
            if($this->input->post('tr_id')==0){
                if($this->input->post('v_status')=='Approved'){
                    $this->accounts_model->create_transaction(array(
                        'T_DATE'=>date('Y-m-d',strtotime($this->input->post('date'))),
                        'T_PAY' => $this->input->post('paid_to'),
                        'T_TYPE'=>"voucher",
                        'CREATED_BY'=>$this->session->userdata('user_id')
                    ));
                    $tr_id=$this->db->insert_id();
                    $tr_data=array();
                    $tr_data[]=array(
                        'T_ID'          => $tr_id,
                        'A_ID'          => $this->input->post('debit_account'),
                        'PARTICULARS'   => $this->input->post('desc'),
                        'TM_AMOUNT'     => $this->input->post('amount'),
                        'TM_TYPE'       => "Debit",
                        'IS_ACTIVE'     => 1
                    );
                    $tr_data[]=array(
                        'T_ID'          => $tr_id,
                        'A_ID'          => $this->input->post('credit_account'),
                        'PARTICULARS'   => $this->input->post('desc'),
                        'TM_AMOUNT'     => $this->input->post('amount'),
                        'TM_TYPE'       => "Credit",
                        'IS_ACTIVE'     => 1
                    );
                    $this->accounts_model->create_transaction_meta($tr_data);
                }
            }elseif($this->input->post('tr_id')!=''){
                if($this->input->post('v_status')=='Approved') {
                    $this->accounts_model->update_transaction(array(
                        'T_ID'      => $this->input->post('tr_id'),
                        'T_DATE'    => date('Y-m-d', strtotime($this->input->post('date'))),
                        'T_PAY'     => $this->input->post('paid_to'),
                        'UPDATED_BY'=>$this->session->userdata('user_id')
                    ));
                    $tr_data_debit = array(
                        'T_ID' => $this->input->post('tr_id'),
                        'A_ID' => $this->input->post('debit_account'),
                        'PARTICULARS' => $this->input->post('desc'),
                        'TM_AMOUNT' => $this->input->post('amount'),
                        'TM_TYPE' => "Debit",
                        'IS_ACTIVE' => 1
                    );
                    $this->accounts_model->update_transaction_meta($tr_data_debit);
                    $tr_data_credit = array(
                        'T_ID' => $this->input->post('tr_id'),
                        'A_ID' => $this->input->post('credit_account'),
                        'PARTICULARS' => "",
                        'TM_AMOUNT' => $this->input->post('amount'),
                        'TM_TYPE' => "Credit",
                        'IS_ACTIVE' => 1
                    );
                    $this->accounts_model->update_transaction_meta($tr_data_credit);
                }elseif($this->input->post('v_status')=='Cancel'){
                    $this->accounts_model->delete_transaction($this->input->post('tr_id'));
                    $tr_id =0;
                }
            }
            $expenseData=array(
                'E_ID'              => $this->input->post('voucher_id'),
                'DEBIT_ACCOUNT'     => $this->input->post('debit_account'),
                'CREDIT_ACCOUNT'    => $this->input->post('credit_account'),
                'PAID_TO'           => $this->input->post('paid_to'),
                'CHEQUE_NO'         => $this->input->post('chq_no'),
                'DESCRIPTION'       => $this->input->post('desc'),
                'AMOUNT'            => $this->input->post('amount'),
                'AMOUNT_IN_WORDS'   => $this->input->post('amountWords'),
                'E_DATE'            => $this->input->post('date'),
                'T_ID'              => $tr_id,
                'E_STATUS'          => $this->input->post('v_status'),
                'IS_ACTIVE'         => 1,
                'UPDATED_BY'=>$this->session->userdata('user_id')
            );
            if($this->accounts_model->update_expense($expenseData)){
                $activity = array(
                    'user' => $this->session->userdata('user_id'),
                    'module' => 'entry book',
                    'module_field_id' => $this->input->post('voucher_id'),
                    'activity' => lang('activity_general_voucher_updated'),
                    'icon' => 'fa-usd',
                    'value1' => "PKR ".$_POST['amount'],
                );
                $this->invoice_model->_table_name = 'tbl_activities';
                $this->invoice_model->_primary_key = 'activities_id';
                $this->invoice_model->save($activity);
                $type = "success";
                $message = lang('general_voucher_updated');
                set_message($type, $message);
                redirect('admin/accounts/voucher_list');
            }else{
                $type = "error";
                $message = 'OOPS! Something went wrong';
                set_message($type, $message);
                redirect('admin/accounts/voucher_list');
            }
        }
    }
    
    public function delete_voucher($voucher_id)
    {
        $rs=$this->accounts_model->ajax_select_expense_by_id($voucher_id);
        $this->accounts_model->delete_transaction($rs->T_ID);
        $status=$this->accounts_model->delete_voucher($voucher_id);
        echo json_encode($status);
        die();
    }
    
    public function view_voucher($voucher_id)
    {
        $voucher_id = decode($voucher_id);
        $data['voucher'] = $this->accounts_model->read_expense_by_id($voucher_id);
        $data['subview'] = $this->load->view('admin/accounts/voucher', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
    /*** END GENERAL VOUCHER ***/

    #---------------/End Voucher Section-------------------#

    #---------------Start Cheque Journal Section-------------------#
    public function filterAccounts(){
        if(!isset($_POST['searchTerm'])){
            $fetchData = $this->db->query("select * from accounts order by A_ID limit 10");
        }else{
            $search = $_POST['searchTerm'];
            $fetchData = $this->db->query("select * from accounts where A_NAME like '%".$search."%' limit 10");
        }
        $data = array();
        foreach ($fetchData->result() as $row)
        {
            $client = ($row->CLIENT_ID == "")?"":" (".client_name($row->CLIENT_ID).")";
            $data[] = array("id"=>$row->A_ID, "text"=>$row->A_NAME, 'selected'=>'true');
        }
        echo json_encode($data);
    }

    public function getAccountName($id){
        $a = read_subHead($id);
        $client = ($a->CLIENT_ID == "")?'':" (".client_name($a->CLIENT_ID).")";
        $result = $a->A_NAME;
        echo json_encode($result);
    }

	public function cheques()
    {
        $data['cheques'] = $this->accounts_model->read_cheques();
        $data['banks']  = $this->accounts_model->read_bank_where('B_STATUS',1);
        $data['accounts']= $this->accounts_model->ajax_select_accounts(array('A_STATUS'=>1));
		$data['subview'] = $this->load->view('admin/accounts/cheque_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function cheques_list()
    {
        $search_array=array();
        $rows = array();
        $search_value=$_POST['search']['value'];
        if ($search_value){
            $search_array=array(
                'b.B_NAME'  => $search_value
            );
        }
        $length = 10; $start = 1;
        if($_POST['length'] != -1){
            $length = $_POST['length'];
            $start = $_POST['start'];
        }
        $cheque_search_list = $this->accounts_model->cheque_searching_list($search_array,$length,$start);
        $cheque_search_list_rows = $this->accounts_model->cheque_list_rows($search_array);

        foreach($cheque_search_list as $cheque){
            $client = ($cheque->CLIENT_ID == "")?'':" (".client_name($cheque->CLIENT_ID).")";
            $action = '<a target="_blank" href="'.base_url('admin/accounts/view_cheque/'.$cheque->C_ID).'" type="button" data-popup="tooltip" title="" data-container="body" data-original-title="View Cheque" class="btn btn-default btn-xs"><i class="fa fa-print"></i></a>
                       <button type="button" onclick="getEditCheque(this.value)" value="'.$cheque->C_ID.'" data-popup="tooltip" data-container="body" data-original-title="Edit Cheque" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#" ><i class="fa fa-edit"></i></button>';
            $rows[] = array(
                $cheque->B_NAME,
                $cheque->BR_NAME,
                $cheque->A_NAME.$client,
                $cheque->C_PAY,
                number_format($cheque->C_AMOUNT,2),
                date('d-m-Y',strtotime($cheque->C_DATE)),
                $cheque->C_STATUS,
                UCFIRST($cheque->username),
                $action
            );
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $cheque_search_list_rows,
            "recordsFiltered" => $cheque_search_list_rows,
            "data" => $rows
        );
        echo json_encode($output);
    }

    public function add_cheque()
    {
        if($this->input->post()){
            #--------Insert Data in Transaction ----#
            $this->accounts_model->create_transaction(array(
                'T_DATE'=>$this->input->post('ch_date'),
                'T_PAY' => $this->input->post('ch_user'),
                'T_TYPE'=>"cheque",
                'CREATED_BY'=> $this->session->userdata('user_id')
            ));
            $tr_id=$this->db->insert_id();
            $tr_data=array();
            $tr_data[]=array(
                'T_ID'          => $tr_id,
                'A_ID'          => $this->input->post('debit_account'),
                'PARTICULARS'   => $this->input->post('ch_desc'),
                'TM_AMOUNT'     => $this->input->post('ch_amount'),
                'TM_TYPE'       => "Debit",
                'IS_ACTIVE'     => 1
            );
            $tr_data[]=array(
                'T_ID'          => $tr_id,
                'A_ID'          => $this->input->post('account_id'),
                'PARTICULARS'   => $this->input->post('ch_desc'),
                'TM_AMOUNT'     => $this->input->post('ch_amount'),
                'TM_TYPE'       => "Credit",
                'IS_ACTIVE'     => 1
            );

            $this->accounts_model->create_transaction_meta($tr_data);

            #--------Insert Data in Cheque ----#
            $postData=array(
                'BR_ID'         => $this->input->post('branch_name'),
                'DB_ACCOUNTID'  => $this->input->post('debit_account'),
                'C_PAY'         => $this->input->post('ch_user'),
                'C_NO'          => $this->input->post('ch_no'),
                'C_AMOUNT'      => $this->input->post('ch_amount'),
                'C_AMOUNTWORDS' => $this->input->post('ch_amountWords'),
                'C_DATE'        => $this->input->post('ch_date'),
                'C_DESC'        => $this->input->post('ch_desc'),
                'T_ID'          => $tr_id,
                'AC_PAYEES'     => $this->input->post('ch_payees'),
                'C_STATUS'      => "Approved",
                'CREATED_BY'    => $this->session->userdata('user_id')
            );
            $this->invoice_model->_table_name = 'cheques';
            $this->invoice_model->_primary_key = 'C_ID';
            $id = $this->invoice_model->save($postData);
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'bank & cash',
                'module_field_id' => $id,
                'activity' => lang('activity_added_cheque'),
                'icon' => 'fa-usd',
                'value1' => "PKR ".$_POST['ch_amount'],
            );
            $this->invoice_model->_table_name = 'tbl_activities';
            $this->invoice_model->_primary_key = 'activities_id';
            $this->invoice_model->save($activity);
            $type = "success";
            $message = lang('cheque_added');
            set_message($type, $message);
            redirect('admin/accounts/cheques');
        }
    }

    public function view_cheque($cheque_id)
    {
        $data['cheque']=$this->accounts_model->select_edit_cheque($cheque_id);
        $this->load->view('admin/accounts/view_cheque',$data);
    }

    public function ajax_edit_cheque($cheque_id)
    {
        $result=$this->accounts_model->select_edit_cheque($cheque_id);
        $branches=$this->accounts_model->read_branch_where($result->B_ID);
        $output =array(
            'ch_data'   => $result,
            'branch'    => $branches
        );
        echo json_encode($output);
		die();
    }

    public function update_cheque_status()
    {
        if($this->input->post()){
            $tr_id=$this->input->post('tr_id');
            if($tr_id==0){
                if($this->input->post('ch_status')=='Approved'){
                    #--------Insert Data in Transaction ----#
                    $this->accounts_model->create_transaction(array(
                        'T_DATE'=>$this->input->post('ch_date'),
                        'T_PAY' => $this->input->post('ch_user'),
                        'T_TYPE'=>"cheque",
                        'CREATED_BY'=> $this->session->userdata('user_id')
                    ));
                    $tr_id+=$this->db->insert_id();
                    $tr_data=array();
                    $tr_data[]=array(
                        'T_ID'          => $tr_id,
                        'A_ID'          => $this->input->post('debit_account'),
                        'PARTICULARS'   => $this->input->post('ch_desc'),
                        'TM_AMOUNT'     => $this->input->post('ch_amount'),
                        'TM_TYPE'       => "Debit",
                        'IS_ACTIVE'     => 1
                    );
                    $tr_data[]=array(
                        'T_ID'          => $tr_id,
                        'A_ID'          => $this->input->post('account_id'),
                        'PARTICULARS'   => $this->input->post('ch_desc'),
                        'TM_AMOUNT'     => $this->input->post('ch_amount'),
                        'TM_TYPE'       => "Credit",
                        'IS_ACTIVE'     => 1
                    );
                    $this->accounts_model->create_transaction_meta($tr_data);
                }
            }elseif($tr_id!=''){
                if($this->input->post('ch_status')=='Approved') {
                    #-------Update Transaction Data------#
                    $this->accounts_model->update_transaction(array(
                        'T_ID' => $tr_id,
                        'T_DATE' => $this->input->post('ch_date'),
                        'T_PAY' => $this->input->post('ch_user'),
                        'UPDATED_BY'     => $this->session->userdata('user_id')
                    ));
                    $tr_data_debit = array(
                        'T_ID' => $this->input->post('tr_id'),
                        'A_ID' => $this->input->post('debit_account'),
                        'PARTICULARS' => $this->input->post('ch_desc'),
                        'TM_AMOUNT' => $this->input->post('ch_amount'),
                        'TM_TYPE' => "Debit",
                        'IS_ACTIVE' => 1
                    );
                    $this->accounts_model->update_transaction_meta($tr_data_debit);
                    $tr_data_credit = array(
                        'T_ID' => $this->input->post('tr_id'),
                        'A_ID' => $this->input->post('account_id'),
                        'PARTICULARS' => $this->input->post('ch_desc'),
                        'TM_AMOUNT' => $this->input->post('ch_amount'),
                        'TM_TYPE' => "Credit",
                        'IS_ACTIVE' => 1
                    );
                    $this->accounts_model->update_transaction_meta($tr_data_credit);
                }elseif($this->input->post('ch_status')=='Cancel'){
                    $this->accounts_model->delete_transaction($tr_id);
                    $tr_id =0;
                }
            }

            #-------Update Cheques Data------#
            $postData=array(
                'C_ID'          => $this->input->post('ch_id'),
                'BR_ID'         => $this->input->post('branch_name'),
                'C_PAY'         => $this->input->post('ch_user'),
                'DB_ACCOUNTID'  => $this->input->post('debit_account'),
                'C_NO'          => $this->input->post('ch_no'),
                'C_AMOUNT'      => $this->input->post('ch_amount'),
                'C_AMOUNTWORDS' => $this->input->post('ch_amountWords'),
                'C_DATE'        => $this->input->post('ch_date'),
                'C_DESC'        => $this->input->post('ch_desc'),
                'T_ID'          => $tr_id,
                'AC_PAYEES'     => $this->input->post('ch_payees'),
                'C_STATUS'      => $this->input->post('ch_status'),
                'UPDATED_BY'     => $this->session->userdata('user_id')
            );
            if($this->accounts_model->update_cheque($postData)) {
                $activity = array(
                    'user' => $this->session->userdata('user_id'),
                    'module' => 'bank & cash',
                    'module_field_id' => $_POST['ch_id'],
                    'activity' => lang('activity_updated_cheque'),
                    'icon' => 'fa-usd',
                    'value1' => "PKR ".$_POST['ch_amount'],
                );
                $this->invoice_model->_table_name = 'tbl_activities';
                $this->invoice_model->_primary_key = 'activities_id';
                $this->invoice_model->save($activity);
                $type = "success";
                $message = lang('cheque_updated');
                set_message($type, $message);
                redirect('admin/accounts/cheques');
            }
            else{
                $type = "error";
                $message = 'OOPS! Something went wrong';
                set_message($type, $message);
                redirect('admin/accounts/cheques');
            }
        }
    }

    public function delete_cheque($cheque_id)
    {
        $result=$this->accounts_model->select_edit_cheque($cheque_id);
        $this->accounts_model->delete_transaction($result->T_ID);
        $status=$this->accounts_model->delete_cheque($cheque_id);
        echo json_encode($status);
		die();
    }
    #---------------End Cheque Section-------------------#

    /*** PAYMENT RECEIVED ***/
    public function pre_job_advance_list()
    {
        $this->invoice_model->_table_name = 'tbl_advance_payments';
        $this->invoice_model->_order_by = 'ap_id';
        $data['advances'] = $this->invoice_model->get();
        $data['subview'] = $this->load->view('admin/accounts/pre_job_advance_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function manage_pre_job_advance($action = NULL, $id = NULL) {
        $id = (!empty($id))?decode($id):NULL;
        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'client_id';
        $data['all_clients'] = $this->invoice_model->get();
        $this->db->select('*');
        $this->db->from('tbl_invoices');
        $this->db->order_by('created_date', 'DESC');
        $data['all_invoices'] = $this->db->get()->result();
        $data['banks'] = $this->accounts_model->read_bank_where('B_STATUS',1);
        $data['accounts']= $this->accounts_model->ajax_select_accounts(array('A_STATUS'=>1));
        if($action == 'edit'){
            $data['advance_info'] = $this->invoice_model->check_by(array('ap_id'=>$id), 'tbl_advance_payments');
            $data['advance_details'] = $this->accounts_model->job_advances_details_by_id($id);
            $data['transfer_amount'] = $this->accounts_model->transfer_details_by_paymentId($id);
            $subview = "edit_pre_job_advance";
        }
        elseif($action == 'view'){
            $data['advance_info'] = $this->accounts_model->pre_job_advances_by_id($id);
            $data['advance_details'] = $this->accounts_model->job_advances_details_by_id($id);
            $data['transfer_detail'] = $this->accounts_model->transfer_details_by_paymentId($id);
            $subview = "advance_details";
        }
        else{
            $subview = "manage_pre_job_advance";
        }
        $data['title'] = lang('create_pre_job_advances');
        $data['subview'] = 'admin/accounts/'.$subview;
        $this->load->view('admin/_layout_main2', $data);
    }
    
    public function payment_by_invoice_client(){
        $exp_type=$this->input->post('exp_type');
        $client=$this->input->post('client');
        $invoice=$this->input->post('invoice');
        if ($exp_type=='Job'){
            $where_array=array(
                'expense_type'  => $exp_type,
                'invoices_id'   => $invoice
            );
        }elseif ($exp_type=='Misc'){
            $where_array=array(
                'expense_type'  => $exp_type,
                'client_id'     => $client
            );
        }
        $advances=$this->invoice_model->check_by_all($where_array,'tbl_advance_payments');
        $rows=array();
        if(!empty($advances)){
            foreach ($advances as $advance){
                if ($advance->expense_type == 'Job') {
                    $job_info = $this->invoice_model->check_by(array('invoices_id' => $advance->invoices_id), 'tbl_invoices');
                    $client_info = $this->invoice_model->check_by(array('client_id' => $job_info->client_id), 'tbl_client');
                    $job = $this->invoice_model->job_no_creation($advance->invoices_id);
                    $client = $client_info->name;
                } else {
                    $job = "--";
                    $client_info = $this->invoice_model->check_by(array('client_id' => $advance->client_id), 'tbl_client');
                    $client = $client_info->name;
                }
                $branch_info = $this->invoice_model->check_by(array('A_ID' => $advance->debit_account), 'branches');
                if (!empty($branch_info)) {
                    $bank_info = $this->invoice_model->check_by(array('B_ID' => $branch_info->B_ID), 'banks');
                }
                $rows[] = '<tr style="white-space: nowrap;"><td>'.date('d-m-Y',strtotime($advance->created_date)).'</td><td>'.date('h:i:s A',strtotime($advance->created_date)).'</td><td>'.date('d-m-Y',strtotime($advance->payment_date)).'</td><td>'.$job.'</td><td>'.$client.'</td><td>'.$advance->payment_method.'</td><td>'.(($advance->payment_method == "Cash" || $advance->payment_method == "Third Party")?'--':$bank_info->B_NAME).'</td><td>'.(($advance->payment_method == "Cash" || $advance->payment_method == "Third Party")?'--':$branch_info->BR_NAME).'</td><td>'.number_format($advance->amount,2).'</td></tr>';
            }
        }
        echo json_encode($rows);
    }

    public function save_pre_job_advance($id = NULL)
    {
        if(!empty($id)) {
            $expense_id = decrypt($id);
            $expense_type = $this->input->post('expense_type');
            $tr_id = $this->input->post('transaction_id');
            $this->invoice_model->_table_name = 'transactions_meta';
            $this->invoice_model->delete_multiple(array('T_ID'=>$tr_id));
            $this->invoice_model->_table_name = 'tbl_advance_payment_details';
            $this->invoice_model->delete_multiple(array('apd_id'=>$expense_id));
            if($expense_type == 'Misc'){
                $this->db->trans_begin();
                $total_amount = $this->input->post('misc_amount');
                $client_id=$this->input->post('client_id');
                $t_data=array(
                    'T_DATE'=>$this->input->post('misc_date'),
                    'UPDATED_BY'=>$this->session->userdata('user_id'),
                );
                $this->invoice_model->_table_name = 'transactions';
                $this->invoice_model->_primary_key = 'T_ID';
                $this->invoice_model->save($t_data, $tr_id);
                if($this->input->post('misc_payment_type') == "Cash"){
                    $account_id = 1;
                }
                else{
                    $account_id = $this->input->post('misc_account_id');
                }

                $ap_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Liabilities', 'NAME' => 'Current Liabilities'), 'sub_heads');
                $ap_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_head->SUB_HEAD_ID, 'H_NAME' => 'Advances from clients'), 'accounts_head');
                $advances_accounts = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_sub_head->SUB_HEAD_ID, 'H_ID' => $ap_sub_head->H_ID, 'CLIENT_ID' => $client_id, 'A_NAME'=>client_name($client_id).' (Advances from clients)'), 'accounts');

                $data = array(
                    'expense_type'      => $expense_type,
                    'client_id'         => $client_id,
                    'invoices_id'       => NULL,
                    'debit_account'     => $account_id,
                    'credit_account'    => $advances_accounts->A_ID,
                    'payment_method'    => $this->input->post('misc_payment_type'),
                    'instrument_type'   => $this->input->post('misc_instrument_type'),
                    'cheque_payorder'   => $this->input->post('misc_pay_order'),
                    'description'       => $this->input->post('misc_desc'),
                    'amount'            => $total_amount,
                    'payment_date'      => $this->input->post('misc_date'),
                    'transaction_id'    => $tr_id,
                    'updated_by'        => $this->session->userdata('user_id')
                );

                $this->invoice_model->_table_name = 'tbl_advance_payments';
                $this->invoice_model->_primary_key = 'ap_id';
                $this->invoice_model->save($data, $expense_id);

                $this->invoice_model->_table_name = 'transactions_meta';
                $this->invoice_model->delete_multiple(array('T_ID'=>$tr_id));

                $tr_data = array();
                $tr_data[] = array(
                    'T_ID'          => $tr_id,
                    'A_ID'          => $advances_accounts->A_ID,
                    'PARTICULARS'   => $this->input->post('misc_desc'),
                    'TM_AMOUNT'     => $total_amount,
                    'TM_TYPE'       => "Credit",
                    'IS_ACTIVE'     => 1
                );
                $tr_data[]=array(
                    'T_ID'          => $tr_id,
                    'A_ID'          => $account_id,
                    'PARTICULARS'   => $this->input->post('misc_desc'),
                    'TM_AMOUNT'     => $total_amount,
                    'TM_TYPE'       => "Debit",
                    'IS_ACTIVE'     => 1
                );
                $this->accounts_model->create_transaction_meta($tr_data);
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                }
            }else {
                $this->db->trans_begin();
                $total_amount = $this->input->post('amount');
                $t_data=array(
                    'T_DATE'=>$this->input->post('date'),
                    'UPDATED_BY'=>$this->session->userdata('user_id')
                );
                $this->invoice_model->_table_name = 'transactions';
                $this->invoice_model->_primary_key = 'T_ID';
                $this->invoice_model->save($t_data, $tr_id);

                $client = $this->invoice_model->check_by(array('invoices_id' => $_POST['invoice_id']), 'tbl_invoices');
                $ap_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Liabilities', 'NAME' => 'Current Liabilities'), 'sub_heads');
                $ap_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_head->SUB_HEAD_ID, 'H_NAME' => 'Advances from clients'), 'accounts_head');
                $advances_accounts = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_sub_head->SUB_HEAD_ID, 'H_ID' => $ap_sub_head->H_ID, 'CLIENT_ID' => $client->client_id, 'A_NAME' =>client_name($client->client_id).' (Advances from clients)'), 'accounts');
                
                if ($this->input->post('payment_type') == "Cash") {
                    $account_id = 1;
                } else {
                    $account_id = $this->input->post('account_id');
                }

                $payment_type = $this->input->post('payment_type');
                if ($payment_type == 'Cash' || $payment_type == 'Bank') {
                    $data = array(
                        'expense_type'      => $expense_type,
                        'client_id'         => NULL,
                        'invoices_id'       => $this->input->post('invoice_id'),
                        'debit_account'     => $account_id,
                        'credit_account'    => $advances_accounts->A_ID,
                        'payment_method'    => $payment_type,
                        'instrument_type'   => $this->input->post('instrument_type'),
                        'cheque_payorder'   => $this->input->post('cheque_payorder'),
                        'description'       => $this->input->post('desc'),
                        'amount'            => $total_amount,
                        'payment_date'      => $this->input->post('date'),
                        'transaction_id'    => $tr_id,
                        'updated_by'    => $this->session->userdata('user_id')
                    );

                    $this->invoice_model->_table_name = 'tbl_advance_payments';
                    $this->invoice_model->_primary_key = 'ap_id';
                    $this->invoice_model->save($data, $expense_id);

                    $this->invoice_model->_table_name = 'transactions_meta';
                    $this->invoice_model->delete_multiple(array('T_ID'=>$tr_id));

                    $tr_data = array();
                    $tr_data[] = array(
                        'T_ID' => $tr_id,
                        'A_ID' => $advances_accounts->A_ID,
                        'PARTICULARS' => $this->input->post('desc'),
                        'TM_AMOUNT' => $total_amount,
                        'TM_TYPE' => "Credit",
                        'IS_ACTIVE' => 1
                    );
                    $tr_data[] = array(
                        'T_ID' => $tr_id,
                        'A_ID' => $account_id,
                        'PARTICULARS' => $this->input->post('desc'),
                        'TM_AMOUNT' => $total_amount,
                        'TM_TYPE' => "Debit",
                        'IS_ACTIVE' => 1
                    );

                    $this->accounts_model->create_transaction_meta($tr_data);
                } else {
                    $title = $this->input->post('pay_order_title');
                    $amount = $this->input->post('pay_order_amount');
                    $pay_order = $this->input->post('pay_order_no');
                    $total_amount = 0;
                    if ($payment_type == 'Third Party') {
                        for ($x = 0; $x < count($title); $x++) {
                            $total_amount += $amount[$x];
                        }
                    } else {
                        $total_amount = $this->input->post('amount');
                    }
                    $data = array(
                        'expense_type' => $expense_type,
                        'client_id'     => NULL,
                        'invoices_id' => $this->input->post('invoice_id'),
                        'debit_account' => NULL,
                        'credit_account' => $advances_accounts->A_ID,
                        'payment_method' => $payment_type,
                        'description' => $this->input->post('desc'),
                        'amount' => $total_amount,
                        'payment_date' => $this->input->post('date'),
                        'transaction_id' => $tr_id,
                        'updated_by' => $this->session->userdata('user_id')
                    );

                    $this->invoice_model->_table_name = 'tbl_advance_payments';
                    $this->invoice_model->_primary_key = 'ap_id';
                    $this->invoice_model->save($data,$expense_id);

                    if (!empty($title)) {
                        for ($x = 0; $x < count($title); $x++) {
                            $data = array(
                                'ap_id' => $expense_id,
                                'invoices_id' => $this->input->post('invoice_id'),
                                'apd_amount' => $amount[$x],
                                'apd_title' => $title[$x],
                                'apd_pay_order' => $pay_order[$x]
                            );
                            $this->invoice_model->_table_name = 'tbl_advance_payment_details';
                            $this->invoice_model->_primary_key = 'apd_id';
                            $this->invoice_model->save($data);
                        }
                    }
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                }
            }

            $action = lang('activity_added_payment_received');
            $msg = lang('payment_received_updated');
        }
        else{
            $expense_type = $this->input->post('expense_type');
            if($expense_type == 'Misc'){
                $total_amount = $this->input->post('misc_amount');
                $client_id=$this->input->post('client_id');
                $this->accounts_model->create_transaction(array(
                    'T_DATE'=>$this->input->post('misc_date'),
                    'T_TYPE'=>"Payment Receipt - Misc",
                    'CREATED_BY'=>$this->session->userdata('user_id')
                ));
                $tr_id = $this->db->insert_id();
                if($this->input->post('misc_payment_type') == "Cash"){
                    $account_id = 1;
                }
                else{
                    $account_id = $this->input->post('misc_account_id');
                }

                $ap_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Liabilities', 'NAME' => 'Current Liabilities'), 'sub_heads');
                $ap_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_head->SUB_HEAD_ID, 'H_NAME' => 'Advances from clients'), 'accounts_head');
                $advances_accounts = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_sub_head->SUB_HEAD_ID, 'H_ID' => $ap_sub_head->H_ID, 'CLIENT_ID' => $client_id, 'A_NAME'=>client_name($client_id).' (Advances from clients)'), 'accounts');

                $data = array(
                    'expense_type'      => $expense_type,
                    'client_id'         => $client_id,
                    'invoices_id'         => NULL,
                    'debit_account'     => $account_id,
                    'credit_account'    => $advances_accounts->A_ID,
                    'payment_method'    => $this->input->post('misc_payment_type'),
                    'cheque_payorder'   => $this->input->post('misc_pay_order'),
                    'instrument_type'   => $this->input->post('misc_instrument_type'),
                    'description'       => $this->input->post('misc_desc'),
                    'amount'            => $total_amount,
                    'payment_date'      => $this->input->post('misc_date'),
                    'transaction_id'    => $tr_id,
                    'created_by'    => $this->session->userdata('user_id')
                );

                $this->invoice_model->_table_name = 'tbl_advance_payments';
                $this->invoice_model->_primary_key = 'ap_id';
                $expense_id = $this->invoice_model->save($data);

                $tr_data = array();
                $tr_data[] = array(
                    'T_ID'          => $tr_id,
                    'A_ID'          => $advances_accounts->A_ID,
                    'PARTICULARS'   => $this->input->post('misc_desc'),
                    'TM_AMOUNT'     => $this->input->post('misc_amount'),
                    'TM_TYPE'       => "Credit",
                    'IS_ACTIVE'     => 1
                );
                $tr_data[]=array(
                    'T_ID'          => $tr_id,
                    'A_ID'          => $account_id,
                    'PARTICULARS'   => $this->input->post('misc_desc'),
                    'TM_AMOUNT'     => $this->input->post('misc_amount'),
                    'TM_TYPE'       => "Debit",
                    'IS_ACTIVE'     => 1
                );
                $this->accounts_model->create_transaction_meta($tr_data);
            }else{
                $this->accounts_model->create_transaction(array(
                    'T_DATE'=>$this->input->post('date'),
                    'T_TYPE'=>"Payment Receipt - Job",
                    'CREATED_BY'=>$this->session->userdata('user_id')
                ));
                $tr_id = $this->db->insert_id();

                $client = $this->invoice_model->check_by(array('invoices_id'=>$_POST['invoice_id']), 'tbl_invoices');
                $ap_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Liabilities', 'NAME' => 'Current Liabilities'), 'sub_heads');
                $ap_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_head->SUB_HEAD_ID, 'H_NAME' => 'Advances from clients'), 'accounts_head');
                $advances_accounts = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_sub_head->SUB_HEAD_ID, 'H_ID' => $ap_sub_head->H_ID, 'CLIENT_ID' => $client->client_id, 'A_NAME'=>client_name($client->client_id).' (Advances from clients)'), 'accounts');

                if($this->input->post('payment_type') == "Cash"){
                    $account_id = 1;
                } else{
                    $account_id = $this->input->post('account_id');
                }
    
                $total_amount = $this->input->post('amount');
                $payment_type = $this->input->post('payment_type');
                if($payment_type == 'Cash' || $payment_type == 'Bank'){
                    $data = array(
                        'expense_type'      => $expense_type,
                        'client_id'       => NULL,
                        'invoices_id'       => $this->input->post('invoice_id'),
                        'debit_account'     => $account_id,
                        'credit_account'    => $advances_accounts->A_ID,
                        'payment_method'    => $payment_type,
                        'description'       => $this->input->post('desc'),
                        'amount'            => $total_amount,
                        'payment_date'      => $this->input->post('date'),
                        'instrument_type'   => $this->input->post('instrument_type'),
                        'cheque_payorder'   => $this->input->post('cheque_payorder'),
                        'transaction_id'    => $tr_id,
                        'created_by'    => $this->session->userdata('user_id')
                    );

                    $this->invoice_model->_table_name = 'tbl_advance_payments';
                    $this->invoice_model->_primary_key = 'ap_id';
                    $expense_id = $this->invoice_model->save($data);

                    $tr_data = array();
                    $tr_data[] = array(
                        'T_ID'          => $tr_id,
                        'A_ID'          => $advances_accounts->A_ID,
                        'PARTICULARS'   => $this->input->post('desc'),
                        'TM_AMOUNT'     => $this->input->post('amount'),
                        'TM_TYPE'       => "Credit",
                        'IS_ACTIVE'     => 1
                    );
                    $tr_data[]=array(
                        'T_ID'          => $tr_id,
                        'A_ID'          => $account_id,
                        'PARTICULARS'   => $this->input->post('desc'),
                        'TM_AMOUNT'     => $this->input->post('amount'),
                        'TM_TYPE'       => "Debit",
                        'IS_ACTIVE'     => 1
                    );
                    $this->accounts_model->create_transaction_meta($tr_data);
                }else{
                    $title = $this->input->post('pay_order_title');
                    $amount = $this->input->post('pay_order_amount');
                    $pay_order = $this->input->post('pay_order_no');
                    $total_amount = 0;
                    if($payment_type == 'Third Party'){
                        for ($x = 0; $x < count($title); $x++) {
                            $total_amount += $amount[$x];
                        }
                    }else{
                        $total_amount = $this->input->post('amount');
                    }
                    $data = array(
                        'expense_type'      => $expense_type,
                        'client_id'       => NULL,
                        'invoices_id'       => $this->input->post('invoice_id'),
                        'debit_account'     => NULL,
                        'credit_account'    => $advances_accounts->A_ID,
                        'payment_method'    => $payment_type,
                        'description'       => $this->input->post('desc'),
                        'amount'            => $total_amount,
                        'payment_date'      => $this->input->post('date'),
                        'transaction_id'    => $tr_id,
                        'created_by'    => $this->session->userdata('user_id')
                    );

                    $this->invoice_model->_table_name = 'tbl_advance_payments';
                    $this->invoice_model->_primary_key = 'ap_id';
                    $expense_id = $this->invoice_model->save($data);

                    if (!empty($title)) {
                        for ($x = 0; $x < count($title); $x++) {
                            $data = array(
                                'ap_id'         => $expense_id,
                                'invoices_id'   => $this->input->post('invoice_id'),
                                'apd_amount'    => $amount[$x],
                                'apd_title'     => $title[$x],
                                'apd_pay_order' => $pay_order[$x]
                            );
                            $this->invoice_model->_table_name = 'tbl_advance_payment_details';
                            $this->invoice_model->_primary_key = 'apd_id';
                            $this->invoice_model->save($data);
                        }
                    }
                }
            }

            $action = lang('activity_added_payment_received');
            $msg = lang('payment_received_added');
        }
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'entry book',
            'module_field_id' => $expense_id,
            'activity' => $action."(".$expense_type." expense)",
            'icon' => 'fa-circle-o',
            'value1' => "PKR ".$total_amount
        );
        $this->invoice_model->_table_name = 'tbl_activities';
        $this->invoice_model->_primary_key = 'activities_id';
        $this->invoice_model->save($activity);

        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/accounts/pre_job_advance_list');
    }
    /*** END PAYMENT RECEIVED ***/

    /*** JOB EXPENSES ***/
    public function job_expenses_list()
    {
        $data['vouchers'] = $this->invoice_model->check_by_all(array('voucher_type'=>'Exp'), 'tbl_vouchers');
        $data['accounts']= $this->accounts_model->ajax_select_accounts(array('A_STATUS'=>1));
        $data['subview'] = $this->load->view('admin/accounts/job_expenses_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function job_expenses()
    {
        $search_array=array();
        $rows = array();
        $search_value=$_POST['search']['value'];
        if ($search_value){
            $search_array=array(
                'PC_VOUCHER_NO'  => $search_value
            );
        }
        $length = 10; $start = 1;
        if($_POST['length'] != -1){
            $length = $_POST['length'];
            $start = $_POST['start'];
        }
        $job_voucher_list = $this->accounts_model->job_voucher_list($search_array,$length,$start);
        $job_voucher_list_rows = $this->accounts_model->job_voucher_list_rows($search_array);
        $i = $_POST['start']+1;
        foreach($job_voucher_list as $voucher){
            $jobs = "";$titles = "";$amounts = ""; $invoices_no="";
                $jobs = job_info($voucher->invoices_id)->reference_no."<br />".$jobs;
                if ($voucher->job_expense_id != 0) {
                    $titles = job_expense_title($voucher->job_expense_id) . "<br />" . $titles;
                }else{
                    $titles= 'Security Deposit';
                }
                $invoices_no=$this->invoice_model->job_no_creation($voucher->invoices_id) . "<br />" . $invoices_no;
                $amounts = number_format($voucher->amount,2)."<br />".$amounts;
            $rows[] = array(
                $voucher->voucher_no,
                $invoices_no,
                date('d-m-Y',strtotime($voucher->created_date)),
                $titles,
                date('d-m-Y',strtotime($voucher->payment_date)),
                ($voucher->payment_method == 1)?'Cash':'Bank',
                $amounts,
                btn_view('admin/accounts/manage_job_expense/voucher_detail/'.$voucher->voucher_id)
            );
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $job_voucher_list_rows,
            "recordsFiltered" => $job_voucher_list_rows,
            "data" => $rows
        );
        echo json_encode($output);
    }

    public function manage_job_expense($action = NULL, $id = NULL) {
        $this->db->select('*');
        $this->db->from('tbl_invoices');

        $this->db->order_by('created_date', 'DESC');
        $data['all_invoices'] = $this->db->get()->result();
        $data['banks'] = $this->accounts_model->read_bank_where('B_STATUS',1);
        $data['vouchers'] = $this->accounts_model->read_expense();
        $data['accounts']= $this->accounts_model->ajax_select_accounts(array('A_STATUS'=>1));
        $this->invoice_model->_table_name = 'tbl_staff';
        $this->invoice_model->_order_by = 'staff_id';
        $data['all_staffs'] = $this->invoice_model->get();
        $this->invoice_model->_table_name = 'tbl_job_expenses';
        $data['job_expenses'] = $this->invoice_model->get();

        if($action == 'edit_voucher'){
            $this->db->select('*');
            $this->db->from('tbl_vouchers');
            $this->db->where('voucher_id', $id);
            $data['voucher_info'] = $this->db->get()->row();
            $subview = "manage_job_expense";
        }
        elseif($action == 'voucher_detail'){
            $data['voucher'] = $this->invoice_model->check_by(array('voucher_id'=>$id), 'tbl_vouchers');
            $subview = "job_expenses";
        }
        else{
            $subview = "manage_job_expense";
        }
        $data['title'] = "Job Expenses";
        $data['subview'] = 'admin/accounts/'.$subview;
        $this->load->view('admin/_layout_main2', $data);
    }

    public function save_job_expense($id = NULL){

        if(!empty($id)){
            $voucher_id = $id;
            $invoices_id = $this->input->post('invoice_id');
            $debit_account=array();
            $this->db->trans_begin();
            $tr_id=$this->input->post('transaction_id');
            $tr_id1=$this->input->post('transaction_id1');
            $t_data = array(
                'T_PAY'=>'',
                'T_DATE'=>$this->input->post('date')
            );

            $this->invoice_model->_table_name = 'transactions';
            $this->invoice_model->_primary_key = 'T_ID';
            $this->invoice_model->save($t_data, $tr_id);

            $this->invoice_model->_table_name = 'transactions_meta';
            $this->invoice_model->delete_multiple(array('T_ID'=>$tr_id));

            $account_id = NULL;
            $staff_account=$this->input->post('staff_account');
            $amount=$this->input->post('amount');
            $total_amount=array_sum($amount);
            if($this->input->post('payment_type') == 1){
                $account_id = $this->input->post('payment_type');
                $tr_data[]=array(
                    'T_ID'          => $tr_id,
                    'A_ID'          => $this->input->post('payment_type'),
                    'PARTICULARS'   => $this->input->post('desc'),
                    'TM_AMOUNT'     => $total_amount,
                    'TM_TYPE'       => "Credit",
                    'IS_ACTIVE'     => 1
                );

                $tr_data[] = array(
                    'T_ID'          => $tr_id,
                    'A_ID'          => $staff_account,
                    'PARTICULARS'   => $this->input->post('desc'),
                    'TM_AMOUNT'     => $total_amount,
                    'TM_TYPE'       => "Debit",
                    'IS_ACTIVE'     => 1
                );
                $this->accounts_model->create_transaction_meta($tr_data);
                if($tr_id1!=0){
                    $this->invoice_model->_table_name = 'transactions';
                    $this->invoice_model->_primary_key = 'T_ID';
                    $this->invoice_model->save($t_data, $tr_id1);

                    $this->invoice_model->_table_name = 'transactions_meta';
                    $this->invoice_model->delete_multiple(array('T_ID'=>$tr_id1));
                }elseif ($tr_id1==0){
                    $this->accounts_model->create_transaction(array(
                        'T_DATE'=>$this->input->post('date'),
                        'T_PAY' => '',
                        'T_TYPE'=>"Job Expenses"
                    ));
                    $tr_id1 = $this->db->insert_id();
                }

                $tr_data1[] = array(
                    'T_ID'          => $tr_id1,
                    'A_ID'          => $staff_account,
                    'PARTICULARS'   => $this->input->post('desc'),
                    'TM_AMOUNT'     => $total_amount,
                    'TM_TYPE'       => "Credit",
                    'IS_ACTIVE'     => 1
                );

                $expense_account=$this->input->post('debit_account');
                for($k=0;$k<count($amount);$k++){
                    $client = $this->invoice_model->check_by(array('invoices_id'=>$invoices_id[$k]), 'tbl_invoices');
                    $ap_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Expense', 'NAME' => 'Cost of Sales'), 'sub_heads');
                    $ap_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_head->SUB_HEAD_ID, 'H_NAME' => 'Client Expenses'), 'accounts_head');
                    $payable_account = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_sub_head->SUB_HEAD_ID, 'H_ID' => $ap_sub_head->H_ID, 'CLIENT_ID' => $client->client_id, 'A_NAME'=>client_name($client->client_id).' (Client Expenses)'), 'accounts');

                    $debit_account[]=$payable_account->A_ID;
                    $tr_data1[] = array(
                        'T_ID'          => $tr_id1,
                        'A_ID'          => $payable_account->A_ID,
                        'PARTICULARS'   => $this->input->post('desc'),
                        'TM_AMOUNT'     => $amount[$k],
                        'TM_TYPE'       => "Debit",
                        'IS_ACTIVE'     => 1
                    );
                }
                $this->accounts_model->create_transaction_meta($tr_data1);
            } else{
                $account_id = $this->input->post('account_id');
                $tr_data[]=array(
                    'T_ID'          => $tr_id,
                    'A_ID'          => $account_id,
                    'PARTICULARS'   => $this->input->post('desc'),
                    'TM_AMOUNT'     => $total_amount,
                    'TM_TYPE'       => "Credit",
                    'IS_ACTIVE'     => 1
                );

                for($k=0;$k<count($amount);$k++){
                    $client = $this->invoice_model->check_by(array('invoices_id'=>$invoices_id[$k]), 'tbl_invoices');
                    $ap_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Liabilities', 'NAME' => 'Current Liabilities'), 'sub_heads');
                    $ap_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_head->SUB_HEAD_ID, 'H_NAME' => 'Account Payables'), 'accounts_head');
                    $payable_account = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_sub_head->SUB_HEAD_ID, 'H_ID' => $ap_sub_head->H_ID, 'CLIENT_ID' => $client->client_id, 'A_NAME'=>client_name($client->client_id).' (Account Payables)'), 'accounts');
                    $debit_account[]=$payable_account->A_ID;
                    $tr_data[] = array(
                        'T_ID'          => $tr_id,
                        'A_ID'          => $payable_account->A_ID,
                        'PARTICULARS'   => $this->input->post('desc'),
                        'TM_AMOUNT'     => $amount[$k],
                        'TM_TYPE'       => "Debit",
                        'IS_ACTIVE'     => 1
                    );
                }
                $this->accounts_model->create_transaction_meta($tr_data);

                if($tr_id1!=0){
                    $this->invoice_model->_table_name = 'transactions_meta';
                    $this->invoice_model->delete_multiple(array('T_ID'=>$tr_id1));

                    $this->invoice_model->_table_name = 'transactions';
                    $this->invoice_model->delete_multiple(array('T_ID'=>$tr_id1));

                    $tr_id1=0;
                }
            }
            $voucherData = array(
                'credit_account' => $account_id,
                'transaction_id' => $tr_id,
                'transaction_id1' => (!empty($tr_id1))?$tr_id1:0,
                'payment_method' => $this->input->post('payment_type'),
                'voucher_type' => 'Exp',
                'paid_to' => $staff_account,
                'cheque_no' => $this->input->post('chq_no'),
                'description' => $this->input->post('desc'),
                'payment_date' => $this->input->post('date'),
                'created_by'=> $this->session->userdata('user_id')
            );

            $this->invoice_model->_table_name = 'tbl_vouchers';
            $this->invoice_model->_primary_key = 'voucher_id';
            $this->invoice_model->save($voucherData, $id);

            $this->invoice_model->_table_name = 'tbl_voucher_details';
            $this->invoice_model->delete_multiple(array('voucher_id'=>$id));
            /*** TRANSACTION META ***/
            $job_expenses = $this->input->post('job_expenses');
            $amount = $this->input->post('amount');
            if (!empty($invoices_id)) {
                for($x=0; $x<count($invoices_id); $x++){
                    $voucher_detail = array(
                        'voucher_id'=>$voucher_id,
                        'invoices_id'=>$invoices_id[$x],
                        'job_expense_id'=> $job_expenses[$x],
                        'debit_account'=>$debit_account[$x],
                        'amount'=>$amount[$x]
                    );
                    $this->invoice_model->_table_name = 'tbl_voucher_details';
                    $this->invoice_model->_primary_key = 'voucher_detail_id';
                    $this->invoice_model->save($voucher_detail);
                }
            }
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            }
            /*** END TRANSACTION META ***/

            $action = lang('activity_updated_a_job_expense');
            $msg = lang('job_expense_updated');
        }else{

            #--Voucher No--#
            $last_ref = $this->accounts_model->last_voucher_no();
            $no = $last_ref->voucher_id;
            $ref_no="100".($no+1);

            $invoices_id = $this->input->post('invoice_id');
            $debit_account=array();
            $this->accounts_model->create_transaction(array(
                'T_DATE'=>$this->input->post('date'),
                'T_PAY' => '',
                'T_TYPE'=>"Job Expenses",
                'CREATED_BY'=> $this->session->userdata('user_id'),
                'CREATED_AT' => date('Y-m-d H:i:s')
            ));
            $tr_id = $this->db->insert_id();
            $tr_data = array();

            $account_id = NULL;
            $staff_account=$this->input->post('staff_account');
            $amount=$this->input->post('amount');
            $total_amount=array_sum($amount);
            if($this->input->post('payment_type') == 1){
                $account_id = $this->input->post('payment_type');
                $tr_data[] = array(
                    'T_ID'          => $tr_id,
                    'A_ID'          => $staff_account,
                    'PARTICULARS'   => $this->input->post('desc'),
                    'TM_AMOUNT'     => $total_amount,
                    'TM_TYPE'       => "Credit",
                    'IS_ACTIVE'     => 1
                );
                for($k=0;$k<count($amount);$k++){
                    $client = $this->invoice_model->check_by(array('invoices_id'=>$invoices_id[$k]), 'tbl_invoices');
                    $ap_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Expense', 'NAME' => 'Cost of Sales'), 'sub_heads');
                    $ap_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_head->SUB_HEAD_ID, 'H_NAME' => 'Client Expenses'), 'accounts_head');
                    $payable_account = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_sub_head->SUB_HEAD_ID, 'H_ID' => $ap_sub_head->H_ID, 'CLIENT_ID' => $client->client_id, 'A_NAME'=>client_name($client->client_id).' (Client Expenses)'), 'accounts');

                    $debit_account[]=$payable_account->A_ID;
                    $tr_data[] = array(
                        'T_ID'          => $tr_id,
                        'A_ID'          => $payable_account->A_ID,
                        'PARTICULARS'   => $this->input->post('desc'),
                        'TM_AMOUNT'     => $amount[$k],
                        'TM_TYPE'       => "Debit",
                        'IS_ACTIVE'     => 1
                    );
                }
                $this->accounts_model->create_transaction_meta($tr_data);
            } else{
                $account_id = $this->input->post('account_id');
                $tr_data[]=array(
                    'T_ID'          => $tr_id,
                    'A_ID'          => $account_id,
                    'PARTICULARS'   => $this->input->post('desc'),
                    'TM_AMOUNT'     => $total_amount,
                    'TM_TYPE'       => "Credit",
                    'IS_ACTIVE'     => 1
                );

                $expense_account=$this->input->post('debit_account');
                for($k=0;$k<count($amount);$k++){
                    $client = $this->invoice_model->check_by(array('invoices_id'=>$invoices_id[$k]), 'tbl_invoices');

                    $expense_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Expense', 'NAME' => 'Cost of Sales'), 'sub_heads');
                    $expense_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $expense_head->SUB_HEAD_ID, 'H_NAME' => 'Client Expenses'), 'accounts_head');
                    $expense_account = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $expense_sub_head->SUB_HEAD_ID, 'H_ID' => $expense_sub_head->H_ID, 'CLIENT_ID' => $client->client_id,'A_NAME'=> client_name($client->client_id).' (Client Expenses)'), 'accounts');

                    $debit_account[]=$expense_account->A_ID;
                    $tr_data[] = array(
                        'T_ID'          => $tr_id,
                        'A_ID'          => $expense_account->A_ID,
                        'PARTICULARS'   => $this->input->post('desc'),
                        'TM_AMOUNT'     => $amount[$k],
                        'TM_TYPE'       => "Debit",
                        'IS_ACTIVE'     => 1
                    );
                }
                $this->accounts_model->create_transaction_meta($tr_data);
            }

            $voucherData = array(
                'transaction_id' => $tr_id,
                'credit_account' => $account_id,
                'payment_method' => $this->input->post('payment_type'),
                'ref_no'         => $ref_no,
                'voucher_no'    => 'JEV'.$ref_no,
                'voucher_type' => 'Exp',
                'paid_to' => $staff_account,
                'cheque_no' => $this->input->post('chq_no'),
                'description' => $this->input->post('desc'),
                'payment_date' => $this->input->post('date'),
                'created_by'=> $this->session->userdata('user_id')
            );
            $this->invoice_model->_table_name = 'tbl_vouchers';
            $this->invoice_model->_primary_key = 'voucher_id';
            $voucher_id = $this->invoice_model->save($voucherData);

            $job_expenses = $this->input->post('job_expenses');
            $amount = $this->input->post('amount');

            if (!empty($invoices_id)) {
                for($x=0; $x<count($invoices_id); $x++){
                    $voucher_detail = array(
                        'voucher_id'=>$voucher_id,
                        'invoices_id'=>$invoices_id[$x],
                        'job_expense_id'=> $job_expenses[$x],
                        'debit_account'=>$debit_account[$x],
                        'amount'=>$amount[$x]
                    );
                    $this->invoice_model->_table_name = 'tbl_voucher_details';
                    $this->invoice_model->_primary_key = 'voucher_detail_id';
                    $this->invoice_model->save($voucher_detail);
                }
            }
            $action = 'JEV'.$ref_no.' '.lang('activity_added_a_job_expense');
            $msg = lang('job_expense_added');
        }
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'entry book',
            'module_field_id' => $voucher_id,
            'activity' => $action,
            'icon' => 'fa-circle-o',
            'value1' => "PKR ".$total_amount
        );
        $this->invoice_model->_table_name = 'tbl_activities';
        $this->invoice_model->_primary_key = 'activities_id';
        $this->invoice_model->save($activity);

        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/accounts/job_expenses_list');
    }

    public function job_expenses_by_invoice(){
        $invoices_ids=$this->input->post('invoices_ids');
        $invoices_ids=substr_replace($invoices_ids,"", -1);
        $voucher_details=$this->accounts_model->job_expenses_by_multipleIds($invoices_ids);
        $rows=array();
        if (!empty($voucher_details)) {
            foreach ($voucher_details as $vd) {
                if ($vd->job_expense_id != 0) {
                    $titles = job_expense_title($vd->job_expense_id);
                } else {
                    $titles = 'Security Deposit';
                }
                $invoices_no = $this->invoice_model->job_no_creation($vd->invoices_id);
                $amounts = number_format($vd->amount, 2);
                $rows[] = '<tr style="white-space: nowrap;"><td>' . $invoices_no . '</td><td>' . date('d-m-Y', strtotime($vd->created_date)) . '</td><td>' . $titles . '</td><td>' . date('d-m-Y', strtotime($vd->payment_date)) . '</td><td>' . (($vd->payment_method == 1) ? 'Cash' : 'Bank') . '</td><td>' . $amounts . '</td></tr>';
            }
        }
        echo json_encode($rows);
    }

    public function expenses_accounts_invoice($invoice_id){
        $result=$this->accounts_model->expenses_accounts_by_invoice($invoice_id);
        echo json_encode($result);
        die();
    }
    /*** END JOB EXPENSES ***/

    /*** STAFF LEDGER ***/
    public function staff_ledgers ()
    {
        $data['ac']=array();
        if($_POST){
            $account = $this->input->post('accounts');
            $start_date = date('Y-m-d',strtotime($this->input->post('start_date')));
            $end_date = date('Y-m-d',strtotime($this->input->post('end_date')));
            $data['start_date'] = $start_date;
            $data['end_date'] = $end_date;
            $data['staff_name']=read_subHead($account)->A_NAME;
            if($this->input->post('opening')){
                $data['ac'] = $this->accounts_model->account_categories_wise($account);
            }
            $data['general_ledgers'] = $this->accounts_model->general_ledgers_data($start_date,$end_date,$account);
        }
        $this->invoice_model->_table_name = 'tbl_staff';
        $this->invoice_model->_order_by = 'staff_id';
        $data['all_staffs'] = $this->invoice_model->get();
        $data['subview'] = $this->load->view('admin/accounts/staff_ledger', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
    /*** END STAFF LEDGER ***/

    public function ajax_get_jobs_by_client($client_id)
    {
        $reference  = $this->invoice_model->check_by_all(array('client_id'=>$client_id), 'tbl_invoices');
        echo json_encode($reference);
        die();
    }

    /***BEGIN PAYMENT VOUCHER**/
    public function manage_payment_voucher($action = NULL, $id = NULL) {
        $data['banks'] = $this->accounts_model->read_bank_where('B_STATUS',1);
        $data['payment_accounts']= $this->accounts_model->payment_accounts();
        if($action == 'view_payment_voucher'){
            $data['payment_voucher'] = $this->accounts_model->payment_voucher_by_id($id);
            $subview = "payment_voucher";
        }
        else{
            $subview = "add_payment_voucher";
        }
        $data['title'] = "Payment Voucher";
        $data['subview'] = 'admin/accounts/'.$subview;
        $this->load->view('admin/_layout_main2', $data);
    }

    public function save_payment_voucher(){

        $payment_mode=$this->input->post('payment_type');

        $last_ref = $this->accounts_model->max_payment_voucher();
        $no = $last_ref->PV_ID;
        $ref_no = "100".($no+1);
        if ($payment_mode==1){
            $credit_account=1;
            $ref_type='CPV';
        }else{
            $credit_account=$this->input->post('credit_account_id');
            $ref_type='BPV';
        }
        $amount=$this->input->post('amount');

        $this->accounts_model->create_transaction(array(
            'T_DATE'=> date('Y-m-d',strtotime($this->input->post('date'))),
            'T_PAY' => '',
            'T_TYPE'=>"Payment Voucher",
            'CREATED_BY'=> $this->session->userdata('user_id'),
            'CREATED_AT' => date('Y-m-d H:i:s')
        ));
        $tr_id = $this->db->insert_id();
        $tr_data=array();
        $tr_data[] = array(
            'T_ID'          => $tr_id,
            'A_ID'          => $credit_account,
            'PARTICULARS'   => $this->input->post('desc'),
            'TM_AMOUNT'     => $amount,
            'TM_TYPE'       => "Credit",
            'IS_ACTIVE'     => 1
        );
        $tr_data[] = array(
            'T_ID'          => $tr_id,
            'A_ID'          => $this->input->post('paid_accounts'),
            'PARTICULARS'   => $this->input->post('desc'),
            'TM_AMOUNT'     => $amount,
            'TM_TYPE'       => "Debit",
            'IS_ACTIVE'     => 1
        );
        $this->accounts_model->create_transaction_meta($tr_data);

        $payment_voucher = array(
            'U_ID'               => $this->session->userdata('user_id'),
            'PAYMENT_MODE'       => $payment_mode,
            'ACCOUNT_CREDIT'     => $credit_account,
            'PAID_TO_ACCOUNT'    => $this->input->post('paid_accounts'),
            'PV_REF_TYPE'        => $ref_type,
            'PV_REF_NO'          => $ref_no,
            'PV_VOUCHER_NO'      => $ref_type.$ref_no,
            'PV_INSTRUMENT'      => $this->input->post('chq_no'),
            'PV_AMOUNT'          => $amount,
            'PV_DATE'            => date('Y-m-d',strtotime($this->input->post('date'))),
            'PV_DESC'            => $this->input->post('desc'),
            'T_ID'               => $tr_id,
            'PV_CREATED_AT'      => date('Y-m-d H:i:s')
        );

        $this->invoice_model->_table_name = 'payment_vouchers';
        $this->invoice_model->_primary_key = 'PV_ID';
        $pv_id=$this->invoice_model->save($payment_voucher);

        $action = lang('activity_payment_voucher_created');
        $msg = $ref_type.$ref_no.' '.lang('payment_voucher_created');

        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'entry book',
            'module_field_id' => $pv_id,
            'activity' => $action,
            'icon' => 'fa-circle-o',
            'value1' => $amount
        );
        $this->invoice_model->_table_name = 'tbl_activities';
        $this->invoice_model->_primary_key = 'activities_id';
        $this->invoice_model->save($activity);

        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/accounts/payment_voucher_list');
    }

    public function payment_voucher_list()
    {
        $mode='';
        $payment_mode=$this->input->post('payment_type');
        if ($mode==''){
            $this->invoice_model->_table_name = 'payment_vouchers';
            $this->invoice_model->_order_by = 'PV_ID';
            $data['advances'] = $this->invoice_model->get();
        }
        $data['payment_mode']=$payment_mode;
        $data['subview'] = $this->load->view('admin/accounts/payment_voucher_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function payment_voucher_lists($payment_mode=0)
    {
        $search_array = array();
        $rows = array();
        $search_value = $_POST['search']['value'];
        if ($search_value) {
            $search_array = array(
                'pv.PV_VOUCHER_NO' => $search_value
            );
        }
        $length = 10;
        $start = 1;
        if ($_POST['length'] != -1) {
            $length = $_POST['length'];
            $start = $_POST['start'];
        }
        $payment_voucher_list = $this->accounts_model->payment_voucher_searching_list($search_array, $length, $start, $payment_mode);
        $payment_voucher_list_rows = $this->accounts_model->payment_voucher_list_rows($search_array, $payment_mode);
        $i = $_POST['start'] + 1;
        foreach ($payment_voucher_list as $payment_voucher) {
            $payment_mode='Cash';
            $bank_name='--';
            $branch_name='--';
            if ($payment_voucher->PAYMENT_MODE==2){
                $payment_mode='Bank';
                $branch=$this->invoice_model->check_by(array('A_ID'=>$payment_voucher->ACCOUNT_CREDIT), 'branches');
                $branch_name=$branch->BR_NAME;
                $bank=$this->invoice_model->check_by(array('B_ID'=>$branch->B_ID), 'banks');
                $bank_name=$bank->B_NAME;
            }
            $action = btn_view('admin/accounts/manage_payment_voucher/view_payment_voucher/'.$payment_voucher->PV_ID);
            $rows[] = array(
                $payment_voucher->PV_VOUCHER_NO,
                $payment_mode,
                $bank_name,
                $branch_name,
                $payment_voucher->paid_account,
                date('d-m-Y',strtotime($payment_voucher->PV_DATE)),
                $payment_voucher->PV_AMOUNT,
                $payment_voucher->PV_DESC,
                $action
            );
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $payment_voucher_list_rows,
            "recordsFiltered" => $payment_voucher_list_rows,
            "data" => $rows
        );
        echo json_encode($output);
    }
    /***END PAYMENT VOUCHER**/

    /***BEGIN RECEIPT VOUCHER**/
    public function manage_receipt_voucher($action = NULL, $id = NULL) {
        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'client_id';
        $data['all_clients'] = $this->invoice_model->get();

        $this->db->select('*');
        $this->db->from('tbl_invoices');
        $this->db->order_by('created_date', 'DESC');
        $data['all_invoices'] = $this->db->get()->result();

        $data['banks'] = $this->accounts_model->read_bank_where('B_STATUS',1);
        $data['payment_accounts']= $this->accounts_model->receiving_accounts();
        if($action == 'view_receipt_voucher'){
            $data['advance_info'] = $this->accounts_model->pre_job_advances_by_id($id);
            $data['advance_details'] = $this->accounts_model->job_advances_details_by_id($id);
            $subview = "receipt_voucher";
        }
        else{
            $subview = "add_receipt_voucher";
        }
        $data['title'] = "Receipt Voucher";
        $data['subview'] = 'admin/accounts/'.$subview;
        $this->load->view('admin/_layout_main2', $data);
    }

    public function ajax_get_vocher_details($vocher_no){

        $data = $this->invoice_model->check_by(array('ap_id' => $vocher_no), 'tbl_advance_payments');
        echo json_encode($data);

    }

    public function ajax_select_invoices($client_id){

        $data = $this->invoice_model->check_by_all(array('client_id' => $client_id), 'tbl_invoices');
        echo json_encode($data);
    }

    public function save_receipt_voucher()
    {
        $expense_type = $this->input->post('expense_type');
        if($expense_type == 'Misc'){
            $payment_mode=$this->input->post('misc_payment_type');
            $last_ref = $this->accounts_model->max_receipt_voucher();
            $no = $last_ref->ap_id;
            $ref_no = "100".($no+1);

            $client_id=$this->input->post('client_id');
            $this->accounts_model->create_transaction(array(
                'T_DATE'=>$this->input->post('misc_date'),
                'T_PAY' => '',
                'T_TYPE'=>"Receipt Voucher - Misc",
                'CREATED_BY'=> $this->session->userdata('user_id'),
                'CREATED_AT' => date('Y-m-d H:i:s')
            ));
            $tr_id = $this->db->insert_id();
            $account_id = NULL;
            if($payment_mode == "Cash"){
                $account_id = 1;
                $ref_type='CRV';
            } else{
                $account_id = $this->input->post('misc_account_id');
                $ref_type='BRV';
            }

            $ap_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Liabilities', 'NAME' => 'Current Liabilities'), 'sub_heads');
            $ap_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_head->SUB_HEAD_ID, 'H_NAME' => 'Advances from clients'), 'accounts_head');
            $advances_accounts = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_sub_head->SUB_HEAD_ID, 'H_ID' => $ap_sub_head->H_ID, 'CLIENT_ID' => $client_id, 'A_NAME'=>client_name($client_id).' (Advances from clients)'), 'accounts');

            $data = array(
                'expense_type'      => $expense_type,
                'client_id'         => $client_id,
                'debit_account'     => $account_id,
                'credit_account'    => $advances_accounts->A_ID,
                'payment_method'    => $this->input->post('misc_payment_type'),
                'ref_type'          => $ref_type,
                'ref_no'            => $ref_no,
                'voucher_no'        => $ref_type.$ref_no,
                'cheque_payorder'   => $this->input->post('misc_pay_order'),
                'instrument_type'   => $this->input->post('misc_instrument_type'),
                'description'       => $this->input->post('misc_desc'),
                'amount'            => $this->input->post('misc_amount'),
                'payment_date'      => $this->input->post('misc_date'),
                'transaction_id'    => $tr_id,
                'created_by'=> $this->session->userdata('user_id')
            );

            $this->invoice_model->_table_name = 'tbl_advance_payments';
            $this->invoice_model->_primary_key = 'ap_id';
            $expense_id = $this->invoice_model->save($data);

            $tr_data = array();
            $tr_data[] = array(
                'T_ID'          => $tr_id,
                'A_ID'          => $advances_accounts->A_ID,
                'PARTICULARS'   => $this->input->post('misc_desc'),
                'TM_AMOUNT'     => $this->input->post('misc_amount'),
                'TM_TYPE'       => "Credit",
                'IS_ACTIVE'     => 1
            );
            $tr_data[]=array(
                'T_ID'          => $tr_id,
                'A_ID'          => $account_id,
                'PARTICULARS'   => $this->input->post('misc_desc'),
                'TM_AMOUNT'     => $this->input->post('misc_amount'),
                'TM_TYPE'       => "Debit",
                'IS_ACTIVE'     => 1
            );
            $this->accounts_model->create_transaction_meta($tr_data);
        }
        elseif($expense_type=='Job'){
            $last_ref = $this->accounts_model->max_receipt_voucher();
            $no = $last_ref->ap_id;
            $ref_no = "100".($no+1);

            $client = $this->invoice_model->check_by(array('invoices_id'=>$_POST['invoice_id']), 'tbl_invoices');
            $ap_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Liabilities', 'NAME' => 'Current Liabilities'), 'sub_heads');
            $ap_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_head->SUB_HEAD_ID, 'H_NAME' => 'Advances from clients'), 'accounts_head');
            $advances_accounts = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_sub_head->SUB_HEAD_ID, 'H_ID' => $ap_sub_head->H_ID, 'CLIENT_ID' => $client->client_id, 'A_NAME'=>client_name($client->client_id).' (Advances from clients)'), 'accounts');

            $account_id = NULL;
            if($this->input->post('payment_type') == "Cash"){
                $account_id = 1;
                $ref_type='CRV';
            } else{
                $account_id = $this->input->post('account_id');
                $ref_type='BRV';
            }

            $tr_id=0;
            $payment_type = $this->input->post('payment_type');
            if($payment_type == 'Cash' || $payment_type == 'Bank'){
                $this->accounts_model->create_transaction(array(
                    'T_DATE'=>$this->input->post('date'),
                    'T_PAY' => '',
                    'T_TYPE'=>"Receipt Voucher - Job",
                    'CREATED_BY'=> $this->session->userdata('user_id'),
                    'CREATED_AT' => date('Y-m-d H:i:s')
                ));
                $tr_id = $this->db->insert_id();

                $data = array(
                    'expense_type'      => $expense_type,
                    'invoices_id'       => $this->input->post('invoice_id'),
                    'debit_account'     => $account_id,
                    'credit_account'    => $advances_accounts->A_ID,
                    'ref_type'          => $ref_type,
                    'ref_no'            => $ref_no,
                    'voucher_no'        => $ref_type.$ref_no,
                    'payment_method'    => $payment_type,
                    'description'       => $this->input->post('desc'),
                    'amount'            => $this->input->post('amount'),
                    'payment_date'      => $this->input->post('date'),
                    'instrument_type'   => $this->input->post('instrument_type'),
                    'cheque_payorder'   => $this->input->post('cheque_payorder'),
                    'transaction_id'    => $tr_id,
                    'created_by'=> $this->session->userdata('user_id')
                );

                $this->invoice_model->_table_name = 'tbl_advance_payments';
                $this->invoice_model->_primary_key = 'ap_id';
                $expense_id = $this->invoice_model->save($data);

                $tr_data = array();
                $tr_data[] = array(
                    'T_ID'          => $tr_id,
                    'A_ID'          => $advances_accounts->A_ID,
                    'PARTICULARS'   => $this->input->post('desc'),
                    'TM_AMOUNT'     => $this->input->post('amount'),
                    'TM_TYPE'       => "Credit",
                    'IS_ACTIVE'     => 1
                );
                $tr_data[]=array(
                    'T_ID'          => $tr_id,
                    'A_ID'          => $account_id,
                    'PARTICULARS'   => $this->input->post('desc'),
                    'TM_AMOUNT'     => $this->input->post('amount'),
                    'TM_TYPE'       => "Debit",
                    'IS_ACTIVE'     => 1
                );
                $this->accounts_model->create_transaction_meta($tr_data);
            }else{
                $title = $this->input->post('pay_order_title');
                $amount = $this->input->post('pay_order_amount');
                $pay_order = $this->input->post('pay_order_no');
                $self_bank = $this->input->post('self_bank');
                $self_branch = $this->input->post('self_branch');
                $self_account_id = $this->input->post('self_account_id');
                $total_amount = 0;
                if($payment_type == 'Third Party'){
                    $ref_type='TRV';
                    for ($x = 0; $x < count($title); $x++) {
                        $total_amount += $amount[$x];
                    }
                }else{
                    $total_amount = $this->input->post('amount');
                }
                $data = array(
                    'expense_type'      => $expense_type,
                    'invoices_id'       => $this->input->post('invoice_id'),
                    'debit_account'     => $account_id,
                    'credit_account'    => $advances_accounts->A_ID,
                    'ref_type'          => $ref_type,
                    'ref_no'            => $ref_no,
                    'voucher_no'        => $ref_type.$ref_no,
                    'payment_method'    => $payment_type,
                    'description'       => $this->input->post('desc'),
                    'amount'            => $total_amount,
                    'payment_date'      => $this->input->post('date'),
                    'transaction_id'    => $tr_id,
                    'created_by'=> $this->session->userdata('user_id')
                );

                $this->invoice_model->_table_name = 'tbl_advance_payments';
                $this->invoice_model->_primary_key = 'ap_id';
                $expense_id = $this->invoice_model->save($data);

                if (!empty($title)) {
                    for ($x = 0; $x < count($title); $x++) {
                        $data = array(
                            'ap_id'         => $expense_id,
                            'invoices_id'   => $this->input->post('invoice_id'),
                            'apd_amount'    => $amount[$x],
                            'apd_title'     => $title[$x],
                            'apd_pay_order' => $pay_order[$x],
                            'apd_bank'      => $self_bank[$x],
                            'apd_branch'    => $self_branch[$x],
                            'apd_account_id'=> $self_account_id[$x]
                        );
                        $this->invoice_model->_table_name = 'tbl_advance_payment_details';
                        $this->invoice_model->_primary_key = 'apd_id';
                        $this->invoice_model->save($data);
                    }
                }
            }
        }
        else{
            $payment_mode=$this->input->post('others_payment_type');

            $last_ref = $this->accounts_model->max_receipt_voucher();
            $no = $last_ref->ap_id;
            $ref_no = "100".($no+1);
            if ($payment_mode=='Cash'){
                $debit_account=1;
                $ref_type='CRV';
            }else{
                $debit_account=$this->input->post('debit_account_id');
                $ref_type='BRV';
            }
            $amount=$this->input->post('others_amount');

            $this->accounts_model->create_transaction(array(
                'T_DATE'=> date('Y-m-d',strtotime($this->input->post('others_date'))),
                'T_PAY' => '',
                'T_TYPE'=>"Receipt Voucher",
                'CREATED_BY'=> $this->session->userdata('user_id'),
                'CREATED_AT' => date('Y-m-d H:i:s')
            ));
            $tr_id = $this->db->insert_id();
            $tr_data=array();
            $tr_data[] = array(
                'T_ID'          => $tr_id,
                'A_ID'          => $this->input->post('paid_accounts'),
                'PARTICULARS'   => $this->input->post('other_desc'),
                'TM_AMOUNT'     => $amount,
                'TM_TYPE'       => "Credit",
                'IS_ACTIVE'     => 1
            );
            $tr_data[] = array(
                'T_ID'          => $tr_id,
                'A_ID'          => $debit_account,
                'PARTICULARS'   => $this->input->post('other_desc'),
                'TM_AMOUNT'     => $amount,
                'TM_TYPE'       => "Debit",
                'IS_ACTIVE'     => 1
            );
            $this->accounts_model->create_transaction_meta($tr_data);

            $data = array(
                'expense_type'      => $expense_type,
                'invoices_id'       => $this->input->post('invoice_id'),
                'debit_account'     => $debit_account,
                'credit_account'    => $this->input->post('paid_accounts'),
                'ref_type'          => $ref_type,
                'ref_no'            => $ref_no,
                'voucher_no'        => $ref_type.$ref_no,
                'payment_method'    => $payment_mode,
                'description'       => $this->input->post('other_desc'),
                'amount'            => $amount,
                'payment_date'      => date('Y-m-d',strtotime($this->input->post('others_date'))),
                'transaction_id'    => $tr_id,
            );

            $this->invoice_model->_table_name = 'tbl_advance_payments';
            $this->invoice_model->_primary_key = 'ap_id';
            $expense_id = $this->invoice_model->save($data);
        }
        $action = lang('activity_added_payment_received');
        $msg = $ref_type.$ref_no.' '.lang('payment_received_added');

        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'entry book',
            'module_field_id' => $expense_id,
            'activity' => $action,
            'icon' => 'fa-circle-o',
            'value1' => $this->input->post('amount')
        );
        $this->invoice_model->_table_name = 'tbl_activities';
        $this->invoice_model->_primary_key = 'activities_id';
        $this->invoice_model->save($activity);

        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/accounts/receipt_voucher_list');
    }

    public function manage_suspense_payment() {
        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'client_id';
        $data['all_clients'] = $this->invoice_model->get();

        $ap_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Assets', 'NAME' => 'Current Assets'), 'sub_heads');
        $ap_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_head->SUB_HEAD_ID, 'H_NAME' => 'Account Receivables'), 'accounts_head');
        $suspence_account = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_sub_head->SUB_HEAD_ID, 'H_ID' => $ap_sub_head->H_ID, 'A_NAME'=> 'Suspense Payments'), 'accounts');

        $data['all_vochers'] = $this->accounts_model->getSuspenseVocher($suspence_account->A_ID);

        $subview = "add_suspense_payment";
        $data['title'] = "Suspense Payments";
        $data['subview'] = 'admin/accounts/'.$subview;
        $this->load->view('admin/_layout_main2', $data);
    }

    public function save_suspense_voucher(){
        $debit_account = $this->input->post('debit_account');
        $client_id = $this->input->post('client_id');
        $vocher_id = $this->input->post('vocher_id');
        $invoice_id = $this->input->post('invoice_id');

        $adc_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Liabilities', 'NAME' => 'Current Liabilities'), 'sub_heads');
        $adc_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $adc_head->SUB_HEAD_ID, 'H_NAME' => 'Advances from clients'), 'accounts_head');
        $credit_account = $this->invoice_model->check_by(array('H_ID' => $adc_sub_head->H_ID, 'CLIENT_ID' => $client_id,'A_NAME'=>client_name($client_id).' (Advances from clients)'), 'accounts');

        $amount = $this->accounts_model->getVocherAmount($vocher_id);

        $data['client_id'] = $client_id;
        $data['invoices_id'] = $invoice_id;
        $data['transferred'] = $client_id;

        $this->invoice_model->_table_name = 'tbl_advance_payments';
        $this->invoice_model->_primary_key = 'ap_id';
        $ap_id = $this->invoice_model->save($data,$vocher_id);

        $this->accounts_model->create_transaction(array(
            'T_DATE'=>$this->input->post('others_date'),
            'T_PAY' => '',
            'T_TYPE'=> "Voucher transfer - Advances",
            'CREATED_BY'=> $this->session->userdata('user_id'),
            'CREATED_AT' => date('Y-m-d H:i:s')
        ));
        $tr_id = $this->db->insert_id();
        $tr_data=array();
        $tr_data[] = array(
            'T_ID'          => $tr_id,
            'A_ID'          => $credit_account->A_ID,
            'PARTICULARS'   => $this->input->post('other_desc'),
            'TM_AMOUNT'     => $amount->amount,
            'TM_TYPE'       => "Credit",
            'IS_ACTIVE'     => 1
        );
        $tr_data[] = array(
            'T_ID'          => $tr_id,
            'A_ID'          => $debit_account,
            'PARTICULARS'   => $this->input->post('other_desc'),
            'TM_AMOUNT'     => $amount->amount,
            'TM_TYPE'       => "Debit",
            'IS_ACTIVE'     => 1
        );
        $this->accounts_model->create_transaction_meta($tr_data);
        $type = "success";
        $message = 'Successfully done';
        set_message($type, $message);
        redirect('admin/accounts/receipt_voucher_list');

    }

    public function receipt_voucher_list()
    {
        $mode='';
        $payment_mode=$this->input->post('payment_type');
        if ($payment_mode==1){
            $mode='Cash';
        }elseif ($payment_mode==2){
            $mode='Bank';
        }elseif ($payment_mode==3){
            $mode='Third Party';
        }
        if ($mode==''){
            $this->invoice_model->_table_name = 'tbl_advance_payments';
            $this->invoice_model->_order_by = 'ap_id';
            $data['advances'] = $this->invoice_model->get();
        }
        $data['payment_mode']=$payment_mode;
        $data['subview'] = $this->load->view('admin/accounts/receipt_voucher_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function receipt_voucher_lists($payment_mode=0)
    {
        $search_array = array();
        $rows = array();
        $search_value = $_POST['search']['value'];
        if ($search_value) {
            $search_array = array(
                'ad.voucher_no' => $search_value
            );
        }
        $length = 10;
        $start = 1;
        if ($_POST['length'] != -1) {
            $length = $_POST['length'];
            $start = $_POST['start'];
        }
        $receipt_voucher_list = $this->accounts_model->receipt_voucher_searching_list($search_array, $length, $start, $payment_mode);
        $receipt_voucher_list_rows = $this->accounts_model->receipt_voucher_list_rows($search_array, $payment_mode);
        $i = $_POST['start'] + 1;
        foreach ($receipt_voucher_list as $receipt_voucher) {
            if($receipt_voucher->expense_type == 'Job') {
                $job_info = $this->invoice_model->check_by(array('invoices_id' => $receipt_voucher->invoices_id), 'tbl_invoices');
                $client_info = $this->invoice_model->check_by(array('client_id' => $job_info->client_id), 'tbl_client');
                $job = $this->invoice_model->job_no_creation($receipt_voucher->invoices_id);
                $client = $client_info->name;
            }elseif($receipt_voucher->expense_type=='Misc'){
                $job = "--";
                $client_info = $this->invoice_model->check_by(array('client_id' => $receipt_voucher->client_id), 'tbl_client');
                $client = $client_info->name;
            }else{
                $job = "--";
                $client ="--";
            }
            $branch_info = $this->invoice_model->check_by(array('A_ID' => $receipt_voucher->debit_account), 'branches');
            if (!empty($branch_info)){
                $bank_info = $this->invoice_model->check_by(array('B_ID' => $branch_info->B_ID), 'banks');
            }
            $action = btn_view('admin/accounts/manage_receipt_voucher/view_receipt_voucher/'.$receipt_voucher->ap_id);
            $rows[] = array(
                $receipt_voucher->voucher_no,
                date('d-m-Y',strtotime($receipt_voucher->payment_date)),
                $job,
                $client,
                $receipt_voucher->payment_method,
                ($receipt_voucher->payment_method == "Cash" || $receipt_voucher->payment_method == "Third Party")?'--':$bank_info->B_NAME,
                ($receipt_voucher->payment_method == "Cash" || $receipt_voucher->payment_method == "Third Party")?'--':$branch_info->BR_NAME,
                number_format($receipt_voucher->amount,2),
                $action
            );
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $receipt_voucher_list_rows,
            "recordsFiltered" => $receipt_voucher_list_rows,
            "data" => $rows
        );
        echo json_encode($output);
    }
    /***END RECEIPT VOUCHER**/

    /**Get Account Balance**/
    public function get_account_balance($account_id){
        $accounts_detail=$this->accounts_model->read_account_by_id($account_id);

        $balance=0;
        if ($accounts_detail->A_OPENINGTYPE=='Debit'){
            $balance+=$accounts_detail->A_OPENINGBALANCE ;
        }elseif ($accounts_detail->A_OPENINGTYPE=='Credit'){
            $balance-=$accounts_detail->A_OPENINGBALANCE ;
        }
        $current_date=date('Y-m-d');
        $next_date = date('Y-m-d',strtotime(' +1 day'));
        $balance+=transaction_date($account_id,$current_date,'Debit')->amount;
        $balance-=transaction_date($account_id,$next_date,'Credit')->amount;
        echo json_encode($balance);
    }

    /***START REVERSAL FORM**/
    public function reversal_form() {
        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'client_id';

        $data['title'] = "Reversal Form";
        $data['subview'] = 'admin/accounts/reversal_form';
        $this->load->view('admin/_layout_main2', $data);
    }

    public function save_reversal_form()
    {
        $type=$this->input->post('type');
        $voucher_no=$this->input->post('voucher_no');
        $amount=0;
        if ($type==1){
            $payment_voucher = $this->invoice_model->check_by(array('PV_VOUCHER_NO'=>$voucher_no),'payment_vouchers');
            if (!empty($payment_voucher)){
                $amount=$payment_voucher->PV_AMOUNT;
                $this->accounts_model->create_transaction(array(
                    'T_DATE'=> date('Y-m-d'),
                    'T_PAY' => '',
                    'T_TYPE'=>"Reverse Entry",
                    'CREATED_BY'=> $this->session->userdata('user_id'),
                    'CREATED_AT' => date('Y-m-d H:i:s')
                ));
                $tr_id = $this->db->insert_id();
                $tr_data=array();
                $tr_data[] = array(
                    'T_ID'          => $tr_id,
                    'A_ID'          => $payment_voucher->PAID_TO_ACCOUNT,
                    'PARTICULARS'   => $this->input->post('desc'),
                    'TM_AMOUNT'     => $payment_voucher->PV_AMOUNT,
                    'TM_TYPE'       => "Credit",
                    'IS_ACTIVE'     => 1
                );
                $tr_data[] = array(
                    'T_ID'          => $tr_id,
                    'A_ID'          => $payment_voucher->ACCOUNT_CREDIT,
                    'PARTICULARS'   => $this->input->post('desc'),
                    'TM_AMOUNT'     => $payment_voucher->PV_AMOUNT,
                    'TM_TYPE'       => "Debit",
                    'IS_ACTIVE'     => 1
                );
                $this->accounts_model->create_transaction_meta($tr_data);

                $reverse_entry = array(
                    'voucher_id'         => $payment_voucher->PV_ID,
                    'transaction_id'     => $tr_id,
                    'voucher_type'       => $type,
                    'description'        => $this->input->post('desc'),
                    'created_by'         => $this->session->userdata('user_id'),
                    'created_at'         => date('Y-m-d H:i:s')
                );

                $this->invoice_model->_table_name = 'reverse_entries';
                $this->invoice_model->_primary_key = 'reverse_id';
                $reverse_id=$this->invoice_model->save($reverse_entry);

                $msg="Payment Voucher Reversed Successfully!";
                $msg_type="success";
                $url='admin/accounts/reversal_list';

                $action = "Payment Voucher Reversed!";

                $activity = array(
                    'user'              => $this->session->userdata('user_id'),
                    'module'            => 'entry book',
                    'module_field_id'   => $reverse_id,
                    'activity'          => $action,
                    'icon'              => 'fa-circle-o',
                    'value1'            => "Reverse Entry",
                    'value2'            => 'PKR '.$amount
                );
                $this->invoice_model->_table_name = 'tbl_activities';
                $this->invoice_model->_primary_key = 'activities_id';
                $this->invoice_model->save($activity);
            }else{
                $msg="This voucher does not exist!";
                $msg_type="error";
                $url='admin/accounts/reversal_form';
            }
        }elseif ($type==2){
            $receipt_voucher = $this->invoice_model->check_by(array('voucher_no'=>$voucher_no),'tbl_advance_payments');
            if (!empty($receipt_voucher)){
                if ($receipt_voucher->payment_method=='Third Party'){
                    $msg="This type of receipt cannot reverse!";
                    $msg_type="error";
                    $url='admin/accounts/reversal_form';
                }else{
                    $amount=$receipt_voucher->amount;
                    $this->accounts_model->create_transaction(array(
                        'T_DATE'=> date('Y-m-d'),
                        'T_PAY' => '',
                        'T_TYPE'=>"Reverse Entry",
                        'CREATED_BY'=> $this->session->userdata('user_id'),
                        'CREATED_AT' => date('Y-m-d H:i:s')
                    ));
                    $tr_id = $this->db->insert_id();
                    $tr_data=array();
                    $tr_data[] = array(
                        'T_ID'          => $tr_id,
                        'A_ID'          => $receipt_voucher->debit_account,
                        'PARTICULARS'   => $this->input->post('desc'),
                        'TM_AMOUNT'     => $receipt_voucher->amount,
                        'TM_TYPE'       => "Credit",
                        'IS_ACTIVE'     => 1
                    );
                    $tr_data[] = array(
                        'T_ID'          => $tr_id,
                        'A_ID'          => $receipt_voucher->credit_account,
                        'PARTICULARS'   => $this->input->post('desc'),
                        'TM_AMOUNT'     => $receipt_voucher->amount,
                        'TM_TYPE'       => "Debit",
                        'IS_ACTIVE'     => 1
                    );
                    $this->accounts_model->create_transaction_meta($tr_data);

                    $reverse_entry = array(
                        'voucher_id'         => $receipt_voucher->ap_id,
                        'transaction_id'     => $tr_id,
                        'voucher_type'       => $type,
                        'description'        => $this->input->post('desc'),
                        'created_by'         => $this->session->userdata('user_id'),
                        'created_at'         => date('Y-m-d H:i:s')
                    );

                    $this->invoice_model->_table_name = 'reverse_entries';
                    $this->invoice_model->_primary_key = 'reverse_id';
                    $reverse_id=$this->invoice_model->save($reverse_entry);

                    $msg="Receipt Voucher Reversed Successfully!";
                    $msg_type="success";
                    $url='admin/accounts/reversal_list';

                    $action = "Receipt Voucher Reversed!";

                    $activity = array(
                        'user'              => $this->session->userdata('user_id'),
                        'module'            => 'entry book',
                        'module_field_id'   => $reverse_id,
                        'activity'          => $action,
                        'icon'              => 'fa-circle-o',
                        'value1'            => "Reverse Entry",
                        'value2'            => 'PKR '.$amount
                    );
                    $this->invoice_model->_table_name = 'tbl_activities';
                    $this->invoice_model->_primary_key = 'activities_id';
                    $this->invoice_model->save($activity);
                }
            }else{
                $msg="This voucher does not exist!";
                $msg_type="error";
                $url='admin/accounts/reversal_form';
            }
        }elseif ($type==3){
            $general_expense = $this->invoice_model->check_by(array('PC_VOUCHER_NO'=>$voucher_no),'petty_cash');
            if (!empty($general_expense)){
                $amount=0;
                $general_expense_detail = $this->invoice_model->check_by_all(array('PC_ID'=>$general_expense->PC_ID),'petty_cash_details');
                $this->accounts_model->create_transaction(array(
                    'T_DATE'    => date('Y-m-d'),
                    'T_PAY'     => '',
                    'T_TYPE'    =>"Reverse Entry",
                    'CREATED_BY'=> $this->session->userdata('user_id'),
                    'CREATED_AT'=> date('Y-m-d H:i:s')
                ));
                $tr_id = $this->db->insert_id();
                $tr_data=array();
                foreach ($general_expense_detail as $details){
                    $amount+=$details->PCD_AMOUNT;
                    $tr_data[] = array(
                        'T_ID'          => $tr_id,
                        'A_ID'          => $details->A_ID,
                        'PARTICULARS'   => $this->input->post('desc'),
                        'TM_AMOUNT'     => $details->PCD_AMOUNT,
                        'TM_TYPE'       => "Credit",
                        'IS_ACTIVE'     => 1
                    );
                }
                $tr_data[] = array(
                    'T_ID'          => $tr_id,
                    'A_ID'          => $general_expense->PC_PAID_TO,
                    'PARTICULARS'   => $this->input->post('desc'),
                    'TM_AMOUNT'     => $amount,
                    'TM_TYPE'       => "Debit",
                    'IS_ACTIVE'     => 1
                );
                $this->accounts_model->create_transaction_meta($tr_data);

                $reverse_entry = array(
                    'voucher_id'         => $general_expense->PC_ID,
                    'transaction_id'     => $tr_id,
                    'voucher_type'       => $type,
                    'description'        => $this->input->post('desc'),
                    'created_by'         => $this->session->userdata('user_id'),
                    'created_at'         => date('Y-m-d H:i:s')
                );

                $this->invoice_model->_table_name = 'reverse_entries';
                $this->invoice_model->_primary_key = 'reverse_id';
                $reverse_id=$this->invoice_model->save($reverse_entry);

                $msg="General Expense Reversed Successfully!";
                $msg_type="success";
                $url='admin/accounts/reversal_list';

                $action = "General Expense Reversed!";

                $activity = array(
                    'user'              => $this->session->userdata('user_id'),
                    'module'            => 'entry book',
                    'module_field_id'   => $reverse_id,
                    'activity'          => $action,
                    'icon'              => 'fa-circle-o',
                    'value1'            => "Reverse Entry",
                    'value2'            => "PKR ".$amount
                );
                $this->invoice_model->_table_name = 'tbl_activities';
                $this->invoice_model->_primary_key = 'activities_id';
                $this->invoice_model->save($activity);
            }else{
                $msg="This voucher does not exist!";
                $msg_type="error";
                $url='admin/accounts/reversal_form';
            }
        }

        $type = $msg_type;
        $message = $msg;
        set_message($type, $message);
        redirect($url);
    }

    public function reversal_list()
    {
        $this->invoice_model->_table_name = 'reverse_entries';
        $this->invoice_model->_order_by = 'reverse_id';
        $data['reversal_entries'] = $this->invoice_model->get();
        $data['subview'] = $this->load->view('admin/accounts/reversal_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function reversal_lists(){
        $search_array = array();
        $rows = array();
        $search_value = $_POST['search']['value'];
        if ($search_value) {
            $search_array = array(
                'PV_VOUCHER_NO' => $search_value,
                'voucher_no' => $search_value,
                'PC_VOUCHER_NO' => $search_value
            );
        }
        $length = 10;
        $start = 1;
        if ($_POST['length'] != -1) {
            $length = $_POST['length'];
            $start = $_POST['start'];
        }
        $reverse_list = $this->accounts_model->reverse_searching_list($search_array, $length, $start);
        $reverse_list_rows = $this->accounts_model->reverse_searching_list_rows($search_array);
        $i = $_POST['start'] + 1;
        foreach ($reverse_list as $reverses) {
            if ($reverses->voucher_type == 1) {
                $voucher_type = 'Payment Voucher';
                /*$payment_voucher = $this->invoice_model->check_by(array('PV_ID' => $reverses->voucher_id), 'payment_vouchers');*/
                $voucher_no = $reverses->PV_VOUCHER_NO;
            } elseif ($reverses->voucher_type == 2) {
                $voucher_type = 'Receipt Voucher';
                /*$receipt_voucher = $this->invoice_model->check_by(array('ap_id' => $reverses->voucher_id), 'tbl_advance_payments');*/
                $voucher_no = $reverses->voucher_no;
            } elseif ($reverses->voucher_type == 3) {
                $voucher_type = 'General Expense';
                /*$receipt_voucher = $this->invoice_model->check_by(array('PC_ID' => $reverses->voucher_id), 'petty_cash');*/
                $voucher_no = $reverses->PC_VOUCHER_NO;
            }
            $rows[] = array(
                $voucher_type,
                $voucher_no,
                $reverses->created_at,
                $reverses->description
            );
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $reverse_list_rows,
            "recordsFiltered" => $reverse_list_rows,
            "data" => $rows
        );
        echo json_encode($output);
    }

    public function getEntryDetails(){
        $type=$this->input->post('voucher_type');
        $voucher_no=$this->input->post('voucher_no');
        $rows=array();
        if ($type==1){
            $bank_rows='';
            $payment_voucher = $this->invoice_model->check_by(array('PV_VOUCHER_NO'=>$voucher_no),'payment_vouchers');
            if (!empty($payment_voucher)){
                if ($payment_voucher->PAYMENT_MODE==1){
                    $payment_mode='Cash';
                }elseif ($payment_voucher->PAYMENT_MODE==2){
                    $payment_mode='Bank';
                    $branch=$this->invoice_model->check_by(array('A_ID'=>$payment_voucher->ACCOUNT_CREDIT),'branches');
                    $bank=$this->invoice_model->check_by(array('B_ID'=>$branch->B_ID),'banks');
                    $bank_rows='<div class="form-group"><label class="col-lg-4 control-label">'.lang('banks').'</label><div class="col-lg-3"><input type="text" class="form-control mb-5" value="'.$bank->B_NAME.'" disabled></div></div><div class="form-group"><label class="col-lg-4 control-label">'.lang('branches').'</label><div class="col-lg-3"><input type="text" class="form-control mb-5" value="'.$branch->BR_NAME.'" disabled></div></div>';
                }
                $rows[]='<div class="form-group"><label class="col-lg-4 control-label">'.lang('payment_mode').'</label><div class="col-lg-3"><input type="text" class="form-control mb-5" value="'.$payment_mode.'" disabled></div></div>'.$bank_rows.'<div class="form-group"><label class="col-lg-4 control-label">Paid To/By</label><div class="col-lg-3"><input type="text" class="form-control mb-5" value="'.read_subHead($payment_voucher->PAID_TO_ACCOUNT)->A_NAME.'" disabled></div></div><div class="form-group"><label class="col-lg-4 control-label">'.lang('amount').'</label><div class="col-lg-3"><input type="text" class="form-control mb-5" value="'.number_format($payment_voucher->PV_AMOUNT,2).'" disabled></div></div><div class="form-group"><label class="col-lg-4 control-label">'.lang('date').'</label><div class="col-lg-3"><input type="text" name="date" class="form-control" value="'.$payment_voucher->PV_DATE.'" disabled></div></div><div class="form-group"><label class="col-lg-3 control-label"></label><div class="col-lg-4"><button type="submit" class="btn btn-sm btn-success pull-right">'.lang('reverse').'</button></div></div>';
            }else{
                $rows[]='<div class="form-group"><label class="col-lg-4 control-label">&nbsp;</label><div class="col-lg-3"><p style="color: red;">This voucher does not exist!</p></div></div>';
            }
        }elseif ($type==2){
            $receipt_voucher = $this->invoice_model->check_by(array('voucher_no'=>$voucher_no),'tbl_advance_payments');
            if (!empty($receipt_voucher)){
                if ($receipt_voucher->payment_method=='Third Party'){
                    $rows[]='<div class="form-group"><label class="col-lg-4 control-label">&nbsp;</label><div class="col-lg-3"><p style="color: red;">This type of receipt cannot reverse!</p></div></div>';
                }else {
                    $bank_rows = '';
                    if ($receipt_voucher->expense_type == 'Job') {
                        $type = 'Job';
                        $client_info = $this->invoice_model->check_by(array('client_id' => $receipt_voucher->client_id), 'tbl_client');
                        $receive_fields = '<div class="form-group"><label class="col-lg-4 control-label">' . lang('reference_no') . '</label><div class="col-lg-3"><input type="text" class="form-control mb-5" value="' . $this->invoice_model->job_no_creation($receipt_voucher->invoices_id) . '" disabled></div></div><div class="form-group"><label class="col-lg-4 control-label">' . lang('client') . '</label><div class="col-lg-3"><input type="text" class="form-control mb-5" value="" disabled></div></div>';
                    } elseif ($receipt_voucher->expense_type == 'Misc') {
                        $type = 'Client';
                        $client_info = $this->invoice_model->check_by(array('client_id' => $receipt_voucher->client_id), 'tbl_client');
                        $receive_fields = '<div class="form-group"><label class="col-lg-4 control-label">' . lang('client') . '</label><div class="col-lg-3"><input type="text" class="form-control mb-5" value="' . $client_info->name . '" disabled></div></div>';
                    } elseif ($receipt_voucher->expense_type == 'Others') {
                        $type = 'Others';
                        $receive_fields = '<div class="form-group"><label class="col-lg-4 control-label">' . lang('received_from') . '</label><div class="col-lg-3"><input type="text" class="form-control mb-5" value="' . read_subHead($receipt_voucher->credit_account)->A_NAME . '" disabled></div></div>';
                    }
                    if ($receipt_voucher->payment_method == 'Cash') {
                        $payment_mode = 'Cash';
                    } elseif ($receipt_voucher->payment_method == 'Bank') {
                        $payment_mode = 'Bank';
                        $branch = $this->invoice_model->check_by(array('A_ID' => $receipt_voucher->debit_account), 'branches');
                        $bank = $this->invoice_model->check_by(array('B_ID' => $branch->B_ID), 'banks');
                        $bank_rows = '<div class="form-group"><label class="col-lg-4 control-label">' . lang('banks') . '</label><div class="col-lg-3"><input type="text" class="form-control mb-5" value="' . $bank->B_NAME . '" disabled></div></div><div class="form-group"><label class="col-lg-4 control-label">' . lang('branches') . '</label><div class="col-lg-3"><input type="text" class="form-control mb-5" value="' . $branch->BR_NAME . '" disabled></div></div>';
                    }
                    $rows[] = '<div class="form-group"><label class="col-lg-4 control-label">Receipt Type</label><div class="col-lg-3"><input type="text" class="form-control mb-5" value="' . $type . '" disabled></div></div>' . $receive_fields . '<div class="form-group"><label class="col-lg-4 control-label">' . lang('payment_mode') . '</label><div class="col-lg-3"><input type="text" class="form-control mb-5" value="' . $payment_mode . '" disabled></div></div>' . $bank_rows . '<div class="form-group"><label class="col-lg-4 control-label">' . lang('amount') . '</label><div class="col-lg-3"><input type="text" class="form-control mb-5" value="' . number_format($receipt_voucher->amount, 2) . '" disabled></div></div><div class="form-group"><label class="col-lg-4 control-label">' . lang('date') . '</label><div class="col-lg-3"><input type="text" name="date" class="form-control"  value="' . $receipt_voucher->payment_date . '" disabled></div></div><div class="form-group"><label class="col-lg-3 control-label"></label><div class="col-lg-4"><button type="submit" class="btn btn-sm btn-success pull-right">' . lang('reverse') . '</button></div></div>';
                }
            }else{
                $rows[]='<div class="form-group"><label class="col-lg-4 control-label">&nbsp;</label><div class="col-lg-3"><p style="color: red;">This voucher does not exist!</p></div></div>';
            }
        }elseif ($type==3){
            $general_expense = $this->invoice_model->check_by(array('PC_VOUCHER_NO'=>$voucher_no),'petty_cash');
            if (!empty($general_expense)){
                $general_expense_detail = $this->invoice_model->check_by_all(array('PC_ID'=>$general_expense->PC_ID),'petty_cash_details');
                $rows[]='<div class="form-group"><label class="col-lg-4 control-label">'.lang('paid_to').'</label><div class="col-lg-3"><input type="text" class="form-control mb-5" value="'.read_subHead($general_expense->PC_PAID_TO)->A_NAME.'" disabled></div></div>';
                foreach ($general_expense_detail as $details){
                    $rows[]='<div class="form-group"><label class="col-lg-4 control-label">'.lang('title').'</label><div class="col-lg-3"><input type="text" class="form-control mb-5" value="'.read_subHead($details->A_ID)->A_NAME.'" disabled></div></div><div class="form-group"><label class="col-lg-4 control-label">'.lang('amount').'</label><div class="col-lg-3"><input type="text" class="form-control mb-5" value="'.$details->PCD_AMOUNT.'" disabled></div></div>';
                }
                $rows[]='<div class="form-group"><label class="col-lg-4 control-label">'.lang('paid_to').'</label><div class="col-lg-3"><input type="text" class="form-control mb-5" value="'.$general_expense->PC_DATE.'" disabled></div></div><div class="form-group"><label class="col-lg-3 control-label"></label><div class="col-lg-4"><button type="submit" class="btn btn-sm btn-success pull-right">'.lang('reverse').'</button></div></div>';
            }else{
                $rows[]='<div class="form-group"><label class="col-lg-4 control-label">&nbsp;</label><div class="col-lg-3"><p style="color: red;">This voucher does not exist!</p></div></div>';
            }
        }
        echo json_encode($rows);
    }

    /***END REVERSAL FORM**/
}
