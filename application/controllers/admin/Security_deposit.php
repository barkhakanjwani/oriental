<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Security_deposit extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('invoice_model');
        $this->load->model('accounts_model');

        $this->load->helper('ckeditor');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "99.8%",
                'height' => "400px"
            )
        );
    }

    public function index(){
        $subview = 'manage_deposit';
        $data['subview'] = 'admin/security_deposit/'.$subview;
        $this->load->view('admin/_layout_main2', $data);
    }

    public function manage_deposit($action = NULL, $id = NULL) {
        $data['page'] = lang('deposit');
        $this->db->select('*')->from('tbl_invoices');
        $this->db->where('`invoices_id` NOT IN (SELECT `invoices_id` FROM `tbl_security_deposit`)', NULL, FALSE);
        /*$this->db->where('(`invoices_id` IN (SELECT `invoices_id` FROM `tbl_voucher_details`) OR `invoices_id` IN (SELECT `invoices_id` FROM `tbl_advance_payment_details` WHERE apd_title = "Security Deposit"))', NULL, FALSE);*/
        $data['all_invoices'] = $this->db->get()->result();
        /*$data['search_value'] = "";*/
        $data['title'] = "Create Deposit";
        $this->invoice_model->_table_name = 'banks';
        $this->invoice_model->_order_by = 'B_ID';
        $data['banks'] = $this->invoice_model->get();

        /*** SHIPPING COMPANIES LIST ***/
        $this->invoice_model->_table_name = 'tbl_shipping_line';
        $this->invoice_model->_order_by = 'shipping_id';
        $data['shipping_company_list'] = $this->invoice_model->get();
        /*** END SHIPPING COMPANIES LIST ***/

        if (!empty($id)) {
            $data['title'] = "Manage Deposit";
            $this->db->select('*')->from('tbl_invoices');
            $this->db->where('`invoices_id` NOT IN (SELECT `invoices_id` FROM `tbl_security_deposit` where `deposit_id`!='.decode($id).')', NULL, FALSE);
            /*$this->db->where('(`invoices_id` IN (SELECT `invoices_id` FROM `tbl_voucher_details`) OR `invoices_id` IN (SELECT `invoices_id` FROM `tbl_advance_payment_details` WHERE apd_title = "Security Deposit"))', NULL, FALSE);*/
            $data['all_invoices'] = $this->db->get()->result();
            $this->db->select('*');
            $this->db->from('tbl_security_deposit');
            $this->db->join('tbl_invoices', 'tbl_invoices.invoices_id = tbl_security_deposit.invoices_id');
            $this->db->where('tbl_security_deposit.deposit_id', decode($id));
            $data['deposit_info'] = $this->db->get()->row();
        }
        /** ALL JOBS **/
        $this->invoice_model->_table_name = 'tbl_invoices';
        $this->invoice_model->_order_by = 'invoices_id';
        $data['all_invoice_info'] = $this->invoice_model->get();
        if ($action == 'deposit_details') {
            $data['title'] = "Deposit Details";
            $this->db->select('tbl_security_deposit.*, tbl_invoices.invoices_id as invoices_id, tbl_invoices.client_id as client_id, tbl_invoices.created_date as job_date, tbl_invoices.reference_no as reference_no, tbl_invoices.type as type, tbl_invoices.shipping_line as shipping_line, tbl_invoices.consignment_type as consignment_type, tbl_invoices.bl_no as bl_no');
            $this->db->from('tbl_security_deposit');
            $this->db->join('tbl_invoices', 'tbl_invoices.invoices_id = tbl_security_deposit.invoices_id');
            $this->db->where('tbl_security_deposit.deposit_id', decode($id));
            $data['deposit_info'] = $this->db->get()->row();
            $subview = 'deposit_details';
        }
        else{
            $subview = 'manage_deposit';
        }

        $user_id = $this->session->userdata('user_id');

        $user_info = $this->invoice_model->check_by(array('user_id' => $user_id), 'tbl_users');
        $data['role'] = $user_info->role_id;
        $data['subview'] = 'admin/security_deposit/'.$subview;
        $this->load->view('admin/_layout_main2', $data);
    }

    public function search_deposit(){
        /*$this->db->select('tbl_security_deposit.*, tbl_invoices.invoices_id as invoices_id, tbl_invoices.client_id as client_id, tbl_invoices.created_date as job_date, tbl_invoices.reference_no as reference_no, tbl_invoices.type as type, tbl_invoices.vessel as vessel, tbl_invoices.shipping_line as shipping_line, tbl_invoices.consignment_type as consignment_type, tbl_invoices.bl_no as bl_no');
        $this->db->from('tbl_security_deposit');
        $this->db->join('tbl_invoices', 'tbl_invoices.invoices_id = tbl_security_deposit.invoices_id');
        $data['deposit_info'] = $this->db->get()->result();
        if($_POST){
            if(isset($_POST['client_id'])){
                $client_id = $_POST['client_id'];
                $data['client_id'] = $client_id;
                $this->db->select('tbl_security_deposit.*, tbl_invoices.invoices_id as invoices_id, tbl_invoices.client_id as client_id, tbl_invoices.created_date as job_date, tbl_invoices.reference_no as reference_no, tbl_invoices.type as type, tbl_invoices.vessel as vessel, tbl_invoices.shipping_line as shipping_line, tbl_invoices.consignment_type as consignment_type, tbl_invoices.bl_no as bl_no');
                $this->db->from('tbl_security_deposit');
                $this->db->join('tbl_invoices', 'tbl_invoices.invoices_id = tbl_security_deposit.invoices_id');
                $this->db->where('tbl_invoices.client_id',$_POST['client_id']);
                $data['deposit_info'] = $this->db->get()->result();
            }
            if(isset($_POST['reference_no'])) {
                $reference_no = $_POST['reference_no'];
                $data['reference_no'] = $reference_no;
                $invoice_info = $this->invoice_model->check_by(array('reference_no' => $reference_no), 'tbl_invoices');

                if (!empty($invoice_info)){
                    $this->db->select('tbl_security_deposit.*, tbl_invoices.invoices_id as invoices_id, tbl_invoices.client_id as client_id, tbl_invoices.created_date as job_date, tbl_invoices.reference_no as reference_no, tbl_invoices.type as type, tbl_invoices.vessel as vessel, tbl_invoices.shipping_line as shipping_line, tbl_invoices.consignment_type as consignment_type, tbl_invoices.bl_no as bl_no');
                    $this->db->from('tbl_security_deposit');
                    $this->db->join('tbl_invoices', 'tbl_invoices.invoices_id = tbl_security_deposit.invoices_id');
                    $this->db->where('tbl_invoices.invoices_id', $invoice_info->invoices_id);
                    $invoice_info = $this->db->get()->result();
                }
                $data['deposit_info'] = $invoice_info;
            }
        }
        $this->invoice_model->_table_name = 'tbl_invoices';
        $this->invoice_model->_order_by = 'invoices_id';
        $data['all_invoices'] = $this->invoice_model->get();
        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'name';
        $data['all_client'] = $this->invoice_model->get();
        $this->invoice_model->_table_name = 'tbl_bills';
        $this->invoice_model->_order_by = 'bill_id';
        $data['all_bill'] = $this->invoice_model->get();*/

        $data['title'] = "Search Deposit";
        $subview = 'search_deposit';

        if ($_POST) {
            $data['client_id'] = $_POST['client_id'];
            $data['reference_no'] = $_POST['reference_no'];
            $data['commodities'] = $_POST['commodity'];
            $data['bl_no'] = $_POST['bl_no'];
        }

        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'name';
        $data['all_client'] = $this->invoice_model->get();

        $user_id = $this->session->userdata('user_id');
        $user_info = $this->invoice_model->check_by(array('user_id' => $user_id), 'tbl_users');
        $data['role'] = $user_info->role_id;

        $data['subview'] = $this->load->view('admin/security_deposit/' . $subview, $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function search_deposits($client_id = null, $reference_no = null, $bl_no = null, $commodity = null){
        $search_array = array();
        $rows = array();
        $search_value = $_POST['search']['value'];
        if ($search_value) {
            $search_array = array(
                'cl.client_id' => $search_value,
                'inv.reference_no' => $search_value,
                'inv.bl_no' => $search_value,
                'comm.commodity' => $search_value
            );
        }
        $length = 10;
        $start = 1;
        if ($_POST['length'] != -1) {
            $length = $_POST['length'];
            $start = $_POST['start'];
        }
        $deposit_search_list = $this->invoice_model->deposit_searching_list($search_array, $length, $start, $client_id, $reference_no, $bl_no, $commodity);
        $deposit_search_list_rows = $this->invoice_model->deposit_searching_list_rows($search_array, $client_id, $reference_no, $bl_no, $commodity);
        $i = $_POST['start'] + 1;
        foreach ($deposit_search_list as $v_deposit) {
          
            $job_no = '<a class="text-info" href="'. base_url('admin/invoice/manage_invoice/invoice_details/'. $v_deposit->invoices_id).'">'. $this->invoice_model->job_no_creation($v_deposit->invoices_id).'</a>';
            $client_info = $this->invoice_model->check_by(array('client_id'=>$v_deposit->client_id), 'tbl_client');

            $rows[] = array(
                $job_no,
                strftime(config_item('date_format'), strtotime($v_deposit->created_date)),
                ucfirst($client_info->name),
                $v_deposit->name_representative,
                $v_deposit->container_no,
                number_format(($v_deposit->deposit_amount),2),
                btn_view('admin/security_deposit/manage_deposit/deposit_details/' . encode($v_deposit->deposit_id))." ".btn_edit('admin/security_deposit/manage_deposit/create_deposit/' . encode($v_deposit->deposit_id))
            );
           
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $deposit_search_list_rows,
            "recordsFiltered" => $deposit_search_list_rows,
            "data" => $rows
        );
        echo json_encode($output);
    }
    
    public function save_deposit($id = NULL) {
        $data = $this->invoice_model->array_from_post(array('invoices_id', 'shipping_account', 'name_representative', 'container_no', 'delivery_date_shipment', 'deposit_amount', 'arrival', 'advance_rent', 'remaining_rent', 'final_rent','container_detention', 'refund_amount', 'expected_date_of_refund'));
        $client = $this->invoice_model->check_by(array('invoices_id'=>$_POST['invoices_id']), 'tbl_invoices');
        if (!empty($_FILES['deposit_file']['name'])) {
            $val = $this->invoice_model->uploadAllType('deposit_file');
            $data['deposit_file'] = $val['path'];
        }

        if(!empty($id)){
            if($_POST['deposit_return'] == "Yes"){
                $data['deposit_return'] = "Yes";
                $data['deposit_return_amount'] = $_POST["deposit_return_amount"];
                $data['return_date_container'] = $_POST["return_date_container"];
            }else{
                $data['deposit_return'] = "No";
                $data['deposit_return_amount'] = 0.00;
                $data['return_date_container'] = NULL;
            }
            $action = lang('activity_deposit_updated');
            $msg = lang('deposit_updated');
        }else{
            $action = lang('activity_deposit_created');
            $msg = lang('deposit_created');
        }

        $this->invoice_model->_table_name = 'tbl_security_deposit';
        $this->invoice_model->_primary_key = 'deposit_id';
        $id = $this->invoice_model->save($data, $id);

        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'Security Deposit',
            'module_field_id' => $id,
            'activity' => $action,
            'icon' => 'fa-circle-o',
            'value1' => $client->reference_no,
            'value2' => 'PKR '.$_POST['deposit_amount']
        );
        $this->invoice_model->_table_name = 'tbl_activities';
        $this->invoice_model->_primary_key = 'activities_id';
        $this->invoice_model->save($activity);

        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/security_deposit/search_deposit');
    }
    
    public function get_client_name($invoice_id){
        $get_invoice_info = $this->invoice_model->check_by(array('invoices_id' => $invoice_id), 'tbl_invoices');
        $get_client_info = $this->invoice_model->check_by(array('client_id' => $get_invoice_info->client_id), 'tbl_client');
        echo json_encode($get_client_info->name);
        die();
    }

    public function shipping_calculator(){
        $data['title'] = "Shipping Calculator";
        $subview = 'shipping_calculator';
        $data['all_clients'] = $this->invoice_model->check_by_all(array('client_status'=>1),'tbl_client');

        $data['subview'] = $this->load->view('admin/security_deposit/' . $subview, $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function ajax_get_jobs_by_client($client_id)
    {
        $reference  = $this->invoice_model->check_by_all(array('client_id'=>$client_id), 'tbl_invoices');
        echo json_encode($reference);
        die();
    }

    public function ajax_get_invoice_detail($invoice_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_invoices');
        $this->db->join('tbl_shipping_line', 'tbl_shipping_line.shipping_id = tbl_invoices.shipping_line');
        $this->db->where('tbl_invoices.invoices_id', $invoice_id);
        $query = $this->db->get();
        $result = $query->row();
        echo json_encode($result);
        die();
    }

    public function save_shipping($id = NULL){
        $data = $this->invoice_model->array_from_post(array('client_id', 'invoices_id', 'shipping_company', 'number_container', 'rent_per_day', 'totals', 'exchange_rates', 'per_day_rent_total', 'free_day', 'igm_dates', 'shipping_rent_date', 'total_days', 'total_in_usd', 'total_in_pkr','er_date','additional_rent_days', 'additional_rent_in_usd', 'total_amount_in_pkr'));

        if(!empty($id)){

            $action = lang('activity_shipping_updated');
            $msg = lang('shipping_updated');
        }else{
            $action = lang('activity_shipping_created');
            $msg = lang('shipping_created');
        }
        
        $this->invoice_model->_table_name = 'tbl_shipping_calculator';
        $this->invoice_model->_primary_key = 'sc_id';
        $id = $this->invoice_model->save($data, $id);

        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'security deposit',
            'module_field_id' => $id,
            'activity' => $action,
            'icon' => 'fa-circle-o'
        );
        $this->invoice_model->_table_name = 'tbl_activities';
        $this->invoice_model->_primary_key = 'activities_id';
        $this->invoice_model->save($activity);

        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/security_deposit/search_shipping_calculator');

    }

    public function search_shipping_calculator(){
        $this->db->select('tbl_shipping_calculator.*, tbl_invoices.invoices_id as invoices_id, tbl_invoices.client_id as client_id, tbl_invoices.created_date as job_date, tbl_invoices.reference_no as reference_no, tbl_invoices.type as type, tbl_invoices.shipping_line as shipping_line, tbl_invoices.consignment_type as consignment_type,tbl_invoices.bl_no as bl_no');
        $this->db->from('tbl_shipping_calculator');
        $this->db->join('tbl_invoices', 'tbl_invoices.invoices_id = tbl_shipping_calculator.invoices_id');
        $data['shipping_info'] = $this->db->get()->result();
        if($_POST){
            if(isset($_POST['client_id'])){
                $client_id = $_POST['client_id'];
                $data['client_id'] = $client_id;
                $this->db->select('tbl_shipping_calculator.*, tbl_invoices.invoices_id as invoices_id, tbl_invoices.client_id as client_id, tbl_invoices.created_date as job_date, tbl_invoices.reference_no as reference_no, tbl_invoices.type as type, tbl_invoices.shipping_line as shipping_line, tbl_invoices.consignment_type as consignment_type, tbl_invoices.bl_no as bl_no');
                $this->db->from('tbl_shipping_calculator');
                $this->db->join('tbl_invoices', 'tbl_invoices.invoices_id = tbl_shipping_calculator.invoices_id');
                $this->db->where('tbl_invoices.client_id',$_POST['client_id']);
                $data['shipping_info'] = $this->db->get()->result();
            }
            if(isset($_POST['reference_no'])) {
                $reference_no = $_POST['reference_no'];
                $data['reference_no'] = $reference_no;
                $invoice_info = $this->invoice_model->check_by(array('reference_no' => $reference_no), 'tbl_invoices');

                if (!empty($invoice_info)){
                    $this->db->select('tbl_shipping_calculator.*, tbl_invoices.invoices_id as invoices_id, tbl_invoices.client_id as client_id, tbl_invoices.created_date as job_date, tbl_invoices.reference_no as reference_no, tbl_invoices.type as type, tbl_invoices.shipping_line as shipping_line, tbl_invoices.consignment_type as consignment_type, tbl_invoices.bl_no as bl_no');
                    $this->db->from('tbl_shipping_calculator');
                    $this->db->join('tbl_invoices', 'tbl_invoices.invoices_id = tbl_shipping_calculator.invoices_id');
                    $this->db->where('tbl_invoices.invoices_id', $invoice_info->invoices_id);
                    $invoice_info = $this->db->get()->result();
                }
                $data['shipping_info'] = $invoice_info;
            }
        }
        $this->invoice_model->_table_name = 'tbl_invoices';
        $this->invoice_model->_order_by = 'invoices_id';
        $data['all_invoices'] = $this->invoice_model->get();
        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'name';
        $data['all_client'] = $this->invoice_model->get();
        $this->invoice_model->_table_name = 'tbl_bills';
        $this->invoice_model->_order_by = 'bill_id';
        $data['all_bill'] = $this->invoice_model->get();

        $data['title'] = "Search Shipping Calculator";
        $subview = 'search_shipping_calculator';

        $data['subview'] = $this->load->view('admin/security_deposit/' . $subview, $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function manage_shipping($action = NULL, $id = NULL) {
        $data['page'] = lang('shipping');

        if (!empty($id)) {
            $data['title'] = "Manage Shipping";
            $this->db->select('*');
            $this->db->from('tbl_shipping_calculator');
            $this->db->join('tbl_invoices', 'tbl_invoices.invoices_id = tbl_shipping_calculator.invoices_id');
            $this->db->where('tbl_shipping_calculator.sc_id', decode($id));
            $data['shipping_info'] = $this->db->get()->row();
        }
        if ($action == 'shipping_details') {
            $data['title'] = "Manage Shipping";
            $this->db->select('*');
            $this->db->from('tbl_shipping_calculator');
            $this->db->join('tbl_invoices', 'tbl_invoices.invoices_id = tbl_shipping_calculator.invoices_id');
            $this->db->where('tbl_shipping_calculator.sc_id', decode($id));
            $data['shipping_info'] = $this->db->get()->row();
            $subview = 'shipping_details';
        }
        else{
            $subview = 'shipping_calculator';
        }

        $data['all_clients'] = $this->invoice_model->check_by_all(array('client_status'=>1),'tbl_client');
        $data['subview'] = 'admin/security_deposit/'.$subview;
        $this->load->view('admin/_layout_main2', $data);
    }

    public function delete_shipping($id)
    {
        $this->db->where('sc_id',decode($id))
            ->delete('tbl_shipping_calculator');
        if ($this->db->affected_rows()) {
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'security deposit',
                'module_field_id' => $id,
                'activity' => 'delete shipping calculator detail',
                'icon' => 'fa-circle-o'
            );
            $this->invoice_model->_table_name = 'tbl_activities';
            $this->invoice_model->_primary_key = 'activities_id';
            $this->invoice_model->save($activity);
            redirect('admin/security_deposit/search_shipping_calculator');
        } else {
            return false;
        }

    }

}
