<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('invoice_model');
    }

    /*     * * Create New User ** */

    public function create_user($id = null) {
        check_url();
        $data['title'] = 'Create User';

        if (!empty($id)) {
            $id = decrypt($id);
            $data['user_id'] = $id;
        } else {
            $data['user_id'] = null;
        }

        $this->user_model->_table_name = 'tbl_menu'; //table name
        $this->user_model->_order_by = 'menu_id';
        $menu_info = $this->user_model->get();

        foreach ($menu_info as $items) {
            $menu['parents'][$items->parent][] = $items;
        }

        $data['result'] = $this->buildChild(0, $menu);

        $this->user_model->_table_name = 'tbl_users'; //table name
        $this->user_model->_order_by = 'user_id';
        $data['user_login_details'] = $this->user_model->get_by(array('user_id' => $data['user_id']), true);

        if ($data['user_login_details']) {
            $role = $this->user_model->select_user_roll_by_id($data['user_id']);

            if ($role) {
                foreach ($role as $value) {
                    $result[$value->menu_id] = $value->menu_id;
                }

                $data['roll'] = $result;
            }
        } else {
            $data['user_login_details'] = $this->user_model->get_new_user();
        }

        $data['subview'] = $this->load->view('admin/user/create_user', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    /*     * * User Permission Level tree Builder ** */

    public function buildChild($parent, $menu) {
        if (isset($menu['parents'][$parent])) {
            foreach ($menu['parents'][$parent] as $ItemID) {
                if (!isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label] = $ItemID->menu_id;
                }
                if (isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label][$ItemID->menu_id] = self::buildChild($ItemID->menu_id, $menu);
                }
            }
        }

        return $result;
    }

    public function user_list($action = NULL, $id = NULL) {
        check_url();
        $user_id = decode($id);
        if ($action == 'edit_user') {
            $data['active'] = 2;
            $this->user_model->_table_name = 'tbl_users';
            $this->user_model->_order_by = 'user_id';
            $data['login_info'] = $this->user_model->get_by(array('user_id' => $user_id), true);

            if ($data['login_info']) {
                $role = $this->user_model->select_user_roll_by_id($user_id);
                if ($role) {
                    foreach ($role as $value) {
                        $result[$value->menu_id] = $value->menu_id;
                    }

                    $data['roll'] = $result;
                }
            }
        } else {
            $data['active'] = 1;
        }

        if($this->session->userdata('role_id') == 4 || $this->session->userdata('role_id') == 5){
            $this->db->select('*');
            $this->db->from('tbl_menu');
            $this->db->join('tbl_user_role', 'tbl_user_role.menu_id = tbl_menu.menu_id');
            $menu_info = $this->db->get()->result();
            foreach ($menu_info as $items) {
                $menu['parents'][$items->parent][] = $items;
            }
        }else{
            $this->user_model->_table_name = 'tbl_menu';
            $this->user_model->_order_by = 'menu_id';
            $menu_info = $this->user_model->get();

            foreach ($menu_info as $items) {
                $menu['parents'][$items->parent][] = $items;
            }
        }

        $data['result'] = $this->buildChild(0, $menu);
        $data['title'] = 'User List';
        $this->user_model->_table_name = 'tbl_client';
        $this->user_model->_order_by = 'client_id';
        $data['all_client_info'] = $this->user_model->get();

        $data['all_user_info'] = $this->user_model->check_by_all(array('role_id !=' => 0), 'tbl_users');
        $data['subview'] = $this->load->view('admin/user/user_list', $data, true);
        $data['subview'] = $this->load->view('admin/user/user_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    public function user_lists()
    {
        $search_array=array();
        $rows = array();
        $search_value=$_POST['search']['value'];
        if ($search_value){
            $search_array=array(
                'us.username'  => $search_value
            );
        }
        $length = 10; $start = 1;
        if($_POST['length'] != -1){
            $length = $_POST['length'];
            $start = $_POST['start'];
        }
        $user_list = $this->user_model->user_searching_list($search_array,$length,$start);
        $user_list_rows = $this->user_model->user_searching_list_rows($search_array);
        $i = $_POST['start']+1;
        foreach($user_list as $v_user){
            $account_info = $this->user_model->check_by(array('user_id' => $v_user->user_id), 'tbl_account_details');
            if (!empty($account_info)) {
                $department_info = $this->user_model->check_by(array('departments_id' => $account_info->departments_id), 'tbl_departments');
                if(!empty($department_info)){
                    $depart = $department_info->deptname;
                }else{
                    $depart = " - ";
                }
            }
            $img = '<img style="width: 36px;margin-right: 10px;" src="'.base_url($account_info->avatar ).'" class="img-circle">';
            if ($v_user->activated == 1){
                    $status = '<a data-toggle="tooltip" data-placement="top" title="Click to Deactive" href="'.base_url('admin/user/change_status/0/'.encrypt($v_user->user_id)).'">'.lang('active').'</a>';
                }else{
                    $status = '<a data-toggle="tooltip" data-placement="top" title="Click to Active "  href="'.base_url('admin/user/change_status/1/'.encrypt($v_user->user_id)).'">'.lang('deactive').'</a>';
                }
                if ($v_user->banned == 1) {
                $status = '<span class="label label-danger" data-toggle="tooltip" data-placement="top" title="'.$v_user->ban_reason.'">'.lang('banned').'</span>';
            }
            $user_type = "";
            if ($v_user->role_id == 1) {
                $user_type = 'Admin';
            } elseif ($v_user->role_id == 3) {
                $user_type = 'Staff';
            }
            $action = "";
            if ($v_user->user_id != 1) {
                if ($v_user->banned == 1) {
                    $action = '<a data-toggle = "tooltip" data-placement = "top" class="btn btn-success btn-xs" title = "Click to'.lang('unbanned').'"  href = "'.base_url('admin/user/set_banned/0/'.encode($v_user->user_id)).'" ><span class="fa fa-check" ></span ></a >';
                } else {
                    $action = btn_banned_modal('admin/user/change_banned/' . encode($v_user->user_id));
                }
                $action = $action." ".btn_view('admin/user/user_details/' . encode($v_user->user_id))." ".btn_edit('admin/user/user_list/edit_user/' . encode($v_user->user_id));
            }
            $rows[] = array(
                $img,
                ucfirst($account_info->fullname),
                $v_user->username,
                $status,
                $user_type,
                $depart,
                $account_info->city,
                $action
            );
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $user_list_rows,
            "recordsFiltered" => $user_list_rows,
            "data" => $rows
        );
        echo json_encode($output);
    }

    public function user_details($id) {
        check_url();
        $data['title'] = lang('user_details');
        $id = decode($id);
        $data['id'] = $id;
        $data['user_role'] = $this->user_model->select_user_roll_by_id($id);

        $data['subview'] = $this->load->view('admin/user/user_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    /*     * * Save New User ** */

    public function save_user($id = NULL) {
        $login_data = $this->user_model->array_from_post(array('username', 'email', 'role_id'));

        /*update root category*/
        $where = array('username' => $login_data['username']);
        $email = array('email' => $login_data['email']);
        /*duplicate value check in DB*/
        if (!empty($id)) {  /*if id exist in db update data*/
            $user_id = decrypt($id);
            $check_id = array('user_id !=' => $user_id);
        } else { /*if id is not exist then set id as null*/
            $check_id = null;
        }
        /*check whether this input data already exist or not*/
        $check_user = $this->user_model->check_update('tbl_users', $where, $check_id);
        $check_email = $this->user_model->check_update('tbl_users', $email, $check_id);
        if (!empty($check_user) || !empty($check_email)) {
            if (!empty($check_user)) {
                $error = $login_data['username'];
            } else {
                $error = $login_data['email'];
            }
            $type = 'error';
            $message = "<strong style='color:#000'>" . $error . '</strong>  ' . lang('already_exist');
        } else {  /*save and update query*/
            $login_data['last_ip'] = $this->input->ip_address();
            if (empty($id)) {
                $password = $this->input->post('password', TRUE);
                $login_data['password'] = $this->hash($password);
                $login_data['online_status'] = 0;
            }
            if (!empty($id)) {
                $this->user_model->_table_name = 'tbl_user_role';
                $this->user_model->_order_by = 'user_id';
                $this->user_model->_primary_key = 'user_role_id';
                $roll = $this->user_model->get_by(array('user_id' => $user_id), false);

                foreach ($roll as $v_roll) {
                    $this->user_model->delete($v_roll->user_role_id);
                }
            }

            $this->user_model->_table_name = 'tbl_users';
            $this->user_model->_primary_key = 'user_id';
            if (!empty($id)) {
                $user_id = decrypt($id);
                $id = $this->user_model->save($login_data, $user_id);
            } else {
                $id = $this->user_model->save($login_data);
            }

            $this->user_model->_table_name = 'tbl_user_role';
            $this->user_model->_primary_key = 'user_role_id';
            $menu = $this->user_model->array_from_post(array('menu'));
            if (!empty($menu['menu'])) {
                foreach ($menu as $v_menu) {
                    foreach ($v_menu as $value) {
                        $mdata['menu_id'] = $value;
                        $mdata['user_id'] = $id;
                        $this->user_model->save($mdata);
                    }
                }
            }

            /*save into tbl_account details*/
            if($this->session->userdata('role_id') == 4 || $this->session->userdata('role_id') == 5){
                $profile_data = $this->user_model->array_from_post(array('fullname', 'phone', 'mobile', 'skype', 'city'));
            }
            else{
                $profile_data = $this->user_model->array_from_post(array('fullname', 'company', 'phone', 'mobile', 'skype', 'city', 'departments_id'));
            }

            $account_details_id = $this->input->post('account_details_id', TRUE);
            if (!empty($_FILES['avatar']['name'])) {
                $val = $this->user_model->uploadImage('avatar');
                $val == TRUE || redirect('admin/user/user_list');
                $profile_data['avatar'] = $val['path'];
            }

            $profile_data['user_id'] = $id;

            $this->user_model->_table_name = 'tbl_account_details'; // table name
            $this->user_model->_primary_key = 'account_details_id'; // $id
            if (!empty($account_details_id)) {
                $account_details_id = decrypt($account_details_id);
                $this->user_model->save($profile_data, $account_details_id);
            } else {
                $this->user_model->save($profile_data);
            }
            if (!empty($user_id)) {
                $message = lang('user_updated');
                $act = lang('activity_updated_new_user');
            } else {
                $message = lang('user_added');
                $act = lang('activity_added_new_user');
            }
            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'user',
                'module_field_id' => $id,
                'activity' => $act,
                'icon' => 'fa-user',
                'value1' => $login_data['username']
            );
            $this->user_model->_table_name = 'tbl_activities';
            $this->user_model->_primary_key = "activities_id";
            $this->user_model->save($activities);
            
            $type = 'success';
        }
        set_message($type, $message);
        redirect('admin/user/user_list');
    }

    /* * * Delete User * * */

    public function delete_user($id = null) {
        if (!empty($id)) {
            $id = $id;
            $user_id = $this->session->userdata('user_id');

            //checking login user trying delete his own account
            if ($id == $user_id) {
                //same user can not delete his own account
                // redirect with error msg
                $type = 'error';
                $message = 'Sorry You can not delete your own account!';
                set_message($type, $message);
                redirect('admin/user/user_list'); //redirect page
            } else {
                //delete procedure run
                // Check user in db or not
                $this->user_model->_table_name = 'tbl_users'; //table name
                $this->user_model->_order_by = 'user_id';
                $result = $this->user_model->get_by(array('user_id' => $id), true);

                if (!empty($result)) {
                    //delete user roll id
                    $this->user_model->_table_name = 'tbl_account_details';
                    $this->user_model->delete_multiple(array('user_id' => $id));

                    $this->user_model->_table_name = 'tbl_activities';
                    $this->user_model->delete_multiple(array('user' => $id));

                    $this->user_model->_table_name = 'tbl_payments';
                    $this->user_model->delete_multiple(array('paid_by' => $id));

                    $this->user_model->_table_name = 'tbl_users';
                    $this->user_model->delete_multiple(array('user_id' => $id));

                    $this->user_model->_table_name = 'tbl_user_role';
                    $this->user_model->delete_multiple(array('user_id' => $id));

                    $this->user_model->_table_name = "tbl_todo"; // table name
                    $this->user_model->delete_multiple(array('user_id' => $id));

                    //redirect successful msg
                    $type = 'success';
                    $message = 'User Delete Successfully!';
                    set_message($type, $message);
                    redirect('admin/user/user_list'); //redirect page
                } else {
                    //redirect error msg
                    $type = 'error';
                    $message = 'Sorry this user not find in database!';
                    set_message($type, $message);
                    redirect('admin/user/user_list'); //redirect page
                }
            }
        }
    }

    public function change_status($flag, $id) {
        // if flag == 1 it is active user else deactive user
        if ($flag == 1) {
            $msg = 'Active';
        } else {
            $msg = 'Deactive';
        }
        $id = decrypt($id);
        $where = array('user_id' => $id);
        $action = array('activated' => $flag);
        $this->user_model->set_action($where, $action, 'tbl_users');

        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'user',
            'module_field_id' => $id,
            'activity' => lang('activity_change_user_status'),
            'icon' => 'fa-user',
            'value1' => $msg,
        );
        $this->user_model->_table_name = 'tbl_activities';
        $this->user_model->_primary_key = "activities_id";
        $this->user_model->save($activities);

        $type = "success";
        $message = "User " . $msg . " Successfully!";
        set_message($type, $message);
        redirect('admin/user/user_list'); //redirect page
    }

    public function set_banned($flag, $id) {

        if ($flag == 1) {
            $msg = lang('banned');
            $action = array('activated' => 0, 'banned' => $flag, 'ban_reason' => $this->input->post('ban_reason', TRUE));
        } else {
            $msg = lang('unbanned');
            $action = array('activated' => 1, 'banned' => $flag);
        }
        $id = decode($id);
        $where = array('user_id' => $id);

        $this->user_model->set_action($where, $action, 'tbl_users');

        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'user',
            'module_field_id' => $id,
            'activity' => lang('activity_change_user_status'),
            'icon' => 'fa-user',
            'value1' => $msg,
        );
        $this->user_model->_table_name = 'tbl_activities';
        $this->user_model->_primary_key = "activities_id";
        $this->user_model->save($activities);

        $type = "success";
        $message = "User " . $msg . " Successfully!";
        set_message($type, $message);
        redirect('admin/user/user_list'); //redirect page
    }

    public function change_banned($id) {
        $data['user_id'] = $id;
        $data['modal_subview'] = $this->load->view('admin/user/_modal_banned_reson', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function hash($string) {
        return hash('sha512', $string . config_item('encryption_key'));
    }
    
    /*crud for sidebar todo list*/
    function todo($task = '', $todo_id = '', $swap_with = '') {
        if ($task == 'add') {
            $this->add_todo();
        }
        if ($task == 'reload_incomplete_todo') {
            $this->get_incomplete_todo();
        }
        if ($task == 'mark_as_done') {
            $this->mark_todo_as_done($todo_id);
        }
        if ($task == 'mark_as_undone') {
            $this->mark_todo_as_undone($todo_id);
        }
        if ($task == 'swap') {

            $this->swap_todo($todo_id, $swap_with);
        }
        if ($task == 'delete') {
            $this->delete_todo($todo_id);
        }
        $todo['opened'] = 1;
        $this->session->set_userdata($todo);
        redirect('admin/dashboard/');
    }

    function add_todo() {
        $data['title'] = $this->input->post('title');
        $data['user_id'] = $this->session->userdata('user_id');

        $this->db->insert('tbl_todo', $data);
        $todo_id = $this->db->insert_id();

        $data['order'] = $todo_id;
        $this->db->where('todo_id', $todo_id);
        $this->db->update('tbl_todo', $data);
    }

    function mark_todo_as_done($todo_id = '') {
        $data['status'] = 1;
        $this->db->where('todo_id', $todo_id);
        $this->db->update('tbl_todo', $data);
    }

    function mark_todo_as_undone($todo_id = '') {
        $data['status'] = 0;
        $this->db->where('todo_id', $todo_id);
        $this->db->update('tbl_todo', $data);
    }

    function swap_todo($todo_id = '', $swap_with = '') {
        $counter = 0;
        $temp_order = $this->db->get_where('tbl_todo', array('todo_id' => $todo_id))->row()->order;
        $user = $this->session->userdata('user_id');

        // Move current todo up.
        if ($swap_with == 'up') {

            // Fetch all todo lists of current user in ascending order.
            $this->db->order_by('order', 'ASC');
            $todo_lists = $this->db->get_where('tbl_todo', array('user_id' => $user))->result_array();
            $array_length = count($todo_lists);

            // Create separate array for orders and todo_id's from above array.
            foreach ($todo_lists as $todo_list) {
                $id_list[] = $todo_list['todo_id'];
                $order_list[] = $todo_list['order'];
            }
        }

        // Move current todo down.
        if ($swap_with == 'down') {

            // Fetch all todo lists of current user in descending order.
            $this->db->order_by('order', 'DESC');
            $todo_lists = $this->db->get_where('tbl_todo', array('user_id' => $user))->result_array();
            $array_length = count($todo_lists);

            // Create separate array for orders and todo_id's from above array.
            foreach ($todo_lists as $todo_list) {
                $id_list[] = $todo_list['todo_id'];
                $order_list[] = $todo_list['order'];
            }
        }

        // Swap orders between current and next/previous todo.
        for ($i = 0; $i < $array_length; $i++) {
            if ($temp_order == $order_list[$i]) {
                if ($counter > 0) {
                    $swap_order = $order_list[$i - 1];
                    $swap_id = $id_list[$i - 1];

                    // Update order of current todo.
                    $data['order'] = $swap_order;
                    $this->db->where('todo_id', $todo_id);
                    $this->db->update('tbl_todo', $data);

                    // Update order of next/previous todo.
                    $data['order'] = $temp_order;
                    $this->db->where('todo_id', $swap_id);
                    $this->db->update('tbl_todo', $data);
                }
            } else
                $counter++;
        }
    }

    function delete_todo($todo_id = '') {
        $this->db->where('todo_id', $todo_id);
        $this->db->delete('tbl_todo');
    }

    function get_incomplete_todo() {
        $user = $this->session->userdata('user_id');
        $this->db->where('user_id', $user);
        $this->db->where('status', 0);
        $query = $this->db->get('tbl_todo');

        $incomplete_todo_number = $query->num_rows();
        if ($incomplete_todo_number > 0) {
            echo '<span class="badge badge-secondary">';
            echo $incomplete_todo_number;
            echo '</span>';
        }
    }

}
