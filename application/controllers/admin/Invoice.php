<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Invoice extends Admin_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('invoice_model');
        
        $this->load->helper('ckeditor');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "99.8%",
                'height' => "400px"
            )
        );
    }
    
    public function manage_invoice($action = NULL, $id = NULL, $item_id = NULL)
    {
        check_url();
        $data['page'] = lang('invoice');
        $data['title'] = "Consignment Details";
        if ($action == 'all_payments') {
            $data['sub_active'] = lang('payments_received');
        } else {
            $data['sub_active'] = lang('invoice');
        }
        if (!empty($item_id)) {
            $item_id = decode($item_id);
            $data['client_payment_info'] = $this->invoice_model->check_by(array('client_id' => $item_id), 'tbl_client');
        }

        if (!empty($id)) {
            $id = decode($id);
            $data['title'] = "Manage Invoice";
            $data['invoice_info'] = $this->invoice_model->check_by(array('invoices_id' => $id), 'tbl_invoices');
            $data['requisition_info'] = $this->invoice_model->check_by(array('invoices_id' => $id), 'tbl_requisitions');
            $data['bill_info'] = $this->invoice_model->check_by(array('invoices_id' => $id), 'tbl_bills');
            $data['invoice_documents_info'] = $this->invoice_model->check_by_all(array('invoices_id' => $id), 'tbl_invoice_documents');
            $data['due_dates_info'] = $this->invoice_model->check_by_all(array('invoices_id' => $id), 'tbl_due_dates');
            $data['container_info'] = $this->invoice_model->check_by_all(array('invoices_id' => $id), 'tbl_containers');
            $data['financial_info'] = $this->invoice_model->check_by(array('invoices_id' => $id), 'tbl_financial_information');
            $data['commodity_info'] = $this->invoice_model->check_by_all(array('invoices_id' => $id), 'tbl_saved_commodities');
            $data['job_activity_info'] = $this->invoice_model->check_by_all(array('invoices_id' => $id), 'tbl_job_activity');
            if(!empty($data['invoice_info']->client_id)){
                $data['consignors_info'] = $this->invoice_model->check_by_all(array('client_id' => $data['invoice_info']->client_id), 'tbl_consignors');
            }
        }
        
        /*** GET CLIENTS ***/
        $data['all_client'] = $this->invoice_model->check_by_all(array('client_status' => 1), 'tbl_client');
        /*** GET SUPPLIER ***/
        $data['all_supplier'] = $this->invoice_model->check_by_all(array('supplier_status' => 1), 'tbl_supplier');
        /*** GET CURRENCIES ***/
        $this->invoice_model->_table_name = 'tbl_currencies';
        $this->invoice_model->_order_by = 'code';
        $data['all_currencies'] = $this->invoice_model->get();
        /*** GET COUNTRIES ***/
        $this->invoice_model->_table_name = 'tbl_countries';
        $this->invoice_model->_order_by = 'id';
        $data['all_countries'] = $this->invoice_model->get();
        /*** GET JOB STATUSES ***/
        $this->invoice_model->_table_name = 'tbl_job_status';
        $this->invoice_model->_order_by = 'job_status_id';
        $data['all_job_status'] = $this->invoice_model->get();
        /*** GET JOB NUMBERS ***/
        $this->invoice_model->_table_name = 'tbl_invoices';
        $this->invoice_model->_order_by = 'invoices_id';
        $data['all_invoice_info'] = $this->invoice_model->get();
        /*** GET BANKS ***/
        $this->invoice_model->_table_name = 'banks';
        $this->invoice_model->_order_by = 'B_ID';
        $data['banks'] = $this->invoice_model->get();
        /*** GET DOCUMENT TYPES ***/
        $this->invoice_model->_table_name = 'tbl_document_types';
        $this->invoice_model->_order_by = 'document_type_id';
        $data['all_document_types'] = $this->invoice_model->get();
        /*** GET COMMODITY TYPES ***/
        $this->invoice_model->_table_name = 'tbl_commodity_types';
        $this->invoice_model->_order_by = 'commodity_type_id';
        $data['all_commodity_types'] = $this->invoice_model->get();
        /*** GET SHIPPING LINE ***/
        $this->invoice_model->_table_name = 'tbl_shipping_line';
        $this->invoice_model->_order_by = 'shipping_id';
        $data['all_shipping_info'] = $this->invoice_model->get();
        /*** GET YARDS ***/
        $this->invoice_model->_table_name = 'tbl_yards';
        $this->invoice_model->_order_by = 'yard_id';
        $data['all_yard_info'] = $this->invoice_model->get();
        /*** GET SHEDS ***/
        $this->invoice_model->_table_name = 'tbl_sheds';
        $this->invoice_model->_order_by = 'shed_id';
        $data['all_shed_info'] = $this->invoice_model->get();
        /*** GET INCOTERM ***/
        $this->invoice_model->_table_name = 'tbl_incoterms';
        $this->invoice_model->_order_by = 'incoterm_id';
        $data['all_incoterm_info'] = $this->invoice_model->get();
        /*** GET WEIGHT UNIT ***/
        $this->invoice_model->_table_name = 'tbl_weight_units';
        $this->invoice_model->_order_by = 'weight_unit_id';
        $data['all_weight_unit_info'] = $this->invoice_model->get();
        /*** GET GD TYPE ***/
        $this->invoice_model->_table_name = 'tbl_gd_types';
        $this->invoice_model->_order_by = 'gd_type_id';
        $data['all_gd_type_info'] = $this->invoice_model->get();
        /*** GET JOB TYPE ***/
        $this->invoice_model->_table_name = 'stbjobtype';
        $this->invoice_model->_order_by = 'Iid';
        $data['all_job_type_info'] = $this->invoice_model->get();
        
        if ($action == 'invoice_details') {

            $data['title'] = "Invoice Details";
            $data['client_info'] = $this->invoice_model->check_by(array('client_id' => $data['invoice_info']->client_id), 'tbl_client');
            $data['invoice_documents_info'] = $this->invoice_model->check_by_all(array('invoices_id' => $id), 'tbl_invoice_documents');
            $subview = 'invoice_details';
        } elseif ($action == 'payment') {

            $data['title'] = "Invoice Payment";
            $subview = 'payment';

        } elseif ($action == 'payments_details') {
            $data['title'] = "Payments Details";
            $subview = 'payments_details';
            // get payment info
            $this->invoice_model->_table_name = 'tbl_payments';
            $this->invoice_model->_order_by = 'invoices_id';
            $data['all_payments_info'] = $this->invoice_model->get_by(array('invoices_id' => $item_id), FALSE);
            // get payment info by id
            $this->invoice_model->_table_name = 'tbl_payments';
            $this->invoice_model->_order_by = 'payments_id';
            $data['payments_info'] = $this->invoice_model->get_by(array('payments_id' => $id), TRUE);
        } elseif ($action == 'invoice_history') {
            $data['title'] = "Invoice History";
            $subview = 'invoice_history';
        } elseif ($action == 'email_invoice') {
            $data['title'] = "Email Invoice";
            $subview = 'email_invoice';
            $data['editor'] = $this->data;
        } elseif ($action == 'send_reminder') {
            $data['title'] = "Send Remainder";
            $subview = 'send_reminder';
            $data['editor'] = $this->data;
        } elseif ($action == 'send_overdue') {
            $data['title'] = lang('send_invoice_overdue'); //Page title
            $subview = 'send_overdue';
            $data['editor'] = $this->data;
        } elseif ($action == 'pdf_invoice') {
            $data['title'] = "Invoice PDF"; //Page title
            $this->load->helper('dompdf');
            $viewfile = $this->load->view('admin/invoice/pdf_invoice', $data, TRUE);
            pdf_create($viewfile, 'Invoice  # ' . $data['invoice_info']->reference_no);
        } else {
            $subview = 'manage_invoice';
        }
        $user_id = $this->session->userdata('user_id');
        
        $user_info = $this->invoice_model->check_by(array('user_id' => $user_id), 'tbl_users');
        $data['role'] = $user_info->role_id;
        /*$data['subview'] = $this->load->view('admin/invoice/' . $subview, $data, TRUE);*/
        $data['subview'] = 'admin/invoice/' . $subview;
        $this->load->view('admin/_layout_main2', $data);
    }
    
    /*public function all_invoices() {
        check_url();
        $data['title'] = "All Jobs";
        $subview = 'all_invoices';

        $this->invoice_model->_table_name = 'tbl_invoices';
        $this->invoice_model->_order_by = 'reference_no';
        $data['all_invoices'] = $this->invoice_model->get_by(array('invoices_id !=' => '0'), FALSE);

        $user_id = $this->session->userdata('user_id');
        $user_info = $this->invoice_model->check_by(array('user_id' => $user_id), 'tbl_users');
        $data['role'] = $user_info->role_id;

        $data['subview'] = $this->load->view('admin/invoice/' . $subview, $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }*/
    
    public function search_invoice()
    {
        check_url();
        if ($_POST) {
            if (isset($_POST['client_id'])) {
                $data['client_id'] = $_POST['client_id'];
                $data['all_invoice_info'] = $this->invoice_model->check_by_all(array('client_id' => $_POST['client_id']), 'tbl_invoices');
            }
            if (isset($_POST['reference_no'])) {
                $data['reference_no'] = $_POST['reference_no'];
                $data['all_invoice_info'] = $this->invoice_model->check_by_all(array('reference_no' => $_POST['reference_no']), 'tbl_invoices');
            }
        }
        
        $this->invoice_model->_table_name = 'tbl_invoices';
        $this->invoice_model->_order_by = 'invoices_id';
        $data['all_invoices'] = $this->invoice_model->get();
        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'name';
        $data['all_client'] = $this->invoice_model->get();
        $data['title'] = "Search Job Number";
        $subview = 'search_invoice';
        $data['subview'] = $this->load->view('admin/invoice/' . $subview, $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
    
    public function hs_code()
    {
        $hs_code = $_POST['hs_code'];
        $get_hs_code = $this->invoice_model->check_by(array('commodity' => $hs_code), 'tbl_saved_commodities');
        if (!empty($get_hs_code)) {
            $value = $get_hs_code->hs_code;
            echo json_encode($value);
            die();
        } else {
            echo json_encode("");
            die();
        }
    }
    
    public function reference_no($id = null)
    {
        $reference_no = $_POST['reference_no'];
        $this->db->select('reference_no');
        $this->db->from('tbl_invoices');
        $this->db->where('job_no', 'Manual');
        $this->db->where('reference_no', $reference_no);
        if (!empty($id)) {
            $id = decode($id);
            $this->db->where('invoices_id !=', $id);
        }
        $get_reference_no = $this->db->get()->row();
        if (!empty($get_reference_no)) {
            echo json_encode(false);
            die();
        } else {
            echo json_encode(true);
            die();
        }
    }
    
    public function convert_amount()
    {
        $amount = $_POST['amount'];
        $converted = $this->invoice_model->convert_number($amount);
        echo json_encode($converted);
        die();
    }
    
    public function document_date_update()
    {
        $invoice_id = $_POST['invoice_id'];
        $colmun = $_POST['column'];
        $value = $_POST['value'];
        $type = $_POST['type'];
        $where = array('invoices_id' => $invoice_id);
        $data = array($colmun . "_" . $type . "_datetime" => date('Y-m-d H:i:s'), $colmun . "_" . $type => $value);
        $this->invoice_model->set_action($where, $data, 'tbl_invoice_documents');
        
        echo json_encode(true);
        die();
    }
    
    public function all_payments($id = NULL)
    {
        check_url();
        if (!empty($id)) {
            $id = decode($id);
            $data['invoice_info'] = $this->invoice_model->check_by(array('invoices_id' => $id), 'tbl_invoices');
            $data['title'] = "Edit Payments";
            $subview = 'edit_payments';
        } else {
            $id = $id;
            $data['title'] = "All Payments"; //Page title
            $subview = 'all_payments';
        }
        // get payment info
        $this->invoice_model->_table_name = 'tbl_payments';
        $this->invoice_model->_order_by = 'invoices_id';
        $data['all_payments_info'] = $this->invoice_model->get_by(array('invoices_id !=' => '0'), FALSE);
        
        // get payment info by id
        $this->invoice_model->_table_name = 'tbl_payments';
        $this->invoice_model->_order_by = 'payments_id';
        $data['payments_info'] = $this->invoice_model->get_by(array('payments_id' => $id), TRUE);
        
        // get all banks
        $this->invoice_model->_table_name = 'banks';
        $this->invoice_model->_order_by = 'B_ID';
        $data['banks'] = $this->invoice_model->get();
        
        $user_id = $this->session->userdata('user_id');
        $user_info = $this->invoice_model->check_by(array('user_id' => $user_id), 'tbl_users');
        $data['role'] = $user_info->role_id;
        
        $data['subview'] = $this->load->view('admin/invoice/' . $subview, $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function all_payment()
    {
        $search_array=array();
        $rows = array();
        $search_value=$_POST['search']['value'];
        if ($search_value){
            $search_array=array(
                'pay.payment_date'  => $search_value
            );
        }
        $length = 10; $start = 1;
        if($_POST['length'] != -1){
            $length = $_POST['length'];
            $start = $_POST['start'];
        }
        $payment_list = $this->invoice_model->payment_searching_list($search_array,$length,$start);
        $payment_list_rows = $this->invoice_model->payment_searching_list_rows($search_array);
        $i = $_POST['start']+1;
        foreach($payment_list as $payment){
            $invoice_info = $this->invoice_model->check_by(array('invoices_id' => $payment->invoices_id), 'tbl_invoices');
            $client_info = $this->invoice_model->check_by(array('client_id' => $invoice_info->client_id), 'tbl_client');

            $rows[] = array(
                strftime(config_item('date_format'), strtotime($payment->payment_date)),
                strftime(config_item('date_format'), strtotime($payment->created_date)),
                $this->invoice_model->job_no_creation($invoice_info->invoices_id),
                $client_info->name,
                number_format($payment->amount, 2),
                $payment->received_by,
                ($payment->payment_type == 1)?'Cash':'Bank',
                btn_edit('admin/invoice/all_payments/' . encode($payment->payments_id))." ".
                btn_view('admin/invoice/manage_invoice/payments_details/' . encode($payment->payments_id)."/".encode($payment->invoices_id))." ".
                btn_delete('admin/invoice/delete/delete_payment/' . encrypt($payment->payments_id))
            );
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $payment_list_rows,
            "recordsFiltered" => $payment_list_rows,
            "data" => $rows
        );
        echo json_encode($output);
    }
    
    public function save_invoice($id = NULL)
    {
        if (!empty($id)) {
            $data = $this->invoice_model->array_from_post(array('collectorate', 'project_name', 'mode', 'mode_options', 'p_o_l', 'p_o_d', 'client_id', 'port', 'gross_weight', 'net_weight','bl_no', 'bl_date', 'igm_no', 'index_no', 'shipping_line', 'forwarding_agent', 'currency', 'igm_date', 'consignment_type', 'exchange_rate', 'eta'));
            $data['supplier_id'] = (!empty($_POST['supplier_id'])) ? $_POST['supplier_id'] : NULL;
            $data['consignor_id'] = (!empty($_POST['consignor_id'])) ? $_POST['consignor_id'] : NULL;
            
            if (!empty($_POST['reference_no'])) {
                $data['reference_no'] = $_POST['reference_no'];
            }
            $invoice_id = decrypt($id);
            $data['updated_by'] = $this->session->userdata('user_id');
            $this->db->trans_begin();
            $data = $this->security->xss_clean($data);
            $this->invoice_model->_table_name = 'tbl_invoices';
            $this->invoice_model->_primary_key = 'invoices_id';
            $this->invoice_model->save($data, $invoice_id);
            /*** FINANCIAL SECTION ***/
            $financial_data = $this->invoice_model->array_from_post(array('invoice_no', 'invoice_date', 'payment_term', 'incoterm', 'currency', 'lc_no', 'lc_date', 'lc_bank', 'fob_value', 'freight','other_charges', 'exchange_rate', 'eif_no'));
            $financial_data['invoices_id'] = $invoice_id;
            $financial_data = $this->security->xss_clean($financial_data);
            $this->invoice_model->_table_name = 'tbl_financial_information';
            $this->invoice_model->_primary_key = 'financial_id';
            $this->invoice_model->save($financial_data, decrypt($_POST['financial_id']));
            /*** END FINANCIAL SECTION ***/
            /*** FCL OR LCL MULTI FIELDS INSERTION ***/
            if (!empty($_POST['consignment_type'])) {
                $this->invoice_model->_table_name = 'tbl_containers';
                $this->invoice_model->delete_multiple(array('invoices_id' => $invoice_id));
                if ($_POST['consignment_type'] == 'FCL') {
                    if (!empty($_POST['container_type_fcl'])) {
                        for ($i = 0; $i < count($_POST['container_type_fcl']); $i++) {
                            $container_data = array(
                                'invoices_id' => $invoice_id,
                                'container_type' => $_POST['container_type_fcl'][$i],
                                'container_no' => $_POST['container_no_fcl'][$i],
                                'container_ft' => $_POST['container_ft_fcl'][$i],
                                'packages' => $_POST['no_of_pkgs_fcl'][$i],
                                'container_total_weight' => $_POST['total_weight_fcl'][$i]
                            );
                            $container_data = $this->security->xss_clean($container_data);
                            $this->invoice_model->_table_name = 'tbl_containers';
                            $this->invoice_model->_primary_key = 'id';
                            $this->invoice_model->save($container_data);
                        }
                    }
                }else if($_POST['consignment_type'] == 'LCL'){
                    if (!empty($_POST['container_shed_lcl'])) {
                        for ($i = 0; $i < count($_POST['container_shed_lcl']); $i++) {
                            $container_data = array(
                                'invoices_id' => $invoice_id,
                                'container_shed' => $_POST['container_shed_lcl'][$i],
                                'packages' => $_POST['no_of_pkgs_lcl'][$i],
                                'container_total_weight' => $_POST['total_weight_lcl'][$i]
                            );
                            $container_data = $this->security->xss_clean($container_data);
                            $this->invoice_model->_table_name = 'tbl_containers';
                            $this->invoice_model->_primary_key = 'id';
                            $this->invoice_model->save($container_data);
                        }
                    }
                }else if($_POST['consignment_type'] == 'By Air'){
                    if (!empty($_POST['container_shed_air'])) {
                        for ($i = 0; $i < count($_POST['container_shed_air']); $i++) {
                            $container_data = array(
                                'invoices_id' => $invoice_id,
                                'container_shed' => $_POST['container_shed_air'][$i],
                                'packages' => $_POST['no_of_pkgs_air'][$i],
                                'container_net_weight' => $_POST['total_net_weight_air'][$i],
                                'container_gross_weight' => $_POST['total_gross_weight_air'][$i]
                            );
                            $container_data = $this->security->xss_clean($container_data);
                            $this->invoice_model->_table_name = 'tbl_containers';
                            $this->invoice_model->_primary_key = 'id';
                            $this->invoice_model->save($container_data);
                        }
                    }
                }else{
                    if (!empty($_POST['no_of_pkgs'])) {
                        $container_data = array(
                            'invoices_id' => $invoice_id,
                            'packages' => $_POST['no_of_pkgs'],
                            'container_net_weight' => $_POST['total_net_weight'],
                            'container_gross_weight' => $_POST['total_gross_weight']
                        );
                        $container_data = $this->security->xss_clean($container_data);
                        $this->invoice_model->_table_name = 'tbl_containers';
                        $this->invoice_model->_primary_key = 'id';
                        $this->invoice_model->save($container_data);
                    }
                }
            }
            /*** END FCL OR LCL MULTI FIELDS INSERTION ***/
            /*** COMMODITY SECTION ***/
            $commodity_id = $this->input->post('commodity_id', TRUE);
            $commodity_type_update = $this->input->post('commodity_type_update', TRUE);
            $commodity_update = $this->input->post('commodity_update', TRUE);
            $hs_code_update = $this->input->post('hs_code_update', TRUE);
            $weight_unit_update = $this->input->post('weight_unit_update', TRUE);
            $no_of_units_update = $this->input->post('no_of_units_update', TRUE);
            $unit_value_assessed_update = $this->input->post('unit_value_assessed_update', TRUE);
            $unit_value_update = $this->input->post('unit_value_update', TRUE);
            $no_of_pcs_update = $this->input->post('no_of_pcs_update', TRUE);
            $import_mode_update = $this->input->post('import_mode_update', TRUE);
            $sro_update = $this->input->post('sro_update', TRUE);
            for ($i = 0; $i < count($commodity_id); $i++) {
                $import_mode_update = (!empty($import_mode_update[$i])) ? $import_mode_update[$i] : 'No';
                $sro_update = (!empty($sro_update[$i])) ? $sro_update[$i] : 'No';
                $commodity_update_data = array('commodity_type' => $commodity_type_update[$i], 'commodity' => $commodity_update[$i], 'hs_code' => $hs_code_update[$i], 'weight_unit' => $weight_unit_update[$i], 'no_of_units' => $no_of_units_update[$i], 'unit_value' => $unit_value_update[$i],
                    'unit_value_assessed'=>$unit_value_assessed_update[$i], 'no_of_pcs'=>$no_of_pcs_update[$i],'temporary_import' => $import_mode_update, 'sro' => $sro_update);
                if($import_mode_update == 'Yes'){
                    $commodity_data['security'] = $_POST['security_update'][$i];
                    $commodity_data['instrument_no'] = $_POST['inst_no_update'][$i];
                    $commodity_data['temporary_amount'] = $_POST['import_amount_update'][$i];
                    $commodity_data['date_of_issue'] = $_POST['date_of_issue_update'][$i];
                    $commodity_data['date_of_expiry'] = $_POST['date_of_expiry_update'][$i];
                    $commodity_data['shipping_bill'] = $_POST['shipping_bill_no_update'][$i];
                    $commodity_data['shipping_date'] = $_POST['shipping_date_update'][$i];
                    $commodity_data['permanent_retention'] = $_POST['permanent_retention_update'][$i];
                    $commodity_data['temporary_remarks'] = $_POST['import_remarks_update'][$i];
                }
                if($sro_update == "Yes"){
                    $commodity_data['sro_no'] = $_POST['sro_no_update'][$i];
                    $commodity_data['sro_date'] = $_POST['sro_date_update'][$i];
                    $commodity_data['serial_no'] = $_POST['serial_no_update'][$i];
                    $commodity_data['sro_remarks'] = $_POST['sro_remarks_update'][$i];
                }
                $commodity_update_data = $this->security->xss_clean($commodity_update_data);
                $this->db->where('id', decrypt($commodity_id[$i]));
                $this->db->update("tbl_saved_commodities", $commodity_update_data);
            }
    
            $commodity_type = $this->input->post('commodity_type', TRUE);
            $commodity = $this->input->post('commodity', TRUE);
            $hs_code = $this->input->post('hs_code', TRUE);
            $weight_unit = $this->input->post('weight_unit', TRUE);
            $no_of_units = $this->input->post('no_of_units', TRUE);
            $unit_value = $this->input->post('unit_value', TRUE);
            $unit_value_assessed = $this->input->post('unit_value_assessed', TRUE);
            $no_of_pcs = $this->input->post('no_of_pcs', TRUE);
            $import_mode = $this->input->post('import_mode', TRUE);
            $sro = $this->input->post('sro', TRUE);
            $sro_no = $this->input->post('sro_no', TRUE);
            $serial_no = $this->input->post('serial_no', TRUE);
            $sro_date = $this->input->post('sro_date', TRUE);
            if (!empty($commodity)) {
                for ($i = 0; $i < count($commodity); $i++) {
                    $import_mode2 = (!empty($import_mode[$i])) ? $import_mode[$i] : 'No';
                    $sro2 = (!empty($sro[$i])) ? $sro[$i] : 'No';
                    $sro_no2 = (!empty($sro_no[$i])) ? $sro_no[$i] : '';
                    $serial_no2 = (!empty($serial_no[$i])) ? $serial_no[$i] : '';
                    $sro_date2 = (!empty($sro_date[$i])) ? $sro_date[$i] : '';
                    $commodity_data = array(
                        'invoices_id' => $invoice_id,
                        'commodity_type' => $commodity_type[$i],
                        'commodity' => $commodity[$i],
                        'hs_code' => $hs_code[$i],
                        'weight_unit' => $weight_unit[$i],
                        'no_of_units' => $no_of_units[$i],
                        'unit_value' => $unit_value[$i],
                        'unit_value_assessed' => $unit_value_assessed[$i],
                        'no_of_pcs' => $no_of_pcs[$i],
                        'temporary_import' => $import_mode2,
                        'sro' => $sro2,
                        'sro_no' => $sro_no2,
                        'serial_no' => $serial_no2,
                        'sro_date' => $sro_date2
                    );
                    if($import_mode2 == 'Yes'){
                        $commodity_data['security'] = $_POST['security'][$i];
                        $commodity_data['instrument_no'] = $_POST['inst_no'][$i];
                        $commodity_data['temporary_amount'] = $_POST['import_amount'][$i];
                        $commodity_data['date_of_issue'] = $_POST['date_of_issue'][$i];
                        $commodity_data['date_of_expiry'] = $_POST['date_of_expiry'][$i];
                        $commodity_data['shipping_bill'] = $_POST['shipping_bill_no'][$i];
                        $commodity_data['shipping_date'] = $_POST['shipping_date'][$i];
                        $commodity_data['permanent_retention'] = $_POST['permanent_retention'][$i];
                        $commodity_data['temporary_remarks'] = $_POST['import_remarks'][$i];
                    }
                    if($sro2 == "Yes"){
                        $commodity_data['sro_no'] = $_POST['sro_no'][$i];
                        $commodity_data['sro_date'] = $_POST['sro_date'][$i];
                        $commodity_data['serial_no'] = $_POST['serial_no'][$i];
                        $commodity_data['sro_remarks'] = $_POST['sro_remarks'][$i];
                    }
                    $commodity_data = $this->security->xss_clean($commodity_data);
                    $this->invoice_model->_table_name = 'tbl_saved_commodities';
                    $this->invoice_model->_primary_key = 'id';
                    $this->invoice_model->save($commodity_data);
                }
            }
            /*** END COMMODITY SECTION ***/
            /*** DUE DATES SECTION ***/
            $due_date_id = $this->input->post('due_date_id', TRUE);
            $due_date_update = $this->input->post('due_date_update', TRUE);
            $due_time_update = $this->input->post('due_time_update', TRUE);
            $due_comment_update = $this->input->post('due_comment_update', TRUE);
            $due_date = $this->input->post('due_date', TRUE);
            $due_time = $this->input->post('due_time', TRUE);
            $due_comment = $this->input->post('due_comment', TRUE);
            if (!empty($due_date_id)) {
                for ($i = 0; $i < count($due_date_id); $i++) {
                    $due_date_data = array('due_date' => $due_date_update[$i], 'due_time' => $due_time_update[$i], 'due_comment' => $due_comment_update[$i]);
                    $due_date_data = $this->security->xss_clean($due_date_data);
                    $this->db->where('due_date_id', decrypt($due_date_id[$i]));
                    $this->db->update("tbl_due_dates", $due_date_data);
                }
            }
            if (!empty($due_date)) {
                for ($x = 0; $x < count($due_date); $x++) {
                    $due_data = array(
                        'invoices_id' => $invoice_id,
                        'due_date' => $due_date[$x],
                        'due_time' => $due_time[$x],
                        'due_comment' => $due_comment[$x]
                    );
                    $due_date = $this->security->xss_clean($due_date);
                    $this->invoice_model->_table_name = 'tbl_due_dates';
                    $this->invoice_model->_primary_key = 'due_date_id';
                    $this->invoice_model->save($due_data);
                }
                $due_date_count = count($due_date) - 1;
                $data['due_date'] = $due_date[$due_date_count];
            } else {
                $due_date_count = count($due_date_update) - 1;
                $data['due_date'] = $due_date_update[$due_date_count];
            }
            /*** DUE DATES SECTION ***/
            /*** JOB STATUS ACTIVITY ENTRIES ***/
            $job_activity_id = $this->input->post('job_activity_id', TRUE);
            $job_activity_date_update = $this->input->post('job_activity_date_update', TRUE);
            $job_activity_status_update = $this->input->post('job_activity_status_update', TRUE);
            $job_activity_notification_update = $this->input->post('job_activity_notification_update', TRUE);
            $job_activity_date = $this->input->post('job_activity_date', TRUE);
            $job_activity_status = $this->input->post('job_activity_status', TRUE);
            $job_activity_notification = $this->input->post('job_activity_notification', TRUE);
            for ($i = 0; $i < count($job_activity_id); $i++) {
                $job_activity = array('job_activity_date' => $job_activity_date_update[$i], 'job_activity_status' => $job_activity_status_update[$i], 'job_activity_notification' => $job_activity_notification_update[$i]);
                $job_activity = $this->security->xss_clean($job_activity);
                $this->db->where('job_activity_id', decrypt($job_activity_id[$i]));
                $this->db->update("tbl_job_activity", $job_activity);
            }
            if (!empty($job_activity_date)) {
                for ($x = 0; $x < count($job_activity_date); $x++) {
                    $notification = (!empty($job_activity_notification[$x])) ? $job_activity_notification[$x] : 'No';
                    $job_activity_data = array(
                        'invoices_id' => $invoice_id,
                        'job_activity_date' => $job_activity_date[$x],
                        'job_activity_status' => $job_activity_status[$x],
                        'job_activity_notification' => $notification
                    );
                    $job_activity_data = $this->security->xss_clean($job_activity_data);
                    $this->invoice_model->_table_name = 'tbl_job_activity';
                    $this->invoice_model->_primary_key = 'job_activity_id';
                    $this->invoice_model->save($job_activity_data);
                }
            }
            /*** END JOB STATUS ACTIVITY ***/
            /*** DOCUMENTS SECTION ***/
            /*** UPDATE ENTRIES ***/
            $document_id = $this->input->post('document_id', TRUE);
            $document_title_update = $this->input->post('document_title_update', TRUE);
            $document_date_update = $this->input->post('document_date_update', TRUE);
            $document_type_update = $this->input->post('document_type_update', TRUE);
            $document_comment_update = $this->input->post('document_comment_update', TRUE);
            
            if (!empty($document_id)) {
                for ($i = 0; $i < count($document_id); $i++) {
                    $print_image = array();
                    $_FILES['file']['name'] = $_FILES['document_file_update']['name'][$i];
                    $_FILES['file']['type'] = $_FILES['document_file_update']['type'][$i];
                    $_FILES['file']['tmp_name'] = $_FILES['document_file_update']['tmp_name'][$i];
                    $_FILES['file']['error'] = $_FILES['document_file_update']['error'][$i];
                    $_FILES['file']['size'] = $_FILES['document_file_update']['size'][$i];
                    
                    $config['upload_path'] = 'uploads/documents/';
                    $config['allowed_types'] = '*';
                    $config['max_size'] = '5000';
                    $config['file_name'] = $_FILES['document_file_update']['name'][$i];
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('file')) {
                        $error = array('error' => $this->upload->display_errors());
                    } else {
                        $data1 = $this->upload->data();
                        $print_image = 'uploads/documents/' . $data1['file_name'];
                    }
                    if (!empty($print_image)) {
                        $document_array = array('document_title' => $document_title_update[$i], 'document_date' => $document_date_update[$i], 'document_file' => $print_image, 'document_type' => $document_type_update[$i], 'document_comment' => $document_comment_update[$i]);
                    } else {
                        $document_array = array('document_title' => $document_title_update[$i], 'document_date' => $document_date_update[$i], 'document_type' => $document_type_update[$i], 'document_comment' => $document_comment_update[$i]);
                    }
                    $document_array = $this->security->xss_clean($document_array);
                    $this->db->where('document_id', decrypt($document_id[$i]));
                    $this->db->update("tbl_invoice_documents", $document_array);
                }
            }
            /*** END UPDATE ENTRIES ***/
            /*** NEW ENTRIES ***/
            $document_title = $this->input->post('document_title', TRUE);
            $document_date = $this->input->post('document_date', TRUE);
            $document_type = $this->input->post('document_type', TRUE);
            $document_comment = $this->input->post('document_comment', TRUE);
            
            for ($i = 0; $i < count($document_title); $i++) {
                if (!empty($document_title[$i])) {
                    $print_image = "";
                    if (!empty($_FILES['document_file']['name'][$i])) {
                        $_FILES['file']['name'] = $_FILES['document_file']['name'][$i];
                        $_FILES['file']['type'] = $_FILES['document_file']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['document_file']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['document_file']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['document_file']['size'][$i];
                        
                        $config['upload_path'] = 'uploads/documents/';
                        $config['allowed_types'] = '*';
                        $config['max_size'] = '5000';
                        $config['file_name'] = $_FILES['document_file']['name'][$i];
                        $this->load->library('upload', $config);
                        if (!$this->upload->do_upload('file')) {
                            $error = array('error' => $this->upload->display_errors());
                        } else {
                            $data1 = $this->upload->data();
                            $print_image = 'uploads/documents/' . $data1['file_name'];
                        }
                    }
                    $document_data = array(
                        'invoices_id' => $invoice_id,
                        'document_title' => $document_title[$i],
                        'document_date' => $document_date[$i],
                        'document_type' => $document_type[$i],
                        'document_comment' => $document_comment[$i],
                        'document_file' => $print_image,
                    );
                    $document_data = $this->security->xss_clean($document_data);
                    $this->invoice_model->_table_name = 'tbl_invoice_documents';
                    $this->invoice_model->_primary_key = 'document_id';
                    $this->invoice_model->save($document_data);
                }
            }
            /*** END NEW ENTRIES ***/
            /*** END DOCUMENTS SECTION ***/
            
            $action = lang('activity_invoice_updated');
            $msg = lang('invoice_updated');
            $url = 'admin/invoice/manage_invoice/create_invoice/' . encode($invoice_id);
        } else {
            $data = $this->invoice_model->array_from_post(array('type', 'collectorate', 'project_name', 'mode', 'mode_options', 'p_o_l', 'p_o_d', 'client_id', 'port', 'gross_weight', 'net_weight','total_pkgs','bl_no', 'bl_date', 'igm_no', 'index_no', 'shipping_line', 'forwarding_agent', 'currency', 'igm_date', 'consignment_type', 'exchange_rate', 'eta'));
            $data['supplier_id'] = (!empty($_POST['supplier_id'])) ? $_POST['supplier_id'] : NULL;;
            $data['consignor_id'] = (!empty($_POST['consignor_id'])) ? $_POST['consignor_id'] : NULL;
            $this->db->trans_begin();
            /*** GENERATE AND CHECK JOB NO ***/
            if (config_item('job_number_auto') == 'Yes') {
                $reference_no = $this->invoice_model->generate_invoice_number($_POST['type']);
                $job_no = 'Auto';
            } else {
                $reference_no = $_POST['reference_no'];
                $job_no = 'Manual';
            }
            $data['reference_no'] = $reference_no;
            $data['job_no'] = $job_no;
            $data['created_by'] = $this->session->userdata('user_id');
            $data = $this->security->xss_clean($data);
            $this->invoice_model->_table_name = 'tbl_invoices';
            $this->invoice_model->_primary_key = 'invoices_id';
            $invoice_id = $this->invoice_model->save($data);
            
            /*** FINANCIAL SECTION ***/
            $financial_data = $this->invoice_model->array_from_post(array('invoice_no', 'invoice_date', 'payment_term', 'incoterm', 'currency', 'lc_no', 'lc_date', 'lc_bank', 'fob_value', 'freight','other_charges', 'exchange_rate', 'eif_no'));
            $financial_data['invoices_id'] = $invoice_id;
            $financial_data = $this->security->xss_clean($financial_data);
            $this->invoice_model->_table_name = 'tbl_financial_information';
            $this->invoice_model->_primary_key = 'financial_id';
            $this->invoice_model->save($financial_data);
            /*** END FINANCIAL SECTION ***/
            
            /*** FCL OR LCL MULTI FIELDS INSERTION ***/
            if (!empty($_POST['consignment_type'])) {
                if ($_POST['consignment_type'] == 'FCL') {
                    if (!empty($_POST['container_type'])) {
                        for ($i = 0; $i < count($_POST['container_type']); $i++) {
                            $container_data = array(
                                'invoices_id' => $invoice_id,
                                'container_type' => $_POST['container_type'][$i],
                                'container_no' => $_POST['container_no'][$i],
                                'container_ft' => $_POST['container_ft'][$i],
                                'packages' => $_POST['no_of_pkgs'][$i],
                                'container_total_weight' => $_POST['total_weight'][$i]
                            );
                            $container_data = $this->security->xss_clean($container_data);
                            $this->invoice_model->_table_name = 'tbl_containers';
                            $this->invoice_model->_primary_key = 'id';
                            $this->invoice_model->save($container_data);
                        }
                    }
                }else if($_POST['consignment_type'] == 'LCL'){
                    if (!empty($_POST['container_shed'])) {
                        for ($i = 0; $i < count($_POST['container_shed']); $i++) {
                            $container_data = array(
                                'invoices_id' => $invoice_id,
                                'container_shed' => $_POST['container_shed'][$i],
                                'packages' => $_POST['no_of_pkgs'][$i],
                                'container_total_weight' => $_POST['total_weight'][$i]
                            );
                            $container_data = $this->security->xss_clean($container_data);
                            $this->invoice_model->_table_name = 'tbl_containers';
                            $this->invoice_model->_primary_key = 'id';
                            $this->invoice_model->save($container_data);
                        }
                    }
                }else if($_POST['consignment_type'] == 'By Air'){
                    if (!empty($_POST['container_shed_air'])) {
                        for ($i = 0; $i < count($_POST['container_shed_air']); $i++) {
                            $container_data = array(
                                'invoices_id' => $invoice_id,
                                'container_shed' => $_POST['container_shed_air'][$i],
                                'packages' => $_POST['no_of_pkgs'][$i],
                                'container_net_weight' => $_POST['total_net_weight'][$i],
                                'container_gross_weight' => $_POST['total_gross_weight'][$i]
                            );
                            $container_data = $this->security->xss_clean($container_data);
                            $this->invoice_model->_table_name = 'tbl_containers';
                            $this->invoice_model->_primary_key = 'id';
                            $this->invoice_model->save($container_data);
                        }
                    }
                }else{
                    if (!empty($_POST['no_of_pkgs'])) {
                        $container_data = array(
                            'invoices_id' => $invoice_id,
                            'packages' => $_POST['no_of_pkgs'],
                            'container_net_weight' => $_POST['total_net_weight'],
                            'container_gross_weight' => $_POST['total_gross_weight']
                        );
                        $container_data = $this->security->xss_clean($container_data);
                        $this->invoice_model->_table_name = 'tbl_containers';
                        $this->invoice_model->_primary_key = 'id';
                        $this->invoice_model->save($container_data);
                    }
                }
            }
            /*** END FCL OR LCL MULTI FIELDS INSERTION ***/
            /*** MULTIPLE COMMODITIES INSERTION ***/
            $commodity_type = $this->input->post('commodity_type', TRUE);
            $commodity = $this->input->post('commodity', TRUE);
            $hs_code = $this->input->post('hs_code', TRUE);
            $weight_unit = $this->input->post('weight_unit', TRUE);
            $no_of_units = $this->input->post('no_of_units', TRUE);
            $unit_value = $this->input->post('unit_value', TRUE);
            $unit_value_assessed = $this->input->post('unit_value_assessed', TRUE);
            $no_of_pcs = $this->input->post('no_of_pcs', TRUE);
            $import_mode = $this->input->post('import_mode', TRUE);
            $sro = $this->input->post('sro', TRUE);
            $sro_no = $this->input->post('sro_no', TRUE);
            $serial_no = $this->input->post('serial_no', TRUE);
            $sro_date = $this->input->post('sro_date', TRUE);
            if (!empty($commodity)) {
                for ($i = 0; $i < count($commodity); $i++) {
                    $import_mode2 = (!empty($import_mode[$i])) ? $import_mode[$i] : 'No';
                    $sro2 = (!empty($sro[$i])) ? $sro[$i] : 'No';
                    $sro_no2 = (!empty($sro_no[$i])) ? $sro_no[$i] : '';
                    $serial_no2 = (!empty($serial_no[$i])) ? $serial_no[$i] : '';
                    $sro_date2 = (!empty($sro_date[$i])) ? $sro_date[$i] : '';
                    $commodity_data = array(
                        'invoices_id' => $invoice_id,
                        'commodity_type' => $commodity_type[$i],
                        'commodity' => $commodity[$i],
                        'hs_code' => $hs_code[$i],
                        'weight_unit' => $weight_unit[$i],
                        'no_of_units' => $no_of_units[$i],
                        'unit_value' => $unit_value[$i],
                        'unit_value_assessed' => $unit_value_assessed[$i],
                        'no_of_pcs' => $no_of_pcs[$i],
                        'temporary_import' => $import_mode2,
                        'sro' => $sro2,
                        'sro_no' => $sro_no2,
                        'serial_no' => $serial_no2,
                        'sro_date' => $sro_date2
                    );
                    if($import_mode2 == 'Yes'){
                        $commodity_data['security'] = $_POST['security'][$i];
                        $commodity_data['instrument_no'] = $_POST['inst_no'][$i];
                        $commodity_data['temporary_amount'] = $_POST['import_amount'][$i];
                        $commodity_data['date_of_issue'] = $_POST['date_of_issue'][$i];
                        $commodity_data['date_of_expiry'] = $_POST['date_of_expiry'][$i];
                        $commodity_data['shipping_bill'] = $_POST['shipping_bill_no'][$i];
                        $commodity_data['shipping_date'] = $_POST['shipping_date'][$i];
                        $commodity_data['permanent_retention'] = $_POST['permanent_retention'][$i];
                        $commodity_data['temporary_remarks'] = $_POST['import_remarks'][$i];
                    }
                    if($sro2 == "Yes"){
                        $commodity_data['sro_no'] = $_POST['sro_no'][$i];
                        $commodity_data['sro_date'] = $_POST['sro_date'][$i];
                        $commodity_data['serial_no'] = $_POST['serial_no'][$i];
                        $commodity_data['sro_remarks'] = $_POST['sro_remarks'][$i];
                    }
                    $commodity_data = $this->security->xss_clean($commodity_data);
                    $this->invoice_model->_table_name = 'tbl_saved_commodities';
                    $this->invoice_model->_primary_key = 'id';
                    $this->invoice_model->save($commodity_data);
                }
            }
            /*** END MULTIPLE COMMODITIES INSERTION ***/
            /*** JOB STATUS ACTIVITY ENTRIES ***/
            $job_activity_date = $this->input->post('job_activity_date', TRUE);
            $job_activity_status = $this->input->post('job_activity_status', TRUE);
            $job_activity_notification = $this->input->post('job_activity_notification', TRUE);
            if (!empty($job_activity_date)) {
                for ($x = 0; $x < count($job_activity_date); $x++) {
                    $notification = (!empty($job_activity_notification[$x])) ? $job_activity_notification[$x] : 'No';
                    $job_activity_data = array(
                        'invoices_id' => $invoice_id,
                        'job_activity_date' => $job_activity_date[$x],
                        'job_activity_status' => $job_activity_status[$x],
                        'job_activity_notification' => $notification
                    );
                    $job_activity_data = $this->security->xss_clean($job_activity_data);
                    $this->invoice_model->_table_name = 'tbl_job_activity';
                    $this->invoice_model->_primary_key = 'job_activity_id';
                    $this->invoice_model->save($job_activity_data);
                }
            }
            /*** END JOB STATUS ACTIVITY ***/
            /*** DOCUMENTS SECTION MULTI FIELDS INSERTION ***/
            $document_title = $this->input->post('document_title', TRUE);
            $document_date = $this->input->post('document_date', TRUE);
            $document_type = $this->input->post('document_type', TRUE);
            $document_comment = $this->input->post('document_comment', TRUE);
            
            for ($i = 0; $i < count($document_title); $i++) {
                if (!empty($document_title[$i])) {
                    $print_image = "";
                    if (!empty($_FILES['document_file']['name'][$i])) {
                        $_FILES['file']['name'] = $_FILES['document_file']['name'][$i];
                        $_FILES['file']['type'] = $_FILES['document_file']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['document_file']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['document_file']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['document_file']['size'][$i];
                        
                        $config['upload_path'] = 'uploads/documents/';
                        $config['allowed_types'] = '*';
                        $config['max_size'] = '5000';
                        $config['file_name'] = $_FILES['document_file']['name'][$i];
                        $this->load->library('upload', $config);
                        if (!$this->upload->do_upload('file')) {
                            $error = array('error' => $this->upload->display_errors());
                        } else {
                            $data1 = $this->upload->data();
                            $print_image = 'uploads/documents/' . $data1['file_name'];
                        }
                    }
                    $document_data = array(
                        'invoices_id' => $invoice_id,
                        'document_title' => $document_title[$i],
                        'document_date' => $document_date[$i],
                        'document_type' => $document_type[$i],
                        'document_comment' => $document_comment[$i],
                        'document_file' => $print_image,
                    );
                    $document_data = $this->security->xss_clean($document_data);
                    $this->invoice_model->_table_name = 'tbl_invoice_documents';
                    $this->invoice_model->_primary_key = 'document_id';
                    $this->invoice_model->save($document_data);
                }
            }
            /*** END DOCUMENTS SECTION MULTI FIELDS INSERTION ***/
            $action = lang('activity_invoice_created');
            $msg = lang('invoice_created');
            $url = 'admin/invoice/manage_invoice';
        }
        $job_no = $this->invoice_model->job_no_creation($invoice_id);
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'job',
            'module_field_id' => $invoice_id,
            'activity' => $action,
            'icon' => 'fa-circle-o',
            'value1' => $job_no
        );
        $activity = $this->security->xss_clean($activity);
        $this->invoice_model->_table_name = 'tbl_activities';
        $this->invoice_model->_primary_key = 'activities_id';
        $this->invoice_model->save($activity);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            set_message('error', 'Something went wrong!');
            redirect($url);
        }else {
            $this->db->trans_commit();
            $type = "success";
            $message = $msg;
            set_message($type, $message);
            $this->session->set_flashdata('message_name', $job_no);
            redirect($url);
        }
    }
    
    public function change_status($action, $id)
    {
        $where = array('invoices_id' => $id);
        if ($action == 'hide') {
            $data = array('show_client' => 'No');
        } else {
            $data = array('show_client' => 'Yes');
        }
        $this->invoice_model->set_action($where, $data, 'tbl_invoices');
        // messages for user
        $type = "success";
        $message = lang('invoice_' . $action);
        set_message($type, $message);
        redirect('admin/invoice/manage_invoice/invoice_details/' . $id);
    }
    
    public function change_job_status($flag, $id)
    {
        $id = decrypt($id);
        $where = array('invoices_id' => $id);
        $action = array('job_status' => $flag);
        $this->invoice_model->set_action($where, $action, 'tbl_invoices');
        $status = ($flag == 1) ? 'Active' : 'Deactive';
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'invoice',
            'module_field_id' => $id,
            'activity' => 'Job Status Successfully changed',
            'icon' => 'fa-file',
            'value1' => $status,
        );
        $this->invoice_model->_table_name = 'tbl_activities';
        $this->invoice_model->_primary_key = "activities_id";
        $this->invoice_model->save($activities);
        
        $type = "success";
        $message = "Job " . $status . " Successfully!";
        set_message($type, $message);
        redirect('admin/invoice/search_invoice');
    }
    
    public function delete($action, $invoices_id, $item_id = NULL, $document_path = NULL)
    {
        check_url();
        $invoices_id = decrypt($invoices_id);
        $item_id = decrypt($item_id);
        $document_path = decrypt($document_path);
        if ($action == 'delete_payment') {
            $this->invoice_model->_table_name = 'tbl_payments';
            $this->invoice_model->_primary_key = 'payments_id';
            $this->invoice_model->delete($invoices_id);
            $this->invoice_model->_table_name = 'transactions_meta';
            $this->invoice_model->delete_multiple(array('PAYMENT_ID' => $invoices_id));
            $module = 'Bill Payment';
        } elseif ($action == 'delete_due_dates') {
            $this->invoice_model->_table_name = 'tbl_due_dates';
            $this->invoice_model->_primary_key = 'due_date_id';
            $this->invoice_model->delete($invoices_id);
            $module = 'Job Due Date';
        } elseif ($action == 'delete_invoice_document') {
            $this->invoice_model->_table_name = 'tbl_invoice_documents';
            $this->invoice_model->_primary_key = 'document_id';
            $this->invoice_model->delete($item_id);
            unlink("uploads/documents/" . $document_path);
            $module = 'Job Document';
        } elseif ($action == 'delete_invoice_document_file') {
            $this->db->where('document_id', $item_id);
            $this->db->update("tbl_invoice_documents", array('document_file' => ""));
            unlink("uploads/documents/" . $document_path);
            $module = 'Job Document File';
        } elseif ($action == 'delete_commodity') {
            $this->invoice_model->_table_name = 'tbl_custom_duties';
            $this->invoice_model->delete_multiple(array('commodity_id' => $document_path));
            $this->invoice_model->_table_name = 'tbl_saved_commodities';
            $this->invoice_model->_primary_key = 'id';
            $this->invoice_model->delete($document_path);
            $module = 'Job Commodity Deleted';
        } elseif ($action == 'delete_job_activity') {
            $this->invoice_model->_table_name = 'tbl_job_activity';
            $this->invoice_model->_primary_key = 'job_activity_id';
            $this->invoice_model->delete($item_id);
            $module = 'Job Activity Deleted';
        }
        $type = "success";
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => $module,
            'module_field_id' => $invoices_id,
            'activity' => 'Delete ' . $module,
            'icon' => 'fa-trash',
        );
        $this->invoice_model->_table_name = 'tbl_activities';
        $this->invoice_model->_primary_key = 'activities_id';
        $this->invoice_model->save($activity);
        $invoices_id = encode($invoices_id);
        $item_id = encode($item_id);
        if ($action == 'delete_item') {
            $text = lang('invoice_item_deleted');
            set_message($type, $text);
            redirect('admin/invoice/manage_invoice/invoice_details/' . $invoices_id);
        } elseif ($action == 'delete_payment') {
            $text = lang('payment_deleted');
            set_message($type, $text);
            redirect('admin/invoice/all_payments');
        } elseif ($action == 'delete_due_dates') {
            $text = "Due Date has been removed";
            set_message($type, $text);
            redirect('admin/invoice/manage_invoice/create_invoice/' . $item_id);
        } elseif ($action == 'delete_invoice_document') {
            $text = "Document has been removed";
            set_message($type, $text);
            redirect('admin/invoice/manage_invoice/create_invoice/' . $invoices_id);
        } elseif ($action == 'delete_invoice_document_file') {
            $text = "File has been removed";
            set_message($type, $text);
            redirect('admin/invoice/manage_invoice/create_invoice/' . $invoices_id);
        } elseif ($action == 'delete_commodity') {
            $text = "Commodity has been removed";
            set_message($type, $text);
            redirect('admin/invoice/manage_invoice/create_invoice/' . $item_id);
        } elseif ($action == 'delete_job_activity') {
            $text = "Job activity has been removed";
            set_message($type, $text);
            redirect('admin/invoice/manage_invoice/create_invoice/' . $invoices_id);
        } else {
            $text = lang('deleted_invoice');
            set_message($type, $text);
            redirect('admin/invoice/manage_invoice');
        }
    }
    
    public function get_payment($invoices_id)
    {

        $invoices_id = decrypt($invoices_id);
        $due = round($this->invoice_model->calculate_to('invoice_due', $invoices_id), 2);
        $inv_info = $this->invoice_model->check_by(array('invoices_id' => $invoices_id), 'tbl_invoices');
        $paid_amount = $this->input->post('amount', TRUE);
        
        if ($paid_amount != 0) {
            if ($paid_amount > $due) {
                $type = "error";
                $message = lang('overpaid_amount');
                set_message($type, $message);
                redirect('admin/invoice/manage_invoice/payment/' . $invoices_id . '/' . $inv_info->client_id);
            } else {
                $data = array(
                    'invoices_id' => $invoices_id,
                    'paid_by' => $inv_info->client_id,
                    /*'payment_method' => $this->input->post('payment_method', TRUE),*/
                    'payment_type' => $this->input->post('payment_type', TRUE),
                    'branch_id' => $this->input->post('branch_id', TRUE),
                    'currency' => $this->input->post('currency', TRUE),
                    'amount' => $paid_amount,
                    'received_by' => $this->input->post('received_by', TRUE),
                    'payment_date' => date('Y-m-d', strtotime($this->input->post('payment_date', TRUE))),
                    'trans_id' => $this->input->post('trans_id'),
                    'cheque_payorder_no' => $this->input->post('cheque_payorder_no'),
                    'notes' => $this->input->post('notes'),
                    'month_paid' => date("m", strtotime($this->input->post('payment_date', TRUE))),
                    'year_paid' => date("Y", strtotime($this->input->post('payment_date', TRUE))),
                );
                
                $this->invoice_model->_table_name = 'tbl_payments';
                $this->invoice_model->_primary_key = 'payments_id';
                $payment_id = $this->invoice_model->save($data);
                
                $bill_info = $this->invoice_model->check_by(array('invoices_id' => $invoices_id), 'tbl_bills');
                $accounts_info = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Liabilities', 'NAME' => 'Current Liabilities'), 'sub_heads');
                $accounts_info = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $accounts_info->SUB_HEAD_ID, 'H_NAME' => 'Advances from clients'), 'accounts_head');
                $accounts_info = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $accounts_info->SUB_HEAD_ID, 'H_ID' => $accounts_info->H_ID, 'CLIENT_ID' => $inv_info->client_id, 'A_NAME' => client_name($inv_info->client_id) . ' (Advances from clients)'), 'accounts');
                $accounts_info = $this->invoice_model->check_by(array('A_ID' => $accounts_info->A_ID, 'T_ID' => $bill_info->transaction_id), 'transactions_meta');
                $data = array(
                    'T_ID' => $accounts_info->T_ID,
                    'A_ID' => $accounts_info->A_ID,
                    'PAYMENT_ID' => $payment_id,
                    'TM_AMOUNT' => $paid_amount,
                    'TM_TYPE' => 'Credit',
                    'IS_ACTIVE' => 1
                );
                $this->invoice_model->_table_name = 'transactions_meta';
                $this->invoice_model->_primary_key = 'TM_ID';
                $this->invoice_model->save($data);
                
                $payment_type = $_POST['payment_type'];
                $account_id = $_POST['account_id'];
                if ($payment_type == '1') {
                    $data = array(
                        'T_ID' => $accounts_info->T_ID,
                        'A_ID' => $payment_type,
                        'PAYMENT_ID' => $payment_id,
                        'TM_AMOUNT' => $paid_amount,
                        'TM_TYPE' => 'Debit',
                        'IS_ACTIVE' => 1
                    );
                } else {
                    $data = array(
                        'T_ID' => $accounts_info->T_ID,
                        'A_ID' => $account_id,
                        'PAYMENT_ID' => $payment_id,
                        'TM_AMOUNT' => $paid_amount,
                        'TM_TYPE' => 'Debit',
                        'IS_ACTIVE' => 1
                    );
                }
                $this->invoice_model->_table_name = 'transactions_meta';
                $this->invoice_model->_primary_key = 'TM_ID';
                $this->invoice_model->save($data);
                
                $activity = array(
                    'user' => $this->session->userdata('user_id'),
                    'module' => 'Bill Payment',
                    'module_field_id' => $invoices_id,
                    'activity' => lang('activity_new_payment'),
                    'icon' => 'fa-usd',
                    'value1' => $inv_info->reference_no,
                    'value2' => 'PKR ' . $paid_amount,
                );
                $this->invoice_model->_table_name = 'tbl_activities';
                $this->invoice_model->_primary_key = 'activities_id';
                $this->invoice_model->save($activity);
                
                /*if ($this->input->post('send_thank_you') == 'on') {
                    $this->send_payment_email($invoices_id, $paid_amount);
                }*/
            }
        }
        $type = "success";
        $message = lang('generate_payment');
        set_message($type, $message);
        redirect('admin/invoice/manage_invoice/invoice_details/' . encode($invoices_id));
    }
    
    public function update_payment($payments_id)
    {
        $payments_id = decrypt($payments_id);
        $payments_info = $this->invoice_model->check_by(array('payments_id' => $payments_id), 'tbl_payments');
        $due = round($this->invoice_model->calculate_to('invoice_due', $payments_info->invoices_id), 2);
        $inv_info = $this->invoice_model->check_by(array('invoices_id' => $payments_info->invoices_id), 'tbl_invoices');
        $paid_amount = $this->input->post('amount', TRUE);
        $amounts = $this->invoice_model->check_by(array('payments_id' => $payments_id), 'tbl_payments');
        $total_amount = $amounts->amount + $due;
        
        if ($paid_amount != 0) {
            if ($paid_amount > $total_amount) {
                $type = "error";
                $message = lang('overpaid_amount');
                set_message($type, $message);
                redirect('admin/invoice/all_payments/' . $payments_id);
            } else {
                $data = array(
                    'payment_type' => $this->input->post('payment_type', TRUE),
                    'branch_id' => $this->input->post('branch_id', TRUE),
                    'amount' => $paid_amount,
                    'received_by' => $this->input->post('received_by', TRUE),
                    'payment_date' => date('Y-m-d', strtotime($this->input->post('payment_date', TRUE))),
                    'cheque_payorder_no' => $this->input->post('cheque_payorder_no'),
                    'notes' => $this->input->post('notes'),
                    'month_paid' => date("m", strtotime($this->input->post('payment_date', TRUE))),
                    'year_paid' => date("Y", strtotime($this->input->post('payment_date', TRUE)))
                );
                
                $this->invoice_model->_table_name = 'tbl_payments';
                $this->invoice_model->_primary_key = 'payments_id';
                $this->invoice_model->save($data, $payments_id);
                
                
                $bill_info = $this->invoice_model->check_by(array('invoices_id' => $payments_info->invoices_id), 'tbl_bills');
                $accounts_info = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Assets', 'NAME' => 'Current Assets'), 'sub_heads');
                $accounts_info = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $accounts_info->SUB_HEAD_ID, 'H_NAME' => 'Account Receivables'), 'accounts_head');
                $accounts_info = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $accounts_info->SUB_HEAD_ID, 'H_ID' => $accounts_info->H_ID, 'CLIENT_ID' => $inv_info->client_id, 'A_NAME' => client_name($inv_info->client_id) . ' (Account Receivables)'), 'accounts');
                $accounts_info = $this->invoice_model->check_by(array('A_ID' => $accounts_info->A_ID, 'T_ID' => $bill_info->transaction_id), 'transactions_meta');
                $data = array(
                    'TM_AMOUNT' => $paid_amount
                );
                $this->db->where('A_ID', $accounts_info->A_ID);
                $this->db->where('PAYMENT_ID', $payments_id);
                $this->db->update("transactions_meta", $data);
                
                $payment_type = $_POST['payment_type'];
                $account_id = $_POST['account_id'];
                $trans_account_id = $_POST['trans_account_id'];
                if ($payment_type == '1') {
                    $data = array(
                        'A_ID' => $payment_type,
                        'TM_AMOUNT' => $paid_amount
                    );
                } else {
                    $data = array(
                        'A_ID' => $account_id,
                        'TM_AMOUNT' => $paid_amount
                    );
                }
                $this->invoice_model->_table_name = 'transactions_meta';
                $this->invoice_model->_primary_key = 'TM_ID';
                $this->invoice_model->save($data, $trans_account_id);
                $activity = array(
                    'user' => $this->session->userdata('user_id'),
                    'module' => 'Bill Payment',
                    'module_field_id' => $payments_id,
                    'activity' => lang('activity_update_payment'),
                    'icon' => 'fa-usd',
                    'value1' => $inv_info->reference_no,
                    'value2' => 'PKR ' . $paid_amount,
                );
                $this->invoice_model->_table_name = 'tbl_activities';
                $this->invoice_model->_primary_key = 'activities_id';
                $this->invoice_model->save($activity);
            }
            
        }
        
        $type = "success";
        $message = lang('generate_payment');
        set_message($type, $message);
        redirect('admin/invoice/all_payments');
    }
    
    public function get_client_consignors($id)
    {
        $consignors = $this->invoice_model->check_by_all(array('client_id' => $id, 'consignor_status' => 1), 'tbl_consignors');
        echo json_encode($consignors);
        die();
    }
    
    public function get_consignor_details($id)
    {
        $consignors = $this->invoice_model->check_by(array('consignor_id' => $id), 'tbl_consignors');
        echo json_encode($consignors);
        die();
    }
    
    /*** Server Side Listing ***/
    public function all_invoices()
    {
        check_url();
        $data['title'] = "All Jobs";
        $subview = 'all_invoices';
        
        if ($_POST) {
            $data['client_id'] = $_POST['client_id'];
            $data['reference_no'] = $_POST['reference_no'];
            $data['commodities'] = $_POST['commodity'];
            $data['bl_no'] = $_POST['bl_no'];
        }
        
        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'name';
        $data['all_client'] = $this->invoice_model->get();
        
        $user_id = $this->session->userdata('user_id');
        $user_info = $this->invoice_model->check_by(array('user_id' => $user_id), 'tbl_users');
        $data['role'] = $user_info->role_id;
        
        $data['subview'] = $this->load->view('admin/invoice/' . $subview, $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
    
    public function all_invoice($client_id = null, $reference_no = null, $bl_no = null, $commodity = null)
    {
        $search_array = array();
        $rows = array();
        $search_value = $_POST['search']['value'];
        if ($search_value) {
            $search_array = array(
                'cl.client_id' => $search_value,
                'inv.reference_no' => $search_value,
                'inv.bl_no' => $search_value,
                'comm.commodity' => $search_value
            );
        }
        $length = 10;
        $start = 1;
        if ($_POST['length'] != -1) {
            $length = $_POST['length'];
            $start = $_POST['start'];
        }
        $invoice_search_list = $this->invoice_model->invoice_searching_list($search_array, $length, $start, $client_id, $reference_no, $bl_no, $commodity);
        $invoice_search_list_rows = $this->invoice_model->invoice_searching_list_rows($search_array, $client_id, $reference_no, $bl_no, $commodity);
        $i = $_POST['start'] + 1;
        foreach ($invoice_search_list as $invoices) {
            $job_no = $this->invoice_model->job_no_creation($invoices->invoices_id);
            $client_info = $this->invoice_model->check_by(array('client_id' => $invoices->client_id), 'tbl_client');
            $bill_info = $this->invoice_model->check_by(array('invoices_id' => $invoices->invoices_id), 'tbl_bills');
            if (!empty($bill_info)) {
                $amount = "PKR " . number_format($this->invoice_model->calculate_bill('bill_grand_total', $bill_info->bill_id), 2);
            } else {
                $amount = 0;
            }
            if (!empty($invoices)) {
                $due_amount = "PKR " . number_format($this->invoice_model->calculate_to('invoice_due', $invoices->invoices_id), 2);
            }
            if ($this->invoice_model->get_payment_status($invoices->invoices_id) == lang('fully_paid')) {
                $invoice_status = lang('fully_paid');
                $label = "success";
            } elseif ($this->invoice_model->get_payment_status($invoices->invoices_id) == lang('partially_paid')) {
                $invoice_status = lang('partially_paid');
                $label = "info";
            } else {
                $invoice_status = lang('not_paid');
                $label = "danger";
            }
            $status = "<label class='label label-" . $label . "'>" . $invoice_status . "</label>";
            
            $action = btn_edit('admin/invoice/manage_invoice/create_invoice/' . encode($invoices->invoices_id)) . " " . btn_view('admin/invoice/manage_invoice/invoice_details/' . encode($invoices->invoices_id));;
            
            $rows[] = array(
                $job_no,
                $client_info->name,
                $amount,
                $due_amount,
                $status,
                $action
            );
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $invoice_search_list_rows,
            "recordsFiltered" => $invoice_search_list_rows,
            "data" => $rows
        );
        echo json_encode($output);
    }
    /*** Delivery Item ***/
    public function manage_delivery($action = NULL , $id = NULL){

        $data['title'] = "Create Delivery Section";
        $subview = 'manage_delivery_section';

        $data['all_reference_no'] = $this->invoice_model->get_reference_no();

        if($action == 'edit_delivery'){
            $data['invoice_info'] = $this->invoice_model->check_by(array('invoices_id' => $id), 'tbl_invoices');
        }

        $data['subview'] = 'admin/invoice/' . $subview;
        $this->load->view('admin/_layout_main2', $data);
    }

    public function type_consignment()
    {
        $id = $_POST['job_no'];
        $html[] = '';
        if (!empty($id)) {
            $consignment_type = $this->invoice_model->get_any_field('tbl_invoices', array('invoices_id' => $id), 'consignment_type');
            $container_info = $this->invoice_model->check_by_all(array('invoices_id' => $id), 'tbl_containers');

            $type = $consignment_type;
            foreach ($container_info as $container) {
                $shed_info = $this->invoice_model->check_by(array('shed_id' => $container->container_shed), 'tbl_sheds');
                if($consignment_type == 'FCL'){
                    $html[] = '<div class="col-lg-12"><div class="form-group">';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Container No</label><input type="text" class="form-control" name="container_no[]" value="'.$container->container_no.'" readonly/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Type</label><input type="text" class="form-control" name="type[]" value="'.$container->container_type.'" readonly/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Container ft</label><input type="text" class="form-control" name="container_ft[]" value="'.$container->container_ft.'" readonly/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">No. of Pkgs</label><input type="text" class="form-control" name="no_of_pkgs[]" value="'.$container->packages.'" readonly/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Total Weight</label><input type="text" class="form-control" name="total_weight[]" value="'.$container->container_total_weight.'" readonly/></div>';
                    $html[] .= '<input type="hidden" name="invoices_id[]" value="'.$container->invoices_id.'"></div>';
                    $html[] .= '<input type="hidden" name="container_id[]" value="'.$container->id.'"></div></div>';
                    $html[] .= '<div class="col-lg-12"><div class="form-group">';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Truck No</label><input type="text" class="form-control" name="truck_no[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Bilty</label><input type="text" class="form-control" name="bilty[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Date of Delivery</label><input type="date" class="form-control datepicker" name="delivery_date[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Date of Receipt at Site</label><input type="date" class="form-control datepicker" name="receipt_site[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Weight Declare</label><input type="text" class="form-control" name="weight_declare[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Weight Found</label><input type="text" class="form-control" name="weight_found[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Transporter</label><input type="text" class="form-control" name="transporter[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">No of Pkgs Received</label><input type="text" class="form-control" name="pkgs_receive[]"/></div>';
                    $html[] .= '<div class="col-lg-4"><label class="control-label">Remarks</label><textarea class="form-control" name="remarks[]"></textarea></div>';
                    $html[] .= '</div></div>';

                }elseif ($consignment_type == 'LCL'){
                    $html[] = '<div class="col-lg-12"><div class="form-group">';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Shed</label><input type="text" class="form-control" name="shed[]" value="'.$shed_info->shed.'" readonly/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">No. of Pkgs</label><input type="text" class="form-control" name="no_of_pkgs[]" value="'.$container->packages.'" readonly/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Total Weight</label><input type="text" class="form-control" name="total_weight[]" value="'.$container->container_total_weight.'" readonly/></div>';
                    $html[] .= '<input type="hidden" name="invoices_id[]" value="'.$container->invoices_id.'"></div>';
                    $html[] .= '<input type="hidden" name="container_id[]" value="'.$container->id.'"></div></div>';
                    $html[] .= '<div class="col-lg-12"><div class="form-group">';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Truck No</label><input type="text" class="form-control" name="truck_no[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Bilty</label><input type="text" class="form-control" name="bilty[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Date of Delivery</label><input type="date" class="form-control datepicker" name="delivery_date[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Date of Receipt at Site</label><input type="date" class="form-control datepicker" name="receipt_site[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Weight Declare</label><input type="text" class="form-control" name="weight_declare[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Weight Found</label><input type="text" class="form-control" name="weight_found[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Transporter</label><input type="text" class="form-control" name="transporter[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">No of Pkgs Received</label><input type="text" class="form-control" name="pkgs_receive[]"/></div>';
                    $html[] .= '<div class="col-lg-4"><label class="control-label">Remarks</label><textarea class="form-control" name="remarks[]"></textarea></div>';
                    $html[] .= '</div></div>';
                }elseif ($consignment_type == 'Break Bulk'){
                    $html[] = '<div class="col-lg-12"><div class="form-group">';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">No. of Pkgs</label><input type="text" class="form-control" name="no_of_pkgs[]" value="'.$container->packages.'" readonly/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Total Net Weight</label><input type="text" class="form-control" name="total_net_weight[]" value="'.$container->container_net_weight.'" readonly/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Total Gross Weight</label><input type="text" class="form-control" name="total_gross_weight[]" value="'.$container->container_gross_weight.'" readonly/></div>';
                    $html[] .= '<input type="hidden" name="invoices_id[]" value="'.$container->invoices_id.'"></div>';
                    $html[] .= '<input type="hidden" name="container_id[]" value="'.$container->id.'"></div></div>';
                    $html[] .= '<div class="col-lg-12"><div class="form-group">';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Truck No</label><input type="text" class="form-control" name="truck_no[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Bilty</label><input type="text" class="form-control" name="bilty[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Date of Delivery</label><input type="date" class="form-control datepicker" name="delivery_date[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Date of Receipt at Site</label><input type="date" class="form-control datepicker" name="receipt_site[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Weight Declare</label><input type="text" class="form-control" name="weight_declare[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Weight Found</label><input type="text" class="form-control" name="weight_found[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Transporter</label><input type="text" class="form-control" name="transporter[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">No of Pkgs Received</label><input type="text" class="form-control" name="pkgs_receive[]"/></div>';
                    $html[] .= '<div class="col-lg-4"><label class="control-label">Remarks</label><textarea class="form-control" name="remarks[]"></textarea></div>';
                    $html[] .= '</div></div>';
                }elseif ($consignment_type == 'By Air'){
                    $html[] = '<div class="col-lg-12"><div class="form-group">';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">No. of Pkgs</label><input type="text" class="form-control" name="no_of_pkgs[]" value="'.$container->packages.'" readonly/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Shed</label><input type="text" class="form-control" name="shed[]" value="'.$shed_info->shed.'" readonly/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Total Net Weight</label><input type="text" class="form-control" name="total_net_weight[]" value="'.$container->container_net_weight.'" readonly/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Total Gross Weight</label><input type="text" class="form-control" name="total_gross_weight[]" value="'.$container->container_gross_weight.'" readonly/></div>';
                    $html[] .= '<input type="hidden" name="invoices_id[]" value="'.$container->invoices_id.'"></div>';
                    $html[] .= '<input type="hidden" name="container_id[]" value="'.$container->id.'"></div></div>';
                    $html[] .= '<div class="col-lg-12"><div class="form-group">';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Truck No</label><input type="text" class="form-control" name="truck_no[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Bilty</label><input type="text" class="form-control" name="bilty[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Date of Delivery</label><input type="date" class="form-control datepicker" name="delivery_date[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Date of Receipt at Site</label><input type="date" class="form-control datepicker" name="receipt_site[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Weight Declare</label><input type="text" class="form-control" name="weight_declare[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Weight Found</label><input type="text" class="form-control" name="weight_found[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">Transporter</label><input type="text" class="form-control" name="transporter[]"/></div>';
                    $html[] .= '<div class="col-lg-2"><label class="control-label">No of Pkgs Received</label><input type="text" class="form-control" name="pkgs_receive[]"/></div>';
                    $html[] .= '<div class="col-lg-4"><label class="control-label">Remarks</label><textarea class="form-control" name="remarks[]"></textarea></div>';
                    $html[] .= '</div></div>';
                }
            }
            $data[0] = $html;
            $data[1] = $type;
            echo json_encode($data);
            die();
        }
    }

    public function save_delivery_section($invoices_id = NULL)
    {

        $data['title'] = "Create Delivery Section";
        $subview = 'manage_delivery_section';
        if(!empty($invoices_id)){
            $dsid = $this->input->post('Iid', TRUE);

            $invoices_id = $this->input->post('invoices_id', TRUE);
            $container_id = $this->input->post('container_id', TRUE);
            $truck_no = $this->input->post('truck_no', TRUE);
            $bilty = $this->input->post('bilty', TRUE);
            $delivery_date = $this->input->post('delivery_date', TRUE);
            $receipt_site = $this->input->post('receipt_site', TRUE);
            $weight_declare = $this->input->post('weight_declare', TRUE);
            $weight_found = $this->input->post('weight_found', TRUE);
            $transporter = $this->input->post('transporter', TRUE);
            $pkgs_receive = $this->input->post('pkgs_receive', TRUE);
            $remarks = $this->input->post('remarks', TRUE);
            for ($i = 0; $i <count($dsid); $i++) {
                $delivery_data[] = array(
                    'Iid' => $dsid[$i],
                    'IInvoicesId' => $invoices_id[$i],
                    'IContainerId' => $container_id[$i],
                    'VCTruckNo' => $truck_no[$i],
                    'VCBilty' => $bilty[$i],
                    'DTDeliveryDate' => $delivery_date[$i],
                    'DTDateReceiptSite' => $receipt_site[$i],
                    'DCWeightDeclare' => $weight_declare[$i],
                    'DCWeightFound' => $weight_found[$i],
                    'VCTransporter' => $transporter[$i],
                    'VCPkgsReceive' => $pkgs_receive[$i],
                    'TRemarks' => $remarks[$i]
                );


                $this->db->where('Iid', $dsid[$i]);
                $this->db->update("stbdeliverysection", $delivery_data[$i]);
            }
            $data['subview'] = 'admin/invoice/' . $subview;
            $this->load->view('admin/_layout_main2', $data);
        }else {

            $invoices_id = $this->input->post('invoices_id', TRUE);
            $container_id = $this->input->post('container_id', TRUE);
            $truck_no = $this->input->post('truck_no', TRUE);
            $bilty = $this->input->post('bilty', TRUE);
            $delivery_date = $this->input->post('delivery_date', TRUE);
            $receipt_site = $this->input->post('receipt_site', TRUE);
            $weight_declare = $this->input->post('weight_declare', TRUE);
            $weight_found = $this->input->post('weight_found', TRUE);
            $transporter = $this->input->post('transporter', TRUE);
            $pkgs_receive = $this->input->post('pkgs_receive', TRUE);
            $remarks = $this->input->post('remarks', TRUE);
            if (!empty($truck_no)) {
                for ($i = 0; $i < count($truck_no); $i++) {
                    $delivery_data = array(
                        'IInvoicesId' => $invoices_id[$i],
                        'IContainerId' => $container_id[$i],
                        'VCTruckNo' => $truck_no[$i],
                        'VCBilty' => $bilty[$i],
                        'DTDeliveryDate' => $delivery_date[$i],
                        'DTDateReceiptSite' => $receipt_site[$i],
                        'DCWeightDeclare' => $weight_declare[$i],
                        'DCWeightFound' => $weight_found[$i],
                        'VCTransporter' => $transporter[$i],
                        'VCPkgsReceive' => $pkgs_receive[$i],
                        'TRemarks' => $remarks[$i]
                    );
                    $this->invoice_model->_table_name = 'stbdeliverysection';
                    $this->invoice_model->_primary_key = 'Iid';
                    $this->invoice_model->save($delivery_data);
                }
            }
            $data['subview'] = 'admin/invoice/' . $subview;
            $this->load->view('admin/_layout_main2', $data);
        }
    }

    public function all_delivery()
    {
        check_url();
        $data['title'] = "List Delivery Section";
        $subview = 'all_delivery_section';

        if ($_POST) {
            $data['client_id'] = $_POST['client_id'];
            $data['reference_no'] = $_POST['reference_no'];
        }

        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'name';
        $data['all_client'] = $this->invoice_model->get();

        $user_id = $this->session->userdata('user_id');
        $user_info = $this->invoice_model->check_by(array('user_id' => $user_id), 'tbl_users');
        $data['role'] = $user_info->role_id;

        $data['subview'] = $this->load->view('admin/invoice/' . $subview, $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function all_deliveries($client_id = null, $reference_no = null)
    {
        $search_array = array();
        $rows = array();
        $search_value = $_POST['search']['value'];
        if ($search_value) {
            $search_array = array(
                'cl.client_id' => $search_value,
                'inv.reference_no' => $search_value
            );
        }
        $length = 10;
        $start = 1;
        if ($_POST['length'] != -1) {
            $length = $_POST['length'];
            $start = $_POST['start'];
        }
        $delivery_search_list = $this->invoice_model->delivery_searching_list($search_array, $length, $start, $client_id, $reference_no);
        $delivery_search_list_rows = $this->invoice_model->delivery_searching_list_rows($search_array, $client_id, $reference_no);
        $i = $_POST['start'] + 1;
        foreach ($delivery_search_list as $delivery) {
            $job_no = $this->invoice_model->job_no_creation($delivery->invoices_id);
            $client_info = $this->invoice_model->check_by(array('client_id' => $delivery->client_id), 'tbl_client');

            $action = btn_edit('admin/invoice/manage_delivery/edit_delivery/'.$delivery->invoices_id);

            $rows[] = array(
                $job_no,
                $client_info->name,
                $action
            );
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $delivery_search_list_rows,
            "recordsFiltered" => $delivery_search_list_rows,
            "data" => $rows
        );
        echo json_encode($output);
    }

    /*** End Delivery Item ***/
}
