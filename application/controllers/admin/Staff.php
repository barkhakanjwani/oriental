<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Staff extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('invoice_model');
        $this->load->model('accounts_model');
        $this->load->model('client_model');
    }

    /*     * * Create New staff ** */
    public function manage_staff($action = NULL, $id = NULL) {
        if ($action == 'edit_staff') {
            $id = decode($id);
            $data['active'] = 2;
            $data['staff_info'] = $this->user_model->check_by(array('staff_id'=>$id),'tbl_staff');
        } else {
            $data['active'] = 1;
        }
        $data['title'] = 'staff List';
        $this->user_model->_table_name = 'tbl_staff';
        $this->user_model->_order_by = 'staff_id';
        $data['all_staffs'] = $this->user_model->get();

        $data['subview'] = $this->load->view('admin/staff/staff_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    public function manage_staffs()
    {
        $search_array=array();
        $rows = array();
        $search_value=$_POST['search']['value'];
        if ($search_value){
            $search_array=array(
                'st .staff_name'  => $search_value
            );
        }
        $length = 10; $start = 1;
        if($_POST['length'] != -1){
            $length = $_POST['length'];
            $start = $_POST['start'];
        }
        $stuff_list = $this->client_model->stuff_searching_list($search_array,$length,$start);
        $stuff_list_rows = $this->client_model->stuff_searching_list_rows($search_array);
        $i = $_POST['start']+1;
        foreach($stuff_list as $staff){
            if ($staff->staff_status == 1){
                $status = '<a data-toggle="tooltip" data-placement="top" title="Click to Deactive" href="'.base_url('admin/staff/change_status/0/'.encrypt($staff->staff_id)).'">'.lang('active').'</a>';
            }else{
                $status = '<a data-toggle="tooltip" data-placement="top" title="Click to Active "  href="'.base_url('admin/staff/change_status/1/'.encrypt($staff->staff_id)).'">'.lang('deactive').'</a>';
            }

            $rows[] = array(
                strftime(config_item('date_format'), strtotime($staff->staff_date)),
                ucfirst($staff->staff_name),
                $staff->staff_email,
                $staff->staff_contact,
                $status,
                btn_edit('admin/staff/manage_staff/edit_staff/' . encode($staff->staff_id))
            );
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $stuff_list_rows,
            "recordsFiltered" => $stuff_list_rows,
            "data" => $rows
        );
        echo json_encode($output);
    }

    public function check_staff_email($id=NULL){
        $email = $_POST['email'];
        $get_email = array();
        if(!empty($id)){
            $get_email = $this->db->select('*')
                ->from('tbl_staff')
                ->where('staff_id != '.$id)
                ->where('staff_email',$email)
                ->get()->result();
        }
        else{
            $get_email = $this->user_model->check_by(array('staff_email' => $email), 'tbl_staff');
        }
        if(!empty($get_email)){
            echo json_encode(false);
            die();
        }
        else{
            echo json_encode(true);
            die();
        }
    }

    public function change_status($flag, $id) {
        if ($flag == 1) {
            $msg = 'Active';
        } else {
            $msg = 'Deactive';
        }
        $where = array('staff_id' => $id);
        $action = array('staff_status' => $flag, 'updated_by'=>$this->session->userdata('user_id'));
        $this->user_model->set_action($where, $action, 'tbl_staff');

        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'staff',
            'module_field_id' => $id,
            'activity' => lang('activity_change_staff_status'),
            'icon' => 'fa-user',
            'value1' => $msg,
        );
        $this->user_model->_table_name = 'tbl_activities';
        $this->user_model->_primary_key = "activities_id";
        $this->user_model->save($activities);

        $type = "success";
        $message = lang('staff')." " . $msg . " Successfully!";
        set_message($type, $message);
        redirect('admin/staff/manage_staff'); //redirect page
    }

    /*** Save New staff ***/

    public function save_staff($id = null) {
        $login_data = $this->user_model->array_from_post(array('staff_name', 'staff_email', 'staff_contact'));
        $id = (!empty($id))?decrypt($id):NULL;
        $message = (!empty($id))?lang('update_staff_info'):lang('save_staff_info');
        $act = (!empty($id))?lang('activity_update_staff'):lang('activity_new_staff');
        if(empty($id)){
            $account_head = $this->user_model->check_by(array('H_TYPE'=>'Assets', 'H_NAME'=>'Outdoor Staff Advances'), 'accounts_head');
            $this->load->model('accounts_model');
            $rs = $this->accounts_model->account_no_by_head($account_head->H_ID);
            if($rs->A_NO!=""){
                $pos=strlen($rs->A_NO)-1;
                $account_no = substr_replace($rs->A_NO,substr($rs->A_NO,$pos)+1,$pos);
            }else{
                $rs = $this->accounts_model->select_edit_head($account_head->H_ID);
                $account_no=$rs->H_NO.".".(1);
            }
            $account = array(
                'H_ID'          => $account_head->H_ID,
                'SUB_HEAD_ID'   => $account_head->SUB_HEAD_ID,
                'A_NAME'        => $this->input->post('staff_name'),
                'A_DESC'        => '',
                'A_OPENINGTYPE' => '',
                'A_OPENINGBALANCE'=>'',
                'A_SELFHEAD_ID' => 0,
                'A_NO'          => $account_no,
                'A_STATUS'      => 1,
                'CREATED_BY'    => $this->session->userdata('user_id'),
                'CREATED_AT'    => date('Y-m-d H:i:s')
            );
            $this->user_model->_table_name = 'accounts';
            $this->user_model->_primary_key = "A_ID";
            $account_id=$this->user_model->save($account);
            
            $login_data['created_by'] = $this->session->userdata('user_id');
            $login_data['account_id'] = $account_id;
            $this->user_model->_table_name = 'tbl_staff';
            $this->user_model->_primary_key = "staff_id";
            $id = $this->user_model->save($login_data);
        } else{
            $staff_info=$this->user_model->check_by(array('staff_id'=>$id),'tbl_staff');
            $account = array(
                'A_NAME' => $this->input->post('staff_name')
            );
            $this->user_model->_table_name = 'accounts';
            $this->user_model->_primary_key = "A_ID";
            $this->user_model->save($account,$staff_info->account_id);
            
            $login_data['updated_by'] = $this->session->userdata('user_id');
            $this->user_model->_table_name = 'tbl_staff';
            $this->user_model->_primary_key = "staff_id";
            $this->user_model->save($login_data, $id);
        }
        
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'staff',
            'module_field_id' => $id,
            'activity' => $act,
            'icon' => 'fa-user',
            'value1' => $login_data['staff_name']
        );
        $this->user_model->_table_name = 'tbl_activities';
        $this->user_model->_primary_key = "activities_id";
        $this->user_model->save($activities);
        
        $type = 'success';
        set_message($type, $message);
        redirect('admin/staff/manage_staff'); //redirect page
    }
}
