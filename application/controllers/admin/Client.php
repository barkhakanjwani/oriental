<?php
class Client extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('client_model');
        $this->load->model('invoice_model');
        $this->load->model('accounts_model');
        $this->load->model('user_model');
    }

    public function hash($string) {
        return hash('sha512', $string . config_item('encryption_key'));
    }

    public function new_client($id = NULL) {
        check_url();
        $data['title'] = "New Client"; //Page title
        $data['page'] = lang('client');
        $data['sub_active'] = lang('new_client');
        if (!empty($id)) {
            $id = decode($id);
            $this->client_model->_table_name = "tbl_client";
            $this->client_model->_order_by = "client_id";
            $data['client_info'] = $this->client_model->get_by(array('client_id' => $id), TRUE);
            $data['client_documents'] = $this->client_model->check_by_all(array('client_id' => $id), 'tbl_client_documents');
            $data['client_accounts'] = $this->client_model->check_by(array('CLIENT_ID'=>$id), 'accounts');
        }
        if (!empty($data['client_info']) && $data['client_info']->client_status == 2) {
            $data['company'] = 1;
        } else {
            $data['person'] = 1;
        }
        
        $this->client_model->_table_name = "tbl_countries"; //table name
        $this->client_model->_order_by = "id";
        $data['countries'] = $this->client_model->get();
        
        $this->client_model->_table_name = 'tbl_currencies';
        $this->client_model->_order_by = 'name';
        $data['currencies'] = $this->client_model->get();
        
        $this->client_model->_table_name = 'tbl_client';
        $this->client_model->_order_by = 'client_id';
        $data['all_client_info'] = $this->client_model->get();

        $chart_of_accounts = $this->db->group_by('HEAD_TYPE')->get_where('sub_heads')->result();
        $data['result'] = $chart_of_accounts;

        $data['subview'] = $this->load->view('admin/client/new_client', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }
    
    public function check_client_email($id=NULL){
        $email = $_POST['email'];
        if(!empty($id)){
            $id = decrypt($id);
            $get_email = $this->db->select('*')
                ->from('tbl_client')
                ->where('client_id != '.$id)
                ->where('email',$email)
                ->get()->result();
            if(empty($get_email)){
                $get_email = $this->db->select('*')
                    ->from('tbl_users')
                    ->where('client_id != '.$id)
                    ->where('email',$email)
                    ->get()->result();
            }
        }
        else{
            $get_email = $this->invoice_model->check_by(array('email' => $email), 'tbl_client');
            if(empty($get_email)){
                $get_email = $this->invoice_model->check_by(array('email'=>$email), 'tbl_users');
            }
        }
        if(!empty($get_email)){
            echo json_encode(false);
            die();
        }
        else{
            echo json_encode(true);
            die();
        }
    }

    public function change_status($flag, $id) {
        check_url();
        if ($flag == 1) {
            $msg = 'Active';
        } else {
            $msg = 'Deactive';
        }
        $id = decrypt($id);
        $where = array('client_id' => $id);
        $action = array('client_status' => $flag, 'updated_by'=>$this->session->userdata('user_id'));
        $this->user_model->set_action($where, $action, 'tbl_client');

        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'client',
            'module_field_id' => $id,
            'activity' => lang('activity_change_status'),
            'icon' => 'fa-user',
            'value1' => $msg,
        );
        $this->user_model->_table_name = 'tbl_activities';
        $this->user_model->_primary_key = "activities_id";
        $this->user_model->save($activities);

        $type = "success";
        $message = "Client " . $msg . " Successfully!";
        set_message($type, $message);
        redirect('admin/client/manage_client');
    }

    public function save_client($id = NULL) {
        $data = $this->client_model->array_from_post(array('name', 'email', 'email_2', 'email_3', 'short_note', 'phone', 'mobile', 'fax', 'ntn', 'strn', 'address', 'city', 'zipcode', 'country', 'contact_p_name', 'contact_p_email', 'contact_p_contact','contact_p_designation'));

        if (!empty($_FILES['profile_photo']['name'])){
            $val = $this->client_model->uploadImage('profile_photo');
            $val == TRUE || redirect('admin/client/new_client');
            $data['profile_photo'] = $val['path'];
        }
        if (!empty($_POST['refered_by'])){
            $data['refered_by'] = $_POST['refered_by'];
        }
        $id = (!empty($id))?decrypt($id):NULL;
        if(!empty($id)){
            $data['updated_by'] = $this->session->userdata('user_id');
        }
        else{
            $data['created_by'] = $this->session->userdata('user_id');
        }
        $this->client_model->_table_name = 'tbl_client';
        $this->client_model->_primary_key = "client_id";
        $client_id = $this->client_model->save($data, $id);

        if(!empty($id)){
            $client_all = $this->client_model->check_by_all(array('client_id'=>$id), 'accounts');
            for($i=0; $i<count($client_all); $i++){
                $client_name = explode('(', $client_all[$i]->A_NAME);
                $client_name[0] = $_POST['name'];
                $name[$i] = $client_name[0].' ('.$client_name[1];

                $accounts = array(
                    'A_NAME' => $name[$i],
                    'UPDATED_BY' => $this->session->userdata('user_id'),
                );

                $this->db->where('A_ID',$client_all[$i]->A_ID);
                $this->db->update('accounts', $accounts);
            }

            if($_POST['chkpass'] == 'Yes') {
                $user_id = $this->client_model->check_by(array('client_id'=>$id), 'tbl_users');
                $password = $this->input->post('password', TRUE);
                $login_data['password'] = $this->hash($password);

                if(!empty($user_id)){
                    $client_data = array(
                        'username' => $this->input->post('email'),
                        'email' => $this->input->post('email'),
                        'password' => $login_data['password']
                    );
                    $this->db->where('client_id', $id);
                    $this->db->update('tbl_users', $client_data);

                    $account_detail = array(
                        'fullname' => $this->input->post('name'),
                        'city' => $this->input->post('city'),
                        'country' => $this->input->post('country'),
                        'address' => $this->input->post('address'),
                        'phone' => $this->input->post('phone'),
                        'mobile' => $this->input->post('mobile')
                    );
                    $this->db->where('user_id',$user_id->user_id);
                    $this->db->update('tbl_account_details', $account_detail);
                }else{
                    $client_data = array(
                        'username' => $this->input->post('email'),
                        'email' => $this->input->post('email'),
                        'password' => $login_data['password'],
                        'client_id' => $client_id,
                        'online_status' => 0
                    );
                    $this->client_model->_table_name = 'tbl_users';
                    $this->client_model->_primary_key = "user_id";
                    $user_id = $this->client_model->save($client_data);

                    /*save into tbl_account_detail*/
                    $account_detail = array(
                        'user_id' => $user_id,
                        'fullname' => $this->input->post('name'),
                        'city' => $this->input->post('city'),
                        'country' => $this->input->post('country'),
                        'address' => $this->input->post('address'),
                        'phone' => $this->input->post('phone'),
                        'mobile' => $this->input->post('mobile')
                    );
                    $this->client_model->_table_name = 'tbl_account_details';
                    $this->client_model->_primary_key = "account_details_id";
                    $this->client_model->save($account_detail);
                }

            }
            $client_documents = $this->client_model->check_by(array('client_id'=>$id), 'tbl_client_documents');
            if(!empty($client_documents)){
                $this->db->where('client_id', $id);
                $this->db->delete('tbl_client_documents');
            }
        }
        else {
            /*** USER ENTRY ***/
            /*save into tbl_users*/
            if(!empty($_POST['chkpass'])) {
                if ($_POST['chkpass'] == 'Yes') {
                    $password = $this->input->post('password', TRUE);
                    $login_data['password'] = $this->hash($password);
                    $client_data = array(
                        'username' => $this->input->post('email'),
                        'email' => $this->input->post('email'),
                        'password' => $login_data['password'],
                        'client_id' => $client_id,
                        'online_status' => 0
                    );
                    $this->client_model->_table_name = 'tbl_users';
                    $this->client_model->_primary_key = "user_id";
                    $user_id = $this->client_model->save($client_data);

                    /*save into tbl_account_detail*/
                    $account_detail = array(
                        'user_id' => $user_id,
                        'fullname' => $this->input->post('name'),
                        'city' => $this->input->post('city'),
                        'country' => $this->input->post('country'),
                        'address' => $this->input->post('address'),
                        'phone' => $this->input->post('phone'),
                        'mobile' => $this->input->post('mobile')
                    );
                    $this->client_model->_table_name = 'tbl_account_details';
                    $this->client_model->_primary_key = "account_details_id";
                    $this->client_model->save($account_detail);
                }
            }
            /*** END USER ENTRY  ***/
            if (!empty($client_id)) {
                /***Client Accounts Entry**/
                $account_data=array();
                $rec_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Assets', 'NAME' => 'Current Assets'), 'sub_heads');
                $rec_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $rec_head->SUB_HEAD_ID, 'H_NAME' => 'Account Receivables'), 'accounts_head');

                $receive_ac_no = $this->accounts_model->max_account_no_by_head($rec_sub_head->H_ID);
                if (!empty($receive_ac_no)) {
                    $a = explode('.', $receive_ac_no->A_NO);
                    $arr = array();
                    for ($k = 0; $k < count($a); $k++) {
                        if ($k == (count($a) - 1)) {
                            $arr[] = $a[$k] + 1;
                        } else {
                            $arr[] = $a[$k];
                        }
                    }
                    $account_no = implode('.', $arr);
                } else {
                    $receive_ac_no = $this->accounts_model->select_edit_head($rec_sub_head->H_ID);
                    $account_no = $receive_ac_no->H_NO . "." . (1);
                }
                $account_data[] = array(
                    'H_ID'              => $rec_sub_head->H_ID,
                    'SUB_HEAD_ID'       => $rec_sub_head->SUB_HEAD_ID,
                    'CLIENT_ID'         => $client_id,
                    'A_NAME'            => $this->input->post('name').' ('.$rec_sub_head->H_NAME.')',
                    'A_SELFHEAD_ID'     => 0,
                    'A_NO'              => $account_no,
                    'A_STATUS'          => 1,
                    'CREATED_BY'        => $this->session->userdata('user_id'),
                    'CREATED_AT'        => date('Y-m-d H:i:s')
                );

                $st_expense_heads = $this->invoice_model->check_by(array('H_TYPE'=>'Expense', 'H_NAME'=>'Sales Tax Expense'),'accounts_head');
                $st_expense_no = $this->accounts_model->max_account_no_by_head($st_expense_heads->H_ID);
                if (!empty($st_expense_no)) {
                    $a = explode('.', $st_expense_no->A_NO);
                    $arr = array();
                    for ($k = 0; $k < count($a); $k++) {
                        if ($k == (count($a) - 1)) {
                            $arr[] = $a[$k] + 1;
                        } else {
                            $arr[] = $a[$k];
                        }
                    }
                    $account_no = implode('.', $arr);
                } else {
                    $st_expense_no = $this->accounts_model->select_edit_head($st_expense_heads->H_ID);
                    $account_no = $st_expense_no->H_NO . "." . (1);
                }
                $account_data[] = array(
                    'H_ID'              => $st_expense_heads->H_ID,
                    'SUB_HEAD_ID'       => $st_expense_heads->SUB_HEAD_ID,
                    'CLIENT_ID'         => $client_id,
                    'A_NAME'            => $this->input->post('name').' ('.$st_expense_heads->H_NAME.')',
                    'A_SELFHEAD_ID'     => 0,
                    'A_NO'              => $account_no,
                    'A_STATUS'          => 1,
                    'CREATED_BY'        => $this->session->userdata('user_id'),
                    'CREATED_AT'        => date('Y-m-d H:i:s')
                );

                $expense_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Expense', 'NAME' => 'Cost of Sales'), 'sub_heads');
                $expense_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $expense_head->SUB_HEAD_ID, 'H_NAME' => 'Client Expenses'), 'accounts_head');
                $expense_sub_no = $this->accounts_model->max_account_no_by_head($expense_sub_head->H_ID);
                if (!empty($expense_sub_no)) {
                    $a = explode('.', $expense_sub_no->A_NO);
                    $arr = array();
                    for ($k = 0; $k < count($a); $k++) {
                        if ($k == (count($a) - 1)) {
                            $arr[] = $a[$k] + 1;
                        } else {
                            $arr[] = $a[$k];
                        }
                    }
                    $account_no = implode('.', $arr);
                } else {
                    $expense_sub_no = $this->accounts_model->select_edit_head($expense_sub_head->H_ID);
                    $account_no = $expense_sub_no->H_NO . "." . (1);
                }
                $account_data[] = array(
                    'H_ID'              => $expense_sub_head->H_ID,
                    'SUB_HEAD_ID'       => $expense_sub_head->SUB_HEAD_ID,
                    'CLIENT_ID'         => $client_id,
                    'A_NAME'            => $this->input->post('name').' ('.$expense_sub_head->H_NAME.')',
                    'A_SELFHEAD_ID'     => 0,
                    'A_NO'              => $account_no,
                    'A_STATUS'          => 1,
                    'CREATED_BY'        => $this->session->userdata('user_id'),
                    'CREATED_AT'        => date('Y-m-d H:i:s')
                );

                $services_head = $this->invoice_model->check_by(array('HEAD_TYPE'=>'Income', 'NAME'=>'Sales Income'),'sub_heads');
                $services_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID'=>$services_head->SUB_HEAD_ID, 'H_NAME'=>'Turnover Services'),'accounts_head');
                $services_sub_no = $this->accounts_model->max_account_no_by_head($services_sub_head->H_ID);
                if (!empty($services_sub_no)) {
                    $a = explode('.', $services_sub_no->A_NO);
                    $arr = array();
                    for ($k = 0; $k < count($a); $k++) {
                        if ($k == (count($a) - 1)) {
                            $arr[] = $a[$k] + 1;
                        } else {
                            $arr[] = $a[$k];
                        }
                    }
                    $account_no = implode('.', $arr);
                } else {
                    $services_sub_no = $this->accounts_model->select_edit_head($services_sub_head->H_ID);
                    $account_no = $services_sub_no->H_NO . "." . (1);
                }
                $account_data[] = array(
                    'H_ID'              => $services_sub_head->H_ID,
                    'SUB_HEAD_ID'       => $services_sub_head->SUB_HEAD_ID,
                    'CLIENT_ID'         => $client_id,
                    'A_NAME'            => $this->input->post('name').' ('.$services_sub_head->H_NAME.')',
                    'A_SELFHEAD_ID'     => 0,
                    'A_NO'              => $account_no,
                    'A_STATUS'          => 1,
                    'CREATED_BY'        => $this->session->userdata('user_id'),
                    'CREATED_AT'        => date('Y-m-d H:i:s')
                );

                $commission_heads = $this->invoice_model->check_by(array('H_TYPE'=>'Income', 'H_NAME'=>'Commission'),'accounts_head');
                $service_account = $this->invoice_model->check_by(array('H_ID'=>$commission_heads->H_ID, 'A_NAME'=>"Service Charges"),'accounts');
                $service_ac_no = $this->accounts_model->max_account_no_self_head($service_account->A_ID);
                if (!empty($service_ac_no)) {
                    $gen_ac_no = strlen($service_ac_no->A_NO) - 1;
                    $account_no = substr_replace($service_ac_no->A_NO, substr($service_ac_no->A_NO, $gen_ac_no) + 1, $gen_ac_no);
                    $self_head_id = $service_ac_no->A_SELFHEAD_ID;
                    $account_name = read_subHead($service_ac_no->A_SELFHEAD_ID)->A_NAME;
                } else {
                    $service_ac_no = $this->accounts_model->read_account_by_id($service_account->A_ID);
                    $account_no = $service_ac_no->A_NO . "." . (1);
                    $self_head_id = $service_ac_no->A_ID;
                    $account_name = $service_ac_no->A_NAME;
                }

                $account_data[] = array(
                    'H_ID'              => $commission_heads->H_ID,
                    'SUB_HEAD_ID'       => $commission_heads->SUB_HEAD_ID,
                    'CLIENT_ID'         => $client_id,
                    'A_NAME'            => $this->input->post('name').' ('.$account_name.')',
                    'A_SELFHEAD_ID'     => $self_head_id,
                    'A_NO'              => $account_no,
                    'A_STATUS'          => 1,
                    'CREATED_BY'        => $this->session->userdata('user_id'),
                    'CREATED_AT'        => date('Y-m-d H:i:s')
                );

                $ap_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Liabilities', 'NAME' => 'Current Liabilities'), 'sub_heads');
                $ap_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_head->SUB_HEAD_ID, 'H_NAME' => 'Account Payables'), 'accounts_head');
                $ap_sub_no = $this->accounts_model->max_account_no_by_head($ap_sub_head->H_ID);
                if (!empty($ap_sub_no)) {
                    $a = explode('.', $ap_sub_no->A_NO);
                    $arr = array();
                    for ($k = 0; $k < count($a); $k++) {
                        if ($k == (count($a) - 1)) {
                            $arr[] = $a[$k] + 1;
                        } else {
                            $arr[] = $a[$k];
                        }
                    }
                    $account_no = implode('.', $arr);
                } else {
                    $ap_sub_no = $this->accounts_model->select_edit_head($ap_sub_head->H_ID);
                    $account_no = $ap_sub_no->H_NO . "." . (1);
                }
                $account_data[] = array(
                    'H_ID'              => $ap_sub_head->H_ID,
                    'SUB_HEAD_ID'       => $ap_sub_head->SUB_HEAD_ID,
                    'CLIENT_ID'         => $client_id,
                    'A_NAME'            => $this->input->post('name').' ('.$ap_sub_head->H_NAME.')',
                    'A_SELFHEAD_ID'     => 0,
                    'A_NO'              => $account_no,
                    'A_STATUS'          => 1,
                    'CREATED_BY'        => $this->session->userdata('user_id'),
                    'CREATED_AT'        => date('Y-m-d H:i:s')
                );

                $adc_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_head->SUB_HEAD_ID, 'H_NAME' => 'Advances from clients'), 'accounts_head');
                $adc_sub_no = $this->accounts_model->max_account_no_by_head($adc_sub_head->H_ID);
                if (!empty($adc_sub_no)) {
                    $a = explode('.', $adc_sub_no->A_NO);
                    $arr = array();
                    for ($k = 0; $k < count($a); $k++) {
                        if ($k == (count($a) - 1)) {
                            $arr[] = $a[$k] + 1;
                        } else {
                            $arr[] = $a[$k];
                        }
                    }
                    $account_no = implode('.', $arr);
                } else {
                    $adc_sub_no = $this->accounts_model->select_edit_head($adc_sub_head->H_ID);
                    $account_no = $adc_sub_no->H_NO . "." . (1);
                }
                $account_data[] = array(
                    'H_ID'              => $adc_sub_head->H_ID,
                    'SUB_HEAD_ID'       => $adc_sub_head->SUB_HEAD_ID,
                    'CLIENT_ID'         => $client_id,
                    'A_NAME'            => $this->input->post('name').' ('.$adc_sub_head->H_NAME.')',
                    'A_SELFHEAD_ID'     => 0,
                    'A_NO'              => $account_no,
                    'A_STATUS'          => 1,
                    'CREATED_BY'        => $this->session->userdata('user_id'),
                    'CREATED_AT'        => date('Y-m-d H:i:s')
                );
                $this->accounts_model->create_account_batch($account_data);
                /***End Client Accounts Entry**/
            }
        }
    
        /*** DOCUMENTS SECTION MULTI FIELDS INSERTION ***/
        $document_title = $this->input->post('document_title', TRUE);
        $document_date = $this->input->post('document_date', TRUE);
        $document_comment = $this->input->post('document_comment', TRUE);
    
        for ($i=0; $i<count($document_title); $i++){
            if (!empty($document_title[$i])){
                $document_data = array(
                    'client_id' => $client_id,
                    'client_document_title' => $document_title[$i],
                    'client_document_date' => $document_date[$i],
                    'client_document_comment' => $document_comment[$i],
                );
                if (!empty($_FILES['document_file']['name'][$i])) {
                    $_FILES['file']['name'] = $_FILES['document_file']['name'][$i];
                    $_FILES['file']['type'] = $_FILES['document_file']['type'][$i];
                    $_FILES['file']['tmp_name'] = $_FILES['document_file']['tmp_name'][$i];
                    $_FILES['file']['error'] = $_FILES['document_file']['error'][$i];
                    $_FILES['file']['size'] = $_FILES['document_file']['size'][$i];
        
                    $config['upload_path'] = 'uploads/client_documents/';
                    $config['allowed_types'] = '*';
                    $config['max_size'] = '5000';
                    $config['file_name'] = $_FILES['document_file']['name'][$i];
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('file')) {
                        $error = array('error' => $this->upload->display_errors());
                    } else {
                        $data1 = $this->upload->data();
                        $document_data['client_document_file'] = 'uploads/client_documents/' . $data1['file_name'];
                    }
                }else{
                    $document_data['client_document_file'] = $_POST['document_file_2'][$i];
                }
                $this->invoice_model->_table_name = 'tbl_client_documents';
                $this->invoice_model->_primary_key = 'client_document_id';
                $this->invoice_model->save($document_data);
            }
        }
        /*** END DOCUMENTS SECTION MULTI FIELDS INSERTION ***/
        
        if (!empty($id)) {
            $id = $id;
            $action = lang('activity_update_company');
            $msg = lang('client_updated');
        } else {
            $id = $client_id;
            $action = lang('activity_added_new_company');
            $msg = lang('client_added');
        }
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'client',
            'module_field_id' => $id,
            'activity' => $action,
            'icon' => 'fa-user',
            'value1' => $this->input->post('name')
        );
        $this->client_model->_table_name = 'tbl_activities';
        $this->client_model->_primary_key = "activities_id";
        $this->client_model->save($activities);
        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/client/manage_client');
    }

    public function manage_client() {
        check_url();
        $data['title'] = "Manage Client"; //Page title
        $data['page'] = lang('client');
        $data['sub_active'] = lang('manage_client');

        // get all client
        $this->client_model->_table_name = 'tbl_client';
        $this->client_model->_order_by = 'client_id';
        $data['all_client_info'] = $this->client_model->get();

        $data['subview'] = $this->load->view('admin/client/manage_client', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function manage_clients()
    {
        $search_array=array();
        $rows = array();
        $search_value=$_POST['search']['value'];
        if ($search_value){
            $search_array=array(
                'cl.name'  => $search_value
            );
        }
        $length = 10; $start = 1;
        if($_POST['length'] != -1){
            $length = $_POST['length'];
            $start = $_POST['start'];
        }
        $client_list = $this->client_model->client_searching_list($search_array,$length,$start);
        $client_list_rows = $this->client_model->client_searching_list_rows($search_array);
        $i = $_POST['start']+1;
        foreach($client_list as $client_details){
            $client_name='-';
            if ($client_details->refered_by != 0){
                $client_info = $this->invoice_model->check_by(array('client_id'=>$client_details->refered_by),'tbl_client');
                $client_name=$client_info->name;
            }

            if ($client_details->client_status == 1){
                $status = '<a data-toggle="tooltip" data-placement="top" title="Click to Deactive" href="'.base_url('admin/client/change_status/0/'. encrypt($client_details->client_id)).'">'.lang('active') .'</a>';
            }else{
                $status = '<a data-toggle="tooltip" data-placement="top" title="Click to Active "  href="'.base_url('admin/client/change_status/1/'. encrypt($client_details->client_id)).'">'. lang('deactive') .'</a>';
            }

            $rows[] = array(
                $client_details->name,
                $client_details->mobile,
                $client_details->email,
                $client_name,
                $status,
                btn_edit('admin/client/new_client/' . encode($client_details->client_id))
            );
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $client_list_rows,
            "recordsFiltered" => $client_list_rows,
            "data" => $rows
        );
        echo json_encode($output);
    }

    public function client_details($id) {
        check_url();
        $data['title'] = "View Client Details"; //Page title
        // get all client details
        $this->client_model->_table_name = "tbl_client"; //table name
        $this->client_model->_order_by = "client_id";
        $data['client_details'] = $this->client_model->get_by(array('client_id' => $id), TRUE);

        // get all invoice by client id
        $this->client_model->_table_name = "tbl_invoices"; //table name
        $this->client_model->_order_by = "client_id";
        $data['client_invoices'] = $this->client_model->get_by(array('client_id' => $id), FALSE);

        // get client contatc by client id
        $data['client_contacts'] = $this->client_model->get_client_contacts($id);

        $data['subview'] = $this->load->view('admin/client/client_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

}
