<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class report extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('report_model');
        $this->load->model('accounts_model');
        $this->load->model('invoice_model');

        $this->load->helper('ckeditor');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "99.8%",
                'height' => "400px"
            )
        );
    }

    /*** My Reporting ***/
    public function sales_report() {
        $data['title'] = lang('sales_report');
        if($_POST){
            $start_date = date('Y-m-d',strtotime($this->input->post('start_date')));
            $end_date = date('Y-m-d',strtotime($this->input->post('end_date')));
            $data['start_date'] = $start_date;
            $data['end_date'] = $end_date;
            $this->db->select('*')
                ->from('tbl_invoices inv')
                ->join('tbl_saved_commodities comm','inv.invoices_id = comm.invoices_id')
                ->where(array(
                    'inv.created_date >='     => $start_date,
                    'inv.created_date <='     => $end_date
                ));
                foreach($_POST as $coulmn=>$value) {
                    $data[$coulmn] = $value;
                    if ($coulmn != 'start_date' && $coulmn != 'end_date') {
                        if(!empty($value)){
                            if($coulmn == 'commodity' || $coulmn == 'hs_code'){
                                $a= 'comm.';
                                $this->db->where($a.$coulmn, $value);
                            }
                            else{
                                $a= 'inv.';
                                $this->db->where($a.$coulmn, $value);
                            }
                        }
                    }
                }
            $this->db->group_by('inv.invoices_id');
            $invoices = $this->db->get()->result();
            $data['invoices'] = $invoices;
        }
        $this->report_model->_table_name = 'tbl_client';
        $this->report_model->_order_by = 'name';
        $data['all_clients'] = $this->report_model->get();
        $this->report_model->_table_name = 'tbl_countries';
        $this->report_model->_order_by = 'id';
        $data['all_countries'] = $this->report_model->get();

        $data['subview'] = $this->load->view('admin/report/sales_report', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function getParams($reportname, $param){
        $qoute = (isset($param)) ? '&': '';

        foreach($_POST as $coulmn=>$value) {
            $param .= $qoute . $coulmn .'=' . $value;
            if ( $qoute == '' ) {
                $qoute = '&';
            }

            if ($coulmn != 'start_date' && $coulmn != 'end_date') {
                if(!empty($value)){
                    if($coulmn == 'commodity' || $coulmn == 'hs_code'){
                        $a= 'comm.';
                        $this->db->where($a.$coulmn, $value);
                    }
                    else{
                        $a= 'inv.';
                        $this->db->where($a.$coulmn, $value);
                    }
                }
            }
        }

        $param =  $this->ReportURL($reportname. '&'. $param);

        return $param;
    }

    public function sales_report_birt() {
        $data['title'] = lang('sales_report');
        $param = '';
        if($_POST){
            $start_date = date('Y-m-d',strtotime($this->input->post('start_date')));
            $end_date = date('Y-m-d',strtotime($this->input->post('end_date')));
            $data['start_date'] = $start_date;
            $data['end_date'] = $end_date;
            $param =  'start_date=' . $start_date . '&end_date=' . $end_date ;


        }

        $data['report_url'] = $this->getParams('sales_report.rptdesign', $param);

        $this->report_model->_table_name = 'tbl_client';
        $this->report_model->_order_by = 'name';
        $data['all_clients'] = $this->report_model->get();
        $this->report_model->_table_name = 'tbl_countries';
        $this->report_model->_order_by = 'id';
        $data['all_countries'] = $this->report_model->get();
        $data['subview'] = $this->load->view('admin/report/sales_report', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function invoice_report() {
        $data['title'] = lang('invoice_report');
        if($_POST){
            $start_date = date('Y-m-d',strtotime($this->input->post('start_date')));
            $end_date = date('Y-m-d',strtotime($this->input->post('end_date')));
            $data['start_date'] = $start_date;
            $data['end_date'] = $end_date;

            $this->db->select('*')
                ->from('tbl_invoices inv')
                ->join('tbl_saved_commodities comm','inv.invoices_id = comm.invoices_id')
                ->where(array(
                    'inv.created_date >='     => $start_date,
                    'inv.created_date <='     => $end_date
                ));
            foreach($_POST as $coulmn=>$value) {
                $data[$coulmn] = $value;
                if ($coulmn != 'start_date' && $coulmn != 'end_date') {
                    $data[$coulmn] = $value;
                    if(!empty($value)){
                        if($coulmn == 'commodity' || $coulmn == 'hs_code'){
                            $a= 'comm.';
                            $this->db->where($a.$coulmn, $value);
                        }
                        else{
                            $a= 'inv.';
                            $this->db->where($a.$coulmn, $value);
                        }
                    }
                }
            }
            $this->db->group_by('inv.invoices_id');
            $invoices = $this->db->get()->result();
            $data['invoices'] = $invoices;

        }
        $this->report_model->_table_name = 'tbl_client';
        $this->report_model->_order_by = 'name';
        $data['all_clients'] = $this->report_model->get();
        $this->report_model->_table_name = 'tbl_countries';
        $this->report_model->_order_by = 'id';
        $data['all_countries'] = $this->report_model->get();
       // $data['origins'] = $this->report_model->check_groupby_all('tbl_invoices', '', array('origin'),array('origin'));
        $data['sheds'] = $this->report_model->check_groupby_all('tbl_invoices', '', array('port'), array('port'));
        $data['subview'] = $this->load->view('admin/report/invoice_report', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }


    public function job_report() {
        $data['title'] = lang('job_report');
        if($_POST){
            $start_date = date('Y-m-d',strtotime($this->input->post('start_date')));
            $end_date = date('Y-m-d',strtotime($this->input->post('end_date')));
            $data['start_date'] = $start_date;
            $data['end_date'] = $end_date;
            $this->db->select('*')
                ->from('tbl_invoices inv')
                ->join('tbl_saved_commodities comm','inv.invoices_id = comm.invoices_id')
                ->join('tbl_financial_information fin', 'inv.invoices_id = fin.invoices_id')
                ->where(array(
                    'inv.created_date >='     => $start_date,
                    'inv.created_date <='     => $end_date
                ));
            foreach($_POST as $coulmn=>$value) {
                $data[$coulmn] = $value;
                if ($coulmn != 'start_date' && $coulmn != 'end_date') {
                    if(!empty($value)){
                        if($coulmn == 'commodity' || $coulmn == 'hs_code'){
                            $a= 'comm.';
                            $this->db->where($a.$coulmn, $value);
                        }
                        else{
                            $a= 'inv.';
                            $this->db->where($a.$coulmn, $value);
                        }
                    }
                }
            }
            $this->db->group_by('inv.invoices_id');
            $invoices = $this->db->get()->result();
            $data['invoices'] = $invoices;
        }
        $this->report_model->_table_name = 'tbl_client';
        $this->report_model->_order_by = 'name';
        $data['all_clients'] = $this->report_model->get();
        /*** GET COUNTRIES ***/
        $this->report_model->_table_name = 'tbl_countries';
        $this->report_model->_order_by = 'id';
        $data['all_countries'] = $this->report_model->get();
        $data['sheds'] = $this->report_model->check_groupby_all('tbl_invoices', '', array('port'), array('port'));
        $data['subview'] = $this->load->view('admin/report/job_report', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    /*** BIRT REPORT DESIGNER ***/
    public function invoice_report_new(){
        $data['report_type'] = 'new_invoice_report';
        $data['subview'] = $this->load->view('admin/report/report_iframe',$data,TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
    public function sales_report_new(){
        $data['report_type'] = 'sales_reports';
        $data['subview'] = $this->load->view('admin/report/report_iframe',$data,TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function ReportURL($type)
    {
        $fname = "/reports/".$type; //.".rptdesign";
        $dest = "http://localhost:8080/birtvu/frameset?__report=" ;
        $dest .= $fname; //urlencode( $fname );
        $data['report_type'] = $dest;
        //header("Location: $dest" );
        return $dest;//$this->load->view('admin/report/report_iframe', $data, true);
    }
    public function report_iframe($type){

        //$type = urldecode($type);

        //$type = str_replace(">>", "&", $type);
        $fname = "/reports/".$type; //.".rptdesign";
        $dest = "http://localhost:8080/birtvu/frameset?__report=" ;
        $dest .= $fname; //urlencode( $fname );
        $data['report_type'] = $dest;
        //header("Location: $dest" );
        return $dest;//$this->load->view('admin/report/report_iframe', $data, true);
    }
    /*** END BIRT REPORT DESIGNER ***/
    /*** Sales Tax Report Server Side Listing ***/
    public function sales_tax_report() {
        check_url();
        $data['title'] = "All Sales Tax Report";
        $subview = 'sales_tax_report';
        if($_POST) {
            $data['date']=$_POST['date'];
            /*$date = date('Y-m-d',strtotime($this->input->post('date')));
            $data['date'] = $date;*/
        }

        $this->invoice_model->_table_name = 'tbl_bills';
        $this->invoice_model->_primary_key = 'bill_id';
        $data['all_sales'] = $this->invoice_model->get();
        
        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'name';
        $data['all_client'] = $this->invoice_model->get();

        $user_id = $this->session->userdata('user_id');
        $user_info = $this->invoice_model->check_by(array('user_id' => $user_id), 'tbl_users');
        $data['role'] = $user_info->role_id;

        $data['subview'] = $this->load->view('admin/report/' . $subview, $data, TRUE);
        $this->load->view('admin/_layout_main', $data);

    }
    public function all_sales_tax_report($date=null)
    {
        $search_array=array();
        $rows = array();
        $search_value=$_POST['search']['value'];
        if ($search_value){
            $search_array=array(
                'cl.name'  => $search_value
            );
        }
        $length = 10; $start = 1;
        if($_POST['length'] != -1){
            $length = $_POST['length'];
            $start = $_POST['start'];
        }
        $sales_tax_search_list = $this->report_model->sales_tax_report_searching_list($search_array,$length,$start,$date);
        $sales_tax_search_list_rows = $this->report_model->sales_tax_report_searching_list_rows($search_array,$date);
        $total_sales_tax = 0;
        foreach($sales_tax_search_list as $sales_tax){
            $job_no = $this->invoice_model->job_no_creation($sales_tax->invoices_id);
            $client_info = $this->invoice_model->check_by(array('client_id' => $sales_tax->client_id), 'tbl_client');
            $agency_commission = $sales_tax->service_charges;
            $tax_per = $agency_commission / 100 * $sales_tax->gst;
            $total_sales_tax += $tax_per;
            $rows[] = array(
                $sales_tax->bill_id,
                $client_info->name,
                $sales_tax->bill_no,
                $job_no,
                $sales_tax->lc_no,
                $sales_tax->currency." ".number_format($this->invoice_model->get_invoice_value($sales_tax->invoices_id),2),
                number_format(($this->invoice_model->get_invoice_value($sales_tax->invoices_id)*$sales_tax->exchange_rate),2),
                number_format($agency_commission,2),
                number_format($tax_per,2)
            );
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $sales_tax_search_list_rows,
            "recordsFiltered" => $sales_tax_search_list_rows,
            "data" => $rows,
            "total" => number_format($total_sales_tax,2)
        );
        echo json_encode($output);
    }
}