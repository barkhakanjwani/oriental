<?php
class Consignor extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('client_model');
        $this->load->model('invoice_model');
        $this->load->model('accounts_model');
        $this->load->model('user_model');
    }

    public function new_consignor($id = NULL) {
        check_url();
        $data['title'] = "New Consigner";
        $data['page'] = lang('consignor');
        $data['sub_active'] = lang('new_consignor');
        if (!empty($id)) {
            $id = decode($id);
            $this->client_model->_table_name = "tbl_consignors";
            $this->client_model->_order_by = "consignor_id";
            $data['consignor_info'] = $this->client_model->get_by(array('consignor_id' => $id), TRUE);
            $data['consignor_documents'] = $this->client_model->check_by_all(array('consignor_id' => $id), 'tbl_consignor_documents');
            $data['consignor_persons'] = $this->client_model->check_by_all(array('consignor_id'=>$id), 'tbl_consignor_persons');
        }
        if (!empty($data['client_info']) && $data['client_info']->client_status == 2) {
            $data['company'] = 1;
        } else {
            $data['person'] = 1;
        }
        
        $this->client_model->_table_name = "tbl_countries";
        $this->client_model->_order_by = "id";
        $data['countries'] = $this->client_model->get();
        
        $this->client_model->_table_name = 'tbl_currencies';
        $this->client_model->_order_by = 'name';
        $data['currencies'] = $this->client_model->get();
        
        $this->client_model->_table_name = 'tbl_client';
        $this->client_model->_order_by = 'client_id';
        $data['all_client_info'] = $this->client_model->get();

        $data['subview'] = $this->load->view('admin/consignor/new_consignor', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
    
    public function check_consignor_email($id=NULL){
        $email = $_POST['email'];
        if(!empty($id)){
            $id = decrypt($id);
            $get_email = $this->db->select('*')
                ->from('tbl_consignors')
                ->where('consignor_id != '.$id)
                ->where('email',$email)
                ->get()->result();
        }
        else{
            $get_email = $this->invoice_model->check_by(array('email' => $email), 'tbl_consignors');
        }
        if(!empty($get_email)){
            echo json_encode(false);
            die();
        }
        else{
            echo json_encode(true);
            die();
        }
    }

    public function change_status($flag, $id) {
        check_url();
        if ($flag == 1) {
            $msg = 'Active';
        } else {
            $msg = 'Deactive';
        }
        $id = decrypt($id);
        $where = array('consignor_id' => $id);
        $action = array('consignor_status' => $flag, 'updated_by'=>$this->session->userdata('user_id'));
        $this->user_model->set_action($where, $action, 'tbl_consignors');

        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'consignor',
            'module_field_id' => $id,
            'activity' => lang('activity_change_consignor_status'),
            'icon' => 'fa-user',
            'value1' => $msg,
        );
        $this->user_model->_table_name = 'tbl_activities';
        $this->user_model->_primary_key = "activities_id";
        $this->user_model->save($activities);

        $type = "success";
        $message = "Consigner " . $msg . " Successfully!";
        set_message($type, $message);
        redirect('admin/consignor/manage_consignor');
    }

    public function save_consignor($id = NULL) {
        $data = $this->client_model->array_from_post(array('name', 'email', 'email_2', 'email_3', 'client_id', 'short_note', 'phone', 'mobile', 'fax', 'ntn', 'strn', 'address', 'city', 'zipcode', 'country'));
        if (!empty($_FILES['profile_photo']['name'])){
            $val = $this->client_model->uploadImage('profile_photo');
            $val == TRUE || redirect('admin/consignor/new_consignor');
            $data['profile_photo'] = $val['path'];
        }
        $id = (!empty($id))?decrypt($id):NULL;
        if(!empty($id)){
            $data['updated_by'] = $this->session->userdata('user_id');
        }
        else{
            $data['created_by'] = $this->session->userdata('user_id');
        }
        $this->client_model->_table_name = 'tbl_consignors';
        $this->client_model->_primary_key = "consignor_id";
        $consignor_id = $this->client_model->save($data, $id);
        if(!empty($id)){
            $consignor_documents = $this->client_model->check_by(array('consignor_id'=>$id), 'tbl_consignor_documents');
            if(!empty($consignor_documents)){
                $this->db->where('consignor_id', $id);
                $this->db->delete('tbl_consignor_documents');
            }
            $consignor_persons = $this->client_model->check_by(array('consignor_id'=>$id), 'tbl_consignor_persons');
            if(!empty($consignor_persons)){
                $this->db->where('consignor_id', $id);
                $this->db->delete('tbl_consignor_persons');
            }
        }
    
        /*** DOCUMENTS SECTION MULTI FIELDS INSERTION ***/
        $document_title = $this->input->post('document_title', TRUE);
        $document_date = $this->input->post('document_date', TRUE);
        $document_comment = $this->input->post('document_comment', TRUE);
    
        for ($i=0; $i<count($document_title); $i++){
            if (!empty($document_title[$i])){
                $document_data = array(
                    'consignor_id' => $consignor_id,
                    'consignor_document_title' => $document_title[$i],
                    'consignor_document_date' => $document_date[$i],
                    'consignor_document_comment' => $document_comment[$i],
                );
                if (!empty($_FILES['document_file']['name'][$i])) {
                    $_FILES['file']['name'] = $_FILES['document_file']['name'][$i];
                    $_FILES['file']['type'] = $_FILES['document_file']['type'][$i];
                    $_FILES['file']['tmp_name'] = $_FILES['document_file']['tmp_name'][$i];
                    $_FILES['file']['error'] = $_FILES['document_file']['error'][$i];
                    $_FILES['file']['size'] = $_FILES['document_file']['size'][$i];
        
                    $config['upload_path'] = 'uploads/consignor_documents/';
                    $config['allowed_types'] = '*';
                    $config['max_size'] = '5000';
                    $config['file_name'] = $_FILES['document_file']['name'][$i];
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('file')) {
                        $error = array('error' => $this->upload->display_errors());
                    } else {
                        $data1 = $this->upload->data();
                        $document_data['consignor_document_file'] = 'uploads/consignor_documents/' . $data1['file_name'];
                    }
                }else{
                    $document_data['consignor_document_file'] = $_POST['document_file_2'][$i];
                }
                $this->invoice_model->_table_name = 'tbl_consignor_documents';
                $this->invoice_model->_primary_key = 'consignor_document_id';
                $this->invoice_model->save($document_data);
            }
        }
        /*** END DOCUMENTS SECTION MULTI FIELDS INSERTION ***/
    
        /*** CONTACT PERSONS ***/
        $consignor_person_name = $this->input->post('contact_p_name', TRUE);
        $consignor_person_email = $this->input->post('contact_p_email', TRUE);
        $consignor_person_contact = $this->input->post('contact_p_contact', TRUE);
        $consignor_person_designation = $this->input->post('contact_p_designation', TRUE);
    
        for ($i=0; $i<count($consignor_person_name); $i++){
            if (!empty($consignor_person_name[$i])){
                $person_data = array(
                    'consignor_id' => $consignor_id,
                    'consignor_person_name' => $consignor_person_name[$i],
                    'consignor_person_email' => $consignor_person_email[$i],
                    'consignor_person_contact' => $consignor_person_contact[$i],
                    'consignor_person_designation' => $consignor_person_designation[$i]
                );
                $this->invoice_model->_table_name = 'tbl_consignor_persons';
                $this->invoice_model->_primary_key = 'consignor_person_id';
                $this->invoice_model->save($person_data);
            }
        }
        /*** END CONTACT PERSONS ***/
        
        if (!empty($id)) {
            $action = lang('activity_updated_consignor');
            $msg = lang('consignor_updated');
        } else {
            $id = $consignor_id;
            $action = lang('activity_added_consignor');
            $msg = lang('consignor_added');
        }
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'consignor',
            'module_field_id' => $id,
            'activity' => $action,
            'icon' => 'fa-user',
            'value1' => $this->input->post('name')
        );
        $this->client_model->_table_name = 'tbl_activities';
        $this->client_model->_primary_key = "activities_id";
        $this->client_model->save($activities);
        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/consignor/manage_consignor');
    }

    public function manage_consignor() {
        check_url();
        $data['title'] = "Manage Consigner"; //Page title
        $data['page'] = lang('consignor');
        $data['sub_active'] = lang('manage_consignor');

        // get all client
        $this->client_model->_table_name = 'tbl_consignors';
        $this->client_model->_order_by = 'consignor_id';
        $data['all_consignor_info'] = $this->client_model->get();

        $data['subview'] = $this->load->view('admin/consignor/manage_consignor', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function manage_consignors()
    {
        $search_array=array();
        $rows = array();
        $search_value=$_POST['search']['value'];
        if ($search_value){
            $search_array=array(
                'co.name'  => $search_value
            );
        }
        $length = 10; $start = 1;
        if($_POST['length'] != -1){
            $length = $_POST['length'];
            $start = $_POST['start'];
        }
        $consignor_list = $this->client_model->consignor_searching_list($search_array,$length,$start);
        $consignor_list_rows = $this->client_model->consignor_searching_list_rows($search_array);
        $i = $_POST['start']+1;
        foreach($consignor_list as $consignor_details){
            if ($consignor_details->consignor_status == 1){
                $status = '<a data-toggle="tooltip" data-placement="top" title="Click to Deactive" href="'.base_url('admin/consignor/change_status/0/'. encrypt($consignor_details->consignor_id)).'">'.lang('active').'</a>';
            }else{
                $status = '<a data-toggle="tooltip" data-placement="top" title="Click to Active "  href="'.base_url('admin/consignor/change_status/1/'. encrypt($consignor_details->consignor_id)).'">'. lang('deactive').'</a>';
            }

            $rows[] = array(
                $consignor_details->name,
                $consignor_details->mobile,
                $consignor_details->email,
                client_name($consignor_details->client_id),
                $status,
                btn_edit('admin/consignor/new_consignor/' . encode($consignor_details->consignor_id))
            );
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $consignor_list_rows,
            "recordsFiltered" => $consignor_list_rows,
            "data" => $rows
        );
        echo json_encode($output);
    }

}
