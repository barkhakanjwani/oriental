<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bill extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('invoice_model');
        $this->load->model('accounts_model');

        $this->load->helper('ckeditor');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "99.8%",
                'height' => "400px"
            )
        );
    }

    public function manage_bill($action = NULL, $id = NULL) {
        check_url();
        $id = decode($id);
        $data['page'] = lang('bills');
        if ($action == 'create_bill') {
            $this->db->select('*');
            $this->db->from('tbl_invoices');
            $this->db->join('tbl_client', 'tbl_invoices.client_id = tbl_client.client_id');
            $this->db->where('tbl_invoices.invoices_id', $id);
            $data['job_info'] = $this->db->get()->row();
            $data['receipt_voucher_info'] = $this->accounts_model->receipt_info($data['job_info']->invoices_id);
            $data['advances'] = $this->invoice_model->pre_job_advances($id);
            $data['deposit_amount'] = $this->invoice_model->security_deposit_amount($id);
            $this->invoice_model->_table_name = 'tbl_job_expenses';
            $data['job_expenses'] = $this->invoice_model->get();
            $subview = 'manage_bill';
        }
        elseif($action == 'edit_bill'){
            $data['bill_info'] = $this->invoice_model->check_by(array('bill_id' => $id), 'tbl_bills');
            $bill_info = $data['bill_info'];
            $this->db->select('*');
            $this->db->from('tbl_invoices');
            $this->db->join('tbl_client', 'tbl_invoices.client_id = tbl_client.client_id');
            $this->db->where('tbl_invoices.invoices_id', $bill_info->invoices_id);
            $data['job_info'] = $this->db->get()->row();
            $data['receipt_voucher_info'] = $this->accounts_model->receipt_info($bill_info->invoices_id);
            $data['advances'] = $this->invoice_model->pre_job_advances($bill_info->invoices_id);
            $data['deposit_amount'] = $this->invoice_model->security_deposit_amount($bill_info->invoices_id);
            $this->invoice_model->_table_name = 'tbl_job_expenses';
            $data['job_expenses'] = $this->invoice_model->get();
            $subview = 'manage_bill';
        }
        elseif($action == 'bill_details'){
            $data['bill_info'] = $this->invoice_model->check_by(array('bill_id' => $id), 'tbl_bills');
            $bill_info = $data['bill_info'];
            $this->db->select('*');
            $this->db->from('tbl_invoices');
            $this->db->join('tbl_client', 'tbl_invoices.client_id = tbl_client.client_id');
            $this->db->join('tbl_financial_information', 'tbl_invoices.invoices_id = tbl_financial_information.invoices_id');
            $this->db->where('tbl_invoices.invoices_id', $bill_info->invoices_id);
            $data['job_info'] = $this->db->get()->row();
            $data['receipt_voucher_info'] = $this->accounts_model->receipt_info($bill_info->invoices_id);
            $data['packages'] = $this->db->where('invoices_id', $bill_info->invoices_id)->select_sum('packages')->get('tbl_containers')->row()->packages;
           /* $data['advances'] = $this->invoice_model->pre_job_advances($bill_info->invoices_id);
            $data['deposit_amount'] = $this->invoice_model->security_deposit_amount($bill_info->invoices_id);*/
            $this->invoice_model->_table_name = 'tbl_job_expenses';
            $data['job_expenses'] = $this->invoice_model->get();
            $subview = 'bill_details';
        }
        elseif($action == 'sales_tax_invoice'){
            $data['bill_info'] = $this->invoice_model->check_by(array('bill_id' => $id), 'tbl_bills');
            $bill_info = $data['bill_info'];
            $this->db->select('*');
            $this->db->from('tbl_invoices');
            $this->db->join('tbl_client', 'tbl_invoices.client_id = tbl_client.client_id');
            $this->db->join('tbl_financial_information', 'tbl_invoices.invoices_id = tbl_financial_information.invoices_id');
            $this->db->where('tbl_invoices.invoices_id', $bill_info->invoices_id);
            $data['job_info'] = $this->db->get()->row();
            $this->invoice_model->_table_name = 'tbl_job_expenses';
            $data['job_expenses'] = $this->invoice_model->get();
            $subview = 'sales_tax_invoice';
        }
        else{
            $data['title'] = "Manage Bill";
            $subview = 'manage_bill';
        }

        $user_id = $this->session->userdata('user_id');

        $user_info = $this->invoice_model->check_by(array('user_id' => $user_id), 'tbl_users');
        $data['role'] = $user_info->role_id;
        $data['subview'] = 'admin/bill/'.$subview;
        $this->load->view('admin/_layout_main2', $data);
    }

    public function search_bill(){
        check_url();
        if($_POST){
            if(isset($_POST['client_id'])){
                $client_id = $_POST['client_id'];
                $data['client_id'] = $client_id;
                $this->db->select('*');
                $this->db->from('tbl_invoices');
                $this->db->join('tbl_client', 'tbl_invoices.client_id = tbl_client.client_id');
                if($this->config->item('requisition_optional') == 'No'){
                    $this->db->join('tbl_requisitions', 'tbl_requisitions.invoices_id = tbl_invoices.invoices_id');
                    $this->db->where('tbl_requisitions.requisition_type', 'Declared');
                }
                $this->db->where('tbl_invoices.client_id', $client_id);
                $data['invoices_info'] = $this->db->get()->result();
            }
            if(isset($_POST['reference_no'])) {
                $reference_no = $_POST['reference_no'];
                $data['reference_no'] = $reference_no;
                $this->db->select('*');
                $this->db->from('tbl_invoices');
                $this->db->join('tbl_client', 'tbl_invoices.client_id = tbl_client.client_id');
                $this->db->where('tbl_invoices.reference_no', $reference_no);
                if($this->config->item('requisition_optional') == 'No'){
                    $this->db->join('tbl_requisitions', 'tbl_requisitions.invoices_id = tbl_invoices.invoices_id');
                    $this->db->where('tbl_requisitions.requisition_type', 'Declared');
                }
                $data['invoices_info'] = $this->db->get()->result();
            }
        }
        $this->invoice_model->_table_name = 'tbl_invoices';
        $this->invoice_model->_order_by = 'invoices_id';
        $data['all_invoices'] = $this->invoice_model->get();
        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'name';
        $data['all_client'] = $this->invoice_model->get();

        $data['title'] = "Search Bill";
        $subview = 'search_bill';

        $data['subview'] = $this->load->view('admin/bill/' . $subview, $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
	
	public function save_bill($id = NULL) {

        $get_bill = $this->input->post('TM_AMOUNT');
        $job_info = $this->invoice_model->check_by(array('invoices_id' => $this->input->post('invoices_id')), 'tbl_invoices');
        /** Advances from Clients **/
        $adc_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Liabilities', 'NAME' => 'Current Liabilities'), 'sub_heads');
        $adc_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $adc_head->SUB_HEAD_ID, 'H_NAME' => 'Advances from clients'), 'accounts_head');
        $adc_account_id = $this->invoice_model->check_by(array('H_ID' => $adc_sub_head->H_ID, 'CLIENT_ID' => $job_info->client_id,'A_NAME'=>client_name($job_info->client_id).' (Advances from clients)'), 'accounts');
        /*Getting Advances */
        $received_advances=$this->accounts_model->advances_by_client($adc_account_id->A_ID,'Credit')->TM_AMOUNT;
        $adjust_advances=$this->accounts_model->advances_by_client($adc_account_id->A_ID,'Debit')->TM_AMOUNT;
        $remaining_advances=$received_advances-$adjust_advances;

		if (!empty($id)) {
		    $id = decrypt($id);
		    $data = $this->invoice_model->array_from_post(array('invoices_id', 'bill_no', 'delivery_date', 'service_charges', 'gst','withholding_tax'));
            $data['current_advance'] = $remaining_advances;
            $data['total_bill'] = $get_bill[0];
			$data['updated_by'] = $this->session->userdata('user_id');
			$this->invoice_model->_table_name = 'tbl_bills';
			$this->invoice_model->_primary_key = 'bill_id';
            $bill_id = $id;
            $this->invoice_model->save($data, $id);

            $bill_detail_id = $_POST['bill_detail_id'];
            $account_id = $_POST['account_id'];
            $bill_amount = $_POST['bill_amount'];

            for($i=0; $i<count($account_id); $i++){
                $bill_data = array(
                    'job_expense_id'=>$account_id[$i],
                    'bill_amount'=>$bill_amount[$i]
                );
                $this->invoice_model->_table_name = 'tbl_bill_details';
                $this->invoice_model->_primary_key = 'bill_detail_id';
                $this->invoice_model->save($bill_data,$bill_detail_id[$i]);
            }

            $TM_ID = $this->input->post('TM_ID');
            $TM_AMOUNT = $this->input->post('TM_AMOUNT');
            for($i=0; $i<count($TM_ID); $i++){
                $this->db->where('TM_ID', $TM_ID[$i]);
                $this->db->update("transactions_meta", array('TM_AMOUNT' => $TM_AMOUNT[$i]));
            }

            $action = lang('activity_bill_updated');
            $msg = lang('bill_updated');
        }
        else {
			$data_transaction = array(
				'T_DATE'=>date('Y-m-d'),
				'T_PAY'=>'',
				'T_TYPE'=>'Bill',
                'created_by'=>$this->session->userdata('user_id')
			);
			$this->accounts_model->create_transaction($data_transaction);
			$transaction_id = $this->db->insert_id();
			
			$data = $this->invoice_model->array_from_post(array('invoices_id', 'bill_no', 'delivery_date', 'service_charges', 'gst','withholding_tax'));
            $data['current_advance'] = $remaining_advances;
            $data['total_bill'] = $get_bill[0];
			$data['transaction_id'] = $transaction_id;
            $data['created_by'] = $this->session->userdata('user_id');
			$this->invoice_model->_table_name = 'tbl_bills';
			$this->invoice_model->_primary_key = 'bill_id';
            $bill_id = $this->invoice_model->save($data);

            $account_id = $_POST['account_id'];
            $bill_amount = $_POST['bill_amount'];

            for($i=0; $i<count($account_id); $i++){
                $bill_data = array(
                    'bill_id'=>$bill_id,
                    'job_expense_id'=>$account_id[$i],
                    'bill_amount'=>$bill_amount[$i]
                );
                $this->invoice_model->_table_name = 'tbl_bill_details';
                $this->invoice_model->_primary_key = 'bill_detail_id';
                $this->invoice_model->save($bill_data);
            }

			$A_ID = $this->input->post('A_ID');
			$TM_AMOUNT = $this->input->post('TM_AMOUNT');
			$TM_TYPE = $this->input->post('TM_TYPE');

            for($i=0; $i<count($A_ID); $i++){
				$account_data = array(
					'T_ID'=>$transaction_id,
					'A_ID'=>$A_ID[$i],
					'PARTICULARS'=>'',
					'TM_AMOUNT'=>$TM_AMOUNT[$i],
					'TM_TYPE'=>$TM_TYPE[$i],
					'IS_ACTIVE'=>1
				);
				/*$total_account_amount += $TM_AMOUNT[$i];*/
				$this->invoice_model->_table_name = 'transactions_meta';
				$this->invoice_model->_primary_key = 'TM_ID';
				$this->invoice_model->save($account_data);
			}
            $action = lang('activity_bill_created');
            $msg = lang('bill_created');
        }

        $job_no = $this->invoice_model->job_no_creation($_POST['invoices_id']);

        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'bill',
            'module_field_id' => $bill_id,
            'activity' => $action,
            'icon' => 'fa-circle-o',
            'value1' => $job_no
        );
        $this->invoice_model->_table_name = 'tbl_activities';
        $this->invoice_model->_primary_key = 'activities_id';
        $this->invoice_model->save($activity);

        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/bill/all_bills');
    }
    
    /*** Server Side Listing ***/
    public function all_bills() {
        check_url();
        $data['title'] = "All Bills";
        $subview = 'all_bills';
        if($_POST) {
            $data['client_id']=$_POST['client_id'];
            $data['reference_no']=$_POST['reference_no'];
            $data['commodities']=$_POST['commodity'];
            $data['bl_no']=$_POST['bl_no'];
        }

        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'name';
        $data['all_client'] = $this->invoice_model->get();

        $user_id = $this->session->userdata('user_id');
        $user_info = $this->invoice_model->check_by(array('user_id' => $user_id), 'tbl_users');
        $data['role'] = $user_info->role_id;

        $data['subview'] = $this->load->view('admin/bill/' . $subview, $data, TRUE);
        $this->load->view('admin/_layout_main', $data);

    }
    public function all_bill($client_id=null,$reference_no=null,$bl_no=null,$commodity=null)
    {
        $search_array=array();
        $rows = array();
        $search_value=$_POST['search']['value'];
        if ($search_value){
            $search_array=array(
                'cl.client_id'  => $search_value,
                'inv.reference_no'  => $search_value,
                'inv.bl_no'  => $search_value,
                'comm.commodity'  => $search_value
            );
        }
        $length = 10; $start = 1;
        if($_POST['length'] != -1){
            $length = $_POST['length'];
            $start = $_POST['start'];
        }
        $bill_search_list = $this->invoice_model->bill_searching_list($search_array,$length,$start,$client_id,$reference_no,$bl_no,$commodity);
        $bill_search_list_rows = $this->invoice_model->bill_searching_list_rows($search_array,$client_id,$reference_no,$bl_no,$commodity);
        $i = $_POST['start']+1;
        foreach($bill_search_list as $bills){
            $job_no = $this->invoice_model->job_no_creation($bills->invoices_id);
            $client_info = $this->invoice_model->check_by(array('client_id' => $bills->client_id), 'tbl_client');
            $bill_info = $this->invoice_model->check_by(array('invoices_id' => $bills->invoices_id), 'tbl_bills');
            $action = '<div class="btn-group">
                    <button class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                        Action
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">';
            if(!empty($bill_info)) {
                $action.='<li><a href="'. base_url().'admin/bill/manage_bill/edit_bill/'. encode($bill_info->bill_id).'" target="_blank">Edit Bill</a></li><li><a href="'. base_url().'admin/bill/manage_bill/bill_details/'. encode($bill_info->bill_id).'" target="_blank">Preview Bill</a></li><li><a href="'. base_url().'admin/bill/manage_bill/sales_tax_invoice/'. encode($bill_info->bill_id).'" target="_blank">Sales Tax Invoice</a></li><li><a href="'. base_url().'admin/invoice/manage_invoice/payment/'. encode($bills->invoices_id).'/'. encode($bills->client_id) .'" target="_blank">'. lang('payment') .'</a></li>';
            } else {
                $action.='<li><a href="'. base_url().'admin/bill/manage_bill/create_bill/'. encode($bills->invoices_id).'">Create Bill</a></li>';
            }
            $action.='</ul>
                </div>';

            $rows[] = array(
                $job_no,
                $client_info->name,
                (!empty($bills->created_date))?strftime(config_item('date_format'), strtotime($bills->created_date)):'',
                $bills->currency." ".number_format($this->invoice_model->get_invoice_value($bills->invoices_id),2),
                $action
            );
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $bill_search_list_rows,
            "recordsFiltered" => $bill_search_list_rows,
            "data" => $rows
        );
        echo json_encode($output);
    }
}
