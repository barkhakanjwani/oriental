
<div class="container">
    <div class="row" >
        <form role="form" action="<?= base_url('login') ?>" method="post">
            <div class="col-sm-4"></div>
            <div class="col-sm-4"  style= "margin:20% 0 0 0;">
                <img class="logo1" src="<?php echo base_url(); ?>asset/img/frago-logosss (1).png" >

                <?= message_box('success'); ?>
                <div class="error_login">
                    <?php echo validation_errors(); ?>
                    <?php
                    $error = $this->session->flashdata('error');
                    if (!empty($error)) {
                        ?>
                        <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                    <?php } ?>
                </div>
                <div class="form-group ">
                    <div class="input-group" style="padding: 10px 0;">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-user"></span>
                        </div>
                        <input class="form-control" type="text" name="user_name" required="true" placeholder="<?= lang('username') ?>" />
                    </div>
                </div>

                <div class="form-group ">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-lock"></span>
                        </div>
                        <input class="form-control" type="password" name="password" required="true" placeholder="<?= lang('password') ?>" />
                    </div>
                </div>
                <input type="image" class="center" src="<?php echo base_url(); ?>asset/img/LOGIN1.png" alt="Submit">
            </div>
            <div class="col-sm-4"></div>
        </form>
    </div>
</div>

<!--<div class="container">
    <div class="row">
        <div class="col-md-3">
        </div>
        <div class="col-md-3" style="text-align: left;">
            <img src="<?php /*echo base_url(); */?>asset/img/tabani.png" width="200">
        </div>
        <div class="col-md-3" style="text-align: right;">
            <img src="<?php /*echo base_url(); */?>asset/img/yy.png" width="100">
        </div>
        <div class="col-md-3">
        </div>
    </div>
</div>-->