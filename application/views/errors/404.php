<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>404 ERROR</title>
	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:900" rel="stylesheet">
	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="<?= base_url('asset/css/404/') ?>style.css" />
    <style>
        a{
            background: #2196F3;
            padding: 5px;
            color: #fff;
            text-decoration: none;
            font-family: 'Montserrat', sans-serif;
        }
        a:hover{
            background: #1089e8;
        }
    </style>
</head>

<body>

	<div id="notfound">
		<div class="notfound">
			<div class="notfound-404">
				<h3>Oops! Page not found</h3>
				<h1><span>4</span><span>0</span><span>4</span></h1>
			</div>
			<h2>we are sorry, but the page you requested was not found</h2>
            <a href="<?= base_url('admin/settings/update_profile') ?>">HOME</a>
		</div>
	</div>

</body>

</html>
