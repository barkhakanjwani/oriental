<?php error_reporting(0) ?>
<!-- style form css -->
<style>
    .redborder1{
        border:1px solid red;
    }
    .redborder2{
        border:1px solid red;
    }
</style>
<!-- /style form css -->
    <!-- Content area -->
    <div class="content">

        <!-- Highlighting rows and columns -->
                <?php echo form_open(base_url('client/report/job_report'),array('class'=>"form-horizontal")); ?>
                <div class="row">
                    <section class="panel panel-default">
                        <header class="panel-heading  "><?= lang('search_by'); ?></header>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-xs-2">
                                    <input type="text" name="start_date" class="form-control datepicker" placeholder="From Date" autocomplete="off" value="<?= (!empty($start_date))?$start_date:'' ?>" required>
                                </div>
                                <div class="col-xs-2">
                                    <input type="text" name="end_date" class="form-control datepicker" placeholder="To Date" value="<?= (!empty($end_date))?$end_date:'' ?>" autocomplete="off" required>
                                </div>
                                <div class="col-xs-3">
                                    <select name="client_id" class="form-control select_box" style="width: 100%">
                                        <option value=""><?= lang('job_status') ?></option>
                                        <?php if(!empty($all_status)):
                                            foreach ($all_status as $status): ?>
                                                <option value="<?php echo $status->job_status_id; ?>" <?php if(!empty($job_status_id)){
                                                    echo ($status->job_status_id == $job_status_id)?'selected':'';
                                                }?>><?php echo $status->job_status_title; ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" name="reference_no" placeholder="<?= lang('reference_no') ?>" value="<?= (!empty($reference_no))?$reference_no:'' ?>" />
                                </div>
                                <div class="col-xs-2">
                                    <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> <?= lang('search') ?></button>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <?php echo form_close(); ?>

            <div class="row">
                <section class="panel panel-default">
                    <header class="panel-heading  "><?= $title; ?></header>
                    <div class="panel-body">
                    <?php if(isset($start_date) && isset($end_date)): ?>
                        <h4 class="text-center text-danger" style="font-family: inherit;"><?= date('d-m-Y',strtotime($start_date)) ?>&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;<?= date('d-m-Y',strtotime($end_date)); ?></h4>
                    <?php endif; ?>
                    <table class="table table-bordered table-responsive table-hover" id="table">
                        <thead>
                        <tr style="border-top: 1px solid #dddddd;">
                            <th><?= lang('created_date') ?></th>
                            <th><?= lang('reference_no') ?></th>
                            <th><?= lang('total_amount') ?></th>
                            <th>Paid Amount</th>
                            <th class="text-center"><?= lang('status') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $total_bill=0; $total_account=0; ?>
                        <?php if(!empty($invoices)): ?>
                            <?php foreach ($invoices as $inv){
                                $bill = $this->report_model->check_by(array('invoices_id'=>$inv->invoices_id), 'tbl_bills');
                                if(!empty($bill)){
                                    $client_name = $this->report_model->get_any_field('tbl_client', array('client_id' => $inv->client_id), 'name');
                                    $status = $this->report_model->get_time_different(strtotime($inv->due_date), strtotime(date('Y-m-d')));
                                    $job_activity = $this->db->select('job_activity_id')
                                        ->from('tbl_job_activity')
                                        ->where('invoices_id',$inv->invoices_id)
                                        ->order_by('job_activity_id','DESC')
                                        ->get()->row()->job_activity_id;
                                    /*** Cost of Sales ***/
                                    $cost_sub_heads = $this->report_model->get_any_field('sub_heads', array('HEAD_TYPE'=>'Expense', 'NAME'=>'Cost of Sales'),'SUB_HEAD_ID');
                                    $cost_of_sales = $this->report_model->check_by_all(array('SUB_HEAD_ID'=>$cost_sub_heads,'CLIENT_ID'=>$inv->client_id),'accounts');
                                    $cost = 0;
                                    foreach($cost_of_sales as $c_o_s){
                                        $costs = $this->report_model->check_by(array('T_ID'=>$bill->transaction_id, 'A_ID'=>$c_o_s->A_ID, 'PAYMENT_ID'=>NULL), 'transactions_meta');
                                        $cost += $costs->TM_AMOUNT;
                                    }
                                    /*** Payable Taxes ***/
                                    $tax_head = $this->report_model->get_any_field('accounts_head', array('H_TYPE'=>'Liabilities', 'H_NAME'=>'Payable Tax'),'H_ID');
                                    $payable_taxes = $this->report_model->check_by_all(array('CLIENT_ID'=>$inv->client_id, 'H_ID'=>$tax_head),'accounts');
                                    $tax = 0;
                                    foreach($payable_taxes as $p_tax){
                                        $taxs = $this->report_model->check_by(array('T_ID'=>$bill->transaction_id, 'A_ID'=>$p_tax->A_ID, 'PAYMENT_ID'=>NULL), 'transactions_meta');
                                        $tax += $taxs->TM_AMOUNT;
                                    }
                                    /*** Commissions ***/
                                    $commission_heads = $this->report_model->get_any_field('accounts_head', array('H_TYPE'=>'Income', 'H_NAME'=>'Commission'),'H_ID');
                                    $commissions = $this->report_model->check_by_all(array('CLIENT_ID'=>$inv->client_id, 'H_ID'=>$commission_heads),'accounts');
                                    $commission = 0;
                                    foreach($commissions as $p_commission){
                                        $comms = $this->report_model->check_by(array('T_ID'=>$bill->transaction_id, 'A_ID'=>$p_commission->A_ID, 'PAYMENT_ID'=>NULL), 'transactions_meta');
                                        $commission += $comms->TM_AMOUNT;
                                    }
                            ?>
                                <tr>
                                    <td><?= date('d-m-Y',strtotime($inv->created_date)); ?></td>
                                    <td><?= $inv->reference_no ?></td>
                                    <td>PKR <?= number_format($this->invoice_model->calculate_bill('bill_grand_total',$bill->bill_id), 2) ?></td>
                                    <td>PKR <?= number_format($this->invoice_model->calculate_to('paid_amount', $inv->invoices_id), 2) ?></td>
                                    <td class="text-center"><?= (!empty($job_activity))?job_activity($job_activity):'-' ?></td>
                                </tr>
                            <?php }} ?>
                        <?php endif; ?>
                        </tbody>
                    </table>
                    </div>
                </section>
            </div>
        </div>
        <!-- /highlighting rows and columns -->
        <?php
            if(!empty($invoices)) {
                $this->load->view('admin/components/buttons_datatable.php');
            }
        ?>