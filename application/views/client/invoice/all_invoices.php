<?= message_box('success') ?>

<section class="panel panel-default">
    <header class="panel-heading"><?= $title; ?> </header>

    <div class="panel-body">
        <table class="table table-striped DataTables " id="DataTables">
            <thead>
            <tr>
                <th><?= lang('job_no') ?></th>
                <th class="col-date"><?= lang('created_date') ?></th>
                <th class="col-date"><?= lang('due_date') ?></th>
                <th class="col-currency"><?= lang('amount') ?></th>
                <th class="col-currency"><?= lang('due_amount') ?></th>
                <th><?= lang('status') ?></th>

            </tr>
            </thead>
            <tbody>
            <?php
            foreach($all_invoices as $v_invoices){
                $bill_info = $this->invoice_model->check_by(array('invoices_id' => $v_invoices->invoices_id), 'tbl_bills');

                if($this->invoice_model->get_payment_status($v_invoices->invoices_id) == lang('fully_paid')){
                    $invoice_status = lang('fully_paid');
                    $label = "success";
                }
                elseif($this->invoice_model->get_payment_status($v_invoices->invoices_id) == lang('partially_paid')){
                    $invoice_status = lang('partially_paid');
                    $label = "info";
                }
                else{
                    $invoice_status = lang('not_paid');
                    $label = "danger";
                }
                ?>
                <tr>
                    <td><?= $this->invoice_model->job_no_creation($v_invoices->invoices_id) ?></td>
                    <td><?= strftime(config_item('date_format'), strtotime($v_invoices->created_date)) ?></td>
                    <td>
                        <?php
                        if(!empty($v_invoices->due_date)){
                            echo strftime(config_item('date_format'), strtotime($v_invoices->due_date));
                        }

                        ?>
                    </td>
                    <td>
                        <?php
                        if(!empty($bill_info)){
                            echo "PKR ".number_format($this->invoice_model->calculate_bill('bill_grand_total', $bill_info->bill_id),2);
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if(!empty($bill_info)){
                            echo "PKR ".number_format($this->invoice_model->calculate_to('invoice_due', $v_invoices->invoices_id),2);
                        }
                        ?>
                    </td>
                    <td><label class="label label-<?= $label ?>"><?= $invoice_status ?></label></td>

                </tr>
                <?php
            }

            ?>
            </tbody>
        </table>
    </div>
</section>