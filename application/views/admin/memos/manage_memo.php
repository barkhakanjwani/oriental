<link href="<?php echo base_url() ?>asset/css/bootstrap-toggle.min.css" rel="stylesheet"> 

<script src="<?php echo base_url() ?>asset/js/bootstrap-toggle.min.js"></script>

<?php echo message_box('success'); ?>

<?php echo message_box('error'); ?>

<?php

//$invoice_info = $this->invoice_model->check_by(array('invoices_id' => $invoices_id), 'tbl_invoices');

?>
<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('create_memo') ?></h1>
<div class="row">

		<div class="col-lg-12">



            <form role="form" enctype="multipart/form-data" id="form" action="<?php echo base_url(); ?>admin/memos/save_memo/<?php

            if(!empty($memo_info)){

                echo encrypt($memo_info->memo_id);

            } ?>" method="post" class="form-horizontal  ">

                <section class="panel panel-default">

                    <header class="panel-heading"><?= lang('create_memo') ?></header>

                    <div class="panel-body">

                        <div class="form-group">

                            <label class="col-lg-4 control-label"><?= lang('type') ?></Name></label>

                            <div class="col-lg-3">

                                <select class="form-control select_box" name="memo_type" required>

                                    <option value="">---</option>

                                    <option value="Debit" <?php

                                    if(!empty($memo_info)){

                                        echo ($memo_info->memo_type == 'Debit')?'selected':'';

                                    }

                                    ?>>Debit Memo</option>

                                    <option value="Credit" <?php

                                    if(!empty($memo_info)){

                                        echo ($memo_info->memo_type == 'Credit')?'selected':'';

                                    }

                                    ?>>Credit Memo</option>

                                </select>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-lg-4 control-label">Client Name</Name></label>

                            <div class="col-lg-3">

                                <select name="client_id" id="client_id" class="form-control select_box" onchange="selectJobno(this.value)" required>

                                    <option value="">---</option>

                                    <?php if(!empty($all_clients)) {

                                        foreach ($all_clients as $client) { ?>

                                            <option value="<?php echo $client->client_id; ?>"<?php

                                                if(!empty($memo_info)){

                                                    if($client->client_id == $memo_info->client_id){

                                                        echo 'selected';

                                                    }

                                                }

                                                ?>

                                            ><?php echo ucfirst($client->name); ?></option>

                                            <?php

                                        }

                                    } ?>

                                </select>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-lg-4 control-label"><?= lang('reference_no') ?></label>

                            <div class="col-lg-3">

                                <select name="invoices_id" id="invoices_id" class="form-control select_box required">

                                    <?php

                                    if(!empty($memo_info)){

                                        if(!empty($all_jobs)) {

                                            foreach ($all_jobs as $job) {

                                                ?>

                                                <option value="<?= $job->invoices_id ?>" <?= $memo_info->invoices_id == $job->invoices_id?'selected':'' ?>><?= $this->invoice_model->job_no_creation($job->invoices_id) ?></option>

                                                <?php

                                            }

                                        }

                                    }else{

                                        ?>

                                        <option value="">---</option>

                                        <?php

                                    }

                                    ?>

                                </select>

                            </div>

                        </div>

                        <div>

                        <?php

                        if(!empty($memo_charges)){

                        $counter = 0;

                        foreach($memo_charges as $memos){

                        ?>

                        <div class="form-group">

                            <label class="col-lg-4 control-label">Remarks</label>

                            <div class="col-md-2">

                                <input type="hidden" value="<?= $memos->mc_id ?>" name="mc_id[]" />

                                <input type="text" class="form-control title" name="mc_title_update[]" value="<?= $memos->mc_title ?>" required placeholder="Title" autocomplete="off" />

                            </div>

                            <div class="col-md-2">

                                <input type="number" class="form-control amount" min="0" name="mc_amount_update[]" value="<?= round($memos->mc_amount,2) ?>" required placeholder="Amount" autocomplete="off" />

                            </div>



                            <?php

                            if($counter == 0){

                            ?>

                            <div style="margin-top:5px;">

                                <button type="button" class="btn btn-primary btn-xs" id="add_multiple" style="border-radius:12px;"><i class="fa fa-plus"></i></button>

                            </div>

                        </div>

                            <?php

                        }else{

                        ?>

                            <div style="margin-top:5px;">

                                <?= btn_delete_clone('admin/memos/delete/' . encrypt($memos->memo_id).'/'. encrypt($memos->mc_id)) ?>

                            </div>

                            <?php

                        }

                        ?>

                        </div>

                            <?php

                            $counter++;

                        }

                        }

                        else {

                        ?>

                        <div class="row">

                            <div class="form-group">

                                <label class="col-lg-4 control-label">Remarks</label>

                                <div class="col-md-2">

                                    <input type="text" class="form-control title" name="mc_title[]" value="" required placeholder="Title" autocomplete="off" />

                                </div>

                                <div class="col-md-2">

                                    <input type="number" class="form-control amount" min="0" name="mc_amount[]" value="" required placeholder="Amount" autocomplete="off" />

                                </div>

                                <div style="margin-top:5px;">

                                    <button type="button" class="btn btn-primary btn-xs" id="add_multiple" style="border-radius:12px;"><i class="fa fa-plus"></i></button>

                                </div>

                            </div>

                            <?php

                            }

                            ?>

                            <div id="add_new_multiple"></div>

                        </div>





                        <div class="form-group">

                            <label class="col-lg-3 control-label"></label>

                            <div class="col-lg-4">

                                <button type="submit" class="btn btn-sm btn-success pull-right"><i class="fa fa-check"></i>

                                    <?php

                                    if(!empty($memo_info)){

                                        echo lang('update_memo');

                                    }else{

                                        echo lang('create_memo');

                                    }

                                    ?>

                                </button>

                            </div>

                        </div>

				    </div>

				</section>

            </form>

		</div>

	</div>



<script type="text/javascript">

    $("#add_multiple").click(function() {

        var columns = '<div><div class="form-group"><label class="col-lg-4 control-label">Remarks</label><div class="col-md-2"><input type="text" class="form-control title" name="mc_title[]" required placeholder="Title" autocomplete="off" /></div><div class="col-md-2"><input type="number" class="form-control amount" min="0" name="mc_amount[]" required placeholder="Amount" autocomplete="off" /></div><div style="margin-top:5px;"><button type="button" class="btn btn-danger btn-xs remCF"  style="border-radius:12px;"><i class="fa fa-minus"></i></button></div></div></div>';

        var add_new = $(columns);

        $("#add_new_multiple").append(add_new.hide().fadeIn(1000));

    });

    $("#add_new_multiple").on('click', '.remCF', function() {

        $(this).fadeOut(300, function() { $(this).parent().parent().remove(); })

    });

</script>



<script type="text/javascript">

    function selectJobno(client_id) {

        var option="";

        var option_empty = '<option value="" selected>---</option>';

        if(client_id == '') {

            $('#invoices_id').html(option_empty).hide().fadeIn(500);

            $('.select_box').select2({});

        }

        else{

            $.getJSON("<?php echo site_url('admin/memos/ajax_get_client_jobs') ?>" + "/" + client_id, function (result) {

                $.each(result, function (index, value) {

                    option = ('<option value="' + value.invoices_id + '">' + value.reference_no + '</option>')+option;

                });

                $('#invoices_id').html(option_empty+option).hide().fadeIn(500);

                $('.select_box').select2({});

            });

        }

    }

</script>