

<section class="content-header">

    <?php

    $client_info = $this->invoice_model->check_by(array('client_id' => $memo_info->client_id), 'tbl_client');

    $commodity_info = $this->invoice_model->check_by_all(array('invoices_id'=>$memo_info->invoices_id), 'tbl_saved_commodities');

    ?>

    <div class="row">

        <div class="col-sm-8">



        </div>

        <div class="col-sm-4 pull-right">

            <a onclick="print_invoice('print_invoice_new')" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Print" class="btn btn-sm btn-danger pull-right"  >

                <i class="fa fa-print"></i>

            </a>

        </div>

    </div>

</section>
<h1 class="header-<?= config_item('sidebar_theme'); ?>" style="margin: 0px 0px 0px 15px;"><?= lang('memos_detail') ?></h1>
<section class="content">

    <!-- Start Display Details -->

    <!-- Main content -->

    <div class="row" >

        <section class="invoice" id="print_invoice">

            <!-- title row -->

            <div class="row">

                <div class="col-xs-12">

                    <h2 class="page-header">

                        <img style="width: 60px;width: 60px;margin-top: -10px;margin-right: 10px;" src="<?= base_url() . config_item('invoice_logo') ?>" ><?= config_item('company_name') ?>

                    </h2>

                </div><!-- /.col -->

            </div>

            <div class="row">

                <div class="col-xs-12 table-responsive">

                    <table class="table table-bordered">

                        <tbody>

                            <tr>

                                <td><b><?= $memo_info->memo_type ?> Memo No : </b><?= $memo_info->memo_no ?></td>

                                <td><b><?= lang('date') ?> : </b><?= strftime(config_item('date_format'), strtotime($memo_info->created_date)) ?></td>

                            </tr>

                            <tr>

                                <td><b><?= lang('client') ?> : </b><?= $client_info->name ?>. . .<?= $client_info->address ?>. <?= $client_info->city ?></td>

                            </tr>

                        </tbody>

                    </table>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 table-responsive">

                    <table class="table table-bordered">

                        <thead>

                        <tr>

                            <th colspan="6">Details</th>

                        </tr>

                        </thead>

                        <tbody>

                        <tr>

                            <td><b><?= lang('lc') ?> : </b><?= $memo_info->lc_no ?></td>

                            <td><b><?= lang('reference_no') ?> : </b><?= $this->invoice_model->job_no_creation($memo_info->invoices_id) ?></td>

                        </tr>

                        <?php

                        if(!empty($commodity_info)){

                            foreach($commodity_info as $commodity){

                                ?>

                                <tr>

                                    <td><b><?= lang('commodity') ?></b></td>

                                    <td><?= $commodity->commodity ?></td>

                                </tr>

                                <?php

                            }

                        }

                        ?>

                        <?php

                        $memos = $this->invoice_model->check_by_all(array('memo_id'=>$memo_info->memo_id), 'tbl_memo_charges');

                        $total_amount = 0;

                        if(!empty($memos)){

                            foreach($memos as $memo){

                                $total_amount +=$memo->mc_amount;

                                ?>

                                <tr>

                                    <td><?= $memo->mc_title ?></td>

                                    <td><?= $memo->mc_amount ?></td>

                                </tr>

                                <?php

                            }

                        }

                        ?>

                        <tr>

                            <td><b>Total <?= lang('amount') ?></b></td>

                            <td><b><?= $total_amount; ?></b></td>

                        </tr>

                        </tbody>

                    </table>

                </div>



            </div>

        </section>

        <section style="display: none;" class="invoice" id="print_invoice_new">

            <?php $this->load->view('admin/memos/pdf_memo'); ?>

        </section>

    </div>



</section>

<script type="text/javascript">

    function print_invoice(print_invoice_new) {

        var printContents = document.getElementById(print_invoice_new).innerHTML;

        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;

    }

</script>