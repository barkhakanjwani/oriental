<?php error_reporting(0) ?>

<!-- style form css -->

<style>

    .redborder1{

        border:1px solid red;

    }

    .redborder2{

        border:1px solid red;

    }

</style>

<!-- /style form css -->

    <!-- Content area -->
<h1 class="header-<?= config_item('sidebar_theme'); ?>" ><?= lang('search_by') ?> <?= lang('sales_report') ?></h1>
    <div class="content">

        <!-- Highlighting rows and columns -->

                <?php echo form_open(base_url('admin/report/sales_report'),array('class'=>"form-horizontal")); ?>

                <div class="row">

                    <section class="panel panel-default">

                        <header class="panel-heading  "><?= lang('search_by'); ?></header>

                        <div class="panel-body">

                            <div class="form-group">

                                <div class="col-xs-2">

                                    <input type="text" name="start_date" class="form-control datepicker" placeholder="From Date" autocomplete="off" value="<?= (!empty($start_date))?$start_date:'' ?>" required>

                                </div>

                                <div class="col-xs-2">

                                    <input type="text" name="end_date" class="form-control datepicker" placeholder="To Date" value="<?= (!empty($end_date))?$end_date:'' ?>" autocomplete="off" required>

                                </div>

                                <div class="col-xs-2">

                                    <select name="client_id" class="form-control select_box" style="width: 100%">

                                        <option value=""><?= lang('client') ?></option>

                                        <?php if(!empty($all_clients)):

                                            foreach ($all_clients as $client): ?>

                                                <option value="<?php echo $client->client_id; ?>" <?php if(!empty($client_id)){

                                                    echo ($client->client_id == $client_id)?'selected':'';

                                                }?>><?php echo $client->name; ?></option>

                                            <?php endforeach; ?>

                                        <?php endif; ?>

                                    </select>

                                </div>

                                <div class="col-xs-2">

                                    <input type="text" class="form-control" name="reference_no" placeholder="<?= lang('reference_no') ?>" value="<?= (!empty($reference_no))?$reference_no:'' ?>" />

                                </div>

                                <div class="col-xs-2">

                                    <input type="text" class="form-control" name="commodity" placeholder="<?= lang('commodity') ?>" value="<?= (!empty($commodity))?$commodity:'' ?>" />

                                </div>

                                <div class="col-xs-2">

                                    <input type="text" class="form-control" name="hs_code" placeholder="<?= lang('hs_code') ?>" value="<?= (!empty($hs_code))?$hs_code:'' ?>" />

                                </div>

                            </div>

                            <div class="form-group">

                               <!-- <div class="col-xs-2">

                                    <select name="origin" class="form-control select_box" style="width: 100%">

                                        <option value=""><?/*= lang('origin') */?></option>

                                        <?php
/*
                                        if (!empty($all_countries)) {

                                            foreach ($all_countries as $v_country) {

                                                */?>

                                                <option value="<?/*= $v_country->id */?>"

                                                    <?php
/*
                                                    if (!empty($origin)) {

                                                        echo ($v_country->id == $origin)?"selected":'';

                                                    }

                                                    */?>

                                                ><?/*= $v_country->value */?> </option>

                                                <?php
/*
                                            }

                                        }

                                        */?>

                                    </select>

                                </div>

                                <div class="col-xs-2">

                                    <select name="port" class="form-control select_box" style="width: 100%">

                                        <option value=""><?/*= lang('port') */?></option>

                                        <option value="Burma Oil Mills" <?php /*if(!empty($port)){

                                            echo ($port) == 'Burma Oil Mills'?'selected':'';

                                        } */?>>Burma Oil Mills</option>

                                        <option value="Al Hamd International Container Terminal" <?php /*if(!empty($port)){

                                            echo ($port) == 'Al Hamd International Container Terminal'?'selected':'';

                                        } */?>>Al Hamd International Container Terminal</option>

                                        <option value="Pak Shaheen" <?php /*if(!empty($port)){

                                            echo ($port) == 'Pak Shaheen'?'selected':'';

                                        } */?>>Pak Shaheen</option>

                                        <option value="NLC" <?php /*if(!empty($port)){

                                            echo ($port) == 'NLC'?'selected':'';

                                        } */?>>NLC</option>

                                        <option value="QICT" <?php /*if(!empty($port)){

                                            echo ($port) == 'QICT'?'selected':'';

                                        } */?>>QICT</option>

                                        <option value="PICT" <?php /*if(!empty($port)){

                                            echo ($port) == 'PICT'?'selected':'';

                                        } */?>>PICT</option>

                                        <option value="KICT" <?php /*if(!empty($port)){

                                            echo ($port) == 'KICT'?'selected':'';

                                        } */?>>KICT</option>

                                        <option value="SAPT" <?php /*if(!empty($port)){

                                            echo ($port) == 'SAPT'?'selected':'';

                                        } */?>>SAPT</option>

                                        <option value="Royal" <?php /*if(!empty($port)){

                                            echo ($port) == 'Royal'?'selected':'';

                                        } */?>>Royal</option>

                                        <option value="Gerrys" <?php /*if(!empty($port)){

                                            echo ($port) == 'Gerrys'?'selected':'';

                                        } */?>>Gerrys</option>

                                        <option value="Gerrys DNATA" <?php /*if(!empty($port)){

                                            echo ($port) == 'Gerrys DNATA'?'selected':'';

                                        } */?>>Gerrys DNATA</option>

                                        <option value="Qattar" <?php /*if(!empty($port)){

                                            echo ($port) == 'Qattar'?'selected':'';

                                        } */?>>Qattar</option>

                                        <option value="PIA" <?php /*if(!empty($port)){

                                            echo ($port) == 'PIA'?'selected':'';

                                        } */?>>PIA</option>

                                        <option value="Emirates" <?php /*if(!empty($port)){

                                            echo ($port) == 'Emirates'?'selected':'';

                                        } */?>>Emirates</option>

                                        <option value="DHL" <?php /*if(!empty($port)){

                                            echo ($port) == 'DHL'?'selected':'';

                                        } */?>>DHL</option>

                                        <option value="BOML" <?php /*if(!empty($port)){

                                            echo ($port) == 'BOML'?'selected':'';

                                        } */?>>BOML</option>

                                        <option value="AICT" <?php /*if(!empty($port)){

                                            echo ($port) == 'AICT'?'selected':'';

                                        } */?>>AICT</option>

                                        <option value="Bay West" <?php /*if(!empty($port)){

                                            echo ($port) == 'Bay West'?'selected':'';

                                        } */?>>Bay West</option>

                                        <option value="PST" <?php /*if(!empty($port)){

                                            echo ($port) == 'PST'?'selected':'';

                                        } */?>>PST</option>

                                        <option value="KEPZ" <?php /*if(!empty($port)){

                                            echo ($port) == 'KEPZ'?'selected':'';

                                        } */?>>KEPZ</option>

                                        <option value="Shaheenin" <?php /*if(!empty($port)){

                                            echo ($port) == 'Shaheenin'?'selected':'';

                                        } */?>>Shaheenin</option>

                                        <option value="Inbond" <?php /*if(!empty($port)){

                                            echo ($port) == 'Inbond'?'selected':'';

                                        } */?>>Inbond</option>

                                        <option value="Exbond" <?php /*if(!empty($port)){

                                            echo ($port) == 'Exbond'?'selected':'';

                                        } */?>>Exbond</option>

                                    </select>

                                </div>-->

                                <div class="col-xs-2">

                                    <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> <?= lang('search') ?></button>

                                </div>

                            </div>

                        </div>

                    </section>

                </div>

                <?php echo form_close(); ?>



            <div class="row">
                <h1 class="header-<?= config_item('sidebar_theme'); ?>" ><?= lang('sales_report_list') ?></h1>
                <section class="panel panel-default">

                    <header class="panel-heading  "><?= $title; ?></header>

                    <div class="panel-body">

                    <?php if(isset($start_date) && isset($end_date)): ?>

                        <h4 class="text-center text-danger" style="font-family: inherit;"><?= date('d-m-Y',strtotime($start_date)) ?>&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;<?= date('d-m-Y',strtotime($end_date)); ?></h4>

                    <?php endif; ?>

                    <table class="table table-bordered table-responsive table-hover" id="table">

                        <thead>

                        <tr style="border-top: 1px solid #dddddd;">

                            <th>Dated</th>

                            <th><?= lang('reference_no') ?></th>

                            <th><?= lang('client') ?></th>

                            <th>Total Amount</th>

                            <th>Due Amount</th>

                            <th>Turnover Services</th>

                            <th>Cost of Sales</th>

                            <th>Taxes</th>

                            <th>Commissions</th>

                        </tr>

                        </thead>

                        <tbody>

                        <?php $total_bill=0; $total_account=0; ?>

                        <?php if(!empty($invoices)): ?>

                            <?php foreach ($invoices as $inv){

                                $bill = $this->report_model->check_by(array('invoices_id'=>$inv->invoices_id), 'tbl_bills');

                                if(!empty($bill)){

                                    $client_name = $this->report_model->get_any_field('tbl_client', array('client_id' => $inv->client_id), 'name');

                                    /*** Turnover Services ***/

                                    $services_sub_head = $this->report_model->get_any_field('sub_heads', array('HEAD_TYPE'=>'Income', 'NAME'=>'Sales Income'),'SUB_HEAD_ID');

                                    $services_head = $this->report_model->get_any_field('accounts_head', array('SUB_HEAD_ID'=>$services_sub_head, 'H_NAME'=>'Turnover Services'),'H_ID');

                                    $services_account_id = $this->report_model->get_any_field('accounts',array('H_ID'=>$services_head,'CLIENT_ID'=>$inv->client_id), 'A_ID');

                                    $turnover_services = $this->report_model->get_any_field('transactions_meta',array('T_ID'=>$bill->transaction_id, 'A_ID'=>$services_account_id, 'PAYMENT_ID'=>NULL), 'TM_AMOUNT');

                                    /*** Cost of Sales ***/

                                    $cost_sub_heads = $this->report_model->get_any_field('sub_heads', array('HEAD_TYPE'=>'Expense', 'NAME'=>'Cost of Sales'),'SUB_HEAD_ID');

                                    $cost_of_sales = $this->report_model->check_by_all(array('SUB_HEAD_ID'=>$cost_sub_heads,'CLIENT_ID'=>$inv->client_id),'accounts');

                                    $cost = 0;

                                    foreach($cost_of_sales as $c_o_s){

                                        $costs = $this->report_model->check_by(array('T_ID'=>$bill->transaction_id, 'A_ID'=>$c_o_s->A_ID, 'PAYMENT_ID'=>NULL), 'transactions_meta');

                                        $cost += $costs->TM_AMOUNT;

                                    }

                                    /*** Payable Taxes ***/

                                    $tax_head = $this->report_model->get_any_field('accounts_head', array('H_TYPE'=>'Liabilities', 'H_NAME'=>'Payable Tax'),'H_ID');

                                    $payable_taxes = $this->report_model->check_by_all(array('H_ID'=>$tax_head),'accounts');

                                    $tax = 0;

                                    foreach($payable_taxes as $p_tax){

                                        $taxs = $this->report_model->check_by(array('T_ID'=>$bill->transaction_id, 'A_ID'=>$p_tax->A_ID, 'PAYMENT_ID'=>NULL), 'transactions_meta');

                                        $tax += $taxs->TM_AMOUNT;

                                    }

                                    /*** Commissions ***/

                                    $commission_heads = $this->report_model->get_any_field('accounts_head', array('H_TYPE'=>'Income', 'H_NAME'=>'Commission'),'H_ID');

                                    $commissions = $this->report_model->check_by_all(array('CLIENT_ID'=>$inv->client_id, 'H_ID'=>$commission_heads),'accounts');

                                    $commission = 0;

                                    foreach($commissions as $p_commission){

                                        $comms = $this->report_model->check_by(array('T_ID'=>$bill->transaction_id, 'A_ID'=>$p_commission->A_ID, 'PAYMENT_ID'=>NULL), 'transactions_meta');

                                        $commission += $comms->TM_AMOUNT;

                                    }

                            ?>

                                <tr>

                                    <td><?= date('d-m-Y',strtotime($inv->created_date)); ?></td>

                                    <td><?= $inv->reference_no ?></td>

                                    <td><?= $client_name ?></td>

                                    <td>PKR <?= number_format($this->invoice_model->calculate_bill('bill_grand_total',$bill->bill_id), 2) ?></td>

                                    <td>PKR <?= number_format($this->invoice_model->calculate_to('invoice_due', $v_invoices->invoices_id), 2) ?></td>

                                    <td>PKR <?= number_format(($turnover_services),2) ?></td>

                                    <td>PKR <?= number_format(($cost),2) ?></td>

                                    <td>PKR <?= number_format(($tax),2) ?></td>

                                    <td>PKR <?= number_format(($commission),2) ?></td>

                                </tr>

                            <?php }} ?>

                        <?php endif; ?>

                        </tbody>

                    </table>

                    </div>

                </section>

            </div>

        </div>

        <!-- /highlighting rows and columns -->

        <?php

            if(!empty($invoices)) {

                $this->load->view('admin/components/buttons_datatable.php');

            }

        ?>