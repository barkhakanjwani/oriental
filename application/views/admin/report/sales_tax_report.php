<?= message_box('success') ?>
<!--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

<?php echo form_open(base_url('admin/report/sales_tax_report'),array('class'=>"form-horizontal")); ?>
<div class="row">
<div class="col-lg-12">
    <section class="panel panel-default">
        <header class="panel-heading  "><?= lang('search_by'); ?></header>
        <div class="panel-body">
            <div class="form-group">
                <div class="col-xs-2">
                    <input type="text" class="date-own form-control" name="date" placeholder="YYYY-MM" autocomplete="off"/>
                </div>
                <div class="col-xs-2">
                    <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> <?= lang('search') ?></button>
                </div>
            </div>
        </div>
    </section>
</div>
</div>
<?php echo form_close(); ?>
<h1 class="header-<?= config_item('sidebar_theme'); ?>">All Sales Tax Report</h1>

<input type="hidden" id="date" value="<?= (!empty($date))?$date:'All' ?>">
<section class="panel panel-default">
    <header class="panel-heading"><?= $title; ?></header>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered data-table-list1">
                        <thead>
                            <th>S/Tax Invoice No</th>
                            <th>Client Name</th>
                            <th>Bill No</th>
                            <th>Reference No / Job No</th>
                            <th>Reference No L/C</th>
                            <th><?= lang('invoice_value') ?></th>
                            <th>I/E Value</th>
                            <th><?= lang('agency_commission') ?></th>
                            <th><?= lang('sales_tax') ?> Amount</th>
                        </thead>
                        <tfoot align="right">
                            <tr>
                                <th colspan="8">Total <?= lang('sales_tax') ?> Amount</th>
                                <th id="total_sales"></th>
                            </tr>
                        </tfoot>
                    </table>
                    <button class="btn btn-sm pull-left" style="color: #fff;background-color: #26a69a" id="btnExport">Export to Excel</button>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var date=$('#date').val();
    var data_url='<?= base_url('admin/report/all_sales_tax_report/') ?>'+date;
</script>
<script type="text/javascript">
    $('.date-own').datepicker({
        minViewMode: 1,
        format: 'yyyy-mm'
    });
</script>

<!-- Export to Excel-->
<div id="export_table" style="display: none;">
    <table style="border: 1px solid;">
        <thead style="border: 1px solid;">
        <tr style="border: 1px solid black;">
            <th style="border: 1px solid black;">S/Tax Invoice No</th>
            <th style="border: 1px solid black;">Client Name</th>
            <th style="border: 1px solid black;">Bill No</th>
            <th style="border: 1px solid black;">Reference No / Job No</th>
            <th style="border: 1px solid black;">Reference No L/C</th>
            <th style="border: 1px solid black;"><?= lang('invoice_value') ?></th>
            <th style="border: 1px solid black;">I/E Value</th>
            <th style="border: 1px solid black;"><?= lang('agency_commission') ?></th>
            <th style="border: 1px solid black;"><?= lang('sales_tax') ?> Amount</th>
        </tr>
        </thead>
        <tbody style="border: 1px solid;">
        <?php if(!empty($all_sales)): ?>
            <?php
            $total_sales_tax = 0;
            foreach ($all_sales as $sales){
                $invoice_info = $this->invoice_model->getInvoiceAndLC($sales->invoices_id);
                $client_info = $this->invoice_model->check_by(array('client_id' => $invoice_info->client_id), 'tbl_client');
                $job_no = $this->invoice_model->job_no_creation($sales->invoices_id);
                $agency_commission = $sales->service_charges;
                $tax_per = $agency_commission / 100 * $sales->gst;
                $total_sales_tax += $tax_per;
                ?>
                <tr style="border: 1px solid black;">
                    <td style="border: 1px solid black;"><?= $sales->bill_id ?></td>
                    <td style="border: 1px solid black;"><?= $client_info->name ?></td>
                    <td style="border: 1px solid black;"><?= $sales->bill_no ?></td>
                    <td style="border: 1px solid black;"><?= $job_no ?></td>
                    <td style="border: 1px solid black;"><?= $invoice_info->lc_no ?></td>
                    <td style="border: 1px solid black;"><?= $invoice_info->currency." ".number_format($this->invoice_model->get_invoice_value($invoice_info->invoices_id),2) ?></td>
                    <td style="border: 1px solid black;"><?= number_format(($this->invoice_model->get_invoice_value($invoice_info->invoices_id)*$invoice_info->exchange_rate),2) ?></td>
                    <td style="border: 1px solid black;"><?= number_format($agency_commission,2) ?></td>
                    <td style="border: 1px solid black;"><?= number_format($tax_per,2) ?></td>
                </tr>
            <?php } ?>
        <?php endif; ?>
        </tbody>
        <tfoot align="right">
        <tr>
            <th colspan="8">Total <?= lang('sales_tax') ?> Amount</th>
            <th style="border: 1px solid black;"><?= $total_sales_tax ?></th>
        </tr>
        </tfoot>
    </table>
</div>
<script>
    $("#btnExport").click(function (e) {
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('#export_table').html() ));
        e.preventDefault();
    });
</script>
<!-- /Export to Excel-->
