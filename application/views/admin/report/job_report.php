<style>
    .liActive{
        background: #337ab7!important;
        color: #fff!important;
        border: 1px solid #fff;
    }
    .table .liActive{
        background: none!important;
        color: #666!important;
    }
    .dropbtn {
        background-color: #00587D;
        color: white;
        padding: 7px;
        font-size: 13px;
        border: none;
        cursor: pointer;
        margin: 0;
        border-radius: 7px;
    }

    .dropbtn:hover, .dropbtn:focus {
        background-color: #00587D;
    }

    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f6f6f6;
        min-width: 120px;
        overflow: auto;
        border: 1px solid #ddd;
        z-index: 1;
        top: 32px;
    }

    .dropdown-content a {
        color: black;
        padding: 6px;
        text-decoration: none;
        display: block;
    }

    .dropdown a:hover {background-color: #ddd;}

    .show {display: block;}

</style>
<?php error_reporting(0) ?>
<!-- style form css -->
<!-- /style form css -->
    <!-- Content area -->
<h1 class="header-<?= config_item('sidebar_theme'); ?>" ><?= lang('search_by') ?> <?= lang('job_report') ?></h1>
    <div class="content">
        <!-- Highlighting rows and columns -->
                <?php echo form_open(base_url('admin/report/job_report'),array('class'=>"form-horizontal")); ?>
                <div class="row">
                    <section class="panel panel-default">
                        <header class="panel-heading  "><?= lang('search_by'); ?></header>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-xs-2">
                                    <input type="text" name="start_date" class="form-control datepicker" placeholder="From Date" autocomplete="off" value="<?= (!empty($start_date))?$start_date:'' ?>" required>
                                </div>
                                <div class="col-xs-2">
                                    <input type="text" name="end_date" class="form-control datepicker" placeholder="To Date" value="<?= (!empty($end_date))?$end_date:'' ?>" autocomplete="off" required>
                                </div>
                                <div class="col-xs-2">
                                    <select name="client_id" class="form-control select_box" style="width: 100%">
                                        <option value=""><?= lang('client') ?></option>
                                        <?php if(!empty($all_clients)):
                                            foreach ($all_clients as $client): ?>
                                                <option value="<?php echo $client->client_id; ?>" <?php if(!empty($client_id)){
                                                    echo ($client->client_id == $client_id)?'selected':'';
                                                }?>><?php echo $client->name; ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" name="reference_no" placeholder="<?= lang('reference_no') ?>" value="<?= (!empty($reference_no))?$reference_no:'' ?>" />
                                </div>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" name="commodity" placeholder="<?= lang('commodity') ?>" value="<?= (!empty($commodity))?$commodity:'' ?>" />
                                </div>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" name="hs_code" placeholder="<?= lang('hs_code') ?>" value="<?= (!empty($hs_code))?$hs_code:'' ?>" />
                                </div>
                            </div>
                            <div class="form-group">
                             <!--   <div class="col-xs-2">
                                    <select name="origin" class="form-control select_box" style="width: 100%">
                                        <option value=""><?/*= lang('origin') */?></option>
                                        <?php
/*                                        if (!empty($all_countries)) {
                                            foreach ($all_countries as $v_country) {
                                                */?>
                                                <option value="<?/*= $v_country->id */?>"
                                                    <?php
/*                                                    if (!empty($origin)) {
                                                        echo ($v_country->id == $origin)?"selected":'';
                                                    }
                                                    */?>
                                                ><?/*= $v_country->value */?> </option>
                                                <?php
/*                                            }
                                        }
                                        */?>
                                    </select>
                                </div>
                                <div class="col-xs-2">
                                    <select name="port" class="form-control select_box" style="width: 100%">
                                        <option value=""><?/*= lang('port') */?></option>
                                        <option value="Burma Oil Mills" <?php /*if(!empty($port)){
                                            echo ($port) == 'Burma Oil Mills'?'selected':'';
                                        } */?>>Burma Oil Mills</option>
                                        <option value="Al Hamd International Container Terminal" <?php /*if(!empty($port)){
                                            echo ($port) == 'Al Hamd International Container Terminal'?'selected':'';
                                        } */?>>Al Hamd International Container Terminal</option>
                                        <option value="Pak Shaheen" <?php /*if(!empty($port)){
                                            echo ($port) == 'Pak Shaheen'?'selected':'';
                                        } */?>>Pak Shaheen</option>
                                        <option value="NLC" <?php /*if(!empty($port)){
                                            echo ($port) == 'NLC'?'selected':'';
                                        } */?>>NLC</option>
                                        <option value="QICT" <?php /*if(!empty($port)){
                                            echo ($port) == 'QICT'?'selected':'';
                                        } */?>>QICT</option>
                                        <option value="PICT" <?php /*if(!empty($port)){
                                            echo ($port) == 'PICT'?'selected':'';
                                        } */?>>PICT</option>
                                        <option value="KICT" <?php /*if(!empty($port)){
                                            echo ($port) == 'KICT'?'selected':'';
                                        } */?>>KICT</option>
                                        <option value="SAPT" <?php /*if(!empty($port)){
                                            echo ($port) == 'SAPT'?'selected':'';
                                        } */?>>SAPT</option>
                                        <option value="Royal" <?php /*if(!empty($port)){
                                            echo ($port) == 'Royal'?'selected':'';
                                        } */?>>Royal</option>
                                        <option value="Gerrys" <?php /*if(!empty($port)){
                                            echo ($port) == 'Gerrys'?'selected':'';
                                        } */?>>Gerrys</option>
                                        <option value="Gerrys DNATA" <?php /*if(!empty($port)){
                                            echo ($port) == 'Gerrys DNATA'?'selected':'';
                                        } */?>>Gerrys DNATA</option>
                                        <option value="Qattar" <?php /*if(!empty($port)){
                                            echo ($port) == 'Qattar'?'selected':'';
                                        } */?>>Qattar</option>
                                        <option value="PIA" <?php /*if(!empty($port)){
                                            echo ($port) == 'PIA'?'selected':'';
                                        } */?>>PIA</option>
                                        <option value="Emirates" <?php /*if(!empty($port)){
                                            echo ($port) == 'Emirates'?'selected':'';
                                        } */?>>Emirates</option>
                                        <option value="DHL" <?php /*if(!empty($port)){
                                            echo ($port) == 'DHL'?'selected':'';
                                        } */?>>DHL</option>
                                        <option value="BOML" <?php /*if(!empty($port)){
                                            echo ($port) == 'BOML'?'selected':'';
                                        } */?>>BOML</option>
                                        <option value="AICT" <?php /*if(!empty($port)){
                                            echo ($port) == 'AICT'?'selected':'';
                                        } */?>>AICT</option>
                                        <option value="Bay West" <?php /*if(!empty($port)){
                                            echo ($port) == 'Bay West'?'selected':'';
                                        } */?>>Bay West</option>
                                        <option value="PST" <?php /*if(!empty($port)){
                                            echo ($port) == 'PST'?'selected':'';
                                        } */?>>PST</option>
                                        <option value="KEPZ" <?php /*if(!empty($port)){
                                            echo ($port) == 'KEPZ'?'selected':'';
                                        } */?>>KEPZ</option>
                                        <option value="Shaheenin" <?php /*if(!empty($port)){
                                            echo ($port) == 'Shaheenin'?'selected':'';
                                        } */?>>Shaheenin</option>
                                        <option value="Inbond" <?php /*if(!empty($port)){
                                            echo ($port) == 'Inbond'?'selected':'';
                                        } */?>>Inbond</option>
                                        <option value="Exbond" <?php /*if(!empty($port)){
                                            echo ($port) == 'Exbond'?'selected':'';
                                        } */?>>Exbond</option>
                                    </select>
                                </div>-->
                                <div class="col-xs-2">
                                    <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> <?= lang('search') ?></button>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <?php echo form_close(); ?>
            <div class="row">
                <h1 class="header-<?= config_item('sidebar_theme'); ?>" ><?= lang('job_report_list') ?></h1>
                <section class="panel panel-default">
                    <header class="panel-heading  "><?= $title; ?></header>
                    <div class="panel-body" style="overflow: auto; min-height: 100vh;">
                        <div class="row">
                            <div class="col-lg-2" style="padding-left: 0px">
                                <div class="dropdown">
                                    <button onclick="myFunction()" class="dropbtn">Columns</button>
                                    <div id="myDropdown" class="dropdown-content">
                                        <a class="toggle-vis val liActive" value="0" data-column="0"><?= lang('created_date') ?></a>
                                        <a class="toggle-vis val liActive" value="1" data-column="1"><?= lang('reference_no') ?></a>
                                        <a class="toggle-vis val liActive" value="2" data-column="2"><?= lang('choose_type') ?></a>
                                     <!--   <a class="toggle-vis val liActive" value="3" data-column="3"><?/*= lang('vessel') */?></a>-->
                                        <a class="toggle-vis val liActive" value="4" data-column="4"><?= lang('client') ?></a>
                                        <a class="toggle-vis val liActive" value="5" data-column="5"><?= lang('consignors') ?></a>
                                        <a class="toggle-vis val liActive" value="6" data-column="6"><?= lang('suppliers') ?></a>
                                        <a class="toggle-vis val liActive" value="7" data-column="7"><?= lang('incoterm') ?></a>
                                        <a class="toggle-vis val liActive" value="8" data-column="8"><?= lang('index') ?></a>
                                        <a class="toggle-vis val liActive" value="9" data-column="9"><?= lang('igm') ?></a>
                                        <a class="toggle-vis val liActive" value="10" data-column="10"><?= lang('igm_date') ?></a>
                                        <a class="toggle-vis val liActive" value="11" data-column="11"><?= lang('gross_weight') ?></a>
                                        <a class="toggle-vis val liActive" value="12" data-column="12"><?= lang('net_weight') ?></a>
                                        <a class="toggle-vis val liActive" value="13" data-column="13"><?= lang('mode') ?></a>
                                        <a class="toggle-vis val liActive" value="14" data-column="14"><?= lang('gd_type') ?></a>
                                        <!--<a class="toggle-vis val liActive" value="15" data-column="15"><?/*= lang('origin') */?></a>-->
                                        <a class="toggle-vis val liActive" value="16" data-column="16"><?= lang('port_of_loading') ?></a>
                                        <a class="toggle-vis val liActive" value="17" data-column="17"><?= lang('port_of_discharge') ?></a>
                                        <a class="toggle-vis val liActive" value="18" data-column="18"><?= lang('port') ?></a>
                                        <a class="toggle-vis val liActive" value="19" data-column="19"><?= lang('invoice_value') ?></a>
                                        <a class="toggle-vis val liActive" value="20" data-column="20"><?= lang('lc_no') ?></a>
                                        <a class="toggle-vis val liActive" value="21" data-column="21"><?= lang('lc_date') ?></a>
                                        <a class="toggle-vis val liActive" value="22" data-column="22"><?= lang('bl_no') ?></a>
                                        <a class="toggle-vis val liActive" value="23" data-column="23"><?= lang('bl_date') ?></a>
                                        <a class="toggle-vis val liActive" value="24" data-column="24"><?= lang('shipping_line') ?></a>
                                        <a class="toggle-vis val liActive" value="25" data-column="25"><?= lang('forwarding_agent') ?></a>
                                        <a class="toggle-vis val liActive" value="26" data-column="26"><?= lang('eif_number') ?></a>
                                        <!--<a class="toggle-vis val liActive" value="27" data-column="27"><?/*= lang('insurance_covernote') */?></a>
                                        <a class="toggle-vis val liActive" value="28" data-column="28"><?/*= lang('gd_number_machine_number') */?></a>
                                        <a class="toggle-vis val liActive" value="29" data-column="29"><?/*= lang('city') */?></a>
                                        <a class="toggle-vis val liActive" value="30" data-column="30"><?/*= lang('yard') */?></a>-->
                                        <a class="toggle-vis val liActive" value="31" data-column="31">ETA</a>
                                        <a class="toggle-vis val liActive" value="32" data-column="32"><?= lang('consignment_type') ?></a>
                                        <a class="toggle-vis val liActive" value="33" data-column="33"><?= lang('gst') ?></a>
                                        <a class="toggle-vis val liActive" value="34" data-column="34"><?= lang('total_amount') ?></a>
                                        <a class="toggle-vis val liActive" value="35" data-column="35"><?= lang('total_expense') ?></a>
                                        <a class="toggle-vis val liActive" value="36" data-column="36"><?= lang('amount_received') ?></a>
                                        <a class="toggle-vis val liActive" value="37" data-column="37"><?= lang('tax') ?></a>
                                        <a class="toggle-vis val liActive" value="38" data-column="38"><?= lang('agency_commission') ?></a>
                                        <a class="toggle-vis val liActive" value="39" data-column="39"><?= lang('status') ?></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-10">
                                <?php if(isset($start_date) && isset($end_date)): ?>
                                    <h4 class="text-center text-danger" style="font-family: inherit;"><?= date('d-m-Y',strtotime($start_date)) ?>&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;<?= date('d-m-Y',strtotime($end_date)); ?></h4>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-bordered table-hover" id="example" width="100%" style="white-space: nowrap">
                        <thead>
                        <tr style="border-top: 1px solid #dddddd;">
                            <th><a class="toggle-vis val liActive" value="0" data-column="0"><?= lang('created_date') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="1" data-column="1"><?= lang('reference_no') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="2" data-column="2"><?= lang('choose_type') ?></a></th>
                           <!-- <th><a class="toggle-vis val liActive" value="3" data-column="3"><?/*= lang('vessel') */?></a></th>-->
                            <th><a class="toggle-vis val liActive" value="4" data-column="4"><?= lang('client') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="5" data-column="5"><?= lang('consignors') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="6" data-column="6"><?= lang('suppliers') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="7" data-column="7"><?= lang('incoterm') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="8" data-column="8"><?= lang('index') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="9" data-column="9"><?= lang('igm') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="10" data-column="10"><?= lang('igm_date') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="11" data-column="11"><?= lang('gross_weight') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="12" data-column="12"><?= lang('net_weight') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="13" data-column="13"><?= lang('mode') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="14" data-column="14"><?= lang('gd_type') ?></a></th>
                            <!--<th><a class="toggle-vis val liActive" value="15" data-column="15"><?/*= lang('origin') */?></a></th>-->
                            <th><a class="toggle-vis val liActive" value="16" data-column="16"><?= lang('port_of_loading') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="17" data-column="17"><?= lang('port_of_discharge') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="18" data-column="18"><?= lang('port') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="19" data-column="19"><?= lang('invoice_value') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="20" data-column="20"><?= lang('lc_no') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="21" data-column="21"><?= lang('lc_date') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="22" data-column="22"><?= lang('bl_no') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="23" data-column="23"><?= lang('bl_date') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="24" data-column="24"><?= lang('shipping_line') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="25" data-column="25"><?= lang('forwarding_agent') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="26" data-column="26"><?= lang('eif_number') ?></a></th>
                            <!--<th><a class="toggle-vis val liActive" value="27" data-column="27"><?/*= lang('insurance_covernote') */?></a></th>
                            <th><a class="toggle-vis val liActive" value="28" data-column="28"><?/*= lang('gd_number_machine_number') */?></a></th>
                            <th><a class="toggle-vis val liActive" value="29" data-column="29"><?/*= lang('city') */?></a></th>
                            <th><a class="toggle-vis val liActive" value="30" data-column="30"><?/*= lang('yard') */?></a></th>-->
                            <th><a class="toggle-vis val liActive" value="31" data-column="31">ETA</a></th>
                            <th><a class="toggle-vis val liActive" value="32" data-column="32"><?= lang('consignment_type') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="33" data-column="33"><?= lang('gst') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="34" data-column="34"><?= lang('total_amount') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="35" data-column="35"><?= lang('total_expense') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="36" data-column="36"><?= lang('amount_received') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="37" data-column="37"><?= lang('tax') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="38" data-column="38"><?= lang('agency_commission') ?></a></th>
                            <th><a class="toggle-vis val liActive" value="39" data-column="39"><?= lang('status') ?></a></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $total_bill=0; $total_account=0; ?>
                        <?php if(!empty($invoices)): ?>
                                <?php foreach ($invoices as $inv){
                                $bill = $this->report_model->check_by(array('invoices_id'=>$inv->invoices_id), 'tbl_bills');
                                if(!empty($bill)){
                                    $client_name = $this->report_model->get_any_field('tbl_client', array('client_id' => $inv->client_id), 'name');
                                    $consignor_name = $this->report_model->get_any_field('tbl_consignors', array('consignor_id' => $inv->consignor_id), 'name');
                                    $supplier_name = $this->report_model->get_any_field('tbl_supplier', array('supplier_id' => $inv->supplier_id), 'supplier_name');
                                    $unit = $this->report_model->get_any_field('tbl_weight_units', array('weight_unit_id' => $inv->unit), 'weight_unit');
                                    $incoterm = $this->report_model->get_any_field('tbl_incoterms', array('incoterm_id' => $inv->incoterm), 'incoterm');
                                    $gd_type = $this->report_model->get_any_field('tbl_gd_types', array('gd_type_id' => $inv->mode_options), 'gd_type');
                                    $origin = $this->report_model->get_any_field('tbl_countries', array('id' => $inv->origin), 'value');
                                    $shipping_line = $this->report_model->get_any_field('tbl_shipping_line', array('shipping_id' => $inv->shipping_line), 'shipping_name');
                                    $yard = $this->report_model->get_any_field('tbl_yards', array('yard_id' => $inv->yard), 'yard');
                                    $status = $this->report_model->get_time_different(strtotime($inv->due_date), strtotime(date('Y-m-d')));
                                    $job_activity = $this->db->select('job_activity_status')
                                        ->from('tbl_job_activity')
                                        ->where('invoices_id',$inv->invoices_id)
                                        ->order_by('job_activity_id','DESC')
                                        ->get()->row()->job_activity_status;
                                    /*** Payable Taxes ***/
                                    $tax_head = $this->report_model->get_any_field('accounts_head', array('H_TYPE'=>'Liabilities', 'H_NAME'=>'Payable Tax'),'H_ID');
                                    $payable_taxes = $this->report_model->check_by_all(array('CLIENT_ID'=>$inv->client_id, 'H_ID'=>$tax_head),'accounts');
                                    $tax = 0;
                                    foreach($payable_taxes as $p_tax){
                                        $taxs = $this->report_model->check_by(array('T_ID'=>$bill->transaction_id, 'A_ID'=>$p_tax->A_ID, 'PAYMENT_ID'=>NULL), 'transactions_meta');
                                        $tax += $taxs->TM_AMOUNT;
                                    }
                                    /*** Commissions ***/
                                    $commission_heads = $this->report_model->get_any_field('accounts_head', array('H_TYPE'=>'Income', 'H_NAME'=>'Commission'),'H_ID');
                                    $commissions = $this->report_model->check_by_all(array('CLIENT_ID'=>$inv->client_id, 'H_ID'=>$commission_heads),'accounts');
                                    $commission = 0;
                                    foreach($commissions as $p_commission){
                                        $comms = $this->report_model->check_by(array('T_ID'=>$bill->transaction_id, 'A_ID'=>$p_commission->A_ID, 'PAYMENT_ID'=>NULL), 'transactions_meta');
                                        $commission += $comms->TM_AMOUNT;
                                    }
                                    $amount_received = ($this->invoice_model->calculate_to('paid_amount', $inv->invoices_id) + $this->invoice_model->pre_job_advances($inv->invoices_id)) - ($this->invoice_model->security_deposit_amount($inv->invoices_id));
                            ?>
                                <tr>
                                    <td><?= date('d-m-Y',strtotime($inv->created_date)); ?></td>
                                    <td><?= $inv->reference_no ?></td>
                                    <td><?= $inv->type ?></td>
                                   <!-- <td><?/*= $inv->vessel */?></td>-->
                                    <td><?= $client_name ?></td>
                                    <td><?= $consignor_name ?></td>
                                    <td><?= $supplier_name ?></td>
                                    <td><?= $incoterm ?></td>
                                    <td><?= $inv->index_no ?></td>
                                    <td><?= $inv->igm_no ?></td>
                                    <td><?= date('d-m-Y',strtotime($inv->igm_date)); ?></td>
                                    <td><?= $inv->gross_weight ?> <?= $unit ?></td>
                                    <td><?= $inv->net_weight ?> <?= $unit ?></td>
                                    <td><?= $inv->mode ?></td>
                                    <td><?= $gd_type ?></td>
                                    <!--<td><?/*= $origin */?></td>-->
                                    <td><?= $inv->p_o_l ?></td>
                                    <td><?= $inv->p_o_d ?></td>
                                    <td><?= $inv->port ?></td>
                                    <td><?= $inv->currency ?> <?= number_format($this->invoice_model->get_invoice_value($inv->invoices_id),2) ?></td>
                                    <td><?= $inv->lc_no ?></td>
                                    <td><?= date('d-m-Y',strtotime($inv->lc_date)); ?></td>
                                    <td><?= date('d-m-Y',strtotime($inv->bl_date)); ?></td>
                                    <td><?= $inv->bl_no ?></td>
                                    <td><?= $shipping_line ?></td>
                                    <td><?= $inv->forwarding_agent ?></td>
                                    <td><?= $inv->eif_no ?></td>
                                   <!-- <td><?/*= $inv->insurance_covernote */?></td>
                                    <td><?/*= $inv->gd_machine */?></td>
                                    <td><?/*= $inv->city */?></td>
                                    <td><?/*= $yard */?></td>-->
                                    <td><?= $inv->eta ?></td>
                                    <td><?= $inv->consignment_type ?></td>
                                    <td><?= $bill->gst ?></td>
                                    <td>PKR <?= number_format($this->invoice_model->calculate_bill('bill_grand_total',$bill->bill_id), 2) ?></td>
                                    <td>PKR <?= number_format($this->invoice_model->job_expenses($inv->invoices_id),2) ?></td>
                                    <td>PKR <?= number_format(abs($amount_received), 2) ?></td>
                                    <td>PKR <?= number_format(($tax),2) ?></td>
                                    <td>PKR <?= number_format(($commission),2) ?></td>
                                    <td class="text-center"><?= (!empty($job_activity))?job_activity($job_activity):'-' ?></td>
                                </tr>
                            <?php }} ?>
                        <?php endif; ?>
                        </tbody>
                    </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /highlighting rows and columns -->
        <?php
          if(!empty($invoices)) {
                $this->load->view('admin/components/buttons_datatable.php');
            }
        ?>
<script>
    $(document).ready(function() {
        var table = $('#example').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            bSort: false,
            buttons: [{name: 'excel',
                title: 'Job Report',
                extend: 'excel',
                filename: 'Job Report',
                orientation: 'landscape',
                exportOptions: {columns: ':visible', orthogonal: 'export'}}]

        });
        $('.val').on('click',function (e) {
            e.preventDefault();
            // Get the column API object
            var id = this.value;
            var column = table.column($(this).attr('data-column') );
            // Toggle the visibility
            var className = $(this).attr('class');
            if(className == "toggle-vis val"){
                $(this).addClass('liActive');
            }else{
                $(this).removeClass('liActive');
            }
            column.visible( ! column.visible() );
        });
    });
    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
    }
</script>
