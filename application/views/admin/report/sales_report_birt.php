<!-- Content area -->
    <div class="content">
        <!-- Highlighting rows and columns -->
                <?php echo form_open(base_url('admin/report/sales_report'),array('class'=>"form-horizontal")); ?>
                <div class="row">
                    <section class="panel panel-default">
                        <header class="panel-heading  "><?= lang('search_by'); ?></header>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-xs-2">
                                    <input type="text" name="start_date" class="form-control datepicker" placeholder="From Date" autocomplete="off" value="<?= (!empty($start_date))?$start_date:'' ?>" required>
                                </div>
                                <div class="col-xs-2">
                                    <input type="text" name="end_date" class="form-control datepicker" placeholder="To Date" value="<?= (!empty($end_date))?$end_date:'' ?>" autocomplete="off" required>
                                </div>
                                <div class="col-xs-2">
                                    <select name="client_id" class="form-control select_box" style="width: 100%">
                                        <option value=""><?= lang('client') ?></option>
                                        <?php if(!empty($all_clients)):
                                            foreach ($all_clients as $client): ?>
                                                <option value="<?php echo $client->client_id; ?>" <?php if(!empty($client_id)){
                                                    echo ($client->client_id == $client_id)?'selected':'';
                                                }?>><?php echo $client->name; ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" name="reference_no" placeholder="<?= lang('reference_no') ?>" value="<?= (!empty($reference_no))?$reference_no:'' ?>" />
                                </div>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" name="commodity" placeholder="<?= lang('commodity') ?>" value="<?= (!empty($commodity))?$commodity:'' ?>" />
                                </div>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" name="hs_code" placeholder="<?= lang('hs_code') ?>" value="<?= (!empty($hs_code))?$hs_code:'' ?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-2">
                                    <select name="origin" class="form-control select_box" style="width: 100%">
                                        <option value=""><?= lang('origin') ?></option>
                                        <?php
                                        if (!empty($all_countries)) {
                                            foreach ($all_countries as $country) {
                                                ?>
                                                <option value="<?= $country->id ?>"
                                                    <?php
                                                    if (!empty($origin)) {
                                                        echo ($country->id == $origin)?"selected":'';
                                                    }
                                                    ?>
                                                ><?= $country->value ?> </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-xs-2">
                                    <select name="port" class="form-control select_box" style="width: 100%">
                                        <option value=""><?= lang('port') ?></option>
                                        <option value="Burma Oil Mills" <?php if(!empty($port)){
                                            echo ($port) == 'Burma Oil Mills'?'selected':'';
                                        } ?>>Burma Oil Mills</option>
                                        <option value="Al Hamd International Container Terminal" <?php if(!empty($port)){
                                            echo ($port) == 'Al Hamd International Container Terminal'?'selected':'';
                                        } ?>>Al Hamd International Container Terminal</option>
                                        <option value="Pak Shaheen" <?php if(!empty($port)){
                                            echo ($port) == 'Pak Shaheen'?'selected':'';
                                        } ?>>Pak Shaheen</option>
                                        <option value="NLC" <?php if(!empty($port)){
                                            echo ($port) == 'NLC'?'selected':'';
                                        } ?>>NLC</option>
                                        <option value="QICT" <?php if(!empty($port)){
                                            echo ($port) == 'QICT'?'selected':'';
                                        } ?>>QICT</option>
                                        <option value="PICT" <?php if(!empty($port)){
                                            echo ($port) == 'PICT'?'selected':'';
                                        } ?>>PICT</option>
                                        <option value="KICT" <?php if(!empty($port)){
                                            echo ($port) == 'KICT'?'selected':'';
                                        } ?>>KICT</option>
                                        <option value="SAPT" <?php if(!empty($port)){
                                            echo ($port) == 'SAPT'?'selected':'';
                                        } ?>>SAPT</option>
                                    </select>
                                </div>
                                <div class="col-xs-2">
                                    <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> <?= lang('search') ?></button>
                                </div>
                            </div>
                        </div>
                    </section>
                        <?php if ($_POST) : ?>
                            <iframe src="<?php echo $report_url ?>" style="border: 0" height="900px" width="100%"></iframe>
                        <?php endif; ?>
                </div>
                <?php echo form_close(); ?>

        <?php
/*        if(!empty($invoices)) {
            echo form_open(base_url('admin/report/sales_report_excel'), array('class' => "form-horizontal")); */?><!--
            <input type="hidden" name="start_date_excel" value="<?/*= $start_date */?>"/>
            <input type="hidden" name="end_date_excel" value="<?/*= $end_date */?>"/>
            <button class="btn btn-sm btn-success pull-right">Export to Excel</button>
            --><?php
/*        }
        echo form_close();
        */?>
        </div>
        <!-- /highlighting rows and columns -->
        <?php
            /*if(!empty($invoices)) {
                $this->load->view('admin/components/buttons_datatable.php');
            }*/
        ?>