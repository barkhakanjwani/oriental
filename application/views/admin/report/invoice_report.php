<!-- Content area -->
<?php error_reporting(0) ?>
<h1 class="header-<?= config_item('sidebar_theme'); ?>" ><?= lang('search_by') ?> <?= lang('invoice_report') ?></h1>
    <div class="content">

        <!-- Highlighting rows and columns -->

                <?php echo form_open(base_url('admin/report/invoice_report'),array('class'=>"form-horizontal")); ?>

                <div class="row">

                    <section class="panel panel-default">

                        <header class="panel-heading  "><?= lang('search_by'); ?></header>

                        <div class="panel-body">

                            <div class="form-group">

                                <div class="col-xs-2">

                                    <input type="text" name="start_date" class="form-control datepicker" placeholder="From Date" autocomplete="off" value="<?= (!empty($start_date))?$start_date:'' ?>" required>

                                </div>

                                <div class="col-xs-2">

                                    <input type="text" name="end_date" class="form-control datepicker" placeholder="To Date" value="<?= (!empty($end_date))?$end_date:'' ?>" autocomplete="off" required>

                                </div>

                                <div class="col-xs-2">

                                    <select name="client_id" class="form-control select_box" style="width: 100%">

                                        <option value=""><?= lang('client') ?></option>

                                        <?php if(!empty($all_clients)):

                                            foreach ($all_clients as $client): ?>

                                                <option value="<?php echo $client->client_id; ?>" <?php if(!empty($client_id)){

                                                    echo ($client->client_id == $client_id)?'selected':'';

                                                }?>><?php echo $client->name; ?></option>

                                            <?php endforeach; ?>

                                        <?php endif; ?>

                                    </select>

                                </div>

                                <div class="col-xs-2">

                                    <input type="text" class="form-control" name="reference_no" placeholder="<?= lang('reference_no') ?>" value="<?= (!empty($reference_no))?$reference_no:'' ?>" />

                                </div>

                                <div class="col-xs-2">

                                    <input type="text" class="form-control" name="commodity" placeholder="<?= lang('commodity') ?>" value="<?= (!empty($commodity))?$commodity:'' ?>" />

                                </div>

                                <div class="col-xs-2">

                                    <input type="text" class="form-control" name="hs_code" placeholder="<?= lang('hs_code') ?>" value="<?= (!empty($hs_code))?$hs_code:'' ?>" />

                                </div>

                            </div>

                            <div class="form-group">

                                <!--<div class="col-xs-2">

                                    <select name="origin" class="form-control select_box" style="width: 100%">

                                        <option value=""><?/*= lang('origin') */?></option>

                                        <?php
/*
                                        if (!empty($all_countries)) {

                                            foreach ($all_countries as $country) {

                                                */?>

                                                <option value="<?/*= $country->id */?>"

                                                    <?php
/*
                                                    if (!empty($origin)) {

                                                        echo ($country->id == $origin)?"selected":'';

                                                    }

                                                    */?>

                                                ><?/*= $country->value */?> </option>

                                                <?php
/*
                                            }

                                        }

                                        */?>

                                    </select>

                                </div>

                                <div class="col-xs-2">

                                    <select name="port" class="form-control select_box" style="width: 100%">

                                        <option value=""><?/*= lang('port') */?></option>

                                        <option value="Burma Oil Mills" <?php /*if(!empty($port)){

                                            echo ($port) == 'Burma Oil Mills'?'selected':'';

                                        } */?>>Burma Oil Mills</option>

                                        <option value="Al Hamd International Container Terminal" <?php /*if(!empty($port)){

                                            echo ($port) == 'Al Hamd International Container Terminal'?'selected':'';

                                        } */?>>Al Hamd International Container Terminal</option>

                                        <option value="Pak Shaheen" <?php /*if(!empty($port)){

                                            echo ($port) == 'Pak Shaheen'?'selected':'';

                                        } */?>>Pak Shaheen</option>

                                        <option value="NLC" <?php /*if(!empty($port)){

                                            echo ($port) == 'NLC'?'selected':'';

                                        } */?>>NLC</option>

                                        <option value="QICT" <?php /*if(!empty($port)){

                                            echo ($port) == 'QICT'?'selected':'';

                                        } */?>>QICT</option>

                                        <option value="PICT" <?php /*if(!empty($port)){

                                            echo ($port) == 'PICT'?'selected':'';

                                        } */?>>PICT</option>

                                        <option value="KICT" <?php /*if(!empty($port)){

                                            echo ($port) == 'KICT'?'selected':'';

                                        } */?>>KICT</option>

                                        <option value="SAPT" <?php /*if(!empty($port)){

                                            echo ($port) == 'SAPT'?'selected':'';

                                        } */?>>SAPT</option>

                                    </select>

                                </div>-->

                                <div class="col-xs-2">

                                    <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> <?= lang('search') ?></button>

                                </div>

                            </div>

                        </div>

                    </section>

                </div>

                <?php echo form_close(); ?>



            <div class="row">
<h1 class="header-<?= config_item('sidebar_theme'); ?>" ><?= lang('invoice_report_list') ?></h1>
                <section class="panel panel-default">

                    <header class="panel-heading  "><?= $title; ?></header>

                    <div class="panel-body">

                    <?php if(isset($start_date) && isset($end_date)): ?>

                        <h4 class="text-center text-danger" style="font-family: inherit;"><?= date('d-m-Y',strtotime($start_date)) ?>&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;<?= date('d-m-Y',strtotime($end_date)); ?></h4>
                    <?php endif; ?>

                    <table class="table table-bordered table-responsive table-hover" id="table">

                        <thead>

                        <tr style="border-top: 1px solid #dddddd;">

                            <th><?= lang('created_date') ?></th>

                            <th><?= lang('due_date') ?></th>

                            <th><?= lang('reference_no') ?></th>

                            <th><?= lang('client') ?></th>

                            <th><?= lang('total_amount') ?></th>

                            <th><?= lang('paid_amount') ?></th>

                            <th><?= lang('due_amount') ?></th>

                            <th class="text-center"><?= lang('invoice_status') ?></th>

                            <th class="text-center"><?= lang('days') ?></th>

                            <th class="text-center"><?= lang('period') ?></th>

                        </tr>

                        </thead>

                        <tbody>

                        <?php $total_bill=0; $total_account=0; ?>

                        <?php if(!empty($invoices)): ?>

                            <?php foreach ($invoices as $inv){

                                $bill = $this->report_model->check_by(array('invoices_id'=>$inv->invoices_id), 'tbl_bills');

                                if(!empty($bill)){

                                    $client_name = $this->report_model->get_any_field('tbl_client', array('client_id' => $inv->client_id), 'name');

                                    $status = $this->report_model->get_time_different(strtotime($inv->due_date), strtotime(date('Y-m-d')));

                                    $explode = explode(',',$status);

                                    if($this->invoice_model->get_payment_status($inv->invoices_id) == lang('fully_paid')){

                                        $invoice_status = lang('fully_paid');

                                        $label = "success";

                                        $days = "-";

                                        $period = "Closed";

                                        $p_label = "default";

                                    }

                                    elseif($this->invoice_model->get_payment_status($inv->invoices_id) == lang('partially_paid')){

                                        $invoice_status = lang('partially_paid');

                                        $label = "info";

                                        $days = $explode[0];

                                        $period = $explode[1];

                                        $p_label = $explode[2];

                                    }

                                    else{

                                        $invoice_status = lang('not_paid');

                                        $label = "danger";

                                        $days = $explode[0];

                                        $period = $explode[1];

                                        $p_label = $explode[2];

                                    }

                            ?>

                                <tr>

                                    <td><?= date('d-m-Y',strtotime($inv->created_date)); ?></td>

                                    <td><?= (!empty($inv->due_date))?date('d-m-Y',strtotime($inv->due_date)):'-' ?></td>

                                    <td><?= $inv->reference_no ?></td>

                                    <td><?= $client_name ?></td>

                                    <td>PKR <?= number_format($this->invoice_model->calculate_bill('bill_grand_total',$bill->bill_id), 2) ?></td>

                                    <td>PKR <?= number_format($this->invoice_model->calculate_to('paid_amount', $inv->invoices_id), 2) ?></td>

                                    <td>PKR <?= number_format($this->invoice_model->calculate_to('invoice_due', $inv->invoices_id), 2) ?></td>

                                    <td class="text-center"><label class="label label-<?= $label ?>"><?= $invoice_status ?></label></td>

                                    <td class="text-center"><?= (!empty($inv->due_date))?$days:'-' ?></td>

                                    <td class="text-center">

                                        <?php if(!empty($inv->due_date)){

                                            ?>

                                        <label class="label label-<?= $p_label ?>"><?= $period ?></label>

                                        <?php } ?>

                                    </td>

                                </tr>

                            <?php }} ?>

                        <?php endif; ?>

                        </tbody>

                    </table>

                    </div>

                </section>

            </div>

        </div>

        <!-- /highlighting rows and columns -->

        <?php

            if(!empty($invoices)) {

                $this->load->view('admin/components/buttons_datatable.php');

            }

        ?>