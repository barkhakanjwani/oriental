<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?= lang('duty') ?> (<?= $po_info->reference_no ?>)</title>
	<style>
		.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #5D6975;
  text-decoration: underline;
}

body {
  position: relative;
  width: 19cm;
  height: 29.7cm; 
  margin: 0 auto; 
  color: #001028;
  background: #FFFFFF; 
  font-family: "verdana", "sans-serif";
  font-size: 10px;
  text-transform: uppercase;
}

header {
  padding: 10px 0;
  margin-bottom: 10px;
}

#logo {
  text-align: center;
  margin-bottom: 10px;
}

#logo img {
  width: 90px;
}

h1 {
  border-top: 1px solid  #5D6975;
  border-bottom: 1px solid  #5D6975;
  color: #5D6975;
  font-size: 2.4em;
  line-height: 1.4em;
  font-weight: normal;
  text-align: center;
  margin: 0 0 20px 0;
  background: url(../../../uploads/dimension.png);
}

#project {
  float: left;
}

#project span {
  color: #5D6975;
  text-align: right;
  width: 52px;
  margin-right: 10px;
  display: inline-block;
  font-size: 0.9em;
}

#company {
  float: right;
  text-align: right;
}

#project div,
#company div {
  white-space: nowrap;        
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 10px;
}

table tr:nth-child(2n-1) td {
  background: #F5F5F5;
}

table th,
table td {
  text-align: center;
}

table th {
  padding: 5px;
  color: #333;
  border: 1px solid #333;
  white-space: nowrap;        
  font-weight: bold;
  text-transform: uppercase;
}

table .service,
table .desc {
  text-align: left;
}

table td {
  padding: 5px;
  text-align: right;
  border:1px solid #333;
}

table td.service,
table td.desc {
  vertical-align: top;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table td.grand {
  border-top: 1px solid #5D6975;;
}

#notices .notice {
  color: #5D6975;
  font-size: 1.5em;
}

footer {
  color: #5D6975;
  width: 100%;
  height: 50px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #C1CED9;
  padding: 8px 0;
  text-align: center;
}
	</style>
</head>
<body>
<?php
    $client_info = $this->invoice_model->check_by(array('client_id' => $po_info->client_id), 'tbl_client');
?>
<header class="clearfix">
    <div id="logo">
        <img src="<?= base_url() . config_item('invoice_logo') ?>">
    </div>
    <h1>Pay Order</h1>
    <div id="project">
        <div><span>To Messer's </span><b><?= ucfirst($client_info->name) ?></b></div>
        <div><span>Address </span><b><?= ucfirst($client_info->address) ?> </b></div>
        <div><span><?= lang('reference_no') ?> </span><b><?= $po_info->reference_no ?> </b></div>
        <div><span><?= lang('created_date') ?> </span><b> <?= strftime(config_item('date_format'), strtotime($po_info->created_date)) ?> </b></div>
    </div>
</header>
<main>
    <table>
        <tr style="background:#ccc;">
            <th colspan="2" style="text-align: left"><?= lang('po_detail') ?></th>
        </tr>
        <tr>
            <td style="text-align:left;"><b><?= lang('po_date') ?></b></td>
            <td><?= strftime(config_item('date_format'), strtotime($po_info->po_date)) ?></td> </tr>
        <tr>
            <td style="text-align:left;"><b><?= lang('title') ?></b></td>
            <td><?= $po_info->A_NAME ?></td>
        </tr>
        <tr>
            <td style="text-align:left;"><b><?= lang('amount') ?></b></td>
            <td>Rs. <?= number_format(($po_info->po_amount),2) ?></td>
        </tr>
        <?php
            $branch_info = $this->invoice_model->check_by(array('BR_ID' => $po_info->branch_id), 'branches');
            $bank_info = $this->invoice_model->check_by(array('B_ID' => $branch_info->B_ID), 'banks');
            ?>
            <tr>
                <td style="text-align:left;"><b>Bank</b></td>
                <td><?= $bank_info->B_NAME ?></td>
            </tr>
            <tr>
                <td style="text-align:left;"><b>Branch</b></td>
                <td><?= $branch_info->BR_NAME ?></td>
            </tr>
        <tr>
            <td style="text-align:left;"><b>Note</b></td>
            <td><?= $po_info->po_note ?></td>
        </tr>
    </table>
</main>
<footer>
    <?= config_item('company_address') ?>, <?= config_item('company_city') ?>-<?= config_item('company_zip_code') ?><br />
    PHONE: <?= config_item('company_phone') ?><br />
    EMAIL: <?= config_item('company_email') ?><br />
    WEB: <?= config_item('company_domain') ?><br />
</footer>
</body>
</html>