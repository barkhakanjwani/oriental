
<section class="content-header">
    <?php
    $client_info = $this->invoice_model->check_by(array('client_id' => $po_info->client_id), 'tbl_client');
    ?>
    <div class="row">
        <div class="col-sm-8">

        </div>
        <div class="col-sm-4 pull-right">
            <a href="<?= base_url() ?>admin/po/manage_po/pdf_po/<?= $po_info->po_id ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="PDF" class="btn btn-sm btn-warning pull-right" >
                <i class="fa fa-file-pdf-o"></i>
            </a>
        </div>
    </div>
</section>
<section class="content">
    <!-- Start Display Details -->
    <!-- Main content -->
    <div class="row" >
        <section class="invoice" id="print_invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <img style="width: 60px;width: 60px;margin-top: -10px;margin-right: 10px;" src="<?= base_url() . config_item('invoice_logo') ?>" ><?= config_item('company_name') ?>
                    </h2>
                </div><!-- /.col -->
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    <div class="col-xs-4"><b><?= lang('client') ?>:</b> <?= $client_info->name ?></div>
                    <div class="col-xs-4 text-center"><b><?= lang('reference_no') ?>:</b> <?= $po_info->reference_no ?></div>
                    <div class="col-xs-4 text-right"><b><?= lang('created_date') ?>:</b> <?= strftime(config_item('date_format'), strtotime($po_info->created_date)) ?></div>
                </div>
                <div class="col-xs-12 table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th colspan="2"><?= lang('po_detail') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><b><?= lang('client') ?></b></td>
                            <td><?= ucfirst($client_info->name) ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('reference_no') ?></b></td>
                            <td><?= $po_info->reference_no ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('po_date') ?></b></td>
                            <td><?= strftime(config_item('date_format'), strtotime($po_info->po_date)) ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('title') ?></b></td>
                            <td><?= $po_info->A_NAME ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('amount') ?></b></td>
                            <td>Rs. <?= number_format(($po_info->po_amount),2) ?></td>
                        </tr>
                        <?php
                            $branch_info = $this->invoice_model->check_by(array('BR_ID' => $po_info->branch_id), 'branches');
                            $bank_info = $this->invoice_model->check_by(array('B_ID' => $branch_info->B_ID), 'banks');
                        ?>
                        <tr>
                            <td><b>Bank</b></td>
                            <td><?= $bank_info->B_NAME ?></td>
                        </tr>
                        <tr>
                            <td><b>Branch</b></td>
                            <td><?= $branch_info->BR_NAME ?></td>
                        </tr>
                        <tr>
                            <td><b>Note</b></td>
                            <td><?= $po_info->po_note ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>

</section>