<link href="<?php echo base_url() ?>asset/css/bootstrap-toggle.min.css" rel="stylesheet"> 
<script src="<?php echo base_url() ?>asset/js/bootstrap-toggle.min.js"></script>
<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
	<div class="row">
		<div class="col-lg-12">
            <form role="form" enctype="multipart/form-data" id="form" action="<?php echo base_url(); ?>admin/po/save_po/<?= (!empty($po_info))?encrypt($po_info->po_id):'' ?>" method="post" class="form-horizontal  ">
                <section class="panel panel-default">
                    <header class="panel-heading"><?= $title ?></header>
                    <?php
                        if(!empty($po_info)) {
                            $trans_account_info = $this->db->select('*')
                                ->from('transactions_meta')
                                ->where('T_ID',$po_info->transaction_id)
                                ->get()->result();
                            ?>

                            <input type="hidden" name="trans_account_id" value="<?= $trans_account_info[0]->TM_ID ?>" />
                            <input type="hidden" name="bank_and_cash_id" value="<?= $trans_account_info[1]->TM_ID ?>" />
                            <?php
                        }
                    ?>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('client') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="client_id" class="form-control select_box" onChange="get_job_no(this.value)" required>
                                    <option value="">---</option>
                                    <?php
                                        if(!empty($all_client)) {
                                            foreach ($all_client as $client) {
                                                ?>
                                                <option value="<?= $client->client_id ?>" <?php
                                                if(!empty($po_info)){
                                                    echo ($client->client_id == $po_info->client_id)?'selected':'';
                                                }
                                                ?>><?= $client->name ?></option>
                                                <?php
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('reference_no') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="invoices_id" class="form-control select_box" id="job_no" onChange="get_advance_accounts(this.value)" required>
                                    <option value="">---</option>
                                    <?php
                                        if(!empty($po_info)){
                                            $jobs = $this->invoice_model->check_by_all(array('client_id'=>$po_info->client_id), 'tbl_invoices');
                                            foreach($jobs as $job){
                                                ?>
                                                <option value="<?= $job->invoices_id ?>" <?= ($po_info->invoices_id == $job->invoices_id)?'selected':'' ?>><?= $job->reference_no ?></option>
                                    <?php
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('title') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="a_id" class="form-control select_box" id="advance_titles" required>
                                    <option value="">---</option>
                                    <?php
                                    if(!empty($po_info)){
                                        $ap_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Liabilities', 'NAME' => 'Current Liabilities'), 'sub_heads');
                                        $ap_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_head->SUB_HEAD_ID, 'H_NAME' => 'Advances from clients'), 'accounts_head');
                                        $advances_accounts = $this->invoice_model->check_by_all(array('SUB_HEAD_ID' => $ap_sub_head->SUB_HEAD_ID, 'H_ID' => $ap_sub_head->H_ID, 'CLIENT_ID' => $po_info->client_id), 'accounts');
                                        foreach($advances_accounts as $account){
                                            ?>
                                            <option value="<?= $account->A_ID ?>" <?= ($po_info->a_id == $account->A_ID)?'selected':'' ?>><?= $account->A_NAME ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="number" required name="po_amount" value="<?= (!empty($po_info))?round($po_info->po_amount,2):'' ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('payment_date') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="text" required name="po_date" class="form-control datepicker" data-date-format="<?= config_item('date_picker_format'); ?>" autocomplete="off" value="<?php
                                if(!empty($po_info)){
                                    echo ($po_info->po_date)?$po_info->po_date:'';
                                }
                                ?>">
                            </div>
                        </div>
                        <div id="cash_at_bank">
                            <?php
                            if(!empty($po_info)) {
                                $branch_info = $this->invoice_model->check_by(array('BR_ID' => $po_info->branch_id), 'branches');
                                $bank_info = $this->invoice_model->check_by(array('B_ID' => $branch_info->B_ID), 'banks');
                            }
                            ?>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Banks <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <select name="bank" required="" class="form-control cash_at_bank select_box" onchange="selectBranches(this.value)" style="width:100%;">
                                        <option value="">Choose Bank</option>
                                        <?php foreach ($banks as $bank): ?>
                                            <option value="<?php echo $bank->B_ID; ?>" <?php
                                            if(!empty($po_info)) {
                                                echo ($bank->B_ID == $bank_info->B_ID) ? 'selected' : '';
                                            }
                                            ?>><?php echo $bank->B_NAME; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div id="branches">
                                <?php
                                if(!empty($po_info)) {
                                        ?>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Branches <span
                                                        class="text-danger">*</span></label>
                                            <div class="col-lg-3">
                                                <select name="branch_id"
                                                        class="form-control cash_at_bank select_box"
                                                        onchange="getAccountId(this.value)">
                                                    <option value="">Choose Branch</option>
                                                    <?php
                                                    $branches = $this->invoice_model->check_by_all(array('B_ID' => $bank_info->B_ID), 'branches');
                                                    foreach ($branches as $branch) {
                                                        ?>
                                                        <option value="<?= $branch->BR_ID ?>" <?= ($branch->BR_ID == $branch_info->BR_ID) ? 'selected' : '' ?>><?= $branch->BR_NAME ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php
                                }
                                ?>
                            </div>
                            <input type="hidden" id="account_id" name="account_id" value="<?= (!empty($po_info))?$account_id->A_ID:'' ?>" />
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Cheque No / Pay Order No <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control cash_at_bank" name="po_no" value="<?= (!empty($po_info))?$po_info->po_no:''; ?>" required />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('notes') ?></label>
                            <div class="col-lg-3">
                                <textarea name="po_note" class="form-control"><?= (!empty($po_info))?$po_info->po_note:'' ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"></label>
                            <div class="col-lg-4">
                                <button type="submit" class="btn btn-sm btn-success pull-right"><i class="fa fa-check"></i>
                                    <?php
                                    if(!empty($po_info)){
                                        echo lang('update_po');
                                    }else{
                                        echo lang('create_po');
                                    }
                                    ?>
                                </button>
                            </div>
                        </div>
                    </div>
				</section>
            </form>
		</div>
	</div>
<script type="text/javascript">
    /*$('#type').on("change", function() {
        var type = $(this).val();
        if(type == '1'){
            $('#cash_at_bank').fadeOut(500, function() { $(this).hide(); });
            $(".cash_at_bank").attr("disabled","disabled");
            $(".cash_at_bank").removeAttr('required');
        }
        else{
            $('#cash_at_bank').fadeIn(500).show();
            $(".cash_at_bank").removeAttr("disabled");
            $(".cash_at_bank").prop('required',true);
        }
    });*/
    function selectBranches(val) {
        var option="";
        if(val == '') {
            $('#branches').hide();
        }
        else{
            $.getJSON("<?php echo site_url('admin/accounts/ajax_select_branches') ?>" + "/" + val, function (result) {
                /*option.empty();*/
                $.each(result, function (index, value) {
                    option = ('<option value="' + value.BR_ID + '">' + value.BR_NAME + '</option>')+option;
                });
                var drop = '<div class="form-group"><label class="col-lg-4 control-label">Branches <span class="text-danger">*</span></label><div class="col-lg-3"><select name="branch_id" required="" class="form-control cash_at_bank select_box" onchange="getAccountId(this.value)"><option value="">Choose Branch</option>' + option + '</select></div></div>';
                $('#branches').html(drop).hide().fadeIn(500);
                $('.select_box').select2({});
                getAccountId();
            });
        }
    }
    function getAccountId(val){
        account_id=$('#account_id');
        $.getJSON( "<?php echo site_url('admin/accounts/ajax_edit_branch') ?>"+"/"+val, function(result) {
            account_id.val(result.A_ID);
        });
    }

    /*** JOB NUMBER AJAX ***/
    function get_job_no(client_id) {
        var option_jobs="";
        var option_empty = '<option value="" selected>---</option>';
        if(client_id == '') {
            $('#job_no').html(option_empty).hide().fadeIn(500);
            $('.select_box').select2({});
        }
        else{
            $.getJSON("<?php echo site_url('admin/po/get_client_jobs') ?>" + "/" + client_id, function (result) {
                $.each(result, function (index, value) {
                    option_jobs = ('<option value="' + value.invoices_id + '">' + value.reference_no + '</option>')+option_jobs;
                });
                $('#job_no').html(option_empty+option_jobs).hide().fadeIn(500);
                $('.select_box').select2({});
            });
        }
    }
    /*** END JOB NUMBER AJAX ***/
    /*** GET ADVANCES ACCOUNTS AJAX ***/
    function get_advance_accounts(job_id) {
        var option_jobs="";
        var option_empty = '<option value="" selected>---</option>';
        if(job_id == '') {
            $('#advance_titles').html(option_empty).hide().fadeIn(500);
            $('.select_box').select2({});
        }
        else{
            $.getJSON("<?php echo site_url('admin/po/get_advance_accounts') ?>" + "/" + job_id, function (result) {
                $.each(result, function (index, value) {
                    option_jobs = ('<option value="' + value.A_ID + '">' + value.A_NAME + '</option>')+option_jobs;
                });
                $('#advance_titles').html(option_empty+option_jobs).hide().fadeIn(500);
                $('.select_box').select2({});
            });
        }
    }
    /*** END GET ADVANCES ACCOUNTS AJAX ***/
</script>