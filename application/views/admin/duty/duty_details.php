<section class="content-header">
    <?php
        $client_info = $this->invoice_model->check_by(array('client_id' => $requisition_info->client_id), 'tbl_client');
    ?>
	<div class="row">
        <div class="col-sm-8">
            <a class="btn btn-sm btn-default" href="<?= base_url() ?>admin/bill/search_bill/" title="Back"><i class="fa fa-arrow-left"></i> Back</a>
        </div>
        <div class="col-sm-4 pull-right">
            <a onclick="print_invoice('print_invoice_new')" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Print" class="btn btn-sm btn-danger pull-right"  >
                <i class="fa fa-print"></i>
            </a>
			<!--<a style="margin-right: 5px" href="<?/*= base_url() */?>admin/duty/manage_duty/pdf_duty/<?/*= $requisition_info->requisition_id */?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="PDF" class="btn btn-sm btn-warning pull-right" >
                <i class="fa fa-file-pdf-o"></i>
            </a>-->
        </div>
    </div>
</section>
<section class="content">
<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('duty_detail') ?></h1>
	<!-- Start Display Details -->
	<!-- Main content -->
    <div class="row" >
        <section class="invoice" id="print_invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <img style="width: 60px;width: 60px;margin-top: -10px;margin-right: 10px;" src="<?= base_url() . config_item('invoice_logo') ?>" ><?= config_item('company_name') ?>
                    </h2>
                </div><!-- /.col -->
            </div>
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th colspan="4">Description Of Consignment</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><b><?= lang('reference_no') ?></b></td>
                            <td><?= $this->invoice_model->job_no_creation($requisition_info->invoices_id) ?></td>
                            <td><b>Document Received Date</b></td>
                            <td class="text-right"><?= strftime(config_item('date_format'), strtotime($requisition_info->created_date)) ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('client') ?></b></td>
                            <td><?= $client_info->name ?></td>
							<td><b><?= lang('mode') ?></b></td>
                            <td class="text-right"><?= $requisition_info->mode; ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('igm') ?></b></td>
                            <td><?= $requisition_info->igm_no; ?></td>
                            <td><b><?= lang('igm_date') ?></b></td>
                            <td class="text-right"><?= ($requisition_info->igm_date != '0000-00-00')?strftime(config_item('date_format'), strtotime($requisition_info->igm_date)):'' ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('net_weight') ?></b></td>
                            <td>
                                <?php
                                    if($requisition_info->net_weight != 0.00){
                                        echo $requisition_info->net_weight. " KGS";
                                    }
                                ?>
                            </td>
                            <td><b><?= lang('gross_weight') ?></b></td>
                            <td class="text-right">
                                <?php
                                    if($requisition_info->gross_weight != 0.00){
                                        echo $requisition_info->gross_weight." KGS";
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><b><?= lang('port') ?></b></td>
                            <td><?= $requisition_info->port ?></td>
                            <td><b><?= lang('bl_no') ?></b></td>
                            <td class="text-right"><?= $requisition_info->bl_no ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('invoice_value') ?></b></td>
                            <td><?= number_format($this->invoice_model->get_invoice_value($requisition_info->invoices_id),2); ?></td>
                            <td><b><?= lang('currency') ?></b></td>
                            <td class="text-right"><?= $requisition_info->currency ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('exchange_rate') ?></b></td>
                            <td><?= number_format($requisition_info->exchange_rate,2)  ?></td>
                            <td><b><?= lang('notes') ?></b></td>
                            <td class="text-right"><?= $requisition_info->requisition_note ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-8">
                <?php
                    if(!empty($custom_duty_info)) {
                        foreach ($custom_duty_info as $duty_info) {
                            ?>
							<div class="col-xs-6 table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th colspan="2"><?= $duty_info->commodity ?></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?= lang('hs_code') ?></td>
                                <td><?= $duty_info->hs_code ?></td>
                            </tr>
                            <tr>
                                <td><?= lang('no_of_unit') ?></td>
                                <td><?= $duty_info->no_of_units ?></td>
                            </tr>
                            <tr>
                                <td><?= lang('unit_value') ?></td>
                                <td><?= $duty_info->unit_value ?></td>
                            </tr>
                            <tr>
                                <td><?= lang('insurance') ?></td>
                                <td><?= ($duty_info->insurance_type == 'Percent')?$duty_info->insurance_value.' %':$duty_info->insurance_value ?></td>
                            </tr>
                            <tr>
                                <td><?= lang('landing_charges') ?></td>
                                <td><?= $duty_info->landing_charges ?></td>
                            </tr>
                        <tr>
                            <td><?= lang('total') ?></td>
                            <td><?= ($requisition_info->requisition_type == 'Declared')?number_format($this->invoice_model->calculate_duty('declared_item_total', $duty_info->custom_duty_id),2):number_format($this->invoice_model->calculate_duty('assessable_item_total', $duty_info->commodity_id),2) ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <?php
					$this->db->select('*');
					$this->db->from('tbl_requisitions');
					$this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
					$this->db->where('tbl_requisitions.requisition_type', 'Declared');
					$this->db->where('tbl_requisitions.invoices_id', $duty_info->invoices_id);
					$this->db->where('tbl_custom_duties.commodity_id', $duty_info->commodity_id);
					$query = $this->db->get();
					$duty_info = $query->row();
				?>
                <div class="col-xs-6 table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th colspan="2">Custom Duties</th>
                            <th class="text-right">Amount (Rs.)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><b><?= lang('cd') ?></b></td>
                            <td class="text-right"><?= ($duty_info->duty_cd != 0.00)? round($duty_info->duty_cd, 2).' %':'' ?></td>
                            <td class="text-right"><?= ($requisition_info->requisition_type == 'Declared')?number_format($this->invoice_model->calculate_duty('invoice_cd',$duty_info->custom_duty_id),2):number_format($this->invoice_model->calculate_duty('assessable_cd',$duty_info->commodity_id),2) ?></td>
							</td>
                        </tr>
                        <tr>
                            <td><b><?= lang('acd') ?></b></td>
                            <td class="text-right"><?= ($duty_info->duty_acd != 0.00)?round($duty_info->duty_acd,2).' %':'' ?></td>
                            <td class="text-right"><?= ($requisition_info->requisition_type == 'Declared')?number_format($this->invoice_model->calculate_duty('invoice_acd',$duty_info->custom_duty_id),2):number_format($this->invoice_model->calculate_duty('assessable_acd',$duty_info->commodity_id),2) ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('rd') ?></b></td>
                            <td class="text-right"><?= ($duty_info->duty_rd != 0.00)?round($duty_info->duty_rd,2).' %':'' ?></td>
                            <td class="text-right"><?= ($requisition_info->requisition_type == 'Declared')?number_format($this->invoice_model->calculate_duty('invoice_rd',$duty_info->custom_duty_id),2):number_format($this->invoice_model->calculate_duty('assessable_rd',$duty_info->commodity_id),2) ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('st') ?></b></td>
                            <td class="text-right"><?= ($duty_info->duty_st != 0.00)?round($duty_info->duty_st,2).' %':'' ?></td>
                            <td class="text-right"><?= ($requisition_info->requisition_type == 'Declared')?number_format($this->invoice_model->calculate_duty('invoice_st',$duty_info->custom_duty_id),2):number_format($this->invoice_model->calculate_duty('assessable_st',$duty_info->commodity_id),2) ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('ast') ?></b></td>
                            <td class="text-right"><?= ($duty_info->duty_ast != 0.00)?round($duty_info->duty_ast,2).' %':'' ?></td>
                            <td class="text-right"><?= ($requisition_info->requisition_type == 'Declared')?number_format($this->invoice_model->calculate_duty('invoice_ast',$duty_info->custom_duty_id),2):number_format($this->invoice_model->calculate_duty('assessable_ast',$duty_info->commodity_id),2) ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('it') ?></b></td>
                            <td class="text-right"><?= ($duty_info->duty_it != 0.00)?round($duty_info->duty_it,2).' %':'' ?></td>
                            <td class="text-right"><?= ($requisition_info->requisition_type == 'Declared')?number_format($this->invoice_model->calculate_duty('invoice_it',$duty_info->custom_duty_id),2):number_format($this->invoice_model->calculate_duty('assessable_it',$duty_info->commodity_id),2) ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"><b><?= lang('total_duties') ?></b></td>
                            <td class="text-right">PKR <?= ($requisition_info->requisition_type == 'Declared')?number_format($this->invoice_model->calculate_duty('declared_duty_total',$duty_info->custom_duty_id),2):number_format($this->invoice_model->calculate_duty('assessable_duty_total',$duty_info->commodity_id),2) ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                            <?php
                        }
                    }
                ?>
                </div>
                <div class="col-xs-4 table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Other Expenses</th>
                            <th class="text-right">Amount (Rs.)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $total_other_expenses=0;
                            $this->db->select('*');
                            $this->db->from('tbl_other_expenses op');
                            $this->db->join('tbl_requisition_expenses re','op.requisition_expense_id = re.requisition_expense_id');
                            $this->db->where('op.invoices_id', $requisition_info->invoices_id);
                            $other_expenses = $this->db->get()->result();
                            foreach($other_expenses as $expense) {
                                if($expense->other_expense_amount == 0){
                                }
                                else {
                                    ?>
									<tr>
                                        <td><b><?= $expense->requisition_expense_title ?></b></td>
                                        <td class="text-right"><?= number_format(($expense->other_expense_amount), 2);
                                                $total_other_expenses+=$expense->other_expense_amount;
                                            ?></td>
                                    </tr>
                                    <?php
                                }
                            }
                        ?>
						<tr>
                            <td><b><?= lang('total_other_expenses') ?></b></td>
                            <td class="text-right">PKR <?= number_format($total_other_expenses,2); ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
				<div class="col-md-12">
					<b><input type="text" value="<?= lang('grand_total') ?> : PKR <?= ($requisition_info->requisition_type == 'Declared')?number_format($this->invoice_model->calculate_duty('declared_grand_total', $requisition_info->requisition_id),2):number_format($this->invoice_model->calculate_duty('assessable_grand_total', $requisition_info->requisition_id),2) ?>" class="form-control text-center" readonly /></b>
				</div>
            </div>
        </section>
        <section style="display: none;" class="invoice" id="print_invoice_new">
            <?php $this->load->view('admin/duty/pdf_duty'); ?>
        </section>
    </div>
</section>
<script type="text/javascript">
    function print_invoice(print_invoice_new) {
        var printContents = document.getElementById(print_invoice_new).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>