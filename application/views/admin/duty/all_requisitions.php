<?= message_box('success') ?>

<?php echo form_open(base_url('admin/duty/all_duties'),array('class'=>"form-horizontal")); ?>
<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-default">
            <header class="panel-heading  "><?= lang('search_by'); ?></header>
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-xs-2">
                        <select class="form-control select_box client" style="width: 100%"  name="client_id">
                            <option value="">Choose Client</option>
                            <?php
                            if (!empty($all_client)) {
                                foreach ($all_client as $v_client) {
                                    ?>
                                    <option value="<?= $v_client->client_id ?>" <?php
                                    if(isset($client_id)){
                                        echo ($v_client->client_id == $client_id)?'selected':'';
                                    }  ?>><?= $v_client->name ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-xs-2">
                        <input type="text" class="form-control" name="reference_no" placeholder="<?= lang('reference_no') ?>" value="<?= (!empty($reference_no))?$reference_no:'' ?>" />
                    </div>
                    <div class="col-xs-2">
                        <input type="text" class="form-control" name="bl_no" placeholder="<?= lang('bl_no') ?>" value="<?= (!empty($bl_no))?$bl_no:'' ?>" />
                    </div>
                    <div class="col-xs-2">
                        <input type="text" class="form-control" name="commodity" placeholder="<?= lang('commodity') ?>" value="<?= (!empty($commodity))?$commodity:'' ?>" />
                    </div>
                    <div class="col-xs-2">
                        <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> <?= lang('search') ?></button>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php echo form_close(); ?>
<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('all_duty') ?></h1>

<!--<section class="panel panel-default">
    <header class="panel-heading"><?/*= $title; */?> </header>
    <div class="panel-body">
        <table class="table table-striped DataTables " id="DataTables">
            <thead>
            <tr>
                <th><?/*= lang('job_no') */?></th>
                <th><?/*= lang('created_date') */?></th>
                <th><?/*= lang('requisition_type') */?></th>
                <th><?/*= lang('invoice_value') */?></th>
                <th><?/*= lang('exchange_rate') */?></th>
                <th><?/*= lang('total_duties') */?></th>
                <th><?/*= lang('total_other_expenses') */?></th>
                <th><?/*= lang('grand_total') */?></th>
                <th class="col-options no-sort" ><?/*= lang('action') */?></th>
            </tr>
            </thead>
            <tbody>
            <?php
/*
            $counter = 1;
            if (!empty($requisition_info)) {
                foreach ($requisition_info as $v_duties) {
                    */?>
                    <tr>
                        <td><a class="text-info" href="<?/*= base_url() */?>admin/invoice/manage_invoice/invoice_details/<?/*= encode($v_duties->invoices_id) */?>"><?/*= $this->invoice_model->job_no_creation($v_duties->invoices_id) */?></a></td>
                        <td><?/*= strftime(config_item('date_format'), strtotime($v_duties->created_date)) */?></td>
                        <td><?/*= $v_duties->requisition_type */?></td>
                        <td><?/*= $v_duties->currency */?> <?/*= number_format(($v_duties->invoice_value),2) */?></td>
                        <td>PKR <?/*= number_format(($v_duties->exchange_rate),2) */?></td>
                        <td>PKR <?/*= ($v_duties->requisition_type == 'Declared')?number_format($this->invoice_model->calculate_duty('declared_grand_total_duties', $v_duties->requisition_id),2):number_format($this->invoice_model->calculate_duty('assessable_grand_total_duties', $v_duties->requisition_id),2) */?></td>
                        <td>PKR <?/*= number_format($this->invoice_model->calculate_duty('other_expenses_total', $v_duties->invoices_id),2); */?></td>
                        <td>PKR <?/*= ($v_duties->requisition_type == 'Declared')?number_format($this->invoice_model->calculate_duty('declared_grand_total', $v_duties->requisition_id),2):number_format($this->invoice_model->calculate_duty('assessable_grand_total', $v_duties->requisition_id),2) */?></td>
                        <td>
                            <?/*= btn_view('admin/duty/manage_duty/duty_details/' . encode($v_duties->requisition_id)) */?>
                            <?/*= btn_edit('admin/duty/manage_duty/edit_duty/' . encode($v_duties->requisition_id)) */?>
                        </td>
                    </tr>
                    <?php
/*
                    $counter++;
                }
            }
            */?>
            </tbody>
        </table>
    </div>
</section>-->
<input type="hidden" id="client_id" value="<?= (!empty($client_id))?$client_id:'All' ?>">
<input type="hidden" id="reference_no" value="<?= (!empty($reference_no))?$reference_no:'All' ?>">
<input type="hidden" id="bl_no" value="<?= (!empty($bl_no))?$bl_no:'All' ?>">
<input type="hidden" id="commodity" value="<?= (!empty($commodities))?$commodities:'All' ?>">
<section class="panel panel-default">
    <header class="panel-heading"><?= $title; ?></header>
    <div class="panel-body" id="export_table">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered data-table-list">
                        <thead>
                        <th><?= lang('job_no') ?></th>
                        <th><?= lang('created_date') ?></th>
                        <th><?= lang('requisition_type') ?></th>
                        <th><?= lang('invoice_value') ?></th>
                        <th><?= lang('exchange_rate') ?></th>
                        <th><?= lang('total_duties') ?></th>
                        <th><?= lang('total_other_expenses') ?></th>
                        <th><?= lang('grand_total') ?></th>
                        <th class="col-options no-sort" ><?= lang('action') ?></th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    var client_id=$('#client_id').val();
    var reference_no=$('#reference_no').val();
    var bl_no=$('#bl_no').val();
    var commodity=$('#commodity').val();
    var data_url='<?= base_url('admin/duty/all_duty/') ?>'+client_id+'/'+reference_no+'/'+bl_no+'/'+commodity;
</script>