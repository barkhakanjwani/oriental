<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{
		padding: 4px;
	}
</style>
<section class="content-header">
    <div class="row">
        <div class="col-md-4">
        <a class="btn btn-sm btn-default" href="<?= base_url() ?>admin/duty/search_duty/" title="Back"><i class="fa fa-arrow-left"></i> Back</a>
        </div>
    </div>
</section>
<link href="<?php echo base_url() ?>asset/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?php echo base_url() ?>asset/js/bootstrap-toggle.min.js"></script>
<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
				<?php
                    $invoice_info = $this->invoice_model->check_by(array('invoices_id' => $invoices_id), 'tbl_invoices');
                ?>
			<form role="form" enctype="multipart/form-data" id="form_label" action="<?php echo base_url(); ?>admin/duty/save_duty/<?= (!empty($requisition_info))?encrypt($requisition_info->requisition_id):'' ?>" method="post" class="form-horizontal  ">
                <h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('create_duties') ?></h1>
				<section class="panel panel-default">
					<header class="panel-heading  "><?= $title ?></header>
					<div class="panel-body">
						<div class="form-group">
							<label class="col-lg-3 control-label"><?= lang('reference_no') ?></label>
							<div class="col-lg-5">
								<input type="text" class="form-control" value="<?= $this->invoice_model->job_no_creation($invoice_info->invoices_id) ?>" readonly />
								<input type="hidden" name="invoices_id" value="<?= $invoice_info->invoices_id ?>" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label"><?= lang('invoice_value') ?></label>
							<div class="col-lg-5">
								<input type="text" class="form-control" value="<?= round($this->invoice_model->get_invoice_value($invoices_id)) ?>" id="invoice_value" disabled />
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label"><?= lang('exchange_rate') ?></label>
							<div class="col-lg-5">
								<input type="number" min="0" class="form-control calculator" id="exchange_rate" value="<?= $invoice_info->exchange_rate ?>" disabled />
							</div>
						</div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"><?= lang('notes') ?></label>
                            <div class="col-lg-5">
                                <textarea class="form-control" name="requisition_note"><?= (!empty($requisition_info))?$requisition_info->requisition_note:'' ?></textarea>
                            </div>
                        </div>
                        <?php
                            $commodity_info = $this->invoice_model->check_by_all(array('invoices_id' => $invoices_id), 'tbl_saved_commodities');
                            if(!empty($commodity_info)) {
                                ?>
								<div class="col-md-8">
                                <?php
                                    $k=0;
                                    foreach ($commodity_info as $commodities_info){
                                    	if(!empty($requisition_info)){
                                            $duty_info = $this->invoice_model->check_by(array('commodity_id' => $commodities_info->id, 'requisition_id' => $requisition_info->requisition_id), 'tbl_custom_duties');
                                        }
                                        ?>
										<h4 style="border-bottom: 3px solid #527e92;padding: 8px;color: #527e92;text-transform: uppercase;font-weight: bold;"><?= $commodities_info->commodity ?></h4>
										<input type="hidden" value="<?= $commodities_info->id ?>" name="commodity_id[]" />
										<div class=" table-responsive">
                                <table class="table">
                                    <tr>
                                        <th colspan="2"><?= lang('insurance') ?></th>
                                        <th><?= lang('landing_charges') ?></th>
                                        <th><?= lang('no_of_unit') ?></th>
                                        <th colspan="2"><?= lang('unit_value') ?></th>
                                        <th><?= lang('total') ?></th>
                                    </tr>
                                    <tr class="commodity_total">
                                        <!--<td><input type="text" class="form-control" id="hs_code<?/*= $k */?>" value="<?/*= $commodities_info->hs_code */?>" readonly></td>
                                        <td><input type="text" class="form-control weight" id="weight<?/*= $k */?>" value="<?/*= $commodities_info->weight */?>" readonly></td>-->
                                        <td colspan="2">
											<div class="input-group">
												<input class="form-control calculator" type="number" min="0" name="insurance_value[]" id="insurance<?= $k ?>" onkeyup="calculateInvoiceTotal();auto_calculate('<?= $k ?>');" value="<?php
												if(!empty($requisition_info)){
													echo ($duty_info->insurance_value!= 0.00)?round($duty_info->insurance_value,2):'';
												} ?>" required>
												<span class="input-group-addon">
													<input type="checkbox" name="insurance_type[]" class="calculator" id="insurance_check<?= $k ?>" value="Percent" onClick="calculateInvoiceTotal();auto_calculate('<?= $k ?>');" <?php
	                                                if (!empty($requisition_info)) {
	                                                	echo ($duty_info->insurance_type == 'Percent')?"checked":'';
													}
												?> /> %
												</span>
											</div>
										</td>
                                        <td class="text-center">
                                    		<input type="checkbox" name="landing_charges[]" class="calculator" id="landing_charges<?= $k ?>" onClick="calculateInvoiceTotal();auto_calculate('<?= $k ?>');"  value="Yes" <?php
		                                        if (!empty($requisition_info)) {
		                                        	echo ($duty_info->landing_charges == 'Yes')?"checked":'';
												}
											?> />
										</td>
                                        <td><input type="text" class="form-control no_of_unit" id="no_of_unit<?= $k ?>" value="<?= $commodities_info->no_of_units ?>" readonly></td>
										<td><input type="number" class="form-control unit_value" name="value_per_unit[]" id="unit_value<?= $k ?>" value="<?= round($commodities_info->unit_value_assessed,2) ?>" onkeyup="calculateInvoiceTotal()" readonly></td>
                                        <td colspan="2"><input type="text" class="form-control total_sum text-center" name="total" id="total<?= $k ?>" value="<?= round(($commodities_info->no_of_units*$commodities_info->unit_value_assessed),2)?>" readonly>
                                            <input type="hidden" id="cmd_sum<?= $k ?>" value="<?= ($commodities_info->no_of_units*$commodities_info->unit_value_assessed) ?>"></td>
                                    </tr>
                                    <tr>
                                        <th>CD</th>
                                        <th>FED</th>
                                        <th>ACD</th>
                                        <th>RD</th>
                                        <th>ST</th>
                                        <th>AST</th>
                                        <th>IT</th>
                                    </tr>
    
                                    <?php
                                        if(!empty($requisition_info)) {
                                            if(!empty($duty_info)) {
                                                ?>
												<input type="hidden" value="<?= $duty_info->custom_duty_id ?>"
													   name="custom_duty_id[]"/>
                                                <?php
                                            }
            
                                            ?>
                                        <tr>
                                            <td>
                                                <input class="form-control calculator" type="number" min="0" name="duty_cd[]" id="cd<?= $k ?>" onkeyup="auto_calculate(<?= $k ?>)" value="<?php
                                                    if (!empty($requisition_info->requisition_type == 'Declared') && !empty($duty_info)) {
                                                        echo ($duty_info->duty_cd != 0.00)?round($duty_info->duty_cd,2):'';
                                                    }
                                                ?>">
                                            </td>
                                            <td>
                                                <input class="form-control calculator" type="number" min="0" name="duty_fed[]" id="fed<?= $k ?>" onkeyup="auto_calculate(<?= $k ?>)" value="<?php
                                                    if (!empty($requisition_info->requisition_type == 'Declared') && !empty($duty_info)) {
                                                        echo ($duty_info->duty_fed != 0.00)?round($duty_info->duty_fed,2):'';
                                                    }
                                                ?>">
                                            </td>
                                            <td><input class="form-control calculator" type="number" min="0" name="duty_acd[]" id="acd<?= $k ?>" onkeyup="auto_calculate(<?= $k ?>)" value="<?php
                                                    if (!empty($requisition_info->requisition_type == 'Declared') && !empty($duty_info)) {
                                                        echo ($duty_info->duty_acd != 0.00)?round($duty_info->duty_acd,2):'';
                                                    }
                                                ?>"></td>
                                            <td><input class="form-control calculator" type="number" min="0" name="duty_rd[]" id="rd<?= $k ?>" onkeyup="auto_calculate(<?= $k ?>)" value="<?php
                                                    if (!empty($requisition_info->requisition_type == 'Declared') && !empty($duty_info)) {
                                                        echo ($duty_info->duty_rd != 0.00)?round($duty_info->duty_rd,2):'';
                                                    }
                                                ?>"></td>
                                            <td><input class="form-control calculator" type="number" min="0" name="duty_st[]" id="st<?= $k ?>" onkeyup="auto_calculate(<?= $k ?>)" value="<?php
                                                    if (!empty($requisition_info->requisition_type == 'Declared') && !empty($duty_info)) {
                                                        echo ($duty_info->duty_st != 0.00)?round($duty_info->duty_st,2):'';
                                                    }
                                                ?>"></td>
                                            <td><input class="form-control calculator" type="number" min="0" name="duty_ast[]" id="ast<?= $k ?>" onkeyup="auto_calculate(<?= $k ?>)" value="<?php
                                                    if (!empty($requisition_info->requisition_type == 'Declared') && !empty($duty_info)) {
                                                        echo ($duty_info->duty_ast != 0.00)?round($duty_info->duty_ast,2):'';
                                                    }
                                                ?>"></td>
                                            <td><input class="form-control calculator" type="number" min="0" name="duty_it[]" id="it<?= $k ?>" onkeyup="auto_calculate(<?= $k ?>)" value="<?php
                                                    if (!empty($requisition_info->requisition_type == 'Declared') && !empty($duty_info)) {
                                                        echo ($duty_info->duty_it != 0.00)?round($duty_info->duty_it,2):'';
                                                    }
                                                ?>"></td>
                                        </tr>
											<tr>
                                            <td><input class="form-control text-center calculate_sum" type="text" id="cd_sum<?= $k ?>" min="0" value="<?= (!empty($duty_info))?round($this->invoice_model->calculate_duty('invoice_cd',$duty_info->custom_duty_id),2):'' ?>" readonly></td>
                                            <td><input class="form-control text-center calculate_sum" type="text" id="fed_sum<?= $k ?>" min="0" value="<?= (!empty($duty_info))?round($this->invoice_model->calculate_duty('invoice_fed',$duty_info->custom_duty_id),2):'' ?>" readonly></td>
                                            <td><input class="form-control text-center calculate_sum" type="text" id="acd_sum<?= $k ?>" min="0" value="<?= (!empty($duty_info))?round($this->invoice_model->calculate_duty('invoice_acd',$duty_info->custom_duty_id),2):'' ?>" readonly></td>
                                            <td><input class="form-control text-center calculator" type="text" min="0" id="rd_sum<?= $k ?>" value="<?= (!empty($duty_info))?round($this->invoice_model->calculate_duty('invoice_rd',$duty_info->custom_duty_id),2):'' ?>" readonly></td>
                                            <td><input class="form-control text-center calculator" type="text" id="st_sum<?= $k ?>" min="0" value="<?= (!empty($duty_info))?round($this->invoice_model->calculate_duty('invoice_st',$duty_info->custom_duty_id),2):'' ?>" readonly></td>
                                            <td><input class="form-control text-center calculator" type="text" min="0" id="ast_sum<?= $k ?>" value="<?= (!empty($duty_info))?round($this->invoice_model->calculate_duty('invoice_ast',$duty_info->custom_duty_id),2):'' ?>" readonly></td>
                                            <td><input class="form-control text-center calculator" type="text" id="it_sum<?= $k ?>" value="<?= (!empty($duty_info))?round($this->invoice_model->calculate_duty('invoice_it',$duty_info->custom_duty_id),2):'' ?>" readonly></td>
                                        </tr>
											<tr>
                                            <td colspan="7"><input class="form-control text-center comodity_duties" type="text" id="total_duties<?= $k ?>" name="total_duties[]" value="TOTAL DUTIES : <?php
                                                    if (!empty($requisition_info->requisition_type == 'Declared') && !empty($duty_info)) {
                                                        echo number_format($this->invoice_model->calculate_duty('declared_duty_total', $duty_info->custom_duty_id),2);
                                                    }
                                                ?>" disabled></td>
                                        </tr>
                                            <?php
                                        }else {
                                            ?>
											<tr>
                                            <td><input class="form-control calculator" type="number" min="0"
													   name="duty_cd[]" id="cd<?= $k ?>"
													   onkeyup="auto_calculate(<?= $k ?>)"
													   ></td>
                                            <td><input class="form-control calculator" type="number" min="0"
													   name="duty_fed[]" id="fed<?= $k ?>"
													   onkeyup="auto_calculate(<?= $k ?>)"
												></td>
                                            <td><input class="form-control calculator" type="number" min="0"
													   name="duty_acd[]" id="acd<?= $k ?>"
													   onkeyup="auto_calculate(<?= $k ?>)"
													  ></td>
                                            <td><input class="form-control calculator" type="number" min="0"
													   name="duty_rd[]" id="rd<?= $k ?>"
													   onkeyup="auto_calculate(<?= $k ?>)"
													   ></td>
                                            <td><input class="form-control calculator" type="number" min="0"
													   name="duty_st[]" id="st<?= $k ?>"
													   onkeyup="auto_calculate(<?= $k ?>)"
													   ></td>
                                            <td><input class="form-control calculator" type="number" min="0"
													   name="duty_ast[]" id="ast<?= $k ?>"
													   onkeyup="auto_calculate(<?= $k ?>)"
													   ></td>
                                            <td><input class="form-control calculator" type="number" min="0"
													   name="duty_it[]" id="it<?= $k ?>"
													   onkeyup="auto_calculate(<?= $k ?>)"
													   ></td>
                                        </tr>
											<tr>
                                            <td><input class="form-control text-center calculate_sum" type="number"
													   id="cd_sum<?= $k ?>" min="0" name="duty_cd_sum[]" readonly></td>
                                            <td><input class="form-control text-center calculate_sum" type="number"
													   id="fed_sum<?= $k ?>" min="0" name="duty_fed_sum[]" readonly></td>
                                            <td><input class="form-control text-center calculate_sum" type="number"
													   id="acd_sum<?= $k ?>" min="0" name="duty_acd_sum[]" readonly>
                                            </td>
                                            <td><input class="form-control text-center calculator" type="number" min="0"
													   id="rd_sum<?= $k ?>" name="duty_rd_sum[]" readonly></td>
                                            <td><input class="form-control text-center calculator" type="number"
													   id="st_sum<?= $k ?>" min="0" name="duty_st_sum[]" readonly></td>
                                            <td><input class="form-control text-center calculator" type="number" min="0"
													   id="ast_sum<?= $k ?>" name="duty_ast_sum[]"
													   onkeyup="auto_calculate(<?= $k ?>)"
													    readonly></td>
                                            <td><input class="form-control text-center calculator" type="number"
													   id="it_sum<?= $k ?>" name="duty_it_sum[]" readonly></td>
                                        </tr>
											<tr>
                                            <td colspan="7"><input class="form-control text-center comodity_duties"
																   type="text" id="total_duties<?= $k ?>"
																   name="total_duties[]" value="TOTAL DUTIES : 0.00"
																   disabled></td>
                                        </tr>
                                            <?php
                                        }
                                    ?>
                                </table>
                                    </div>
                                        <?php
                                        $k++;
                                    }
                                ?>
                            </div>
                                <?php
                            }
                        ?>
						<!--- OTHER EXPENSES --->
						<div class="col-lg-4">
                            <h4 style="border-bottom: 3px solid #527e92;padding: 8px;color: #527e92;text-transform: uppercase;font-weight: bold;text-align: center;">Other Expenses</h4>
                            <?php
                                $total_other_expenses=0;
                                if(!empty($requisition_expenses)){
                                    $counter = 1;
                                    foreach($requisition_expenses as $expense) {
                                        if (empty($requisition_info)) {
                                            ?>
											<div class="form-group">
                                            <label class="col-lg-6 control-label"><?= $expense->requisition_expense_title ?></label>
                                            <div class="col-lg-6">
                                                <input type="hidden" name="requisition_expense_id[]"
													   value="<?= $expense->requisition_expense_id ?>"/>
                                                <input class="form-control calculator calculate_expenses" type="number"
													   min="0" name="other_expense_amount[]" placeholder="Amount"/>
                                            </div>
                                        </div>
                                            <?php
                                        }else{
                                            $other_expense = $this->invoice_model->check_by(array('requisition_expense_id'=>$expense->requisition_expense_id, 'requisition_id'=>$requisition_info->requisition_id), 'tbl_other_expenses');
                                            ?>
											<div class="form-group">
                                                <label class="col-lg-6 control-label"><?= $expense->requisition_expense_title ?></label>
                                                <div class="col-lg-6">
                                                    <input type="hidden" name="requisition_expense_id[]"
														   value="<?= $expense->requisition_expense_id ?>"/>
                                                    <input class="form-control calculator calculate_expenses"
														   type="number"
														   min="0" name="other_expense_amount[]" placeholder="Amount"
														   value="<?= (!empty($other_expense))?round($other_expense->other_expense_amount, 2):'' ?>"/>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                }
                            ?>
							<div class="form-group">
								<label class="col-lg-6 control-label"><?= lang('total_other_expenses') ?> </label>
								<div class="col-lg-6">
									<input class="form-control text-center" type="text" id="total_other_expenses" value="<?php
                                        if (!empty($requisition_info)) {
                                            echo number_format($this->invoice_model->calculate_duty('other_expenses_total', $requisition_info->invoices_id),2);
                                        }
                                    ?>" disabled />
								</div>
							</div>
						</div>
                    </div>
                    <div class="form-group" id="grand_total_div">
                        <label class="col-lg-3 control-label"><strong><?= lang('grand_total') ?></strong> </label>
                        <div class="col-lg-4">
                            <input class="form-control text-center" type="text" id="grand_total" value="<?php
                                if (!empty($requisition_info)) {
                                    echo number_format($this->invoice_model->calculate_duty('declared_grand_total', $requisition_info->requisition_id),2);
                                }
                            ?>" disabled />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"></label>
                        <div class="col-lg-4">
                            <button type="submit" class="btn btn-sm btn-success pull-right"><i class="fa fa-check"></i>
                                <?php
                                    if(!empty($requisition_info)){
                                        echo lang('update_duty');
                                    }else{
                                        echo lang('create_duty');
                                    }
                                ?>
                            </button>
                        </div>
                    </div>
                </section>
            </form>
		</div>
	</div>
</section>
<script type="text/javascript">
    /*$(function(){
        $('input[name="requisition_type"]').click(function(){
            if ($(this).is(':checked'))
            {
                var value = $(this).val();
                if(value == 'Declared'){
                    $('#assessable').hide();
                    $('#grand_total_div').show();
                    $('#declared').fadeIn(500).show();
                    $('.declared_columns').fadeIn(500).show();
                    $(".unit_value").attr("readonly","readonly");
                }
                else{
                    $('#declared').hide();
                    $('#grand_total_div').hide();
                    $('.declared_columns').hide();
                    $('#assessable').fadeIn(500).show();
                    $(".unit_value").removeAttr("readonly");
                }
            }
        });
    });*/
    function calculateInvoiceTotal() {
    
        var exchange_rate = $('#exchange_rate').val() == '' ? 0 : parseFloat($('#exchange_rate').val());
        var ind=0;
            $('.commodity_total').each(function() {
                var total_sum = $('#no_of_unit'+ind).val() * $('#unit_value'+ind).val() * exchange_rate;
                var insurance = $('#insurance'+ind).val() == '' ? 0 : parseFloat($('#insurance'+ind).val());
                if($('#insurance_check'+ind).is(':checked')){
                    total_sum = parseFloat(total_sum)+parseFloat(insurance*total_sum/100);
                }else{
                    total_sum = parseFloat(total_sum)+parseFloat(insurance);
                }
                if($('#landing_charges'+ind).is(':checked')){
                    total_sum += parseFloat(total_sum*1/100);
                }else{
                    total_sum += 0;
                }
                $('#total'+ind).val(total_sum.toFixed(2));
                $('#cmd_sum'+ind).val(total_sum.toFixed(2));
                ind+=1;
            });
        calculateDutiesAndTaxes();
    }
    function auto_calculate(index){
        /* Total Duties*/
        var commodity_total = $('#total'+index).val() == '' ? 0 : parseFloat($('#total'+index).val());
        var cd = $('#cd'+index).val() == '' ? 0 : parseFloat($('#cd'+index).val());
        var fed = $('#fed'+index).val() == '' ? 0 : parseFloat($('#fed'+index).val());
        var acd = $('#acd'+index).val() == '' ? 0 : parseFloat($('#acd'+index).val());
        var rd = $('#rd'+index).val() == '' ? 0 : parseFloat($('#rd'+index).val());
        var st = $('#st'+index).val() == '' ? 0 : parseFloat($('#st'+index).val());
        var ast = $('#ast'+index).val() == '' ? 0 : parseFloat($('#ast'+index).val());
        var it = $('#it'+index).val() == '' ? 0 : parseFloat($('#it'+index).val());
        var cd_total = commodity_total/100 * cd;
        var fed_total = commodity_total/100 * fed;
        var acd_total = commodity_total/100 * acd;
        var rd_total = commodity_total/100 * rd;
        $("#cd_sum"+index).val(cd_total.toFixed(2));
        $("#fed_sum"+index).val(fed_total.toFixed(2));
        $("#acd_sum"+index).val(acd_total.toFixed(2));
        $("#rd_sum"+index).val(rd_total.toFixed(2));
        // ST
        var st_total = cd_total+acd_total+rd_total+commodity_total;
        var st_percent = st_total/100 * st;
        $("#st_sum"+index).val(st_percent.toFixed(2));
        // AST
        var ast_total = cd_total+acd_total+rd_total+commodity_total;
        var ast_percent = ast_total/100 * ast;
        $("#ast_sum"+index).val(ast_percent.toFixed(2));
        // IT
        var it_total = cd_total+acd_total+rd_total+st_percent+ast_percent+commodity_total;
        var it_percent = it_total/100 * it;
        $("#it_sum"+index).val(it_percent.toFixed(2));
        // Total
        var total_duties = cd_total+fed_total+acd_total+rd_total+st_percent+ast_percent+it_percent;
        $('#total_duties'+index).val("TOTAL DUTIES : "+total_duties.toFixed(2));
        /*** OTHER EXPENSES ***/
        var total_expenses = 0;
        $('.calculate_expenses').each(function () {
            if ($(this).val() != '') {
                total_expenses = total_expenses + parseFloat($(this).val());
            }
        });
        $('#total_other_expenses').val(total_expenses.toFixed(2));
        var total_duties=0;
        $('.comodity_duties').each(function() {
            var val = $(this).val().split(':');
            if(val[1] != ' '){
                total_duties = total_duties + parseFloat(val[1]);
            }
        });
        var grand_total = total_duties+total_expenses;
        $('#grand_total').val(grand_total.toFixed(2));
    }
    function calculateDutiesAndTaxes() {
        /*** OTHER EXPENSES ***/
        var total_expenses = 0;
        $('.calculate_expenses').each(function () {
            if ($(this).val() != '') {
                total_expenses = total_expenses + parseFloat($(this).val());
            }
        });
        $('#total_other_expenses').val(total_expenses.toFixed(2));
        var total_duties=0;
        $('.comodity_duties').each(function() {
            var val = $(this).val().split(':');
            if(val[1] != ' '){
                total_duties = total_duties + parseFloat(val[1]);
            }
        });
        var grand_total = total_duties+total_expenses;
        $('#grand_total').val(grand_total.toFixed(2));
    }
    $('.calculate_expenses').keyup(function() {
        calculateDutiesAndTaxes();
    });
    $('.calculate_expenses').change(function() {
        calculateDutiesAndTaxes();
    });

    $(document).ready(function () {
        calculateInvoiceTotal();
        calculateDutiesAndTaxes();

    })
    
</script>