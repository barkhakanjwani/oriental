<link href="<?php echo base_url() ?>asset/css/bootstrap-toggle.min.css" rel="stylesheet">

<script src="<?php echo base_url() ?>asset/js/bootstrap-toggle.min.js"></script>

<?php echo message_box('success'); ?>

<?php echo message_box('error'); ?>

	<div class="row">

	<div class="col-lg-12">

        <form role="form" enctype="multipart/form-data" id="form_label" action="<?php echo base_url(); ?>admin/duty/create_duty" method="post" class="form-horizontal  ">
            <h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('create_duties') ?></h1>
            <section class="panel panel-default">

                <header class="panel-heading  "><?= lang('create_duty') ?></header>

                <div class="panel-body">

                    <div class="form-group">

                        <label class="col-lg-3 control-label"><?= lang('search_by') ?> <span class="text-danger">*</span></label>

                        <div class="col-lg-3">

                            <select class="form-control select_box" style="width: 100%" id="search_by"  required>

                                <option value=""><?= lang('search_by'); ?></option>

                                <option value="job_no" <?= (isset($reference_no))?'selected':'' ?>><?= lang('reference_no'); ?></option>

                                <option value="client" <?= (isset($client_id))?'selected':'' ?>><?= lang('client'); ?></option>

                            </select>

                        </div>

                    </div>

                    <div class="form-group" style="<?= (isset($client_id))?'display:block;':'display:none;' ?>" id="client">

                        <label class="col-lg-3 control-label"><?= lang('client') ?> <span class="text-danger">*</span></label>

                        <div class="col-lg-3">

                            <select class="form-control select_box client" style="width: 100%"  name="client_id" required <?= (isset($client_id))?'':'disabled' ?>>

                                <option value="">Choose Client</option>

                                <?php

                                if (!empty($all_client)) {

                                    foreach ($all_client as $v_client) {

                                        ?>

                                        <option value="<?= $v_client->client_id ?>" <?php

                                        if(isset($client_id)){

                                            echo ($v_client->client_id == $client_id)?'selected':'';

                                        }  ?>><?= $v_client->name ?></option>

                                        <?php

                                    }

                                }

                                ?>

                            </select>

                        </div>

                    </div>

                    <div class="form-group" style="<?= (isset($reference_no))?'display:block;':'display:none;' ?>" id="job_no">

                        <label class="col-lg-3 control-label"><?= lang('reference_no') ?> <span class="text-danger">*</span></label>

                        <div class="col-lg-3">

                            <input type="text" class="form-control job_no" name="reference_no" placeholder="<?= lang('reference_no') ?>" value="<?= (isset($reference_no))?$reference_no:'' ?>" <?= (isset($reference_no))?'':'disabled' ?> required autocomplete="off" />

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-lg-3 control-label"></label>

                        <div class="col-lg-3">

                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-search"></i> <?= lang('search') ?></button>

                        </div>

                    </div>

                </div>

            </section>

        </form>

		</div>

	</div>

	<?php

		if(isset($reference_no) || isset($client_id)){

	?>

	<div class="row">

		<div class="col-lg-12">

			<section class="panel panel-default">

                <header class="panel-heading  "><?= lang('manage_duties') ?></header>

                <div class="panel-body">

			<div class="table-responsive">

				<table class="table table-striped DataTables " id="DataTables">

                    <thead>

                        <tr>

                            <!--<th>#</th>-->

                            <th><?= lang('job_no') ?></th>

                            <th><?= lang('created_date') ?></th>

                            <th><?= lang('invoice_value') ?></th>

                            <th><?= lang('exchange_rate') ?></th>

                            <th class="col-options no-sort" ><?= lang('action') ?></th>

                        </tr>

                    </thead>

                    <tbody>

                        <?php
                            $counter = 1;
							if (!empty($job_info)) {
								foreach ($job_info as $v_job) {
                                    ?>
                                    <tr>
                                        <td><a class="text-info" href="<?= base_url() ?>admin/invoice/manage_invoice/invoice_details/<?= encode($v_job->invoices_id) ?>"><?= $this->invoice_model->job_no_creation($v_job->invoices_id) ?></a></td>
                                        <td><?= strftime(config_item('date_format'), strtotime($v_job->created_date)) ?></td>
                                        <td><?= $v_job->currency ?> <?= number_format(($this->invoice_model->get_invoice_value($v_job->invoices_id)),2) ?></td>
                                        <td>PKR <?= number_format(($v_job->exchange_rate),2) ?></td>
                                        <td><a class="btn btn-info btn-xs" href="<?= base_url('admin/duty/manage_duty/create_duty/'.encode($v_job->invoices_id)) ?>"><i class="fa fa-plus"></i> Create</a></td>
                                    </tr>
									<?php
                                    $counter++;
								}
							}
							?>
                    </tbody>
                </table>
			</div>
		</section>
	</div>
	</div>
	<?php

		}

	?>

    <script>

        /*$(".client").attr("disabled", true);

        $(".job_no").attr("disabled", true);*/

        $('#search_by').on("change", function() {

            var type = $(this).val();

            if(type == 'client'){

                $('#client').show();

                $('#job_no').hide();

                $(".client").removeAttr("disabled");

                $(".job_no").attr("disabled","disabled");

            }

            else if (type == 'job_no'){

                $('#job_no').show();

                $('#client').hide();

                $(".job_no").removeAttr("disabled");

                $(".client").attr("disabled","disabled");

            }

            else{

                $('#client').hide();

                $('#job_no').hide();

                $(".client").removeAttr("disabled");

                $(".job_no").removeAttr("disabled");

            }

        });

    </script>

