<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?= lang('duty') ?> (<?= $this->invoice_model->job_no_creation($requisition_info->invoices_id) ?>)</title>
    <style>
        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #5D6975;
            text-decoration: underline;
        }

        body {
            position: relative;
            width: 19cm;
            height: 29.7cm;
            margin: 0 auto;
            color: #001028;
            background: #FFFFFF;
            font-family: "verdana", "sans-serif";
            font-size: 6px;
            text-transform: uppercase;
        }

        header {
            padding: 10px 0;
            margin-bottom: 10px;
        }

        #logo {
            text-align: center;
            margin-bottom: 10px;
        }

        #logo img {
            width: 90px;
        }

        h1 {
            border-top: 1px solid  #5D6975;
            border-bottom: 1px solid  #5D6975;
            color: #5D6975;
            font-size: 2.4em;
            line-height: 1.4em;
            font-weight: normal;
            text-align: center;
            margin: 0 0 20px 0;
            background: url(../../../uploads/dimension.png);
        }

        #project {
            float: left;
        }

        #project span {
            color: #5D6975;
            text-align: right;
            width: 52px;
            margin-right: 10px;
            display: inline-block;
            font-size: 0.9em;
        }

        #company {
            float: right;
            text-align: right;
        }

        #project div,
        #company div {
            white-space: nowrap;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 10px;
        }

        table tr:nth-child(2n-1) td {
            background: #F5F5F5;
        }

        table th,
        table td {
            text-align: center;
        }

        table th {
            padding: 5px;
            color: #333;
            border: 1px solid #333;
            white-space: nowrap;
            font-weight: bold;
            text-transform: uppercase;
        }

        table .service,
        table .desc {
            text-align: left;
        }

        table td {
            padding: 5px;
            text-align: right;
            border:1px solid #333;
        }

        table td.service,
        table td.desc {
            vertical-align: top;
        }

        table td.unit,
        table td.qty,
        table td.total {
            font-size: 1.2em;
        }

        table td.grand {
            border-top: 1px solid #5D6975;;
        }

        #notices .notice {
            color: #5D6975;
            font-size: 1.5em;
        }

        footer {
            color: #5D6975;
            width: 100%;
            height: 10px;
            position: absolute;
            bottom: 0;
            border-top: 1px solid #C1CED9;
            padding: 8px 0;
            text-align: center;
        }
    </style>
</head>
<body>
<?php
    $client_info = $this->invoice_model->check_by(array('client_id' => $requisition_info->client_id), 'tbl_client');
?>
<header class="clearfix">
    <div id="logo">
        <img src="<?= base_url() . config_item('invoice_logo') ?>">
    </div>
    <h1><?= lang('duty') ?></h1>
    <div id="project">
        <div><span>To Messer's </span><b><?= ucfirst($client_info->name) ?></b></div>
        <div><span>Address </span><b><?= ucfirst($client_info->address) ?> </b></div>
    </div>
</header>
<main>
    <table>
        <tr style="background:#ccc;">
            <th colspan="4" style="text-align: left">Description Of Consignment</th>
        </tr>
        <tr>
            <td style="text-align:left;"><b><?= lang('reference_no') ?></b></td>
            <td><?= $this->invoice_model->job_no_creation($requisition_info->invoices_id) ?></td>
            <td><b>Document Received Date</b></td>
            <td class="text-right"><?= ($requisition_info->created_date != '0000-00-00')?strftime(config_item('date_format'), strtotime($requisition_info->created_date)):'' ?></td>
        </tr>
        <tr>
            <td style="text-align:left;"><b><?= lang('client') ?></b></td>
            <td><?= $client_info->name ?></td>
			<td style="text-align:left;"><b><?= lang('mode') ?></b></td>
            <td class="text-right"><?= $requisition_info->mode; ?></td>
        </tr>
        <tr>
            <td style="text-align:left;"><b><?= lang('igm') ?></b></td>
            <td><?= $requisition_info->igm_no; ?></td>
            <td><b><?= lang('igm_date') ?></b></td>
            <td class="text-right"><?= ($requisition_info->igm_date != '0000-00-00')?strftime(config_item('date_format'), strtotime($requisition_info->igm_date)):'' ?></td>
        </tr>
        <?php
        $commodities = $this->invoice_model->check_by_all(array('invoices_id'=>$requisition_info->invoices_id), 'tbl_saved_commodities');
        if(!empty($commodities)){
            foreach($commodities as $commodity){
                ?>
                <tr>
                    <td style="text-align:left;"><b><?= lang('commodity') ?></b></td>
                    <td><?= $commodity->commodity ?></td>
                    <td><b><?= lang('hs_code') ?></b></td>
                    <td class="text-right"><?= $commodity->hs_code ?></td>
                </tr>
                <?php
            }
        }
        ?>
        <tr>
            <td style="text-align:left;"><b><?= lang('net_weight') ?></b></td>
            <td><?= ($requisition_info->net_weight != 0.00)?$requisition_info->net_weight:'KGS' ?></td>
            <td><b><?= lang('gross_weight') ?></b></td>
            <td class="text-right"><?= ($requisition_info->gross_weight != 0.00)?$requisition_info->gross_weight:'KGS' ?></td>
        </tr>
        <tr>
            <td style="text-align:left;"><b><?= lang('packages') ?></b></td>
            <td><?= ($requisition_info->packages != 0.00)?$requisition_info->packages:'' ?></td>
            <td><b><?= lang('lc_no') ?></b></td>
            <td class="text-right"><?= $requisition_info->lc_no ?></td>
        </tr>
        <tr>
            <td style="text-align:left;"><b><?= lang('port') ?></b></td>
            <td><?= $requisition_info->port ?></td>
            <td><b><?= lang('bl_no') ?></b></td>
            <td class="text-right"><?= $requisition_info->bl_no ?></td>
        </tr>
        <tr>
            <td style="text-align:left;"><b><?= lang('insurance_covernote') ?></b></td>
            <td><?= $requisition_info->insurance_covernote ?></td>
            <td><b><?= lang('insurance') ?> Amount</b></td>
            <td class="text-right">
                    <?php
                    if($requisition_info->insurance_value != 0.00){
                        echo ($requisition_info->insurance_type == 'Amount')?'PKR '.number_format($requisition_info->insurance_value,2):round($requisition_info->insurance_value,2)." %";
                    }
                    ?>
            </td>
        </tr>
        <tr>
            <td style="text-align:left;"><b><?= lang('invoice_value') ?></b></td>
            <td><?= number_format($this->invoice_model->get_invoice_value($requisition_info->invoices_id),2); ?></td>
            <td><b><?= lang('currency') ?></b></td>
            <td class="text-right"><?= $requisition_info->currency ?></td>
        </tr>
        <tr>
            <td style="text-align:left;"><b><?= lang('exchange_rate') ?></b></td>
            <td><?= number_format($requisition_info->exchange_rate,2)  ?></td>
            <td><b><?= lang('assessable_value') ?></b></td>
            <td class="text-right">PKR <?= ($requisition_info->requisition_type == 'Manual')?$requisition_info->invoice_total:number_format($this->invoice_model->calculate_duty('invoice_total', $requisition_info->custom_duty_id),2); ?></td>
        </tr>
        <tr>
            <td style="text-align:left;"><b><?= lang('ioco_item') ?></b></td>
            <td><?= $requisition_info->ioco_item ?></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <table style="width:54%;float:left;">
        <thead>
        <tr style="background:#ccc;">
            <th colspan="2" style="text-align:left;">Custom Dues</th>
            <th style="text-align:right;">Amount (Rs.)</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><b><?= lang('cd') ?></b></td>
            <td class="text-right"><?= ($requisition_info->duty_cd != 0.00)? round($requisition_info->duty_cd, 2).' %':'' ?></td>
            <td class="text-right">
                <?php
                if($requisition_info->duty_cd_sum != 0.00){
                    echo($requisition_info->requisition_type == 'Manual')?round($requisition_info->duty_cd_sum,2):$this->invoice_model->calculate_duty('invoice_cd', $requisition_info->requisition_id);
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><b><?= lang('acd') ?></b></td>
            <td class="text-right"><?= ($requisition_info->duty_acd != 0.00)?round($requisition_info->duty_acd,2).' %':'' ?></td>
            <td class="text-right">
                <?php
                if($requisition_info->duty_acd_sum != 0.00){
                    echo($requisition_info->requisition_type == 'Manual')?round($requisition_info->duty_acd_sum,2):$this->invoice_model->calculate_duty('invoice_acd', $requisition_info->requisition_id);
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><b><?= lang('rd') ?></b></td>
            <td class="text-right"><?= ($requisition_info->duty_rd != 0.00)?round($requisition_info->duty_rd,2).' %':'' ?></td>
            <td class="text-right">
                <?php
                if($requisition_info->duty_rd_sum != 0.00){
                    echo($requisition_info->requisition_type == 'Manual')?round($requisition_info->duty_rd_sum,2):$this->invoice_model->calculate_duty('invoice_rd', $requisition_info->requisition_id);
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><b><?= lang('st') ?></b></td>
            <td class="text-right"><?= ($requisition_info->duty_st != 0.00)?round($requisition_info->duty_st,2).' %':'' ?></td>
            <td class="text-right">
                <?php
                if($requisition_info->duty_st_sum != 0.00){
                    echo($requisition_info->requisition_type == 'Manual')?round($requisition_info->duty_st_sum,2):$this->invoice_model->calculate_duty('invoice_st', $requisition_info->requisition_id);
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><b><?= lang('ast') ?></b></td>
            <td class="text-right"><?= ($requisition_info->duty_ast != 0.00)?round($requisition_info->duty_ast,2).' %':'' ?></td>
            <td class="text-right">
                <?php
                if($requisition_info->duty_ast_sum != 0.00){
                    echo($requisition_info->requisition_type == 'Manual')?round($requisition_info->duty_ast_sum,2):$this->invoice_model->calculate_duty('invoice_ast', $requisition_info->requisition_id);
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><b><?= lang('it') ?></b></td>
            <td class="text-right"><?= ($requisition_info->duty_it != 0.00)?round($requisition_info->duty_it,2).' %':'' ?></td>
            <td class="text-right">
                <?php
                if($requisition_info->duty_it_sum != 0.00){
                    echo($requisition_info->requisition_type == 'Manual')?round($requisition_info->duty_it_sum,2):$this->invoice_model->calculate_duty('invoice_it', $requisition_info->requisition_id);
                }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: left;" class="grand total"><b>Total Duties</b></td>
            <td class="total">PKR <?= number_format($this->invoice_model->calculate_duty('invoice_total_duties', $requisition_info->requisition_id),2); ?></td>
        </tr>
        </tbody>
    </table>
    <!---- OTHER EXPENSES ---->
    <table style="width:86%;margin-left:300px;">
        <thead>
            <tr style="background:#ccc;">
                <th style="text-align:left;">Other Expenses</th>
                <th style="text-align:right;">Amount (Rs.)</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $total_other_expenses=0;
            $expenses = $this->invoice_model->check_by_all(array('requisition_id'=>$requisition_info->requisition_id), 'tbl_advances');
            if(!empty($expenses)){
                foreach($expenses as $expense) {
                    if($expense->advance_amount == 0){

                    }else {
                        ?>
                        <tr>
                            <td style="text-align:right;"><b><?= read_subHead($expense->account_id)->A_NAME ?></b></td>
                            <td><?= number_format(($expense->advance_amount), 2);
                                $total_other_expenses+=$expense->advance_amount;
                            ?></td>
                        </tr>
                        <?php
                    }
                }
            }
            ?>
        <tr>
            <td class="grand total" style="text-align: left;"><b><?= lang('total_other_expenses') ?></b></td>
            <td class="text-right total">PKR <?= number_format($total_other_expenses,2); ?></td>
        </tr>
        </tbody>
    </table>
    <table style="margin-top:20px;">
        <tr>
            <td style="text-align: left">In Words: <b><?= $this->invoice_model->convert_number($this->invoice_model->calculate_duty('grand_total', $requisition_info->requisition_id)); ?></b></td>
            <td class="grand total"><b>GRAND TOTAL</b></td>
            <td class="grand total"><b>PKR <?= number_format($this->invoice_model->calculate_duty('grand_total', $requisition_info->requisition_id)+$total_other_expenses,2); ?></b></td>
        </tr>
    </table>
</main>
<footer>
    <?= config_item('company_address') ?>, <?= config_item('company_city') ?>-<?= config_item('company_zip_code') ?><br />
    PHONE: <?= config_item('company_phone') ?><br />
    EMAIL: <?= config_item('company_email') ?><br />
    WEB: <?= config_item('company_domain') ?><br />
</footer>
</body>
</html>
