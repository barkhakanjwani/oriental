<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html lang=en>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title><?= lang('requisition') ?></title>
    <style>
    
		body {
			background: white;
			color: black;
			margin: 0px;
		}
		#h11 {
			color: #000;
			background: none;
			font-weight: bold;
			font-size: 2.5em;
			margin: 10px 0 0 0;
			text-align: center;
			font-size:25px;
		}
		
		@media print {
			input#toggler, .toolbar, { display: none; }
			html, body {
				/*height:100vh;*/
				margin: 0 !important;
				padding: 0 !important;
				overflow: hidden;
				font-size: 10px;
			}
			tr{
				border:2px solid;
			}
			td {
				height:1px;
				font-family: 'bitstream cyberbit', sans-serif;
			}
			th{
				background-color: #333333;
				color: white;
				-webkit-print-color-adjust: exact;
				text-align:center;
                padding:3px;
            }
		}
		table { width: 100%; }
    </style>
	  <style>
            /** Define the margins of your page **/
			@page {
				margin: 100px 25px;
			}
			
			footer {
				position: fixed;
				bottom: -60px;
				left: 0px;
				right: 0px;
				height: 20px;
			}
        </style>
    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
<body>
<?php
    $client_info = $this->invoice_model->check_by(array('client_id' => $requisition_info->client_id), 'tbl_client');
?>
<h1 id="h11"><?= config_item('company_name') ?></h1><br>
<p style="text-align: center;">
    <?php
        if ($client_info->client_status == 1) {
            $status = 'Person';
        } else {
            $status = 'Company';
        }
    ?>
	<span style="font-size:15px;">
    	<?= config_item('company_address') ?>
        <?= config_item('company_city') ?>, <?= config_item('company_country') ?> <br />
        Phone: <?= config_item('company_phone') ?><br />
        Email: <?= config_item('company_email') ?><br />
        Web: <?= config_item('company_domain') ?><br />
        <span>
</p>
<div style="margin-top: 1%;">
    <div class="col-xs-12">
		<table style="border: 1px solid black;">
			<thead style="font-size: 12px">
                <tr>
                    <td style="border: 1px solid black;"><b>Total Value:</b> <?= $requisition_info->currency." ".number_format($this->invoice_model->get_invoice_value($requisition_info->invoices_id), 2) ?> </td>
					<td style="border: 1px solid black;"><b>Freight Amount:</b> <?= ($financial_info->incoterm == 1)?$financial_info->freight:'' ?> </td>
                </tr>
                <tr>
                    <td>&nbsp;<b>Ex-Rate :</b> <?= $requisition_info->currency." ".number_format($requisition_info->exchange_rate,2) ?></td>
				   <td></td>
                </tr>
            </thead>
		</table>
        <table style="border: 1px solid black;">
            <?php
                if(!empty($custom_duty_info)) {
                    $count=1;
                    $total_invoice_value = 0;
                    $total_invoice_value_pkr = 0;
                    foreach ($custom_duty_info as $duty_info) {
                        $invoice_value = $duty_info->unit_value_assessed*$duty_info->no_of_units;
                        $invoice_value_pkr = $duty_info->unit_value*$duty_info->no_of_units*$requisition_info->exchange_rate;
                        $invoice_value_pkr = ($duty_info->insurance_type == 'Percent')?$invoice_value_pkr/100*$duty_info->insurance_value+$invoice_value_pkr:$invoice_value_pkr+$duty_info->insurance_value;
                        $invoice_value_pkr = ($duty_info->landing_charges == 'Yes')?$invoice_value_pkr/100*1+$invoice_value_pkr:0;
                        $total_invoice_value += $invoice_value;
                        $total_invoice_value_pkr += $invoice_value_pkr;
                        ?>
			  <tbody>
					<tr style="border: 1px solid black;">
						<th style="text-align: center;border: 1px solid black;">&nbsp;SR&nbsp;</th>
						<th style="text-align: center;border: 1px solid black;">&nbsp;HS Code&nbsp;</th>
						<th style="text-align: center;border: 1px solid black;">Item</th>
						<th style="text-align: center;border: 1px solid black;">Currency</th>
						<th style="text-align: center;border: 1px solid black;">PKR</th>
						<th style="text-align: center;border: 1px solid black;">Levy</th>
						<th style="text-align: center;border: 1px solid black;">Rate</th>
						<th style="text-align: center;border: 1px solid black;">Amount</th>
					</tr>
                <tr style="border: 1px solid black;">
                    <td style="border: 1px solid black;text-align: center;" rowspan="7"><?= $count ?></td>
                    <td style="border: 1px solid black;text-align: center;" rowspan="7"><?= $duty_info->hs_code ?></td>
                    <td style="border: 1px solid black;text-align: center;" rowspan="7"><?= $duty_info->commodity ?></td>
                    <td style="border: 1px solid black;text-align: center;" rowspan="7"><?= $requisition_info->currency." ".$invoice_value ?></td>
                    <td style="border: 1px solid black;text-align: center;" rowspan="7">PKR <?= number_format($invoice_value_pkr,2) ?></td>
                    <td style="border: 1px solid black;" >&nbsp;<b><?= lang('cd') ?></b></td>
                    <td style="border: 1px solid black;text-align: center;" ><?= ($duty_info->duty_cd != 0.00)? round($duty_info->duty_cd, 2).' %':'' ?></td>
                    <td style="border: 1px solid black;text-align: right;">PKR <?= ($requisition_info->requisition_type == 'Declared')?number_format($this->invoice_model->calculate_duty('invoice_cd',$duty_info->custom_duty_id),2):number_format($this->invoice_model->calculate_duty('assessable_cd',$duty_info->commodity_id),2) ?>&nbsp;</td>
                </tr>
                <tr style="border: 1px solid black;">
                    <td style="border: 1px solid black;">&nbsp;<b><?= lang('acd') ?></b></td>
                    <td style="border: 1px solid black;text-align: center;"><?= ($duty_info->duty_acd != 0.00)?round($duty_info->duty_acd,2).' %':'' ?></td>
                    <td style="border: 1px solid black;text-align: right;">PKR <?= ($requisition_info->requisition_type == 'Declared')?number_format($this->invoice_model->calculate_duty('invoice_acd',$duty_info->custom_duty_id),2):number_format($this->invoice_model->calculate_duty('assessable_acd',$duty_info->commodity_id),2) ?>&nbsp;</td>
                </tr>
                <tr style="border: 1px solid black;">
                    <td style="border: 1px solid black;">&nbsp;<b><?= lang('rd') ?></b></td>
                    <td style="border: 1px solid black;text-align: center;"><?= ($duty_info->duty_acd != 0.00)?round($duty_info->duty_rd,2).' %':'' ?></td>
                    <td style="border: 1px solid black;text-align: right;">PKR <?= ($requisition_info->requisition_type == 'Declared')?number_format($this->invoice_model->calculate_duty('invoice_rd',$duty_info->custom_duty_id),2):number_format($this->invoice_model->calculate_duty('assessable_rd',$duty_info->commodity_id),2) ?>&nbsp;</td>
                </tr>
                <tr style="border: 1px solid black;">
                    <td style="border: 1px solid black;">&nbsp;<b><?= lang('st') ?></b></td>
                    <td style="border: 1px solid black;text-align: center;"><?= ($duty_info->duty_acd != 0.00)?round($duty_info->duty_st,2).' %':'' ?></td>
                    <td style="border: 1px solid black;text-align: right;">PKR <?= ($requisition_info->requisition_type == 'Declared')?number_format($this->invoice_model->calculate_duty('invoice_st',$duty_info->custom_duty_id),2):number_format($this->invoice_model->calculate_duty('assessable_st',$duty_info->commodity_id),2) ?>&nbsp;</td>
                </tr>
                <tr style="border: 1px solid black;">
                    <td style="border: 1px solid black;">&nbsp;<b><?= lang('ast') ?></b></td>
                    <td style="border: 1px solid black;text-align: center;"><?= ($duty_info->duty_acd != 0.00)?round($duty_info->duty_ast,2).' %':'' ?></td>
                    <td style="border: 1px solid black;text-align: right;">PKR <?= ($requisition_info->requisition_type == 'Declared')?number_format($this->invoice_model->calculate_duty('invoice_ast',$duty_info->custom_duty_id),2):number_format($this->invoice_model->calculate_duty('assessable_ast',$duty_info->commodity_id),2) ?>&nbsp;</td>
                </tr>
                <tr style="border: 1px solid black;">
                    <td style="border: 1px solid black;">&nbsp;<b><?= lang('it') ?></b></td>
                    <td style="border: 1px solid black;text-align: center;"><?= ($duty_info->duty_acd != 0.00)?round($duty_info->duty_it,2).' %':'' ?></td>
                    <td style="border: 1px solid black;text-align: right;">PKR <?= ($requisition_info->requisition_type == 'Declared')?number_format($this->invoice_model->calculate_duty('invoice_it',$duty_info->custom_duty_id),2):number_format($this->invoice_model->calculate_duty('assessable_it',$duty_info->commodity_id),2) ?>&nbsp;</td>
                </tr>
                <tr style="border: 1px solid black;">
                    <td>&nbsp;<b><?= lang('total_duties') ?></b></td>
                    <td></td>
                    <td style="border: 1px solid black;text-align: right;">PKR <?= ($requisition_info->requisition_type == 'Declared')?number_format($this->invoice_model->calculate_duty('declared_duty_total',$duty_info->custom_duty_id),2):number_format($this->invoice_model->calculate_duty('assessable_duty_total',$duty_info->commodity_id),2) ?>&nbsp;</td>
                </tr>
                        <?php
                        $count++;
                    }
                    ?>
				<tr style="border: 1px solid black;">
					<td colspan="3"><b><?= lang('total') ?></b></td>
					<td class="text-center"><b><?= $requisition_info->currency." ".number_format($total_invoice_value,2) ?></b></td>
					<td class="text-center"><b><?= "PKR ".number_format($total_invoice_value_pkr,2) ?></b></td>
					<td colspan="4" class="text-right"><b><?= "PKR ".number_format($this->invoice_model->calculate_duty('declared_grand_total_duties', $requisition_info->requisition_id),2) ?></b></td>
				</tr>
					</tbody>
			<?php
                }
            ?>
        </table>
    </div>
</div>
</body>
</html>