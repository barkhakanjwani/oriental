<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">

<html lang=en>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Invoice</title>

<style>

html/*{margin:3px 15px !important;*/}

body {

  background: white;

  color: black;

  font-family: 'bitstream cyberbit', sans-serif;

  margin: 0px;

}





#h11 {

  color: #000;

  background: none;

  font-weight: bold;

  font-size: 2.5em;

  margin: 10px 0 0 0;

  text-align: center;

  font-size:32px;

}

#h12

{

  color: #000;

  background: none;

  font-weight: bold;

  font-size: 2em;

  margin: 10px 0 0 0;

  text-align: center;

  font-size:25px;

}



h2 {

  color: #00008b;

  background: none;

  font-weight: bold

}



h3 {

  color: #000;

  background: none;

  margin-left: 4%;

  margin-right: 4%;

  font-weight: bold;

  margin: 10px 0 0 0;

  text-align: center;

}



h4 {

  margin-left: 6%;

  margin-right: 6%;

  font-weight: bold

}



h5 {

  margin-left: 6%;

  margin-right: 6%;

  font-weight: bold

}



ul, ol, dl, p {

  margin-left: 6%;

  margin-right: 6%

}



ul ul, table ol, table ul, dl ol, dl ul {

  margin-left: 1.2em;

  margin-right: 1%;

  padding-left: 0

}



pre {

  margin-left: 10%;

  white-space: pre

}



table caption {

  font-size: larger;

  font-weight: bolder

}



table p, table dl, ol p, ul p, dl p, blockquote p, .note p, .note ul, .note ol, .note dl, li pre, dd pre {

  margin-left: 2%;

  margin-right: 2%;

}

td

{

align: right;

}



p.top {

  margin-left: 1%;

  margin-right: 1%

}



blockquote {

  margin-left: 8%;

  margin-right: 8%;

  border: thin ridge #dc143c

}



blockquote pre {

  margin-left: 1%;

  margin-right: 1%

}





.css {

  color: #800000;

  background: none

}



.javascript {

  color: #008000;

  background: none

}



.example { margin-left: 10% }



dfn {

  font-style: normal;

  font-weight: bolder

}



var sub { font-style: normal }



.note {

  font-size: 85%;

  margin-left: 10%

}



.SMA {

  color: fuchsia;

  background: none;

  font-family: Kids, "Comic Sans MS", Jester

}



.oops {

  font-family: Jester, "Comic Sans MS"

}



.author {

  font-style: italic

}



.copyright {

  font-size: smaller;

  text-align: right;

  clear: right

}



.toolbar {

  text-align: center

}



.toolbar IMG {

  float: right

}



.error {

  color: #DC143C;

  background: none;

  text-decoration: none

}



.warning {

  color: #FF4500;

  background: none;

  text-decoration: none

}



.error strong {

  color: #DC143C;

  background: #FFD700;

  text-decoration: none

}



.warning strong {

  color: #FF4500;

  background: #FFD700;

  text-decoration: none

}





colgroup.entity { text-align: center }



.default { text-decoration: underline; font-style: normal }

.required { font-weight: bold }

td li.transitional, .elements li.transitional {

  font-weight: lighter;

  color: #696969;

  background: none

}

td li.frameset, .elements li.frameset {

  font-weight: lighter;

  color: #808080;

  background: none

}



.footer, .checkedDocument {

 

  width:93%;

  float:centre;

  margin-left:3%;

  border-top: solid thin black

}



strong.legal {

  font-weight: normal;

  text-transform: uppercase

}



@media print {

  input#toggler, .toolbar { display: none }

}



table { width: 100%;  border: 0.5px solid black;}

td { border: 0.5px solid black; height:1px;



 }

 #thid2

 {

font-size:11px; 

 }

#thid { border: 1px solid black; font-size:13px;}

#tableid

{

font-size:12px;

}

</style>

<meta name="author" content="">

<meta name="description" content="">

<meta name="keywords" content="">

<body style="margin-left:3%; margin-right:3%;">

        <?php

        $client_info = $this->invoice_model->check_by(array('client_id' => $invoice_info->client_id), 'tbl_client');

        $client_lang = $client_info->language;

        $language_info = $this->lang->load('en_lang', $client_lang, TRUE, FALSE, '', TRUE);

        ?> 

<h1 id="h11"> <img style="width: 60px;width: 60px;margin-top: 0px;margin-right: 10px;" src="<?= config_item('invoice_logo') ?>" > <?= config_item('company_name') ?></h1><br><br>

<center><strong ><?= (config_item('company_legal_name_' . $client_lang) ? config_item('company_legal_name_' . $client_lang) : config_item('company_legal_name')) ?></strong></center>



<div class=footer>

</div>

<p style="text-align: center;">

<?php

if ($client_info->client_status == 1) {

    $status = 'Person';

} else {

    $status = 'Company';

}

?>

<!--<h1 style="font-size:11px;">-->                                    

<!--<strong><?= ucfirst($client_info->name . ' (' . $status . ')') ?></strong> -->

<span style="font-size:11px;">

<?= ucfirst($client_info->address) ?> &nbsp;

<?= ucfirst($client_info->city) ?> &nbsp;

<!--<?= ucfirst($client_info->country) ?> <br/>-->

<!--<?= $language_info['phone'] ?>: <a href="tel:<?= ucfirst($client_info->phone) ?>"><?= ucfirst($client_info->phone) ?></a>--><span>

</p><!--<h1>-->

<br>

<center><u id="h12">INVOICE</u></center><br>

<br>

<div style="margin-left:3%; margin-right:3%;">

<table style=" border:none" id="tableid">

  <thead>

  </thead>

  <tr style="border:none">

    <td scope=row style="border:none" width="300px"><strong><?= $language_info['invoice_date'] ?> : </strong> <?= strftime(config_item('date_format'), strtotime($invoice_info->created_date)); ?></td>

    <td scope=row style="text-align: left; border:none"><strong>LC / No : </strong> <?= $invoice_info->lc_no ?></td>

    <td  align="right" scope=row style="border:none"><strong>Job # : </strong> <?= $this->invoice_model->job_no_creation($invoice_info->invoices_id) ?></td>

    </tr>

  <tr>

  

  </tr>

  </table>

  <table id="tableid" style="border-bottom:none;"><!--

      <tr>

    <th scope=row></th>

    <th scope=row></th>

    <th scope=row></th>

  </tr>-->

  <tr><td scope=col colspan="3" ><?= ucfirst($client_info->name . ' (' . $status . ')') ?></td></tr>

    <tr>

    <td scope=row width="240px"></td>

    <td scope=row><strong><?= $language_info['packages'] ?> : </strong> <?= $invoice_info->packages ?></td>

    <td scope=row><strong><?= $language_info['gross_weight'] ?> : </strong> <?= $invoice_info->gross_weight ?></td>

    </tr>

<tr>

    <td scope=row><strong><?= $language_info['net_weight'] ?> : </strong> <?= $invoice_info->net_weight ?></td>

    <td scope=row><strong><?= $language_info['igm'] ?> : </strong> <?= $invoice_info->igm_no ?></td>

    <td scope=row><strong><?= $language_info['igm_date'] ?> : </strong> 

	<?php

		if(!empty($invoice_info->igm_date)){

			strftime(config_item('date_format'), strtotime($invoice_info->igm_date)); 

		}

	?>

	</td>

</tr>

<tr>

    <td scope=row><strong><?= $language_info['index'] ?> : </strong> <?= $invoice_info->index_no ?></td>

    <td scope=row><strong></td>

    <td scope=row><strong>Date : </strong> 

	<?php

		if(!empty($invoice_info->index_date)){

			strftime(config_item('date_format'), strtotime($invoice_info->index_date)); 

		}

	?>

	</td>

  </tr>

  </table>

  <table style="border-top:none;" id="tableid">

    <!--<tr>

    

      <th scope=row></th>

      <th scope=row></th>

     

    </tr>-->

    <tr>

    <td scope=row width="240px"><strong><?= $language_info['bl'] ?> : </strong> <?= $invoice_info->bl_no ?></td>

    <td scope=row><strong></td>

    </tr>

  </table>



<br>



</div>



</body>

</html>

