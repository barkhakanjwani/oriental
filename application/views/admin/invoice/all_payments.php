<?= message_box('success') ?>
<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('all_payments') ?></h1>
<!--<section class="panel panel-default">
    <header class="panel-heading"><?/*= lang('all_payments') */?> </header>
    <div class="panel-body">
        <table class="table table-striped DataTables " id="DataTables">
            <thead>
                <tr>
                    <th><?/*= lang('payment_date') */?></th>
                    <th><?/*= lang('created_date') */?></th>
                    <th><?/*= lang('reference_no') */?></th>
                    <th><?/*= lang('client') */?></th>
                    <th><?/*= lang('amount') */?></th>
                    <th><?/*= lang('received_by') */?></th>
                    <th><?/*= lang('payment_mode') */?></th>
                    <th ><?/*= lang('action') */?></th>
                </tr>
            </thead>
            <tbody>
                <?php
/*                if (!empty($all_payments_info)) {
                    foreach ($all_payments_info as $v_payments_info) {
                        */?>
                        <tr>
                            <?php
/*                                $invoice_info = $this->invoice_model->check_by(array('invoices_id' => $v_payments_info->invoices_id), 'tbl_invoices');
                                $client_info = $this->invoice_model->check_by(array('client_id' => $invoice_info->client_id), 'tbl_client');
                            */?>
                            <td><?/*= strftime(config_item('date_format'), strtotime($v_payments_info->payment_date)); */?></td>
                            <td><?/*= strftime(config_item('date_format'), strtotime($v_payments_info->created_date)) */?></td>
                            <td><a class="text-info" href="<?/*= base_url() */?>admin/invoice/manage_invoice/invoice_details/<?/*= encode($v_payments_info->invoices_id) */?>"><?/*= $this->invoice_model->job_no_creation($invoice_info->invoices_id); */?></a></td>
                            <td><?/*= $client_info->name; */?></td>
                            <td>PKR <?/*= number_format($v_payments_info->amount, 2) */?></td>
                            <td><?/*= $v_payments_info->received_by */?></td>
                            <td><?/*= ($v_payments_info->payment_type == 1)?'Cash':'Bank' */?></td>
                            <td>
                                <?/*= btn_edit('admin/invoice/all_payments/' . encode($v_payments_info->payments_id)) */?>
                                <?/*= btn_view('admin/invoice/manage_invoice/payments_details/' . encode($v_payments_info->payments_id)."/".encode($v_payments_info->invoices_id)) */?>
                                <?/*= btn_delete('admin/invoice/delete/delete_payment/' . encrypt($v_payments_info->payments_id)) */?>
                            </td>
                        </tr>
                        <?php
/*                    }
                }
                */?>
            </tbody>
        </table>
    </div>
</section>-->

<section class="panel panel-default">

    <header class="panel-heading"><?= lang('all_payments') ?></header>

    <div class="panel-body" id="export_table">

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive" id="customers">
                    <table class="table table-bordered data-table-list dtHorizontalExample" id="tab_customers">
                        <thead>
                            <th><?= lang('payment_date') ?></th>
                            <th><?= lang('created_date') ?></th>
                            <th><?= lang('reference_no') ?></th>
                            <th><?= lang('client') ?></th>
                            <th><?= lang('amount') ?></th>
                            <th><?= lang('received_by') ?></th>
                            <th><?= lang('payment_mode') ?></th>
                            <th ><?= lang('action') ?></th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

</section>

<script>
    var data_url='<?= base_url('admin/invoice/all_payment') ?>';
</script>