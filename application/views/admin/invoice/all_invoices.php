<?= message_box('success') ?>
<?php echo form_open(base_url('admin/invoice/all_invoices'),array('class'=>"form-horizontal")); ?>

<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-default">
            <header class="panel-heading  "><?= lang('search_by'); ?></header>
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-xs-2">
                        <select class="form-control select_box client" style="width: 100%"  name="client_id">
                            <option value="">Choose Client</option>
                            <?php
                            if (!empty($all_client)) {
                                foreach ($all_client as $v_client) {
                                    ?>
                                    <option value="<?= $v_client->client_id ?>" <?php
                                    if(isset($client_id)){
                                        echo ($v_client->client_id == $client_id)?'selected':'';
                                    }  ?>><?= $v_client->name ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-xs-2">
                        <input type="text" class="form-control" name="reference_no" placeholder="<?= lang('reference_no') ?>" value="<?= (!empty($reference_no))?$reference_no:'' ?>" />
                    </div>
                    <div class="col-xs-2">
                        <input type="text" class="form-control" name="bl_no" placeholder="<?= lang('bl_no') ?>" value="<?= (!empty($bl_no))?$bl_no:'' ?>" />
                    </div>
                    <div class="col-xs-2">
                        <input type="text" class="form-control" name="commodity" placeholder="<?= lang('commodity') ?>" value="<?= (!empty($commodity))?$commodity:'' ?>" />
                    </div>
                    <div class="col-xs-2">
                        <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> <?= lang('search') ?></button>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php echo form_close(); ?>
<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('all_jobs') ?></h1>
<!--<section class="panel panel-default">
    <header class="panel-heading"><?/*= $title; */?> </header>
    <div class="panel-body">
        <table class="table table-striped DataTables " id="DataTables">
            <thead>
            <tr>
                <th><?/*= lang('job_no') */?></th>
                <th class="col-date"><?/*= lang('created_date') */?></th>
                <th class="col-date"><?/*= lang('due_date') */?></th>
                <th><?/*= lang('client_name') */?></th>
                <th class="col-currency"><?/*= lang('amount') */?></th>
                <th class="col-currency"><?/*= lang('due_amount') */?></th>
                <th><?/*= lang('status') */?></th>
                <th class="col-options no-sort" ><?/*= lang('action') */?></th>
            </tr>
            </thead>
            <tbody>
            <?php
/*            foreach($all_invoices as $v_invoices){
                $bill_info = $this->invoice_model->check_by(array('invoices_id' => $v_invoices->invoices_id), 'tbl_bills');
                if($this->invoice_model->get_payment_status($v_invoices->invoices_id) == lang('fully_paid')){
                    $invoice_status = lang('fully_paid');
                    $label = "success";
                }
                elseif($this->invoice_model->get_payment_status($v_invoices->invoices_id) == lang('partially_paid')){
                    $invoice_status = lang('partially_paid');
                    $label = "info";
                }else{
                    $invoice_status = lang('not_paid');
                    $label = "danger";
                }
                */?>
                <tr>
                    <td><a class="text-info" href="<?/*= base_url() */?>admin/invoice/manage_invoice/invoice_details/<?/*= encode($v_invoices->invoices_id) */?>"><?/*= $this->invoice_model->job_no_creation($v_invoices->invoices_id) */?></a></td>
                    <td><?/*= strftime(config_item('date_format'), strtotime($v_invoices->created_date)) */?></td>
                    <td>
                        <?php
/*                        if(!empty($v_invoices->due_date)){
                            echo strftime(config_item('date_format'), strtotime($v_invoices->due_date));
                        }
                        */?>
                    </td>
                    <?php
/*                    $client_info = $this->invoice_model->check_by(array('client_id' => $v_invoices->client_id), 'tbl_client');
                    */?>
                    <td><?/*= $client_info->name */?></td>
                    <td>
                        <?php
/*                        if(!empty($bill_info)){
                            echo "PKR ".number_format($this->invoice_model->calculate_bill('bill_grand_total', $bill_info->bill_id),2);
                        }
                        */?>
                    </td>
                    <td>
                        <?php
/*                        if(!empty($bill_info)){
                            echo "PKR ".number_format($this->invoice_model->calculate_to('invoice_due', $v_invoices->invoices_id),2);
                        }
                        */?>
                    </td>
                    <td><label class="label label-<?/*= $label */?>"><?/*= $invoice_status */?></label></td>
                    <td>
                        <?php
/*                            echo btn_edit('admin/invoice/manage_invoice/create_invoice/' . encode($v_invoices->invoices_id))." ";
                            echo btn_view('admin/invoice/manage_invoice/invoice_details/' . encode($v_invoices->invoices_id));
                            */?>
                    </td>
                </tr>
                <?php
/*            }
            */?>
            </tbody>
        </table>
    </div>
</section>-->

<input type="hidden" id="client_id" value="<?= (!empty($client_id))?$client_id:'All' ?>">
<input type="hidden" id="reference_no" value="<?= (!empty($reference_no))?$reference_no:'All' ?>">
<input type="hidden" id="bl_no" value="<?= (!empty($bl_no))?$bl_no:'All' ?>">
<input type="hidden" id="commodity" value="<?= (!empty($commodities))?$commodities:'All' ?>">
<section class="panel panel-default">
    <header class="panel-heading"><?= $title; ?></header>
    <div class="panel-body" id="export_table">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered data-table-list">
                        <thead>
                        <th><?= lang('job_no') ?></th>
                        <th><?= lang('client_name') ?></th>
                        <th><?= lang('amount') ?></th>
                        <th><?= lang('due_amount') ?></th>
                        <th><?= lang('status') ?></th>
                        <th><?= lang('action') ?></th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    var client_id=$('#client_id').val();
    var reference_no=$('#reference_no').val();
    var bl_no=$('#bl_no').val();
    var commodity=$('#commodity').val();
    var data_url='<?= base_url('admin/invoice/all_invoice/') ?>'+client_id+'/'+reference_no+'/'+bl_no+'/'+commodity;
</script>