<?= message_box('success') ?>
<?php echo form_open(base_url('admin/invoice/all_delivery'),array('class'=>"form-horizontal")); ?>

<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-default">
            <header class="panel-heading  "><?= lang('search_by'); ?></header>
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-xs-2">
                        <select class="form-control select_box client" style="width: 100%"  name="client_id">
                            <option value="">Choose Client</option>
                            <?php
                            if (!empty($all_client)) {
                                foreach ($all_client as $v_client) {
                                    ?>
                                    <option value="<?= $v_client->client_id ?>" <?php
                                    if(isset($client_id)){
                                        echo ($v_client->client_id == $client_id)?'selected':'';
                                    }  ?>><?= $v_client->name ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-xs-2">
                        <input type="text" class="form-control" name="reference_no" placeholder="<?= lang('reference_no') ?>" value="<?= (!empty($reference_no))?$reference_no:'' ?>" />
                    </div>
                    <div class="col-xs-2">
                        <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> <?= lang('search') ?></button>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php echo form_close(); ?>
<h1 class="header-<?= config_item('sidebar_theme'); ?>">List Delivery Section</h1>

<input type="hidden" id="client_id" value="<?= (!empty($client_id))?$client_id:'All' ?>">
<input type="hidden" id="reference_no" value="<?= (!empty($reference_no))?$reference_no:'All' ?>">
<section class="panel panel-default">
    <header class="panel-heading"><?= $title; ?></header>
    <div class="panel-body" id="export_table">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered data-table-list">
                        <thead>
                        <th><?= lang('job_no') ?></th>
                        <th><?= lang('client_name') ?></th>
                        <th><?= lang('action') ?></th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    var client_id=$('#client_id').val();
    var reference_no=$('#reference_no').val();
    var data_url='<?= base_url('admin/invoice/all_deliveries/') ?>'+client_id+'/'+reference_no;
</script>