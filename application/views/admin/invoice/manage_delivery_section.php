<style>
   .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
	   vertical-align: middle;
   }
</style>
<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php
    if (!empty($this->session->flashdata('message_name'))) {
        
        ?>
		<div class="callout callout-success">
   <h4><?= $this->session->flashdata('message_name') ?></h4>
   <p><?= lang('invoice_created') ?></p>
</div>
        <?php
    }

?>
<?php
    if (!empty($this->session->flashdata('message_error'))) {
        
        ?>
		<div class="callout callout-danger">
   <h4><?= $this->session->flashdata('message_error') ?></h4>
   <p>Job Number already exist.</p>
</div>
        <?php
    }
        ?>
<div class="row">
   <div class="col-lg-12">
      <form role="form" enctype="multipart/form-data" id="form"
			action="<?php echo base_url(); ?>admin/invoice/save_delivery_section/<?php
                if (!empty($invoice_info)) {
                    echo $invoice_info->invoices_id;
                }
            ?>" method="post" class="form-horizontal">
         <h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= (!empty($invoice_info))?lang('edit_job'):lang('enter_job') ?> <span class="text-danger" style="font-size:12px;text-transform: lowercase">Fields with (*) are required</span></h1>
         <section class="panel panel-default">
            <header class="panel-heading  "><?= $title ?></header>
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-lg-10">
                        <div class="col-lg-2">
                            <label class="control-label">Job No</label>
                            <?php if (empty($invoice_info)) {
                                ?>
                                <select class="form-control select_box" name="job_no" id="job_no" onChange="selectConsignment(this.value);" data-width="100%" autocomplete="off">
                                    <option value="">---</option>
                                    <?php
                                    if (!empty($all_reference_no)) {
                                        foreach ($all_reference_no as $reference_no) {
                                            ?>
                                            <option value="<?= $reference_no->invoices_id ?>"><?= $reference_no->reference_no ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            <?php } else {
                                ?>
                                <input type="text" name="job_no" id="job_no" class="form-control" value="<?= $invoice_info->reference_no ?>"
                                       readonly/>
                                <?php
                            } ?>
                        </div>

                        <div id="consignment">
                            <div class="col-lg-2">
                                <label class="control-label">Consignment Type</label>
                                <input type="text" id="consignment_type" name="consignment_type" class="form-control"  readonly
                                       value="<?= (!empty($invoice_info))? $invoice_info->consignment_type : ''; ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <?php
                if(!empty($invoice_info)){
                    $consignment_type = $this->invoice_model->get_any_field('tbl_invoices', array('invoices_id' => $invoice_info->invoices_id), 'consignment_type');
                    $invoices_info = $this->invoice_model->check_by(array('invoices_id' => $invoice_info->invoices_id), 'tbl_invoices');
                    $container_info = $this->invoice_model->check_by_all(array('invoices_id' => $invoices_info->invoices_id), 'tbl_containers');
                    $delivery_info = [];
                    foreach ($container_info as $container)
                        $delivery_info += $this->invoice_model->check_by_all(array('IInvoicesId' => $container->invoices_id), 'stbdeliverysection');
                    $i=0;
                    $type = $consignment_type;
                    foreach ($container_info as $container) {

                        $shed_info = $this->invoice_model->check_by(array('shed_id' => $container->container_shed), 'tbl_sheds');
                        if($consignment_type == 'FCL'){
                            ?>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="col-lg-2">
                                        <input type="hidden" name="invoices_id[]" value="<?= $container->invoices_id ?>">
                                        <input type="hidden" name="container_id[]" value="<?= $container->id ?>">
                                        <label class="control-label">Container No</label>
                                        <input type="text" class="form-control" name="container_no[]" value="<?= $container->container_no ?>" readonly/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="control-label">Type</label>
                                        <input type="text" class="form-control" name="type[]" value="<?= $container->container_type ?>" readonly/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="control-label">Container ft</label>
                                        <input type="text" class="form-control" name="container_ft[]" value="<?= $container->container_ft ?>" readonly/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="control-label">No. of Pkgs</label>
                                        <input type="text" class="form-control" name="no_of_pkgs[]" value="<?= $container->packages ?>" readonly/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="control-label">Total Weight</label>
                                        <input type="text" class="form-control" name="total_weight[]" value="<?= $container->container_total_weight ?>" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="col-lg-2">
                                        <input type="hidden" name="Iid[]" value="<?= $delivery_info[$i]->Iid ?>"/>
                                        <label class="control-label">Truck No</label>
                                        <input type="text" class="form-control" name="truck_no[]" value="<?= $delivery_info[$i]->VCTruckNo?>"/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="control-label">Bilty</label>
                                        <input type="text" class="form-control" name="bilty[]" value="<?= $delivery_info[$i]->VCBilty?>"/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="control-label">Date of Delivery</label>
                                        <input type="date" class="form-control datepicker" name="delivery_date[]" value="<?= $delivery_info[$i]->DTDeliveryDate?>"/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="control-label">Date of Receipt at Site</label>
                                        <input type="date" class="form-control datepicker" name="receipt_site[]" value="<?= $delivery_info[$i]->DTDateReceiptSite?>"/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="control-label">Weight Declare</label>
                                        <input type="text" class="form-control" name="weight_declare[]" value="<?= $delivery_info[$i]->DCWeightDeclare?>"/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="control-label">Weight Found</label>
                                        <input type="text" class="form-control" name="weight_found[]" value="<?= $delivery_info[$i]->DCWeightFound?>"/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="control-label">Transporter</label>
                                        <input type="text" class="form-control" name="transporter[]" value="<?= $delivery_info[$i]->VCTransporter?>"/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="control-label">No of Pkgs Received</label>
                                        <input type="text" class="form-control" name="pkgs_receive[]" value="<?= $delivery_info[$i]->VCPkgsReceive?>"/>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="control-label">Remarks</label>
                                        <textarea class="form-control" name="remarks[]"><?= $delivery_info[$i]->TRemarks?></textarea>
                                    </div>
                                </div>
                            </div>
                    <?php
                        }elseif ($consignment_type == 'LCL'){
                    ?>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="col-lg-2">
                                        <input type="hidden" name="invoices_id[]" value="<?= $container->invoices_id ?>">
                                        <input type="hidden" name="container_id[]" value="<?=$container->id ?>">
                                        <label class="control-label">Shed</label>
                                        <input type="text" class="form-control" name="shed[]" value="<?= $shed_info->shed ?>" readonly/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="control-label">No. of Pkgs</label>
                                        <input type="text" class="form-control" name="no_of_pkgs[]" value="<?= $container->packages ?>" readonly/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="control-label">Total Weight</label>
                                        <input type="text" class="form-control" name="total_weight[]" value="<?=$container->container_total_weight ?>" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="col-lg-2">
                                        <input type="hidden" name="Iid[]" value="<?= $delivery_info[$i]->Iid ?>"/>
                                        <label class="control-label">Truck No</label>
                                        <input type="text" class="form-control" name="truck_no[]" value="<?= $delivery_info[$i]->VCTruckNo?>"/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="control-label">Bilty</label>
                                        <input type="text" class="form-control" name="bilty[]" value="<?= $delivery_info[$i]->VCBilty?>"/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="control-label">Date of Delivery</label>
                                        <input type="date" class="form-control datepicker" name="delivery_date[]" value="<?= $delivery_info[$i]->DTDeliveryDate?>"/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="control-label">Date of Receipt at Site</label>
                                        <input type="date" class="form-control datepicker" name="receipt_site[]" value="<?= $delivery_info[$i]->DTDateReceiptSite?>"/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="control-label">Weight Declare</label>
                                        <input type="text" class="form-control" name="weight_declare[]" value="<?= $delivery_info[$i]->DCWeightDeclare?>"/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="control-label">Weight Found</label>
                                        <input type="text" class="form-control" name="weight_found[]" value="<?= $delivery_info[$i]->DCWeightFound?>"/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="control-label">Transporter</label>
                                        <input type="text" class="form-control" name="transporter[]" value="<?= $delivery_info[$i]->VCTransporter?>"/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="control-label">No of Pkgs Received</label>
                                        <input type="text" class="form-control" name="pkgs_receive[]" value="<?= $delivery_info[$i]->VCPkgsReceive?>"/>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="control-label">Remarks</label>
                                        <textarea class="form-control" name="remarks[]"><?= $delivery_info[$i]->TRemarks?></textarea>
                                    </div>
                                </div>
                            </div>
                    <?php
                        }elseif ($consignment_type == 'Break Bulk'){
                    ?>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="col-lg-2">
                                    <input type="hidden" name="invoices_id[]" value="<?= $container->invoices_id ?>">
                                    <input type="hidden" name="container_id[]" value="<?= $container->id ?>">
                                    <label class="control-label">No. of Pkgs</label>
                                    <input type="text" class="form-control" name="no_of_pkgs[]" value="<?= $container->packages ?>" readonly/>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">Total Net Weight</label>
                                    <input type="text" class="form-control" name="total_net_weight[]" value="<?= $container->container_net_weight ?>" readonly/>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">Total Gross Weight</label>
                                    <input type="text" class="form-control" name="total_gross_weight[]" value="<?= $container->container_gross_weight ?>" readonly/>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="col-lg-2">
                                    <input type="hidden" name="Iid[]" value="<?= $delivery_info[$i]->Iid ?>"/>
                                    <label class="control-label">Truck No</label>
                                    <input type="text" class="form-control" name="truck_no[]" value="<?= $delivery_info[$i]->VCTruckNo?>"/>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">Bilty</label>
                                    <input type="text" class="form-control" name="bilty[]" value="<?= $delivery_info[$i]->VCBilty?>"/>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">Date of Delivery</label>
                                    <input type="date" class="form-control datepicker" name="delivery_date[]" value="<?= $delivery_info[$i]->DTDeliveryDate?>"/>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">Date of Receipt at Site</label>
                                    <input type="date" class="form-control datepicker" name="receipt_site[]" value="<?= $delivery_info[$i]->DTDateReceiptSite?>"/>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">Weight Declare</label>
                                    <input type="text" class="form-control" name="weight_declare[]" value="<?= $delivery_info[$i]->DCWeightDeclare?>"/>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">Weight Found</label>
                                    <input type="text" class="form-control" name="weight_found[]" value="<?= $delivery_info[$i]->DCWeightFound?>"/>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">Transporter</label>
                                    <input type="text" class="form-control" name="transporter[]" value="<?= $delivery_info[$i]->VCTransporter?>"/>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">No of Pkgs Received</label>
                                    <input type="text" class="form-control" name="pkgs_receive[]" value="<?= $delivery_info[$i]->VCPkgsReceive?>"/>
                                </div>
                                <div class="col-lg-4">
                                    <label class="control-label">Remarks</label>
                                    <textarea class="form-control" name="remarks[]"><?= $delivery_info[$i]->TRemarks?></textarea>
                                </div>
                            </div>
                        </div>
                    <?php
                        }elseif ($consignment_type == 'By Air'){
                    ?>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="col-lg-2">
                                    <input type="hidden" name="invoices_id[]" value="<?= $container->invoices_id ?>">
                                    <input type="hidden" name="container_id[]" value="<?= $container->id ?>">
                                    <label class="control-label">No. of Pkgs</label>
                                    <input type="text" class="form-control" name="no_of_pkgs[]" value="<?= $container->packages ?>" readonly/>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">Shed</label>
                                    <input type="text" class="form-control" name="shed[]" value="<?= $shed_info->shed ?>" readonly/>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">Total Net Weight</label>
                                    <input type="text" class="form-control" name="total_net_weight[]" value="<?= $container->container_net_weight ?>" readonly/>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">Total Gross Weight</label>
                                    <input type="text" class="form-control" name="total_gross_weight[]" value="<?= $container->container_gross_weight ?>" readonly/>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="col-lg-2">
                                    <input type="hidden" name="Iid[]" value="<?= $delivery_info[$i]->Iid ?>"/>
                                    <label class="control-label">Truck No</label>
                                    <input type="text" class="form-control" name="truck_no[]" value="<?= $delivery_info[$i]->VCTruckNo?>"/>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">Bilty</label>
                                    <input type="text" class="form-control" name="bilty[]" value="<?= $delivery_info[$i]->VCBilty?>"/>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">Date of Delivery</label>
                                    <input type="date" class="form-control datepicker" name="delivery_date[]" value="<?= $delivery_info[$i]->DTDeliveryDate?>"/>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">Date of Receipt at Site</label>
                                    <input type="date" class="form-control datepicker" name="receipt_site[]" value="<?= $delivery_info[$i]->DTDateReceiptSite?>"/>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">Weight Declare</label>
                                    <input type="text" class="form-control" name="weight_declare[]" value="<?= $delivery_info[$i]->DCWeightDeclare?>"/>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">Weight Found</label>
                                    <input type="text" class="form-control" name="weight_found[]" value="<?= $delivery_info[$i]->DCWeightFound?>"/>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">Transporter</label>
                                    <input type="text" class="form-control" name="transporter[]" value="<?= $delivery_info[$i]->VCTransporter?>"/>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">No of Pkgs Received</label>
                                    <input type="text" class="form-control" name="pkgs_receive[]" value="<?= $delivery_info[$i]->VCPkgsReceive?>"/>
                                </div>
                                <div class="col-lg-4">
                                    <label class="control-label">Remarks</label>
                                    <textarea class="form-control" name="remarks[]"><?= $delivery_info[$i]->TRemarks?></textarea>
                                </div>
                            </div>
                        </div>
                    <?php
                        }
                 $i++;
                    }
                }
                ?>

                <div class="form-group">
                    <div class="col-md-10">
                        <div id="delivery_section"></div>
                        <div id="fcl" class="section"></div>
                        <div id="lcl" class="section"></div>
                        <div id="break_bulk" class="section"></div>
                        <div id="by_air" class="section">
                        </div>
                    </div>
                </div>
               <div class="form-group">
                  <div class="col-lg-12 text-center">
                     <button type="submit" class="btn btn-sm btn-success" id="submit"><i class="fa fa-check"></i>
                         <?php
                             if (!empty($invoice_info)) {
            
                                 echo lang('update');
            
                             } else {
            
                                 echo lang('create');
            
                             }
    
                         ?>
                     </button>
                  </div>
               </div>
            </div>
         </section>
      </form>
   </div>
</div>
<script type="text/javascript">
   $(document).ready(function () {
       $('#form').on('keyup keypress', function (e) {
           var keyCode = e.keyCode || e.which;
           if (keyCode === 13) {
               e.preventDefault();
               return false;
           }
       });
   });

   /*** DOCUMENTS SECTION ***/

   function selectConsignment(val){
       console.log(val);
       if (val.length > 0) {
           /*$(this).addClass('spinner');*/
           $.ajax({
               url: "<?php echo base_url(); ?>admin/invoice/type_consignment",
               type: "POST",
               dataType: "json",
               data: {"job_no": val},
               success: function (data) {
                   console.log(data);
                   if (data) {
                       $('#consignment_type').val(data[1]);
                       $('#delivery_section').html(data[0]);
                       $('.select_box').select2({});
                   }else{
                       $('#delivery_section').html("<p class='text-danger text-center'><b>Delivery Section not found</b></p>");
                   }
               },
               error: function (data) {
                   console.log('error');
               }
           });
       }
       else {
           $('#delivery_section').html("<p class='text-danger text-center'><b>Job Type not selected</b></p>");
       }

   }

   /*** END DOCUMENT SECTION ***/




   /*** Consignment Type***/

   /*function selectConsignment22(val) {
       var consignment_type=$('#consignment_type');
       $.getJSON(" echo site_url('admin/invoice/get_consignment')"+"/"+val, function (result) {
           (result != null)?consignment_type.val(result.invoiceinfo.consignment_type):'';
           var fclall = '';
           $.each(result.container_info, function (index, value) {
               var fcl = '<div class="col-lg-2"><label class="control-label">Container No</label><input type="text" class="form-control" name="container_no" value="' + value.container_no + '" readonly/></div><div class="col-lg-2"><label class="control-label">Type</label><input type="text" class="form-control" name="type" value="' + value.container_type + '" readonly/></div><div class="col-lg-2"><label class="control-label">Container ft</label><input type="text" class="form-control" name="container_ft" value="' + value.container_ft + '" readonly/></div><div class="col-lg-2"><label class="control-label">No. of Pkgs</label><input type="number" min="0" class="form-control" name="no_of_pkgs" value="' + value.packages + '" readonly/></div><div class="col-lg-2"><label class="control-label">Total Weight</label><input type="number" min="0" class="form-control" name="total_weight" value="" readonly/></div>';
               fcl += '<div class="col-lg-12">';
               fcl += '<div class="form-group">';
               fcl += '<div class="col-lg-2">';
               fcl += '<label class="control-label">Truck No</label>';
               fcl += '<input type="text" class="form-control" name="truck_no[]"/>';
               fcl += '</div>';
               fcl += '<div class="col-lg-2">';
               fcl += '<label class="control-label">Bilty</label>';
               fcl += '<input type="text" class="form-control" name="bilty[]"/>';
               fcl += '</div>';
               fcl += '<div class="col-lg-2">';
               fcl += '<label class="control-label">Date of Delivery</label>';
               fcl += '<input type="text" class="form-control datepicker" name="delivery_date[]"/>';
               fcl += '</div>';
               fcl += '<div class="col-lg-2">';
               fcl += '<label class="control-label">Date of Receipt at Site</label>';
               fcl += '<input type="text" class="form-control datepicker" name="receipt_site[]"/>';
               fcl += '</div>';
               fcl += '<div class="col-lg-2">';
               fcl += '<label class="control-label">Weight Declare</label>';
               fcl += '<input type="text" class="form-control" name="weight_declare[]"/>';
               fcl += '</div>';
               fcl += '<div class="col-lg-2">';
               fcl += '<label class="control-label">Weight Found</label>';
               fcl += '<input type="text" class="form-control" name="weight_found[]"/>';
               fcl += '</div>';
               fcl += '<div class="col-lg-2">';
               fcl += '<label class="control-label">Transporter</label>';
               fcl += '<input type="text" class="form-control" name="transporter[]"/>';
               fcl += '</div>';
               fcl += '<div class="col-lg-2">';
               fcl += '<label class="control-label">No of Pkgs Received</label>';
               fcl += '<input type="number" min="0" class="form-control" name="pkgs_receive[]"/>';
               fcl += '</div>';
               fcl += '<div class="col-lg-3">';
               fcl += '<label class="control-label">Remarks</label>';
               fcl += '<textarea class="form-control" name="remarks[]"></textarea>';
               fcl += '</div>';
               fcl += '</div>';
               fcl += '<hr/>';
               fcl += '</div>';
               fclall += fcl;
           });
           $('#fcl').html(fclall).hide().fadeIn(500);
       });
   }*/


           /*var lclall ='';
               var lcl = '<div class="col-lg-3"><label class="control-label">Shed</label><input type="text" class="form-control" name="Shed" value="" readonly/></div><div class="col-lg-3"><label class="control-label">No. of Pkgs</label><input type="number" min="0" class="form-control" name="no_of_pkgs" value="" readonly/></div><div class="col-lg-3"><label class="control-label">Total Net Weight</label><input type="number" min="0" class="form-control" name="total_net_weight" value="" readonly/></div><div class="col-lg-3"><label class="control-label">Total Gross Weight</label><input type="number" min="0" class="form-control" name="total_gross_weight" value="" readonly/></div>';
               */


   /*** END Consignment Type ***/
   /*$(function () {
       $('#lcl').hide();
       $('#fcl').hide();
       $('#break_bulk').hide();
       $('#by_air').hide();
       $('#consignment_type').change(function () {
           if (this.input[this.type].value == 'LCL') {
               $('#lcl').show();
           }else if (this.input[this.type].value == 'FCL') {
               $('#fcl').show();
           }else if (this.input[this.type].value == 'Break Bulk') {
               $('#break_bulk').show();
           }else if (this.input[this.type].value == 'By Air') {
               $('#by_air').show();
           }
       });
   });*/



   
</script>