<?= message_box('success') ?>
<?= message_box('error') ?>
<section class="content-header">
<h1 class="header-<?= config_item('sidebar_theme'); ?>" style="margin: 0px 0px 10px 0px;"><?= lang('job_detail') ?></h1>
    <div class="row">
        <div class="col-sm-8">
            <?php
                $client_info = $this->invoice_model->check_by(array('client_id' => $invoice_info->client_id), 'tbl_client');
                $bill_info = $this->invoice_model->check_by(array('invoices_id' => $invoice_info->invoices_id), 'tbl_bills');
            ?>
            <?php /*if ($invoice_info->show_client == 'Yes') { */?><!--
                <a class="btn btn-sm btn-success" href="<?/*= base_url() */?>admin/invoice/change_status/hide/<?/*= $invoice_info->invoices_id */?>" title="<?/*= lang('hide_to_client') */?>"><i class="fa fa-eye-slash"></i> <?/*= lang('hide_to_client') */?>
                </a><?php /*} else { */?>
                <a class="btn btn-sm btn-warning" href="<?/*= base_url() */?>admin/invoice/change_status/show/<?/*= $invoice_info->invoices_id */?>" title="<?/*= lang('show_to_client') */?>"><i class="fa fa-eye"></i> <?/*= lang('show_to_client') */?>
                </a>
			--><?php /*} */?>
			<a href="#" data-toggle="modal" data-target="#documents" class="btn btn-sm btn-success"><i class="fa fa-files-o"></i> View Documents</a>
            <?php if ($role == 1) { ?>
                <?php if (!empty($bill_info)) { ?>
					<a class="btn btn-sm btn-danger" href="<?= base_url() ?>admin/invoice/manage_invoice/payment/<?= encode($invoice_info->invoices_id) ?>/<?= encode($invoice_info->client_id) ?>"
					   title="<?= lang('add_payment') ?>"><i class="fa fa-credit-card"></i> <?= lang('pay_invoice') ?>
                    </a>
                    <?php
                }
            }
            ?>
        </div>
		<!--<div class="col-sm-4 pull-right">
            <a onclick="print_invoice('print_invoice_new')" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Print" class="btn btn-sm btn-danger pull-right"  >
                <i class="fa fa-print"></i>
            </a>
            <a style="margin-right: 5px" href="<?/*= base_url() */?>admin/invoice/manage_invoice/pdf_invoice/<?/*= $invoice_info->invoices_id */?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="PDF" class="btn btn-sm btn-success pull-right" >
                <i class="fa fa-file-pdf-o"></i>
            </a>
        </div>  -->
    </div>
</section>
<section class="content">
    <!-- Start Display Details -->
	<!-- Main content -->
    <div class="row" >
        <section class="invoice" id="print_invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <img style="width: 60px;width: 60px;margin-top: -10px;margin-right: 10px;" src="<?= base_url() . config_item('invoice_logo') ?>" ><?= config_item('company_name') ?>
						<small class="pull-right"><?= lang('created_date') ?> : <?= strftime(config_item('date_format'), strtotime($invoice_info->created_date)); ?></small>
                    </h2>
                </div><!-- /.col -->
				<div class="col-md-4">
					<div class="col-md-12 table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th colspan="2">Received From</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><strong><?= lang('name') ?></strong></td>
									<td><?= (config_item('company_legal_name') ? config_item('company_legal_name') : config_item('company_legal_name')) ?></td>
								</tr>
								<tr>
									<td><strong><?= lang('address') ?></strong></td>
									<td><?= (config_item('company_address') ? config_item('company_address') : config_item('company_address')) ?></td>
								</tr>
								<tr>
									<td><strong><?= lang('zip_code') ?></strong></td>
									<td><?= config_item('company_zip_code') ?></td>
								</tr>
								<tr>
									<td><strong><?= lang('country') ?></strong></td>
									<td><?= (config_item('company_country') ? config_item('company_country') : config_item('company_country')) ?></td>
								</tr>
								<tr>
									<td><strong><?= lang('phone') ?></strong></td>
									<td><?= config_item('company_phone') ?></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-md-12 table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th colspan="2">Bill To</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><strong><?= lang('name') ?></strong></td>
									<td><?= ucfirst($client_info->name) ?></td>
								</tr>
								<tr>
									<td><strong><?= lang('address') ?></strong></td>
									<td><?= ucfirst($client_info->address) ?></td>
								</tr>
								<tr>
									<td><strong><?= lang('city') ?></strong></td>
									<td><?= ucfirst($client_info->city) ?></td>
								</tr>
								<tr>
									<td><strong><?= lang('phone') ?></strong></td>
									<td><?= ucfirst($client_info->phone) ?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-md-8">
				<div class="col-md-12">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th colspan="4">Description Of Consignment</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><strong><?= lang('type') ?> </strong></td>
								<td><?= $invoice_info->type ?></td>
								<td><strong><?= lang('reference_no') ?></strong></td>
								<td><?= $this->invoice_model->job_no_creation($invoice_info->invoices_id) ?></td>
							</tr>
							<!--<tr>
								<td><strong><?/*= lang('vessel') */?></strong></td>
								<td><?/*= $invoice_info->vessel */?></td>
								<td><strong><?/*= lang('unit') */?></strong></td>
								<td><?/*= weight_unit($invoice_info->unit) */?></td>
							</tr>-->
							<tr>
								<td><strong><?= lang('net_weight') ?></strong></td>
								<td><?= ($invoice_info->net_weight != 0.00)?$invoice_info->net_weight.' KGS':'' ?></td>
								<td><strong><?= lang('gross_weight') ?></strong></td>
								<td><?= ($invoice_info->gross_weight != 0.00)?$invoice_info->gross_weight.' KGS':'' ?></td>
							</tr>
							<tr>
								<td><strong><?= lang('mode') ?></strong></td>
								<td><?= ucfirst($invoice_info->mode) ?></td>
								<td><strong><?= lang('gd_type') ?></strong></td>
								<td><?= ucfirst(gd_type($invoice_info->mode_options)) ?></td>
							</tr>
							<tr>
								<td><strong><?= lang('port_of_loading') ?></strong></td>
								<td><?= $invoice_info->p_o_l ?></td>
								<td><strong><?= lang('port_of_discharge') ?></strong></td>
								<td><?= $invoice_info->p_o_d ?></td>
							</tr>
							<tr>
								<td><strong><?= lang('port') ?></strong></td>
								<td><?= $invoice_info->port ?></td>
							<!--	<td><strong><?/*= lang('yard') */?></strong></td>
								<td><?/*= yard($invoice_info->yard) */?></td>-->
							</tr>
                            <?php
                                if($invoice_info->mode == 'air') {
                                    ?>
									<tr>
                                        <td><strong><?= lang('paid_packages_charges') ?></strong></td>
                                        <td><?= number_format($invoice_info->paid_packages_charges, 2) ?></td>
                                        <td><strong><?= lang('total_caa_per_kgs') ?></strong></td>
                                        <td><?= number_format($invoice_info->total_caa_per_kgs, 2) ?></td>
                                    </tr>
									<tr>
                                        <td><strong><?= lang('paid_commission') ?></strong></td>
                                        <td><?= number_format($invoice_info->paid_commission, 2) ?></td>
                                        <td><strong><?= lang('total_caa_paid_amount') ?></strong></td>
                                        <td><?= number_format($invoice_info->paid_packages_charges + $invoice_info->paid_commission, 2) ?></td>
                                    </tr>
                                    <?php
                                }
                            ?>
						</tbody>
					</table>
				</div>
            <div class="col-md-12" style="padding:0;">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th colspan="3">Job Activity</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th><?= lang('date') ?></th>
                            <th><?= lang('status') ?></th>
                            <th>Date Saved</th>
                        </tr>
                        <?php
                            $job_activities = $this->invoice_model->check_by_all(array('invoices_id'=>$invoice_info->invoices_id), 'tbl_job_activity');
                            if(!empty($job_activities)){
                                foreach($job_activities as $job_activity){
                                    $job_status = $this->invoice_model->check_by(array('job_status_id'=>$job_activity->job_activity_status), 'tbl_job_status');
                                    ?>
									<tr>
                                    <td><?= strftime(config_item('date_format'), strtotime($job_activity->job_activity_date)) ?></td>
                                    <td><?= $job_status->job_status_title ?></td>
                                    <td><?= date('d-m-Y h:i:s A', strtotime($job_activity->datetime)) ?></td>
                                </tr>
                                    <?php
                                }
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-12" style="padding:0;">
                <div class="col-md-5">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th colspan="3">Description of Items</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th><?= lang('type') ?></th>
                            <th><?= lang('commodity') ?></th>
                            <th><?= lang('hs_code') ?></th>
                        </tr>
                        <?php
                            if(!empty($commodity_info)){
                                foreach($commodity_info as $commodity){
                                    ?>
									<tr>
                                    <td><?= commodity_type($commodity->commodity_type) ?></td>
                                    <td><?= $commodity->commodity ?></td>
                                    <td><?= $commodity->hs_code ?></td>
                                </tr>
                                    <?php
                                }
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-7">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th colspan="3">Description of Due Dates</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Comment</th>
                        </tr>
                        <?php
                            if(!empty($due_dates_info)){
                                foreach($due_dates_info as $due_dates){
                                    /*$status_info = $this->invoice_model->check_by(array('job_status_id' => $due_dates->due_status), 'tbl_job_status');*/
                                    ?>
									<tr>
                                    <td>
                                        <?php
                                            if($due_dates->due_date != '0000-00-00'){
                                                echo strftime(config_item('date_format'), strtotime($due_dates->due_date));
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            if(!empty($due_dates->due_time)){
                                                echo $due_dates->due_time;
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            if(!empty($due_dates->due_comment)){
                                                echo $due_dates->due_comment;
                                            }
                                        ?>
                                    </td>
                                </tr>
                                    <?php
                                }
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
				
                <?php
                    $bill_info = $this->invoice_model->check_by(array('invoices_id' => $invoice_info->invoices_id), 'tbl_bills');
                    if($bill_info){
                        ?>
						<div class="col-xs-4 pull-right">
					<div class="table-responsive">
						<table class="table table-bordered">
							<tr class="my_line">
								<th style="width:50%" class="text-right">Total Amount : </th>
								<td> PKR  <?= number_format($this->invoice_model->calculate_bill('bill_grand_total',$bill_info->bill_id), 2) ?></td>
							</tr>
							<tr class="my_line">
								<th style="width:50%" class="text-right">Advances : </th>
								<td> PKR <?= number_format($this->invoice_model->pre_job_advances($invoice_info->invoices_id), 2) ?></td>
							</tr>
                            <tr class="my_line">
                                <th style="width:50%" class="text-right">Received Amount : </th>
                                <td> PKR <?= number_format($this->invoice_model->calculate_to('paid_amount', $invoice_info->invoices_id), 2) ?></td>
                            </tr>
							<tr class="my_line">
								<th style="width:50%" class="text-right">Due Amount : </th>
								<td class="text-danger"> PKR <?= number_format($this->invoice_model->calculate_to('invoice_due', $invoice_info->invoices_id), 2) ?></td>
							</tr>
						</table>
					</div>
				</div><!-- /.col -->
                        <?php
                    }
                ?>
            </div>
        </section>
		<!--<section style="display: none;" class="invoice" id="print_invoice_new">
			<?php /*$this->load->view('admin/invoice/pdf_invoice'); */?>
        </section>-->
		<!-- Documents modal -->
        <div id="documents" class="modal fade">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h6 class="modal-title">Job Document</h6>
                    </div>
                    <div class="modal-body">
						<div class="modal-body">
                            <div class="form-group">
                                <div class="row">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>Document Type</th>
												<th><?= lang('date') ?></th>
												<th><?= lang('type') ?></th>
												<th><?= lang('file') ?></th>
												<th><?= lang('comment') ?></th>
											</tr>
										</thead>
										<tbody>
										<?php
											if(!empty($invoice_documents_info)) {
												foreach($invoice_documents_info as $document) {
                                                    ?>
											<tr>
												<td><?= document_title($document->document_title) ?></td>
												<td><?= ($document->document_date != '0000-00-00')?strftime(config_item('date_format'), strtotime($document->document_date)):' - ' ?></td>
												<td><?= $document->document_type ?></td>
												<td>
													<?php
														if(!empty($document->document_file)){
													?>
													<a href="<?= base_url($document->document_file) ?>">Download</a>
															<?php
															}else {
                                                            ?>
                                                    <span> - </span>
                                                            <?php
                                                        }
															?>
												</td>
												<td><?= $document->document_comment ?></td>
											</tr>
                                                    <?php
                                                }
                                            }
										?>
										</tbody>
									</table>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
		<!-- /END DOCUMENT MODAL -->
    </div>
</section>
<script type="text/javascript">
    function print_invoice(print_invoice_new) {
        var printContents = document.getElementById(print_invoice_new).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>