<link href="<?php echo base_url() ?>asset/css/bootstrap-toggle.min.css" rel="stylesheet">

<script src="<?php echo base_url() ?>asset/js/bootstrap-toggle.min.js"></script>

<?php echo message_box('success'); ?>

<?php echo message_box('error'); ?>

	<div class="row">

	<div class="col-lg-12">

        <form role="form" enctype="multipart/form-data" id="form_label" action="<?php echo base_url(); ?>admin/invoice/search_invoice" method="post" class="form-horizontal  ">
        	<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('manage_job') ?></h1>
			<section class="panel panel-default">

                <header class="panel-heading  "><?= lang('search_job_no') ?></header>

                <div class="panel-body">

					<div class="form-group">

						<label class="col-lg-3 control-label"><?= lang('search_by') ?> <span class="text-danger">*</span></label>

						<div class="col-lg-3">

							<select class="form-control select_box" style="width: 100%" id="search_by"  required>

								<option value=""><?= lang('search_by'); ?></option>

								<option value="job_no" <?= (isset($reference_no))?'selected':'' ?>><?= lang('reference_no'); ?></option>

								<option value="client" <?= (isset($client_id))?'selected':'' ?>><?= lang('client'); ?></option>

							</select>

						</div>

					</div>

					<div class="form-group" style="<?= (isset($client_id))?'display:block;':'display:none;' ?>" id="client">

						<label class="col-lg-3 control-label"><?= lang('client') ?> <span class="text-danger">*</span></label>

						<div class="col-lg-3">

							<select class="form-control select_box client" style="width: 100%"  name="client_id" required <?= (isset($client_id))?'':'disabled' ?>>

                                <option value="">Choose Client</option>

                            <?php

                            if (!empty($all_client)) {

                                foreach ($all_client as $v_client) {

                                    ?>

                                    <option value="<?= $v_client->client_id ?>" <?php

                                    if(isset($client_id)){

                                        echo ($v_client->client_id == $client_id)?'selected':'';

                                    }  ?>><?= $v_client->name ?></option>

                                            <?php

                                        }

                                    }

                                    ?>

							</select>

						</div>

					</div>

					<div class="form-group" style="<?= (isset($reference_no))?'display:block;':'display:none;' ?>" id="job_no">

						<label class="col-lg-3 control-label"><?= lang('reference_no') ?> <span class="text-danger">*</span></label>

						<div class="col-lg-3">

                            <input type="text" class="form-control job_no" name="reference_no" placeholder="<?= lang('reference_no') ?>" value="<?= (isset($reference_no))?$reference_no:'' ?>" <?= (isset($reference_no))?'':'disabled' ?> required autocomplete="off" />

						</div>

					</div>

					<div class="form-group">

						<label class="col-lg-3 control-label"></label>

						<div class="col-lg-3">

							<button type="submit" class="btn btn-sm btn-success"><i class="fa fa-search"></i> <?= lang('search') ?></button>

						</div>

					</div>

				</div>

			</section>

        </form>

		</div>

	</div>

	<?php

		if(isset($client_id) || isset($reference_no)){

	?>

	<div class="row">

		<div class="col-lg-12">

			<section class="panel panel-default">

                <header class="panel-heading  "><?= lang('manage_job_no') ?></header>

                <div class="panel-body">

			<div class="table-responsive">

				<table class="table table-striped DataTables " id="DataTables">

					<thead>

						<tr>

							<th><?= lang('job_no') ?></th>

							<th><?= lang('client_name') ?></th>

							<th class="col-currency"><?= lang('amount') ?></th>

							<th class="col-currency"><?= lang('due_amount') ?></th>

							<th><?= lang('invoice_status') ?></th>

							<?php

								if($this->session->userdata('role_id') == 1 ||  $this->session->userdata('dept_id') == 1 ||  $this->session->userdata('dept_id') == 3){

							?>

							<!--<th><?/*= lang('status') */?></th>-->

							<th class="col-options no-sort" ><?= lang('action') ?></th>

							<?php

								}

							?>

						</tr>

					</thead>

					<tbody>

						<?php

							foreach($all_invoice_info as $v_invoices){

								$bill_info = $this->invoice_model->check_by(array('invoices_id' => $v_invoices->invoices_id), 'tbl_bills');

                                if($this->invoice_model->get_payment_status($v_invoices->invoices_id) == lang('fully_paid')){

                                    $invoice_status = lang('fully_paid');

                                    $label = "success";

                                }

                                elseif($this->invoice_model->get_payment_status($v_invoices->invoices_id) == lang('partially_paid')){

                                    $invoice_status = lang('partially_paid');

                                    $label = "info";

                                }

                                else{

                                    $invoice_status = lang('not_paid');

                                    $label = "danger";

                                }

						?>

								<tr>

									<td><a class="text-info" href="<?= base_url() ?>admin/invoice/manage_invoice/invoice_details/<?= encode($v_invoices->invoices_id) ?>"><?= $this->invoice_model->job_no_creation($v_invoices->invoices_id) ?></a></td>

									<?php

									$client_info = $this->invoice_model->check_by(array('client_id' => $v_invoices->client_id), 'tbl_client');

									?>

									<td><?= $client_info->name ?></td>

									<td>

									<?php

										if(!empty($bill_info)){

											echo "PKR ".number_format($this->invoice_model->calculate_bill('bill_grand_total', $bill_info->bill_id),2);

										}

									?>

									</td>

									<td>

									<?php

										if(!empty($bill_info)){

											echo "PKR ".number_format($this->invoice_model->calculate_to('invoice_due', $v_invoices->invoices_id),2);

										}

									?>

									</td>

									<td><label class="label label-<?= $label ?>"><?= $invoice_status ?></label></td>

									<!--<td>

                                        <?php

/*                                            if($v_invoices->job_status == 1) {

                                                */?>

                                                    <a data-toggle="tooltip" data-placement="top"

                                                       title="Click to Deactive"

                                                       href="<?php /*echo base_url() */?>admin/invoice/change_job_status/0/<?php /*echo encrypt($v_invoices->invoices_id); */?>"><label class="label label-success"><?/*= lang('active') */?></label></a>

                                                <?php

/*                                            }else {

                                                */?>

                                                    <a data-toggle="tooltip" data-placement="top"

                                                       title="Click to Active"

                                                       href="<?php /*echo base_url() */?>admin/invoice/change_job_status/1/<?php /*echo encrypt($v_invoices->invoices_id); */?>"><label class="label label-danger"><?/*= lang('deactive') */?></label></a>

                                                <?php

/*                                            }

                                        */?>

                                    </td>-->

									<td>

										<?php

                                            echo btn_edit('admin/invoice/manage_invoice/create_invoice/' . encode($v_invoices->invoices_id))." ";

                                            /*echo btn_delete('admin/invoice/delete/delete_invoice/' . $v_invoices->invoices_id)." ";*/

                                            echo btn_view('admin/invoice/manage_invoice/invoice_details/' . encode($v_invoices->invoices_id));

										?>

									</td>

								</tr>

							<?php

								}

							?>

					</tbody>

				</table>

			</div>

		</section>

	</div>

	</div>

	<?php

		}

	?>

	<script>

		/*$(".client").attr("disabled", true);

		$(".job_no").attr("disabled", true);*/

		$('#search_by').on("change", function() {

			var type = $(this).val();

			if(type == 'client'){

				$('#client').show();

				$('#job_no').hide();

				$(".client").removeAttr("disabled");

				$(".job_no").attr("disabled","disabled");

			}

			else if (type == 'job_no'){

				$('#job_no').show();

				$('#client').hide();

				$(".job_no").removeAttr("disabled");

				$(".client").attr("disabled","disabled");

			}

			else{

				$('#client').hide();

				$('#job_no').hide();

				$(".client").removeAttr("disabled");

				$(".job_no").removeAttr("disabled");

			}

		});

	</script>