

<div class="row">

    <section class="col-sm-12">

        <?= message_box('error') ?>

        <!-- Start create invoice -->
<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('edit_payment') ?></h1>
        <section class="panel panel-default">



            <header class="panel-heading  "><?= lang('payment_details') ?> - TRANS <?= $payments_info->trans_id ?></header>

            <div class="panel-body">

                <form method="post" action="<?= base_url() ?>admin/invoice/update_payment/<?php

                if (!empty($payments_info)) {

                    echo encrypt($payments_info->payments_id);

                }

                ?>" class="form-horizontal">

                    <div class="form-group">

                        <label class="col-lg-3 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label>

                        <div class="col-lg-4">

                            <input type="text" required="" class="form-control" value="<?= round($payments_info->amount,2) ?>" name="amount">

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-lg-3 control-label"><?= lang('received_by') ?></label>

                        <div class="col-lg-4">

                            <input type="text" class="form-control" value="<?= $payments_info->received_by ?>" name="received_by">

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-lg-3 control-label"><?= lang('payment_date') ?></label>

                        <div class="col-lg-4">

                            <input type="text" required="" name="payment_date"  class="form-control datepicker" value="<?php

                            if (!empty($payments_info->payment_date)) {

                                echo $payments_info->payment_date;

                            } else {

                                echo date('Y-m-d');

                            }

                            ?>" data-date-format="<?= config_item('date_picker_format'); ?>">

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-lg-3 control-label"><?= lang('payment_mode') ?> <span class="text-danger">*</span></label>

                        <div class="col-lg-4">

                            <select name="payment_type" class="form-control select_box" id="type">

                                <option value="1" <?= ($payments_info->payment_type == 1)?'selected':'' ?>>Cash in Hand</option>

                                <option value="2" <?= ($payments_info->payment_type == 2)?'selected':'' ?>>Cash at Bank</option>

                            </select>

                        </div>

                    </div>

                    <div id="cash_at_bank" style="<?= ($payments_info->payment_type == 1)?'display:none;':'display:block;' ?>">

                        <?php

                        if($payments_info->payment_type == 2){

                            $branch_info = $this->invoice_model->check_by(array('BR_ID' => $payments_info->branch_id), 'branches');

                            $bank_info = $this->invoice_model->check_by(array('B_ID' => $branch_info->B_ID), 'banks');

                            $account_id = $this->invoice_model->check_by(array('PAYMENT_ID' => $payments_info->payments_id), 'transactions_meta');

                        }

                        ?>

                        <div class="form-group">

                            <label class="col-lg-3 control-label">Banks <span class="text-danger">*</span></label>

                            <div class="col-lg-4">

                                <select name="bank" class="form-control cash_at_bank select_box" onchange="selectBranches(this.value)" style="width:100%;">

                                    <option value="">Choose Bank</option>

                                    <?php foreach ($banks as $bank): ?>

                                        <option value="<?php echo $bank->B_ID; ?>" <?php

                                        if($payments_info->payment_type == 2){

                                            echo ($bank->B_ID == $bank_info->B_ID)?'selected':'';

                                        }

                                        ?>><?php echo $bank->B_NAME; ?></option>

                                    <?php endforeach; ?>

                                </select>

                            </div>

                        </div>

                        <div id="branches">

                            <?php

                            if($payments_info->payment_type == 2) {

                                ?>

                                <div class="form-group">

                                    <label class="col-lg-3 control-label">Branches <span

                                                class="text-danger">*</span></label>

                                    <div class="col-lg-4">

                                        <select name="branch_id"

                                                class="form-control cash_at_bank select_box"

                                                onchange="getAccountId(this.value)">

                                            <option value="">Choose Branch</option>

                                            <?php

                                            $branches = $this->invoice_model->check_by_all(array('B_ID'=>$bank_info->B_ID),'branches');

                                            foreach($branches as $branch) {

                                                ?>

                                                <option value="<?= $branch->BR_ID ?>" <?= ($branch->BR_ID == $branch_info->BR_ID)?'selected':'' ?>><?= $branch->BR_NAME ?></option>

                                                <?php

                                            }

                                            ?>

                                        </select>

                                    </div>

                                </div>

                                <?php

                            }

                            ?>

                        </div>

                        <input type="hidden" id="account_id" name="account_id" value="<?= ($payments_info->payment_type == 2)?$account_id->A_ID:'' ?>" />

                        <?php

                        $trans_account_info = $this->db->select('*')

                            ->from('transactions_meta')

                            ->where('PAYMENT_ID',$payments_info->payments_id)

                            ->order_by('TM_ID','DESC')

                            ->get()->result();

                        ?>

                        <div class="form-group">

                            <label class="col-lg-3 control-label">Cheque No / Pay Order No <span class="text-danger">*</span></label>

                            <div class="col-lg-4">

                                <input type="text" class="form-control cash_at_bank" name="cheque_payorder_no" value="<?= ($payments_info->payment_type == 2)?$payments_info->cheque_payorder_no:'' ?>" />

                            </div>

                        </div>

                    </div>

                    <input type="hidden" name="trans_account_id" value="<?= $trans_account_info[0]->TM_ID ?>" />

                    <div class="form-group">

                        <label class="col-lg-3 control-label"><?= lang('notes') ?> </label>

                        <div class="col-lg-7">

                            <textarea name="notes" class="textarea form-control"><?= $payments_info->notes ?></textarea>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-lg-3 control-label"></label>

                        <div class="col-lg-6">

                            <button type="submit" class="btn btn-sm btn-success<?= config_item('button_color') ?>"> <?= lang('save_changes') ?></button>

                        </div>

                    </div>

                </form>

            </div>

        </section>

    </section>

</div>

<script type="text/javascript">

    $('#type').on("change", function() {

        var type = $(this).val();

        if(type == '1'){

            $('#cash_at_bank').fadeOut(500, function() { $(this).hide(); });

            $(".cash_at_bank").attr("disabled","disabled");

            $(".cash_at_bank").removeAttr('required');

        }

        else{

            $('#cash_at_bank').fadeIn(500).show();

            $(".cash_at_bank").removeAttr("disabled");

            $(".cash_at_bank").prop('required',true);

        }

    });

    function selectBranches(val) {

        var option="";

        console.log(val);

        if(val == '') {

            $('#branches').hide();

        }

        else{

            $.getJSON("<?php echo site_url('admin/accounts/ajax_select_branches') ?>" + "/" + val, function (result) {

                /*option.empty();*/

                $.each(result, function (index, value) {

                    option = ('<option value="' + value.BR_ID + '">' + value.BR_NAME + '</option>')+option;

                });

                var drop = '<div class="form-group"><label class="col-lg-3 control-label">Branches <span class="text-danger">*</span></label><div class="col-lg-4"><select name="branch_id" required="" class="form-control cash_at_bank select_box" onchange="getAccountId(this.value)"><option value="">Choose Branch</option>' + option + '</select></div></div>';

                $('#branches').html(drop).hide().fadeIn(500);

                $('.select_box').select2({});

                getAccountId();

            });

        }

    }

    function getAccountId(val){

        account_id=$('#account_id');

        $.getJSON( "<?php echo site_url('admin/accounts/ajax_get_branch_aid') ?>"+"/"+val, function(result) {

            (result != null)?account_id.val(result.A_ID):'';

        });

    }

</script>

<!-- end -->













