<style>
   .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
	   vertical-align: middle;
   }
    .select2-container{
        min-width: 100%;
    }
    .box-title{
        color: #000;
        font-weight:600 ;
    }
    .box{
        border-top: none;
    }
</style>
<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php
    if (!empty($this->session->flashdata('message_name'))) {
        
        ?>
		<div class="callout callout-success">
   <h4><?= $this->session->flashdata('message_name') ?></h4>
   <p><?= lang('invoice_created') ?></p>
</div>
        <?php
    }

?>
<?php
    if (!empty($this->session->flashdata('message_error'))) {
        
        ?>
		<div class="callout callout-danger">
   <h4><?= $this->session->flashdata('message_error') ?></h4>
   <p>Job Number already exist.</p>
</div>
        <?php
    }
    
    if (!empty($invoice_info)) {
        
        ?>
		<section class="content-header">
   <div class="row form-group">
      <a class="btn btn-sm btn-default" href="<?= base_url() ?>admin/invoice/search_invoice/" title="Back"><i
				  class="fa fa-arrow-left"></i> Back</a>
   </div>
</section>
    <?php } ?>
<div class="row">
   <div class="col-lg-12">
      <form role="form" enctype="multipart/form-data" id="form"
			action="<?php echo base_url(); ?>admin/invoice/save_invoice/<?php
                if (!empty($invoice_info)) {
                    echo encrypt($invoice_info->invoices_id);
                }
            ?>" method="post" class="form-horizontal  ">
         <h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= (!empty($invoice_info))?lang('edit_job'):lang('enter_job') ?> <span class="text-danger" style="font-size:12px;text-transform: lowercase">Fields with (*) are required</span></h1>
         <section class="panel panel-default">
            <header class="panel-heading  "><?= $title ?></header>
            <div class="panel-body">
				<?php
                    if(!empty($invoice_info) && $invoice_info->job_no == 'Manual') {
                        ?>
						<div class="form-group">
					  <div class="col-lg-4 bg-warning">
						 <label class="control-label text-danger"><?= lang('job_no') ?> <span
									 class="text-danger">*</span></label>
						 <input type="text" class="form-control"
								value="<?= $invoice_info->reference_no ?>"
								name="reference_no"
								placeholder="Enter Job  Number Here" id="reference_no" autocomplete="off" required>
						  <span class="text-danger" id="reference_error"></span>
					  </div>
				   </div>
                        <?php
                    }else{
                        if(config_item('job_number_auto') == 'No') {
                            ?>
							<div class="form-group">
						  <div class="col-lg-4 bg-warning">
							 <label class="control-label text-danger"><?= lang('job_no') ?> <span
										 class="text-danger">*</span></label>
							 <input type="text" class="form-control"
									value="<?= (!empty($invoice_info)) ? $invoice_info->reference_no : '' ?>"
									name="reference_no"
									placeholder="Enter Job  Number Here" id="reference_no" autocomplete="off" required>
							  <span class="text-danger" id="reference_error"></span>
						  </div>
					   </div>
                            <?php
                        }
                    }
                ?>
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Basic Infomation</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-lg-3">
                                    <label class="control-label"><?= lang('choose_type') ?> <span
                                                class="text-danger">*</span></label>
                                    <?php if (empty($invoice_info)) {
                                        ?>
                                        <select class="form-control select_box" style="width: 100%" name="type" id="type">
                                            <option value="">-</option>
                                            <?php
                                            if($all_job_type_info) {
                                                foreach ($all_job_type_info as $job_type) {
                                                    ?>
                                                    <option value="<?= $job_type->Iid ?>"><?= $job_type->VCJobType ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    <?php } else {
                                        ?>
                                        <input type="text" name="type" class="form-control" value="<?= job_type($invoice_info->type) ?>"
                                               readonly/>
                                        <?php
                                    } ?>
                                </div>
                                <div class="col-lg-3">
                                    <label class="control-label">Collectorate</label>
                                    <input type="text" class="form-control" name="collectorate" value="<?= (!empty($invoice_info))?$invoice_info->collectorate:'' ?>" />
                                </div>
                                <div class="col-lg-3">
                                    <label class="control-label">GD Type <span class="text-danger">*</span></label>
                                    <select class="form-control select_box" name="mode_options" style="width: 100%" required>
                                        <option value="">-</option>
                                        <?php
                                        if (!empty($all_gd_type_info)) {
                                            foreach ($all_gd_type_info as $v_gd_type) {
                                                ?>
                                                <option value="<?= $v_gd_type->gd_type_id ?>"
                                                    <?php
                                                    if (!empty($invoice_info)) {
                                                        if ($v_gd_type->gd_type_id == $invoice_info->mode_options) {
                                                            echo 'selected';
                                                        }
                                                    }
                                                    ?>
                                                ><?= ucfirst($v_gd_type->gd_type) ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <label class="control-label">Mode of Transport <span class="text-danger">*</span></label>
                                    <select class="form-control select_box" style="width: 100%" name="mode" id="mode_of_shipment" required>
                                        <option value="">-</option>
                                        <option value="sea"
                                            <?php
                                            if (!empty($invoice_info)) {

                                                echo ($invoice_info->mode == 'sea') ? 'selected' : '';

                                            } ?>>Sea</option>
                                        <option value="air"
                                            <?php
                                            if (!empty($invoice_info)) {

                                                echo ($invoice_info->mode == 'air') ? 'selected' : '';

                                            } ?>>Air</option>
                                        <option value="byroad"
                                            <?php
                                            if (!empty($invoice_info)) {

                                                echo ($invoice_info->mode == 'byroad') ? 'selected' : '';

                                            } ?>>By Road</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box box-default collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Consignor & Consignee Information</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-lg-3">
								 <label class="control-label"><?= lang('client') ?> <span
											 class="text-danger">*</span> </label>
                                <?php if (empty($requisition_info)) { ?>
									<select class="form-control select_box" style="width: 100%" name="client_id" onChange="selectConsignors(this.value);">
													<option value="">-</option>
                                        <?php
                                            if (!empty($all_client)) {
                                                foreach ($all_client as $v_client) {
                                                    ?>
													<option value="<?= $v_client->client_id ?>"
                                                        <?php
                                                            if (!empty($invoice_info)) {
                                                                if ($v_client->client_id == $invoice_info->client_id) {
                                                                    echo 'selected';
                                                                }
                                                            }
                                                        ?>
													><?= ucfirst($v_client->name) ?></option>
                                                    <?php
                                                }
                                            }
                                        ?>
												 </select>
                                    <?php
                                } else {
                                    $client_info = $this->invoice_model->check_by(array('client_id' => $invoice_info->client_id), 'tbl_client');
                                    ?>
									<input type="text" class="form-control" value="<?= $client_info->name ?>" readonly/>
									<input type="hidden" name="client_id" value="<?= $client_info->client_id ?>"/>
                                <?php } ?>
							  </div>
                            <div id="consignors">
							   <div class="col-lg-3">
								   <label class="control-label"><?= lang('consignor') ?></label>
								   <select class="form-control select_box" name="consignor_id" data-width="100%" onchange="getConsignorDetails(this.value)">
									   <option value="">-</option>
                                       <?php
                                           if(!empty($invoice_info)) {
                                               if (!empty($consignors_info)) {
                                                   foreach ($consignors_info as $consignor) {
                                                       ?>
													   <option value="<?= $consignor->consignor_id ?>" <?php
                                                           if(!empty($invoice_info->consignor_id)){
                                                               echo ($consignor->consignor_id == $invoice_info->consignor_id)?'selected':'';
                                                           } ?>><?= $consignor->name ?></option>
                                                       <?php
                                                   }
                                               }
                                           }
                                       ?>
								   </select>
							   </div>
						   </div>
							<div class="col-lg-3">
								 <label class="control-label"><?= lang('suppliers') ?></label>
								 <select class="form-control select_box" style="width: 100%" name="supplier_id" >
									<option value="">-</option>
                                     <?php
                                         if (!empty($all_supplier)) {
                                             foreach ($all_supplier as $supplier) {
                                                 ?>
												 <option value="<?= $supplier->supplier_id ?>"
                                                     <?php
                                                         if (!empty($invoice_info)) {
                                                             echo ($supplier->supplier_id == $invoice_info->supplier_id) ? 'selected' : '';
                                                         }
                                                     ?>
												 ><?= ucfirst($supplier->supplier_name) ?></option>
                                                 <?php
                                             }
                                         }
                                     ?>
								 </select>
							  </div>
                            <div class="col-lg-3">
                                <label class="control-label">Project Name</label>
                                <input type="text" class="form-control" name="project_name" value="<?= (!empty($invoice_info))?$invoice_info->project_name:'' ?>"/>
                            </div>
                        </div>
						<!--<div class="form-group">
                            <div class="col-lg-4">
                                <label class="control-label">Client Ref</label>
                                <input type="text" class="form-control" name="client_ref" value=""/>
                            </div>
                            <div class="col-lg-4">
                                <label class="control-label">Consignor/Supplier</label>
                                <input type="text" class="form-control" name="consignor" value=""/>
                            </div>
                            <div class="col-lg-4">
                                <label class="control-label">Consignor Address</label>
                                <textarea class="form-control" name="cons_address"></textarea>
                            </div>
                        </div>-->
                    </div>
                </div>

                <div class="box box-default collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Shipping Info</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-lg-2">
                                <label class="control-label"><?= lang('bl_no') ?></label>
                                <input type="text" class="form-control" name="bl_no" value="<?= (!empty($invoice_info)) ? $invoice_info->bl_no : '' ?>" />
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label"><?= lang('bl_date') ?></label>
								<div class="input-group">
                                    <input type="text" class="form-control datepicker " name="bl_date"  value="<?php
                                        if (!empty($invoice_info->bl_date)) {
                                            echo ($invoice_info->bl_date != '0000-00-00') ? $invoice_info->bl_date : '';
                                        }
                                    ?>" autocomplete="off"/>
									<div class="input-group-addon">
                                        <a><i class="entypo-calendar"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">I.G.M No</label>
                                <input type="text" class="form-control" name="igm_no" value="<?= (!empty($invoice_info)) ? $invoice_info->igm_no : '' ?>" />
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">I.G.M Date</label>
								<div class="input-group">
                                    <input type="text" class="form-control datepicker" name="igm_date"  value="<?php
                                        if (!empty($invoice_info->igm_date)) {
                                            echo ($invoice_info->igm_date != '0000-00-00') ? $invoice_info->igm_date : '';
                                        }
                                    ?>" autocomplete="off"/>
									<div class="input-group-addon">
                                        <a><i class="entypo-calendar"></i></a>
                                    </div>
                                </div>
                            </div>
							<div class="col-lg-2">
                                <label class="control-label">Index No</label>
                                <input type="text" class="form-control" name="index_no" value="<?= (!empty($invoice_info)) ? $invoice_info->index_no : '' ?>" />
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">Shipping Line</label>
                                <select class="form-control select_box" style="width: 100%" name="shipping_line">
									<option value="">-</option>
                                    <?php
                                        if (!empty($all_shipping_info)) {
                                            foreach ($all_shipping_info as $v_shipping_info) {
                                                ?>
												<option value="<?= $v_shipping_info->shipping_id ?>"
                                                    <?php
                                                        if (!empty($invoice_info) && !empty($invoice_info->shipping_line)) {
                                                            if ($v_shipping_info->shipping_id == $invoice_info->shipping_line) {
                                                                echo 'selected';
                                                            }
                                                        }
                                                    ?>
												><?= ucfirst($v_shipping_info->shipping_name) ?></option>
                                                <?php
                                            }
                                        }
                                    ?>
								 </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2">
                                <label class="control-label">Forwarding Agent</label>
                                <input type="text" class="form-control" value="<?= (!empty($invoice_info)) ? $invoice_info->forwarding_agent : '' ?>" name="forwarding_agent">
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">Port of Loading</label>
                                <input type="text" class="form-control" value="<?= (!empty($invoice_info)) ? $invoice_info->p_o_l : '' ?>" name="p_o_l">
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">Port of Discharge</label>
                                <input type="text" class="form-control" value="<?= (!empty($invoice_info)) ? $invoice_info->p_o_d : '' ?>" name="p_o_d">
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">Terminal / Port</label>
                                <input type="text" class="form-control" name="port" value="<?= (!empty($invoice_info))?$invoice_info->port:'' ?>" />
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">ETA</label>
                                <div class="input-group">
                                    <input type="text" class="form-control datepicker" name="eta" value="<?php
                                    if (!empty($invoice_info)) {
                                        echo ($invoice_info->eta != '0000-00-00') ? $invoice_info->eta : '';
                                    }
                                    ?>" />
                                    <div class="input-group-addon">
                                        <a><i class="entypo-calendar"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label"><?= lang('gross_weight') ?></label>
                                 <input class="form-control calculate_caa" id="gross_weight" type="number" min="0"
										value="<?php
                                            if (!empty($invoice_info)) {
                                                echo ($invoice_info->gross_weight != 0.00) ? round($invoice_info->gross_weight, 2) : '';
                                            }
                                        ?>" name="gross_weight">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2">
                                <label class="control-label"><?= lang('net_weight') ?></label>
								 <input class="form-control " type="number" min="0" value="<?php
                                     if (!empty($invoice_info)) {
                                         echo ($invoice_info->net_weight != 0.00) ? round($invoice_info->net_weight, 2) : '';
                                     }
                                
                                 ?>" name="net_weight">
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">Total Pkgs</label>
                                <input type="number" min="0" class="form-control" name="total_pkgs" value="<?php
                                    if (!empty($invoice_info)) {
                                        echo ($invoice_info->total_pkgs != 0.00) ? round($invoice_info->total_pkgs, 2) : '';
                                    }
                                ?>" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box box-default collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Consignment Mode</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label class="control-label">Select any option<span class="text-danger">*</span></label>
                                <br/>
                                <br/>
								<div class="col-md-2">
                                <input type="radio" value="LCL" name="consignment_type" class="minimal" <?php
                                    if (!empty($invoice_info)) {
                                        echo ($invoice_info->consignment_type == 'LCL') ? "checked" : '';
                                    }
                                ?> required> LCL
								</div>
								<div class="col-md-2">
                                <input type="radio" value="FCL" name="consignment_type" class="minimal" <?php
                                    if (!empty($invoice_info)) {
        
                                        echo ($invoice_info->consignment_type == 'FCL') ? "checked" : '';
        
                                    }
                                ?> required> FCL
								</div>
                                <div class="col-md-2">
                                <input type="radio" value="Break Bulk" name="consignment_type" class="minimal" <?php
                                    if (!empty($invoice_info)) {
        
                                        echo ($invoice_info->consignment_type == 'Break Bulk') ? "checked" : '';
        
                                    }

                                ?> required> Break Bulk
								</div>
                                <div class="col-md-2" id="consignment_air" style="<?php
                                    if(!empty($invoice_info)){
                                        echo ($invoice_info->mode == 'air')?"display:block;":"display:none;";
                                    }
                                ?>
										">
									<input type="radio" value="By Air" name="consignment_type" class="minimal" <?php
                                        if (!empty($invoice_info)) {
                                            echo ($invoice_info->consignment_type == 'By Air') ? "checked" : '';
                                        }
                                    ?> required> By Air
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
									<!---- LCL CONTAINER ----->
                                    <div id="lcl" style="<?php
                                        if (!empty($invoice_info)) {
                                            echo ($invoice_info->consignment_type == 'LCL') ? "display:block;" : "display:none;";
                                        } else {
                                            echo "display:none;";
                                        }
                                    ?> margin-top:10px;">
										<?php
                                            if(!empty($invoice_info) && $invoice_info->consignment_type == 'LCL') {
                                                $count = 0;
                                                foreach($container_info as $cont_info) {
                                                    ?>
													<div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-lg-3">
                                                    <label class="control-label">Shed <span class="text-danger">*</span></label>
                                                    <select class="form-control select_box" name="container_shed_lcl[]" data-width="100%" required>
														<option value="">-</option>
                                                        <?php
                                                            if (!empty($all_shed_info)) {
                                                                foreach ($all_shed_info as $v_shed) {
                                                                    ?>
																	<option value="<?= $v_shed->shed_id ?>" <?= $v_shed->shed_id == $cont_info->container_shed?'selected':'' ?>><?= ucfirst($v_shed->shed) ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                        ?>
													 </select>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label class="control-label">No. of Pkgs <span class="text-danger">*</span></label>
                                                    <input type="number" min="0" class="form-control" name="no_of_pkgs_lcl[]" value="<?= ($cont_info->packages != 0.00)?round($cont_info->packages,2):'' ?>" required />
                                                </div>
                                                <div class="col-lg-3">
                                                    <label class="control-label">Total Weight <span class="text-danger">*</span></label>
                                                    <input type="number" min="0" class="form-control" name="total_weight_lcl[]"  value="<?= ($cont_info->container_total_weight != 0.00)?round($cont_info->container_total_weight,2):'' ?>" required />
                                                </div>
                                                <?php
                                                    if($count > 0) {
                                                        ?>
														<div class="col-lg-2" style="margin-top:25px;">
															<button type="button" class="btn btn-danger btn-xs remCF"><i class="fa fa-minus"></i></button>
														</div>
                                                        <?php
                                                    }else {
                                                        ?>
														<div class="col-lg-2" style="margin-top:25px;">
															<button type="button" class="btn btn-primary btn-xs"
																	id="add_lcl"><i
																		class="fa fa-plus"></i></button>
														</div>
                                                        <?php
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                                    <?php
                                                    $count++;
                                                }
                                            }else {
                                                ?>
												<div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-lg-3">
                                                    <label class="control-label">Shed <span class="text-danger">*</span></label>
                                                    <select class="form-control select_box" name="container_shed_lcl[]" data-width="100%" required>
														<option value="">-</option>
                                                        <?php
                                                            if (!empty($all_shed_info)) {
                                                                foreach ($all_shed_info as $v_shed) {
                                                                    ?>
																	<option value="<?= $v_shed->shed_id ?>"><?= ucfirst($v_shed->shed) ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                        ?>
													 </select>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label class="control-label">No. of Pkgs <span class="text-danger">*</span></label>
                                                    <input type="number" min="0" class="form-control" name="no_of_pkgs_lcl[]" required />
                                                </div>
                                                <div class="col-lg-3">
                                                    <label class="control-label">Total Weight <span class="text-danger">*</span></label>
                                                    <input type="number" min="0" class="form-control" name="total_weight_lcl[]" required />
                                                </div>
                                                <div class="col-lg-2" style="margin-top:25px;">
                                                    <button type="button" class="btn btn-primary btn-xs" id="add_lcl"><i class="fa fa-plus"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                                <?php
                                            }
                                        ?>
										<div id="add_new_lcl"></div>
                                    </div>
									<!---- END LCL CONTAINER ----->
									<!---- FCL CONTAINER ----->
                                    <div id="fcl" style="<?php
                                        if (!empty($invoice_info)) {
                                            echo ($invoice_info->consignment_type == 'FCL') ? "display:block;" : "display:none;";
                                        } else {
                                            echo "display:none;";
                                        }
                                    ?> margin-top:10px;">
										<?php
                                            if(!empty($invoice_info) && $invoice_info->consignment_type == 'FCL') {
                                                $count = 0;
                                                foreach($container_info as $cont_info) {
                                                    ?>
													<div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-lg-2">
                                                    <label class="control-label">Type <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" name="container_type_fcl[]" value="<?= $cont_info->container_type ?>" required />
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="control-label">Container No <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" name="container_no_fcl[]"  value="<?= $cont_info->container_no ?>" required />
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="control-label">Container ft <span class="text-danger">*</span></label>
                                                    <select class="form-control select_box" name="container_ft_fcl[]"
															data-width="100%" required>
													  <option value="">-</option>
													  <option value="20ft" <?= ($cont_info->container_ft == '20ft') ? 'selected' : '' ?>>20ft</option>
													  <option value="40ft" <?= ($cont_info->container_ft == '40ft') ? 'selected' : '' ?>>40ft</option>
													  <option value="45ft" <?= ($cont_info->container_ft == '45ft') ? 'selected' : '' ?>>45ft</option>
													  <option value="Open Top" <?= ($cont_info->container_ft == 'Open Top') ? 'selected' : '' ?>>Open Top</option>
												   </select>
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="control-label">No. of Pkgs <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" name="no_of_pkgs_fcl[]"  value="<?= $cont_info->packages != 0.00?round($cont_info->packages,2):'' ?>" required/>
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="control-label">Total Weight <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" name="total_weight_fcl[]"  value="<?= $cont_info->container_total_weight != 0.00?round($cont_info->container_total_weight):'' ?>" required />
                                                </div>
                                                <?php
                                                    if($count > 0) {
                                                        ?>
														<div class="col-lg-2" style="margin-top:25px;">
															<button type="button" class="btn btn-danger btn-xs remCF"><i class="fa fa-minus"></i></button>
														</div>
                                                        <?php
                                                    }else {
                                                        ?>
														<div class="col-lg-2" style="margin-top:25px;">
															<button type="button" class="btn btn-primary btn-xs"
																	id="add_fcl"><i
																		class="fa fa-plus"></i></button>
														</div>
                                                        <?php
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                                    <?php
                                                    $count++;
                                                }
                                            }else {
                                                ?>
												<div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-lg-2">
                                                    <label class="control-label">Type <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" name="container_type_fcl[]" required/>
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="control-label">Container No <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" name="container_no_fcl[]" required/>
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="control-label">Container ft <span class="text-danger">*</span></label>
                                                    <select class="form-control select_box" name="container_ft_fcl[]"
															data-width="100%" required>
														  <option value="">-</option>
														  <option value="20ft">20ft</option>
														  <option value="40ft">40ft</option>
														  <option value="45ft">45ft</option>
														  <option value="Open Top">Open Top</option>
													   </select>
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="control-label">No. of Pkgs <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" name="no_of_pkgs_fcl[]" required/>
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="control-label">Total Weight <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" name="total_weight_fcl[]" required/>
                                                </div>
                                                <div class="col-lg-2" style="margin-top:25px;">
                                                    <button type="button" class="btn btn-primary btn-xs" id="add_fcl"><i
																class="fa fa-plus"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                                <?php
                                            }
                                        ?>
										<div id="add_new_fcl"></div>
                                    </div>
									<!---- END FCL CONTAINER ----->
									<!---- BREAK BULK CONTAINER ----->
                                    <div id="break_bulk" style="<?php
                                        if (!empty($invoice_info)) {
                                            echo ($invoice_info->consignment_type == 'Break Bulk') ? "display:block;" : "display:none;";
                                        } else {
                                            echo "display:none;";
                                        }
                                    ?> margin-top:10px;">
										<?php
                                            if(!empty($invoice_info) && $invoice_info->consignment_type == 'Break Bulk') {
                                                $count = 0;
                                                foreach ($container_info as $cont_info) {
                                                    ?>
													<div class="col-lg-3">
                                            <label class="control-label">No of Pkgs <span
														class="text-danger">*</span></label>
                                            <input type="text" class="form-control" name="no_of_pkgs" value="<?= $cont_info->packages != 0.00?round($cont_info->packages,2):'' ?>" required/>
                                        </div>
													<div class="col-lg-3">
                                            <label class="control-label">Total Net Weight <span
														class="text-danger">*</span></label>
                                            <input type="text" class="form-control" name="total_net_weight"  value="<?= $cont_info->container_net_weight != 0.00?round($cont_info->container_net_weight,2):'' ?>" required/>
                                        </div>
													<div class="col-lg-3">
                                            <label class="control-label">Total Gross Weight <span
														class="text-danger">*</span></label>
                                            <input type="text" class="form-control" name="total_gross_weight" value="<?= $cont_info->container_gross_weight != 0.00?round($cont_info->container_gross_weight,2):'' ?>" required/>
                                        </div>
                                                    <?php
                                                }
                                            }else {
                                                ?>
												<div class="col-lg-3">
                                            <label class="control-label">No of Pkgs <span
														class="text-danger">*</span></label>
                                            <input type="text" class="form-control" name="no_of_pkgs" required/>
                                        </div>
												<div class="col-lg-3">
                                            <label class="control-label">Total Net Weight <span
														class="text-danger">*</span></label>
                                            <input type="text" class="form-control" name="total_net_weight" required/>
                                        </div>
												<div class="col-lg-3">
                                            <label class="control-label">Total Gross Weight <span
														class="text-danger">*</span></label>
                                            <input type="text" class="form-control" name="total_gross_weight" required/>
                                        </div>
                                                <?php
                                            }
                                        ?>
                                    </div>
									<!---- END BREAK BULK CONTAINER ----->
									<!---- BY AIR CONTAINER ----->
                                    <div id="by_air" style="<?php
                                    if (!empty($invoice_info)) {
                                        echo ($invoice_info->consignment_type == 'By Air') ? "display:block;" : "display:none;";
                                    } else {
                                        echo "display:none;";
                                    }
                                    ?> margin-top:10px;">
										<?php
                                            if(!empty($invoice_info) && $invoice_info->consignment_type == 'By Air') {
                                                $count = 0;
                                                foreach($container_info as $cont_info) {
                                                    ?>
													<div class="col-md-12">
													<div class="form-group">
														<div class="col-lg-2">
															<label class="control-label">No of Pkgs <span class="text-danger">*</span></label>
															<input type="number" min="0" class="form-control"
																   name="no_of_pkgs_air[]"
																   value="<?= $cont_info->packages != 0.00 ? round($cont_info->packages) : '' ?>" required/>
														</div>
														<div class="col-lg-2">
															<label class="control-label">Shed <span class="text-danger">*</span></label>
															<select class="form-control select_box"
																	name="container_shed_air[]"
																	data-width="100%" required>
																<option value="">-</option>
                                                                <?php
                                                                    if (!empty($all_shed_info)) {
                                                                        foreach ($all_shed_info as $v_shed) {
                                                                            ?>
																			<option value="<?= $v_shed->shed_id ?>" <?= $v_shed->shed_id == $cont_info->container_shed ? 'selected' : '' ?>><?= ucfirst($v_shed->shed) ?></option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                ?>
															 </select>
														</div>
														<div class="col-lg-2">
															<label class="control-label">Total Net Weight <span class="text-danger">*</span></label>
															<input type="number" min="0" class="form-control"
																   name="total_net_weight_air[]"
																   value="<?= $cont_info->container_net_weight != 0.00 ? round($cont_info->container_net_weight) : '' ?>" required/>
														</div>
														<div class="col-lg-2">
															<label class="control-label">Total Gross Weight <span class="text-danger">*</span></label>
															<input type="number" min="0" class="form-control"
																   name="total_gross_weight_air[]"
																   value="<?= $cont_info->container_gross_weight != 0.00 ? round($cont_info->container_gross_weight) : '' ?>" required/>
														</div>
                                                        <?php
                                                            if($count > 0) {
                                                                ?>
																<div class="col-lg-2" style="margin-top:25px;">
															<button type="button" class="btn btn-danger btn-xs remCF"><i class="fa fa-minus"></i></button>
														</div>
                                                                <?php
                                                            }else {
                                                                ?>
																<div class="col-lg-2" style="margin-top:25px;">
															<button type="button" class="btn btn-primary btn-xs"
																	id="add_byair"><i
																		class="fa fa-plus"></i></button>
														</div>
                                                                <?php
                                                            }
                                                        ?>
													</div>
												</div>
                                                    <?php
                                                    $count++;
                                                }
                                            }else {
                                                ?>
												<div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-lg-2">
                                                    <label class="control-label">No of Pkgs <span class="text-danger">*</span></label>
                                                    <input type="number" min="0" class="form-control"
														   name="no_of_pkgs_air[]" required/>
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="control-label">Shed <span class="text-danger">*</span></label>
                                                    <select class="form-control select_box" name="container_shed_air[]"
															data-width="100%" required>
														<option value="">-</option>
                                                        <?php
                                                            if (!empty($all_shed_info)) {
                                                                foreach ($all_shed_info as $v_shed) {
                                                                    ?>
																	<option value="<?= $v_shed->shed_id ?>"><?= ucfirst($v_shed->shed) ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                        ?>
													 </select>
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="control-label">Total Net Weight <span class="text-danger">*</span></label>
                                                    <input type="number" min="0" class="form-control"
														   name="total_net_weight_air[]" required/>
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="control-label">Total Gross Weight <span class="text-danger">*</span></label>
                                                    <input type="number" min="0" class="form-control"
														   name="total_gross_weight_air[]" required/>
                                                </div>
                                                <div class="col-lg-2" style="margin-top:25px;">
                                                    <button type="button" class="btn btn-primary btn-xs" id="add_byair"><i
																class="fa fa-plus"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                                <?php
                                            }
                                        ?>
                                    </div>
                                    <div id="add_new_byair"></div>
									<!---- END BY AIR CONTAINER ----->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box box-default collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Financial Information</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="form-group">
							<?php
                                if(!empty($financial_info)) {
                                    ?>
									<input type="hidden" value="<?= encrypt($financial_info->financial_id) ?>" name="financial_id" />
                                    <?php
                                }
                            ?>
							<div class="col-lg-3">
                                <label class="control-label">Invoice No</label>
                                <input type="text" class="form-control" name="invoice_no" value="<?= (!empty($financial_info))?$financial_info->invoice_no:'' ?>"/>
                            </div>
                            <div class="col-lg-3">
                                <label class="control-label">Invoice Date</label>
								<div class="input-group">
                                    <input type="text" class="form-control datepicker" name="invoice_date"  value="<?php
                                        if (!empty($financial_info)) {
                                            echo ($financial_info->invoice_date != '0000-00-00') ? $financial_info->invoice_date : '';
                                        }
                                    ?>" autocomplete="off"/>
									<div class="input-group-addon">
                                        <a><i class="entypo-calendar"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">Payment Term <span class="text-danger">*</span></label>
                                <select class="form-control select_box" name="payment_term" id="payment_term" data-width="100%" required>
									<option value="">-</option>
									<option value="LC" <?= (!empty($financial_info) && $financial_info->payment_term == 'LC')?'selected':'' ?>>LC</option>
									<option value="Without LC" <?= (!empty($financial_info) && $financial_info->payment_term == 'Without LC')?'selected':'' ?>>Without LC</option>
								</select>
                            </div><div class="col-lg-2">
                                <label class="control-label">Incoterm <span class="text-danger">*</span></label>
                               <select class="form-control select_box" name="incoterm" id="incoterm" required>
									<option value="">-</option>
                                   <?php
                                       if (!empty($all_incoterm_info)) {
                                           foreach ($all_incoterm_info as $v_incoterm) {
                                               ?>
											   <option value="<?= $v_incoterm->incoterm_id ?>"
                                                   <?php
                                                       if (!empty($invoice_info)) {
                                                           if ($v_incoterm->incoterm_id == $financial_info->incoterm) {
                                                               echo 'selected';
                                                           }
                                                       }
                                                   ?>
											   ><?= ucfirst($v_incoterm->incoterm) ?></option>
                                               <?php
                                           }
                                       }
                                   ?>
								 </select>
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">Currency</label>
                                <label class="control-label"><?= lang('currency') ?> <span class="text-danger">*</span></label>
                                <?php
                                    if (empty($requisition_info)) {
                                        ?>
										<select class="form-control select_box" style="width: 100%" name="currency" required>
											<?php
                                                if (!empty($all_currencies)) {
                                                    foreach ($all_currencies as $v_currency) {
                                                        ?>
														<option value="<?= $v_currency->code ?>"
                                                            <?php
                                                                if (!empty($invoice_info)) {
                                                                    echo ($v_currency->code == $financial_info->currency) ? "selected" : '';
                                                                } else {
                                                                    echo ($v_currency->code == 'USD') ? "selected" : '';
                                                                }
                                                            ?>
														><?= $v_currency->name ?> (<?= $v_currency->code ?>)
											</option>
                                                        <?php
                                                    }
                                                }
                                            ?>
										 </select>
                                        <?php
                                    } else {
                                        ?>
										<input type="text" class="form-control" value="<?= $financial_info->currency ?>"
											   name="currency" readonly/>
                                        <?php
                                    }
                                ?>
                            </div>
                        </div>
						<div class="form-group bg-info" id="lc_container" style="<?php
                            if(!empty($financial_info)){
                                echo ($financial_info->payment_term == 'LC')?"display: block":"display: none";
                            }else{
                                echo "display: none";
                            }
                        ?>">
                            <div class="col-lg-3">
                                <label class="control-label">LC NO.</label>
                                <input type="text" class="form-control" name="lc_no" value="<?= (!empty($financial_info) && $financial_info->payment_term == 'LC')?$financial_info->lc_no:'' ?>" />
                            </div>
                            <div class="col-lg-3">
                                <label class="control-label">LC Date</label>
								<div class="input-group">
                                    <input type="text" class="form-control datepicker" name="lc_date"  value="<?php
                                        if (!empty($financial_info) && $financial_info->payment_term == 'LC') {
                                            echo ($financial_info->lc_date != '0000-00-00') ? $financial_info->lc_date : '';
                                        }
                                    ?>" autocomplete="off"/>
									<div class="input-group-addon">
                                        <a><i class="entypo-calendar"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <label class="control-label">Bank</label>
                                <input type="text" class="form-control" autocomplete="off" name="lc_bank" value="<?= (!empty($financial_info))?$financial_info->lc_bank:'' ?>" />
                            </div>
						</div>
                        <div class="form-group bg-info" id="fob_container" style="<?php
                            if(!empty($financial_info)){
                                echo ($financial_info->incoterm == 1)?"display: block":"display: none";
                            }else{
                                echo "display: none";
                            }
                        ?>">
                            <div class="col-lg-3">
                                <label class="control-label">FOB Value <span class="text-danger">*</span></label>
                                <input type="number" min="0" class="form-control fob" name="fob_value" id="fob_value" value="<?= (!empty($financial_info) && $financial_info->incoterm == 1 && $financial_info->fob_value != 0.00)?round($financial_info->fob_value,2):'' ?>" required />
                            </div>
                            <div class="col-lg-3">
                                <label class="control-label">Freight <span class="text-danger">*</span></label>
                                <input type="number" min="0" class="form-control fob" name="freight" id="freight" value="<?= (!empty($financial_info) && $financial_info->incoterm == 1 && $financial_info->freight != 0.00)?round($financial_info->freight,2):'' ?>" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-3">
                                <label class="control-label">CFR Value</label>
                                <input type="text" class="form-control" id="cfr_value" value="<?php
                                    if(!empty($financial_info)){
                                        echo ($financial_info->incoterm == 1)?number_format($financial_info->fob_value+$financial_info->freight,2):'';
                                    }
                                ?>" disabled />
                            </div>
                            <div class="col-lg-3">
                                <label class="control-label">Assessed Value</label>
                                <input type="text" class="form-control" id="assessed_value" value="<?php
                                    if(!empty($financial_info)){
                                        echo ($financial_info->incoterm == 1)?number_format($financial_info->fob_value+$financial_info->freight,2):'';
                                    }
                                ?>" disabled />
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">Other Charges</label>
                                <input type="number" min="0" class="form-control" name="other_charges" value="<?= (!empty($financial_info) && $financial_info->other_charges != 0.00)?round($financial_info->other_charges,2):'' ?>"/>
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">Exchange Rate <span class="text-danger">*</span></label>
                                <input type="number" min="0" class="form-control" name="exchange_rate" value="<?= (!empty($financial_info) && $financial_info->exchange_rate != 0.00)?round($financial_info->exchange_rate,2):'' ?>" required/>
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">EIF No</label>
                                <input type="text" class="form-control" name="eif_no"  value="<?= (!empty($financial_info))?$financial_info->eif_no:'' ?>"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box box-default collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Item</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
						 <?php
                             if (!empty($commodity_info)) {
                                 $counter = 0;
                                 foreach ($commodity_info as $commodity) {
                                     ?>
									 <div class="form-group">
							<input type="hidden" value="<?= encrypt($commodity->id) ?>" name="commodity_id[]" />
							<div class="col-lg-12">
								<label class="control-label">Item Description <span class="text-danger">*</span></label>
								<textarea class="form-control commodity" name="commodity_update[]" required><?= $commodity->commodity ?></textarea>
							</div>
							
                            <div class="col-lg-2">
                                <label class="control-label">Type <span class="text-danger">*</span></label>
                                <select class="form-control select_box" name="commodity_type_update[]" data-width="100%"
										autocomplete="off" required>
                                    <option value="">-</option>
                                    <?php
                                        if (!empty($all_commodity_types)) {
                                            foreach ($all_commodity_types as $item) {
                                                ?>
												<option value="<?= $item->commodity_type_id ?>" <?= $commodity->commodity_type == $item->commodity_type_id?'selected':'' ?>><?= $item->commodity_type ?></option>
                                                <?php
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">HS Code <span class="text-danger">*</span></label>
                                <input type="text" class="form-control hs_code" name="hs_code_update[]" value="<?= $commodity->hs_code ?>" required/>
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">Quantity <span class="text-danger">*</span></label>
                                <input type="number" min="0" class="form-control calculate_item no_of_units"
									   name="no_of_units_update[]" value="<?= round($commodity->no_of_units,2) ?>" required/>
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">UOM <span class="text-danger">*</span></label>
                                <select class="form-control select_box" name="weight_unit_update[]" data-width="100%"
										autocomplete="off" required>
                                    <option value="">-</option>
                                    <?php
                                        if (!empty($all_weight_unit_info)) {
                                            foreach ($all_weight_unit_info as $weight_unit) {
                                                ?>
												<option value="<?= $weight_unit->weight_unit_id ?>" <?= $commodity->weight_unit == $weight_unit->weight_unit_id?'selected':'' ?>><?= $weight_unit->weight_unit ?></option>
                                                <?php
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">Unit Value <span class="text-danger">*</span></label>
                                <input type="number" min="0" class="form-control calculate_item unit_value"
									   name="unit_value_update[]" value="<?= $commodity->unit_value != 0.00?round($commodity->unit_value,2):'' ?>" required/>
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">Unit Value Assessed  <span class="text-danger">*</span></label>
                                <input type="text" class="form-control calculate_item unit_value_assessed"
									   name="unit_value_assessed_update[]" value="<?= $commodity->unit_value_assessed != 0.00?round($commodity->unit_value_assessed,2):'' ?>" required />
                            </div>
							
                            <div class="col-lg-2">
                                <label class="control-label">No of Pcs</label>
                                <input type="number" min="0" class="form-control" name="no_of_pcs_update[]" value="<?= ($commodity->no_of_pcs != 0.00)?round($commodity->no_of_pcs,2):'' ?>"/>
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">Declared Value</label>
                                <input type="text" class="form-control declared_value"
									   disabled/>
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">Assessed Value</label>
                                <input type="text" class="form-control assessed_value"
									   disabled/>
                            </div>
                            <div class="col-lg-2" style="margin-top: 25px;">
                                <input type="checkbox" value="Yes" name="import_mode_update[]" <?= $commodity->temporary_import == 'Yes'?'checked':'' ?> class="temp minimal"/> Temporary Import
                            </div>
                            <div class="col-lg-1" style="margin-top: 25px;">
                                <input type="checkbox" value="Yes" class="sro minimal" name="sro_update[]" <?= $commodity->sro == 'Yes'?'checked':'' ?> /> SRO
                            </div>
                                         <?php
                                             if ($counter == 0) {
                                                
                                                 ?>
												 <div class="col-lg-2" style="margin-top:25px;">
                              <button type="button" class="btn btn-primary btn-xs"
									  id="add_item" style="border-radius:12px;"><i
										  class="fa fa-plus"></i></button>
                           </div>
                                                 <?php
                                             } else {
                                                
                                                 ?>
												 <div class="col-lg-2" style="margin-top:24px;">
                              <?= btn_delete_clone('admin/invoice/delete/delete_commodity/import/' . encrypt($invoice_info->invoices_id) . '/' . encrypt($commodity->id)) ?>
                           </div>
                                                 <?php
                                             }
                                        
                                         ?>
										 <div class="form-group">
                            <div class="col-lg-12">
								<div class="sro_div" style="<?= ($commodity->sro == 'Yes')?"display: block":"display: none" ?>">
									<div class="col-lg-2">
										<label class="control-label">SRO No.</label>
										<input type="text" name="sro_no_update[]" class="form-control sro_no" value="<?= ($commodity->sro == 'Yes')?$commodity->sro_no:'' ?>">
									</div>
									<div class="col-lg-2">
										<label class="control-label">Date</label>
										<div class="input-group">
											<input type="text" class="form-control datepicker" name="sro_date_update[]"
												   autocomplete="off"  value="<?= ($commodity->sro == 'Yes' && $commodity->sro_date != '0000-00-00')?$commodity->sro_date:'' ?>" />
											<div class="input-group-addon">
												<a><i class="entypo-calendar"></i></a>
											</div>
										</div>
									</div>
									<div class="col-lg-2">
										<label class="control-label">Serial No.</label>
										<input type="text" name="serial_no_update[]" class="form-control serial_no"  value="<?= ($commodity->sro == 'Yes')?$commodity->serial_no:'' ?>">
									</div>
									<div class="col-lg-3">
										<label class="control-label">Remarks</label>
										<textarea class="form-control" name="sro_remarks_update[]"><?= ($commodity->sro == 'Yes')?$commodity->sro_remarks:'' ?></textarea>
									</div>
								</div>
							</div>
                            <div class="col-lg-12">
								<div class="temp_div" style="<?= ($commodity->temporary_import == 'Yes')?"display: block":"display: none" ?>">
                                        <div class="col-lg-2">
                                            <label class="control-label">Security <span
														class="text-danger">*</span></label>
                                            <input type="text" name="security_update[]" class="form-control"  value="<?= ($commodity->temporary_import == 'Yes')?$commodity->security:'' ?>">
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="control-label">Instrument No </label>
                                            <input type="text" name="inst_no_update[]" class="form-control" value="<?= ($commodity->temporary_import == 'Yes')?$commodity->instrument_no:'' ?>">
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="control-label">Amount </label>
                                            <input type="number" min="0" name="import_amount_update[]" class="form-control" value="<?= ($commodity->temporary_import == 'Yes')?$commodity->temporary_amount:'' ?>">
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="control-label">Date of Issue </label>
											<div class="input-group">
												<input type="text" class="form-control datepicker"
													   name="date_of_issue_update[]" autocomplete="off"  value="<?= ($commodity->temporary_import == 'Yes' && $commodity->date_of_issue != '0000-00-00')?$commodity->date_of_issue:'' ?>"/>
												<div class="input-group-addon">
													<a><i class="entypo-calendar"></i></a>
												</div>
											</div>
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="control-label">Date of Expiry</label>
											<div class="input-group">
												<input type="text" class="form-control datepicker"
													   name="date_of_expiry_update[]" autocomplete="off" value="<?= ($commodity->temporary_import == 'Yes' && $commodity->date_of_expiry != '0000-00-00')?$commodity->date_of_expiry:'' ?>"/>
												<div class="input-group-addon">
													<a><i class="entypo-calendar"></i></a>
												</div>
											</div>
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="control-label">Shipping Bill No</label>
                                            <input type="text" name="shipping_bill_no_update[]" class="form-control" value="<?= ($commodity->temporary_import == 'Yes')?$commodity->shipping_bill:'' ?>">
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="control-label">Date</label>
											<div class="input-group">
												<input type="text" class="form-control datepicker"
													   name="shipping_date_update[]" autocomplete="off" value="<?= ($commodity->temporary_import == 'Yes' && $commodity->shipping_date != '0000-00-00')?$commodity->shipping_date:'' ?>"/>
												<div class="input-group-addon" >
													<a><i class="entypo-calendar"></i></a>
												</div>
											</div>
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="control-label">Permanent Retention</label>
                                            <input type="text" name="permanent_retention_update[]" class="form-control" value="<?= ($commodity->temporary_import == 'Yes')?$commodity->permanent_rentention:'' ?>">
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="control-label">Remarks</label>
                                            <textarea class="form-control" name="import_remarks_update[]"><?= ($commodity->temporary_import == 'Yes')?$commodity->temporary_remarks:'' ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                                     <?php
                                     $counter++;
                                 }
                             }else {
                                 ?>
								 <div class="form-group">
							<div class="col-lg-12">
								<label class="control-label">Item Description <span class="text-danger">*</span></label>
								<textarea class="form-control commodity" name="commodity[]" required></textarea>
							</div>
                            <div class="col-lg-2">
                                <label class="control-label">Type <span class="text-danger">*</span></label>
                                <select class="form-control select_box" name="commodity_type[]" data-width="100%"
										autocomplete="off" required>
                                    <option value="">-</option>
                                    <?php
                                        if (!empty($all_commodity_types)) {
                                            foreach ($all_commodity_types as $commodity) {
                                                ?>
												<option value="<?= $commodity->commodity_type_id ?>"><?= $commodity->commodity_type ?></option>
                                                <?php
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">HS Code <span class="text-danger">*</span></label>
                                <input type="text" class="form-control hs_code" name="hs_code[]" required/>
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">Quantity <span class="text-danger">*</span></label>
                                <input type="number" min="0" class="form-control calculate_item no_of_units"
									   name="no_of_units[]" required/>
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">UOM <span class="text-danger">*</span></label>
                                <select class="form-control select_box" name="weight_unit[]" data-width="100%"
										autocomplete="off" required>
                                    <option value="">-</option>
                                    <?php
                                        if (!empty($all_weight_unit_info)) {
                                            foreach ($all_weight_unit_info as $weight_unit) {
                                                ?>
												<option value="<?= $weight_unit->weight_unit_id ?>"><?= $weight_unit->weight_unit ?></option>
                                                <?php
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">Unit Value <span class="text-danger">*</span></label>
                                <input type="number" min="0" class="form-control calculate_item unit_value"
									   name="unit_value[]" required/>
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">Unit Value Assessed <span class="text-danger">*</span></label>
                                <input type="number" min="0" class="form-control calculate_item unit_value_assessed"
									   name="unit_value_assessed[]" required/>
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">No of Pcs</label>
                                <input type="number" min="0" class="form-control" name="no_of_pcs[]"/>
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">Declared Value</label>
                                <input type="text" class="form-control declared_value" name="declared_value[]"
									   disabled/>
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">Assessed Value</label>
                                <input type="text" class="form-control assessed_value" name="assessed_value[]"
									   disabled/>
                            </div>
                            <div class="col-lg-2" style="margin-top: 25px;">
                                <input type="checkbox" value="Yes" name="import_mode[]" class="temp minimal"/> Temporary Import
                            </div>
                            <div class="col-lg-1" style="margin-top: 25px;">
                                <input type="checkbox" value="Yes" class="sro minimal" name="sro[]"/> SRO
                            </div>
                            <div class="col-lg-1" style="margin-top:25px;">
                                <button type="button" class="btn btn-primary btn-xs" id="add_item"><i
											class="fa fa-plus"></i></button>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <div class="sro_div" style="display: none;">
                                        <div class="col-lg-2">
                                            <label class="control-label">SRO No.</label>
                                            <input type="text" name="sro_no[]" class="form-control sro_no">
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="control-label">Date</label>
											<div class="input-group">
												<input type="text" class="form-control datepicker" name="sro_date[]"
													   autocomplete="off"/>
												<div class="input-group-addon">
													<a><i class="entypo-calendar"></i></a>
												</div>
											</div>
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="control-label">Serial No.</label>
                                            <input type="text" name="serial_no[]" class="form-control serial_no">
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="control-label">Remarks</label>
                                            <textarea class="form-control" name="sro_remarks[]"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="temp_div" style="display: none;">
                                        <div class="col-lg-2">
                                            <label class="control-label">Security <span
														class="text-danger">*</span></label>
                                            <input type="text" name="security[]" class="form-control">
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="control-label">Instrument No </label>
                                            <input type="text" name="inst_no[]" class="form-control">
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="control-label">Amount </label>
                                            <input type="number" min="0" name="import_amount[]" class="form-control">
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="control-label">Date of Issue </label>
											<div class="input-group">
												<input type="text" class="form-control datepicker"
													   name="date_of_issue[]" autocomplete="off"/>
												<div class="input-group-addon">
													<a><i class="entypo-calendar"></i></a>
												</div>
											</div>
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="control-label">Date of Expiry</label>
											<div class="input-group">
												<input type="text" class="form-control datepicker"
													   name="date_of_expiry[]" autocomplete="off"/>
												<div class="input-group-addon">
													<a><i class="entypo-calendar"></i></a>
												</div>
											</div>
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="control-label">Shipping Bill No</label>
                                            <input type="text" name="shipping_bill_no[]" class="form-control">
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="control-label">Date</label>
											<div class="input-group">
												<input type="text" class="form-control datepicker"
													   name="shipping_date[]" autocomplete="off"/>
												<div class="input-group-addon">
													<a><i class="entypo-calendar"></i></a>
												</div>
											</div>
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="control-label">Permanent Retention</label>
                                            <input type="text" name="permanent_retention[]" class="form-control">
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="control-label">Remarks</label>
                                            <textarea class="form-control" name="import_remarks[]"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                                 <?php
                             }
                         ?>
						<div id="add_new_item"></div>
                    </div>
                </div>
				<!--- DUE DATES SECTION --->
                <?php
                    if (!empty($invoice_info)) {
                        ?>
                        <div class="box box-default collapsed-box">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?= lang('due_dates_section') ?></h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>

                            <div class="box-body">
                                 <?php
                                     if (!empty($due_dates_info)) {
                                         $counter = 0;
                                         $length = count($due_dates_info);
                                         foreach ($due_dates_info as $due_dates) {
                                             ?>
                                             <div class="form-group">
                                    <div class="col-lg-4">
                                       <input type="hidden" value="<?= encrypt($due_dates->due_date_id) ?>"
                                              name="due_date_id[]"/>
                                       <label class="control-label"><?= lang('due_date') ?></label>
                                       <input type="text" name="due_date_update[]"
                                              class="form-control datepicker"
                                              value="<?= ($due_dates->due_date != '0000-00-00') ? $due_dates->due_date : '' ?>"
                                              data-date-format="<?= config_item('date_picker_format'); ?>"
                                              placeholder="Y-m-d" autocomplete="off">
                                    </div>
                                    <div class="col-lg-4">
                                       <label class="control-label"><?= lang('due_time') ?></label>
                                       <input type="text" name="due_time_update[]"
                                              class="form-control timepicker"
                                              value="<?= (!empty($due_dates->due_time)) ? $due_dates->due_time : '' ?>"
                                              autocomplete="off">
                                    </div>
                                    <div class="col-lg-3">
                                       <label class="control-label"><?= lang('comment') ?></label>
                                       <textarea name="due_comment_update[]"
                                                 class="form-control"><?= $due_dates->due_comment ?></textarea>
                                    </div>
                                                 <?php
                                                     if ($counter == 0) {
                                                         ?>
                                                         <div style="margin-top:24px;">
                                       <button type="button" class="btn btn-primary btn-xs" id="add_more"><i class="fa fa-plus"></i>
                                       </button>
                                    </div>
                                                         <?php
                                                     } else {
                                                         ?>
                                                         <div style="margin-top:20px;">
                                       <?= btn_delete_clone('admin/invoice/delete/delete_due_dates/' . encrypt($due_dates->due_date_id) . '/' . encrypt($due_dates->invoices_id)) ?>
                                    </div>
                                                         <?php
                                                     }
                                                 ?>
                                 </div>
                                             <?php
                                             $counter++;
                                         }
                                     } else {

                                         ?>
                                         <div class="form-group">
                                    <div class="col-lg-4">
                                       <label class="control-label"><?= lang('date') ?></label>
                                       <input type="text" name="due_date[]" class="form-control datepicker"
                                              data-date-format="<?= config_item('date_picker_format'); ?>"
                                              placeholder="Y-m-d" autocomplete="off">
                                    </div>
                                    <div class="col-lg-4">
                                       <label class="control-label"><?= lang('time') ?></label>
                                       <input type="text" name="due_time[]" class="form-control timepicker"
                                              autocomplete="off">
                                    </div>
                                    <div class="col-lg-3">
                                       <label class="control-label"><?= lang('comment') ?></label>
                                       <textarea name="due_comment[]" class="form-control"></textarea>
                                    </div>
                                    <div style="margin-top:24px;">
                                       <button type="button" class="btn btn-primary btn-xs" id="add_more"><i class="fa fa-plus"></i></button>
                                    </div>
                                 </div>
                                         <?php
                                     }
                                 ?>
                                  <div id="add_new"></div>
                              </div>
                        </div>
                        <?php
                    }
                ?>
				<!--- END DUE DATES SECTION --->
				<!--- JOB STATUS SECTION --->
               <div class="box box-default collapsed-box">
                   <div class="box-header with-border">
                       <h3 class="box-title"><?= lang('job_status_section') ?></h3>
                       <div class="box-tools pull-right">
                           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                       </div>
                   </div>

                  <div class="box-body">
                     <?php
                         if (!empty($job_activity_info)) {
                             $counter = 0;
                             foreach ($job_activity_info as $job_activity) {
                                 ?>
								 <div class="form-group">
                        <div class="col-lg-3">
                           <input type="hidden"
								  value="<?= encrypt($job_activity->job_activity_id) ?>"
								  name="job_activity_id[]"/>
                           <label class="control-label"><?= lang('date') ?> <span
									   class="text-danger">*</span></label>
							<div class="input-group">
                                    <input type="text" name="job_activity_date_update[]"
										   class="form-control datepicker" autocomplete="off"
										   value="<?= $job_activity->job_activity_date ?>"
										   placeholder="Y-m-d" required>
									<div class="input-group-addon">
                                        <a><i class="entypo-calendar"></i></a>
                                    </div>
                                </div>
                        </div>
                        <div class="col-lg-4">
                           <label class="control-label"><?= lang('status') ?><span
									   class="text-danger">*</span></label>
                           <select class="form-control select_box"
								   name="job_activity_status_update[]" style="min-width: 400px" required>
                              <option value=""><?= lang('choose_status') ?></option>
                               <?php
                                   foreach ($all_job_status as $job_status) {
                                       ?>
									   <option value="<?= $job_status->job_status_id ?>" <?= ($job_status->job_status_id == $job_activity->job_activity_status) ? 'selected' : '' ?>><?= $job_status->job_status_title ?></option>
                                       <?php
                                   }
                               ?>
                           </select>
                        </div>
                        <div class="col-lg-2">
                           <div class="col-lg-2">
                              <label class="control-label" style="color: #000;font-weight: 400;">Notification</label>
                              <input type="checkbox" name="job_activity_notification_update[]"
									 value="Yes" <?= ($job_activity->job_activity_notification == 'Yes') ? 'checked' : '' ?>
									 data-style="ios" data-toggle="toggle" data-on=" "
									 data-off=" " data-onstyle="success" data-offstyle="default">
                           </div>
                        </div>
                                     <?php
                                         if ($counter == 0) {
                                             ?>
											 <div style="margin-top:24px;">
                           <button type="button" class="btn btn-primary btn-xs"
								   id="add_job_activity"><i
									   class="fa fa-plus"></i></button>
                        </div>
                                             <?php
                                         } else {
                                             ?>
											 <div style="margin-top:24px;">
                           <?= btn_delete_clone('admin/invoice/delete/delete_job_activity/' . encrypt($invoice_info->invoices_id) . '/' . encrypt($job_activity->job_activity_id)) ?>
                        </div>
                                             <?php
                                         }
                                     ?>
                     </div>
                                 <?php
                                 $counter++;
                             }
                         } else {
                             ?>
							 <div class="form-group">
                        <div class="col-lg-3">
                           <label class="control-label"><?= lang('date') ?> <span
									   class="text-danger">*</span></label>
							<div class="input-group">
                                    <input type="text" name="job_activity_date[]"
										   class="form-control datepicker" autocomplete="off"
										   placeholder="Y-m-d" required>
									<div class="input-group-addon">
                                        <a><i class="entypo-calendar"></i></a>
                                    </div>
                                </div>
                        </div>
                        <div class="col-lg-4">
                           <label class="control-label"><?= lang('status') ?><span
									   class="text-danger">*</span></label>
                           <select class="form-control select_box" name="job_activity_status[]" style="min-width: 400px" required>
                              <option value=""><?= lang('choose_status') ?></option>
                               <?php
                                   foreach ($all_job_status as $job_status) {
            
                                       ?>
									   <option value="<?= $job_status->job_status_id ?>"><?= $job_status->job_status_title ?></option>
                                       <?php
                                   }
    
                               ?>
                           </select>
                        </div>
                        <div class="col-lg-2">
                           <div class="col-lg-2">
                              <label class="control-label" style="color: #000;font-weight: 400;">Notification</label>
                              <input type="checkbox" name="job_activity_notification[]" value="Yes"
									 data-style="ios" data-toggle="toggle" data-on=" " data-off=" "
									 data-onstyle="success" data-offstyle="default"
									 class="toggle-radio">
                           </div>
                        </div>
                        <div style="margin-top:24px;">
                           <button type="button" class="btn btn-primary btn-xs" id="add_job_activity"><i class="fa fa-plus"></i></button>
                        </div>
                     </div>
                             <?php
                         }
                     ?>
					  <div id="add_new_job_activity"></div>
                  </div>
               </div>
				<!--- END JOB STATUS SECTION --->
				<!--- DOCUMENTS UPLOADING --->
               <div class="box box-default collapsed-box">
                   <div class="box-header with-border">
                       <h3 class="box-title">Documents Section</h3>
                       <div class="box-tools pull-right">
                           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                       </div>
                   </div>

                  <div class="box-body">
                     <?php
                         if (!empty($invoice_documents_info)) {
                             $counter = 0;
                             $length = count($invoice_documents_info);
                             ?>
							 <div class="table-responsive">
                        <table class="table table-bordered" style="margin:0px;">
                           <tr>
                              <th>Document Title</th>
                              <th>Date</th>
                              <th class="text-center">File</th>
                              <th class="text-center">Type</th>
                              <th>Comment</th>
                              <th class="text-center">Action</th>
                           </tr>
                            <?php
                                foreach ($invoice_documents_info as $invoice_documents) {
                                    ?>
									<tr>
                              <td class="col-md-2">
                                 <input type="hidden" name="document_id[]"
										value="<?= encrypt($invoice_documents->document_id) ?>"/>
                                 <select class="form-control select_box"
										 name="document_title_update[]">
                                    <option value="">---</option>
                                     <?php
                                         if (!empty($all_document_types)) {
                                             foreach ($all_document_types as $document_type) {
                                                 ?>
												 <option value="<?= $document_type->document_type_id ?>" <?= ($document_type->document_type_id == $invoice_documents->document_title) ? 'selected' : '' ?>><?= $document_type->document_title ?></option>
                                                 <?php
                                             }
                                         }
                                     ?>
                                 </select>
                              </td>
                              <td class="col-md-2"><input type="text"
														  class="form-control datepicker"
														  placeholder="Y-m-d" autocomplete="off"
														  name="document_date_update[]"
														  value="<?= ($invoice_documents->document_date != '0000-00-00') ? $invoice_documents->document_date : '' ?>"/></td>
                              <td class="text-center col-md-2">
                                 <input type="file" name="document_file_update[]"/>
                                  <?php
                                      if (!empty($invoice_documents->document_file)) {
                                          ?>
										  <a href="<?= base_url('') . $invoice_documents->document_file ?>"
											 target="_blank"><?php $explode = explode('/', $invoice_documents->document_file);
                                                  echo $explode[2]; ?> <?= btn_delete_file('admin/invoice/delete/delete_invoice_document_file/' . encrypt($invoice_documents->invoices_id) . '/' . encrypt($invoice_documents->document_id) . '/' . $explode[2]) ?></a>
                                          <?php
                                      }
                                  ?>
                              </td>
                              <td class="text-center col-md-2">
                                 <select
										 class="form-control select_box"
										 name="document_type_update[]">
                                    <option value="">---</option>
                                    <option value="original" <?php if (!empty($invoice_info)) {
                                        echo ($invoice_documents->document_type) == 'original' ? 'selected' : '';
                                    } ?>>Original</option>
                                    <option value="copy" <?php if (!empty($invoice_info)) {
                                        echo ($invoice_documents->document_type) == 'copy' ? 'selected' : '';
                                    } ?>>Copy</option>
                                 </select>
                              </td>
                              <td class="col-md-3">
                                 <textarea class="form-control"
										   placeholder="Your comment goes here..."
										   name="document_comment_update[]"><?= $invoice_documents->document_comment ?></textarea>
                              </td>
                                        <?php
                                            if ($counter == 0) {
                                                ?>
												<td class="text-center col-md-2"><button type="button"
																						 class="btn btn-primary btn-xs"
																						 id="add_document"><i
																class="fa fa-plus"></i></button></td>
                                                <?php
                                            } else {
                                                ?>
												<td class="text-center col-md-2"><?= btn_delete_clone('admin/invoice/delete/delete_invoice_document/' . encrypt($invoice_documents->invoices_id) . '/' . encrypt($invoice_documents->document_id) . '/' . $explode[2]) ?></td>
                                                <?php
                                            }
                                        ?>
                           </tr>
                                    <?php
                                    $counter++;
                                } ?>
                        </table>
                        <div id="add_new_document"></div>
                     </div>
                             <?php
                         } else {
                             ?>
							 <div class="table-responsive">
                        <table class="table table-bordered" style="margin:0px;">
                           <tr>
                              <td class="col-md-2">
								  <div class="document_dropdown">
									 <select class="form-control select_box document_types" name="document_title[]" data-width="100%">
										<option value="">---</option>
                                         <?php
                                             if (!empty($all_document_types)) {
                                                 foreach ($all_document_types as $document_type) {
                                                     ?>
													 <option value="<?= $document_type->document_type_id ?>"><?= $document_type->document_title ?></option>
                                                     <?php
                                                 }
                                             }
                                         ?>
									 </select>
								  </div>
                              </td>
                              <td class="col-md-2">
								  <div>
									  <div class="input-group">
											<input type="text" class="form-control datepicker"
												   placeholder="Y-m-d" autocomplete="off"
												   name="document_date[]"/>
											<div class="input-group-addon">
												<a><i class="entypo-calendar"></i></a>
											</div>
										</div>
								  </div>
							  </td>
                              <td class="text-center col-md-2">
								  <div>
										<input type="file" class="browse" name="document_file[]"/>
								  </div>
                              </td>
                              <td class="text-center col-md-2">
								  <div>
									 <select class="form-control select_box" data-width="100%"
											 name="document_type[]">
										<option value="">---</option>
										<option value="original">Original</option>
										<option value="copy">Copy</option>
									 </select>
								  </div>
                              </td>
                              <td class="col-md-3">
								  <div>
                                 		<textarea class="form-control" placeholder="Your comment goes here..."
												  name="document_comment[]"></textarea>
								  </div>
                              </td>
                              <td class="text-center col-md-2"><button type="button"
																	   class="btn btn-primary btn-xs"
																	   id="add_document"><i
											  class="fa fa-plus"></i></button></td>
                           </tr>
                        </table>
                        <div id="add_new_document"></div>
                     </div>
                             <?php
                         }
                     ?>
                  </div>
               </div>
				<!--- END DOCUMENTS UPLOADING --->

               <div class="form-group">
                  <div class="col-lg-12 text-center">
                     <button type="submit" class="btn btn-sm btn-success" id="submit"><i class="fa fa-check"></i>
                         <?php
                             if (!empty($invoice_info)) {
            
                                 echo lang('update_job_no');
            
                             } else {
            
                                 echo lang('create_job_no');
            
                             }
    
                         ?>
                     </button>
                  </div>
               </div>
            </div>
         </section>
      </form>
   </div>
</div>
<script type="text/javascript">
   $(document).ready(function () {
       <?php
       if(!empty($invoice_info)){
       ?>
       item_calculator();
       <?php
       }
       ?>
       $('#mode_of_shipment').on("change", function () {
           var mode = $(this).val();
           if (mode == 'air') {
               $('#consignment_air').show();
           } else {
               $('#consignment_air').hide();
           }
       });
    
       $('#payment_term').on("change", function () {
           var mode = $(this).val();
           if (mode == 'LC') {
               $('#lc_container').show();
           } else {
               $('#lc_container').hide();
           }
       });
       $('#incoterm').on("change", function () {
           var incoterm = $(this).val();
           if (incoterm == 1) {
               $('#fob_container').show();
               var fob = $('#fob_value').val() == '' ? 0 : parseFloat($('#fob_value').val());
               var freight = $('#freight').val() == '' ? 0 : parseFloat($('#freight').val());
               var cfr = fob+freight;
               $('#cfr_value').val(cfr.toFixed(2));
               $('#assessed_value').val(cfr.toFixed(2));
               $(".unit_value_assessed").attr("readonly","readonly");
           } else {
               $('#fob_container').hide();
               $('#cfr_value').val("");
               $('#assessed_value').val("");
               $(".unit_value_assessed").removeAttr("readonly");
               var total_decalred_value = 0;
               $('.declared_value').each(function () {
                   if ($(this).val() != '') {
                       total_decalred_value = total_decalred_value + parseFloat($(this).val());
                   }
               });
               var total_assessed_value = 0;
               $('.assessed_value').each(function () {
                   if ($(this).val() != '') {
                       total_assessed_value = total_assessed_value + parseFloat($(this).val());
                   }
               });
               $('#cfr_value').val(total_decalred_value.toFixed(2));
               $('#assessed_value').val(total_assessed_value.toFixed(2));
           }
           item_calculator();
       });
    
       $('.fob').keyup(function() {
           var fob = $('#fob_value').val() == '' ? 0 : parseFloat($('#fob_value').val());
           var freight = $('#freight').val() == '' ? 0 : parseFloat($('#freight').val());
           var cfr = fob+freight;
           $('#cfr_value').val(cfr.toFixed(2));
           $('#assessed_value').val(cfr.toFixed(2));
           item_calculator();
       });
   });

   $(function () {
       $('input[name="consignment_type"]').on('ifChanged', function (e) {
           var value = $(this).val();
           if (value == 'LCL') {
               $('#lcl').fadeIn(500).show();
               $('#fcl').hide();
               $('#break_bulk').hide();
               $('#by_air').hide();
           }else if (value == 'FCL') {
               $('#fcl').fadeIn(500).show();
               $('#lcl').hide();
               $('#break_bulk').hide();
               $('#by_air').hide();
           } else if (value == 'Break Bulk') {
               $('#break_bulk').fadeIn(500).show();
               $('#fcl').hide();
               $('#lcl').hide();
               $('#by_air').hide();
           }
           else if (value == 'By Air') {
               $('#by_air').fadeIn(500).show();
               $('#fcl').hide();
               $('#lcl').hide();
               $('#break_bulk').hide();
           }
       });
   });

   /*** COMMODITY SECTION ***/

   $(document).on('keyup', ".calculate_item", function () {
       item_calculator();
   });
   function item_calculator(){
       $(".calculate_item").each(function() {
           var $this = $(this);
           var unit = $this.parent().parent().find('.unit_value').val() == '' ? 0 : parseFloat($this.parent().parent().find('.unit_value').val());
           var no_of_units = $this.parent().parent().find('.no_of_units').val() == '' ? 0 : parseFloat($this.parent().parent().find('.no_of_units').val());
           if($("#incoterm").val() == 1) {
               $this.parent().parent().find('.unit_value_assessed').attr("readonly","readonly");
               var fob = $('#fob_value').val() == '' ? 0 : parseFloat($('#fob_value').val());
               var freight = $('#freight').val() == '' ? 0 : parseFloat($('#freight').val());
            
               if(fob > 0 && freight > 0 && unit > 0 && no_of_units > 0) {
                   var cfr = fob + freight;
                   var a = (unit / fob) * 100;
                   var b = a * freight / 100;
                   //var c = cfr + b;
                   var uva = b + unit;
                   var av = uva * no_of_units;
                   var dv = unit * no_of_units;

                   $this.parent().parent().find('.unit_value_assessed').val(uva.toFixed(2));
                   $this.parent().parent().find('.declared_value').val(dv.toFixed(2));
                   $this.parent().parent().find('.assessed_value').val(av.toFixed(2));
               }else{
                   $this.parent().parent().find('.unit_value_assessed').val("");
                   $this.parent().parent().find('.declared_value').val("");
                   $this.parent().parent().find('.assessed_value').val("");
               }
           }else{
               var unit_assessed = unit > 0?unit:'';
               $this.parent().parent().find('.unit_value_assessed').removeAttr("readonly");
               /*$this.parent().parent().find('.unit_value_assessed').val(unit_assessed);*/
               var assessed_unit = $this.parent().parent().find('.unit_value_assessed').val() == '' ? 0 : parseFloat($this.parent().parent().find('.unit_value_assessed').val());
            
               var declared_value = unit*no_of_units;
               var assessed_value = assessed_unit*no_of_units;
            
               $this.parent().parent().find('.declared_value').val(declared_value.toFixed(2));
               $this.parent().parent().find('.assessed_value').val(assessed_value.toFixed(2));
               var total_decalred_value = 0;
               $('.declared_value').each(function () {
                   if ($this.val() != '') {
                       total_decalred_value = total_decalred_value + parseFloat($this.val());
                   }
               });
               var total_assessed_value = 0;
               $('.assessed_value').each(function () {
                   if ($this.val() != '') {
                       total_assessed_value = total_assessed_value + parseFloat($this.val());
                   }
               });
               $('#cfr_value').val(total_decalred_value.toFixed(2));
               $('#assessed_value').val(total_assessed_value.toFixed(2));
           }
       });
   }

   $(function () {
       $(document).on('ifChanged', ".sro", function (e) {
           $(this).each(function () {
               if ($(this).is(':checked')) {
                   $(this).parent().parent().parent().find('.sro_div').fadeIn(500).show();
               }
               else {
                   $(this).parent().parent().parent().find('.sro_div').hide();
               }
           });
       });
    
   });

   $(function () {
       /* $(document).on('click', ".sro", function () {*/
       $(document).on('ifChanged', ".temp", function (e) {
           $(this).each(function () {
               if ($(this).is(':checked')) {
                   $(this).parent().parent().parent().find('.temp_div').fadeIn(500).show();
               }
               else {
                   $(this).parent().parent().parent().find('.temp_div').hide();
               }
           });
       });
    
   });
   /*** END COMMODITY SECTION ***/
   /*** LCL CONTAINER SECTION ***/

   $("#add_lcl").click(function () {
       var sheds = " <?php
           foreach ($all_shed_info as $v_shed) {
               echo "<option value=$v_shed->shed_id>$v_shed->shed</option>";
           }
           ?>";
       var columns = '<div class="col-md-12">';
       columns += '<div class="form-group">';
       columns += '<div class="col-lg-3">';
       columns += '<label class="control-label">Shed <span class="text-danger">*</span></label>';
       columns += '<select class="form-control select_box" name="container_shed_lcl[]" data-width="100%" required>';
       columns += '<option value="">-</option>'+sheds;
       columns += '</select>';
       columns += '</div>';
       columns += '<div class="col-lg-3">';
       columns += '<label class="control-label">No. of Pkgs <span class="text-danger">*</span></label>';
       columns += '<input type="number" min="0" class="form-control" name="no_of_pkgs_lcl[]" required />';
       columns += '</div>';
       columns += '<div class="col-lg-3">';
       columns += '<label class="control-label">Total Weight <span class="text-danger">*</span></label>';
       columns += '<input type="number" min="0" class="form-control" name="total_weight_lcl[]" required />';
       columns += '</div>';
       columns += '<div class="col-lg-2" style="margin-top:25px;">';
       columns += '<button type="button" class="btn btn-danger btn-xs remCF"><i class="fa fa-minus"></i></button>';
       columns += '</div>';
       columns += '</div>';
       columns += '</div>';
    
       var add_new = $(columns);
       $("#add_new_lcl").append(add_new.hide().fadeIn(1000));
       $('.select_box').select2({});
   });

   $("#add_new_lcl").on('click', '.remCF', function () {
       $(this).fadeOut(300, function () {
           $(this).parent().parent().remove();
       })
   });

   /*** END LCL CONTAINER SECTION ***/
   /*** FCL CONTAINER SECTION ***/

   $("#add_fcl").click(function () {
       var columns = '<div class="col-md-12">';
       columns += '<div class="form-group">';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Type <span class="text-danger">*</span></label>';
       columns += '<input type="text" class="form-control" name="container_type_fcl[]" required />';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Container No <span class="text-danger">*</span></label>';
       columns += '<input type="text" class="form-control" name="container_no_fcl[]" required />';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Container ft <span class="text-danger">*</span></label>';
       columns += '<select class="form-control select_box" name="container_ft_fcl[]" data-width="100%" required>';
       columns += '<option value="">-</option>';
       columns += '<option value="20ft">20ft</option>';
       columns += '<option value="40ft">40ft</option>';
       columns += '<option value="45ft">45ft</option>';
       columns += '<option value="Open Top">Open Top</option>';
       columns += '</select>';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">No. of Pkgs <span class="text-danger">*</span></label>';
       columns += '<input type="number" min="0" class="form-control" name="no_of_pkgs_fcl[]" required />';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Total Weight <span class="text-danger">*</span></label>';
       columns += '<input type="number" min="0" class="form-control" name="total_weight_fcl[]" required />';
       columns += '</div>';
       columns += '<div class="col-lg-2" style="margin-top:25px;">';
       columns += '<button type="button" class="btn btn-danger btn-xs remCF"><i class="fa fa-minus"></i></button>';
       columns += '</div>';
       columns += '</div>';
       columns += '</div>';
    
       var add_new = $(columns);
       $("#add_new_fcl").append(add_new.hide().fadeIn(1000));
       $('.select_box').select2({});
   });

   $("#add_new_fcl").on('click', '.remCF', function () {
       $(this).fadeOut(300, function () {
           $(this).parent().parent().remove();
       })
   });

   /*** END FCL CONTAINER SECTION ***/
   /*** BY AIR CONTAINER SECTION ***/

   $("#add_byair").click(function () {
       var sheds = " <?php
           foreach ($all_shed_info as $v_shed) {
               echo "<option value=$v_shed->shed_id>$v_shed->shed</option>";
           }
           ?>";
       var columns = '<div class="col-md-12">';
       columns += '<div class="form-group">';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">No of Pkgs <span class="text-danger">*</span></label>';
       columns += '<input type="number" min="0" class="form-control" name="no_of_pkgs_air[]" required />';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Shed <span class="text-danger">*</span></label>';
       columns += '<select class="form-control select_box" name="container_shed_air[]" data-width="100%" required>';
       columns += '<option value="">-</option>'+sheds;
       columns += '</select>';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Total Net Weight <span class="text-danger">*</span></label>';
       columns += '<input type="number" min="0" class="form-control" name="total_net_weight_air[]" required />';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Total Gross Weight <span class="text-danger">*</span></label>';
       columns += '<input type="number" min="0" class="form-control" name="total_gross_weight_air[]" required />';
       columns += '</div>';
       columns += '<div class="col-lg-2" style="margin-top:25px;">';
       columns += '<button type="button" class="btn btn-danger btn-xs remCF"><i class="fa fa-minus"></i></button>';
       columns += '</div>';
       columns += '</div>';
       columns += '</div>';
    
       var add_new = $(columns);
    
       $("#add_new_byair").append(add_new.hide().fadeIn(1000));
    
       $('.select_box').select2({});
    
   });

   $("#add_new_byair").on('click', '.remCF', function () {
    
       $(this).fadeOut(300, function () {
           $(this).parent().parent().remove();
       })
    
   });

   /*** END BY AIR CONTAINER SECTION ***/
   /*** ITEM CONTAINER SECTION ***/

   $("#add_item").click(function () {
       var commodity_type = " <?php
           foreach ($all_commodity_types as $commodity) {
               echo "<option value=$commodity->commodity_type_id>$commodity->commodity_type</option>";
           }
           ?>";
       var weight_unit = " <?php
           foreach ($all_weight_unit_info as $weight_unit) {
               echo "<option value=$weight_unit->weight_unit_id>$weight_unit->weight_unit</option>";
           }
           ?>";
       var columns = '<div class="form-group bg-info">';
       columns += '<div class="col-lg-12">';
       columns += '<label class="control-label">Item Description <span class="text-danger">*</span></label>';
       columns += '<textarea class="form-control commodity" name="commodity[]" required></textarea>';
       columns += ' </div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Type <span class="text-danger">*</span></label>';
       columns += '<select class="form-control select_box" name="commodity_type[]" data-width="100%" autocomplete="off" required>';
       columns += '<option value="">-</option>'+commodity_type;
       columns += '</select>';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">HS Code <span class="text-danger">*</span></label>';
       columns += '<input type="text" class="form-control hs_code" name="hs_code[]" required />';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Quantity <span class="text-danger">*</span></label>';
       columns += '<input type="number" min="0" class="form-control calculate_item no_of_units" name="no_of_units[]" required />';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">UOM <span class="text-danger">*</span></label>';
       columns += '<select class="form-control select_box" name="weight_unit[]" data-width="100%" autocomplete="off" required>';
       columns += '<option value="">-</option>'+weight_unit;
       columns += '</select>';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Unit Value <span class="text-danger">*</span></label>';
       columns += '<input type="number" min="0" class="form-control calculate_item unit_value" name="unit_value[]" required />';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Unit Value Assessed <span class="text-danger">*</span></label>';
       columns += '<input type="number" min="0" class="form-control calculate_item unit_value_assessed" name="unit_value_assessed[]" required />';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">No of Pcs</label>';
       columns += '<input type="number" min="0" class="form-control" name="no_of_pcs[]" />';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Declared Value</label>';
       columns += '<input type="text" class="form-control declared_value" name="declared_value[]" disabled />';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Assessed Value</label>';
       columns += '<input type="text" class="form-control assessed_value" name="assessed_value[]" disabled />';
       columns += '</div>';
       columns += '<div class="col-lg-2" style="margin-top: 25px;">';
       columns += '<input type="checkbox" value="Yes" name="import_mode[]" class="temp minimal" /> Temporary Import';
       columns += '</div>';
       columns += '<div class="col-lg-1" style="margin-top: 25px;">';
       columns += '<input type="checkbox" value="Yes" class="sro minimal" name="sro[]" /> SRO';
       columns += '</div>';
       columns += '<div class="col-lg-1" style="margin-top:25px;">';
       columns += '<button type="button" class="btn btn-danger btn-xs remCF"><i class="fa fa-minus"></i></button>';
       columns += '</div>';
       columns += '<div class="form-group">';
       columns += '<div class="col-lg-12">';
       columns += '<div class="sro_div" style="display: none;">';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">SRO No.</label>';
       columns += '<input type="text" name="sro_no[]" class="form-control sro_no">';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Date</label>';
       columns += '<div class="input-group">';
       columns += '<input type="text" class="form-control datepicker delivery_date" name="sro_date[]" autocomplete="off"/>';
       columns += '<div class="input-group-addon">';
       columns += '<a><i class="entypo-calendar"></i></a>';
       columns += '</div>';
       columns += '</div>';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Serial No.</label>';
       columns += '<input type="text" name="serial_no[]" class="form-control serial_no">';
       columns += '</div>';
       columns += '<div class="col-lg-3">';
       columns += '<label class="control-label">Remarks</label>';
       columns += '<textarea class="form-control" name="sro_remarks[]"></textarea>';
       columns += '</div>';
       columns += '</div>';
       columns += '<div class="temp_div" style="display: none;">';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Security</label>';
       columns += '<input type="text" name="security[]" class="form-control">';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Instrument No </label>';
       columns += '<input type="text" name="inst_no[]" class="form-control">';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Amount</label>';
       columns += '<input type="number" min="0" name="import_amount[]" class="form-control">';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Date of Issue</label>';
       columns += '<input type="text" name="date_of_issue[]" class="form-control datepicker" autocomplete="off">';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Date of Expiry</label>';
       columns += '<input type="text" name="date_of_expiry[]" class="form-control datepicker" autocomplete="off">';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Shipping Bill No</label>';
       columns += '<input type="text" name="shipping_bill_no[]" class="form-control">';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Date</label>';
       columns += '<div class="input-group">';
       columns += '<input type="text" class="form-control datepicker" placeholder="Y-m-d" autocomplete="off" name="shipping_date[]"/>';
       columns += '<div class="input-group-addon">';
       columns += '<a><i class="entypo-calendar"></i></a>';
       columns += '</div>';
       columns += '</div>';
       columns += '</div>';
       columns += '<div class="col-lg-2">';
       columns += '<label class="control-label">Permanent Retention</label>';
       columns += '<input type="text" name="permanent_retention[]" class="form-control">';
       columns += '</div>';
       columns += '<div class="col-lg-3">';
       columns += '<label class="control-label">Remarks</label>';
       columns += '<textarea class="form-control" name="import_remarks[]"></textarea>';
       columns += '</div>';
       columns += '</div>';
       columns += '</div>';
       columns += '</div>';
       columns += '</div>';
    
       var add_new = $(columns);
       $("#add_new_item").append(add_new.hide().fadeIn(1000));
       $('.select_box').select2({});
       $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
           checkboxClass: 'icheckbox_square-blue',
           radioClass: 'iradio_square-blue'
       })
       /*item_calculator($('.calculate_item'));*/
   });

   $("#add_new_item").on('click', '.remCF', function () {
       $(this).fadeOut(300, function () {
           $(this).parent().parent().remove();
       })
   });
   /*** END ITEM CONTAINER SECTION ***/

   $(document).ready(function () {
    
       var maxAppend = 0;
    
       /*** DUE DATES SECTION ***/
    
       $("#add_more").click(function () {
        
           var columns = '<div class="col-lg-4"><label class="control-label"><?= lang('date') ?> <span class="text-danger">*</span></label><input type="text" name="due_date[]"  class="form-control datepicker" data-date-format="<?= config_item('date_picker_format'); ?>" placeholder="Y-m-d" autocomplete="off" required></div><div class="col-lg-4"><label class="control-label"><?= lang('due_time') ?></label><input type="text" name="due_time[]"  class="form-control timepicker" autocomplete="off"></div><div class="col-lg-3"><label class="control-label"><?= lang('comment') ?></label><textarea name="due_comment[]" class="form-control"></textarea></div><div style="margin-top:24px;"><a href="javascript:void(0);" class="remCF btn btn-danger btn-xs"><i class="fa fa-minus"></i></a></div>';
        
           var add_new = $('<div class="form-group">' + columns + '</div>');
        
           maxAppend++;
        
           $("#add_new").append(add_new.hide().fadeIn(1000));
        
       });
    
    
       $("#add_new").on('click', '.remCF', function () {
        
           $(this).fadeOut(300, function () {
               $(this).parent().parent().remove();
           })
        
       });
    
       $(document).on('focus', '.datepicker', function () {
        
           $(this).datepicker({format: 'yyyy-mm-dd', autoclose: true});
        
           $('.select_box').select2({});
        
       });
    
       $(document).on('focus', '.timepicker', function () {
        
           $(this).timepicker({format: 'hh:mm A'});
        
       });
    
       /*** END DUE DATES SECTION ***/
    
       /*** JOB STATUS SECTION ***/
    
       $("#add_job_activity").click(function () {
        
           var status = " <?php
               foreach ($all_job_status as $job_status) {
                
                   echo "<option value=$job_status->job_status_id>$job_status->job_status_title</option>";
                
               }
            
               ?>";
        
           var column = '<div class="col-lg-3">';
        
           column += '<label class="control-label"><?= lang('date') ?> <span class="text-danger">*</span></label>';
        
           column += '<div class="input-group">';
           column += '<input type="text" class="form-control datepicker" placeholder="Y-m-d" autocomplete="off" name="job_activity_date[]"/>';
           column += '<div class="input-group-addon">';
           column += '<a><i class="entypo-calendar"></i></a>';
           column += '</div>';
           column += '</div>';
           column += '</div>';
        
           column += '<div class="col-lg-4">';
        
           column += '<label class="control-label"><?= lang('status') ?> <span class="text-danger">*</span></label>';
        
           column += '<select name="job_activity_status[]" class="form-control select_box" data-width="100%" required>';
        
           column += '<option value="">Choose Status</option>' + status + '</select>';
        
           column += '</div>';
        
           column += '<div class="col-lg-2" style="padding:17px 0px 0px 29px;">';
        
           column += '<input type="checkbox" name="job_activity_notification[]" value="Yes" class="jobActivity" data-style="ios" data-toggle="toggle" data-on=" " data-off=" " data-onstyle="success" data-offstyle="default"/>';
        
           column += '</div>';
        
           column += '<div style="margin-top:24px;">';
        
           column += '<a href="javascript:void(0);" class="remCF btn btn-danger btn-xs"><i class="fa fa-minus"></i></a>';
        
           column += '</div>';
        
           var add_new = $('<div class="form-group">' + column + '</div>');
        
           maxAppend++;
        
           $("#add_new_job_activity").append(add_new.hide().fadeIn(1000));
        
           $('.select_box').select2({});
        
           $('.jobActivity').bootstrapToggle({
            
               on: '',
            
               off: ''
            
           });
        
       });
    
    
       $("#add_new_job_activity").on('click', '.remCF', function () {
        
           $(this).fadeOut(300, function () {
               $(this).parent().parent().remove();
           })
        
       });
    
       $(document).on('focus', '.datepicker', function () {
        
           $(this).datepicker({format: 'yyyy-mm-dd', autoclose: true});
        
           $('.select_box').select2({});
        
       });
    
       /*** END JOB STATUS SECTION ***/
       /*$(document).on('keyup', ".commodity", function () {
           $(this).each(function () {
               if ($(this).val().length > 0) {
                   $(this).parent().parent().find('.hs_code').addClass('spinner');
                   var value = $(this).val();
                   $.ajax({
                       url: "admin/invoice/hs_code",
                       type: "POST",
                       dataType: "json",
                       data: {"hs_code": value},
                       success: (data) => {
                           $(this).parent().parent().find('.hs_code').removeClass('spinner').val(data);
                       },
                       error: function (data) {
                           console.log('error');
                       }
                   });
               }
           });
       });*/
    
       $(document).on('keyup', "#reference_no", function () {
           if ($(this).val().length > 0) {
               $(this).addClass('spinner');
               var value = $(this).val();
               $.ajax({
                   url: "<?php echo base_url(); ?>admin/invoice/reference_no/<?= !empty($invoice_info)?encode($invoice_info->invoices_id):'' ?>",
                   type: "POST",
                   dataType: "json",
                   data: {"reference_no": value},
                   success: function (data) {
                       if (data) {
                           $('#reference_error').text("");
                           $('#submit').removeAttr('disabled');
                           $('#reference_no').removeClass('spinner');
                       }
                       else {
                           $('#reference_error').text("Job Number already exist");
                           $('#submit').attr('disabled', 'disabled');
                           $('#reference_no').removeClass('spinner');
                       }
                   },
                   error: function (data) {
                       console.log('error');
                   }
               });
           }
           else {
               $('#reference_error').text("");
               $('#submit').removeAttr('disabled');
               $(this).removeClass('spinner')
           }
       });
    
   });

   /*** END COMMODITIES SECTION ***/

   /*** DOCUMENTS SECTION ***/

   $("#add_document").click(function () {
       var titles = " <?php
           foreach ($all_document_types as $document_type) {
               echo "<option value=$document_type->document_type_id>$document_type->document_title</option>";
           }
           ?>";
    
       var columns = '<table class="table table-bordered" style="margin:0px;">';
    
       columns += '<tr>';
    
       columns += '<td class="col-md-2"><div class="document_dropdown"><select class="form-control select_box document_types" name="document_title[]" data-width="100%"><option value="">---</option>' + titles + '</select></div></td>';
    
       columns += '<td class="col-md-2">';
       columns += '<div class="input-group">';
       columns += '<input type="text" class="form-control datepicker" placeholder="Y-m-d" autocomplete="off" name="document_date[]"/>';
       columns += '<div class="input-group-addon">';
       columns += '<a><i class="entypo-calendar"></i></a>';
       columns += '</div>';
       columns += '</div>';
       columns += '</td>';
    
       columns += '<td class="text-center col-md-2">';
    
       columns += '<input type="file" class="browse" name="document_file[]" />';
    
       columns += '</td>';
    
       columns += '<td class="text-center col-md-2"><select class="form-control select_box" name="document_type[]">';
    
       columns += '<option>---</option>';
    
       columns += '<option value="original">Original</option>';
    
       columns += '<option value="copy">Copy</option>';
    
       columns += '</select></td>';
    
       columns += '<td class="col-md-3">';
    
       columns += '<textarea class="form-control" placeholder="Your comment goes here..." name="document_comment[]"></textarea>';
    
       columns += '</td>';
    
       columns += '<td class="text-center col-md-2"><button type="button" class="btn btn-danger btn-xs remCF"><i class="fa fa-minus"></i></button></td>';
    
       columns += '</tr>';
    
       columns += '</table>';
    
       var add_new = $(columns);
    
       $("#add_new_document").append(add_new.hide().fadeIn(1000));
    
       $('.select_box').select2({});
    
   });


   $("#add_new_document").on('click', '.remCF', function () {
    
       $(this).fadeOut(300, function () {
           $(this).parent().parent().remove();
       })
    
   });

   /*** END CONTAINER SECTION ***/
   /*** CONSIGNOR SECTION ***/
   function selectConsignors(val) {
       var option="";
       if(val == '') {
           $('#consignors').hide();
       }
       else{
           $.getJSON("<?php echo site_url('admin/invoice/get_client_consignors') ?>" + "/" + val, function (result) {
               $.each(result, function (index, value) {
                   option = ('<option value="' + value.consignor_id + '">' + value.name + '</option>')+option;
               });
               var drop = '<div class="col-lg-3"><label class="control-label"><?= lang('consignors') ?></label><select class="form-control select_box" name="consignor_id" data-width="100%" onchange="getConsignorDetails(this.value)"><option value="">---</option>' + option + '</select></div>';
               $('#consignors').html(drop).hide().fadeIn(500);
               $('.select_box').select2({});
           });
       }
   }
   function getConsignorDetails(val){
       if(val != ""){
           $.getJSON( "<?php echo site_url('admin/invoice/get_consignor_details') ?>"+"/"+val, function(result) {
               $("#consignor_details").show();
               $("#consignor_email").val(result.email);
               $("#consignor_contact").val(result.mobile);
               $("#consignor_address").text(result.address);
           });
       }else{
           $("#consignor_details").hide();
       }
   }
   /*** END CONSIGNOR SECTION ***/

   $(document).on('change', '.browse', function () {
       $(this).each(function(){
           var file_name = this.value;
           console.log(file_name);
           if(file_name != "") {
               $(this).closest('tr').find('.document_types').attr('required','required');
           }
           else{
               $(this).closest('tr').find('.document_types').removeAttrs('required','required');
           }
       });
   });

   /** Fancy Checkbox & Radio**/
   $(function () {
       $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
           checkboxClass: 'icheckbox_square-blue',
           radioClass: 'iradio_square-blue'
       })
   })

</script>