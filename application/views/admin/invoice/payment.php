

<div class="row">

    <div class="col-sm-3">
<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('all_invoices') ?></h1>
        <div class="panel panel-default">

            <div class="panel-heading">

                <?php

                if ($role == '1') {

                    ?>

                    <a style="margin-top: -5px;" href="<?= base_url() ?>admin/invoice/manage_invoice/create_invoice" data-original-title="New Job" data-toggle="tooltip" data-placement="top" class="btn btn-icon btn-<?= config_item('button_color') ?> btn-sm pull-right"><i class="fa fa-plus"></i></a>

                <?php } ?>

                <?= lang('all_invoices') ?>

            </div>



            <div class="panel-body">

                <section class="scrollable  ">

                    <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="5px" data-color="#333333">

                        <ul class="nav"><?php

                            $all_invoices_info = $this->db->where('client_id',$client_payment_info->client_id)->get('tbl_invoices')->result();

                            if (!empty($all_invoices_info)) {

                                foreach ($all_invoices_info as $v_invoices) {

                                    $client_info = $this->invoice_model->check_by(array('client_id' => $v_invoices->client_id), 'tbl_client');

                                    $bill_info = $this->invoice_model->check_by(array('invoices_id' => $v_invoices->invoices_id), 'tbl_bills');

                                    $payment_info = $this->invoice_model->check_by(array('invoices_id' => $v_invoices->invoices_id), 'tbl_payments');

                                    if($this->invoice_model->get_payment_status($v_invoices->invoices_id) == lang('fully_paid')){

                                        $invoice_status = lang('fully_paid');

                                        $label = "success";

                                    }

                                    elseif($this->invoice_model->get_payment_status($v_invoices->invoices_id) == lang('partially_paid')){

                                        $invoice_status = lang('partially_paid');

                                        $label = "info";

                                    }

                                    else{

                                        $invoice_status = lang('not_paid');

                                        $label = "danger";

                                    }

                                    if(!empty($bill_info)){

                                        ?>

                                        <li class="<?php

                                        if ($v_invoices->invoices_id == decode($this->uri->segment(5))) {

                                            echo "active";

                                        }

                                        ?>">

                                            <a href="<?= base_url() ?>admin/invoice/manage_invoice/payment/<?= encode($v_invoices->invoices_id) ?>/<?= encode($client_payment_info->client_id) ?>">

                                                <?= $client_info->name ?>

                                                <div class="pull-right">

                                                    PKR  <?= number_format($this->invoice_model->calculate_bill('bill_grand_total',$bill_info->bill_id), 2) ?>

                                                </div> <br>

                                                <small class="block small text-muted"><?= $this->invoice_model->job_no_creation($v_invoices->invoices_id) ?> <br><span class="label label-<?= $label ?>"><?= $invoice_status ?></span></small>

                                            </a>

                                        </li>

                                        <?php

                                    }

                                }

                            }

                            ?>

                        </ul>



                    </div></section>

            </div>

        </div>

    </div>

    <section class="col-sm-9">

        <?= message_box('error') ?>

        <!-- Start create invoice -->
<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('pay_invoice') ?></h1>
        <section class="panel panel-default" >

            <?php $payment_info = $this->invoice_model->check_by(array('invoices_id' => $this->uri->segment(5)), 'tbl_payments'); ?>

            <header class="panel-heading"><?= lang('pay_invoice') ?>

                <?php

                if(!empty($payment_info)){

                    ?>

                    <a href="<?= base_url() ?>admin/invoice/manage_invoice/payments_details/<?= encode($payment_info->payments_id) ?>/<?= encode($this->uri->segment(5)) ?>" class="btn btn-warning btn-xs pull-right">Payment History</a>

                    <?php

                }

                ?>

            </header>

            <div class="panel-body">

                <form method="post" action="<?= base_url() ?>admin/invoice/get_payment/<?= encrypt($invoice_info->invoices_id) ?>" class="form-horizontal" role="form" id="form">

                    <input type="hidden" name="currency" value="PKR">

                    <div class="form-group">

                        <label class="col-lg-3 control-label"><?= lang('trans_id') ?> <span class="text-danger">*</span></label>

                        <div class="col-lg-6">

                            <?php $this->load->helper('string'); ?>

                            <input type="text" class="form-control" value="<?= random_string('nozero', 6); ?>" name="trans_id" readonly>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-lg-3 control-label"><?= lang('amount') ?> (PKR) <span class="text-danger">*</span></label>

                        <div class="col-lg-6">

                            <input type="text" required="" class="form-control" value="<?= round($this->invoice_model->calculate_to('invoice_due', $invoice_info->invoices_id), 2) ?>" name="amount">

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-lg-3 control-label"><?= lang('received_by') ?></label>

                        <div class="col-lg-6">

                            <input type="text" class="form-control"  name="received_by">

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-lg-3 control-label"><?= lang('payment_date') ?></label>

                        <div class="col-lg-6">

                            <!--<div class="input-group">-->

                            <input type="text" required="" name="payment_date"  class="form-control datepicker" value="<?php

                            if (!empty($payment_info->payment_date)) {

                                echo $payment_info->payment_date;

                            } else {

                                echo date('Y-m-d');

                            }

                            ?>" data-date-format="<?= config_item('date_picker_format'); ?>">


                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-lg-3 control-label"><?= lang('payment_mode') ?> <span class="text-danger">*</span></label>

                        <div class="col-lg-6">

                            <select name="payment_type" required="" class="form-control select_box" id="type">

                                <option value="1">Cash in Hand</option>

                                <option value="2">Cash at Bank</option>

                            </select>

                        </div>

                    </div>

                    <div id="cash_at_bank">

                        <div class="form-group">

                            <label class="col-lg-3 control-label">Banks <span class="text-danger">*</span></label>

                            <div class="col-lg-6">

                                <select name="bank" required="" class="form-control cash_at_bank select_box" onchange="selectBranches(this.value)" style="width:100%;">

                                    <option value="">Choose Bank</option>

                                    <?php foreach ($banks as $bank): ?>

                                        <option value="<?php echo $bank->B_ID; ?>"><?php echo $bank->B_NAME; ?></option>

                                    <?php endforeach; ?>

                                </select>

                            </div>

                        </div>

                        <div id="branches"></div>

                        <input type="hidden" id="account_id" name="account_id" />

                        <div class="form-group">

                            <label class="col-lg-3 control-label">Cheque No / Pay Order No <span class="text-danger">*</span></label>

                            <div class="col-lg-6">

                                <input type="text" class="form-control cash_at_bank" name="cheque_payorder_no" required />

                            </div>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-lg-3 control-label"><?= lang('notes') ?></label>

                        <div class="col-lg-6">

                            <textarea name="notes" class="form-control"></textarea>

                        </div>

                    </div>



                    <!--<div class="form-group">

                        <label class="col-lg-3 control-label"><?/*= lang('send_email') */?></label>

                        <div class="col-lg-6">

                            <label >

                                <input type="checkbox" class="custom-checkbox" name="send_thank_you">

                                <span></span>

                            </label>

                        </div>

                    </div>-->

                    <div class="form-group">

                        <label class="col-lg-3"></label>

                        <div class="col-lg-6">

                            <button type="submit" class="btn btn-primary"><?= lang('add_payment') ?></button>

                        </div>

                    </div>

                </form>

            </div>

        </section>

    </section>

</div>

<script type="text/javascript">

    $('#cash_at_bank').hide();

    $('#type').on("change", function() {

        var type = $(this).val();

        if(type == '1'){

            $('#cash_at_bank').fadeOut(500, function() { $(this).hide(); });

            $(".cash_at_bank").attr("disabled","disabled");

            $(".cash_at_bank").removeAttr('required');

        }

        else{

            $('#cash_at_bank').fadeIn(500).show();

            $(".cash_at_bank").removeAttr("disabled");

            $(".cash_at_bank").prop('required',true);

        }

    });

    function selectBranches(val) {

        var option="";

        if(val == '') {

            $('#branches').hide();

        }

        else{

            $.getJSON("<?php echo site_url('admin/accounts/ajax_select_branches') ?>" + "/" + val, function (result) {

                /*option.empty();*/

                $.each(result, function (index, value) {

                    option = ('<option value="' + value.BR_ID + '">' + value.BR_NAME + '</option>')+option;

                });

                var drop = '<div class="form-group"><label class="col-lg-3 control-label">Branches <span class="text-danger">*</span></label><div class="col-lg-6"><select name="branch_id" required="" class="form-control cash_at_bank select_box" onchange="getAccountId(this.value)"><option value="">Choose Branch</option>' + option + '</select></div></div>';

                $('#branches').html(drop).hide().fadeIn(500);

                $('.select_box').select2({});

                getAccountId();

            });

        }

    }

    function getAccountId(val){

        account_id=$('#account_id');

        $.getJSON( "<?php echo site_url('admin/accounts/ajax_get_branch_aid') ?>"+"/"+val, function(result) {

            (result != null)?account_id.val(result.A_ID):'';

        });

    }

</script>

<!-- end -->