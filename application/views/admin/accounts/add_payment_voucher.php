<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('payment_voucher') ?></h1>
<div class="row">
    <div class="col-lg-12">
        <form role="form" enctype="multipart/form-data" id="form" action="<?php echo base_url(); ?>admin/accounts/save_payment_voucher" method="post" class="form-horizontal  ">
            <section class="panel panel-default">
                <header class="panel-heading"><?= $title ?></header>
                <input type="hidden" name="payment_type" value="1">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><?= lang('payment_mode') ?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <select name="payment_type" class="form-control select_box required_select" id="type" data-width="100%" onchange="getAccountBalance(this.value)">
                                <option value="1">Cash</option>
                                <option value="2">Bank</option>
                            </select>
                        </div>
                    </div>
                    <div id="cash_at_bank" style="display:none;">
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Banks <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="bank" class="form-control cash_at_bank select_box required_select" onchange="selectBranches(this.value)" style="width:100%;">
                                    <option value="">-</option>
                                    <?php foreach ($banks as $bank): ?>
                                        <option value="<?php echo $bank->B_ID; ?>"><?php echo $bank->B_NAME; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div id="branches">
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Branches <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <select name="branch_id" class="form-control cash_at_bank select_box" onchange="getAccountId(this.value)" data-width="100%">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="account_id" name="credit_account_id" />
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('cheque_payorder') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="text" name="chq_no"class="form-control required mb-5">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Balance</label>
                        <div class="col-lg-3">
                            <input type="text" id="op_balance" class="form-control mb-5" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><?= lang('paid_to').' / '.lang('by') ?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <select name="paid_accounts"  class="form-control required_select mb-5 heads select_box" id="paid_accounts" data-width="100%">
                                <?php $type=array('Assets','Liabilities','Expense');
                                for ($i=0;$i<count($type);$i++){ ?>
                                    <optgroup label="<?= $type[$i] ?>"></optgroup><?php
                                    $sub_heads=$this->invoice_model->check_by_all(array('HEAD_TYPE'=>$type[$i]),'sub_heads');
                                    foreach ($sub_heads as $sb) {
                                        if (count_accounts_by_sub_head($sb->SUB_HEAD_ID)>0) { ?>
                                        <optgroup label="<?= '&nbsp;&nbsp;' . $sb->NAME ?>">
                                            <?php $accounts_head = account_head_by_subId($sb->SUB_HEAD_ID);
                                            foreach ($accounts_head as $ah) {
                                                if ($ah->H_ID != 4) {
                                                    $accounts = account_by_head($ah->H_ID);
                                                    if (!empty($accounts)) { ?>
                                                        <optgroup label="<?= '&nbsp;&nbsp;&nbsp;&nbsp;' . $ah->H_NAME ?>"><?php
                                                            foreach ($accounts as $account) {
                                                                $account1 = accounts_by_self($account->A_ID);
                                                                if (!empty($account1)) { ?>
                                                                    <optgroup label="<?= $account->A_NAME ?>"><?php
                                                                    foreach ($account1 as $ac1) {
                                                                        $account2 = accounts_by_self($ac1->A_ID);
                                                                        if (!empty($account2)) { ?>
                                                                            <optgroup label="<?= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $ac1->A_NAME ?>"><?php
                                                                            foreach ($account2 as $ac2) { ?>
                                                                                <option value="<?php echo $ac2->A_ID; ?>"><?php echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $ac2->A_NAME; ?></option><?php
                                                                            } ?></optgroup><?php
                                                                        } else { ?>
                                                                            <option value="<?php echo $ac1->A_ID; ?>"><?php echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $ac1->A_NAME; ?></option><?php
                                                                        }
                                                                    } ?>
                                                                    </optgroup><?php
                                                                } else { ?>
                                                                    <option value="<?php echo $account->A_ID; ?>"><?php echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $account->A_NAME; ?></option><?php
                                                                }
                                                            } ?>
                                                        </optgroup><?php
                                                    }
                                                }
                                            } ?></optgroup><?php
                                        }
                                    }
                                } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <input type="number" name="amount" class="form-control" autocomplete="off" step="any" min="0">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><?= lang('date') ?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <input type="text" name="date" class="form-control datepicker" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><?= lang('description') ?></label>
                        <div class="col-lg-3">
                            <textarea name="desc" class="form-control mb-5"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"></label>
                        <div class="col-lg-4">
                            <button type="submit" class="btn btn-sm btn-success pull-right"><i class="fa fa-check"></i><?php echo lang('create_voucher'); ?></button>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>
</div>
<script type="text/javascript">
    $('#type').on("change", function() {
        var type = $(this).val();
        if(type == '1'){
            $('#cash_at_bank').fadeOut(500, function() { $(this).hide(); });
            $('#branches').fadeOut(500, function() { $(this).hide(); });
            $(".cash_at_bank").attr("disabled","disabled");
        }
        else{
            $('#cash_at_bank').fadeIn(500).show();
            $(".cash_at_bank").removeAttr("disabled");
        }
    });
    function selectBranches(val) {
        var option="";
        if(val == '') {
            $('#branches').hide();
        }
        else{
            $.getJSON("<?php echo site_url('admin/accounts/ajax_select_branches') ?>" + "/" + val, function (result) {
                $.each(result, function (index, value) {
                    option = ('<option value="' + value.BR_ID + '">' + value.BR_NAME + '</option>')+option;
                });
                var drop = '<div class="form-group"><label class="col-md-4 control-label">Branches <span class="text-danger">*</span></label><div class="col-lg-3" id="branches"><select name="branch_id" class="form-control cash_at_bank select_box required_select" onchange="getAccountId(this.value)">' + option + '</select></div></div>';
                $('#branches').html(drop).hide().fadeIn(500);
                $('.select_box').select2({});
                getAccountId(result[0].BR_ID);
            });
        }
    }

    function getAccountId(val){
        var account_id=$('#account_id');
        $.getJSON( "<?php echo site_url('admin/accounts/ajax_get_branch_aid') ?>/"+val, function(result) {
            account_id.val(result.A_ID);
            getAccountBalance(result.A_ID);
        });
    }

    function getAccountBalance(val){
        var balance_field=$('#op_balance');
        $.getJSON( "<?php echo site_url('admin/accounts/get_account_balance') ?>/"+val, function(result) {
            balance_field.val(result);
        });
    }

    $(document).ready(function () {
       getAccountBalance($('#type').val());
    });
</script>