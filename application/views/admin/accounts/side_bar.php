<!-- Page container -->
	<div class="page-container">
		<!-- Page content -->
		<div class="page-content">
			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">
					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold"><?php echo $this->session->userdata('fullname');?></span>
									<div class="text-size-mini text-muted">
										<i class="status-mark border-success position-left"></i> Active
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->
					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">
								<!-- Main -->
								<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
								<li class="<?php if($this->uri->segment(2)=="home"){echo "active";}?>" ><a href="<?php echo base_url('admin/home');?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
								<li class="<?php if($this->uri->segment(2)=="banks"){echo "active";}?>">
									<a href="<?php echo base_url('admin/banks');?>"><i class="fa fa-bank"></i> <span>Banks Section </span></a>
								</li>
								<li class="<?php if($this->uri->segment(2)=="branches"){echo "active";}?>">
									<a href="<?php echo base_url('admin/branches');?>"><i class="fa fa-building-o"></i> <span>Branch Section </span></a>
								</li>
								<li class="<?php if($this->uri->segment(2)=="users"){echo "active";}?>">
									<a href="<?php echo base_url('admin/users');?>"><i class="fa fa-users"></i> <span>Users Section </span></a>
								</li>
								<li class="<?php if($this->uri->segment(2)=="cheques"){echo "active";}?>">
									<a href="<?php echo base_url('admin/cheques');?>"><i class="fa fa-money"></i> <span>Cheque Section </span></a>
								</li>
								<li class="<?php if($this->uri->segment(1)=="cash_inHand" && $this->uri->segment(1)=="cash_andBank" ){echo "active";}?>">
									<a href="#"><i class="fa fa-credit-card"></i> <span>Cash & Bank Book</span></a>
									<ul>
										<li class="<?php if($this->uri->segment(2)=="cash_atBank"){echo "active";}?>"><a href="<?php echo base_url('Accounts/cash_atBank');?>"><i class="icon-file-plus"></i><span>Cash at Bank </span></a></li>
										<li class="<?php if($this->uri->segment(2)=="cash_inHand"){echo "active";}?>"><a href="<?php echo base_url('Accounts/cash_inHand');?>"><i class="icon-file-plus"></i><span>Cash in Hand</span></a></li>

									</ul>
								</li>
								<li class="<?php if($this->uri->segment(1)=="accounts"){echo "active";}?>">
									<a href="#"><i class="fa fa-credit-card"></i> <span>Accounts Section </span></a>
									<ul>
										<li class="<?php if($this->uri->segment(2)=="accountHead"){echo "active";}?>"><a href="<?php echo base_url('Accounts/accountHead');?>"><i class="icon-file-plus"></i><span>Head of Accounts </span></a></li>
										<li class="<?php if($this->uri->segment(2)==""){echo "active";}?>"><a href="<?php echo base_url('Accounts/');?>"><i class="icon-file-plus"></i><span>Sub Accounts </span></a></li>
										<li class="<?php if($this->uri->segment(2)=="general_journal"){echo "active";}?>">
											<a href="<?php echo base_url('Accounts/general_journal');?>"><i class="icon-file-openoffice"></i> <span>General Journal </span></a>
										</li>
										<li class="<?php if($this->uri->segment(2)=="general_ledgers"){echo "active";}?>">
											<a href="<?php echo base_url('Accounts/general_ledgers');?>"><i class="icon-file-openoffice"></i> <span>General Ledgers </span></a>
										</li>
										<li class="<?php if($this->uri->segment(2)=="trial_balance"){echo "active";}?>">
											<a href="<?php echo base_url('Accounts/trial_balance');?>"><i class="icon-file-openoffice"></i> <span>Trial Balance</span></a>
										</li>
										<li class="<?php if($this->uri->segment(2)=="balance_sheet"){echo "active";}?>">
											<a href="<?php echo base_url('Accounts/balance_sheet');?>"><i class="icon-file-openoffice"></i> <span>Balance Sheet</span></a>
										</li>
										<li class="<?php if($this->uri->segment(2)=="income_statement"){echo "active";}?>">
											<a href="<?php echo base_url('Accounts/income_statement');?>"><i class="icon-file-openoffice"></i> <span>Income Statement</span></a>
										</li>
									</ul>
								</li>
								<li class="<?php if($this->uri->segment(2)=="voucher_list"){echo "active";}?>">
									<a href="<?php echo base_url('Accounts/voucher_list');?>"><i class="icon-file-openoffice"></i> <span>Voucher Section </span></a>
								</li>
								<li class="<?php if($this->uri->segment(2)=="logs_detail"){echo "active";}?>">
									<a href="<?php echo base_url('admin/logs_detail');?>"><i class="icon-file-openoffice"></i> <span>Logs Detail </span></a>
								</li>
							</ul>
						</div>
					</div>
					<!-- /main navigation -->
				</div>
			</div>
			<!-- /main sidebar -->