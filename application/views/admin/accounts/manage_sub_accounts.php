<?php echo message_box('success'); ?>

<?php echo message_box('error'); ?>
<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('create_sub_account') ?></h1>
<div class="row">

    <section class="col-sm-12">

        <?= message_box('error') ?>

        <!-- Start create invoice -->                        

        <section class="panel panel-default" >

		<?php $payment_info = $this->invoice_model->check_by(array('invoices_id' => $this->uri->segment(5)), 'tbl_payments'); ?>

            <header class="panel-heading"><?= $title; ?></header>

            <div class="panel-body">

                <form method="post" action="<?= base_url() ?>admin/accounts/save_account" class="form-horizontal" role="form" id="form">

                    <div class="form-group">

                        <label class="col-lg-4 control-label">Type <span class="text-danger">*</span></label>

                        <div class="col-lg-4">

                            <select name="h_type" id="ha_type" onchange="ReadSubType(this.value)" class="form-control mb-5 select_box" required>

                                <option value="">-----</option>

                                <option value="Assets">Assets</option>

                                <option value="Liabilities">Liabilities</option>

                                <option value="Income">Income</option>

                                <option value="Expense">Expense</option>

                                <option value="Equity">Equity</option>

                            </select>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-lg-4 control-label">Sub Type <span class="text-danger">*</span></label>

                        <div class="col-lg-4">

                            <select name="sub_type" class="form-control mb-5 select_box" id="subType1" onchange="selectaccountsHead(this.value)" required>

                                <option value="">----</option>

                            </select>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-lg-4 control-label">Account Head <span class="text-danger">*</span></label>

                        <div class="col-lg-4">

                            <select name="a_head" id="ac_head" onchange="selectSubHead(this.value)" class="form-control mb-5 select_box" required>

                                <option value="">----</option>

                            </select>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-lg-4 control-label">Sub Head</label>

                        <div class="col-lg-4">

                            <select name="a_subHead" id="ac_subHead" class="form-control mb-5 select_box">

                                <option value="">----</option>

                            </select>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-lg-4 control-label">Name <span class="text-danger">*</span></label>

                        <div class="col-lg-4">

                            <input type="text" name="a_name" placeholder="Name" class="form-control mb-5" required>

                        </div> 

                    </div>

                    <div class="form-group">

                        <label class="col-lg-4 control-label">Client Account</label>

                        <div class="col-lg-4">

                            <input type="checkbox" class="custom-checkbox" name="client_account">

                        </div>

                    </div>



                    <div class="form-group">

                        <label class="col-lg-4 control-label">Opening Type</label>

                        <div class="col-lg-4">

                            <div class="form-check mb-5">

                                <label class="radio-inline"><input type="radio" name="a_openingType" value="Debit">Debit</label>

                                <label class="radio-inline"><input type="radio" name="a_openingType" value="Credit">Credit</label>

                            </div>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-lg-4 control-label">Opening Balance</label>

                        <div class="col-lg-4">

                            <input type="number" name="a_openingBalance" min="0" step="any" class="form-control mb-5">

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-lg-4 control-label">Status</label>

                        <div class="col-lg-4">

                            <select name="a_status" class="form-control select_box">

                                <option value="1">Active</option>

                                <option value="0">Inactive</option>

                            </select>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-lg-4 control-label">Description</label>

                        <div class="col-lg-4">

                            <textarea name="a_desc" class="form-control mb-5"></textarea>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-lg-4"></label>

                        <div class="col-lg-4">

                            <button type="submit" class="btn btn-primary">Add Account</button>

                        </div>            

                    </div>            

                </form>

            </div>

        </section>

    </section>    

</div>

<!--<script type="text/javascript">

    $('#cash_at_bank').hide();

    $('#type').on("change", function() {

        var type = $(this).val();

        if(type == '1'){

            $('#cash_at_bank').fadeOut(500, function() { $(this).hide(); });

            $(".cash_at_bank").attr("disabled","disabled");

        }

        else{

            $('#cash_at_bank').fadeIn(500).show();

            $(".cash_at_bank").removeAttr("disabled");

        }

    });

    function selectBranches(val) {

        var option="";

        console.log(val);

        if(val == '') {

            $('#branches').hide();

        }

        else{

            $.getJSON("<?php /*echo site_url('admin/accounts/ajax_select_branches') */?>" + "/" + val, function (result) {

                /*option.empty();*/

                $.each(result, function (index, value) {

                    option = ('<option value="' + value.BR_ID + '">' + value.BR_NAME + '</option>')+option;

                });

                var drop = '<div class="form-group"><label class="col-lg-3 control-label">Branches <span class="text-danger">*</span></label><div class="col-lg-6"><select name="branch_id" required="" class="form-control cash_at_bank select_box" onchange="getAccountId(this.value)"><option value="">Choose Branch</option>' + option + '</select></div></div>';

                $('#branches').html(drop).hide().fadeIn(500);

                $('.select_box').select2({});

                getAccountId();

            });

        }

    }

    function getAccountId(val){

        account_id=$('#account_id');

        $.getJSON( "<?php /*echo site_url('admin/accounts/ajax_edit_branch') */?>"+"/"+val, function(result) {

            account_id.val(result.A_ID);

        });

    }

</script>-->

<!-- Fill dropdown on another basis -->

<script type="text/javascript">

    $(document).ready(function () {

        ReadSubType($('#ha_type').val());

    });

    /*Select Sub Type on Type Basis*/

    function ReadSubType(val) {

        var option="";

        if($('#a_id').val()!='' && $('#edit_account').is(':visible')){

            option=$('#subType2');

        }else {

            option=$('#subType1');

        }

        $.getJSON( "<?php echo site_url('admin/accounts/select_sub_head') ?>"+"/"+val, function(result) {

            option.empty();

            option.append('<option value="' + 0 + '">----</option>');

            $.each(result, function (index, value) {

                option.append('<option value="' + value.SUB_HEAD_ID + '">' + value.NAME + '</option>');

            });

            selectaccountsHead(option.val());

        });

    }

    /*Select Account Head on Sub Type basis*/

    function selectaccountsHead(val){

        var option="";

        if($('#a_id').val()!='' && $('#edit_account').is(':visible')){

            option=$('#a_head');

        }else {

            option=$('#ac_head');

        }

        $.getJSON( "<?php echo site_url('admin/accounts/ajax_select_head') ?>"+"/"+val, function(result) {

            option.empty();

            option.append('<option value="' + 0 + '">----</option>');

            $.each(result, function (index, value) {

                option.append('<option value="' + value.H_ID + '">' + value.H_NAME + '</option>');

                $('.select_box').select2({});

            });

            selectSubHead(option.val());

        });

    }



    /*Select Sub Head on Account Head Basis*/

    function selectSubHead(val) {

        var option="";

        if($('#a_id').val()!='' && $('#edit_account').is(':visible')){

            option=$('#a_subHead');

        }else {

            option=$('#ac_subHead');

        }

        $.getJSON( "<?php echo site_url('admin/accounts/ajax_select_accounts') ?>"+"/"+val, function(result) {

            option.empty();

            option.append('<option value="' + 0 + '">----</option>');

            $.each(result, function (index, value) {

                option.append('<option value="' + value.A_ID + '">' + value.A_NAME + '</option>');

            });

        });

    }

</script>

<!-- /Fill dropdown on another basis -->

<!-- end -->













