<div class="content-wrapper" style="margin: auto;">
    <div class="content">
        <div class="row">
            <div class="col-sm-offset-2 col-sm-8">
                <div class="panel panel-flat thumbnail">
                    <div class="panel-heading no-print">
                        <div class="btn-group">
                            <button type="button" onclick="printDiv('PrintMe')" class="btn btn-default"><i class="fa fa-print"></i></button>
                            <script>
                                function printDiv(divName) {
                                    var printContents = document.getElementById(divName).innerHTML;
                                    var originalContents = document.body.innerHTML;
                                    document.body.innerHTML = printContents;
                                    window.print();
                                    document.body.innerHTML = originalContents;
                                }
                            </script>
                        </div>
                    </div>
                    <div id="PrintMe">
                        <style>
                            .table-main>tbody>tr>td,
                            .table-main>tfoot>tr>td,
                            .table-main>tr>td {
                                border: none;
                            }
                        </style>
                        <div class="panel-heading">
                            <div class="text-center">
                                <h1><?= config_item('company_name') ?></h1>
                                <h4></h4>
                                <h4>General Expense Voucher</h4>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table table-main">
                                        <tr>
                                            <td style="font-size: 13px;width: 20%;"><b>V. No.</b></td>
                                            <td style="font-size: 13px;width: 30%;"><?= $voucher->PC_VOUCHER_NO; ?></td>
                                            <td style="font-size: 13px;width: 20%;"><b>Date</b></td>
                                            <td style="font-size: 13px;width: 30%;"><?= date('d-m-Y',strtotime($voucher->PC_DATE)); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 13px;width: 20%;"><b>Paid To.</b></td>
                                            <td colspan="3" style="font-size: 13px;width: 30%;"><?= $voucher->staff_name; ?></td>
                                        </tr>
                                        <tr>
                                            <th colspan="3" style="border: 1px solid black;">Particulars</th>
                                            <th style="border: 1px solid black;text-align:right;">Amount</th>
                                        </tr>
                                        <?php $total_amount=0;
                                        $details=petty_cash_details($voucher->PC_ID);
                                        foreach ($details as $detail) { ?>
                                            <tr>
                                                <td colspan="3" style="border: 1px solid black;"><?= $detail->A_NAME ?></td>
                                                <td style="border: 1px solid black;text-align:right;"><?= number_format($detail->PCD_AMOUNT,2) ?></td>
                                                <?php $total_amount+=$detail->PCD_AMOUNT; ?>
                                            </tr>
                                            <?php
                                        } ?>
                                        <tr>
                                            <td colspan="3" style="border: 1px solid black;text-align:center"><b>
                                                    In Words : <?php echo $this->invoice_model->convert_number($total_amount); ?>
                                                </b></td>
                                            <td style="border: 1px solid black;text-align:right;"><b>
                                                    Total : <?= number_format($total_amount,2); ?>
                                                </b></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="" style="padding-top: 0px; margin-top: 50px;">
                            <table class="table table-main">
                                <tr align="center">
                                    <td>_________________________
                                        <br>
                                        <h4>Approved By</h4> </td>
                                    <td>_________________________
                                        <br>
                                        <h4>Paid By</h4> </td>
                                    <td>_________________________
                                        <br>
                                        <h4>Receiver Signature</h4> </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>