<!-- style form css -->
<style>
    .redborder1{
        border:1px solid red;
    }
    .redborder2{
        border:1px solid red;
    }
</style>
<!-- /style form css -->
<!-- Content area -->
<div class="content">
    <!-- Highlighting rows and columns -->
    <div class="col-xs-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <?php echo form_open(base_url('admin/accounts/job_ledgers'),array('class'=>"form-horizontal")); ?>
                <div class="row">
                    <div class="col-xs-2">
                        <select name="client_id" class="form-control select_box" onchange="selectJobno(this.value)" style="width: 100%" required>
                            <option value="">Choose Client</option>
                            <?php if(!empty($all_clients)):
                                foreach ($all_clients as $client): ?>
                                    <option value="<?php echo $client->client_id; ?>" <?php
                                    if(isset($client_id)){
                                        echo ($client_id == $client->client_id)?'selected':'';
                                    } ?>><?php echo $client->name; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                    <div class="col-xs-2">
                        <select name="invoices_id" id="invoices_id" class="form-control select_box" style="width: 100%" required>
                            <!--<option value="0">All Jobs</option>-->
                            <?php if (!empty($client_invoices)):
                                foreach ($client_invoices as $inv):?>
                                    <option value="<?= $inv->invoices_id ?>" <?= ($invoices_id==$inv->invoices_id)?'selected':'' ?>><?= $inv->reference_no ?></option>
                                <?php endforeach;
                            endif; ?>
                        </select>
                    </div>
                    <!--<div class="col-xs-2">
                        <label class="checkbox-inline">
                            <input type="checkbox" name="opening"> Opening Balance
                        </label>
                    </div>-->
                    <div class="col-xs-2">
                        <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> Search</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <div class="panel-body">
                <div class="row" id="export_table">
                    <?php if(isset($client_name)): ?>
                        <h4 class="text-center text-uppercase">Selected Client : <b class="text-danger"><?= $client_name ?></b></h4>
                    <?php endif; ?>
                    <!--<table class="table table-bordered table-responsive table-hover" id="table">-->
                    <table class="table table-striped" id="ledger_DataTables">
                        <thead>
                        <tr style="border-top: 1px solid #dddddd;">
                            <th style="text-align: center;vertical-align: middle">Date</th>
                            <th style="vertical-align: middle">Reference</th>
                            <th style="text-align: right;vertical-align: middle">Debit Amount</th>
                            <th style="text-align: right;vertical-align: middle">Credit Amount</th>
                            <th style="text-align: right;vertical-align: middle">Debit <br /><br />Balance</th>
                            <th style="text-align: right;">Credit <br /><br />Balance</th>
                        </tr>
                        <!--<tr style="border-top: 1px solid #dddddd;">
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class="text-center">Debit</th>
                            <th class="text-center">Credit</th>
                        </tr>-->
                        </thead>
                        <tbody>
                        <?php
                        $sum_credit=0; $sum_debit=0;$balance=0;$debit_remaining=0;$credit_remaining=0;$t_debit=0;
                        if (!empty($job_ledgers)){
                            foreach ($job_ledgers as $ledger){
                                $job_info = $this->invoice_model->check_by(array('invoices_id'=> $ledger->invoice_id), 'tbl_invoices');
                                $in_qty=0; $out_qty=0;
                                if ($ledger->type=='Debit'){
                                    $in_qty=$ledger->amount;
                                    $balance+=$ledger->amount;
                                    $sum_debit+=$ledger->amount;
                                }elseif ($ledger->type=='Credit'){
                                    $out_qty=$ledger->amount;
                                    $balance-=$ledger->amount;
                                    $sum_credit+=$ledger->amount;
                                }
                                ?>
                                <tr>
                                    <td class="text-center"><?= date('d-m-Y',strtotime($ledger->date)) ?></td>
                                    <td><b><?= ($ledger->prefix=='Payment Received')?$ledger->prefix.'('.$ledger->reference_no.')':($ledger->prefix.$ledger->reference_no.' '.(($ledger->prefix=='BL')?$job_info->reference_no:'')) ?></b></td>
                                    <td class="text-right"><?= ($in_qty!=0)?number_format(abs($in_qty),2):'' ?></td>
                                    <td class="text-right"><?= ($out_qty!=0)?number_format(($out_qty),2):'' ?></td>
                                    <td class="text-right"><?= ($balance > 0)?number_format(($balance),2):'' ?></td>
                                    <td class="text-right"><?= ($balance < 0)?number_format(abs($balance),2):'' ?></td>
                                </tr>
                            <?php }
                        }
                        ?>

                        </tbody>
                        <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td class="text-right"><b><?php echo number_format($sum_debit,2); ?></b></td>
                            <td class="text-right"><b><?php echo number_format($sum_credit,2); ?></b></td>
                            <td class="text-right"><b><?= ($balance>0)?number_format(abs($balance),2):'' ?></b></td>
                            <td class="text-right"><b><?= ($balance<0)?number_format(abs($balance),2):'' ?></b></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <button class="btn btn-sm pull-right" style="color: #fff;background-color: #26a69a" id="btnExport">Export to Excel</button>
            </div>
        </div>
        <!-- /highlighting rows and columns -->
        <?php /*$this->load->view('admin/components/buttons_datatable.php'); */?>

        <script type="text/javascript">
            function selectJobno(client_id) {
                var option="";
                var option_empty = /*'<option value="0" selected>All Jobs</option>'*/'';
                if(client_id == '') {
                    $('#invoices_id').html(option_empty).hide().fadeIn(500);
                    $('.select_box').select2({});
                }
                else{
                    $.getJSON("<?php echo site_url('admin/accounts/ajax_get_jobs_by_client/') ?>" + "/" + client_id, function (result) {
                        $.each(result, function (index, value) {
                            option = ('<option value="' + value.invoices_id + '">' + value.reference_no + '</option>')+option;
                        });
                        $('#invoices_id').html(option_empty+option).hide().fadeIn(500);
                        $('.select_box').select2({});
                    });
                }
            }

            $("#btnExport").click(function (e) {
                window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('#export_table').html() ));
                e.preventDefault();
            });

        </script>
        <script type="text/javascript">
           /* $(document).ready(function() {
                $('#ledger_DataTables').dataTable({
                    paging: false,
                    "bSort": true,
                    "searching": false,
                });
            });*/
        </script>