<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header">
        <!--	<div class="page-header-content">
                <div class="page-title">
                    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Employee</span> Employee -1X1</h4>
                </div>

                <!--<div class="heading-elements">
                    <div class="heading-btn-group">
                        <a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
                        <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
                        <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
                    </div>
                </div>
            </div>-->

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="<?php echo base_url('admin/home');?>"><i class="fa fa-home"></i> Home</a></li>
                <li class="active">Users</li>
            </ul>

            <ul class="breadcrumb-elements">
                <li>
                    <a href="#" data-toggle="modal" data-target="#add_users" class="btn btn-xs" style="color: #fff;background-color: #26a69a"><i class="fa fa-plus"></i> Add User</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Highlighting rows and columns -->
        <div class="panel panel-flat">
            <table class="table table-bordered table-hover datatable-basic">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php if(!empty($users)): ?>
                    <?php foreach ($users as $user){ ?>
                        <tr style="white-space: nowrap">
                            <td><?php echo $user->U_NAME; ?></td>
                            <td><?php echo $user->U_EMAIL; ?></td>
                            <td><?php echo $user->R_NAME; ?></td>
                            <td><?php echo ($user->U_STATUS==1)?"Active":"Inactive"; ?></td>
                            <td><button type="button" onclick="getEditUser(this.value)" value="<?php echo $user->U_ID;?>" data-popup="tooltip" title="" data-container="body" data-original-title="Edit User" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#" ><i class="fa fa-edit"></i></button>
                                <button type="button" value="<?php echo $user->U_ID;?>" data-popup="tooltip" title="" data-container="body" data-original-title="Delete User" class="btn btn-danger btn-xs delete"><i class="fa fa-trash"></i></button></td>
                        </tr>
                    <?php } ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <!-- /highlighting rows and columns -->
        <!-- Add User modal -->
        <div id="add_users" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h6 class="modal-title">Add User</h6>
                    </div>

                    <div class="modal-body">
                        <?php echo form_open(base_url(''),array('class'=>"form-horizontal",'id'=>'ad_user')); ?>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id='salessuccess' class='alert alert-info' style='display:none;'></div>
                                        <?php if($this->session->flashdata('salessuccess')){ ?>
                                            <div class="alert bg-info alert-rounded" style='text-align:center;'><?php echo $this->session->flashdata('salessuccess'); ?></div>
                                        <?php } ?>
                                        <div class="col-sm-12">
                                            <label>Name</label>
                                            <input type="text" name="U_NAME" placeholder="Name" class="form-control required mb-5">
                                        </div>
                                        <div class="col-sm-12">
                                            <label>Email</label>
                                            <input type="email" name="U_EMAIL" placeholder="Email" class="form-control required mb-5">
                                        </div>
                                        <div class="col-sm-12">
                                            <label>Password</label>
                                            <input type="password" name="U_PASSWORD" placeholder="*******" class="form-control required mb-5">
                                        </div>
                                        <div class="col-sm-12">
                                            <label>User Role</label>
                                            <?php echo form_dropdown('R_ID', $role_list , "", ' class="form-control required" id="" ');  ?>
                                        </div>
                                        <input type="hidden" name="CREATED_BY" value="<?php echo $this->session->userdata('u_id'); ?>">
                                        <div class="col-sm-12">
                                            <label>Status</label>
                                            <Select name="U_STATUS" class="form-control required">
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                            <!--<label class="radio-inline"><input type="radio" name="status" value="1" checked>Active</label>
                                            <label class="radio-inline"><input type="radio" name="status" value="0">Inactive</label>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                            <input  type="submit" class="btn btn-info"  value="Add User"/>

                        </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Add User -->
        <!-- style form css -->
        <style>
            .redborder1{
                border:1px solid red;
            }
            .redborder2{
                border:1px solid red;
            }
        </style>
        <!-- /style form css -->
        <!-- add User script -->
        <script type='text/javascript'>

            var ad_user = $('#ad_user');
            ad_user.on('submit',function(e){
                e.preventDefault();

                var new_user_add = ad_user.serialize();
                console.log(new_user_add);
                var assetserror = 0;
                $('.required').each(function(){
                    if($(this).val() == ''){$(this).addClass('redborder1');assetserror = assetserror + 1;}else{$(this).removeClass('redborder1');}
                });

                if(assetserror > 0){return false;}else{

                    $.ajax({
                        type:'POST',
                        url:'<?php echo base_url('Admin/add_user'); ?>',
                        data:new_user_add,
                        dataType:'JSON',
                        success:function(data){

                            console.log('first'+data);
                            if(data.status == 'true'){
                                console.log('second'+data);
                                $('#salessuccess').show().html('User added Successfully').hide(3000);
                                ad_user[0].reset();
                                setTimeout(function(){location.href="<?php echo base_url('Admin/users'); ?>"} , 3000);
                            }else{
                                console.log('third'+data);
                            }
                        },
                        error:function(data){console.log('error: '+data.responseText);}
                    });
                }
            });
        </script>
        <!-- /add User script -->

        <!-- Edit User modal -->
        <div id="edit_user" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h6 class="modal-title">Edit User</h6>
                    </div>

                    <div class="modal-body">
                        <?php echo form_open(base_url(''),array('class'=>"form-horizontal",'id'=>'ed_user')); ?>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id='salessuccess' class='alert alert-info' style='display:none;'></div>
                                        <?php if($this->session->flashdata('salessuccess')){ ?>
                                            <div class="alert bg-info alert-rounded" style='text-align:center;'><?php echo $this->session->flashdata('salessuccess'); ?></div>
                                        <?php } ?>
                                        <input type="hidden" name="U_ID" id="u_id">
                                        <div class="col-sm-12">
                                            <label>Name</label>
                                            <input type="text" name="U_NAME" id="u_name" placeholder="Name" class="form-control require mb-5">
                                        </div>
                                        <div class="col-sm-12">
                                            <label>Email</label>
                                            <input type="email" name="U_EMAIL" id="u_email" placeholder="Email" class="form-control require mb-5">
                                        </div>
                                        <div class="col-sm-12">
                                            <label>Password</label>
                                            <input type="password" name="U_PASSWORD" id="u_pwd" placeholder="*******" class="form-control require mb-5">
                                        </div>
                                        <div class="col-sm-12">
                                            <label>User Role</label>
                                            <?php echo form_dropdown('R_ID', $role_list , "", ' class="form-control require" id="u_role" ');  ?>
                                        </div>
                                        <div class="col-sm-12">
                                            <label>Status</label>
                                            <Select name="U_STATUS" id="u_status" class="form-control require">
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                            <!--<label class="radio-inline"><input type="radio" name="status" value="1" checked>Active</label>
                                            <label class="radio-inline"><input type="radio" name="status" value="0">Inactive</label>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                            <input  type="submit" class="btn btn-info"  value="Edit User"/>

                        </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Edit User Modal -->
        <!-- edit select user id script -->
        <script type='text/javascript'>
            function getEditUser(id)
            {
                $('document').ready(function(){
                    var controlmodal=$('#edit_user');
                    var option={
                        "backdrop" : "static"
                    }
                    controlmodal.modal(option);
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url("admin/ajax_edit_user/");?>'+id,
                        data:false,
                        contentType: false,
                        dataType:"json",
                        success:function(data){
                            $('#u_id').val(data['U_ID']);
                            $('#u_name').val(data['U_NAME']);
                            $('#u_email').val(data['U_EMAIL']);
                            $('#u_pwd').val(data['U_PASSWORD']);
                            $('#u_role').val(data['R_ID']);
                            $('#u_status').val(data['U_STATUS']);
                        },
                        error:function(data){
                            console.log(data.responseText + "not work")
                        },
                    });
                });
            }
        </script>
        <!-- edit select user id script -->

        <!-- Edit User script -->
        <script type='text/javascript'>

            var edit_user = $('#ed_user');
            edit_user.on('submit',function(e){
                e.preventDefault();

                var new_user_edit = $('#ed_user').serialize();
                console.log(new_user_edit);
                var assetserror = 0;
                $('.require').each(function(){
                    if($(this).val() == ''){$(this).addClass('redborder2');assetserror = assetserror + 1;}else{$(this).removeClass('redborder2');}
                });

                if(assetserror > 0){return false;}else{

                    $.ajax({
                        type:'POST',
                        url:'<?php echo base_url('Admin/edit_user'); ?>',
                        data:new_user_edit,
                        dataType:'JSON',
                        success:function(data){

                            console.log('first'+data);
                            if(data.status == 'true'){
                                console.log('second'+data);
                                $('#edit_user').modal('hide');
                                setTimeout(function(){location.href="<?php echo base_url('Admin/users'); ?>"} , 2000);
                                // Success notification
                                $.jGrowl('User has been Updated', {
                                    header: 'Successfully',
                                    theme: 'bg-success alert-styled-left alert-styled-check'
                                });


                            }else{
                                console.log('third'+data);
                            }
                        },
                        error:function(data){console.log('error: '+data.responseText);}
                    });
                }
            });
        </script>
        <!-- /Edit User script -->

        <!--Delete User Script-->
        <script type="text/javascript">
            $('.delete').click(function () {
               if(confirm("Are you sure?")){
                   $.ajax({
                      type:"POST",
                       url:"<?php echo base_url('Admin/delete_user/'); ?>"+$(this).val(),
                       dataType:'JSON',
                       success:function(data){
                           if(data>0){
                               setTimeout(function(){location.href="<?php echo base_url('Admin/users'); ?>"} , 2000);
                               $.jGrowl('User has been Deleted', {
                                   header: 'Successfully',
                                   theme: 'bg-danger alert-styled-left alert-styled-check'
                               });
                           }
                       }
                   });
               }else {
                   return false;
               }
            });
        </script>
        <!--/Delete User Script-->