<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html lang=en>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title><?= lang('requisition') ?></title>
    <style>
        html{margin:3px 15px !important;}
        body {
            background: white;
            color: black;
            margin: 0px;
        }


        #h11 {
            color: #000;
            background: none;
            font-weight: bold;
            font-size: 2.5em;
            margin: 10px 0 0 0;
            text-align: center;
            font-size:32px;
        }
        #h12
        {
            color: #000;
            background: none;
            font-weight: bold;
            font-size: 2em;
            margin: 10px 0 0 0;
            text-align: center;
            font-size:25px;
        }

        h2 {
            color: #00008b;
            background: none;
            font-weight: bold
        }

        h3 {
            color: #000;
            background: none;
            margin-left: 4%;
            margin-right: 4%;
            font-weight: bold;
            margin: 10px 0 0 0;
            text-align: center;
        }

        h4 {
            margin-left: 6%;
            margin-right: 6%;
            font-weight: bold
        }

        h5 {
            margin-left: 6%;
            margin-right: 6%;
            font-weight: bold
        }

        ul, ol, dl, p {
            margin-left: 6%;
            margin-right: 6%
        }

        ul ul, table ol, table ul, dl ol, dl ul {
            margin-left: 1.2em;
            margin-right: 1%;
            padding-left: 0
        }

        pre {
            margin-left: 10%;
            white-space: pre
        }

        /* table caption {
          font-size: larger;
          font-weight: bolder
        } */

        /* table p, table dl, ol p, ul p, dl p, blockquote p, .note p, .note ul, .note ol, .note dl, li pre, dd pre {
          margin-left: 2%;
          margin-right: 2%;
        } */
        td
        {
            align: right;
            border: 1px solid black;
        }

        th
        {
            text-align:center;
            border: 1px solid black;
        }

        p.top {
            margin-left: 1%;
            margin-right: 1%
        }

        blockquote {
            margin-left: 8%;
            margin-right: 8%;
            border: thin ridge #dc143c
        }

        blockquote pre {
            margin-left: 1%;
            margin-right: 1%
        }


        .css {
            color: #800000;
            background: none
        }

        .javascript {
            color: #008000;
            background: none
        }

        .example { margin-left: 10% }

        dfn {
            font-style: normal;
            font-weight: bolder
        }

        var sub { font-style: normal }

        .note {
            font-size: 85%;
            margin-left: 10%
        }

        .SMA {
            color: fuchsia;
            background: none;
            font-family: Kids, "Comic Sans MS", Jester
        }

        .oops {
            font-family: Jester, "Comic Sans MS"
        }

        .author {
            font-style: italic
        }

        .copyright {
            font-size: smaller;
            text-align: right;
            clear: right
        }

        .toolbar {
            text-align: center
        }

        .toolbar IMG {
            float: right
        }

        .error {
            color: #DC143C;
            background: none;
            text-decoration: none
        }

        .warning {
            color: #FF4500;
            background: none;
            text-decoration: none
        }

        .error strong {
            color: #DC143C;
            background: #FFD700;
            text-decoration: none
        }

        .warning strong {
            color: #FF4500;
            background: #FFD700;
            text-decoration: none
        }


        colgroup.entity { text-align: center }

        .default { text-decoration: underline; font-style: normal }
        .required { font-weight: bold }
        td li.transitional, .elements li.transitional {
            font-weight: lighter;
            color: #696969;
            background: none
        }
        td li.frameset, .elements li.frameset {
            font-weight: lighter;
            color: #808080;
            background: none
        }

        .footer, .checkedDocument {

            width:93%;
            margin-left:3%;
            border-top: solid thin black
        }

        strong.legal {
            font-weight: normal;
            text-transform: uppercase
        }

        @media print {
            input#toggler, .toolbar, { display: none; }
            html, body {
                height:100vh;
                margin: 0 !important;
                padding: 0 !important;
                overflow: hidden;
            }
            td {
                height:1px;
                font-size:12px;
                padding:3px;
                font-family: 'bitstream cyberbit', sans-serif;
            }
            th{
                text-align:center;
                padding:5px;
            }
        }

        table { width: 100%; }

    </style>
    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
<body style="margin-left:3%; margin-right:3%;">
<?php
    $client_info = $this->invoice_model->check_by(array('client_id' => $memo_info->client_id), 'tbl_client');
?>
<h1 id="h11"><?= config_item('company_name') ?></h1><br>

<p style="text-align: center;">
    <?php
    if ($client_info->client_status == 1) {
        $status = 'Person';
    } else {
        $status = 'Company';
    }
    ?>
    <span style="font-size:11px;">
    <?= config_item('company_address') ?>
        <?= config_item('company_city') ?>, <?= config_item('company_country') ?> <br />
        Phone: <?= config_item('company_phone') ?><br />
        Email: <?= config_item('company_email') ?><br />
        Web: <?= config_item('company_domain') ?><br />
        <span>
</p>
<div class="footer"></div>
<br>


<div class="row">
    <div class="col-xs-12 table-responsive">
        <table style="border: 1px solid black;">
            <thead>
            <tr>
                <th style="text-align: center;">Date</th>
                <th style="text-align: center;">Reference</th>
                <th style="text-align: center;">Debit Amount</th>
                <th style="text-align: center;">Credit Amount</th>
                <th style="text-align: center;">Debit Amount</th>
                <th style="text-align: center;">Credit Amount</th>
            </tr>
            <tr>
                <th style="text-align: center;">Date</th>
                <th style="text-align: center;">Reference</th>
                <th style="text-align: center;">Debit Amount</th>
                <th style="text-align: center;">Credit Amount</th>
                <th style="text-align: center;">Debit Amount</th>
                <th style="text-align: center;">Credit Amount</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="text-align: center;">ABC</td>
                    <td style="text-align: center;">DEF</td>
                    <td style="text-align: center;">GHI</td>
                    <td style="text-align: center;">JKL</td>
                    <td style="text-align: center;">MNO</td>
                    <td style="text-align: center;">PQR</td>
                </tr>
                <tr>
                    <td style="text-align: center;">ABC</td>
                    <td style="text-align: center;">DEF</td>
                    <td style="text-align: center;">GHI</td>
                    <td style="text-align: center;">JKL</td>
                    <td style="text-align: center;">MNO</td>
                    <td style="text-align: center;">PQR</td>
                </tr>
                <tr>
                    <td style="text-align: center;">ABC</td>
                    <td style="text-align: center;">DEF</td>
                    <td style="text-align: center;">GHI</td>
                    <td style="text-align: center;">JKL</td>
                    <td style="text-align: center;">MNO</td>
                    <td style="text-align: center;">PQR</td>
                </tr>
            </tbody>
        </table>
    </div>

</div>

</body>
</html>
