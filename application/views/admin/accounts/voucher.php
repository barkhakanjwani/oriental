<div class="content-wrapper" style="margin: auto;">
    <div class="content">
        <div class="row">
            <div class="col-sm-offset-2 col-sm-8">
                <div class="panel panel-flat thumbnail">
                    <div class="panel-heading no-print">
                        <div class="btn-group">
                            <button type="button" onclick="printDiv('PrintMe')" class="btn btn-default"><i class="fa fa-print"></i></button>
                            <script>
                                function printDiv(divName) {
                                    var printContents = document.getElementById(divName).innerHTML;
                                    var originalContents = document.body.innerHTML;
                                    document.body.innerHTML = printContents;
                                    window.print();
                                    document.body.innerHTML = originalContents;
                                }
                            </script>
                        </div>
                    </div>
                    <div id="PrintMe">
                        <style>
                            .table-main>tbody>tr>td,
                            .table-main>tfoot>tr>td,
                            .table-main>tr>td {
                                border: none;
                            }
                        </style>
                        <div class="panel-heading">
                            <div class="text-center">
                                <h1><?= config_item('company_name') ?></h1>
                                <h4></h4>
                                <h4>General Voucher</h4>
                                <h5>Serial No <?php echo sprintf('%06d', $voucher->E_ID);; ?></h5>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table table-main">
                                        <tr>
                                            <td style="border: 1px solid black;font-size: 13px;width: 20%;"><b>V. No.</b></td>
                                            <td style="border: 1px solid black;font-size: 13px;width: 30%;"><?php echo $voucher->E_ID; ?></td>
                                            <td style="border: 1px solid black;font-size: 13px;width: 20%;"><b>Date</b></td>
                                            <td style="border: 1px solid black;font-size: 13px;width: 30%;"><?php echo date('d-m-Y',strtotime($voucher->E_DATE)); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="border: 1px solid black;font-size: 13px;"><b>Debit To </b></td>
                                            <td style="border: 1px solid black;font-size: 13px"><?php echo $voucher->DEBIT_NAME; ?></td>
                                            <td style="border: 1px solid black;font-size: 13px;"><b>Credit To </b></td>
                                            <td style="border: 1px solid black;font-size: 13px"><?php echo $voucher->CREDIT_NAME; ?></td>
                                        </tr>
                                    <!--</table>
                                    <table class="table table-main">-->
                                        <tr>
                                            <th style="border: 1px solid black;">Paid To / By</th>
                                            <th colspan="2" style="border: 1px solid black;">Particulars</th>
                                            <th style="border: 1px solid black;text-align:right;">Amount</th>
                                        </tr>
                                            <tr>
                                                <td style="border: 1px solid black;"><?php echo $voucher->PAID_TO; ?></td>
                                                <td colspan="2" style="border: 1px solid black;"><?php echo $voucher->DESCRIPTION; ?></td>
                                                <td style="border: 1px solid black;text-align:right;"><?= number_format($voucher->AMOUNT,2) ?></td>
                                            </tr>
                                        <tr>
                                            <td colspan="3" style="border: 1px solid black;text-align:center"><b>
                                                    In Words : <?php echo $voucher->AMOUNT_IN_WORDS; ?>
                                                </b></td>
                                            <td style="border: 1px solid black;text-align:right;"><b>
                                                    Total : <?php echo $voucher->AMOUNT; ?>
                                                </b></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="" style="padding-top: 0px; margin-top: 50px;">
                            <table class="table table-main">
                                <tr align="center">
                                    <td>_________________________
                                        <br>
                                        <h4>Approved By</h4> </td>
                                    <td>_________________________
                                        <br>
                                        <h4>Paid By</h4> </td>
                                    <td>_________________________
                                        <br>
                                        <h4>Receiver Signature</h4> </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>