<section class="content-header">
    <div class="row">
        <div class="col-md-12">
            <span class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('receipt_voucher_list') ?></span> <a href="<?= base_url('admin/accounts/manage_receipt_voucher')?>" class="btn btn-danger btn-xs" style="vertical-align: text-bottom;"><?= lang('add_new') ?></a>
        </div>
    </div>
</section>
<!-- Content area -->
<div class="content">

    <!-- Highlighting rows and columns -->
    <!--<div class="panel panel-flat">
        <div class="panel-heading">
            <div class="form-group">
                <div class="col-lg-3">
                    <select name="payment_type" class="form-control select_box" id="type" data-width="100%">
                        <option value="" <?/*= ($payment_mode=='')?'selected':'' */?>>All</option>
                        <option value="1" <?/*= ($payment_mode==1)?'selected':'' */?>>Cash</option>
                        <option value="2" <?/*= ($payment_mode==2)?'selected':'' */?>>Bank</option>
                        <option value="3" <?/*= ($payment_mode==3)?'selected':'' */?>>Third Party</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="panel-body">
        <table class="table table-bordered table-hover DataTables" id="DataTables">
            <thead>
            <tr>
                <th><?/*= lang('voucher_no') */?></th>
                <th>Payment Date</th>
                <th><?/*= lang('reference_no') */?></th>
                <th><?/*= lang('client') */?></th>
                <th>Payment Method</th>
                <th>Bank</th>
                <th>Branch</th>
                <th>Amount</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php /*if(!empty($advances)): */?>
                <?php /*foreach ($advances as $advance){
                    if($advance->expense_type == 'Job') {
                        $job_info = $this->invoice_model->check_by(array('invoices_id' => $advance->invoices_id), 'tbl_invoices');
                        $client_info = $this->invoice_model->check_by(array('client_id' => $job_info->client_id), 'tbl_client');
                        $job = $this->invoice_model->job_no_creation($advance->invoices_id);
                        $client = $client_info->name;
                    } elseif($advance->expense_type=='Misc'){
                        $job = "--";
                        $client_info = $this->invoice_model->check_by(array('client_id' => $advance->client_id), 'tbl_client');
                        $client = $client_info->name;
                    }else{
                        $job = "--";
                        $client ="--";
                    }
                    
                    $branch_info = $this->invoice_model->check_by(array('A_ID' => $advance->debit_account), 'branches');
                    if (!empty($branch_info)){
                        $bank_info = $this->invoice_model->check_by(array('B_ID' => $branch_info->B_ID), 'banks');
                    } */?>
                    <tr style="white-space: nowrap;">
                        <td><?/*= $advance->voucher_no */?></td>
                        <td><?php /*echo date('d-m-Y',strtotime($advance->payment_date)); */?></td>
                        <td><?/*= $job */?></td>
                        <td><?php /*echo $client; */?></td>
                        <td><?php /*echo $advance->payment_method */?></td>
                        <td><?php /*echo ($advance->payment_method == "Cash" || $advance->payment_method == "Third Party")?'--':$bank_info->B_NAME; */?></td>
                        <td><?php /*echo ($advance->payment_method == "Cash" || $advance->payment_method == "Third Party")?'--':$branch_info->BR_NAME; */?></td>
                        <td><?php /*echo number_format($advance->amount,2); */?></td>
                        <td>
                            <?php /*btn_edit('admin/accounts/manage_pre_job_advance/edit/'.$advance->ap_id) */?>
                            <?/*= btn_view('admin/accounts/manage_receipt_voucher/view_receipt_voucher/'.$advance->ap_id) */?>
                        </td>
                    </tr>
                <?php /*} */?>
            <?php /*endif; */?>
            </tbody>
        </table>
        </div>
    </div>-->
    <!-- /highlighting rows and columns -->
    <!--<script>
        $('#type').change(function () {
            var type=$(this).val();
            location.href= '<?/*= base_url('admin/accounts/receipt_voucher_list/') */?>'+type;
        });
    </script>-->

    <?php echo form_open(base_url('admin/accounts/receipt_voucher_list/'),array('class'=>"form-horizontal")); ?>

    <div class="row">
        <div class="col-lg-12">
            <section class="panel panel-default">
                <header class="panel-heading  "><?= lang('search_by'); ?></header>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-xs-3">
                            <select name="payment_type" class="form-control select_box" id="type" data-width="100%">
                                <option value="0" <?= ($payment_mode=='')?'selected':'' ?>>All</option>
                                <option value="1" <?= ($payment_mode==1)?'selected':'' ?>>Cash</option>
                                <option value="2" <?= ($payment_mode==2)?'selected':'' ?>>Bank</option>
                                <option value="3" <?= ($payment_mode==3)?'selected':'' ?>>Third Party</option>
                            </select>
                        </div>

                        <div class="col-xs-2">
                            <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> <?= lang('search') ?></button>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <?php echo form_close(); ?>
    <input type="hidden" id="payment_type" value="<?= (!empty($payment_mode))?$payment_mode:'All' ?>">
    <section class="panel panel-default">

        <header class="panel-heading"><?= lang('receipt_voucher_list') ?></header>

        <div class="panel-body" id="export_table">

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive" id="customers">
                        <table class="table table-bordered data-table-list dtHorizontalExample" id="tab_customers">
                            <thead>
                            <th><?= lang('voucher_no') ?></th>
                            <th>Payment Date</th>
                            <th><?= lang('reference_no') ?></th>
                            <th><?= lang('client') ?></th>
                            <th>Payment Method</th>
                            <th>Bank</th>
                            <th>Branch</th>
                            <th>Amount</th>
                            <th>Action</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <script>
        var payment_type=$('#payment_type').val();
        var data_url='<?= base_url('admin/accounts/receipt_voucher_lists/') ?>'+payment_type;
    </script>
