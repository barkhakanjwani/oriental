<link href="<?php echo base_url() ?>asset/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?php echo base_url() ?>asset/js/bootstrap-toggle.min.js"></script>
<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<div class="row">
    <div class="col-lg-12">
        <form role="form" enctype="multipart/form-data" id="form" action="<?php echo base_url(); ?>admin/accounts/save_direct_payment/<?= (!empty($advance_info))?$advance_info->ap_id:'' ?>" method="post" class="form-horizontal  ">
            <section class="panel panel-default">
                <header class="panel-heading"><?= $title ?></header>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><?= lang('reference_no') ?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <select name="invoice_id" class="form-control select_box required_select mb-5" data-width="100%" id="invoice_id" onchange="showClientName(this.value)">
                                <option value="">---</option>
                                <?php if(!empty($all_invoices)):
                                    foreach ($all_invoices as $invoice): ?>
                                        <option value="<?php echo $invoice->invoices_id; ?>" <?php
                                        if(!empty($advance_info)){
                                            echo ($advance_info->invoices_id == $invoice->invoices_id)?'selected':'';
                                        }
                                        ?>><?php echo $invoice->reference_no; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><?= lang('client') ?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <input type="text" id="client_name" readonly class="form-control required mb-5">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><?= lang('cheque_payorder') ?> <span class="text-danger"></span></label>
                        <div class="col-lg-3">
                            <input type="text" name="chq_no"class="form-control mb-5" value="<?= (!empty($advance_info))?$advance_info->cheque_payorder:'' ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <input type="number" name="amount" class="form-control required mb-5" min="0" step="any"  value="<?= (!empty($advance_info))?round($advance_info->amount,2):'' ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><?= lang('date') ?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <input type="text" name="date" class="form-control datepicker" autocomplete="off"  value="<?= (!empty($advance_info))?$advance_info->payment_date:'' ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><?= lang('description') ?></label>
                        <div class="col-lg-3">
                            <textarea name="desc" class="form-control mb-5"><?= (!empty($advance_info))?$advance_info->description:'' ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"></label>
                        <div class="col-lg-4">
                            <button type="submit" class="btn btn-sm btn-success pull-right"><i class="fa fa-check"></i>
                                <?php
                                if(!empty($advance_info)){
                                    echo lang('edit_payment');
                                }else{
                                    echo lang('payment');
                                }
                                ?>
                            </button>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>
</div>
<script>
    function showClientName(invoice) {
        $.getJSON("<?php echo site_url('admin/security_deposit/get_client_name') ?>" + "/" + invoice, function (result) {
            $('#client_name').val(result);
        });
    }

    $(document).ready(function () {
        showClientName($('#invoice_id').val());
    });
</script>