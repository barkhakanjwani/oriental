
<section class="content-header">
    <div class="row">
        <div class="col-md-12">
            <span class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('general_expense_list') ?></span> <a href="<?= base_url('admin/accounts/manage_exp_voucher')?>" class="btn btn-danger btn-xs" style="vertical-align: text-bottom;"><?= lang('add_new') ?></a>
        </div>
    </div>
</section>
<!-- Content area -->
<div class="content">
    <!-- Highlighting rows and columns -->
    <!--<div class="panel panel-flat">
        <table class="table table-bordered table-hover DataTables" id="DataTables">
            <thead>
            <tr>
                <th>Voucher No.</th>
                <th>Payment Mode</th>
                <th>Paid To</th>
                <th>Expenses</th>
                <th>Date</th>
                <th>Amount</th>
                <th>Created By</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php /*if(!empty($vouchers)): */?>
                <?php /*foreach ($vouchers as $voucher){ */?>
                    <tr style="white-space: nowrap;">
                        <td><?/*= $voucher->PC_VOUCHER_NO */?></td>
                        <td><?php /*echo "Cash"; */?></td>

                        <td><?php /*echo $voucher->staff_name; */?></td>
                        <td><?php
/*                            $details=petty_cash_details($voucher->PC_ID);
                            $datacount=count($details);
                            $ids = 1;
                            $sum_amount=0;
                            foreach ($details as $detail){
                                echo  $detail->A_NAME;
                                $sum_amount+=$detail->PCD_AMOUNT;
                                if ( $datacount != $ids) {
                                    echo ", ";
                                }
                                $ids++;
                            }
                            */?>
                        </td>
                        <td><?php /*echo date('d-m-Y',strtotime($voucher->PC_DATE)); */?></td>
                        <td><?php /*echo number_format($sum_amount,2); */?></td>
                        <td><?php /*echo $voucher->username; */?></td>-->
                        <!--<td><label class="label label-success"><?php /*echo $voucher->PC_STATUS; */?></label></td>-->
                        <!--<td>
                            <?php /*btn_edit('admin/accounts/manage_exp_voucher/edit_voucher/'.$voucher->PC_ID) */?>
                            <?/*= btn_view('admin/accounts/manage_exp_voucher/voucher_detail/'.$voucher->PC_ID) */?>
                        </td>
                    </tr>
                <?php /*} */?>
            <?php /*endif; */?>
            </tbody>
        </table>
    </div>-->
    <!-- /highlighting rows and columns -->

    <section class="panel panel-default">

        <header class="panel-heading"><?= lang('general_expense_list') ?></header>

        <div class="panel-body" id="export_table">

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive" id="customers">
                        <table class="table table-bordered data-table-list dtHorizontalExample" id="tab_customers">
                            <thead>
                            <th>Voucher No.</th>
                            <th>Payment Mode</th>
                            <th>Paid To</th>
                            <th>Expenses</th>
                            <th>Date</th>
                            <th>Amount</th>
                            <th>Created By</th>
                            <th>Action</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <script>
        var data_url='<?= base_url('admin/accounts/exp_voucher') ?>';
    </script>
