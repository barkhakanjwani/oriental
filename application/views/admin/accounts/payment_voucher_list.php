<!-- Content area -->
<section class="content-header">
    <div class="row">
        <div class="col-md-12">
            <span class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('payment_voucher_list') ?></span> <a href="<?= base_url('admin/accounts/manage_payment_voucher')?>" class="btn btn-danger btn-xs" style="vertical-align: text-bottom;"><?= lang('add_new') ?></a>
        </div>
    </div>
</section>

<div class="content">
    <!-- Highlighting rows and columns -->
    <!--<div class="panel panel-flat">
        <div class="panel-heading">
            <div class="form-group">
                <div class="col-lg-3">
                    <select name="payment_type" class="form-control select_box" id="type" data-width="100%">
                        <option value="" <?/*= ($payment_mode=='')?'selected':'' */?>>All</option>
                        <option value="1" <?/*= ($payment_mode==1)?'selected':'' */?>>Cash</option>
                        <option value="2" <?/*= ($payment_mode==2)?'selected':'' */?>>Bank</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="panel-body">
        <table class="table table-bordered table-hover DataTables" id="DataTables">
            <thead>
            <tr>
                <th><?/*= lang('voucher_no') */?></th>
                <th><?/*= lang('payment_mode') */?></th>
                <th><?/*= lang('bank') */?></th>
                <th><?/*= lang('branches') */?></th>
                <th><?/*= lang('paid_to').' / '.lang('by') */?></th>
                <th><?/*= lang('payment_date') */?></th>
                <th><?/*= lang('amount') */?></th>
                <th><?/*= lang('description') */?></th>
                <th class="text-center"><?/*= lang('action') */?></th>
            </tr>
            </thead>
            <tbody>
            <?php /*if(!empty($payment_vouchers)): */?>
                <?php /*foreach ($payment_vouchers as $pv){
                    $payment_mode='Cash';
                    $bank_name='--';
                    $branch_name='--';
                    if ($pv->PAYMENT_MODE==2){
                        $payment_mode='Bank';
                        $branch=$this->invoice_model->check_by(array('A_ID'=>$pv->ACCOUNT_CREDIT), 'branches');
                        $branch_name=$branch->BR_NAME;
                        $bank=$this->invoice_model->check_by(array('B_ID'=>$branch->B_ID), 'banks');
                        $bank_name=$bank->B_NAME;
                    }
                    */?>
                        <tr style="white-space: nowrap;">
                            <td><?/*= $pv->PV_VOUCHER_NO */?></td>
                            <td><?/*= $payment_mode */?></td>
                            <td><?/*= $bank_name */?></td>
                            <td><?/*= $branch_name */?></td>
                            <td><?/*= $pv->paid_account */?></td>
                            <td><?/*= date('d-m-Y',strtotime($pv->PV_DATE)) */?></td>
                            <td><?/*= $pv->PV_AMOUNT */?></td>
                            <td><?/*= $pv->PV_DESC */?></td>
                            <td class="text-center">
                                <?/*= btn_view('admin/accounts/manage_payment_voucher/view_payment_voucher/'.$pv->PV_ID) */?>
                            </td>
                        </tr>
                <?php /*} */?>
            <?php /*endif; */?>
            </tbody>
        </table>
        </div>
    </div>-->
    <!-- /highlighting rows and columns -->
    <!--<script>
        $('#type').change(function () {
            var type=$(this).val();
            location.href= '<?/*= base_url('admin/accounts/payment_voucher_list/') */?>'+type;
        });
    </script>-->

    <?php echo form_open(base_url('admin/accounts/payment_voucher_list/'),array('class'=>"form-horizontal")); ?>

    <div class="row">
        <div class="col-lg-12">
            <section class="panel panel-default">
                <header class="panel-heading  "><?= lang('search_by'); ?></header>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-xs-3">
                            <select name="payment_type" class="form-control select_box" id="type" data-width="100%">
                                <option value="0" <?= ($payment_mode=='')?'selected':'' ?>>All</option>
                                <option value="1" <?= ($payment_mode==1)?'selected':'' ?>>Cash</option>
                                <option value="2" <?= ($payment_mode==2)?'selected':'' ?>>Bank</option>
                            </select>
                        </div>

                        <div class="col-xs-2">
                            <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> <?= lang('search') ?></button>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <?php echo form_close(); ?>
    <input type="hidden" id="payment_type" value="<?= (!empty($payment_mode))?$payment_mode:'All' ?>">
    <section class="panel panel-default">

        <header class="panel-heading"><?= lang('payment_voucher_list') ?></header>

        <div class="panel-body" id="export_table">

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive" id="customers">
                        <table class="table table-bordered data-table-list dtHorizontalExample" id="tab_customers">
                            <thead>
                            <th><?= lang('voucher_no') ?></th>
                            <th><?= lang('payment_mode') ?></th>
                            <th><?= lang('bank') ?></th>
                            <th><?= lang('branches') ?></th>
                            <th><?= lang('paid_to').' / '.lang('by') ?></th>
                            <th><?= lang('payment_date') ?></th>
                            <th><?= lang('amount') ?></th>
                            <th><?= lang('description') ?></th>
                            <th class="text-center"><?= lang('action') ?></th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <script>
        var payment_type=$('#payment_type').val();
        var data_url='<?= base_url('admin/accounts/payment_voucher_lists/') ?>'+payment_type;
    </script>