<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('edit_job_advances') ?></h1>
<div class="row">
    <div class="col-lg-12">
        <form role="form" enctype="multipart/form-data" id="form_v" action="<?php echo base_url(); ?>admin/accounts/save_pre_job_advance/<?= (!empty($advance_info))?encrypt($advance_info->ap_id):'' ?>" method="post" class="form-horizontal  ">
            <section class="panel panel-default">
                <header class="panel-heading"><?= $title ?></header>
                <?php
                if(!empty($advance_info)) {
                    $trans_account_info = $this->db->select('*')
                        ->from('transactions_meta')
                        ->where('T_ID',$advance_info->transaction_id)
                        ->get()->result();
                    ?>
                    <input type="hidden" name="transaction_id" value="<?= $advance_info->transaction_id ?>" />
                    <?php
                } ?>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-lg-4 control-label"></div>
                        <div class="col-lg-3">
                            <input type="radio" value="Job" name="expense_type" class="expense_type" <?= (!empty($advance_info) && $advance_info->expense_type=='Job')?'checked':'' ?> required /> Job
                            <input type="radio" value="Misc" name="expense_type" class="expense_type" <?= (!empty($advance_info) && $advance_info->expense_type=='Misc')?'checked':'' ?> required /> Client
                        </div>
                    </div>
                    <div id="job_expense" style="display:<?= ( $advance_info->expense_type=='Job')?'block':'none'; ?>">
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('reference_no') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="invoice_id" class="form-control select_box mb-5" data-width="100%" id="invoice_id" onchange="showClientName(this.value)" required>
                                    <option value="">---</option>
                                    <?php if(!empty($all_invoices)):
                                        foreach ($all_invoices as $invoice): ?>
                                            <option value="<?php echo $invoice->invoices_id; ?>" <?php
                                            if(!empty($advance_info)){
                                                echo ($advance_info->invoices_id == $invoice->invoices_id)?'selected':'';
                                            }
                                            ?>><?php echo $this->invoice_model->job_no_creation($invoice->invoices_id); ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('client') ?></label>
                            <div class="col-lg-3">
                                <input type="text" id="client_name" readonly class="form-control mb-5">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('payment_mode') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="payment_type" class="form-control select_box" id="type" data-width="100%" required>
                                    <option value="Cash" <?= ($advance_info->payment_method=='Cash')?'selected':'' ?>>Cash</option>
                                    <option value="Bank" <?= ($advance_info->payment_method=='Bank')?'selected':'' ?>>Bank</option>
                                    <option value="Third Party" <?= ($advance_info->payment_method=='Third Party')?'selected':'' ?>>Third Party</option>
                                </select>
                            </div>
                        </div>
                        <div id="cash_at_bank" style="display:<?= ($advance_info->payment_method == 'Bank')?'block':'none'; ?>">
                            <?php
                            if(!empty($advance_info) && $advance_info->payment_method == 'Bank') {
                                $branch_info = $this->invoice_model->check_by(array('A_ID' => $advance_info->debit_account), 'branches');
                                $bank_info = $this->invoice_model->check_by(array('B_ID' => $branch_info->B_ID), 'banks');
                            }
                            ?>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Banks <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <select name="bank" class="form-control cash_at_bank select_box" onchange="selectBranches(this.value)" data-width="100%" required>
                                        <option value="">---</option>
										<?php foreach ($banks as $bank): ?>
                                            <option value="<?php echo $bank->B_ID; ?>" <?php
												if($advance_info->payment_method == 'Bank') {
                                                    echo ($bank->B_ID == $bank_info->B_ID) ? 'selected' : '';
                                                }?>><?php echo $bank->B_NAME; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div id="branches">
                                <?php
                                if(!empty($advance_info) && $advance_info->payment_method == "Bank") {
                                    ?>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">Branches <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <select name="branch_id" class="form-control cash_at_bank select_box" onchange="getAccountId(this.value)" data-width="100%" required>
                                                <?php
                                                $branches = $this->invoice_model->check_by_all(array('B_ID' => $bank_info->B_ID), 'branches');
                                                foreach ($branches as $branch) { ?>
                                                    <option value="<?= $branch->BR_ID ?>" <?= ($branch->BR_ID == $branch_info->BR_ID) ? 'selected' : '' ?>><?= $branch->BR_NAME ?></option>
                                                    <?php
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php
                                } ?>
                            </div>
                            <input type="hidden" id="account_id" name="account_id" value="<?= (!empty($advance_info))?$advance_info->debit_account:'' ?>" />
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= 'Instrument Type' ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <select name="instrument_type" class="form-control select_box" id="instrument_type" data-width="100%" required>
										<option value="">---</option>
                                        <option value="Pay Order" <?= ($advance_info->instrument_type=='Pay Order')?'selected':'' ?>>Pay Order</option>
                                        <option value="Demand Draft" <?= ($advance_info->instrument_type=='Demand Draft')?'selected':'' ?>>Demand Draft</option>
                                        <option value="T.T" <?= ($advance_info->instrument_type=='T.T')?'selected':'' ?>>T.T</option>
                                        <option value="Cheque" <?= ($advance_info->instrument_type=='Cheque')?'selected':'' ?>>Cheque</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= lang('cheque_payorder') ?> </label>
                                <div class="col-lg-3">
                                    <input type="number" name="cheque_payorder" class="form-control mb-5" value="<?= $advance_info->cheque_payorder ?>">
                                </div>
                            </div>
                        </div>
                        <div>
                            <?php
                            $k=0;
                            if($advance_info->payment_method == 'Third Party') {
                                foreach ($advance_details as $advance_detail) { ?>
                                    <div>
                                        <div class="form-group pay_order">
                                            <label class="col-lg-4 control-label"><?= lang('title') ?> <span class="text-danger">*</span></label>
                                            <div class="col-lg-3">
                                                <select class="form-control select_box title" name="pay_order_title[]" data-width="100%" required>
                                                    <option value="Security Deposit" <?= ($advance_detail->apd_title=='Security Deposit')?'selected':'' ?>>Security Deposit</option>
                                                    <option value="Collector of Custom" <?= ($advance_detail->apd_title=='Collector of Custom')?'selected':'' ?>>Collector of Custom</option>
                                                    <option value="Excise and Taxation" <?= ($advance_detail->apd_title=='Excise and Taxation')?'selected':'' ?>>Excise and Taxation</option>
                                                    <option value="Delivery Order" <?= ($advance_detail->apd_title=='Delivery Order')?'selected':'' ?>>Delivery Order</option>
                                                    <option value="Terminal" <?= ($advance_detail->apd_title=='Terminal')?'selected':'' ?>>Terminal</option>
                                                    <option value="LOLO" <?= ($advance_detail->apd_title=='LOLO')?'selected':'' ?>>LOLO</option>
                                                    <option value="Late DO" <?= ($advance_detail->apd_title=='Late DO')?'selected':'' ?>>Late DO</option>
                                                    <option value="Container Detention" <?= ($advance_detail->apd_title=='Container Detention')?'selected':'' ?>>Container Detention</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group pay_order">
                                            <label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label>
                                            <div class="col-lg-3">
                                                <input type="number" name="pay_order_amount[]" class="form-control mb-5" min="0" step="any" value="<?= round($advance_detail->apd_amount,2) ?>" required>
                                            </div>
                                        </div>
                                        <div class="form-group  pay_order">
                                            <label class="col-lg-4 control-label"><?= lang('cheque_payorder') ?></label>
                                            <div class="col-lg-3">
                                                <input type="text" name="pay_order_no[]" class="form-control mb-5" value="<?= $advance_detail->apd_pay_order ?>">
                                            </div>
                                            <?php if($k==0){ ?>
                                                <div class="col-lg-2">
                                                    <button type="button" class="btn btn-primary btn-xs" id="add"><i class="fa fa-plus"></i></button>
                                                </div>
                                            <?php }elseif($k>0){ ?>
                                                <div class="col-lg-2">
                                                    <button type="button" class="btn btn-danger btn-xs remCF2"><i class="fa fa-minus"></i></button>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php $k++; }
                            }else {
                                ?>
								<div class="form-group pay_order" style="display: none">
									<label class="col-lg-4 control-label"><?= lang('title') ?> <span class="text-danger">*</span></label>
									<div class="col-lg-3">
										<select class="form-control select_box required title" name="pay_order_title[]" data-width="100%" required>
											<option value="">---</option>
											<option value="Security Deposit">Security Deposit</option>
											<option value="Collector of Custom">Collector of Custom</option>
											<option value="Excise and Taxation">Excise and Taxation</option>
											<option value="Delivery Order">Delivery Order</option>
											<option value="Terminal">Terminal</option>
											<option value="LOLO">LOLO</option>
											<option value="Late DO">Late DO</option>
											<option value="Container Detention">Container Detention</option>
										</select>
									</div>
								</div>
								<div class="form-group pay_order" style="display: none">
									<label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label>
									<div class="col-lg-3">
										<input type="number" name="pay_order_amount[]" class="form-control required mb-5" min="0" step="any" required>
									</div>
								</div>
								<div class="form-group  pay_order" style="display: none">
									<label class="col-lg-4 control-label"><?= lang('cheque_payorder') ?></label>
									<div class="col-lg-3">
										<input type="text" name="pay_order_no[]" class="form-control mb-5">
									</div>
									<div class="col-lg-2">
										<button type="button" class="btn btn-primary btn-xs" id="add"><i
													class="fa fa-plus"></i></button>
									</div>
								</div>
                                <?php
                            }
							?>
                        </div>
                        <div id="new"></div>
                        <div class="form-group" id="amount" style="<?php echo ($advance_info->payment_method != 'Third Party')?'display:block;':'display:none;'; ?>">
                            <label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="number" name="amount" class="form-control mb-5" min="0" step="any" value="<?= round($advance_info->amount,2) ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('date') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="text" name="date" class="form-control datepicker" autocomplete="off" value="<?= (!empty($advance_info))?$advance_info->payment_date:'' ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('description') ?></label>
                            <div class="col-lg-3">
                                <textarea name="desc" class="form-control mb-5"><?= (!empty($advance_info))?$advance_info->description:'' ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div id="misc_expense" style="display: <?= ( $advance_info->expense_type=='Misc')?'block':'none'; ?>">
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('client') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="client_id" class="form-control select_box mb-5" data-width="100%" required>
                                    <option value="">---</option>
									<?php if(!empty($all_clients)):
                                        foreach ($all_clients as $client): ?>
                                            <option value="<?php echo $client->client_id; ?>" <?= ($advance_info->client_id == $client->client_id)?'selected':''; ?>><?php echo $client->name; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('payment_mode') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="misc_payment_type" class="form-control select_box required_select" id="misc_type" data-width="100%" required>
                                    <option value="">---</option>
									<option value="Cash" <?php
										if($advance_info->expense_type == 'Misc') {
                                            echo ($advance_info->payment_method == 'Cash') ? 'selected' : '';
                                        }?>>Cash</option>
                                    <option value="Bank" <?php
                                        if($advance_info->expense_type == 'Misc') {
                                            echo ($advance_info->payment_method == 'Bank') ? 'selected' : '';
                                        }?>>Bank</option>
                                </select>
                            </div>
                        </div>
                        <div id="misc_cash_at_bank" style="display:<?php echo ($advance_info->payment_method == "Bank" && $advance_info->expense_type == 'Misc')?'block':'none' ?>;">
                            <?php
                            if($advance_info->payment_method == "Bank" && $advance_info->expense_type == 'Misc') {
                                $branch_info = $this->invoice_model->check_by(array('A_ID' => $advance_info->debit_account), 'branches');
                                $bank_info = $this->invoice_model->check_by(array('B_ID' => $branch_info->B_ID), 'banks');
                            }
                            ?>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Banks <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <select name="misc_bank" class="form-control misc_cash_at_bank select_box" onchange="selectMiscBranches(this.value)" data-width="100%" required>
                                        <option value="">---</option>
										<?php foreach ($banks as $bank): ?>
                                            <option value="<?php echo $bank->B_ID; ?>" <?php
                                                if ($advance_info->payment_method == "Bank" && $advance_info->expense_type == 'Misc') {
                                                    echo ($bank->B_ID == $bank_info->B_ID) ? 'selected' : '';
                                                }
                                            ?>><?php echo $bank->B_NAME; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div id="misc_branches">
                                <?php
                                if(!empty($advance_info) && $advance_info->payment_method == "Bank" && $advance_info->expense_type == 'Misc') {
                                    ?>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">Branches <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <select name="misc_branch_id" class="form-control cash_at_bank select_box" onchange="getMiscAccountId(this.value)" data-width="100%" required>
                                                <option value="">---</option>
												<?php
                                                $branches = $this->invoice_model->check_by_all(array('B_ID' => $bank_info->B_ID), 'branches');
                                                foreach ($branches as $branch) { ?>
                                                    <option value="<?= $branch->BR_ID ?>" <?= ($branch->BR_ID == $branch_info->BR_ID) ? 'selected' : '' ?>><?= $branch->BR_NAME ?></option>
                                                    <?php
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php
                                } ?>
                            </div>
                            <input type="hidden" id="misc_account_id" name="misc_account_id" value="<?= $advance_info->debit_account ?>"/>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="number" name="misc_amount" class="form-control mb-5" min="0" step="any" value="<?= round($advance_info->amount,2) ?>" required>
                            </div>
                        </div>
                        <div id="misc_instrument" style="display:<?php echo ($advance_info->payment_method != "Cash")?'block':'none' ?>;">
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= 'Instrument Type' ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <select name="misc_instrument_type" class="form-control select_box required_select" id="instrument_type" data-width="100%" required>
                                        <option value="Pay Order" <?= ($advance_info->instrument_type=='Pay Order')?'selected':'' ?>>Pay Order</option>
                                        <option value="Demand Draft" <?= ($advance_info->instrument_type=='Demand Draft')?'selected':'' ?>>Demand Draft</option>
                                        <option value="T.T" <?= ($advance_info->instrument_type=='T.T')?'selected':'' ?>>T.T</option>
                                        <option value="Cheque" <?= ($advance_info->instrument_type=='Cheque')?'selected':'' ?>>Cheque</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= lang('cheque_payorder') ?> </label>
                                <div class="col-lg-3">
                                    <input type="number" name="misc_pay_order" class="form-control mb-5" value="<?= $advance_info->cheque_payorder ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('date') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="text" name="misc_date" class="form-control datepicker" autocomplete="off" value="<?= $advance_info->payment_date ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('description') ?></label>
                            <div class="col-lg-3">
                                <textarea name="misc_desc" class="form-control mb-5"><?= $advance_info->description ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"></label>
                        <div class="col-lg-4">
                            <button type="submit" class="btn btn-sm btn-success pull-right"><i class="fa fa-check"></i>
                                <?php
                                if(!empty($advance_info)){
                                    echo lang('edit_payment');
                                }else{
                                    echo lang('payment');
                                }
                                ?>
                            </button>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>
</div>
<script type="text/javascript">
    $('.expense_type').click(function () {
        $('.pageloader').show();
        var val = $(this).val();
        if (val == 'Misc'){
            $('#job_expense').hide();
            $('#misc_expense').show();
        }else {
            $('#misc_expense').hide();
            $('#job_expense').show();
        }
        $('.pageloader').hide();
    });
    $('#type').on("change", function() {
        $('.pageloader').show();
        var type = $(this).val();
        if(type == ''){
            $('#transfer_to_bank').hide();
            $('.pay_order').hide();
            $('#cash_at_bank').hide();
            $('.self_cash_at_bank').hide();
        }
        else if(type == 'Third Party'){
            $('#transfer_to_bank').hide();
            $('#amount').hide();
            $('.pay_order').show();
            $('#cash_at_bank').hide();
        }else if(type == 'Cash'){
            $('.pay_order').hide();
            $('#cash_at_bank').hide();
            $('.self_cash_at_bank').hide();
            $('#amount').show();
            $('#transfer_to_bank').show();
        }
        else{
            $('#transfer_to_bank').hide();
            $('.pay_order').hide();
            $('.self_cash_at_bank').hide();
            $('#amount').show();
            $('#cash_at_bank').show();
        }
        $('.pageloader').hide();
    });
    $('#transfer_check').click(function () {
        if ($(this).prop('checked')==true){
            $('#transfer_div').show();
        }else {
            $('#transfer_div').hide();
        }
    });
    $('#misc_type').on("change", function() {
        var type = $(this).val();
        if(type == 'Cash'){
            $('#misc_cash_at_bank').hide();
            $('#misc_instrument').hide();
        }
        else{
            $('#misc_cash_at_bank').show();
            $('#misc_instrument').show();
        }
    });
    function selectBranches(val) {
        var option="";
        if(val == '') {
            $('#branches').hide();
        }
        else{
            $('.pageloader').show();
            $.getJSON("<?php echo site_url('admin/accounts/ajax_select_branches') ?>" + "/" + val, function (result) {
                $.each(result, function (index, value) {
                    option = ('<option value="' + value.BR_ID + '">' + value.BR_NAME + '</option>')+option;
                });
                var drop = '<div class="form-group"><label class="col-md-4 control-label">Branches <span class="text-danger">*</span></label><div class="col-lg-3" id="branches"><select class="form-control select_box" onchange="getAccountId(this.value)" data-width="100%" required><option value="">---</option>' + option + '</select></div></div>';
                $('#branches').html(drop).hide().fadeIn(500);
                $('.select_box').select2({});
                $('.pageloader').hide();
                getAccountId();
            });
        }
    }
    function selectMiscBranches(val) {
        var option="";
        if(val == '') {
            $('#misc_branches').hide();
        }
        else{
            $('.pageloader').show();
            $.getJSON("<?php echo site_url('admin/accounts/ajax_select_branches') ?>" + "/" + val, function (result) {
                $.each(result, function (index, value) {
                    option = ('<option value="' + value.BR_ID + '">' + value.BR_NAME + '</option>')+option;
                });
                var drop = '<div class="form-group"><label class="col-md-4 control-label">Branches <span class="text-danger">*</span></label><div class="col-lg-3" id="misc_branches"><select class="form-control select_box" onchange="getMiscAccountId(this.value)" data-width="100%" required><option value="">---</option>' + option + '</select></div></div>';
                $('#misc_branches').html(drop).hide().fadeIn(500);
                $('.select_box').select2({});
                $('.pageloader').hide();
                getMiscAccountId();
            });
        }
    }

    function selectTransferBranches(val) {
        var option="";
        if(val == '') {
            $('#branches_to').hide();
        }
        else{
            $('.pageloader').show();
            $.getJSON("<?php echo site_url('admin/accounts/ajax_select_branches') ?>" + "/" + val, function (result) {
                $.each(result, function (index, value) {
                    option = ('<option value="' + value.BR_ID + '">' + value.BR_NAME + '</option>')+option;
                });
                var drop = '<div class="form-group"><label class="col-md-4 control-label">Branches <span class="text-danger">*</span></label><div class="col-lg-3" id="branches"><select  name="branch_to" class="form-control select_box" onchange="getTransferAccountId(this.value)" data-width="100%" required><option value="">---</option>' + option + '</select></div></div>';
                $('#branches_to').html(drop).hide().fadeIn(500);
                $('.select_box').select2({});
                $('.pageloader').hide();
                getTransferAccountId();
            });
        }
    }

    function getTransferAccountId(val){
        var account_id=$('#transfer_account_id');
        $.getJSON( "<?php echo site_url('admin/accounts/ajax_get_branch_aid') ?>"+"/"+val, function(result) {
            (result != null)?account_id.val(result.A_ID):'';
        });
    }

    function getAccountId(val){
        account_id=$('#account_id');
        $.getJSON( "<?php echo site_url('admin/accounts/ajax_get_branch_aid') ?>"+"/"+val, function(result) {
            (result != null)?account_id.val(result.A_ID):'';
        });
    }

    function getMiscAccountId(val){
        $.getJSON( "<?php echo site_url('admin/accounts/ajax_get_branch_aid') ?>"+"/"+val, function(result) {
            (result != null)?$('#misc_account_id').val(result.A_ID):'';
        });
    }

    function showClientName(invoice) {
        $.getJSON("<?php echo site_url('admin/security_deposit/get_client_name') ?>" + "/" + invoice, function (result) {
            $('#client_name').val(result);
        });
    }

    $(document).ready(function () {
        showClientName($('#invoice_id').val());
    });

    $(document).ready(function () {
        var count=<?= $k ?>;
        $("#add").click(function() {
            var columns = '<div class="bg-warning"><div class="form-group pay_order">';
            columns += '<label class="col-lg-4 control-label"><?= lang("title") ?> <span class="text-danger">*</span></label>';
            columns += '<div class="col-lg-3">';
            columns += '<select class="form-control select_box title" name="pay_order_title[]" data-width="100%" required>';
            columns += '<option value="">---</option>';
            columns += '<option value="Security Deposit">Security Deposit</option>';
            columns += '<option value="Collector of Custom">Collector of Custom</option>';
            columns += '<option value="Excise and Taxation">Excise and Taxation</option>';
            columns += '<option value="Delivery Order">Delivery Order</option>';
            columns += '<option value="Terminal">Terminal</option>';
            columns += '<option value="LOLO">LOLO</option>';
            columns += '<option value="Late DO">Late DO</option>';
            columns += '<option value="Container Detention">Container Detention</option>';
            columns += '</select>';
            columns += '</div>';
            columns += '</div>';
            columns += '<div class="form-group pay_order">';
            columns += '<label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label>';
            columns += '<div class="col-lg-3">';
            columns += '<input type="number" name="pay_order_amount[]" class="form-control required mb-5" min="0" step="any" required>';
            columns += '</div>';
            columns += '</div>';
            columns += ' <div class="self_cash_at_bank" style="display: none;">';
            columns += '<div class="form-group">';
            columns += '<label class="col-lg-4 control-label">Banks <span class="text-danger">*</span></label>';
            columns += '<div class="col-lg-3">';
            columns += '<select name="self_bank[]" class="form-control select_box self_bank" onchange="selectSelfBranches('+count+',this.value)" data-width="100%" required>';
            columns += '<option value="">---</option>';
            columns += '<?php foreach ($banks as $bank): ?>';
            columns += '<option value="<?php echo $bank->B_ID; ?>"><?php echo $bank->B_NAME; ?></option>';
            columns += '<?php endforeach; ?>';
            columns += '</select>';
            columns += '</div>';
            columns += '</div>';
            columns += '<div class="self_branches">';
            columns += '<div class="form-group">';
            columns += '<label class="col-lg-4 control-label">Branches <span class="text-danger">*</span></label>';
            columns += ' <div class="col-lg-3">';
            columns += '<select name="self_branch[]" class="form-control select_box self_branch" id="self_branch'+count+'" onchange="getSelfAccountId('+count+',this.value)" data-width="100%" required>';
            columns += '<option value="">Choose Branch</option>';
            columns += '</select>';
            columns += '</div>';
            columns += '</div>';
            columns += '</div>';
            columns += '<input type="hidden" id="self_account_id'+count+'" name="self_account_id[]" />';
            columns += '</div>';
            columns += '<div class="form-group pay_order">';
            columns += '<label class="col-lg-4 control-label"><?= lang('cheque_payorder') ?></label>';
            columns += '<div class="col-lg-3">';
            columns += '<input type="text" name="pay_order_no[]" class="form-control mb-5">';
            columns += '</div>';
            columns += '<div class="col-lg-2">';
            columns += '<button type="button" class="btn btn-danger btn-xs remCF"><i class="fa fa-minus"></i></button>';
            columns += '</div>';
            columns += '</div>';
            columns += '</div>';
            var add_new = $(columns);
            count++;
            $("#new").append(add_new.hide().fadeIn(1000));
            $( '.select_box' ).select2({});
        });
        $("#new").on('click', '.remCF', function() {
            $(this).fadeOut(300, function() { $(this).parent().parent().parent().remove(); })
        });
        $(document).on('focus', '.datepicker', function() {
            $( this ).datepicker({format: 'yyyy-mm-dd',autoclose: true});
        });
        $(".remCF2").on('click', function() {
            $(this).fadeOut(300, function() { $(this).parent().parent().parent().remove(); })
        });
    });
    /*** SELF ***/
    $(document).on('change', ".title",function () {
        $('.pageloader').show();
        $(this).each(function() {
            if ($(this).val().length > 0) {
                if ($(this).val() == 'Self') {
                    $(this).parent().parent().parent().find('.self_cash_at_bank').show();
                }else{
                    $(this).parent().parent().parent().find('.self_cash_at_bank').hide();
                }
            }else{
                $(this).parent().parent().parent().find('.self_cash_at_bank').hide();
            }
            $('.pageloader').hide();
        });
    });

    function selectSelfBranches(counter,val){
        $('.pageloader').show();
        $('#self_branch'+counter).empty();
        $.getJSON("<?php echo site_url('admin/accounts/ajax_select_branches') ?>" + "/" + val, function (result) {
            var option="";
            $.each(result, function (index, value) {
                option = ('<option value="' + value.BR_ID + '">' + value.BR_NAME + '</option>')+option;
            });
            var drop = '<div class="form-group"><label class="col-md-4 control-label">Branches <span class="text-danger">*</span></label><div class="col-lg-3" id="branches"><select class="form-control select_box" onchange="getAccountId(this.value)" data-width="100%" required><option value="">-</option>' + option + '</select></div></div>';
            $('#self_branch'+counter).html(drop).hide().fadeIn(500);
            $('.pageloader').hide();
            $('.select_box').select2({});
            getSelfAccountId();
        });
    }
    function getSelfAccountId(counter,val){
        account_id=$('#self_account_id'+counter);
        $.getJSON( "<?php echo site_url('admin/accounts/ajax_get_branch_aid') ?>"+"/"+val, function(result) {
            (result != null)?account_id.val(result.A_ID):'';
        });
    }
    /*** END SELF ***/

</script>