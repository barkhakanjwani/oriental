<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cheque System | Admin Login </title>
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/images/icon.png">
    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/icons/icomoon/styles.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/minified/bootstrap.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/minified/core.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/minified/components.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/minified/colors.min.css');?>" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/loaders/pace.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/core/libraries/jquery.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/core/libraries/bootstrap.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/loaders/blockui.min.js');?>"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/forms/styling/uniform.min.js');?>"></script>

    <script type="text/javascript" src="<?php echo base_url('assets/js/core/app.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/pages/login.js');?>"></script>
    <!-- /theme JS files -->
    <style>
        body{
            background-image: url("<?php echo site_url('assets/images/bg.jpg')?>" );
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>

</head>

<body class="bg-slate-800">

<!-- Page container -->
<div class="page-container login-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                <!-- Advanced login -->
                <form action="#" method="POST" id='login' class='login'>
                    <div class="panel panel-body login-form" style="opacity:0.9">
                        <div class="text-center">
                            <div class="icon-object border-info-400 text-blue-400"><i class="icon-people"></i></div>
                            <h5 class="content-group-lg">Login to your admin account <small class="display-block">Enter your credentials</small></h5>
                        </div>
                        <div class="message alert alert-styled-left"></div>
                        <div class="form-group has-feedback has-feedback-left">
                            <input type="email" name="email" id="u_email" class="form-control" placeholder="Email">
                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group has-feedback has-feedback-left">
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                        </div>
                        <div class="form-group">
                            <!--<button type="submit" name="submit" class="btn bg-blue btn-block">Login <i class="icon-circle-right2 position-right"></i></button>-->
                            <input type="submit" name="submit" class="btn bg-blue btn-block" value="Sign In">
                        </div>
                    </div>
                </form>
                <!-- /advanced login -->


                <!-- Footer -->
                <div class="footer text-white">
                    &copy; 2017. <a href="#" class="text-white">Cheque System</a> by <a href="#" class="text-white" target="_blank">yy technologies</a>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
<script type="text/javascript">
    $(document).ready(function() {
        $('.message').hide();
        var form = $('#login');
        form.on('submit', function(e) {
            e.preventDefault();
            var email = $('#u_email').val();
            var password = $('#password').val();
            if (email == '') {
                $('#u_email').addClass('border-warning');
                $('.message').hide();
            } else {
                $('#u_email').removeClass('border-warning');
            }
            if (password == '') {
                $('#password').addClass('border-warning');
                $('.message').hide();
            } else {
                $('#password').removeClass('border-warning');
            }
            if (email != '' && password != '') {
                $.ajax({
                    type: 'POST',
                    data: form.serialize(),
                    url: '<?php echo site_url('Admin/login'); ?>',
                    dataType: 'JSON',
                    success: function(data) {
                        if (data != false ) {
                            $('.message').removeClass('alert-warning');
                            $('.message').addClass('alert-success');
                            $('.message').show().html('login success').hide(3000);
                            $('#u_email').removeClass('border-warning');
                            $('#password').removeClass('border-warning');
                            setTimeout(function() {
                                location.href = "<?php echo base_url('admin/home'); ?>"
                            }, 2500);
                        } else {
                            $('.message').removeClass('alert-success');
                            $('.message').addClass('alert-warning');
                            $('.message').show().html('login failed. Try again');
                            $('#u_email').addClass('border-warning');
                            $('#password').addClass('border-warning');
                        }
                    },
                    error: function(data) {
                        console.log(data.responseText);
                    }
                });
            }
        });
    });
</script>





