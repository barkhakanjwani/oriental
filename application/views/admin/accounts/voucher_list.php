<section class="content-header">
    <div class="row">
        <div class="col-md-12">
			<span class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('general_voucher_list') ?></span> <a href="#" data-toggle="modal" data-target="#add_voucher" class="btn btn-danger btn-xs" style="vertical-align: text-bottom;"><?= lang('add_new') ?></a>
		</div>
    </div>
</section>
<!-- Content area -->
<div class="content">
        <!-- Highlighting rows and columns -->
        <!--<div class="panel panel-flat">
            <table class="table table-bordered table-hover DataTables " id="DataTables">
                <thead>
                <tr>
                    <th>Debit Account</th>
                    <th>Credit Account</th>
                    <th>Paid To</th>
                    <th>Cheque No</th>
                    <th>Date</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php /*if(!empty($vouchers)): */?>
                    <?php /*foreach ($vouchers as $voucher){ */?>
                        <tr style="white-space: nowrap;">
                            <td><?php /*echo $voucher->DEBIT_NAME; */?></td>
                            <td><?php /*echo $voucher->CREDIT_NAME; */?></td>
                            <td><?php /*echo $voucher->PAID_TO; */?></td>
                            <td><?php /*echo $voucher->CHEQUE_NO; */?></td>
                            <td><?php /*echo date('d-m-Y',strtotime($voucher->E_DATE)); */?></td>
                            <td><?php /*echo number_format($voucher->AMOUNT,2); */?></td>
                            <td><?php /*echo $voucher->E_STATUS; */?></td>
                            <td>
                                <button type="button" onclick="getEditVoucher(this.value)" value="<?php /*echo $voucher->E_ID;*/?>" data-popup="tooltip" data-container="body" data-original-title="Edit Voucher" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#" ><i class="fa fa-edit"></i></button>
                                <a target="_blank" href="<?php /*echo base_url('admin/accounts/view_voucher/'.encode($voucher->E_ID)); */?>" type="button" data-popup="tooltip" title="" data-container="body" data-original-title="View Voucher" class="btn btn-info btn-xs"><i class="fa fa-list-alt"></i></a>
                            </td>
                        </tr>
                    <?php /*} */?>
                <?php /*endif; */?>
                </tbody>
            </table>
        </div>-->
        <!-- /highlighting rows and columns -->



        <!-- Add Voucher modal -->
        <div id="add_voucher" class="modal fade">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h6 class="modal-title">Create Voucher</h6>
                    </div>

                    <div class="modal-body">
                        <?php echo form_open(base_url('admin/accounts/add_voucher'),array('class'=>"form-horizontal",'id'=>'form')); ?>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Debit Account <span class="text-danger">*</span></label>
                                                <select name="debit_account" class="form-control select_box" data-width="100%" required>
                                                    <?php if(!empty($accounts)):
                                                        foreach ($accounts as $account):
                                                            $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")";
                                                            ?>
                                                            <option value="<?php echo $account->A_ID; ?>"><?php echo $account->A_NAME.$client; ?></option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">Credit Account <span class="text-danger">*</span></label>
                                                <select name="credit_account" class="form-control select_box" data-width="100%" required>
                                                    <?php if(!empty($accounts)):
                                                        foreach ($accounts as $account):
                                                            $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")"; ?>
                                                            <option value="<?php echo $account->A_ID; ?>"><?php echo $account->A_NAME.$client; ?></option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Paid To <span class="text-danger">*</span></label>
                                                <input type="text" name="paid_to" class="form-control" required>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">Cheque No</label>
                                                <input type="text" name="chq_no"class="form-control">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Amount <span class="text-danger">*</span></label>
                                                <input type="number" name="amount" onkeyup="ch_amountWords.value=numberToEnglish(this.value)" class="form-control" min="0" step="any" required>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">Amount in Words</label>
                                                <input type="text" name="amountWords" id="ch_amountWords" class="form-control mb-5" readonly>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Date <span class="text-danger">*</span></label>
                                                <input type="text" name="date" class="form-control datepicker" autocomplete="off" required>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">Description</label>
                                                <textarea name="desc" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input  type="submit" class="btn btn-primary"  value="Create Voucher"/>
                        </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Add Voucher modal-->
        <!-- Edit Voucher modal -->
        <div id="edit_voucher" class="modal fade">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h6 class="modal-title">Edit Voucher</h6>
                    </div>

                    <div class="modal-body">
                        <?php echo form_open(base_url('admin/accounts/edit_voucher'),array('class'=>"form-horizontal form")); ?>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <input type="hidden" name="voucher_id" id="v_id">
                                            <input type="hidden" name="tr_id" id="tr_id">
                                            <div class="col-sm-6">
                                                <label class="control-label">Debit Account <span class="text-danger">*</span></label>
                                                <select name="debit_account" id="debit_account" class="form-control select_box" data-width="100%" required>
                                                    <?php if(!empty($accounts)):
                                                        foreach ($accounts as $account):
                                                            $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")";
                                                            ?>
                                                            <option value="<?php echo $account->A_ID; ?>"><?php echo $account->A_NAME.$client; ?></option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">Credit Account <span class="text-danger">*</span></label>
                                                <select name="credit_account" id="credit_account" class="form-control select_box" data-width="100%" required>
                                                    <?php if(!empty($accounts)):
                                                        foreach ($accounts as $account):
                                                            $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")"; ?>
                                                            <option value="<?php echo $account->A_ID; ?>"><?php echo $account->A_NAME.$client; ?></option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Paid To <span class="text-danger">*</span></label>
                                                <input type="text" name="paid_to" id="paid_to" class="form-control" required>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">Cheque No</label>
                                                <input type="text" name="chq_no" id="chq_no" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Amount <span class="text-danger">*</span></label>
                                                <input type="number" name="amount" id="v_amount" onkeyup="v_amountWords.value=numberToEnglish(this.value)" class="form-control" min="0" step="any" required>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">Amount in Words</label>
                                                <input type="text" name="amountWords" id="v_amountWords" class="form-control" readonly>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Date <span class="text-danger">*</span></label>
                                                <input type="text" name="date" id="v_date" class="form-control datepicker" autocomplete="off" required>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">Status <span class="text-danger">*</span></label>
                                                <div class="form-check">
                                                    <label class="radio-inline"><input type="radio" name="v_status" id="v_status" value="Pending">Pending</label>
                                                    <label class="radio-inline"><input type="radio" name="v_status" id="v_status1" value="Approved">Approved</label>
                                                    <label class="radio-inline"><input type="radio" name="v_status" id="v_status2" value="Cancel">Cancel</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label class="control-label">Description</label>
                                                <textarea name="desc" id="v_desc" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input  type="submit" class="btn btn-primary"  value="Edit Voucher"/>

                        </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Edit Voucher modal-->

    <section class="panel panel-default">

        <header class="panel-heading"><?= lang('general_voucher_list') ?></header>

        <div class="panel-body" id="export_table">

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive" id="customers">
                        <table class="table table-bordered data-table-list dtHorizontalExample" id="tab_customers">
                            <thead>
                            <th>Debit Account</th>
                            <th>Credit Account</th>
                            <th>Paid To</th>
                            <th>Cheque No</th>
                            <th>Date</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th>Action</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <script>
        var data_url='<?= base_url('admin/accounts/vouchers_list') ?>';
    </script>

        <!-- edit select Voucher id script -->
        <script type='text/javascript'>
            function getEditVoucher(id)
            {
                $('document').ready(function(){
                    var controlmodal=$('#edit_voucher');
                    var option={
                        "backdrop" : "static"
                    }
                    controlmodal.modal(option);
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url("admin/accounts/ajax_edit_voucher");?>'+"/"+id,
                        data:false,
                        contentType: false,
                        dataType:"json",
                        success:function(data){
                            $('#v_id').val(data['E_ID']);
                            $('#tr_id').val(data['T_ID']);
                            $('#debit_account').val(data['DEBIT_ACCOUNT']);
                            $('#debit_account').select2({});
                            $('#credit_account').val(data['CREDIT_ACCOUNT']);
                            $('#credit_account').select2({});
                            $('#paid_to').val(data['PAID_TO']);
                            $('#chq_no').val(data['CHEQUE_NO']);
                            $('#v_amount').val(data['AMOUNT']);
                            $('#v_amountWords').val(data['AMOUNT_IN_WORDS']);
                            $('#v_date').val(data['E_DATE']);
                            $('#v_desc').val(data['DESCRIPTION']);
                            if(data['E_STATUS']=='Pending'){
                                $('#v_status').prop('checked',true);
                            }else if(data['E_STATUS']=='Approved'){
                                $('#v_status1').prop('checked',true);
                            }else if(data['E_STATUS']=='Cancel'){
                                $('#v_status2').prop('checked',true);
                            }
                        },
                        error:function(data){
                            console.log(data.responseText + "not work")
                        },
                    });
                });
            }
        </script>
        <!-- edit select Voucher id script -->
