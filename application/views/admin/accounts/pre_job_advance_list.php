<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<!-- /style form css -->
<!-- Content area -->
<h1 class="header-<?= config_item('sidebar_theme'); ?>" style="margin: 0px 0px 0px 15px;"><?= lang('list_pre_job_advances') ?></h1>

<div class="content">

    <!-- Highlighting rows and columns -->
    <div class="panel panel-flat">
        <table class="table table-bordered table-hover DataTables" id="DataTables">
            <thead>
            <tr>
                <th><?= lang('created_date') ?></th>
                <th><?= lang('time') ?></th>
                <th>Payment Date</th>
                <th><?= lang('reference_no') ?></th>
                <th><?= lang('client') ?></th>
                <th>Payment Method</th>
                <th>Bank</th>
                <th>Branch</th>
                <th>Amount</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php if(!empty($advances)): ?>
                <?php foreach ($advances as $advance){
                    if($advance->expense_type == 'Job') {
                        $job_info = $this->invoice_model->check_by(array('invoices_id' => $advance->invoices_id), 'tbl_invoices');
                        $client_info = $this->invoice_model->check_by(array('client_id' => $job_info->client_id), 'tbl_client');
                        $job = $this->invoice_model->job_no_creation($advance->invoices_id);
                        $client = $client_info->name;
                    } else{
                        $job = "--";
                        $client_info = $this->invoice_model->check_by(array('client_id' => $advance->client_id), 'tbl_client');
                        $client = $client_info->name;
                    }
                    
                    $branch_info = $this->invoice_model->check_by(array('A_ID' => $advance->debit_account), 'branches');
                    if (!empty($branch_info)){
                        $bank_info = $this->invoice_model->check_by(array('B_ID' => $branch_info->B_ID), 'banks');
                    }
                ?>
                    <tr style="white-space: nowrap;">
                        <td><?php echo date('d-m-Y',strtotime($advance->created_date)); ?></td>
                        <td><?php echo date('h:i:s A',strtotime($advance->created_date)); ?></td>
                        <td><?php echo date('d-m-Y',strtotime($advance->payment_date)); ?></td>
                        <td><?= $job ?></td>
                        <td><?php echo $client; ?></td>
                        <td><?php echo $advance->payment_method ?></td>
                        <td><?php echo ($advance->payment_method == "Cash" || $advance->payment_method == "Third Party")?'--':$bank_info->B_NAME; ?></td>
                        <td><?php echo ($advance->payment_method == "Cash" || $advance->payment_method == "Third Party")?'--':$branch_info->BR_NAME; ?></td>
                        <td><?php echo number_format($advance->amount,2); ?></td>
                        <td>
                            <?= btn_edit('admin/accounts/manage_pre_job_advance/edit/'.encode($advance->ap_id)) ?>
                            <?= btn_view('admin/accounts/manage_pre_job_advance/view/'.encode($advance->ap_id)) ?>
                        </td>
                    </tr>
                <?php } ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
    <!-- /highlighting rows and columns -->
