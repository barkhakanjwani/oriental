    <h1 class="header-<?= config_item('sidebar_theme'); ?>" style="margin: 0px 0px 0px 15px;"><?= lang('balance_sheet') ?></h1>
    <div class="content">
        <!-- Highlighting rows and columns -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <?php echo form_open(base_url('admin/accounts/balance_sheet'),array('class'=>"form-horizontal", 'id'=>"form_label")); ?>
                <div class="row">
                    <div class="col-xs-2">
						<input type="text" name="start_date" class="form-control datepicker" placeholder="From Date" autocomplete="off" value="<?= !empty($start_date)?$start_date:'' ?>" required>
					</div>
					<div class="col-xs-2">
						<input type="text" name="end_date" class="form-control datepicker" placeholder="To Date" autocomplete="off" value="<?= !empty($end_date)?$end_date:'' ?>" required>
					</div>
                    <div class="col-xs-2">
                        <label class="checkbox-inline">
                            <input type="checkbox" name="opening" value="1" <?= !empty($opening_value)?'checked':'' ?>> Opening Balance
                        </label>
                    </div>
                    <div class="col-xs-2">
                        <button class="btn btn-sm btn-success"><i class="fa fa-search"></i></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <div class="panel-body">
                <div class="row" id="export_table">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th width="50%">Account Name</th>
                            <th width="15%">Account No</th>
                            <th width="30%">Balance</th>
                            <th width="5%">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $sub_types=sub_type_for_balance('Assets'); ?>
                        <?php if(!empty($sub_types)):
                             $total_opening=0; $total_debit=0; $total_credit=0;$opening_balance=0; $balance=0; ?>
                        <tr>
                            <th colspan="4">Assets</th>
                        </tr>
                         <?php  foreach ($sub_types as $sb){
                                ?>
                                <tr>
                                    <th><?= $sb->NAME; ?></th>
                                    <th><?= $sb->SUB_NO; ?></th>
                                    <?php $sub_op=0;
                                    if($opening_value==1):
                                        $op_bal_debit=opening_bal_by_sub_head($sb->SUB_HEAD_ID,"Debit")->opening_balance;
                                        $op_bal_credit=opening_bal_by_sub_head($sb->SUB_HEAD_ID,"Credit")->opening_balance;
                                        $head_debit_tr=transaction_date_by_subhead($sb->SUB_HEAD_ID,$start_date,"Debit")->amount;
                                        $head_credit_tr=transaction_date_by_subhead($sb->SUB_HEAD_ID,$start_date,"Credit")->amount;
                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                            $sub_op+=($op_bal_debit-$op_bal_credit)-$head_debit_tr+$head_credit_tr;
                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                            $sub_op+=($op_bal_debit-$op_bal_credit)+$head_debit_tr-$head_credit_tr;
                                        }
                                        $total_opening+=$sub_op;
                                       endif;
                                    $sub_head_debit=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Debit")->amount;
                                    $sub_head_credit=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Credit")->amount; ?>
                                    <th><?php $sub_head_bal=0;
                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                            $sub_head_bal+=$sub_op-$sub_head_debit+$sub_head_credit;
                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                            $sub_head_bal+=$sub_op+$sub_head_debit-$sub_head_credit;
                                        }
                                        echo number_format(abs($sub_head_bal),2);?></th>
                                    <th><?php
                                        if($sub_head_bal<0){
                                            $balance+=$sub_head_bal;
                                            echo "Cr";
                                        }elseif($sub_head_bal>0){
                                            $balance+=$sub_head_bal;
                                            echo "Dr";
                                        }
                                        ?></th>
                                </tr>
                                <?php $account_head=account_head_by_subId($sb->SUB_HEAD_ID);?>

                                <?php foreach ($account_head as $ah){ ?>

                                    <?php $accounts = account_by_head($ah->H_ID); ?>
                                    <tr>
                                        <td><?= $ah->H_NAME; ?></td>
                                        <td><?= $ah->H_NO; ?></td>
                                        <?php $head_op=0;
                                        if($opening_value==1):
                                            $head_op_debit=opening_bal_by_head($ah->H_ID,"Debit")->opening_balance;
                                            $head_op_credit=opening_bal_by_head($ah->H_ID,"Credit")->opening_balance;
                                            $Ahead_debit_tr=transaction_date_by_head($ah->H_ID,$start_date,"Debit")->amount;
                                            $Ahead_credit_tr=transaction_date_by_head($ah->H_ID,$start_date,"Credit")->amount;
                                            if($ah->H_TYPE=='Liabilities' || $ah->H_TYPE=='Equity' || $ah->H_TYPE=='Income'){
                                                $head_op+=($head_op_debit-$head_op_credit)-$Ahead_debit_tr+$Ahead_credit_tr;
                                            }elseif ($ah->H_TYPE=='Assets' || $ah->H_TYPE=='Expense'){
                                                $head_op+=($head_op_debit-$head_op_credit)+$Ahead_debit_tr-$Ahead_credit_tr;
                                            }
                                        endif;
                                        $head_debit=transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Debit")->amount;
                                        $head_credit=transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Credit")->amount; ?>
                                        <td><?php $head_bal=0;
                                            if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                $head_bal+=$head_op-$head_debit+$head_credit;
                                            }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                $head_bal+=$head_op+$head_debit-$head_credit;
                                            }
                                            echo number_format(abs($head_bal),2);?></td>
                                        <td><?php
                                            if($head_bal<0){
                                                echo "Cr";
                                            }elseif($head_bal>0){
                                                echo "Dr";
                                            }
                                            ?></td>
                                    </tr>
                                <?php }
                            } ?>
                        <tr>
                            <th colspan="2"><b style="align:center;">Total</b></th>
                            <?php $total_asset=$balance; ?>
                            <th><?= number_format(abs($total_asset),2); ?></th>
                            <th><?php if($total_asset<0){
                                    echo "Cr";
                                }elseif($total_asset>0){
                                    echo "Dr";
                                } ?></th>
                        </tr>
                        <?php endif; ?>
                        <?php $sub_types=sub_type_for_balance('Equity'); ?>
                        <?php if(!empty($sub_types)):
                            $total_opening=0; $total_debit=0; $total_credit=0;$opening_balance=0; $balance_eq=0; ?>
                        <tr>
                            <th colspan="4">Equity & Liabilities</th>
                        </tr>
                        <?php   foreach ($sub_types as $sb){
                                ?>
                                <tr>
                                    <th><?= $sb->NAME; ?></th>
                                    <th><?= $sb->SUB_NO; ?></th>
                                    <?php $sub_op=0;
                                    if($opening_value==1):
                                        $op_bal_debit=opening_bal_by_sub_head($sb->SUB_HEAD_ID,"Debit")->opening_balance;
                                        $op_bal_credit=opening_bal_by_sub_head($sb->SUB_HEAD_ID,"Credit")->opening_balance;
                                        $head_debit_tr=transaction_date_by_subhead($sb->SUB_HEAD_ID,$start_date,"Debit")->amount;
                                        $head_credit_tr=transaction_date_by_subhead($sb->SUB_HEAD_ID,$start_date,"Credit")->amount;
                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                            $sub_op+=($op_bal_debit-$op_bal_credit)-$head_debit_tr+$head_credit_tr;
                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                            $sub_op+=($op_bal_debit-$op_bal_credit)+$head_debit_tr-$head_credit_tr;
                                        }
                                        $total_opening+=$sub_op;
                                    endif;
                                    $sub_head_debit=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Debit")->amount;
                                    $sub_head_credit=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Credit")->amount; ?>
                                    <th><?php $sub_head_bal=0;
                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                            $sub_head_bal+=$sub_op-$sub_head_debit+$sub_head_credit;
                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                            $sub_head_bal+=$sub_op+$sub_head_debit-$sub_head_credit;
                                        }
                                        echo number_format(abs($sub_head_bal),2);?></th>
                                    <th><?php
                                        if($sub_head_bal<0){
                                            $balance_eq+=$sub_head_bal;
                                            echo "Cr";
                                        }elseif($sub_head_bal>0){
                                            $balance_eq+=$sub_head_bal;
                                            echo "Dr";
                                        }
                                        ?></th>
                                </tr>
                                <?php $account_head=account_head_by_subId($sb->SUB_HEAD_ID);?>
                                <?php foreach ($account_head as $ah){ ?>

                                    <?php $accounts = account_by_head($ah->H_ID); ?>
                                    <tr>
                                        <td><?= $ah->H_NAME; ?></td>
                                        <td><?= $ah->H_NO; ?></td>
                                        <?php $head_op=0;
                                        if($opening_value==1):
                                            $head_op_debit=opening_bal_by_head($ah->H_ID,"Debit")->opening_balance;
                                            $head_op_credit=opening_bal_by_head($ah->H_ID,"Credit")->opening_balance;
                                            $Ahead_debit_tr=transaction_date_by_head($ah->H_ID,$start_date,"Debit")->amount;
                                            $Ahead_credit_tr=transaction_date_by_head($ah->H_ID,$start_date,"Credit")->amount;
                                            if($ah->H_TYPE=='Liabilities' || $ah->H_TYPE=='Equity' || $ah->H_TYPE=='Income'){
                                                $head_op+=($head_op_debit-$head_op_credit)-$Ahead_debit_tr+$Ahead_credit_tr;
                                            }elseif ($ah->H_TYPE=='Assets' || $ah->H_TYPE=='Expense'){
                                                $head_op+=($head_op_debit-$head_op_credit)+$Ahead_debit_tr-$Ahead_credit_tr;
                                            }
                                        endif;
                                        $head_debit=transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Debit")->amount;
                                        $head_credit=transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Credit")->amount; ?>
                                        <td><?php $head_bal=0;
                                            if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                $head_bal+=$head_op-$head_debit+$head_credit;
                                            }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                $head_bal+=$head_op+$head_debit-$head_credit;
                                            }
                                            echo number_format(abs($head_bal),2);?></td>
                                        <td><?php
                                            if($head_bal<0){
                                                echo "Cr";
                                            }elseif($head_bal>0){
                                                echo "Dr";
                                            }
                                            ?></td>
                                    </tr>
                                <?php }
                            } ?>
                        <?php endif; ?>
                        <tr>
                            <?php
                            $sb=sub_type_by_id(16);
                            $ac_balance=0;
                            $db_transactions=transactions_by_sub_type(16,$start_date,$end_date,"Debit");
                            $cr_transactions=transactions_by_sub_type(16,$start_date,$end_date,"Credit");
                            if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                $ac_balance+=$db_transactions->amount+$cr_transactions->amount;
                            }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                $ac_balance+=$db_transactions->amount-$cr_transactions->amount;
                            }
                            $sales=$ac_balance;

                            $sb=sub_type_by_id(17);
                            $ac_balance=0;
                            $db_transactions=transactions_by_sub_type(17,$start_date,$end_date,"Debit");
                            $cr_transactions=transactions_by_sub_type(17,$start_date,$end_date,"Credit");
                            if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                $ac_balance+=$db_transactions->amount+$cr_transactions->amount;
                            }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                $ac_balance+=$db_transactions->amount-$cr_transactions->amount;
                            }
                            $cost_of_sales=$ac_balance;

                            $gross_profit=$sales-$cost_of_sales;

                            $sb=sub_type_by_id(8);
                            $ac_balance=0;
                            $db_transactions=transactions_by_sub_type(8,$start_date,$end_date,"Debit");
                            $cr_transactions=transactions_by_sub_type(8,$start_date,$end_date,"Credit");
                            if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                $ac_balance+=$db_transactions->amount+$cr_transactions->amount;
                            }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                $ac_balance+=$db_transactions->amount-$cr_transactions->amount;
                            }
                            $other_income=$ac_balance;

                            $sb=sub_type_by_id(10);

                            $ac_balance=0;
                            $db_transactions=transactions_by_sub_type(10,$start_date,$end_date,"Debit");
                            $cr_transactions=transactions_by_sub_type(10,$start_date,$end_date,"Credit");
                            if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                $ac_balance+=$db_transactions->amount+$cr_transactions->amount;
                            }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                $ac_balance+=$db_transactions->amount-$cr_transactions->amount;
                            }
                            $op_expense=$ac_balance;

                            $sb=sub_type_by_id(14);

                            $ac_balance=0;
                            $db_transactions=transactions_by_sub_type(14,$start_date,$end_date,"Debit");
                            $cr_transactions=transactions_by_sub_type(14,$start_date,$end_date,"Credit");
                            if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                $ac_balance+=$db_transactions->amount+$cr_transactions->amount;
                            }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                $ac_balance+=$db_transactions->amount-$cr_transactions->amount;
                            }
                            $other_expense=$ac_balance;

                            $op_profit=$gross_profit+$other_income-$op_expense-$other_expense;

                            $sb=sub_type_by_id(11);

                            $ac_balance=0;
                            $db_transactions=transactions_by_sub_type(11,$start_date,$end_date,"Debit");
                            $cr_transactions=transactions_by_sub_type(11,$start_date,$end_date,"Credit");
                            if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                $ac_balance+=$db_transactions->amount+$cr_transactions->amount;
                            }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                $ac_balance+=$db_transactions->amount-$cr_transactions->amount;
                            }
                            $finance_cost=$ac_balance;

                            $profit_before_tax=$op_profit-$finance_cost;

                            $sb=sub_type_by_id(12);

                            $ac_balance=0;
                            $db_transactions=transactions_by_sub_type(12,$start_date,$end_date,"Debit");
                            $cr_transactions=transactions_by_sub_type(12,$start_date,$end_date,"Credit");
                            if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                $ac_balance+=$db_transactions->amount+$cr_transactions->amount;
                            }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                $ac_balance+=$db_transactions->amount-$cr_transactions->amount;
                            }
                            $income_tax=$ac_balance;

                            $net_profit=$profit_before_tax-$income_tax;
                            $balance_eq+=$net_profit;
                            ?>
                            <th>Profit & Loss</th>
                            <th></th>
                            <th><?= number_format(abs($net_profit),2) ?></th>
                            <th><?php if($net_profit<0){
                                    echo "Cr";
                                }elseif($net_profit>0){
                                    echo "Dr";
                                } ?></th>
                        </tr>
                        <?php $sub_types=sub_type_for_balance('Liabilities'); ?>
                        <?php if(!empty($sub_types)):
                            foreach ($sub_types as $sb){
                                ?>
                                <tr>
                                    <th><?= $sb->NAME; ?></th>
                                    <th><?= $sb->SUB_NO; ?></th>
                                    <?php $sub_op=0;
                                    if($opening_value==1):
                                        $op_bal_debit=opening_bal_by_sub_head($sb->SUB_HEAD_ID,"Debit")->opening_balance;
                                        $op_bal_credit=opening_bal_by_sub_head($sb->SUB_HEAD_ID,"Credit")->opening_balance;
                                        $head_debit_tr=transaction_date_by_subhead($sb->SUB_HEAD_ID,$start_date,"Debit")->amount;
                                        $head_credit_tr=transaction_date_by_subhead($sb->SUB_HEAD_ID,$start_date,"Credit")->amount;
                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                            $sub_op+=($op_bal_debit-$op_bal_credit)-$head_debit_tr+$head_credit_tr;
                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                            $sub_op+=($op_bal_debit-$op_bal_credit)+$head_debit_tr-$head_credit_tr;
                                        }
                                        $total_opening+=$sub_op;
                                    endif;
                                    $sub_head_debit=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Debit")->amount;
                                    $sub_head_credit=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Credit")->amount; ?>
                                    <th><?php $sub_head_bal=0;
                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                            $sub_head_bal+=$sub_op-$sub_head_debit+$sub_head_credit;
                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                            $sub_head_bal+=$sub_op+$sub_head_debit-$sub_head_credit;
                                        }
                                        echo number_format(abs($sub_head_bal),2);?></th>
                                    <th><?php
                                        if($sub_head_bal<0){
                                            $balance_eq+=$sub_head_bal;
                                            echo "Cr";
                                        }elseif($sub_head_bal>0){
                                            $balance_eq+=$sub_head_bal;
                                            echo "Dr";
                                        }
                                        ?></th>
                                </tr>
                                <?php $account_head=account_head_by_subId($sb->SUB_HEAD_ID);?>

                                <?php foreach ($account_head as $ah){ ?>

                                    <?php $accounts = account_by_head($ah->H_ID); ?>
                                    <tr>
                                        <td><?= $ah->H_NAME; ?></td>
                                        <td><?= $ah->H_NO; ?></td>
                                        <?php $head_op=0;
                                        if($opening_value==1):
                                            $head_op_debit=opening_bal_by_head($ah->H_ID,"Debit")->opening_balance;
                                            $head_op_credit=opening_bal_by_head($ah->H_ID,"Credit")->opening_balance;
                                            $Ahead_debit_tr=transaction_date_by_head($ah->H_ID,$start_date,"Debit")->amount;
                                            $Ahead_credit_tr=transaction_date_by_head($ah->H_ID,$start_date,"Credit")->amount;
                                            if($ah->H_TYPE=='Liabilities' || $ah->H_TYPE=='Equity' || $ah->H_TYPE=='Income'){
                                                $head_op+=($head_op_debit-$head_op_credit)-$Ahead_debit_tr+$Ahead_credit_tr;
                                            }elseif ($ah->H_TYPE=='Assets' || $ah->H_TYPE=='Expense'){
                                                $head_op+=($head_op_debit-$head_op_credit)+$Ahead_debit_tr-$Ahead_credit_tr;
                                            }
                                        endif;
                                        $head_debit=transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Debit")->amount;
                                        $head_credit=transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Credit")->amount; ?>
                                        <td><?php $head_bal=0;
                                            if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                $head_bal+=$head_op-$head_debit+$head_credit;
                                            }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                $head_bal+=$head_op+$head_debit-$head_credit;
                                            }
                                            echo number_format(abs($head_bal),2);?></td>
                                        <td><?php
                                            if($head_bal<0){
                                                echo "Cr";
                                            }elseif($head_bal>0){
                                                echo "Dr";
                                            }
                                            ?></td>
                                    </tr>
                                <?php }
                            } ?>
                            <tr>
                                <th colspan="2">Total</th>
                                <?php $total_eq_li=$balance_eq; ?>
                                <th><?= number_format(abs($total_eq_li),2); ?></th>
                                <th><?php if($total_eq_li<0){
                                        echo "Cr";
                                    }elseif($total_eq_li>0){
                                        echo "Dr";
                                    } ?></th>
                            </tr>
                        <?php endif; ?>
                            <tr>
                                <th colspan="2">Difference</th>
                                <th><?= number_format(abs($total_asset-$total_eq_li),2) ?></th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <button class="btn btn-sm pull-right" style="color: #fff;background-color: #26a69a" id="btnExport">Export to Excel</button>
            </div>
        </div>
        <!-- /highlighting rows and columns -->


        <!-- Export to Excel-->
        <script>
            $("#btnExport").click(function (e) {
                window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('#export_table').html() ));
                e.preventDefault();
            });
        </script>
        <!-- /Export to Excel-->

        <!--Jquery Datepicker-->
        <script>
            $( function() {
                $( ".datepicker" ).datepicker({dateFormat: 'dd-mm-yy'});
            } );
        </script>
        <!--Jquery Datepicker-->