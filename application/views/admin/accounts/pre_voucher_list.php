<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<!-- /style form css -->
<!-- Content area -->
<div class="content">

    <!-- Highlighting rows and columns -->
    <div class="panel panel-flat">
        <table class="table table-bordered table-hover DataTables" id="DataTables">
            <thead>
            <tr>
                <th><?= lang('created_date') ?></th>
                <th><?= lang('payment_date') ?></th>
                <th><?= lang('payment_method') ?></th>
                <th><?= lang('paid_to') ?></th>
                <th><?= lang('cheque_payorder') ?></th>
                <th><?= lang('status') ?></th>
                <th class="text-center"><?= lang('action') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php if(!empty($vouchers)): ?>
                <?php foreach ($vouchers as $voucher){
                ?>
                    <tr style="white-space: nowrap;">
                        <td><?php echo date('d-m-Y',strtotime($voucher->created_date)); ?></td>
                        <td><?php echo date('d-m-Y',strtotime($voucher->payment_date)); ?></td>
                        <td><?php echo ($voucher->payment_method == 1)?'Cash':'Bank'; ?></td>
                        <td><?php echo $voucher->paid_to; ?></td>
                        <td><?php echo ($voucher->payment_method == 2)?$voucher->cheque_no:'-'; ?></td>
                        <td><?php echo ($voucher->status == 1)?'<label class="label label-success">Approved</label>':'<label class="label label-danger">Reject</label>'; ?></td>
                        <td class="text-center">
                            <?= btn_edit('admin/accounts/manage_pre_voucher/edit_voucher/'.$voucher->voucher_id) ?>
                            <?= btn_view('admin/accounts/manage_pre_voucher/voucher_detail/'.$voucher->voucher_id) ?>
                        </td>
                    </tr>
                <?php } ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
    <!-- /highlighting rows and columns -->
