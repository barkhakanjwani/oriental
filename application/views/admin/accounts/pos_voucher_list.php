<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<!-- /style form css -->
<!-- Content area -->
<div class="content">

    <!-- Highlighting rows and columns -->
    <div class="panel panel-flat">
        <table class="table table-bordered table-hover DataTables" id="DataTables">
            <thead>
            <tr>
                <th><?= lang('reference_no') ?></th>
                <th><?= lang('client') ?></th>
                <th>Debit Account</th>
                <th>Credit Account</th>
                <th>Paid To</th>
                <th>Cheque No</th>
                <th>Date</th>
                <th>Amount</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php if(!empty($vouchers)): ?>
                <?php foreach ($vouchers as $voucher){
                    $client_info = $this->invoice_model->check_by(array('client_id'=>$voucher->client_id), 'tbl_client');
                ?>
                    <tr style="white-space: nowrap;">
                        <td><?php echo $voucher->reference_no; ?></td>
                        <td><?php echo $client_info->name; ?></td>
                        <td><?php echo $voucher->DEBIT_NAME; ?></td>
                        <td><?php echo $voucher->CREDIT_NAME; ?></td>
                        <td><?php echo $voucher->PAID_TO; ?></td>
                        <td><?php echo $voucher->CHEQUE_NO; ?></td>
                        <td><?php echo date('d-m-Y',strtotime($voucher->E_DATE)); ?></td>
                        <td><?php echo number_format($voucher->AMOUNT,2); ?></td>
                        <td><label class="label label-success"><?php echo $voucher->E_STATUS; ?></label></td>
                        <td>
                            <?= btn_edit('admin/accounts/manage_pos_voucher/edit_voucher/'.$voucher->E_ID) ?>
                            <?= btn_view('admin/accounts/manage_pos_voucher/voucher_detail/'.$voucher->E_ID) ?>
                        </td>
                    </tr>
                <?php } ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
    <!-- /highlighting rows and columns -->
