<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<section class="content-header">
    <div class="row">
        <div class="col-md-12">
            <a href="#" data-toggle="modal" data-target="#add_account" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Add Account</a>
        </div>
    </div>
</section>
<!-- Content area -->
<div class="content">
    <!-- Highlighting rows and columns -->
    <div class="panel panel-flat">
        <table class="table table-bordered table-hover DataTables " id="DataTables">
            <thead>
            <tr>
                <th>Type - Sub Type</th>
                <th>Account Name</th>
                <th>Account No</th>
                <th>Status</th>
                <th class="text-center">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php if(!empty($accountsHead)): ?>
                <?php $prev_val=$accountsHead[0]->H_ID; ?>
                <?php foreach ($accountsHead as $ah){ ?>
                    <tr style="white-space: nowrap;">
                        <td><?php echo $ah->H_TYPE." - ".$ah->NAME; ?></td>
                        <td><?php echo $ah->H_NAME; ?></td>
                        <td><?php echo $ah->H_NO; ?></td>
                        <td><?php echo ($ah->H_STATUS==1)?"Active":"Inactive"; ?></td>
                        <td><?php echo ""; ?></td>
                    </tr>
                    <?php
                    $accounts = account_by_head($ah->H_ID);
                    /*$accounts = $this->invoice_model->check_by_all(array('CLIENT_ID'=>NULL), 'accounts');*/

                    ?>
                    <?php foreach ($accounts as $account){ ?>
                        <?php if($account->A_SELFHEAD_ID==0){ ?>
                            <tr style="white-space: nowrap;">
                                <td><?php echo $ah->H_TYPE." - ".$ah->NAME; ?></td>
                                <td><?php echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp".$account->A_NAME; ?></td>
                                <td><?php echo $account->A_NO; ?></td>
                                <td><?php echo ($account->A_STATUS==1)?"Active":"Inactive"; ?></td>
                                <td class="text-center">
                                    <button type="button" onclick="getEditAccount(this.value)" value="<?php echo $account->A_ID;?>" data-popup="tooltip" title="" data-container="body" data-original-title="Edit Account" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#" ><i class="fa fa-edit"></i></button>
                                    <!--<button type="button" value="<?php /*echo $account->A_ID;*/?>" data-popup="tooltip" title="" data-container="body" data-original-title="Delete Account" class="btn btn-danger btn-xs delete"><i class="fa fa-trash"></i></button>-->
                                </td>
                            </tr>
                            <?php foreach ($accounts as $acc){ ?>
                                <?php if($acc->A_SELFHEAD_ID==$account->A_ID){
                                    ?>
                                    <tr style="white-space: nowrap;">
                                        <td><?php echo $ah->H_TYPE." - ".$ah->NAME; ?></td>
                                        <td><?php echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp".$acc->A_NAME ?></td>
                                        <td><?php echo $acc->A_NO; ?></td>
                                        <td><?php echo ($acc->A_STATUS==1)?"Active":"Inactive"; ?></td>
                                        <td class="text-center">
                                            <button type="button" onclick="getEditAccount(this.value)" value="<?php echo $acc->A_ID;?>" data-popup="tooltip" title="" data-container="body" data-original-title="Edit Account" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#" ><i class="fa fa-edit"></i></button>
                                            <!--<button type="button" value="<?php /*echo $acc->A_ID;*/?>" data-popup="tooltip" title="" data-container="body" data-original-title="Delete Account" class="btn btn-danger btn-xs delete"><i class="fa fa-trash"></i></button>-->
                                        </td>
                                    </tr>
                                    <?php foreach ($accounts as $ac){ ?>
                                        <?php if($ac->A_SELFHEAD_ID==$acc->A_ID){
                                            ?>
                                            <tr style="white-space: nowrap;">
                                                <td><?php echo $ah->H_TYPE." - ".$ah->NAME; ?></td>
                                                <td><?php echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp".$ac->A_NAME; ?></td>
                                                <td><?php echo $ac->A_NO; ?></td>
                                                <td><?php echo ($ac->A_STATUS==1)?"Active":"Inactive"; ?></td>
                                                <td class="text-center">
                                                    <button type="button" onclick="getEditAccount(this.value)" value="<?php echo $ac->A_ID;?>" data-popup="tooltip" title="" data-container="body" data-original-title="Edit Account" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#" ><i class="fa fa-edit"></i></button>
                                                    <!--<button type="button" value="<?php /*echo $ac->A_ID;*/?>" data-popup="tooltip" title="" data-container="body" data-original-title="Delete Account" class="btn btn-danger btn-xs delete"><i class="fa fa-trash"></i></button>-->
                                                </td>
                                            </tr>
                                            <?php foreach ($accounts as $act){ ?>
                                                <?php if($act->A_SELFHEAD_ID==$ac->A_ID){
                                                    ?>
                                                    <tr style="white-space: nowrap;">
                                                        <td><?php echo $ah->H_TYPE." - ".$ah->NAME; ?></td>
                                                        <td><?php echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp".$act->A_NAME; ?></td>
                                                        <td><?php echo $act->A_NO; ?></td>
                                                        <td><?php echo ($act->A_STATUS==1)?"Active":"Inactive"; ?></td>
                                                        <td class="text-center">
                                                            <button type="button" onclick="getEditAccount(this.value)" value="<?php echo $act->A_ID;?>" data-popup="tooltip" title="" data-container="body" data-original-title="Edit Account" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#" ><i class="fa fa-edit"></i></button>
                                                            <!--<button type="button" value="<?php /*echo $act->A_ID;*/?>" data-popup="tooltip" title="" data-container="body" data-original-title="Delete Account" class="btn btn-danger btn-xs delete"><i class="fa fa-trash"></i></button>-->
                                                        </td>
                                                    </tr>
                                                    <?php foreach ($accounts as $accn){ ?>
                                                        <?php if($accn->A_SELFHEAD_ID==$act->A_ID){
                                                            ?>
                                                            <tr style="white-space: nowrap;">
                                                                <td><?php echo $ah->H_TYPE." - ".$ah->NAME; ?></td>
                                                                <td><?php echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp".$accn->A_NAME; ?></td>
                                                                <td><?php echo $accn->A_NO; ?></td>
                                                                <td><?php echo ($accn->A_STATUS==1)?"Active":"Inactive"; ?></td>
                                                                <td class="text-center">
                                                                    <button type="button" onclick="getEditAccount(this.value)" value="<?php echo $accn->A_ID;?>" data-popup="tooltip" title="" data-container="body" data-original-title="Edit Account" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#" ><i class="fa fa-edit"></i></button>
                                                                    <!--<button type="button" value="<?php /*echo $accn->A_ID;*/?>" data-popup="tooltip" title="" data-container="body" data-original-title="Delete Account" class="btn btn-danger btn-xs delete"><i class="fa fa-trash"></i></button>-->
                                                                </td>
                                                            </tr>
                                                        <?php }
                                                    } ?>
                                                <?php }
                                            } ?>
                                        <?php }
                                    } ?>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
    <!-- /highlighting rows and columns -->
    <!-- Add Account modal -->
    <div id="add_account" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h6 class="modal-title">Add Account</h6>
                </div>
                <div class="modal-body">
                    <?php echo form_open(base_url('admin/accounts/add_account'),array('class'=>"form-horizontal",'id'=>'ad_account2')); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id='salessuccess' class='alert alert-info' style='display:none;'></div>
                                    <?php if($this->session->flashdata('salessuccess')){ ?>
                                        <div class="alert bg-info alert-rounded" style='text-align:center;'><?php echo $this->session->flashdata('salessuccess'); ?></div>
                                    <?php } ?>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Type</label>
                                            <select name="h_type" id="ha_type" onchange="ReadSubType(this.value)" class="form-control required mb-5">
                                                <option value="Assets">Assets</option>
                                                <option value="Liabilities">Liabilities</option>
                                                <option value="Income">Income</option>
                                                <option value="Expense">Expense</option>
                                                <option value="Equity">Equity</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Sub Type</label>
                                            <select name="sub_type" class="form-control required mb-5" id="subType1" onchange="selectaccountsHead(this.value)">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Account Head</label>
                                            <select name="a_head" id="ac_head" onchange="selectSubHead(this.value)" class="form-control required mb-5">
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Sub Head</label>
                                            <select name="a_subHead" id="ac_subHead" class="form-control mb-5">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Name</label>
                                            <input type="text" name="a_name" placeholder="Name" class="form-control required mb-5">
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Opening Type</label>
                                            <div class="form-check mb-5">
                                                <label class="radio-inline"><input type="radio" name="a_openingType" value="Debit">Debit</label>
                                                <label class="radio-inline"><input type="radio" name="a_openingType" value="Credit">Credit</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Opening Balance</label>
                                            <input type="number" name="a_openingBalance" min="0" step="any" class="form-control mb-5">
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Status</label>
                                            <Select name="a_status" class="form-control required">
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Description</label>
                                            <textarea name="a_desc" class="form-control mb-5"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input  type="submit" name="btn_addAccount" class="btn btn-primary"  value="Add Account"/>
                    </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
    <!-- /Add Account -->
    <!-- style form css -->
    <style>
        .redborder1{
            border:1px solid red;
        }
        .redborder2{
            border:1px solid red;
        }
    </style>
    <!-- /style form css -->
    <!-- add Account script -->
    <!--<script type='text/javascript'>

            var ad_account = $('#ad_account');
            ad_account.on('submit',function(e){
                e.preventDefault();

                var new_account_add =$(this).serialize();
                console.log(new_account_add);
                var assetserror = 0;
                $('.required').each(function(){
                    if($(this).val() == ''){$(this).addClass('redborder1');assetserror = assetserror + 1;}else{$(this).removeClass('redborder1');}
                });

                if(assetserror > 0){return false;}else{

                    $.ajax({
                        type:'POST',
                        url:'<?php /*echo base_url('admin/accounts/add_account'); */?>',
                        data:new_account_add,
                        dataType:'JSON',
                        success:function(data){
                            console.log('first'+data);
                            if(data.status=='true'){
                                $('#salessuccess').show().html('Account added Successfully').hide(3000);
                                ad_account[0].reset();
                                setTimeout(function(){location.href="<?php /*echo base_url('admin/accounts/'); */?>"} , 3000);
                            }else{
                                console.log('third'+data);
                            }
                        },
                        error:function(data){console.log('error: '+data.responseText);}
                    });
                }
            });
        </script>-->
    <!-- /add Account script -->

    <!-- Edit Account modal -->
    <div id="edit_account" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h6 class="modal-title">Edit Account</h6>
                </div>

                <div class="modal-body">
                    <?php echo form_open(base_url('admin/accounts/edit_account'),array('class'=>"form-horizontal",'id'=>'ed_account2')); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <!--<div id='salessuccess' class='alert alert-info' style='display:none;'></div>
                                        <?php /*if($this->session->flashdata('salessuccess')){ */?>
                                            <div class="alert bg-info alert-rounded" style='text-align:center;'><?php /*echo $this->session->flashdata('salessuccess'); */?></div>
                                        --><?php /*} */?>
                                    <input type="hidden" name="a_id" id="a_id">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Type</label>
                                            <select name="h_type" id="h_type" onchange="ReadSubType(this.value)" class="form-control require mb-5">
                                                <option value="Assets">Assets</option>
                                                <option value="Liabilities">Liabilities</option>
                                                <option value="Income">Income</option>
                                                <option value="Expense">Expense</option>
                                                <option value="Equity">Equity</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Sub Type</label>
                                            <select name="sub_type" class="form-control required mb-5" id="subType2" onchange="selectaccountsHead(this.value)">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Account Head</label>
                                            <select name="a_head" id="a_head" onchange="selectSubHead(this.value)" class="form-control require mb-5">
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Sub Head</label>
                                            <select name="a_subHead" id="a_subHead" class="form-control mb-5">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Name</label>
                                            <input type="text" name="a_name" id="a_name" placeholder="Name" class="form-control require mb-5">
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Opening Type</label>
                                            <div class="form-check mb-5">
                                                <label class="radio-inline"><input type="radio" name="a_openingType" id="a_openingType1" value="Debit">Debit</label>
                                                <label class="radio-inline"><input type="radio" name="a_openingType" id="a_openingType2" value="Credit">Credit</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Opening Balance</label>
                                            <input type="number" name="a_openingBalance" id="op_balance" min="0" step="any" class="form-control mb-5">
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Status</label>
                                            <Select name="a_status" id="a_status" class="form-control require">
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Description</label>
                                            <textarea name="a_desc" id="a_desc" class="form-control mb-5"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input  type="submit" name="btn_editAccount" class="btn btn-primary"  value="Edit Account"/>

                    </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
    <!-- /Edit Account Modal -->
    <!-- edit select Account id script -->
    <script type='text/javascript'>
        function getEditAccount(id)
        {
            $('document').ready(function(){
                var controlmodal=$('#edit_account');
                var option={
                    "backdrop" : "static"
                }
                controlmodal.modal(option);
                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url("admin/accounts/ajax_edit_account");?>'+"/"+id,
                    data:false,
                    contentType: false,
                    dataType:"json",
                    success:function(data){
                        console.log(data.account);
                        console.log(data.head);
                        fillDropDowns(data);
                        $('#a_id').val(data.account['A_ID']);
                        $('#h_type').val(data.account['H_TYPE']);
                        $('#subType2').val(data.account['SUB_HEAD_ID']);
                        $('#a_head').val(data.account['H_ID']);
                        $('#a_subHead').val(data.account['A_SELFHEAD_ID']);
                        $('#a_name').val(data.account['A_NAME']);
                        $('#a_desc').val(data.account['A_DESC']);
                        if(data.account['A_OPENINGTYPE']=='Debit'){
                            $('#a_openingType1').attr('checked',true);
                        }else if(data.account['A_OPENINGTYPE']=='Credit'){
                            $('#a_openingType2').attr('checked',true);
                        }
                        $('#op_balance').val(data.account['A_OPENINGBALANCE']);
                        $('#a_status').val(data.account['A_STATUS']);
                    },
                    error:function(data){
                        console.log(data.responseText + "not work")
                    }
                });
            });
        }
        function fillDropDowns(data) {
            $('#subType2').empty();
            $.each(data.subType, function (index, value) {
                $('#subType2').append('<option value="' + value.SUB_HEAD_ID + '">' + value.NAME + '</option>');
            });
            $('#a_head').empty();
            $.each(data.head, function (index, value) {
                $('#a_head').append('<option value="' + value.H_ID + '">' + value.H_NAME + '</option>');
            });
            $('#a_subHead').empty();
            $('#a_subHead').append('<option value="' + 0 + '">----</option>');
            $.each(data.subHead, function (index, value) {
                $('#a_subHead').append('<option value="' + value.A_ID + '">' + value.A_NAME + '</option>');
            });
        }
    </script>
    <!-- edit select Account id script -->

    <!-- Edit Account script -->
    <!--<script type='text/javascript'>

            var edit_account = $('#ed_account');
            edit_account.on('submit',function(e){
                e.preventDefault();
                var new_account_edit = $(this).serialize();
                console.log(new_account_edit);
                var assetserror = 0;
                $('.require').each(function(){
                    if($(this).val() == ''){$(this).addClass('redborder2');assetserror = assetserror + 1;}else{$(this).removeClass('redborder2');}
                });
                if(assetserror > 0){return false;}else{

                    $.ajax({
                        type:'POST',
                        url:'<?php /*echo base_url('admin/accounts/edit_account'); */?>',
                        data:new_account_edit,
                        dataType:'JSON',
                        success:function(data){
                            console.log('first'+data);
                            if(data.status=='true'){
                                console.log('second'+data);
                                $('#edit_account').modal('hide');
                                setTimeout(function(){location.href="<?php /*echo base_url('admin/accounts/'); */?>"} , 2000);
                                // Success notification
                                $.jGrowl('Account has been Updated', {
                                    header: 'Successfully',
                                    theme: 'bg-success alert-styled-left alert-styled-check'
                                });
                            }else{
                                console.log('third'+data);
                            }
                        },
                        error:function(data){console.log('error: '+data.responseText);}
                    });
                }
            });
        </script>-->
    <!-- /Edit Account script -->

    <!--Delete Account Script-->
    <script type="text/javascript">
        $('.delete').click(function () {
            if(confirm("Are you sure you want to delete this Account?")){
                $.ajax({
                    type:"POST",
                    url:"<?php echo base_url('admin/accounts/delete_account'); ?>"+"/"+$(this).val(),
                    dataType:'JSON',
                    success:function(data){
                        if(data==true){
                            setTimeout(function(){location.href="<?php echo base_url('admin/accounts/'); ?>"} , 1000);
                            $.jGrowl('Account has been Deleted', {
                                header: 'Successfully',
                                theme: 'bg-danger alert-styled-left alert-styled-check'
                            });
                        }
                    }
                });
            }else {
                return false;
            }
        });
    </script>
    <!--/Delete Account Script-->

    <!-- Fill dropdown on another basis -->
    <script type="text/javascript">
        $(document).ready(function () {
            ReadSubType($('#ha_type').val());
        });
        /*Select Sub Type on Type Basis*/
        function ReadSubType(val) {
            var option="";
            if($('#a_id').val()!='' && $('#edit_account').is(':visible')){
                option=$('#subType2');
            }else {
                option=$('#subType1');
            }
            $.getJSON( "<?php echo site_url('admin/accounts/select_sub_head') ?>"+"/"+val, function(result) {
                console.log(result);
                option.empty();
                $.each(result, function (index, value) {
                    option.append('<option value="' + value.SUB_HEAD_ID + '">' + value.NAME + '</option>');
                });
                selectaccountsHead(option.val());
            });
        }
        /*Select Account Head on Sub Type basis*/
        function selectaccountsHead(val){
            var option="";
            if($('#a_id').val()!='' && $('#edit_account').is(':visible')){
                option=$('#a_head');
            }else {
                option=$('#ac_head');
            }
            $.getJSON( "<?php echo site_url('admin/accounts/ajax_select_head') ?>"+"/"+val, function(result) {
                option.empty();
                $.each(result, function (index, value) {
                    option.append('<option value="' + value.H_ID + '">' + value.H_NAME + '</option>');
                });
                selectSubHead(option.val());
            });
        }

        /*Select Sub Head on Account Head Basis*/
        function selectSubHead(val) {
            var option="";
            if($('#a_id').val()!='' && $('#edit_account').is(':visible')){
                option=$('#a_subHead');
            }else {
                option=$('#ac_subHead');
            }
            $.getJSON( "<?php echo site_url('admin/accounts/ajax_select_accounts') ?>"+"/"+val, function(result) {
                option.empty();
                option.append('<option value="' + 0 + '">----</option>');
                $.each(result, function (index, value) {
                    option.append('<option value="' + value.A_ID + '">' + value.A_NAME + '</option>');
                });
            });
        }
    </script>
    <!-- /Fill dropdown on another basis -->