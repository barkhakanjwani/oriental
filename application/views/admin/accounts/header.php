<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Cheque System | Home </title>
	<link rel="icon" type="image/png" href="<?php echo base_url();?>assets/images/icon.png">
	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('');?>assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('');?>assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('');?>assets/css/extras/animate.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('');?>assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('');?>assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('');?>assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('');?>assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->
	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/loaders/blockui.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/uploaders/fileinput.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/pages/uploader_bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/velocity/velocity.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/velocity/velocity.ui.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/buttons/spin.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/buttons/ladda.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/notifications/pnotify.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/notifications/noty.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/notifications/jgrowl.min.js"></script>
	<!--<script type="text/javascript" src="<?php //echo base_url('');?>assets/js/pages/extension_velocity_examples.js"></script>-->
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/ui/prism.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/forms/tags/tagsinput.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/forms/tags/tokenfield.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/pages/components_buttons.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/pages/components_notifications_other.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/pages/myFunctions.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/core/libraries/jquery_ui/core.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/forms/inputs/touchspin.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<!--<script type="text/javascript" src="assets/js/pages/datatables_advanced.js"></script>-->
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/pages/datatables_basic.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/pages/form_input_groups.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/forms/selects/selectboxit.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
	<!--<script type="text/javascript" src="assets/js/pages/colors_primary.js"></script>-->
<!-- /theme JS files -->

	<!-- Javascript for High Charts -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/charts/high/highcharts.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/charts/high/highcharts-3d.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/charts/high/exporting.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/charts/high/drilldown.js'); ?>"></script>
	<!-- /Javascript for High Charts -->
	<script type="text/javascript" src="<?php echo base_url('');?>assets/js/core/app.js"></script>
</head>
