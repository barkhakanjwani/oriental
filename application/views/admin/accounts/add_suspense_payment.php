<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('manage_receipt_voucher') ?></h1>
<div class="row">
    <div class="col-lg-12">
        <form role="form" enctype="multipart/form-data" id="form" action="<?php echo base_url(); ?>admin/accounts/save_suspense_voucher/<?= (!empty($advance_info))?$advance_info->ap_id:'' ?>" method="post" class="form-horizontal  ">
            <section class="panel panel-default">
                <header class="panel-heading"><?= $title ?></header>
                <div class="panel-body">
                    <div id="others_receipt">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= lang('received_from') ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                   <?php
                                     $ap_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Assets', 'NAME' => 'Current Assets'), 'sub_heads');
                                     $ap_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_head->SUB_HEAD_ID, 'H_NAME' => 'Account Receivables'), 'accounts_head');
                                     $suspence_account = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_sub_head->SUB_HEAD_ID, 'H_ID' => $ap_sub_head->H_ID, 'A_NAME'=> 'Suspense Payments'), 'accounts');
                                     ?>
                                    <input type="hidden" name="debit_account" value="<?= $suspence_account->A_ID?>" id="debit_account" readonly>
                                    <input type="text"  class="form-control required mb-5" value="<?= $suspence_account->A_NAME?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Transfer To <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <select name="client_id" class="form-control required_select mb-5 heads select_box" id="client" data-width="100%" onchange="getInvoiceId(this.value)">
                                        <option>Select Client</option>
                                        <?php foreach ($all_clients as $client) {?>
                                            <option value="<?php echo $client->client_id; ?>"><?php echo $client->name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Vochers <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <select name="vocher_id" class="form-control required_select mb-5 heads select_box" id="vocher_id" data-width="100%" onchange="getVocherAmount(this.value)">
                                        <option>Select vocher</option>
                                        <?php foreach ($all_vochers as $vochers) {?>
                                            <option value="<?php echo $vochers->ap_id; ?>"><?php echo $vochers->voucher_no; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div id="invoices">
                                <div class="form-group">
                                    <label class="col-lg-4 control-label">JOB #</label>
                                    <div class="col-lg-3">
                                        <select name="invoice_id" id="invoice_id" class="form-control select_box" data-width="100%">

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <input type="number" id="amount" name="others_amount" class="form-control" autocomplete="off" step="any" min="0" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= lang('date') ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <input type="text" name="others_date" class="form-control datepicker" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= lang('description') ?></label>
                                <div class="col-lg-3">
                                    <textarea name="other_desc" class="form-control mb-5"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"></label>
                        <div class="col-lg-4">
                            <button type="submit" class="btn btn-sm btn-success pull-right"><i class="fa fa-check"></i> Transfer</button>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>
</div>
<script type="text/javascript">

    function getVocherAmount(vocher_no) {
        $.getJSON( "<?php echo site_url('admin/accounts/ajax_get_vocher_details') ?>"+"/"+vocher_no, function(result) {
            if(result != null){
                $('#amount').val(result.amount);
            }
        });
    }

    function getInvoiceId(Client_id) {
        var option="<option>---</option>";
            $.getJSON("<?php echo site_url('admin/accounts/ajax_select_invoices') ?>" + "/" +Client_id, function (result) {
                $.each(result, function (index, value) {
                    if(value.type == 1){
                        reference_no1 = 'imp-'+ value.reference_no;
                    }else{
                        reference_no1 = 'exp-'+ value.reference_no;
                    }
                    option += ('<option value="' + value.invoices_id + '">' + reference_no1 + '</option>');
                });
                $('#invoice_id').html(option).hide().fadeIn(500);
            });
    }
</script>