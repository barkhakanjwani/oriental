
		<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header mb-5">
					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url('admin/home');?>"><i class="fa fa-home"></i> Home</a></li>
							<li class="active">Dashboard</li>
						</ul>

					</div>
				</div>
				<!-- /page header -->
	<style>
.highcharts-credits{
display:none;
}
</style>
		<!-- Content area -->
				<div class="content">
				
				<!-- Properties -->
					<div class="panel panel-flat">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-3">
									<div class="panel bg-teal-400 mb-5">
										<div class="panel-body" style="padding-left: 10px;padding-right: 10px;">
											<div class="heading-elements" style="margin-top: 5px;">
												<span class="heading-text" style="height:55px;width: 38px;margin-top: 12px;border-radius: 35px;"><i class ="fa fa-money" style="margin-top: 7px;font-size: 43px;"></i></span>
											</div>
											<h5 class="no-margin">All Cheques</h5>
											<?php echo $total_cheques; ?>
											<div class="progress-bar bg-teal-800 progress-bar-striped active progress progress-xxs mt-10" style="width: 100%">
											</div>
											<a class="heading-elements-toggle"><i class="fa fa-money"></i></a>
										</div>
									</div>
								</div>	
								<div class="col-md-3">
									<div class="panel bg-blue-400 mb-5">
										<div class="panel-body" style="padding-left: 10px;padding-right: 10px;">
											<div class="heading-elements" style="margin-top: 5px;">
												<span class="heading-text" style="height:55px;width: 38px;margin-top: 12px;border-radius: 35px;margin-left: 20px;"><i class ="icon-stack-down" style="margin-top: 7px;font-size: 43px;"></i></span>
											</div>
											<h5 class="no-margin">Pending Cheques</h5>
											<?php echo count_cheques(
												array(
													'C_STATUS'=>'Pending'
												)); ?>
											<div class="progress-bar bg-blue-800 progress-bar-striped active progress progress-xxs mt-10" style="width: 100%">
											</div>
											<a class="heading-elements-toggle"><i class="icon-stack-down"></i></a>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="panel bg-orange-400 mb-5">
										<div class="panel-body" style="padding-left: 10px;padding-right: 10px;">
											<div class="heading-elements" style="margin-top: 5px;">
												<span class="heading-text" style="height:55px;width: 38px;margin-top: 12px;border-radius: 35px;"><i class ="fa fa-check-square-o" style="margin-top: 7px;font-size: 43px;"></i></span>
											</div>
											<h5 class="no-margin">Done Cheques</h5>
											<?php echo count_cheques(
												array(
													'C_STATUS'=>'Approved'
												)); ?>
											<div class="progress-bar bg-orange-800 progress-bar-striped active progress progress-xxs mt-10" style="width: 100%">
											</div>
											<a class="heading-elements-toggle"><i class="fa fa-check-square-o"></i></a>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="panel bg-pink-400 mb-5">
										<div class="panel-body" style="padding-left: 10px;padding-right: 10px;">
											<div class="heading-elements" style="margin-top: 5px;">
												<span class="heading-text" style="height:55px;width: 30px;margin-top: 12px;border-radius: 35px;"><i class ="fa fa-times" style="margin-top: 7px;font-size: 43px;"></i></span>
											</div>
											<h5 class="no-margin">Canceled Cheques</h5>
											<?php echo count_cheques(
												array(
													'C_STATUS'=>'Cancel'
												)); ?>
											<div class="progress-bar bg-pink-800 progress-bar-striped active progress progress-xxs mt-10" style="width: 100%">
											</div>
											<a class="heading-elements-toggle"><i class="fa fa-times"></i></a>
										</div>
									</div>
								</div>
								
				            </div>
							</div>
					</div>
					<!-- /properties -->
				<!-- Properties -->
					<div class="panel panel-flat" st>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">

									<div id="container" style="min-width: 310px; max-width: 600px; height: 400px; margin: 0 auto"></div>

								</div>
								<div class="col-md-6">

									<div id="container1" style="min-width: 310px; max-width: 600px; height: 400px; margin: 0 auto"></div>

								</div>
							</div>
						</div>
					</div>
					<!-- /properties -->

					
				<script type="text/javascript">
					// Create the chart
					Highcharts.chart('container', {
						chart: {
							type: 'pie'
						},
						title: {
							text: 'Banks & Branches'
						},
						subtitle: {
							text: ''
						},
						plotOptions: {
							series: {
								dataLabels: {
									enabled: true,
									//format: '{point.name}: {point.y:.0f}'
								}
							}
						},

						tooltip: {
							headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
							//pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> Branches<br/>'
						},
						series: [{
							name: 'Branches',
							colorByPoint: true,
							data: <?php echo json_encode($banks_data); ?>
						}],
						drilldown: {
							series:<?php echo json_encode($branch_data); ?>
						}
					});

					Highcharts.chart('container1', {
						chart: {
							type: 'pie'
						},
						title: {
							text: 'Cheques & Branches'
						},
						subtitle: {
							text: ''
						},
						plotOptions: {
							series: {
								dataLabels: {
									enabled: true,
									//format: '{point.name}: {point.y:.1f}%'
								}
							}
						},

						tooltip: {
							headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
							//pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
						},
						series: [{
							name: 'Cheques',
							colorByPoint: true,
							data: <?php echo json_encode($cheques_data); ?>
						}],
						drilldown: {
							series: <?php echo json_encode($branches_data) ?>
						}
					});
				</script>

