<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<section class="content-header">
    <div class="row">
        <div class="col-md-12">
			<span class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('list_branches') ?></span> <a href="#" data-toggle="modal" data-target="#add_branch" class="btn btn-danger btn-xs" style="vertical-align: text-bottom;"><?= lang('add_new') ?></a>
		</div>
    </div>
</section>
<!-- Content area -->
<div class="content">
    <!-- Highlighting rows and columns -->
    <div class="panel panel-flat">
        <table class="table table-bordered table-hover DataTables" id="DataTables">
            <thead>
            <tr>
                <th>Bank Name</th>
                <th>Branch Name</th>
                <th>Branch Code</th>
                <th>Branch Address</th>
                <th>Branch City</th>
                <th class="text-center">Status</th>
                <th class="text-center">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php if(!empty($branches)): ?>
                <?php foreach ($branches as $branch){ ?>
					<tr style="white-space: nowrap">
                        <td><?php echo $branch->B_NAME; ?></td>
                        <td><?php echo $branch->BR_NAME; ?></td>
                        <td><?php echo $branch->BR_CODE; ?></td>
                        <td><?php echo $branch->BR_ADDRESS; ?></td>
                        <td><?php echo city($branch->BR_CITY); ?></td>
                            <td class="text-center"><?php echo ($branch->BR_STATUS==1)?"<label class='label label-success'>Active</label>":"<label class='label label-danger'>Inactive</label>"; ?></td>
                        <td class="text-center"><button type="button" onclick="getEditBranch(this.value)" value="<?php echo encrypt($branch->BR_ID);?>" data-popup="tooltip" title="" data-container="body" data-original-title="Edit Branch" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#" ><i class="fa fa-edit"></i></button>
							<!--<button type="button" value="<?php /*echo $branch->BR_ID;*/?>" data-popup="tooltip" title="" data-container="body" data-original-title="Delete Branch" class="btn btn-danger btn-xs delete"><i class="fa fa-trash"></i></button></td>-->
                    </tr>
                <?php } ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
	<!-- /highlighting rows and columns -->
	<!-- Add Branch modal -->
    <div id="add_branch" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h6 class="modal-title">Add Branch</h6>
                </div>
                <div class="modal-body">
                    <?php echo form_open(base_url('admin/accounts/add_branch'),array('class'=>"form-horizontal",'id'=>'form')); ?>
					<div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="control-label">Bank Name <span class="text-danger">*</span></label>
                                            <select name="bank_name" id="bank_name1" onchange="getAccountId()" class="form-control mb-5 select_box" data-width="100%" required>
                                                <option value="">---</option>
                                                <?php foreach ($banks as $bank): ?>
													<option value="<?php echo $bank->B_ID; ?>"><?php echo $bank->B_NAME; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <input type="hidden" name="account_id" id="ac_id">
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="control-label">Branch Name <span class="text-danger">*</span></label>
                                            <input type="text" name="branch_name" placeholder="Branch Name" class="form-control mb-5" required />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="control-label">Branch Code <span class="text-danger">*</span></label>
                                            <input type="text" name="branch_code" placeholder="Branch Code" class="form-control mb-5" required />
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="control-label">Branch Address</label>
                                            <textarea name="branch_address" class="form-control mb-5" placeholder="Address"></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="control-label">Branch City <span class="text-danger">*</span></label>
											<!--<input type="text" name="branch_city" placeholder="Branch City" class="form-control required mb-5" />-->
                                            <select class="form-control select_box" name="branch_city" data-width="100%" required>
                                                <option value="">---</option>
                                                <?php
                                                    foreach($cities as $city) {
                                                        ?>
														<option value="<?= $city->id ?>"><?= $city->name ?></option>
                                                        <?php
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="control-label">Opening Type <span class="text-danger">*</span></label>
											<br /><input type="radio" name="opening_type" value="Debit" required>Debit &nbsp;&nbsp;&nbsp;
											<input type="radio" name="opening_type" value="Credit" required>Credit
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="control-label">Opening Balance <span class="text-danger">*</span></label>
                                            <input type="number" name="opening_balance" min="0" step="any" class="form-control mb-5" required>
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="control-label">Status <span class="text-danger">*</span></label>
                                            <select name="branch_status" class="form-control select_box" data-width="100%" required>
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input  type="submit" class="btn btn-primary"  value="Add Branch"/>
                    </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
	<!-- /Add Branch -->
	<!-- Edit Branch modal -->
    <div id="edit_branch" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h6 class="modal-title">Edit Branch</h6>
                </div>
                <div class="modal-body">
                    <?php echo form_open(base_url('admin/accounts/edit_branch'),array('class'=>"form-horizontal form")); ?>
					<div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="hidden" name="branch_id" id="br_id">
                                    <input type="hidden" name="acc_id" id="account_id">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="control-label">Bank Name <span class="text-danger">*</span></label>
                                            <select name="bank_name" id="bank_name" onchange="getAccountId()" class="form-control select_box" data-width="100%" required>
                                                <option value="">---</option>
												<?php foreach ($banks as $bank): ?>
													<option value="<?php echo $bank->B_ID; ?>"><?php echo $bank->B_NAME; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <input type="hidden" name="account_id" id="ac_id1">
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="control-label">Branch Name <span class="text-danger">*</span></label>
                                            <input type="text" name="branch_name" id="branch_name" placeholder="Branch Name" class="form-control mb-5" required />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="control-label">Branch Code <span class="text-danger">*</span></label>
                                            <input type="text" name="branch_code" id="branch_code" placeholder="Branch Code" class="form-control mb-5" required />
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="control-label">Branch Address</label>
                                            <textarea name="branch_address" id="branch_address" class="form-control mb-5" ></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="control-label">Branch City <span class="text-danger">*</span></label>
                                            <select class="form-control select_box" id="branch_city" name="branch_city" data-width="100%" required>
                                                <option value="">---</option>
                                                <?php
                                                    foreach($cities as $city) {
                                                        ?>
														<option value="<?= $city->id ?>"><?= $city->name ?></option>
                                                        <?php
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="control-label">Opening Type <span class="text-danger">*</span></label>
                                                <br /><input type="radio" name="opening_type" id="opening_type1" value="Debit" required>Debit &nbsp;&nbsp;&nbsp;
                                                <input type="radio" name="opening_type" id="opening_type2" value="Credit" required>Credit
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="control-label">Opening Balance <span class="text-danger">*</span></label>
                                            <input type="number" name="opening_balance" id="opening_balance" min="0" step="any" class="form-control mb-5">
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="control-label">Status <span class="text-danger">*</span></label>
                                            <select name="branch_status" id="branch_status" class="form-control select_box" data-width="100%" required>
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input  type="submit" class="btn btn-primary"  value="Edit Branch"/>
                    </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
	<!-- /Edit Branch Modal -->
	<!-- edit select Branch id script -->
    <script type='text/javascript'>
        function getEditBranch(id)
        {
            $('document').ready(function(){
                var controlmodal=$('#edit_branch');
                var option={
                    "backdrop" : "static"
                }
                controlmodal.modal(option);
                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url("admin/accounts/ajax_edit_branch");?>'+"/"+id,
                    data:false,
                    contentType: false,
                    dataType:"json",
                    success:function(data){
                        $('#br_id').val(data['BR_ID']);
                        $('#bank_name').val(data['B_ID']);
                        $('#bank_name').select2({});
                        $('#branch_name').val(data['BR_NAME']);
                        $('#branch_code').val(data['BR_CODE']);
                        $('#branch_address').val(data['BR_ADDRESS']);
                        $('#branch_city').val(data['BR_CITY']);
                        $('#branch_city').select2({});
                        if(data['BR_OPENINGTYPE']=='Debit'){
                            $('#opening_type1').attr('checked',true);
                        }else if(data['BR_OPENINGTYPE']=='Credit'){
                            $('#opening_type2').attr('checked',true);
                        }
                        $('#opening_balance').val(data['BR_OPENINGBALANCE']);
                        $('#branch_status').val(data['BR_STATUS']);
                        $('#branch_status').select2({});
                        $('#account_id').val(data['A_ID']);
                        getAccountId();
                    },
                    error:function(data){
                        console.log(data.responseText + "not work")
                    },
                });
            });
        }
    </script>
	<!-- edit select Branch id script -->
	<!--Delete Branch Script-->
    <script type="text/javascript">
        $('.delete').click(function () {
            if(confirm("Are you sure you want to delete this branch?")){
                $.ajax({
                    type:"POST",
                    url:"<?php echo base_url('admin/accounts/delete_branch'); ?>"+"/"+$(this).val(),
                    dataType:'JSON',
                    success:function(data){
                        if(data>0){
                            setTimeout(function(){location.href="<?php echo base_url('admin/accounts/branches'); ?>"} , 2000);
                            $.jGrowl('Branch has been Deleted', {
                                header: 'Successfully',
                                theme: 'bg-danger alert-styled-left alert-styled-check'
                            });
                        }
                    }
                });
            }else {
                return false;
            }
        });
    </script>
	<!--/Delete Branch Script-->
	<!-- Select Bank Account Id-->
    <script type="text/javascript">
        $(document).ready(function () {
            getAccountId();
        });
        function getAccountId() {
            var option="";
            var account_id="";
            if($('#br_id').val()!='' && $('#edit_branch').modal('show')){
                option=$('#bank_name');
                account_id=$('#ac_id1');
            }else {
                option=$('#bank_name1');
                account_id=$('#ac_id');
            }
            $.getJSON( "<?php echo site_url('admin/accounts/ajax_edit_bank') ?>"+"/"+option.val(), function(result) {
                if(result != null){
                    account_id.val(result.A_ID);
                }
            });
        }
    </script>
	<!-- /Select Bank Account Id-->