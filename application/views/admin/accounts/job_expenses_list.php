
<section class="content-header">
    <div class="row">
        <div class="col-md-12">
            <span class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('job_expenses') ?></span> <a href="<?= base_url('admin/accounts/manage_job_expense')?>" class="btn btn-danger btn-xs" style="vertical-align: text-bottom;"><?= lang('add_new') ?></a>
        </div>
    </div>
</section>
<!-- Content area -->
<div class="content">
    <!-- Highlighting rows and columns -->
    <!--<div class="panel panel-flat">
        <table class="table table-bordered table-hover DataTables" id="DataTables">
            <thead>
            <tr>
                <th>Voucher No.</th>
                <th>
                    <?/*= lang('job_no') */?>
                </th>
                <th>
                    <?/*= lang('created_date') */?>
                </th>
                <th>
                    <?/*= lang('title') */?>
                </th>
                <th>
                    <?/*= lang('payment_date') */?>
                </th>
                <th>
                    <?/*= lang('payment_method') */?>
                </th>
                <th>
                    <?/*= lang('amount') */?>
                </th>
                <th class="text-center">
                    <?/*= lang('action') */?>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php /*if(!empty($vouchers)): */?>
                <?php /*foreach ($vouchers as $voucher){
                    $voucher_details = $this->invoice_model->check_by_all(array('voucher_id'=>$voucher->voucher_id), 'tbl_voucher_details');
                    $jobs = "";$titles = "";$amounts = ""; $invoices_no="";
                    foreach($voucher_details as $vd){
                        $jobs = job_info($vd->invoices_id)->reference_no."<br />".$jobs;
                        if ($vd->job_expense_id != 0) {
                            $titles = job_expense_title($vd->job_expense_id) . "<br />" . $titles;
                        }else{
                            $titles= 'Security Deposit';
                        }
                        $invoices_no=$this->invoice_model->job_no_creation($vd->invoices_id) . "<br />" . $invoices_no;
                        $amounts = number_format($vd->amount,2)."<br />".$amounts;                    }                */?>
                        <tr style="white-space: nowrap;">
                            <td><?/*= $voucher->voucher_no */?></td>
                            <td>
                                <?/*= $invoices_no */?>
                            </td>
                            <td>
                                <?php /*echo date('d-m-Y',strtotime($voucher->created_date)); */?>
                            </td>
                            <td>
                                <?/*= $titles */?>
                            </td>
                            <td>
                                <?php /*echo date('d-m-Y',strtotime($voucher->payment_date)); */?>
                            </td>
                            <td>
                                <?php /*echo ($voucher->payment_method == 1)?'Cash':'Bank'; */?>
                            </td>
                            <td>
                                <?/*= $amounts */?>
                            </td>-->
                            <!--<td>
                                <?php /*echo ($voucher->status == 1)?'<label class="label label-success">Approved</label>':'<label class="label label-danger">Reject</label>'; */?>
                            </td>-->
                            <!--<td class="text-center">
                                <?php /*btn_edit('admin/accounts/manage_job_expense/edit_voucher/'.$voucher->voucher_id) */?>
                                <?/*= btn_view('admin/accounts/manage_job_expense/voucher_detail/'.$voucher->voucher_id) */?>
                            </td>
                        </tr>
                <?php /*} */?>
            <?php /*endif; */?>
            </tbody>
        </table>
    </div>-->
    <!-- /highlighting rows and columns -->

    <section class="panel panel-default">

        <header class="panel-heading">List <?= lang('job_expenses') ?></header>

        <div class="panel-body" id="export_table">

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive" id="customers">
                        <table class="table table-bordered data-table-list dtHorizontalExample" id="tab_customers">
                            <thead>
                            <th>Voucher No.</th>
                            <th><?= lang('job_no') ?></th>
                            <th><?= lang('created_date') ?></th>
                            <th><?= lang('title') ?></th>
                            <th><?= lang('payment_date') ?></th>
                            <th><?= lang('payment_method') ?></th>
                            <th><?= lang('amount') ?></th>
                            <th class="text-center"><?= lang('action') ?></th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <script>
        var data_url='<?= base_url('admin/accounts/job_expenses') ?>';
    </script>