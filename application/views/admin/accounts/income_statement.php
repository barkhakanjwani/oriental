	
	<!-- Content area -->
    <h1 class="header-<?= config_item('sidebar_theme'); ?>" style="margin: 0px 0px 0px 15px;"><?= lang('search_by') ?> <?= lang('income_statement_list') ?></h1>
    <div class="content">
        <!-- Highlighting rows and columns -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <?php echo form_open(base_url('admin/accounts/income_statement'),array('class'=>"form-horizontal")); ?>
                <div class="row">
                    <div class="col-xs-2">
                        <input type="text" name="start_date" class="form-control datepicker" placeholder="From Date" autocomplete="off">
                    </div>
                    <div class="col-xs-2">
                        <input type="text" name="end_date" class="form-control datepicker" placeholder="To Date" autocomplete="off">
                    </div>
                    <div class="col-xs-2">
                        <button class="btn btn-sm btn-success"><i class="fa fa-search"></i></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <div class="panel-body">
                <div class="row" id="export_table">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th width="50%">Account Name</th>
                            <th width="15%">Account No</th>
                            <th width="30%">Balance</th>
                            <th width="5%">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $total_opening=0; $total_debit=0; $total_credit=0;
                            $t_opening=0; ?>
                                <?php $sb=sub_type_by_id(16);
                                $sales=0;
                                $account_head=account_head_by_subId($sb->SUB_HEAD_ID);
                                ?>
                                <tr>
                                    <th><?= $sb->NAME; ?></th>
                                    <th><?= $sb->SUB_NO; ?></th>
                                    <?php
                                        $ac_balance=0;
                                        $db_transactions=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Debit");
                                        $cr_transactions=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Credit");
                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                            $ac_balance+=$db_transactions->amount+$cr_transactions->amount;
                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                            $ac_balance+=$db_transactions->amount-$cr_transactions->amount;
                                        }
                                        $sales+=$ac_balance;
                                    ?>
                                    <th><?= number_format(abs($sales),2);?></th>
                                    <th><?php if($sales<0){
                                            echo "Cr";
                                        }elseif($sales>0){
                                            echo "Dr";
                                        }
                                        ?></th>
                                </tr>
                                <?php foreach ($account_head as $ah) {
                                    $head_balance = 0;
                                    $accounts = account_by_head($ah->H_ID);
                                    ?>
                                    <tr>
                                        <td><?= $ah->H_NAME; ?></td>
                                        <td><?= $ah->H_NO; ?></td>
                                        <?php /*foreach ($accounts as $acc) { */
                                            $ac_bal = 0;
                                            $db_transactions = transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Debit");
                                            $cr_transactions = transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Credit");
                                            if ($sb->HEAD_TYPE == 'Liabilities' || $sb->HEAD_TYPE == 'Equity' || $sb->HEAD_TYPE == 'Income') {
                                                $ac_bal += $db_transactions->amount+$cr_transactions->amount;
                                            } elseif ($sb->HEAD_TYPE == 'Assets' || $sb->HEAD_TYPE == 'Expense') {
                                                $ac_bal += $db_transactions->amount-$cr_transactions->amount;
                                            }
                                            $head_balance += $ac_bal;
                                        /*}*/ ?>
                                        <td><?= number_format(abs($head_balance), 2);?></td>
                                        <td><?php if ($head_balance < 0) {
                                                echo "Cr";
                                            } elseif ($head_balance > 0) {
                                                echo "Dr";
                                            }
                                            ?></td>
                                    </tr>
                                    <?php foreach ($accounts as $a){
                                        $client = ($a->CLIENT_ID == "")?'':" (".client_name($a->CLIENT_ID).")";
                                        $account_balance=0;
                                        if($a->A_SELFHEAD_ID==0){ ?>
                                            <tr>
                                                <td><?= $a->A_NAME.$client; ?></td>
                                                <td><?= $a->A_NO; ?></td>
                                                <?php
                                                $debit_transactions=transaction_date_by_self($a->A_ID,$start_date,$end_date,"Debit");
                                                $credit_transactions=transaction_date_by_self($a->A_ID,$start_date,$end_date,"Credit");
                                                if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                    $account_balance-=$debit_transactions->amount+$credit_transactions->amount;
                                                }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                    $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                                }
                                                ?>
                                                <td><?= number_format(abs($account_balance),2);?></td>
                                                <td><?php if($account_balance<0){
                                                        echo "Cr";
                                                    }elseif($account_balance>0){
                                                        echo "Dr";
                                                    }
                                                    ?></td>
                                            </tr>
                                            <?php foreach ($accounts as $ac){
                                                $client = ($ac->CLIENT_ID == "")?'':" (".client_name($ac->CLIENT_ID).")";
                                                $account_balance=0;
                                                if($ac->A_SELFHEAD_ID==$a->A_ID){?>
                                                    <tr>
                                                        <td><?= $ac->A_NAME.$client; ?></td>
                                                        <td><?= $ac->A_NO; ?></td>
                                                        <?php
                                                        $debit_transactions=transaction_date_by_self($ac->A_ID,$start_date,$end_date,"Debit");
                                                        $credit_transactions=transaction_date_by_self($ac->A_ID,$start_date,$end_date,"Credit");
                                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                            $account_balance-=$debit_transactions->amount+$credit_transactions->amount;
                                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                            $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                                        }
                                                        ?>
                                                        <td><?= number_format(abs($account_balance),2);?></td>
                                                        <td><?php if($account_balance<0){
                                                                echo "Cr";
                                                            }elseif($account_balance>0){
                                                                echo "Dr";
                                                            }?></td>
                                                    </tr>
                                                    <?php foreach ($accounts as $account){
                                                        $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")";
                                                        $account_balance=0;
                                                        if($account->A_SELFHEAD_ID==$ac->A_ID){ ?>
                                                            <tr>
                                                                <td><?= $account->A_NAME.$client; ?></td>
                                                                <td><?= $account->A_NO; ?></td>
                                                                <?php
                                                                $debit_transactions=transaction_date_by_self($account->A_ID,$start_date,$end_date,"Debit");
                                                                $credit_transactions=transaction_date_by_self($account->A_ID,$start_date,$end_date,"Credit");
                                                                if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                                    $account_balance+=$debit_transactions->amount+$credit_transactions->amount;
                                                                }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                                    $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                                                }
                                                                ?>
                                                                <td><?= number_format(abs($account_balance),2);?></td>
                                                                <td><?php if($account_balance<0){
                                                                        echo "Cr";
                                                                    }elseif($account_balance>0){
                                                                        echo "Dr";
                                                                    }?></td>
                                                            </tr>
                                                            <?php foreach ($accounts as $acc){
                                                                $client = ($acc->CLIENT_ID == "")?'':" (".client_name($acc->CLIENT_ID).")";
                                                                $account_balance=0;
                                                                if($acc->A_SELFHEAD_ID==$account->A_ID){ ?>
                                                                    <tr>
                                                                        <td><?= $acc->A_NAME; ?></td>
                                                                        <td><?= $acc->A_NO; ?></td>
                                                                        <?php
                                                                        $debit_transactions=transaction_date_by_self($acc->A_ID,$start_date,$end_date,"Debit");
                                                                        $credit_transactions=transaction_date_by_self($acc->A_ID,$start_date,$end_date,"Credit");
                                                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                                            $account_balance+=$debit_transactions->amount+$credit_transactions->amount;
                                                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                                            $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                                                        }
                                                                        ?>
                                                                        <td><?= number_format(abs($account_balance),2);?></td>
                                                                        <td><?php if($account_balance<0){
                                                                                echo "Cr";
                                                                            }elseif($account_balance>0){
                                                                                echo "Dr";
                                                                            }?></td>
                                                                    </tr>
                                                                    <?php foreach ($accounts as $acct){
                                                                        $client = ($acct->CLIENT_ID == "")?'':" (".client_name($acct->CLIENT_ID).")";
                                                                        $account_balance=0;
                                                                        if($acct->A_SELFHEAD_ID==$acc->A_ID){ ?>
                                                                            <tr>
                                                                                <td><?= $acct->A_NAME.$client; ?></td>
                                                                                <td><?= $acct->A_NO; ?></td>
                                                                                <?php
                                                                                $debit_transactions=transaction_date_by_self($acct->A_ID,$start_date,$end_date,"Debit");
                                                                                $credit_transactions=transaction_date_by_self($acct->A_ID,$start_date,$end_date,"Credit");
                                                                                if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                                                    $account_balance+=$debit_transactions->amount+$credit_transactions->amount;
                                                                                }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                                                    $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                                                                }
                                                                                ?>
                                                                                <td><?= number_format(abs($account_balance),2);?></td>
                                                                                <td><?php if($account_balance<0){
                                                                                        echo "Cr";
                                                                                    }elseif($account_balance>0){
                                                                                        echo "Dr";
                                                                                    }?></td>
                                                                            </tr>
                                                                        <?php }
                                                                    } ?>
                                                                <?php }
                                                            } ?>
                                                        <?php }
                                                    } ?>
                                                <?php }
                                            } ?>
                                        <?php }
                                    }

                                }?>

                                <?php $sb=sub_type_by_id(17);
                                $cost_of_sales=0;
                                $account_head=account_head_by_subId($sb->SUB_HEAD_ID); ?>
                                <tr>
                                    <th><?= $sb->NAME; ?></th>
                                    <th><?= $sb->SUB_NO; ?></th>
                                    <?php
                                        $ac_balance=0;
                                        $db_transactions=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Debit");
                                        $cr_transactions=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Credit");
                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                            $ac_balance+=$db_transactions->amount+$cr_transactions->amount;
                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                            $ac_balance+=$db_transactions->amount-$cr_transactions->amount;
                                        }
                                        $cost_of_sales+=$ac_balance;
                                    ?>
                                    <th><?= number_format(abs($cost_of_sales),2);?></th>
                                    <th><?php if($cost_of_sales<0){
                                            echo "Cr";
                                        }elseif($cost_of_sales>0){
                                            echo "Dr";
                                        }
                                        ?></th>
                                </tr>
                                <?php foreach ($account_head as $ah) {
                                    $head_balance = 0;
                                    $accounts = account_by_head($ah->H_ID);
                                    ?>
                                    <tr>
                                        <td><?= $ah->H_NAME; ?></td>
                                        <td><?= $ah->H_NO; ?></td>
                                        <?php /*foreach ($accounts as $acc) { */
                                            $ac_bal = 0;
                                            $db_transactions = transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Debit");
                                            $cr_transactions = transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Credit");
                                            if ($sb->HEAD_TYPE == 'Liabilities' || $sb->HEAD_TYPE == 'Equity' || $sb->HEAD_TYPE == 'Income') {
                                                $ac_bal += $db_transactions->amount+$cr_transactions->amount;
                                            } elseif ($sb->HEAD_TYPE == 'Assets' || $sb->HEAD_TYPE == 'Expense') {
                                                $ac_bal += $db_transactions->amount-$cr_transactions->amount;
                                            }
                                            $head_balance += $ac_bal;
                                        /*}*/ ?>
                                        <td><?= number_format(abs($head_balance), 2);?></td>
                                        <td><?php
                                            if ($head_balance < 0) {
                                                echo "Cr";
                                            } elseif ($head_balance > 0) {
                                                echo "Dr";
                                            }
                                            ?></td>
                                    </tr>
                                    <?php foreach ($accounts as $account){
                                        $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")";
                                        $account_balance=0;
                                        if($account->A_SELFHEAD_ID==$ac->A_ID){
                                            ?>
                                            <tr>
                                                <td><?= $account->A_NAME.$client; ?></td>
                                                <td><?= $account->A_NO; ?></td>
                                                <?php
                                                $debit_transactions=transaction_date_by_self($account->A_ID,$start_date,$end_date,"Debit");
                                                $credit_transactions=transaction_date_by_self($account->A_ID,$start_date,$end_date,"Credit");
                                                if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                    $account_balance+=$debit_transactions->amount+$credit_transactions->amount;
                                                }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                    $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                                }
                                                ?>
                                                <td><?= number_format(abs($account_balance),2);?></td>
                                                <td><?php
                                                    if($account_balance<0){
                                                        echo "Cr";
                                                    }elseif($account_balance>0){
                                                        echo "Dr";
                                                    }?></td>
                                            </tr>
                                            <?php foreach ($accounts as $acc){
                                                $client = ($acc->CLIENT_ID == "")?'':" (".client_name($acc->CLIENT_ID).")";
                                                $account_balance=0;
                                                if($acc->A_SELFHEAD_ID==$account->A_ID){
                                                    ?>
                                                    <tr>
                                                        <td><?= $acc->A_NAME.$client; ?></td>
                                                        <td><?= $acc->A_NO; ?></td>
                                                        <?php
                                                        $debit_transactions=transaction_date_by_self($acc->A_ID,$start_date,$end_date,"Debit");
                                                        $credit_transactions=transaction_date_by_self($acc->A_ID,$start_date,$end_date,"Credit");
                                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                            $account_balance+=$debit_transactions->amount+$credit_transactions->amount;
                                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                            $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                                        }
                                                        ?>
                                                        <td><?= number_format(abs($account_balance),2);?></td>
                                                        <td><?php
                                                            if($account_balance<0){
                                                                echo "Cr";
                                                            }elseif($account_balance>0){
                                                                echo "Dr";
                                                            }?></td>
                                                    </tr>
                                                    <?php foreach ($accounts as $acct){
                                                        $client = ($acct->CLIENT_ID == "")?'':" (".client_name($acct->CLIENT_ID).")";
                                                        $account_balance=0;
                                                        if($acct->A_SELFHEAD_ID==$acc->A_ID){ ?>
                                                            <tr>
                                                                <td><?= $acct->A_NAME.$client; ?></td>
                                                                <td><?= $acct->A_NO; ?></td>
                                                                <?php
                                                                $debit_transactions=transaction_date_by_self($acct->A_ID,$start_date,$end_date,"Debit");
                                                                $credit_transactions=transaction_date_by_self($acct->A_ID,$start_date,$end_date,"Credit");
                                                                if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                                    $account_balance+=$debit_transactions->amount+$credit_transactions->amount;
                                                                }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                                    $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                                                }
                                                                ?>
                                                                <td><?= number_format(abs($account_balance),2);?></td>
                                                                <td><?php
                                                                    if($account_balance<0){
                                                                        echo "Cr";
                                                                    }elseif($account_balance>0){
                                                                        echo "Dr";
                                                                    }?></td>
                                                            </tr>
                                                        <?php }
                                                    } ?>
                                                <?php }
                                            } ?>
                                        <?php }
                                    }
                                }?>
                            <tr>
                                <th>Gross Profit/Loss</th>
                                <th></th>
                                <th><?php $gross_profit=$sales-$cost_of_sales;
                                    echo number_format(abs($gross_profit),2);?></th>
                                <th><?php
                                    if($gross_profit<0){
                                        echo "Cr";
                                    }elseif($gross_profit>0){
                                        echo "Dr";
                                    }
                                    ?></th>
                            </tr>
                                <?php $sb=sub_type_by_id(8);
                                $other_income=0;
                                $account_head=account_head_by_subId($sb->SUB_HEAD_ID); ?>
                                <tr>
                                    <th><?= $sb->NAME; ?></th>
                                    <th><?= $sb->SUB_NO; ?></th>
                                    <?php
                                        $ac_balance=0;
                                        $db_transactions=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Debit");
                                        $cr_transactions=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Credit");
                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                            $ac_balance+=$db_transactions->amount+$cr_transactions->amount;
                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                            $ac_balance+=$db_transactions->amount-$cr_transactions->amount;
                                        }
                                        $other_income+=$ac_balance;
                                    ?>
                                    <th><?= number_format(abs($other_income),2);?></th>
                                    <th><?php
                                        if($other_income<0){
                                            echo "Cr";
                                        }elseif($other_income>0){
                                            echo "Dr";
                                        }
                                        ?></th>
                                </tr>
                                <?php foreach ($account_head as $ah) {
                                    $head_balance = 0;
                                    $accounts = account_by_head($ah->H_ID);
                                    ?>
                                    <tr>
                                        <td><?= $ah->H_NAME; ?></td>
                                        <td><?= $ah->H_NO; ?></td>
                                        <?php /*foreach ($accounts as $acc) { */
                                            $ac_bal = 0;
                                            $db_transactions = transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Debit");
                                            $cr_transactions = transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Credit");
                                            if ($sb->HEAD_TYPE == 'Liabilities' || $sb->HEAD_TYPE == 'Equity' || $sb->HEAD_TYPE == 'Income') {
                                                $ac_bal += $db_transactions->amount+$cr_transactions->amount;
                                            } elseif ($sb->HEAD_TYPE == 'Assets' || $sb->HEAD_TYPE == 'Expense') {
                                                $ac_bal += $db_transactions->amount-$cr_transactions->amount;
                                            }
                                            $head_balance += $ac_bal;
                                        /*}*/ ?>
                                        <td><?= number_format(abs($head_balance), 2);?></td>
                                        <td><?php
                                            if ($head_balance < 0) {
                                                echo "Cr";
                                            } elseif ($head_balance > 0) {
                                                echo "Dr";
                                            }
                                            ?></td>
                                    </tr>
                                    <?php foreach ($accounts as $account){
                                        $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")";
                                        $account_balance=0;
                                        if($account->A_SELFHEAD_ID==$ac->A_ID){ ?>
                                            <tr>
                                                <td><?= $account->A_NAME.$client; ?></td>
                                                <td><?= $account->A_NO; ?></td>
                                                <?php
                                                $debit_transactions=transaction_date_by_self($account->A_ID,$start_date,$end_date,"Debit");
                                                $credit_transactions=transaction_date_by_self($account->A_ID,$start_date,$end_date,"Credit");
                                                if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                    $account_balance+=$debit_transactions->amount+$credit_transactions->amount;
                                                }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                    $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                                }
                                                ?>
                                                <td><?= number_format(abs($account_balance),2);?></td>
                                                <td><?php
                                                    if($account_balance<0){
                                                        echo "Cr";
                                                    }elseif($account_balance>0){
                                                        echo "Dr";
                                                    }?></td>
                                            </tr>
                                            <?php foreach ($accounts as $acc){
                                                $client = ($acc->CLIENT_ID == "")?'':" (".client_name($acc->CLIENT_ID).")";
                                                $account_balance=0;
                                                if($acc->A_SELFHEAD_ID==$account->A_ID){ ?>
                                                    <tr>
                                                        <td><?= $acc->A_NAME.$client; ?></td>
                                                        <td><?= $acc->A_NO; ?></td>
                                                        <?php
                                                        $debit_transactions=transaction_date_by_self($acc->A_ID,$start_date,$end_date,"Debit");
                                                        $credit_transactions=transaction_date_by_self($acc->A_ID,$start_date,$end_date,"Credit");
                                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                            $account_balance+=$debit_transactions->amount+$credit_transactions->amount;
                                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                            $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                                        }
                                                        ?>
                                                        <td><?= number_format(abs($account_balance),2);?></td>
                                                        <td><?php
                                                            if($account_balance<0){
                                                                echo "Cr";
                                                            }elseif($account_balance>0){
                                                                echo "Dr";
                                                            }?></td>
                                                    </tr>
                                                    <?php foreach ($accounts as $acct){
                                                        $client = ($acct->CLIENT_ID == "")?'':" (".client_name($acct->CLIENT_ID).")";
                                                        $account_balance=0;
                                                        if($acct->A_SELFHEAD_ID==$acc->A_ID){ ?>
                                                            <tr>
                                                                <td><?= $acct->A_NAME.$client; ?></td>
                                                                <td><?= $acct->A_NO; ?></td>
                                                                <?php
                                                                $debit_transactions=transaction_date_by_self($acct->A_ID,$start_date,$end_date,"Debit");
                                                                $credit_transactions=transaction_date_by_self($acct->A_ID,$start_date,$end_date,"Credit");
                                                                if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                                    $account_balance+=$debit_transactions->amount+$credit_transactions->amount;
                                                                }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                                    $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                                                }
                                                                ?>
                                                                <td><?= number_format(abs($account_balance),2);?></td>
                                                                <td><?php
                                                                    if($account_balance<0){
                                                                        echo "Cr";
                                                                    }elseif($account_balance>0){
                                                                        echo "Dr";
                                                                    }?></td>
                                                            </tr>
                                                        <?php }
                                                    } ?>
                                                <?php }
                                            } ?>
                                        <?php }
                                    }
                                }?>

                                <?php $sb=sub_type_by_id(10);
                                $op_expense=0;
                                $account_head=account_head_by_subId($sb->SUB_HEAD_ID); ?>
                                <tr>
                                    <th><?= $sb->NAME; ?></th>
                                    <th><?= $sb->SUB_NO; ?></th>
                                    <?php
                                        $ac_balance=0;
                                        $db_transactions=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Debit");
                                        $cr_transactions=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Credit");
                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                            $ac_balance+=$db_transactions->amount+$cr_transactions->amount;
                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                            $ac_balance+=$db_transactions->amount-$cr_transactions->amount;
                                        }
                                        $op_expense+=$ac_balance;
                                    ?>
                                    <th><?= number_format(abs($op_expense),2);?></th>
                                    <th><?php
                                        if($op_expense<0){
                                            echo "Cr";
                                        }elseif($op_expense>0){
                                            echo "Dr";
                                        }
                                        ?></th>
                                </tr>
                                <?php foreach ($account_head as $ah) {
                                $head_balance = 0;
                                $accounts = account_by_head($ah->H_ID);
                                ?>
                                <tr>
                                    <td><?= $ah->H_NAME; ?></td>
                                    <td><?= $ah->H_NO; ?></td>
                                    <?php /*foreach ($accounts as $acc) {*/
                                        $ac_bal = 0;
                                        $db_transactions = transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Debit");
                                        $cr_transactions = transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Credit");
                                        if ($sb->HEAD_TYPE == 'Liabilities' || $sb->HEAD_TYPE == 'Equity' || $sb->HEAD_TYPE == 'Income') {
                                            $ac_bal += $db_transactions->amount+$cr_transactions->amount;
                                        } elseif ($sb->HEAD_TYPE == 'Assets' || $sb->HEAD_TYPE == 'Expense') {
                                            $ac_bal += $db_transactions->amount-$cr_transactions->amount;
                                        }
                                        $head_balance += $ac_bal;
                                    /*}*/ ?>
                                    <td><?= number_format(abs($head_balance), 2);?></td>
                                    <td><?php
                                        if ($head_balance < 0) {
                                            echo "Cr";
                                        } elseif ($head_balance > 0) {
                                            echo "Dr";
                                        }
                                        ?></td>
                                </tr>
                                <?php foreach ($accounts as $account){
                                        $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")";
                                    $account_balance=0;
                                    if($account->A_SELFHEAD_ID==$ac->A_ID){ ?>
                                        <tr>
                                            <td><?= $account->A_NAME.$client; ?></td>
                                            <td><?= $account->A_NO; ?></td>
                                            <?php
                                            $debit_transactions=transaction_date_by_self($account->A_ID,$start_date,$end_date,"Debit");
                                            $credit_transactions=transaction_date_by_self($account->A_ID,$start_date,$end_date,"Credit");
                                            if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                $account_balance+=$debit_transactions->amount+$credit_transactions->amount;
                                            }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                            }
                                            ?>
                                            <td><?= number_format(abs($account_balance),2);?></td>
                                            <td><?php
                                                if($account_balance<0){
                                                    echo "Cr";
                                                }elseif($account_balance>0){
                                                    echo "Dr";
                                                }?></td>
                                        </tr>
                                        <?php foreach ($accounts as $acc){
                                            $client = ($acc->CLIENT_ID == "")?'':" (".client_name($acc->CLIENT_ID).")";
                                            $account_balance=0;
                                            if($acc->A_SELFHEAD_ID==$account->A_ID){ ?>
                                                <tr>
                                                    <td><?= $acc->A_NAME.$client; ?></td>
                                                    <td><?= $acc->A_NO; ?></td>
                                                    <?php
                                                    $debit_transactions=transaction_date_by_self($acc->A_ID,$start_date,$end_date,"Debit");
                                                    $credit_transactions=transaction_date_by_self($acc->A_ID,$start_date,$end_date,"Credit");
                                                    if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                        $account_balance+=$debit_transactions->amount+$credit_transactions->amount;
                                                    }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                        $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                                    }
                                                    ?>
                                                    <td><?= number_format(abs($account_balance),2);?></td>
                                                    <td><?php
                                                        if($account_balance<0){
                                                            echo "Cr";
                                                        }elseif($account_balance>0){
                                                            echo "Dr";
                                                        }?></td>
                                                </tr>
                                                <?php foreach ($accounts as $acct){
                                                    $client = ($acct->CLIENT_ID == "")?'':" (".client_name($acct->CLIENT_ID).")";
                                                    $account_balance=0;
                                                    if($acct->A_SELFHEAD_ID==$acc->A_ID){ ?>
                                                        <tr>
                                                            <td><?= $acct->A_NAME.$client; ?></td>
                                                            <td><?= $acct->A_NO; ?></td>
                                                            <?php
                                                            $debit_transactions=transaction_date_by_self($acct->A_ID,$start_date,$end_date,"Debit");
                                                            $credit_transactions=transaction_date_by_self($acct->A_ID,$start_date,$end_date,"Credit");
                                                            if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                                $account_balance+=$debit_transactions->amount+$credit_transactions->amount;
                                                            }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                                $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                                            }
                                                            ?>
                                                            <td><?= number_format(abs($account_balance),2);?></td>
                                                            <td><?php
                                                                if($account_balance<0){
                                                                    echo "Cr";
                                                                }elseif($account_balance>0){
                                                                    echo "Dr";
                                                                }?></td>
                                                        </tr>
                                                    <?php }
                                                } ?>
                                            <?php }
                                        } ?>
                                    <?php }
                                }
                            }?>

                                <?php $sb=sub_type_by_id(14);
                                $other_expense=0;
                                $account_head=account_head_by_subId($sb->SUB_HEAD_ID); ?>
                                <tr>
                                    <th><?= $sb->NAME; ?></th>
                                    <th><?= $sb->SUB_NO; ?></th>
                                    <?php
                                        $ac_balance=0;
                                        $db_transactions=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Debit");
                                        $cr_transactions=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Credit");
                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                            $ac_balance+=$db_transactions->amount+$cr_transactions->amount;
                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                            $ac_balance+=$db_transactions->amount-$cr_transactions->amount;
                                        }
                                        $other_expense+=$ac_balance;
                                    ?>
                                    <th><?= number_format(abs($other_expense),2);?></th>
                                    <th><?php
                                        if($other_expense<0){
                                            echo "Cr";
                                        }elseif($other_expense>0){
                                            echo "Dr";
                                        }
                                        ?></th>
                                </tr>
                                <?php foreach ($account_head as $ah) {
                                $head_balance = 0;
                                $accounts = account_by_head($ah->H_ID);
                                ?>
                                <tr>
                                    <td><?= $ah->H_NAME; ?></td>
                                    <td><?= $ah->H_NO; ?></td>
                                    <?php /*foreach ($accounts as $acc) {*/
                                        $ac_bal = 0;
                                        $db_transactions = transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Debit");
                                        $cr_transactions = transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Credit");
                                        if ($sb->HEAD_TYPE == 'Liabilities' || $sb->HEAD_TYPE == 'Equity' || $sb->HEAD_TYPE == 'Income') {
                                            $ac_bal += $db_transactions->amount+$cr_transactions->amount;
                                        } elseif ($sb->HEAD_TYPE == 'Assets' || $sb->HEAD_TYPE == 'Expense') {
                                            $ac_bal += $db_transactions->amount-$cr_transactions->amount;
                                        }
                                        $head_balance += $ac_bal;
                                    /*}*/ ?>
                                    <td><?= number_format(abs($head_balance), 2);?></td>
                                    <td><?php
                                        if ($head_balance < 0) {
                                            echo "Cr";
                                        } elseif ($head_balance > 0) {
                                            echo "Dr";
                                        }
                                        ?></td>
                                </tr>
                                <?php foreach ($accounts as $account){
                                        $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")";
                                    $account_balance=0;
                                    if($account->A_SELFHEAD_ID==$ac->A_ID){ ?>
                                        <tr>
                                            <td><?= $account->A_NAME.$client; ?></td>
                                            <td><?= $account->A_NO; ?></td>
                                            <?php
                                            $debit_transactions=transaction_date_by_self($account->A_ID,$start_date,$end_date,"Debit");
                                            $credit_transactions=transaction_date_by_self($account->A_ID,$start_date,$end_date,"Credit");
                                            if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                $account_balance+=$debit_transactions->amount+$credit_transactions->amount;
                                            }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                            }
                                            ?>
                                            <td><?= number_format(abs($account_balance),2);?></td>
                                            <td><?php
                                                if($account_balance<0){
                                                    echo "Cr";
                                                }elseif($account_balance>0){
                                                    echo "Dr";
                                                }?></td>
                                        </tr>
                                        <?php foreach ($accounts as $acc){
                                            $client = ($acc->CLIENT_ID == "")?'':" (".client_name($acc->CLIENT_ID).")";
                                            $account_balance=0;
                                            if($acc->A_SELFHEAD_ID==$account->A_ID){ ?>
                                                <tr>
                                                    <td><?= $acc->A_NAME.$client; ?></td>
                                                    <td><?= $acc->A_NO; ?></td>
                                                    <?php
                                                    $debit_transactions=transaction_date_by_self($acc->A_ID,$start_date,$end_date,"Debit");
                                                    $credit_transactions=transaction_date_by_self($acc->A_ID,$start_date,$end_date,"Credit");
                                                    if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                        $account_balance+=$debit_transactions->amount+$credit_transactions->amount;
                                                    }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                        $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                                    }
                                                    ?>
                                                    <td><?= number_format(abs($account_balance),2);?></td>
                                                    <td><?php
                                                        if($account_balance<0){
                                                            echo "Cr";
                                                        }elseif($account_balance>0){
                                                            echo "Dr";
                                                        }?></td>
                                                </tr>
                                                <?php foreach ($accounts as $acct){
                                                    $client = ($acct->CLIENT_ID == "")?'':" (".client_name($acct->CLIENT_ID).")";
                                                    $account_balance=0;
                                                    if($acct->A_SELFHEAD_ID==$acc->A_ID){ ?>
                                                        <tr>
                                                            <td><?= $acct->A_NAME.$client; ?></td>
                                                            <td><?= $acct->A_NO; ?></td>
                                                            <?php
                                                            $debit_transactions=transaction_date_by_self($acct->A_ID,$start_date,$end_date,"Debit");
                                                            $credit_transactions=transaction_date_by_self($acct->A_ID,$start_date,$end_date,"Credit");
                                                            if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                                $account_balance+=$debit_transactions->amount+$credit_transactions->amount;
                                                            }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                                $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                                            }
                                                            ?>
                                                            <td><?= number_format(abs($account_balance),2);?></td>
                                                            <td><?php
                                                                if($account_balance<0){
                                                                    echo "Cr";
                                                                }elseif($account_balance>0){
                                                                    echo "Dr";
                                                                }?></td>
                                                        </tr>
                                                    <?php }
                                                } ?>
                                            <?php }
                                        } ?>
                                    <?php }
                                }
                            }?>

                            <tr>
                                <th>Operating Profit/ Loss</th>
                                <th></th>
                                <th><?php $op_profit=$gross_profit+$other_income-$op_expense-$other_expense;
                                    echo number_format(abs($op_profit),2);?></th>
                                <th><?php
                                    if($op_profit<0){
                                        echo "Cr";
                                    }elseif($op_profit>0){
                                        echo "Dr";
                                    }
                                    ?></th>
                            </tr>

                                <?php $sb=sub_type_by_id(11);
                                $finance_cost=0;
                                $account_head=account_head_by_subId($sb->SUB_HEAD_ID); ?>
                                <tr>
                                    <th><?= $sb->NAME; ?></th>
                                    <th><?= $sb->SUB_NO; ?></th>
                                    <?php
                                        $ac_balance=0;
                                        $db_transactions=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Debit");
                                        $cr_transactions=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Credit");
                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                            $ac_balance+=$db_transactions->amount+$cr_transactions->amount;
                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                            $ac_balance+=$db_transactions->amount-$cr_transactions->amount;
                                        }
                                        $finance_cost+=$ac_balance;
                                    ?>
                                    <th><?= number_format(abs($finance_cost),2);?></th>
                                    <th><?php
                                        if($finance_cost<0){
                                            echo "Cr";
                                        }elseif($finance_cost>0){
                                            echo "Dr</span>";
                                        }
                                        ?></th>
                                </tr>
                                <?php foreach ($account_head as $ah) {
                                    $head_balance = 0;
                                    $accounts = account_by_head($ah->H_ID);
                                    ?>
                                    <tr>
                                        <td><?= $ah->H_NAME; ?></td>
                                        <td><?= $ah->H_NO; ?></td>
                                        <?php /*foreach ($accounts as $acc) {*/
                                            $ac_bal = 0;
                                            $db_transactions = transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Debit");
                                            $cr_transactions = transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Credit");
                                            if ($sb->HEAD_TYPE == 'Liabilities' || $sb->HEAD_TYPE == 'Equity' || $sb->HEAD_TYPE == 'Income') {
                                                $ac_bal += $db_transactions->amount+$cr_transactions->amount;
                                            } elseif ($sb->HEAD_TYPE == 'Assets' || $sb->HEAD_TYPE == 'Expense') {
                                                $ac_bal += $db_transactions->amount-$cr_transactions->amount;
                                            }
                                            $head_balance += $ac_bal;
                                        /*}*/ ?>
                                        <td><?= number_format(abs($head_balance), 2);?></td>
                                        <td><?php
                                            if ($head_balance < 0) {
                                                echo "Cr";
                                            } elseif ($head_balance > 0) {
                                                echo "Dr";
                                            }
                                            ?></td>
                                    </tr>
                                    <?php foreach ($accounts as $account){
                                        $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")";
                                        $account_balance=0;
                                        if($account->A_SELFHEAD_ID==$ac->A_ID){ ?>
                                            <tr>
                                                <td><?= $account->A_NAME.$client; ?></td>
                                                <td><?= $account->A_NO; ?></td>
                                                <?php
                                                $debit_transactions=transaction_date_by_self($account->A_ID,$start_date,$end_date,"Debit");
                                                $credit_transactions=transaction_date_by_self($account->A_ID,$start_date,$end_date,"Credit");
                                                if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                    $account_balance+=$debit_transactions->amount+$credit_transactions->amount;
                                                }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                    $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                                }
                                                ?>
                                                <td><?= number_format(abs($account_balance),2);?></td>
                                                <td><?php
                                                    if($account_balance<0){
                                                        echo "Cr";
                                                    }elseif($account_balance>0){
                                                        echo "Dr";
                                                    }?></td>
                                            </tr>
                                            <?php foreach ($accounts as $acc){
                                                $client = ($acc->CLIENT_ID == "")?'':" (".client_name($acc->CLIENT_ID).")";
                                                $account_balance=0;
                                                if($acc->A_SELFHEAD_ID==$account->A_ID){ ?>
                                                    <tr>
                                                        <td><?= $acc->A_NAME.$client; ?></td>
                                                        <td><?= $acc->A_NO; ?></td>
                                                        <?php
                                                        $debit_transactions=transaction_date_by_self($acc->A_ID,$start_date,$end_date,"Debit");
                                                        $credit_transactions=transaction_date_by_self($acc->A_ID,$start_date,$end_date,"Credit");
                                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                            $account_balance+=$debit_transactions->amount+$credit_transactions->amount;
                                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                            $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                                        }
                                                        ?>
                                                        <td><?= number_format(abs($account_balance),2);?></td>
                                                        <td><?php
                                                            if($account_balance<0){
                                                                echo "Cr";
                                                            }elseif($account_balance>0){
                                                                echo "Dr";
                                                            }?></td>
                                                    </tr>
                                                    <?php foreach ($accounts as $acct){
                                                        $client = ($acct->CLIENT_ID == "")?'':" (".client_name($acct->CLIENT_ID).")";
                                                        $account_balance=0;
                                                        if($acct->A_SELFHEAD_ID==$acc->A_ID){ ?>
                                                            <tr>
                                                                <td><?= $acct->A_NAME.$client; ?></td>
                                                                <td><?= $acct->A_NO; ?></td>
                                                                <?php
                                                                $debit_transactions=transaction_date_by_self($acct->A_ID,$start_date,$end_date,"Debit");
                                                                $credit_transactions=transaction_date_by_self($acct->A_ID,$start_date,$end_date,"Credit");
                                                                if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                                    $account_balance+=$debit_transactions->amount+$credit_transactions->amount;
                                                                }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                                    $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                                                }
                                                                ?>
                                                                <td><?= number_format(abs($account_balance),2);?></td>
                                                                <td><?php
                                                                    if($account_balance<0){
                                                                        echo "Cr";
                                                                    }elseif($account_balance>0){
                                                                        echo "Dr";
                                                                    }?></td>
                                                            </tr>
                                                        <?php }
                                                    } ?>
                                                <?php }
                                            } ?>
                                        <?php }
                                    }
                                }?>

                            <tr>
                                <th>Profit/Loss before Tax</th>
                                <th></th>
                                <th><?php $profit_before_tax=$op_profit-$finance_cost;
                                    echo number_format(abs($profit_before_tax),2);?></th>
                                <th><?php
                                    if($profit_before_tax<0){
                                        echo "Cr";
                                    }elseif($profit_before_tax>0){
                                        echo "Dr";
                                    }
                                    ?></th>
                            </tr>

                                <?php $sb=sub_type_by_id(12);
                                $income_tax=0;
                                $account_head=account_head_by_subId($sb->SUB_HEAD_ID); ?>
                                <tr>
                                    <th><?= $sb->NAME; ?></th>
                                    <th><?= $sb->SUB_NO; ?></th>
                                    <?php
                                        $ac_balance=0;
                                        $db_transactions=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Debit");
                                        $cr_transactions=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Credit");
                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                            $ac_balance+=$db_transactions->amount+$cr_transactions->amount;
                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                            $ac_balance+=$db_transactions->amount-$cr_transactions->amount;
                                        }
                                        $income_tax+=$ac_balance;
                                    ?>
                                    <th><?= number_format(abs($income_tax),2);?></th>
                                    <th><?php
                                        if($income_tax<0){
                                            echo "Cr";
                                        }elseif($income_tax>0){
                                            echo "Dr";
                                        }
                                        ?></th>
                                </tr>
                                <?php foreach ($account_head as $ah) {
                                    $head_balance = 0;
                                    $accounts = account_by_head($ah->H_ID);
                                    ?>
                                    <tr>
                                        <td><?= $ah->H_NAME; ?></td>
                                        <td><?= $ah->H_NO; ?></td>
                                        <?php /*foreach ($accounts as $acc) {*/
                                            $ac_bal = 0;
                                            $db_transactions = transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Debit");
                                            $cr_transactions = transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Credit");
                                            if ($sb->HEAD_TYPE == 'Liabilities' || $sb->HEAD_TYPE == 'Equity' || $sb->HEAD_TYPE == 'Income') {
                                                $ac_bal += $db_transactions->amount+$cr_transactions->amount;
                                            } elseif ($sb->HEAD_TYPE == 'Assets' || $sb->HEAD_TYPE == 'Expense') {
                                                $ac_bal += $db_transactions->amount-$cr_transactions->amount;
                                            }
                                            $head_balance += $ac_bal;
                                        /*}*/ ?>
                                        <td><?= number_format(abs($head_balance), 2);?></td>
                                        <td><?php
                                            if ($head_balance < 0) {
                                                echo "Cr";
                                            } elseif ($head_balance > 0) {
                                                echo "Dr";
                                            }
                                            ?></td>
                                    </tr>
                                    <?php foreach ($accounts as $account){
                                        $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")";
                                        $account_balance=0;
                                        if($account->A_SELFHEAD_ID==$ac->A_ID){ ?>
                                            <tr>
                                                <td><?= $account->A_NAME.$client; ?></td>
                                                <td><?= $account->A_NO; ?></td>
                                                <?php
                                                $debit_transactions=transaction_date_by_self($account->A_ID,$start_date,$end_date,"Debit");
                                                $credit_transactions=transaction_date_by_self($account->A_ID,$start_date,$end_date,"Credit");
                                                if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                    $account_balance+=$debit_transactions->amount+$credit_transactions->amount;
                                                }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                    $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                                }
                                                ?>
                                                <td><?= number_format(abs($account_balance),2);?></td>
                                                <td><?php
                                                    if($account_balance<0){
                                                        echo "Cr";
                                                    }elseif($account_balance>0){
                                                        echo "Dr";
                                                    }?></td>
                                            </tr>
                                            <?php foreach ($accounts as $acc){
                                                $client = ($acc->CLIENT_ID == "")?'':" (".client_name($acc->CLIENT_ID).")";
                                                $account_balance=0;
                                                if($acc->A_SELFHEAD_ID==$account->A_ID){ ?>
                                                    <tr>
                                                        <td><?= $acc->A_NAME.$client; ?></td>
                                                        <td><?= $acc->A_NO; ?></td>
                                                        <?php
                                                        $debit_transactions=transaction_date_by_self($acc->A_ID,$start_date,$end_date,"Debit");
                                                        $credit_transactions=transaction_date_by_self($acc->A_ID,$start_date,$end_date,"Credit");
                                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                            $account_balance+=$debit_transactions->amount+$credit_transactions->amount;
                                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                            $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                                        }
                                                        ?>
                                                        <td><?= number_format(abs($account_balance),2);?></td>
                                                        <td><?php
                                                            if($account_balance<0){
                                                                echo "Cr";
                                                            }elseif($account_balance>0){
                                                                echo "Dr";
                                                            }?></td>
                                                    </tr>
                                                    <?php foreach ($accounts as $acct){
                                                        $client = ($acct->CLIENT_ID == "")?'':" (".client_name($acct->CLIENT_ID).")";
                                                        $account_balance=0;
                                                        if($acct->A_SELFHEAD_ID==$acc->A_ID){ ?>
                                                            <tr>
                                                                <td><?= $acct->A_NAME.$client; ?></td>
                                                                <td><?= $acct->A_NO; ?></td>
                                                                <?php
                                                                $debit_transactions=transaction_date_by_self($acct->A_ID,$start_date,$end_date,"Debit");
                                                                $credit_transactions=transaction_date_by_self($acct->A_ID,$start_date,$end_date,"Credit");
                                                                if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                                    $account_balance+=$debit_transactions->amount+$credit_transactions->amount;
                                                                }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                                    $account_balance+=$debit_transactions->amount-$credit_transactions->amount;
                                                                }
                                                                ?>
                                                                <td><?= number_format(abs($account_balance),2);?></td>
                                                                <td><?php
                                                                    if($account_balance<0){
                                                                        echo "Cr";
                                                                    }elseif($account_balance>0){
                                                                        echo "Dr";
                                                                    }?></td>
                                                            </tr>
                                                        <?php }
                                                    } ?>
                                                <?php }
                                            } ?>
                                        <?php }
                                    }
                                }?>

                            <tr>
                                <th>Net Profit/Loss</th>
                                <th></th>
                                <th><?php $net_profit=$profit_before_tax-$income_tax;
                                    echo number_format(abs($net_profit),2);?></th>
                                <th><?php
                                    if($net_profit<0){
                                        echo "Cr";
                                    }elseif($net_profit>0){
                                        echo "Dr";
                                    }
                                    ?></th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <button class="btn btn-sm pull-right" style="color: #fff;background-color: #26a69a" id="btnExport">Export to Excel</button>
            </div>
        </div>
        <!-- /highlighting rows and columns -->


        <!-- Export to Excel-->
        <script>
            $("#btnExport").click(function (e) {
                window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('#export_table').html() ));
                e.preventDefault();
            });
        </script>
        <!-- /Export to Excel-->


        <!--Jquery Datepicker-->
        <script>
            $( function() {
                $( ".datepicker" ).datepicker({dateFormat: 'dd-mm-yy'});
            } );
        </script>
        <!--Jquery Datepicker-->