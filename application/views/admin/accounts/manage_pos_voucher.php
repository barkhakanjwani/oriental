<link href="<?php echo base_url() ?>asset/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?php echo base_url() ?>asset/js/bootstrap-toggle.min.js"></script>
<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
	<div class="row">
		<div class="col-lg-12">
            <form role="form" enctype="multipart/form-data" id="form" action="<?php echo base_url(); ?>admin/accounts/save_pos_voucher/<?= (!empty($voucher_info))?$voucher_info->E_ID:'' ?>" method="post" class="form-horizontal  ">
                <section class="panel panel-default">
                    <header class="panel-heading"><?= $title ?></header>
                    <?php
                    if(!empty($voucher_info)) {
                        $trans_account_info = $this->db->select('*')
                            ->from('transactions_meta')
                            ->where('T_ID',$voucher_info->T_ID)
                            ->get()->result();
                        ?>

                        <input type="hidden" name="trans_account_id" value="<?= $trans_account_info[0]->TM_ID ?>" />
                        <input type="hidden" name="bank_and_cash_id" value="<?= $trans_account_info[1]->TM_ID ?>" />
                        <input type="hidden" name="transaction_id" value="<?= $voucher_info->T_ID ?>" />
                        <?php
                    }
                    ?>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('reference_no') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="invoice_id" class="form-control select_box required_select mb-5" data-width="100%" id="invoice_id">
                                    <option value="">---</option>
                                    <?php if(!empty($all_invoices)):
                                        foreach ($all_invoices as $invoice): ?>
                                            <option value="<?php echo $invoice->invoices_id; ?>" <?php
                                            if(!empty($voucher_info)){
                                                echo ($voucher_info->INVOICES_ID == $invoice->invoices_id)?'selected':'';
                                            }
                                            ?>><?php echo $invoice->reference_no; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('client') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="text" name="name" id="name" readonly class="form-control required mb-5" value="<?= (!empty($voucher_info))?$voucher_info->name:'' ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('title') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="debit_account"  class="form-control required_select mb-5 heads select_box" id="debit_account" data-width="100%">
                                    <option value="">---</option>
                                    <?php
                                        if(!empty($voucher_info)) {
                                            $get_client_info = $this->invoice_model->check_by(array('client_id' => $voucher_info->client_id), 'tbl_client');
                                            $ap_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Liabilities', 'NAME' => 'Current Liabilities'), 'sub_heads');
                                            $ap_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $ap_head->SUB_HEAD_ID, 'H_NAME' => 'Account Payables'), 'accounts_head');
                                            $get_client_accounts = $this->invoice_model->check_by_all(array('SUB_HEAD_ID' => $ap_sub_head->SUB_HEAD_ID, 'H_ID' => $ap_sub_head->H_ID, 'CLIENT_ID' => $voucher_info->client_id), 'accounts');
                                            foreach ($get_client_accounts as $account) {
                                                ?>
                                                <option value="<?= $account->A_ID ?>" <?= ($voucher_info->DEBIT_ACCOUNT == $account->A_ID)?'selected':'' ?>><?= $account->A_NAME ?></option>
                                                <?php
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('payment_mode') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="payment_type" class="form-control select_box required_select" id="type" data-width="100%">
                                    <option value="1" <?php
                                    if (!empty($voucher_info)){
                                        echo ($voucher_info->EXPENSE_TYPE == 1)?'selected':'';
                                    } ?>>Cash in Hand</option>
                                    <option value="2" <?php
                                    if (!empty($voucher_info)){
                                        echo ($voucher_info->EXPENSE_TYPE == 2)?'selected':'';
                                    } ?>>Cash at Bank</option>
                                </select>
                            </div>
                        </div>
                        <div id="cash_at_bank" style="<?php
                        if(!empty($voucher_info)){
                            echo ($voucher_info->EXPENSE_TYPE == 2)?'display:block;':'display:none;';
                        }
                        else{
                            echo "display:none;";
                        }
                        ?>">
                            <?php
                            if(!empty($voucher_info)) {
                                $branch_info = $this->invoice_model->check_by(array('A_ID' => $voucher_info->CREDIT_ACCOUNT), 'branches');
                                $bank_info = $this->invoice_model->check_by(array('B_ID' => $branch_info->B_ID), 'banks');
                            }
                            ?>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Banks <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <select name="bank" class="form-control cash_at_bank select_box required_select" onchange="selectBranches(this.value)" style="width:100%;">
                                        <option value="">-</option>
                                        <?php foreach ($banks as $bank): ?>
                                            <option value="<?php echo $bank->B_ID; ?>" <?php
                                            if(!empty($voucher_info)) {
                                                if ($voucher_info->EXPENSE_TYPE == 2) {
                                                    echo ($bank->B_ID == $bank_info->B_ID) ? 'selected' : '';
                                                }
                                            }
                                            ?>><?php echo $bank->B_NAME; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div id="branches">
                                <?php
                                if(!empty($voucher_info)) {
                                        ?>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Branches <span
                                                        class="text-danger">*</span></label>
                                            <div class="col-lg-3">
                                                <select name="branch_id"
                                                        class="form-control cash_at_bank select_box"
                                                        onchange="getAccountId(this.value)">
                                                    <option value="">Choose Branch</option>
                                                    <?php
                                                    $branches = $this->invoice_model->check_by_all(array('B_ID' => $bank_info->B_ID), 'branches');
                                                    foreach ($branches as $branch) {
                                                        ?>
                                                        <option value="<?= $branch->BR_ID ?>" <?= ($branch->BR_ID == $branch_info->BR_ID) ? 'selected' : '' ?>><?= $branch->BR_NAME ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php
                                }
                                ?>
                            </div>
                            <input type="hidden" id="account_id" name="account_id" value="<?= (!empty($voucher_info))?$voucher_info->CREDIT_ACCOUNT:'' ?>" />
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= lang('cheque_payorder') ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <input type="text" name="chq_no"class="form-control required mb-5" value="<?= (!empty($voucher_info))?$voucher_info->CHEQUE_NO:'' ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('paid_to') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="text" name="paid_to" class="form-control required mb-5" value="<?= (!empty($voucher_info))?$voucher_info->PAID_TO:'' ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="number" name="amount" onkeyup="ch_amountWords.value=numberToEnglish(this.value)" class="form-control required mb-5" min="0" step="any"  value="<?= (!empty($voucher_info))?$voucher_info->AMOUNT:'' ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('amount_in_words') ?></label>
                            <div class="col-lg-3">
                                <input type="text" name="amountWords" id="ch_amountWords" class="form-control mb-5"  value="<?= (!empty($voucher_info))?$voucher_info->AMOUNT_IN_WORDS:'' ?>" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('date') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="text" name="date" class="form-control datepicker" autocomplete="off"  value="<?= (!empty($voucher_info))?$voucher_info->E_DATE:'' ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('description') ?></label>
                            <div class="col-lg-3">
                                <textarea name="desc" class="form-control mb-5"><?= (!empty($voucher_info))?$voucher_info->DESCRIPTION:'' ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"></label>
                            <div class="col-lg-4">
                                <button type="submit" class="btn btn-sm btn-success pull-right"><i class="fa fa-check"></i>
                                    <?php
                                    if(!empty($voucher_info)){
                                        echo lang('update_voucher');
                                    }else{
                                        echo lang('create_voucher');
                                    }
                                    ?>
                                </button>
                            </div>
                        </div>
                    </div>
				</section>
            </form>
		</div>
	</div>
<script type="text/javascript">
    $(document).on('change', "#invoice_id",function () {
        if ($(this).val().length > 0) {
            var value = $(this).val();
            $.ajax({
                url : "<?php echo base_url(); ?>admin/accounts/get_post_client_accounts",
                type : "POST",
                dataType : "json",
                data : {"invoice_id" : value},
                success :function(data){
                    $('#name').val(data.client_name);
                    $('.heads').empty();
                    var head = $('.heads');
                    for (var i = 0; i < data.client_accounts.length; i++) {
                        head.append('<option value=' + data.client_accounts[i].A_ID + '>' + data.client_accounts[i].A_NAME + '</option>');
                    }
                },
                error : function(data) {}
            });
        }
    });

    $('#type').on("change", function() {
        var type = $(this).val();
        if(type == '1'){
            $('#cash_at_bank').fadeOut(500, function() { $(this).hide(); });
            $('#branches').fadeOut(500, function() { $(this).hide(); });
            $(".cash_at_bank").attr("disabled","disabled");
        }
        else{
            $('#cash_at_bank').fadeIn(500).show();
            $(".cash_at_bank").removeAttr("disabled");
        }
    });
    function selectBranches(val) {
        var option="";
        if(val == '') {
            $('#branches').hide();
        }
        else{
            $.getJSON("<?php echo site_url('admin/accounts/ajax_select_branches') ?>" + "/" + val, function (result) {
                $.each(result, function (index, value) {
                    option = ('<option value="' + value.BR_ID + '">' + value.BR_NAME + '</option>')+option;
                });
                var drop = '<div class="form-group"><label class="col-md-4 control-label">Branches <span class="text-danger">*</span></label><div class="col-lg-3" id="branches"><select name="branch_id" class="form-control cash_at_bank select_box required_select" onchange="getAccountId(this.value)"><option value="">-</option>' + option + '</select></div></div>';
                $('#branches').html(drop).hide().fadeIn(500);
                $('.select_box').select2({});
                getAccountId();
            });
        }
    }

    function getAccountId(val){
        account_id=$('#account_id');
        $.getJSON( "<?php echo site_url('admin/accounts/ajax_edit_branch') ?>"+"/"+val, function(result) {
            account_id.val(result.A_ID);
        });
    }
</script>