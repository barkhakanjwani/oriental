<div class="content-wrapper" style="margin: auto;">
    <div class="content">
        <div class="row">
            <div class="col-sm-offset-2 col-sm-8">
                <div class="panel panel-flat thumbnail">
                    <div class="panel-heading no-print">
                        <div class="btn-group">
                            <button type="button" onclick="printDiv('PrintMe')" class="btn btn-default"><i class="fa fa-print"></i></button>
                            <script>
                                function printDiv(divName) {
                                    var printContents = document.getElementById(divName).innerHTML;
                                    var originalContents = document.body.innerHTML;
                                    document.body.innerHTML = printContents;
                                    window.print();
                                    document.body.innerHTML = originalContents;
                                }
                            </script>
                        </div>
                    </div>
                    <div id="PrintMe">
                        <style>
                            .table-main>tbody>tr>td,
                            .table-main>tfoot>tr>td,
                            .table-main>tr>td {
                                border: none;
                            }
                        </style>
                        <div class="panel-heading">
                            <div class="text-center">
                                <h1><?= config_item('company_name') ?></h1>
                                <h4></h4>
                                <h4>Job Expense Voucher</h4>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table table-main">
                                        <tr>
                                            <td style="font-size: 13px;width: 20%;"><b>V. No.</b></td>
                                            <td style="font-size: 13px;width: 30%;"><?= $voucher->voucher_id; ?></td>
                                            <td style="font-size: 13px;width: 20%;"><b>Date</b></td>
                                            <td style="font-size: 13px;width: 30%;"><?= date('d-m-Y',strtotime($voucher->payment_date)); ?></td>
                                        </tr>
                                        <?php if ($voucher->payment_method==2){
                                            $branch_info = $this->invoice_model->check_by(array('A_ID' => $voucher->credit_account), 'branches');
                                            $bank_info = $this->invoice_model->check_by(array('B_ID' => $branch_info->B_ID), 'banks');
                                            ?>
                                            <tr>
                                                <td style="font-size: 13px;"><b>Branch</b></td>
                                                <td style="font-size: 13px"><?php echo $bank_info->B_NAME.'  '.$branch_info->BR_NAME ?></td>
                                                <td style="font-size: 13px"><b>Cheque No</b></td>
                                                <td style="font-size: 13px"><?= $voucher->cheque_no ?></td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <th style="border: 1px solid black;">Job No.</th>
                                            <th colspan="2" style="border: 1px solid black;">Particulars</th>
                                            <th style="border: 1px solid black;text-align:right;">Amount</th>
                                        </tr>
                                        <?php
                                        $voucher_details = $this->invoice_model->check_by_all(array('voucher_id'=>$voucher->voucher_id), 'tbl_voucher_details');
                                        $total_amount=0;
                                        foreach($voucher_details as $voucher_detail) {
                                        $reference_no= $this->invoice_model->job_no_creation($voucher_detail->invoices_id);
                                        $total_amount += $voucher_detail->amount; ?>
                                            <tr>
                                                <td style="border: 1px solid black;"><?= $reference_no ?></td>
                                                <td colspan="2" style="border: 1px solid black;"><?= job_expense_title($voucher_detail->job_expense_id) ?></td>
                                                <td style="border: 1px solid black;text-align:right;"><?= number_format($voucher_detail->amount,2) ?></td>
                                            </tr>
                                            <?php
                                        } ?>
                                        <tr>
                                            <td colspan="3" style="border: 1px solid black;text-align:center"><b>
                                                    In Words : <?php echo $this->invoice_model->convert_number($total_amount); ?>
                                                </b></td>
                                            <td style="border: 1px solid black;text-align:right;"><b>
                                                    Total : <?= number_format($total_amount,2); ?>
                                                </b></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="" style="padding-top: 0px; margin-top: 50px;">
                            <table class="table table-main">
                                <tr align="center">
                                    <td>_________________________
                                        <br>
                                        <h4>Approved By</h4> </td>
                                    <td>_________________________
                                        <br>
                                        <h4>Paid By</h4> </td>
                                    <td>_________________________
                                        <br>
                                        <h4>Receiver Signature</h4> </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>