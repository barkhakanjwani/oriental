<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('payment_voucher') ?></h1>
<div class="row">
    <div class="col-lg-12">
        <form role="form" enctype="multipart/form-data" id="form_v" action="<?php echo base_url(); ?>admin/accounts/save_payment_voucher/<?= (!empty($voucher_info))?encrypt($voucher_info->PV_ID):'' ?>" method="post" class="form-horizontal  ">
            <section class="panel panel-default">
                <header class="panel-heading"><?= $title ?></header>
                <?php
                if(!empty($voucher_info)) {
                    ?>
                    <input type="hidden" name="transaction_id" value="<?= $voucher_info->T_ID ?>" />
                    <?php
                }
                ?>
                <div class="panel-body">
					<div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('payment_mode') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="payment_mode" class="form-control select_box" id="type" data-width="100%" required>
									<option value="">---</option>
                                    <option value="1" <?php
                                        if (!empty($voucher_info)){
                                            echo ($voucher_info->PV_PAYMENT_MODE == 1)?'selected':'';
                                        } ?>>Cash</option>
                                    <option value="2" <?php
                                        if (!empty($voucher_info)){
                                            echo ($voucher_info->PV_PAYMENT_MODE == 2)?'selected':'';
                                        } ?>>Bank</option>
                                </select>
                            </div>
                        </div>
                        <div id="cash_at_bank" style="<?php
                            if(!empty($voucher_info)){
                                echo ($voucher_info->PV_PAYMENT_MODE == 2)?'display:block;':'display:none;';
                            }
                            else{
                                echo "display:none;";
                            }
                        ?>">
                            <?php
                                if(!empty($voucher_info) && $voucher_info->PV_PAYMENT_MODE == 2) {
                                    $branch_info = $this->invoice_model->check_by(array('A_ID' => $voucher_info->CREDIT_ACCOUNT), 'branches');
                                    $bank_info = $this->invoice_model->check_by(array('B_ID' => $branch_info->B_ID), 'banks');
                                }
                            ?>
							<div class="form-group">
                                <label class="col-lg-4 control-label">Banks <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <select name="bank" class="form-control cash_at_bank select_box " onchange="selectBranches(this.value)" data-width="100%" required>
                                        <option value="">-</option>
                                        <?php foreach ($banks as $bank): ?>
											<option value="<?php echo $bank->B_ID; ?>" <?php
                                                if(!empty($voucher_info)) {
                                                    if ($voucher_info->PV_PAYMENT_MODE == 2) {
                                                        echo ($bank->B_ID == $bank_info->B_ID) ? 'selected' : '';
                                                    }
                                                }
                                            ?>><?php echo $bank->B_NAME; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div id="branches">
                                <?php
                                    if(!empty($voucher_info)) {
                                        ?>
										<div class="form-group">
                                        <label class="col-lg-4 control-label">Branches <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <select name="branch_id" class="form-control cash_at_bank select_box" onchange="getAccountId(this.value)" data-width="100%" required>
                                                <option value="">-</option>
                                                <?php
                                                    $branches = $this->invoice_model->check_by_all(array('B_ID' => $bank_info->B_ID), 'branches');
                                                    foreach ($branches as $branch) {
                                                        ?>
														<option value="<?= $branch->BR_ID ?>" <?= ($branch->BR_ID == $branch_info->BR_ID) ? 'selected' : '' ?>><?= $branch->BR_NAME ?></option>
                                                        <?php
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                        <?php
                                    }
                                ?>
                            </div>
                            <input type="hidden" id="account_id" name="account_id" value="<?= (!empty($voucher_info))?$voucher_info->CREDIT_ACCOUNT:'' ?>" />
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= lang('cheque_payorder') ?></label>
                                <div class="col-lg-3">
                                    <input type="text" name="chq_no" class="form-control mb-5" value="<?= (!empty($voucher_info))?$voucher_info->CHEQUE_NO:'' ?>">
                                </div>
                            </div>
                        </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><?= lang('paid_to') ?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <select name="staff_account"  class="form-control mb-5 heads select_box" id="staff_account" data-width="100%" required>
                                <option value="">---</option>
                                <?php
                                if(!empty($all_staffs)){
                                    foreach ($all_staffs as $staff) {
                                        ?>
                                        <option value="<?= $staff->account_id ?>" <?php
                                        if(!empty($voucher_info)) {
                                            echo ($voucher_info->PV_PAID_TO == $staff->account_id) ? 'selected' : '';
                                        }?>><?= $staff->staff_name ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <?php /*if (!empty($voucher_details)) {
                        $i=0;
                        foreach ($voucher_details as $details): */?><!--
                        <div id="add_old_multiple">
                            <input type="hidden" name="pcd_id[]" value="<?/*= $details->PVD_ID */?>">
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?/*= lang('title') */?> <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <select name="expense_account[]" class="form-control required_select mb-5 heads select_box" id="expense_account" data-width="100%" required>
                                        <option value="">---</option>
                                        <?php
/*                                        if (!empty($expenses)) {
                                            foreach ($expenses as $expense){ */?>
                                                <option value="<?/*= $expense->A_ID */?>" <?php
/*                                                if (!empty($details->A_ID)) {
                                                    echo ($details->A_ID == $expense->A_ID) ? 'selected' : '';
                                                } */?>><?/*= $expense->A_NAME */?></option>
                                                <?php
/*                                            }
                                        }
                                        */?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?/*= lang('amount') */?> <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <input type="number" name="amount[]" class="form-control mb-5" min="0" step="any" value="<?/*= (!empty($details->PVD_AMOUNT)) ? $details->PVD_AMOUNT : '' */?>" required>
                                </div>
                                <?php /*if ($i==0){ */?>
                                <div style="margin-top:5px;">
                                    <button type="button" class="btn btn-primary btn-xs" id="add_multiple" style="border-radius:12px;"><i class="fa fa-plus"></i></button>
                                </div>
                                <?php /*}else{ */?>
                                    <div style="margin-top:5px;">
                                        <button type="button" class="btn btn-danger btn-xs remCF" style="border-radius:12px;"><i class="fa fa-minus"></i></button>
                                    </div>
                                <?php
/*                                } */?>
                            </div>
                        </div>
                        <?php /*$i++;
                        endforeach;
                    }else{ */?>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?/*= lang('title') */?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="expense_account[]" class="form-control mb-5 heads select_box" id="expense_account" data-width="100%" required>
                                    <option value="">---</option>
                                    <?php
/*                                    if (!empty($expenses)) {
                                        foreach ($expenses as $expense) { */?>
                                            <option value="<?/*= $expense->A_ID */?>"><?/*= $expense->A_NAME */?></option>
                                            <?php
/*                                        }
                                    } */?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?/*= lang('amount') */?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="number" name="amount[]" class="form-control mb-5" min="0" step="any" required>
                            </div>
                            <div style="margin-top:5px;">
                                <button type="button" class="btn btn-primary btn-xs" id="add_multiple" style="border-radius:12px;"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <?php
/*                    }*/?>
                    <div id="add_new_multiple"></div>-->
					<div class="form-group">
						<label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label>
						<div class="col-lg-3">
							<input type="number" name="amount" class="form-control mb-5" min="0" step="any" value="<?= (!empty($voucher_info))?$voucher_info->PV_AMOUNT:'' ?>" required>
						</div>
					</div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><?= lang('date') ?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <input type="text" name="date" class="form-control datepicker" autocomplete="off"  value="<?= (!empty($voucher_info))?$voucher_info->PV_DATE:'' ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><?= lang('description') ?></label>
                        <div class="col-lg-3">
                            <textarea name="desc" class="form-control mb-5"><?= (!empty($voucher_info))?$voucher_info->PV_DESC:'' ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"></label>
                        <div class="col-lg-4">
                            <button type="submit" class="btn btn-sm btn-success pull-right"><i class="fa fa-check"></i>
                                <?php
                                if(!empty($voucher_info)){
                                    echo lang('update_voucher');
                                }else{
                                    echo lang('create_voucher');
                                }
                                ?>
                            </button>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>
</div>
<script type="text/javascript">
    $('#type').on("change", function() {
        var type = $(this).val();
        if(type == '1'){
            $('#cash_at_bank').fadeOut(500, function() { $(this).hide(); });
            $('#branches').fadeOut(500, function() { $(this).hide(); });
            $(".cash_at_bank").attr("disabled","disabled");
        }
        else{
            $('#cash_at_bank').fadeIn(500).show();
            $(".cash_at_bank").removeAttr("disabled");
        }
    });
    function selectBranches(val) {
        var option="";
        if(val == '') {
            $('#branches').hide();
        }
        else{
            $.getJSON("<?php echo site_url('admin/accounts/ajax_select_branches') ?>" + "/" + val, function (result) {
                $.each(result, function (index, value) {
                    option = ('<option value="' + value.BR_ID + '">' + value.BR_NAME + '</option>')+option;
                });
                var drop = '<div class="form-group"><label class="col-md-4 control-label">Branches <span class="text-danger">*</span></label><div class="col-lg-3" id="branches"><select name="branch_id" class="form-control cash_at_bank select_box" onchange="getAccountId(this.value)" required><option value="">-</option>' + option + '</select></div></div>';
                $('#branches').html(drop).hide().fadeIn(500);
                $('.select_box').select2({});
                getAccountId();
            });
        }
    }

    function getAccountId(val){
        account_id=$('#account_id');
        $.getJSON( "<?php echo site_url('admin/accounts/ajax_get_branch_aid') ?>"+"/"+val, function(result) {
            (result != null)?account_id.val(result.A_ID):'';
        });
    }

    $("#add_multiple").click(function() {
        var option = " <?php
            foreach ($expenses as $expense) {
                echo "<option value=$expense->A_ID >$expense->A_NAME</option>";
            }
            ?>";
        var columns = '<div class="bg-warning"><div class="form-group"><label class="col-lg-4 control-label"><?= lang('title') ?> <span class="text-danger">*</span></label><div class="col-lg-3">';
        columns += '<select name="expense_account[]"  class="form-control mb-5 heads select_box" id="expense_account" data-width="100%" required>';
        columns += '<option value="">---</option>'+option;
        columns += '</select></div></div>';
        columns += '<div class="form-group"><label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label><div class="col-lg-3">';
        columns += '<input type="number" name="amount[]" class="form-control required mb-5" min="0" step="any" required></div>';
        columns += '<div style="margin-top:5px;"><button type="button" class="btn btn-danger btn-xs remCF" style="border-radius:12px;"><i class="fa fa-minus"></i></button></div></div>';
        var add_new = $(columns);
        $("#add_new_multiple").append(add_new.hide().fadeIn(1000));
        $( '.select_box' ).select2({});
    });
    $("#add_new_multiple").on('click', '.remCF', function() {
        $(this).fadeOut(300, function() { $(this).parent().parent().parent().remove(); })
    });
    $(".remCF").on('click', function() {
        $(this).fadeOut(300, function() { $(this).parent().parent().parent().remove(); })
    });
</script>