<!-- style form css -->
<style>
    .redborder1{
        border:1px solid red;
    }
    .redborder2{
        border:1px solid red;
    }
</style>
<!-- /style form css -->
<!-- Content area -->
<div class="content">
    <!-- Highlighting rows and columns -->
    <div class="col-xs-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <?php echo form_open(base_url('admin/accounts/client_ledgers'),array('class'=>"form-horizontal")); ?>
                <div class="row">
                    <div class="col-xs-2">
                        <input type="text" name="start_date" class="form-control datepicker" placeholder="From Date" autocomplete="off" value="<?= (isset($start_date))?$start_date:'' ?>">
                    </div>
                    <div class="col-xs-2">
                        <input type="text" name="end_date" class="form-control datepicker" placeholder="To Date" autocomplete="off" value="<?= (isset($end_date))?$end_date:'' ?>">
                    </div>
                    <div class="col-xs-2">
                        <select name="client_id" class="form-control select_box" style="width: 100%" required>
                            <option value="">Choose Client</option>
                            <?php if(!empty($all_clients)):
                                foreach ($all_clients as $client): ?>
                                    <option value="<?php echo $client->client_id; ?>" <?php
                                    if(isset($client_id)){
                                        echo ($client_id == $client->client_id)?'selected':'';
                                    } ?>><?php echo $client->name; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                    <!--<div class="col-xs-2">
                        <label class="checkbox-inline">
                            <input type="checkbox" name="opening"> Opening Balance
                        </label>
                    </div>-->
                    <div class="col-xs-2">
                        <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> Search</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <div class="panel-body">
                <div class="row" id="export_table">
                    <?php if(isset($client_name)): ?>
                        <h4 class="text-center text-uppercase">Selected Client : <b class="text-danger"><?= $client_name ?></b></h4>
                    <?php endif; ?>
                    <table class="table table-bordered table-responsive table-hover" id="table">
                        <thead>
                        <tr style="border-top: 1px solid #dddddd;">
                            <th style="text-align: center;">Date</th>
                            <th>Job No.</th>
                            <th>Reference</th>
                            <th style="text-align: right;">Debit Amount</th>
                            <th style="text-align: right;">Credit Amount</th>
                            <th colspan="2" style="text-align: center;">Balance</th>
                        </tr>
                        <tr style="border-top: 1px solid #dddddd;">
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class="text-right">Debit</th>
                            <th class="text-right">Credit</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sum_credit=0; $sum_debit=0;$balance=0;$debit_remaining=0;$credit_remaining=0;
                        if (!empty($client_ledgers)){
                            foreach ($client_ledgers as $ledger){
								$job_info = $this->invoice_model->check_by(array('invoices_id'=> $ledger->invoice_id), 'tbl_invoices');
                                $in_qty=0; $out_qty=0;
                                if ($ledger->type=='Debit'){
                                    $in_qty=$ledger->amount;
                                    $balance+=$ledger->amount;
                                    $sum_debit+=$ledger->amount;
                                }elseif ($ledger->type=='Credit'){
                                    $out_qty=$ledger->amount;
                                    $balance-=$ledger->amount;
                                    $sum_credit+=$ledger->amount;
                                }
                                ?>
                                <tr>
                                    <td class="text-center"><?= date('d-m-Y',strtotime($ledger->date)) ?></td>
                                    <td><?= (!empty($job_info->reference_no))?$job_info->reference_no:'--' ?></td>
                                    <td><b><?= ($ledger->reference=='' && $ledger->prefix=='BL')?'Cash':($ledger->prefix.$ledger->bill_no.((!empty($ledger->bill_no))?'&nbsp;&nbsp;&nbsp;&nbsp;':'').$ledger->reference) ?></b></td>
                                    <td class="text-right"><?= ($in_qty!=0)?number_format(($in_qty),2):'' ?></td>
                                    <td class="text-right"><?= ($out_qty!=0)?number_format(($out_qty),2):'' ?></td>
                                    <td class="text-right"><?= ($balance > 0)?number_format(($balance),2):'' ?></td>
                                    <td class="text-right"><?= ($balance < 0)?number_format(abs($balance),2):'' ?></td>
                                </tr>
                            <?php }
                        }
                        ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="text-right"><b><?php echo ($sum_debit>0)?number_format($sum_debit,2):''; ?></b></td>
                            <td class="text-right"><b><?php echo ($sum_credit>0)?number_format($sum_credit,2):''; ?></b></td>
                            <td class="text-right"><b><?= ($balance>0)?number_format(abs($balance),2):'' ?></b></td>
                            <td class="text-right"><b><?= ($balance<0)?number_format(abs($balance),2):'' ?></b></td>
                        </tr>
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
                <button class="btn btn-sm pull-right" style="color: #fff;background-color: #26a69a" id="btnExport">Export to Excel</button>
            </div>
        </div>
        <!-- /highlighting rows and columns -->
<!-- Export to Excel-->
        <script>
            $("#btnExport").click(function (e) {
                window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('#export_table').html() ));
                e.preventDefault();
            });
        </script>
        <!-- /Export to Excel-->