<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
	<!-- Content area -->
    <h1 class="header-<?= config_item('sidebar_theme'); ?>" style="margin: 0px 0px 0px 15px;"><?= lang('cash_in_hand') ?></h1>
    <div class="content">
        <!-- Highlighting rows and columns -->
        <div class="panel panel-flat">
            <div class="panel panel-heading">
                <?php echo form_open(base_url('admin/accounts/add_inHand'),array('class'=>"form-horizontal",'id'=>'form')); ?>
                <div class="row">
                    <div class="col-md-12">
						<div class="form-group">
							<div class="col-sm-3">
								<label class="control-label">Opening balance</label>
								<input type="text" name="opening_balance" id="opening_balance" class="form-control" readonly>
							</div>
							<div class="col-sm-3">
								<label class="control-label">Closing balance</label>
								<input type="text" name="closing_balance" id="closing_balance" class="form-control" readonly>
							</div>
							<div class="col-sm-6">
                                <label class="control-label">Account <span class="text-danger">*</span></label>
                                <select name="account" class="form-control mb-5 filterAccounts" required>
                                </select>
                            </div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								<label class="control-label">Type <span class="text-danger">*</span></label> <br />
								<input type="radio" name="type" value="Debit" required>Debit &nbsp;&nbsp;&nbsp;
								<input type="radio" name="type"  value="Credit" required>Credit
							</div>
							<div class="col-sm-3">
								<label class="control-label">Date <span class="text-danger">*</span></label>
								<input type="text" name="date" id="c_date" onblur="opening(this.value)" onchange="opening(this.value)" class="form-control datepicker" value="<?php echo date('Y-m-d') ?>" autocomplete="off" required>
							</div>
                            <div class="col-sm-3">
                                <label class="control-label">Payee</label>
                                <input type="text" name="pay" class="form-control mb-5">
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label">Amount <span class="text-danger">*</span></label>
                                <input type="number" name="amount" class="form-control mb-5" min="0" step="any" required>
                            </div>
						</div>
						<div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label">Particular</label>
                                <textarea name="particular" class="form-control mb-5"></textarea>
                            </div>
						</div>
                        <div class="row col-sm-12">
							<div class="col-sm-12">
								<input type="submit" class="btn pull-right btn-success" id="btn_create"  value="Proceed"/>
							</div>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <div class="panel panel-body">
                <div class="row">
                    <table class="table table-bordered table-responsive table-hover DataTables" id="DataTables">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Debit Account</th>
                                <th>Credit Account</th>
                                <th>Pay</th>
                                <th>Amount</th>
                                <th>Particulars</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /highlighting rows and columns -->
        <!--For Opening & Closing Balance-->
        <script>
            $(document).ready(function () {
                opening($('#c_date').val());
            });
            function opening(val) {
                $.getJSON("<?php echo site_url('admin/accounts/select_opening') ?>"+"/"+1+"/"+val,function(res){
                    var td="";
                    $('#opening_balance').val(Math.abs(res.opening_balance)+"     "+(res.opening_balance<0?"Cr":"Dr"));
                    $('#closing_balance').val(Math.abs(res.closing_balance)+"     "+(res.closing_balance<0?"Cr":"Dr"));
                    $.each(res.data,function(index,value){
                        var delete_row ='return confirm("Are you sure you want to delete this?");';
                        td+="<tr><td>"+value.CB_DATE+"</td><td>"+value.debit_account+"</td><td>"+value.credit_account+"</td><td>"+value.CB_PAY+"</td><td>"+value.CB_AMOUNT+"</td><td>"+value.CB_PARTICULARS+"</td><td><a href='<?= base_url('admin/accounts/edit_cash_inHand') ?>"+"/"+value.CB_ID+"' class='btn btn-xs btn-primary'><i class='fa fa-edit'></i></a> <a href='<?= base_url('admin/accounts/delete_inHand/') ?>"+value.CB_ID+"' class='btn btn-danger btn-xs' title='Delete' data-toggle='tooltip' data-placement='top' onclick='"+delete_row+"'><i class='fa fa-trash'></i></a></td></tr>";
                    });
                    $.each(res.expense_data,function(index,value){
                        td+="<tr><td>"+value.E_DATE+"</td><td>"+value.DEBIT_NAME+"</td><td>"+value.CREDIT_NAME+"</td><td>"+value.PAID_TO+"</td><td>"+value.AMOUNT+"</td><td>"+value.DESCRIPTION+"</td><td></td></tr>";
                    });
                    $.each(res.ch,function(index,value){
                        td+="<tr><td>"+value.C_DATE+"</td><td>"+value.A_NAME+"</td><td>"+value.BR_NAME+"</td><td>"+value.C_PAY+"</td><td>"+value.C_AMOUNT+"</td><td>"+value.C_DESC+"</td><td></td></tr>";
                    });
                    $('#DataTables > tbody').empty().append(td);
                });
            }
        </script>
        <!--For Opening & Closing Balance-->