<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<!-- Content area -->
    <h1 class="header-<?= config_item('sidebar_theme'); ?>" style="margin: 0px 0px 0px 15px;"><?= lang('cash_at_bank') ?></h1>
    <div class="content">
        <!-- Highlighting rows and columns -->
        <div class="panel panel-flat">
            <div class="panel panel-heading">
                <?php echo form_open(base_url('admin/accounts/add_atBank'),array('class'=>"form-horizontal",'id'=>'form')); ?>
				<div class="row">
                    <div class="col-md-12">
                        <div class="row col-sm-12 mb-20">
                            <div class="col-sm-3">
                                <label class="control-label">Banks <span class="text-danger">*</span></label>
                                <select name="bank_name" id="bank_name" onchange="selectBranches(this.value)" class="form-control mb-5 select_box" required>
									<option value="">---</option>
                                    <?php if(!empty($banks)):
                                        foreach ($banks as $bank): ?>
											<option value="<?php echo $bank->B_ID; ?>"><?php echo $bank->B_NAME; ?></option>
                                        <?php
                                        endforeach;
                                    endif; ?>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label">Branch <span class="text-danger">*</span></label>
                                <select name="cast_atBank_Account" id="cast_atBank_Account" onchange="opening($('#c_date').val())" class="form-control mb-5" required>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label">Opening balance</label>
                                <input type="text" name="opening_balance" id="opening_balance" class="form-control" readonly>
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label">Closing balance</label>
                                <input type="text" name="closing_balance" id="closing_balance" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="row col-sm-12">
                            <div class="col-sm-3">
                                <label class="control-label">Date <span class="text-danger">*</span></label>
                                <input type="text" name="date" id="c_date" onblur="opening(this.value)" onchange="opening(this.value)" class="form-control datepicker" value="<?php echo date('Y-m-d') ?>" autocomplete="off" required>
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label">Type <span class="text-danger">*</span></label><br />
								<input type="radio" name="type" value="Debit" required>Debit &nbsp;&nbsp;&nbsp;
								<input type="radio" name="type"  value="Credit" required>Credit
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label">Account <span class="text-danger">*</span></label>
                                <select name="account" class="form-control mb-5 filterAccounts" required>
                                    <?php
                                        /*                                    if (!empty($accounts)){
                                                                                foreach ($accounts as $account){
                                                                                    $account1=accounts_by_self($account->A_ID);
                                                                                    $client_first = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")";
                                                                                    if (!empty($account1)){ */?><!--
                                                <optgroup label="<?/*= $account->A_NAME */?>">
                                                    <?php /*foreach ($account1 as $ac1){
                                                        $client = ($ac1->CLIENT_ID == "")?'':" (".client_name($ac1->CLIENT_ID).")";
                                                        $account2=accounts_by_self($ac1->A_ID);
                                                        if (!empty($account2)){ */?>
                                                            <optgroup label="<?/*= '&nbsp;&nbsp;&nbsp;'.$ac1->A_NAME */?>"><?php
                                        /*                                                            foreach ($account2 as $ac2){*/?>
                                                                <option value="<?php /*echo $ac2->A_ID; */?>"><?php /*echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$ac2->A_NAME; */?></option>
                                                            <?php /*} */?>
                                                            </optgroup>
                                                            <?php
                                        /*                                                        }else{ */?>
                                                            <option value="<?php /*echo $ac1->A_ID; */?>"><?php /*echo '&nbsp;&nbsp;&nbsp;'.$ac1->A_NAME.$client; */?></option>
                                                        <?php /*}
                                                     } */?>
                                                </optgroup>
                                                <?php
                                        /*                                            }else{ */?>
                                                <option value="<?php /*echo $account->A_ID; */?>"><?php /*echo $account->A_NAME.$client_first; */?></option>
                                            --><?php /*}
                                        }
                                    } */?>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label">Cheque No</label>
                                <input type="text" name="chq_no" class="form-control mb-5">
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label">Payee</label>
                                <input type="text" name="pay" class="form-control mb-5">
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label">Amount <span class="text-danger">*</span></label>
                                <input type="number" name="amount" class="form-control mb-5" min="0" step="any" required>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label">Particular</label>
                                <textarea name="particular" class="form-control mb-5"></textarea>
                            </div>
                        </div>
                        <div class="row col-sm-12">
							<div class="col-sm-12">
								<input  type="submit" class="btn pull-right btn-success" id="btn_create"  value="Proceed"/>
							</div>
                        </div>
                    </div>

                </div>
    
                <?php echo form_close(); ?>
            </div>
            <div class="panel panel-body">
                <div class="row">
                    <table class="table table-bordered table-responsive table-hover DataTables" id="DataTables">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Debit Account</th>
                            <th>Credit Account</th>
                            <th>Cheque No</th>
                            <th>Pay</th>
                            <th>Amount</th>
                            <th>Particulars</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
		<!-- /highlighting rows and columns -->
		<!-- Select Branch on bank basis -->
        <script type="text/javascript">
            /*$(document).ready(function () {
                selectBranches($('#bank_name').val());
            });*/
            function selectBranches(val) {
                $.getJSON( "<?php echo site_url('admin/accounts/ajax_select_branches') ?>"+"/"+val, function(result) {
                    $('#cast_atBank_Account').empty();
                    $.each(result, function (index, value) {
                        $('#cast_atBank_Account').append('<option value="' + value.A_ID + '">' + value.BR_NAME + '</option>');
                    });
                    $('#cast_atBank_Account').select2({});
                    opening($('#c_date').val());
                });
            }
        </script>
		<!-- /Select Branch on bank basis -->
	
		<!--For Opening & Closing Balance-->
        <script type="text/javascript">
			/*function encodeID(id){
                var result = jQuery.ajax({
                    type: "POST",
                    url: "",
                    data: { val: id }
				});
                return result;
            }*/
            function opening(val) {
                $.getJSON("<?php echo site_url('admin/accounts/select_opening') ?>"+"/"+$('#cast_atBank_Account').val()+"/"+val,function(res){
                    var td="";
                    $('#opening_balance').val(Math.abs(res.opening_balance)+"     "+(res.opening_balance<0?"Cr":"Dr"));
                    $('#closing_balance').val(Math.abs(res.closing_balance)+"     "+(res.closing_balance<0?"Cr":"Dr"));
                    $.each(res.data,function(index,value){
                        /*var id = encodeID(value.CB_ID);
                        id.done(function(msg) {
                            jQuery.parseJSON(msg);
                        });
                        console.log(id);*/
                        var delete_row ='return confirm("Are you sure you want to delete this?");';
                        td+="<tr><td>"+value.CB_DATE+"</td><td>"+value.debit_account+"</td><td>"+value.credit_account+"</td><td>"+value.CB_CHEQUENO+"</td><td>"+value.CB_PAY+"</td><td>"+value.CB_AMOUNT+"</td><td>"+value.CB_PARTICULARS+"</td><td><a href='<?= base_url('admin/accounts/edit_cash_atBank') ?>"+"/"+value.CB_ID+"' class='btn btn-xs btn-primary' title='Edit' data-toggle='tooltip' data-placement='top'><i class='fa fa-edit'></i></a> <a href='<?= base_url('admin/accounts/delete_atBank/') ?>"+value.CB_ID+"' class='btn btn-danger btn-xs' title='Delete' data-toggle='tooltip' data-placement='top' onclick='"+delete_row+"'><i class='fa fa-trash'></i></a></td></tr>";
                    });
                    $.each(res.expense_data,function(index,value){
                        td+="<tr><td>"+value.E_DATE+"</td><td>"+value.DEBIT_NAME+"</td><td>"+value.CREDIT_NAME+"</td><td>"+value.CHEQUE_NO+"</td><td>"+value.PAID_TO+"</td><td>"+value.AMOUNT+"</td><td>"+value.DESCRIPTION+"</td><td></td></tr>";
                    });
                    $.each(res.ch,function(index,value){
                        td+="<tr><td>"+value.C_DATE+"</td><td>"+value.A_NAME+"</td><td>"+value.BR_NAME+"</td><td>"+value.C_NO+"</td><td>"+value.C_PAY+"</td><td>"+value.C_AMOUNT+"</td><td>"+value.C_DESC+"</td><td></td></tr>";
                    });
                    $('#DataTables > tbody').empty().append(td);
                });
            }
        </script>
		<!--For Opening & Closing Balance-->