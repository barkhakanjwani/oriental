<div class="content-wrapper" style="margin: auto;">
    <div class="content">
        <div class="row">
            <div class="col-sm-offset-2 col-sm-8">
                <div class="panel panel-flat thumbnail">
                    <div class="panel-heading no-print">
                        <div class="btn-group">
                            <button type="button" onclick="printDiv('PrintMe')" class="btn btn-default"><i class="fa fa-print"></i></button>
                            <script>
                                function printDiv(divName) {
                                    var printContents = document.getElementById(divName).innerHTML;
                                    var originalContents = document.body.innerHTML;
                                    document.body.innerHTML = printContents;
                                    window.print();
                                    document.body.innerHTML = originalContents;
                                }
                            </script>
                        </div>
                    </div>
                    <div id="PrintMe">
                        <style>
                            .table-main>tbody>tr>td,
                            .table-main>tfoot>tr>td,
                            .table-main>tr>td {
                                border: none;
                            }
                        </style>
                        <div class="panel-heading">
                            <div class="text-center">
                                <h1><?= config_item('company_name') ?></h1>
                                <h4></h4>
                                <h4><?= ($advance_info->payment_method=='Cash')?'Cash':'Bank' ?> Receipt Voucher</h4>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table table-main">
                                        <tr>
                                            <td style="font-size: 13px;width: 20%;"><b>V. No.</b></td>
                                            <td style="font-size: 13px;width: 30%;"><?= $advance_info->voucher_no; ?></td>
                                            <td style="font-size: 13px;width: 20%;"><b>Date</b></td>
                                            <td style="font-size: 13px;width: 30%;"><?= date('d-m-Y',strtotime($advance_info->payment_date)); ?></td>
                                        </tr>
                                        <?php if ($advance_info->payment_method=='Bank'){
                                            $branch_info = $this->invoice_model->check_by(array('A_ID' => $advance_info->debit_account), 'branches');
                                            $bank_info = $this->invoice_model->check_by(array('B_ID' => $branch_info->B_ID), 'banks');
                                            ?>
                                            <tr>
                                                <td style="font-size: 13px;"><b>Branch</b></td>
                                                <td style="font-size: 13px"><?php echo $bank_info->B_NAME.'  '.$branch_info->BR_NAME ?></td>
                                                <td style="font-size: 13px"><b>Cheque No</b></td>
                                                <td style="font-size: 13px"><?= $advance_info->cheque_payorder ?></td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <th style="border: 1px solid black;"><?= ($advance_info->expense_type=='Others')?'Received From':'Client' ?></th>
                                            <th colspan="2" style="border: 1px solid black;">Particulars</th>
                                            <th style="border: 1px solid black;text-align:right;">Amount</th>
                                        </tr>
                                        <?php $total_amount=$advance_info->amount; ?>
                                        <tr>
                                            <td style="border: 1px solid black;"><?php
                                                if($advance_info->expense_type=='Others'){
                                                    echo read_subHead($advance_info->credit_account)->A_NAME;
                                                }elseif ($advance_info->expense_type=='Misc'){
                                                    $advance_d = $this->invoice_model->check_by(array('ap_id' => $advance_info->ap_id), 'tbl_advance_payments');
                                                    $client_info = $this->invoice_model->check_by(array('client_id' => $advance_d->client_id), 'tbl_client');
                                                    echo $client_info->name;
                                                }elseif ($advance_info->expense_type=='Job'){
                                                    $client_info = $this->invoice_model->check_by(array('client_id' => $advance_info->client_id), 'tbl_client');
                                                    echo $client_info->name;
                                                }
                                                ?></td>
                                            <td colspan="2" style="border: 1px solid black;"><?php
                                                if ($advance_info->expense_type=='Job') {
                                                    echo $this->invoice_model->job_no_creation($advance_info->invoices_id).'<br>';
                                                    if ($advance_info->payment_method=='Third Party'){
                                                        foreach ($advance_details as $adv_detail){
                                                            echo $adv_detail->apd_title;
                                                        }
                                                    }else{
                                                        echo $advance_info->description;
                                                    }
                                                }else{
                                                    echo $advance_info->description;
                                                }
                                                ?></td>
                                            <td style="border: 1px solid black;text-align:right;"><?php
                                                if ($advance_info->expense_type=='Job') {
                                                    echo '<br>';
                                                    if ($advance_info->payment_method=='Third Party'){
                                                        foreach ($advance_details as $adv_detail){
                                                            echo $adv_detail->apd_amount;
                                                        }
                                                    }else{
                                                        echo number_format($advance_info->amount,2);
                                                    }
                                                }else{
                                                    echo number_format($advance_info->amount,2);
                                                }
                                                 ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" style="border: 1px solid black;text-align:center"><b>
                                                    In Words : <?php echo $this->invoice_model->convert_number($total_amount); ?>
                                                </b></td>
                                            <td style="border: 1px solid black;text-align:right;"><b>
                                                    Total : <?= number_format($total_amount,2); ?>
                                                </b></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="" style="padding-top: 0px; margin-top: 50px;margin-right: 30px;">
                            <table class="table table-main">
                                <tr align="right">
                                    <td>_________________________
                                        <br>
                                        <h4>Receiver Signature</h4> </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>