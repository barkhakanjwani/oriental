<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('general_expense') ?></h1>
<div class="row">
    <div class="col-lg-12">
        <form role="form" enctype="multipart/form-data" id="form" action="<?php echo base_url(); ?>admin/accounts/save_exp_voucher/<?= (!empty($voucher_info))?$voucher_info->PC_ID:'' ?>" method="post" class="form-horizontal  ">
            <section class="panel panel-default">
                <header class="panel-heading"><?= $title ?></header>
                <?php
                if(!empty($voucher_info)) {
                    $trans_account_info = $this->db->select('*')
                        ->from('transactions_meta')
                        ->where('T_ID',$voucher_info->T_ID)
                        ->get()->result();
                    ?>
                    <input type="hidden" name="transaction_id" value="<?= $voucher_info->T_ID ?>" />
                    <?php
                }
                ?>

                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><?= lang('payment_mode') ?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <select name="payment_type" class="form-control select_box required_select" id="type" data-width="100%">
                                <option value="1" <?php
                                if (!empty($voucher_info)){
                                    echo ($voucher_info->payment_method == 1)?'selected':'';
                                } ?>>Cash</option>
                                <option value="2" <?php
                                if (!empty($voucher_info)){
                                    echo ($voucher_info->payment_method == 2)?'selected':'';
                                } ?>>Bank</option>
                            </select>
                        </div>
                    </div>
                    <div id="cash_at_bank" style="<?php
                    if(!empty($voucher_info)){
                        echo ($voucher_info->payment_method == 2)?'display:block;':'display:none;';
                    }
                    else{
                        echo "display:none;";
                    }
                    ?>">
                        <?php
                        if(!empty($voucher_info) && $voucher_info->payment_method == 2) {
                            $branch_info = $this->invoice_model->check_by(array('A_ID' => $voucher_info->credit_account), 'branches');
                            $bank_info = $this->invoice_model->check_by(array('B_ID' => $branch_info->B_ID), 'banks');
                        }
                        ?>
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Banks <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="bank" class="form-control cash_at_bank select_box required_select" onchange="selectBranches(this.value)" style="width:100%;">
                                    <option value="">-</option>
                                    <?php foreach ($banks as $bank): ?>
                                        <option value="<?php echo $bank->B_ID; ?>" <?php
                                        if(!empty($voucher_info)) {
                                            if ($voucher_info->payment_method == 2) {
                                                echo ($bank->B_ID == $bank_info->B_ID) ? 'selected' : '';
                                            }
                                        }
                                        ?>><?php echo $bank->B_NAME; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div id="branches">
                            <?php
                            if(!empty($voucher_info)) {
                                ?>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label">Branches <span class="text-danger">*</span></label>
                                    <div class="col-lg-3">
                                        <select name="branch_id" class="form-control cash_at_bank select_box" onchange="getAccountId(this.value)" data-width="100%">
                                            <option value="">-</option>
                                            <?php
                                            $branches = $this->invoice_model->check_by_all(array('B_ID' => $bank_info->B_ID), 'branches');
                                            foreach ($branches as $branch) {
                                                ?>
                                                <option value="<?= $branch->BR_ID ?>" <?= ($branch->BR_ID == $branch_info->BR_ID) ? 'selected' : '' ?>><?= $branch->BR_NAME ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <input type="hidden" id="account_id" name="account_id" value="<?= (!empty($voucher_info))?$voucher_info->CREDIT_ACCOUNT:'' ?>" />

                    </div>
                    <?php if (!empty($voucher_details)) {
                        $i=0;
                        foreach ($voucher_details as $details): ?>
                        <div id="add_old_multiple">
                            <input type="hidden" name="pcd_id[]" value="<?= $details->PCD_ID ?>">
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= lang('title') ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <select name="expense_account[]" class="form-control required_select mb-5 heads select_box" id="expense_account" data-width="100%">
                                        <option value="">---</option>
                                        <?php
                                        if (!empty($expenses)) {
                                            foreach ($expenses as $expense){ ?>
                                                <option value="<?= $expense->A_ID ?>" <?php
                                                if (!empty($details->A_ID)) {
                                                    echo ($details->A_ID == $expense->A_ID) ? 'selected' : '';
                                                } ?>><?= $expense->A_NAME ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <input type="number" name="amount[]" class="form-control required mb-5" min="0" step="any" value="<?= (!empty($details->PCD_AMOUNT)) ? $details->PCD_AMOUNT : '' ?>">
                                </div>
                                <?php if ($i==0){ ?>
                                <div style="margin-top:5px;">
                                    <button type="button" class="btn btn-primary btn-xs" id="add_multiple" style="border-radius:12px;"><i class="fa fa-plus"></i></button>
                                </div>
                                <?php }else{ ?>
                                    <div style="margin-top:5px;">
                                        <button type="button" class="btn btn-danger btn-xs remCF" style="border-radius:12px;"><i class="fa fa-minus"></i></button>
                                    </div>
                                <?php
                                } ?>
                            </div>
                        </div>
                        <?php $i++;
                        endforeach;
                    }else{ ?>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('title') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="expense_account[]" class="form-control required_select mb-5 heads select_box" id="expense_account" data-width="100%">
                                    <option value="">---</option>
                                    <?php
                                    if (!empty($expenses)) {
                                        foreach ($expenses as $expense) { ?>
                                            <option value="<?= $expense->A_ID ?>"><?= $expense->A_NAME ?></option>
                                            <?php
                                        }
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="number" name="amount[]" class="form-control required mb-5" min="0" step="any">
                            </div>
                            <div style="margin-top:5px;">
                                <button type="button" class="btn btn-primary btn-xs" id="add_multiple" style="border-radius:12px;"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <?php
                    }?>
                    <div id="add_new_multiple"></div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><?= lang('date') ?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <input type="text" name="date" class="form-control datepicker" autocomplete="off"  value="<?= (!empty($voucher_info))?$voucher_info->PC_DATE:'' ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><?= lang('description') ?></label>
                        <div class="col-lg-3">
                            <textarea name="desc" class="form-control mb-5"><?= (!empty($voucher_info))?$voucher_info->PC_DESC:'' ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"></label>
                        <div class="col-lg-4">
                            <button type="submit" class="btn btn-sm btn-success pull-right"><i class="fa fa-check"></i>
                                <?php
                                if(!empty($voucher_info)){
                                    echo lang('update_voucher');
                                }else{
                                    echo lang('create_voucher');
                                }
                                ?>
                            </button>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).on('change', "#invoice_id",function () {
        if ($(this).val().length > 0) {
            var value = $(this).val();
            $.ajax({
                url : "<?php echo base_url(); ?>admin/accounts/get_exp_client_accounts",
                type : "POST",
                dataType : "json",
                data : {"invoice_id" : value},
                success :function(data){
                    $('#name').val(data.client_name);
                    $('.heads').empty();
                    var head = $('.heads');
                    for (var i = 0; i < data.client_accounts.length; i++) {
                        head.append('<option value=' + data.client_accounts[i].A_ID + '>' + data.client_accounts[i].A_NAME + '</option>');
                    }
                },
                error : function(data) {}
            });
        }
    });

    $('#type').on("change", function() {
        var type = $(this).val();
        if(type == '1'){
            $('#cash_at_bank').fadeOut(500, function() { $(this).hide(); });
            $('#branches').fadeOut(500, function() { $(this).hide(); });
            $(".cash_at_bank").attr("disabled","disabled");
        }
        else{
            $('#cash_at_bank').fadeIn(500).show();
            $(".cash_at_bank").removeAttr("disabled");
        }
    });
    function selectBranches(val) {
        var option="";
        if(val == '') {
            $('#branches').hide();
        }
        else{
            $.getJSON("<?php echo site_url('admin/accounts/ajax_select_branches') ?>" + "/" + val, function (result) {
                $.each(result, function (index, value) {
                    option = ('<option value="' + value.BR_ID + '">' + value.BR_NAME + '</option>')+option;
                });
                var drop = '<div class="form-group"><label class="col-md-4 control-label">Branches <span class="text-danger">*</span></label><div class="col-lg-3" id="branches"><select name="branch_id" class="form-control cash_at_bank select_box" onchange="getAccountId(this.value)" required><option value="">-</option>' + option + '</select></div></div>';
                $('#branches').html(drop).hide().fadeIn(500);
                $('.select_box').select2({});
                getAccountId();
            });
        }
    }

    function getAccountId(val){
        account_id=$('#account_id');
        $.getJSON( "<?php echo site_url('admin/accounts/ajax_get_branch_aid') ?>"+"/"+val, function(result) {
            (result != null)?account_id.val(result.A_ID):'';
        });
    }

    $("#add_multiple").click(function() {
        var option = " <?php
            foreach ($expenses as $expense) {
                echo "<option value=$expense->A_ID >$expense->A_NAME</option>";
            }
            ?>";
        var columns = '<div class="bg-warning"><div class="form-group"><label class="col-lg-4 control-label"><?= lang('title') ?> <span class="text-danger">*</span></label><div class="col-lg-3">';
        columns += '<select name="expense_account[]"  class="form-control required_select mb-5 heads select_box" id="expense_account" data-width="100%">';
        columns += '<option value="">---</option>'+option;
        columns += '</select></div></div>';
        columns += '<div class="form-group"><label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label><div class="col-lg-3">';
        columns += '<input type="number" name="amount[]" class="form-control required mb-5" min="0" step="any"></div>';
        columns += '<div style="margin-top:5px;"><button type="button" class="btn btn-danger btn-xs remCF" style="border-radius:12px;"><i class="fa fa-minus"></i></button></div></div>';
        var add_new = $(columns);
        $("#add_new_multiple").append(add_new.hide().fadeIn(1000));
        $( '.select_box' ).select2({});
    });
    $("#add_new_multiple").on('click', '.remCF', function() {
        $(this).fadeOut(300, function() { $(this).parent().parent().parent().remove(); })
    });
    $(".remCF").on('click', function() {
        $(this).fadeOut(300, function() { $(this).parent().parent().parent().remove(); })
    });

    /*** AMOUNT IN WORDS ***/
    function numberToEnglish( n ) {

        var string = n.toString(), units, tens, scales, start, end, chunks, chunksLen, chunk, ints, i, word, words, and = '';

        /* Is number zero? */
        if( parseInt( string ) === 0 ) {
            return 'zero';
        }

        /* Array of units as words */
        units = [ '', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen' ];

        /* Array of tens as words */
        tens = [ '', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety' ];

        /* Array of scales as words */
        scales = [ '', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion', 'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quatttuor-decillion', 'quindecillion', 'sexdecillion', 'septen-decillion', 'octodecillion', 'novemdecillion', 'vigintillion', 'centillion' ];

        /* Split user arguemnt into 3 digit chunks from right to left */
        start = string.length;
        chunks = [];
        while( start > 0 ) {
            end = start;
            chunks.push( string.slice( ( start = Math.max( 0, start - 3 ) ), end ) );
        }

        /* Check if function has enough scale words to be able to stringify the user argument */
        chunksLen = chunks.length;
        if( chunksLen > scales.length ) {
            return '';
        }

        /* Stringify each integer in each chunk */
        words = [];
        for( i = 0; i < chunksLen; i++ ) {

            chunk = parseInt( chunks[i] );

            if( chunk ) {

                /* Split chunk into array of individual integers */
                ints = chunks[i].split( '' ).reverse().map( parseFloat );

                /* If tens integer is 1, i.e. 10, then add 10 to units integer */
                if( ints[1] === 1 ) {
                    ints[0] += 10;
                }

                /* Add scale word if chunk is not zero and array item exists */
                if( ( word = scales[i] ) ) {
                    words.push( word );
                }

                /* Add unit word if array item exists */
                if( ( word = units[ ints[0] ] ) ) {
                    words.push( word );
                }

                /* Add tens word if array item exists */
                if( ( word = tens[ ints[1] ] ) ) {
                    words.push( word );
                }

                /* Add 'and' string after units or tens integer if: */
                if( ints[0] || ints[1] ) {

                    /* Chunk has a hundreds integer or chunk is the first of multiple chunks */
                    if( ints[2] || ! i && chunksLen ) {
                        words.push( and );
                    }

                }

                /* Add hundreds word if array item exists */
                if( ( word = units[ ints[2] ] ) ) {
                    words.push( word + ' hundred' );
                }

            }

        }
        if(words != ''){
            return words.reverse().join( ' ' )+' only/=';
        }
        else{
            return "";
        }
    }
    /*** END AMOUNT IN WORDS ***/
</script>