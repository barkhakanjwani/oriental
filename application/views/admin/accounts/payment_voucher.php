<div class="content-wrapper" style="margin: auto;">
    <div class="content">
        <div class="row">
            <div class="col-sm-offset-2 col-sm-8">
                <div class="panel panel-flat thumbnail">
                    <div class="panel-heading no-print">
                        <div class="btn-group">
                            <button type="button" onclick="printDiv('PrintMe')" class="btn btn-default"><i class="fa fa-print"></i></button>
                            <script>
                                function printDiv(divName) {
                                    var printContents = document.getElementById(divName).innerHTML;
                                    var originalContents = document.body.innerHTML;
                                    document.body.innerHTML = printContents;
                                    window.print();
                                    document.body.innerHTML = originalContents;
                                }
                            </script>
                        </div>
                    </div>
                    <div id="PrintMe">
                        <style>
                            .table-main>tbody>tr>td,
                            .table-main>tfoot>tr>td,
                            .table-main>tr>td {
                                border: none;
                            }
                        </style>
                        <div class="panel-heading">
                            <div class="text-center">
                                <h1><?= config_item('company_name') ?></h1>
                                <h4></h4>
                                <h4><?= ($payment_voucher->PAYMENT_MODE==1)?'Cash':'Bank' ?> Payment Voucher</h4>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table table-main">
                                        <tr>
                                            <td style="font-size: 13px;width: 20%;"><b>V. No.</b></td>
                                            <td style="font-size: 13px;width: 30%;"><?= $payment_voucher->PV_VOUCHER_NO; ?></td>
                                            <td style="font-size: 13px;width: 20%;"><b>Date</b></td>
                                            <td style="font-size: 13px;width: 30%;"><?= date('d-m-Y',strtotime($payment_voucher->PV_DATE)); ?></td>
                                        </tr>
                                        <?php if ($payment_voucher->PAYMENT_MODE==2){
                                            $branch_info = $this->invoice_model->check_by(array('A_ID' => $payment_voucher->ACCOUNT_CREDIT), 'branches');
                                            $bank_info = $this->invoice_model->check_by(array('B_ID' => $branch_info->B_ID), 'banks');
                                            ?>
                                        <tr>
                                            <td style="font-size: 13px;"><b>Branch</b></td>
                                            <td style="font-size: 13px"><?php echo $bank_info->B_NAME.'  '.$branch_info->BR_NAME ?></td>
                                            <td style="font-size: 13px"><b>Cheque No</b></td>
                                            <td style="font-size: 13px"><?= $payment_voucher->PV_INSTRUMENT ?></td>
                                        </tr>
                                        <?php } ?>
                                        <tr>
                                            <th style="border: 1px solid black;">Paid To / By</th>
                                            <th colspan="2" style="border: 1px solid black;">Particulars</th>
                                            <th style="border: 1px solid black;text-align:right;">Amount</th>
                                        </tr>
                                        <?php $total_amount=$payment_voucher->PV_AMOUNT; ?>
                                            <tr>
                                                <td style="border: 1px solid black;"><?= $payment_voucher->paid_account ?></td>
                                                <td colspan="2" style="border: 1px solid black;"><?= $payment_voucher->PV_DESC ?></td>
                                                <td style="border: 1px solid black;text-align:right;"><?= number_format($payment_voucher->PV_AMOUNT,2) ?></td>
                                            </tr>
                                        <tr>
                                            <td colspan="3" style="border: 1px solid black;text-align:center"><b>
                                                    In Words : <?php echo $this->invoice_model->convert_number($total_amount); ?>
                                                </b></td>
                                            <td style="border: 1px solid black;text-align:right;"><b>
                                                    Total : <?= number_format($total_amount,2); ?>
                                                </b></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="" style="padding-top: 0px; margin-top: 50px;">
                            <table class="table table-main">
                                <tr align="center">
                                    <td>_________________________
                                        <br>
                                        <h4>Approved By</h4> </td>
                                    <td>_________________________
                                        <br>
                                        <h4>Paid By</h4> </td>
                                    <td>_________________________
                                        <br>
                                        <h4>Receiver Signature</h4> </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>