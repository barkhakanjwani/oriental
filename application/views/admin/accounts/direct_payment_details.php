
<section class="content-header">
    <?php
    $client_info = $this->invoice_model->check_by(array('client_id' => $advance_info->client_id), 'tbl_client');
    ?>
    <div class="row">
        <div class="col-sm-8">
        </div>
        <!--<div class="col-sm-4 pull-right">
            <a onclick="print_invoice('print_invoice_new')" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Print" class="btn btn-sm btn-danger pull-right"  >
                <i class="fa fa-print"></i>
            </a>
        </div>-->
    </div>
</section>
<section class="content">
    <!-- Start Display Details -->
    <!-- Main content -->
    <div class="row" >
        <section class="invoice" id="print_invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <img style="width: 60px;width: 60px;margin-top: -10px;margin-right: 10px;" src="<?= base_url() . config_item('invoice_logo') ?>" ><?= config_item('company_name') ?>
                    </h2>
                </div><!-- /.col -->
            </div>
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th colspan="4">Advance Payment</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td width="20%"><b><?= lang('reference_no') ?></b></td>
                            <td width="30%"><?= $advance_info->reference_no ?></td>
                            <td width="20%"><b><?= lang('client') ?></b></td>
                            <td width="30%" class="text-right"><?= $client_info->name ?></td>
                        </tr>

                        <tr>
                            <td><b><?= lang('advance_date') ?></b></td>
                            <td><?= strftime(config_item('date_format'), strtotime($advance_info->payment_date)) ?></td>
                            <td><b><?= lang('cheque_payorder') ?></b></td>
                            <td class="text-right"><?= $advance_info->cheque_payorder ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('amount') ?></b></td>
                            <td><?= number_format(($advance_info->amount),2) ?></td>
                            <td><b><?= lang('payment_mode') ?></b></td>
                            <td class="text-right"><?= 'Direct Payment' ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('description') ?></b></td>
                            <td colspan="3"><?= $advance_info->description ?></td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </section>
        <section style="display: none;" class="invoice" id="print_invoice_new">
            <?php $this->load->view('admin/accounts/pdf_direct_payment_details'); ?>
        </section>
    </div>

</section>
<script type="text/javascript">
    function print_invoice(print_invoice_new) {
        var printContents = document.getElementById(print_invoice_new).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>