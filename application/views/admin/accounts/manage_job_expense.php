<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('job_expenses') ?></h1>
	<div class="row">
		<div class="col-lg-12">
            <form role="form" enctype="multipart/form-data" id="form" action="<?php echo base_url(); ?>admin/accounts/save_job_expense/<?= (!empty($voucher_info))?$voucher_info->voucher_id:'' ?>" method="post" class="form-horizontal  ">
                <section class="panel panel-default">
                    <header class="panel-heading"><?= $title ?></header>
                    <?php
                    if(!empty($voucher_info)) {
                        ?>
                        <input type="hidden" name="transaction_id" value="<?= $voucher_info->transaction_id ?>" />
                        <!--<input type="hidden" name="transaction_id1" value="<?/*= $voucher_info->transaction_id1 */?>" />-->
                        <?php
                    }
                    ?>
                    <div class="panel-body">
                        <?php
                            if(!empty($voucher_info)) {
                                $voucher_details = $this->invoice_model->check_by_all(array('voucher_id' => $voucher_info->voucher_id), 'tbl_voucher_details');
                                if (!empty($voucher_details)) {
                                    $counter = 1;
                                    foreach($voucher_details as $voucher_detail) {
                                        $expenses_accounts=$this->accounts_model->expenses_accounts_by_invoice($voucher_detail->invoices_id);
                                        ?>
                                        <div>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"><?= lang('reference_no') ?> <span class="text-danger">*</span></label>
                                            <div class="col-lg-3">
                                                <select name="invoice_id[]" class="form-control select_box required_select mb-5" data-width="100%" onchange="selectExpensesAccounts(this.value,<?= $counter-1 ?>)">
                                                    <option value="">---</option>
                                                    <?php if (!empty($all_invoices)):
                                                        foreach ($all_invoices as $invoice): ?>
                                                            <option value="<?php echo $invoice->invoices_id; ?>" <?= ($voucher_detail->invoices_id == $invoice->invoices_id) ? 'selected' : ''; ?>><?php echo $this->invoice_model->job_no_creation($invoice->invoices_id); ?></option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"><?= lang('title') ?> <span class="text-danger">*</span></label>
                                            <div class="col-lg-3">
                                                <select name="job_expenses[]" id="expense_account<?= $counter-1 ?>" class="form-control required_select mb-5 heads select_box" data-width="100%">
                                                    <option value="Security Deposit" <?= $voucher_detail->job_expense_id == 0?'selected':'' ?>>Security Deposit</option>
                                                    <?php foreach ($job_expenses as $job_expense){ ?>
                                                        <option value="<?= $job_expense->job_expense_id ?>" <?= ($job_expense->job_expense_id==$voucher_detail->job_expense_id)?'selected':'' ?>><?= $job_expense->job_expense_title ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label>
                                            <div class="col-lg-3">
                                                <input type="number" name="amount[]" class="form-control required mb-5" min="0" step="any" value="<?= round($voucher_detail->amount,2) ?>">
                                            </div>
                                            <?php
                                                if($counter == 1) {
                                                    ?>
                                                    <div style="margin-top:5px;">
                                                        <button type="button" class="btn btn-primary btn-xs" id="add_multiple" style="border-radius:12px;"><i class="fa fa-plus"></i>
                                                        </button>
                                                    </div>
                                                    <?php
                                                }else {
                                                    ?>
                                                    <div style="margin-top:5px;">
                                                        <button type="button" class="btn btn-danger btn-xs remCF" style="border-radius:12px;"><i class="fa fa-minus"></i>
                                                        </button>
                                                    </div>
                                                    <?php
                                                }
                                                    ?>
                                        </div>
                                        </div>
                                        <?php
                                        $counter++;
                                    }
                                }
                            }
                            else {
                                ?>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label"><?= lang('reference_no') ?> <span class="text-danger">*</span></label>
                                    <div class="col-lg-3">
                                        <select name="invoice_id[]" class="form-control select_box required_select mb-5 invoices_ids" data-width="100%" onchange="job_expense_list()">
                                            <option value="">---</option>
                                            <?php if (!empty($all_invoices)):
                                                foreach ($all_invoices as $invoice): ?>
                                                    <option value="<?php echo $invoice->invoices_id; ?>"><?php echo $this->invoice_model->job_no_creation($invoice->invoices_id); ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label"><?= lang('title') ?> <span class="text-danger">*</span></label>
                                    <div class="col-lg-3">
                                        <select name="job_expenses[]" id="expense_account0" class="form-control required_select mb-5 heads select_box" data-width="100%">
                                            <option value="">---</option>

                                            <?php foreach ($job_expenses as $job_expense){ ?>
                                                <option value="<?= $job_expense->job_expense_id ?>"><?= $job_expense->job_expense_title ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label>
                                    <div class="col-lg-3">
                                        <input type="number" name="amount[]" class="form-control required mb-5" min="0" step="any">
                                    </div>
                                    <div style="margin-top:5px;">
                                        <button type="button" class="btn btn-primary btn-xs" id="add_multiple" style="border-radius:12px;"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                <?php
                            }
                        ?>
                        <div id="add_new_multiple"></div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('payment_mode') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="payment_type" class="form-control select_box required_select" id="type" data-width="100%">
                                    <option value="1" <?php
                                    if (!empty($voucher_info)){
                                        echo ($voucher_info->payment_method == 1)?'selected':'';
                                    } ?>>Cash</option>
                                    <option value="2" <?php
                                    if (!empty($voucher_info)){
                                        echo ($voucher_info->payment_method == 2)?'selected':'';
                                    } ?>>Bank</option>
                                </select>
                            </div>
                        </div>
                        <div id="cash_at_bank" style="<?php
                        if(!empty($voucher_info)){
                            echo ($voucher_info->payment_method == 2)?'display:block;':'display:none;';
                        }
                        else{
                            echo "display:none;";
                        }
                        ?>">
                            <?php
                            if(!empty($voucher_info) && $voucher_info->payment_method == 2) {
                                $branch_info = $this->invoice_model->check_by(array('A_ID' => $voucher_info->credit_account), 'branches');
                                $bank_info = $this->invoice_model->check_by(array('B_ID' => $branch_info->B_ID), 'banks');
                            }
                            ?>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Banks <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <select name="bank" class="form-control cash_at_bank select_box required_select" onchange="selectBranches(this.value)" style="width:100%;">
                                        <option value="">-</option>
                                        <?php foreach ($banks as $bank): ?>
                                            <option value="<?php echo $bank->B_ID; ?>" <?php
                                            if(!empty($voucher_info)) {
                                                if ($voucher_info->payment_method == 2) {
                                                    echo ($bank->B_ID == $bank_info->B_ID) ? 'selected' : '';
                                                }
                                            }
                                            ?>><?php echo $bank->B_NAME; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div id="branches">
                                <?php
                                if(!empty($voucher_info)) {
                                        ?>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Branches <span class="text-danger">*</span></label>
                                            <div class="col-lg-3">
                                                <select name="branch_id" class="form-control cash_at_bank select_box" onchange="getAccountId(this.value)" data-width="100%">
                                                    <option value="">-</option>
                                                    <?php
                                                    $branches = $this->invoice_model->check_by_all(array('B_ID' => $bank_info->B_ID), 'branches');
                                                    foreach ($branches as $branch) {
                                                        ?>
                                                        <option value="<?= $branch->BR_ID ?>" <?= ($branch->BR_ID == $branch_info->BR_ID) ? 'selected' : '' ?>><?= $branch->BR_NAME ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php
                                }
                                ?>
                            </div>
                            <input type="hidden" id="account_id" name="account_id" value="<?= (!empty($voucher_info))?$voucher_info->credit_account:'' ?>" />
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= lang('cheque_payorder') ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <input type="text" name="chq_no"class="form-control required mb-5" value="<?= (!empty($voucher_info))?$voucher_info->cheque_no:'' ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="paid_to" style="<?php
                        if(!empty($voucher_info)){
                            echo ($voucher_info->payment_method == 1)?'display:block;':'display:none;';
                        }
                        ?>">
                            <label class="col-lg-4 control-label"><?= lang('paid_to') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="staff_account"  class="form-control required_select mb-5 heads select_box" id="staff_account" data-width="100%">
                                    <option value="">---</option>
                                    <?php
                                    if(!empty($all_staffs)){
                                        foreach ($all_staffs as $staff){ ?>
                                            <option value="<?= $staff->account_id ?>" <?php
                                            if(!empty($voucher_info)) {
                                                echo ($voucher_info->paid_to == $staff->account_id) ? 'selected' : '';
                                            }?>><?= $staff->staff_name ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('date') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="text" name="date" class="form-control datepicker" autocomplete="off"  value="<?= (!empty($voucher_info))?$voucher_info->payment_date:'' ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('description') ?></label>
                            <div class="col-lg-3">
                                <textarea name="desc" class="form-control mb-5"><?= (!empty($voucher_info))?$voucher_info->description:'' ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"></label>
                            <div class="col-lg-4">
                                <button type="submit" class="btn btn-sm btn-success pull-right"><i class="fa fa-check"></i>
                                    <?php
                                    if(!empty($voucher_info)){
                                        echo lang('update_voucher');
                                    }else{
                                        echo lang('create_voucher');
                                    }
                                    ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row col-md-8 col-md-offset-2">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>
                                        <?= lang('job_no') ?>
                                    </th>
                                    <th>
                                        <?= lang('created_date') ?>
                                    </th>
                                    <th>
                                        <?= lang('title') ?>
                                    </th>
                                    <th>
                                        <?= lang('payment_date') ?>
                                    </th>
                                    <th>
                                        <?= lang('payment_method') ?>
                                    </th>
                                    <th>
                                        <?= lang('amount') ?>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="dynamic_rows">
                                </tbody>
                            </table>
                        </div>
                    </div>
				</section>
            </form>
		</div>
	</div>
<script type="text/javascript">
    var count=1;
    $("#add_multiple").click(function() {
        var status = " <?php
        foreach ($all_invoices as $invoice) {
            $a = $this->invoice_model->job_no_creation($invoice->invoices_id);
            echo "<option value=$invoice->invoices_id>$a</option>";
        }
        ?>";
        var columns = '<div class="bg-warning">';
        columns += '<div class="form-group">';
        columns += '<label class="col-lg-4 control-label">Job No <span class="text-danger">*</span></label>';
        columns += '<div class="col-lg-3">';
        columns += '<select name="invoice_id[]" class="form-control select_box required_select mb-5 invoices_ids" data-width="100%" onchange="job_expense_list()">';
        columns += '<option value="">---</option>'+status;
        columns += '</select>';
        columns += '</div>';
        columns += '</div>';
        columns += '<div class="form-group">';
        columns += '<label class="col-lg-4 control-label"><?= lang('title') ?> <span class="text-danger">*</span></label>';
        columns += '<div class="col-lg-3">';
        columns += '<select name="job_expenses[]" id="expense_account'+count+'"  class="form-control required_select mb-5 heads select_box" data-width="100%">';
        columns += '<option value="">---</option>';
        <?php foreach ($job_expenses as $job_expense){ ?>
        columns += '<option value="<?= $job_expense->job_expense_id ?>"><?= $job_expense->job_expense_title ?></option>';
        <?php } ?>
        columns += '</select>';
        columns += '</div>';
        columns += '</div>';
        columns += '<div class="form-group">';
        columns += '<label class="col-lg-4 control-label">Amount <span class="text-danger">*</span></label>';
        columns += '<div class="col-lg-3">';
        columns += '<input type="number" name="amount[]" class="form-control required mb-5" min="0" step="any" >';
        columns += '</div>';
        columns += '<div style="margin-top:5px;">';
        columns += '<button type="button" class="btn btn-danger btn-xs remCF" style="border-radius:12px;"><i class="fa fa-minus"></i></button>';
        columns += '</div>';
        columns += '</div>';
        columns += '</div>';
        var add_new = $(columns);
        $("#add_new_multiple").append(add_new.hide().fadeIn(1000));
        $( '.select_box' ).select2({});
        count++;
    });
    $("#add_new_multiple").on('click', '.remCF', function() {
        $(this).fadeOut(300, function() { $(this).parent().parent().parent().remove(); })
    });
    $(".remCF").on('click', function() {
        $(this).fadeOut(300, function() { $(this).parent().parent().parent().remove(); })
    });
</script>
<script type="text/javascript">
    $('#type').on("change", function() {
        var type = $(this).val();
        if(type == '1'){
            $('#paid_to').fadeIn(500).show();
            $('#cash_at_bank').fadeOut(500, function() { $(this).hide(); });
            $('#branches').fadeOut(500, function() { $(this).hide(); });
            $(".cash_at_bank").attr("disabled","disabled");
        }
        else{
            $('#paid_to').fadeOut(500, function() { $(this).hide(); });
            $('#cash_at_bank').fadeIn(500).show();
            $(".cash_at_bank").removeAttr("disabled");
        }
    });
    function selectBranches(val) {
        var option="";
        if(val == '') {
            $('#branches').hide();
        }
        else{
            $.getJSON("<?php echo site_url('admin/accounts/ajax_select_branches') ?>" + "/" + val, function (result) {
                $.each(result, function (index, value) {
                    option = ('<option value="' + value.BR_ID + '">' + value.BR_NAME + '</option>')+option;
                });
                var drop = '<div class="form-group"><label class="col-md-4 control-label">Branches <span class="text-danger">*</span></label><div class="col-lg-3" id="branches"><select name="branch_id" class="form-control cash_at_bank select_box required_select" onchange="getAccountId(this.value)"><option value="">-</option>' + option + '</select></div></div>';
                $('#branches').html(drop).hide().fadeIn(500);
                $('.select_box').select2({});
                getAccountId();
            });
        }
    }
    function selectExpensesAccounts(val,count) {
        var exp_accounts=$('#expense_account'+count);
        $.getJSON("<?php echo base_url('admin/accounts/expenses_accounts_invoice') ?>/"+val, function (result) {
            var option="";
            exp_accounts.empty();
            option += '<option value="">----</option><option value="Security Deposit">Security Deposit</option>';
            $.each(result, function (index, value) {
                if(value.name != '1st Duty & Taxes' && value.name != '2nd Duty & Taxes + (If No Invoice fine)' && value.name != '1st CESS' && value.name != '2nd CESS')
                {
                    option += '<option value="' + value.id + '">' + value.name + '</option>';
                }
            });
            exp_accounts.append(option);
            $('.select_box').select2({});
        });
    }

    function getAccountId(val){
        account_id=$('#account_id');
        $.getJSON( "<?php echo site_url('admin/accounts/ajax_get_branch_aid') ?>"+"/"+val, function(result) {
            (result != null)?account_id.val(result.A_ID):'';
        });
    }

    function job_expense_list() {
        var ids='';
        $('.invoices_ids').each(function (index,value) {
            ids += $(this).val()+',';
        });
        $.ajax({
            url : "<?= base_url('admin/accounts/job_expenses_by_invoice'); ?>",
            type : "POST",
            dataType : "json",
            data : {"invoices_ids" : ids},
            success : function(data) {
                console.log(data);
                $('#dynamic_rows').empty();
                $('#dynamic_rows').append(data);
            },
            error : function(data) {
                console.log('error'+data);
            }
        });
    }
</script>