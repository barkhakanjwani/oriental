<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<section class="content-header">
    <div class="row">
        <div class="col-md-12">
			<span class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('list_banks') ?></span> <a href="#" data-toggle="modal" data-target="#add_banks" class="btn btn-danger btn-xs" style="vertical-align: text-bottom;"><?= lang('add_new') ?></a>
		</div>
    </div>
</section>
	<!-- Content area -->
    <div class="content">
        <!-- Highlighting rows and columns -->
        <div class="panel panel-flat">
            <table class="table table-bordered table-hover DataTables " id="DataTables">
                <thead>
                <tr>
                    <th>Name</th>
                    <th class="text-center">Logo</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php if(!empty($banks)): ?>
                    <?php foreach ($banks as $bank){ ?>
						<tr style="white-space: nowrap;">
                            <td><?php echo $bank->B_NAME; ?></td>
                            <td class="text-center"><img src="<?= (!empty($bank->B_LOGO))?base_url().'upload/'.$bank->B_LOGO:base_url().'uploads/default_avatar.jpg'; ?>" height="30" width="30"></td>
                            <td class="text-center"><?php echo ($bank->B_STATUS==1)?"<label class='label label-success'>Active</label>":"<label class='label label-danger'>Inactive</label>"; ?></td>
                            <td class="text-center">
                                <button type="button" onclick="getEditBank(this.value)" value="<?php echo encrypt($bank->B_ID);?>" data-popup="tooltip" title="" data-container="body" data-original-title="Edit Bank" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#" ><i class="fa fa-edit"></i></button>
								<!--<button type="button" value="<?php /*echo $bank->B_ID;*/?>" data-popup="tooltip" title="" data-container="body" data-original-title="Delete Bank" class="btn btn-danger btn-xs delete"><i class="fa fa-trash"></i></button>-->
                            </td>
                        </tr>
                    <?php } ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
		<!-- /highlighting rows and columns -->
		<!-- Add Bank modal -->
        <div id="add_banks" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h6 class="modal-title">Add Bank</h6>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open_multipart(base_url('admin/accounts/add_banks'),array('class'=>"form-horizontal",'id'=>'form')); ?>
						<div class="modal-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
										<div class="col-sm-12">
                                            <label class="control-label">Name <span class="text-danger">*</span></label>
                                            <input type="text" name="ba_name" placeholder="Name" class="form-control mb-5" required>
                                        </div>
                                        <div class="col-sm-12">
                                            <label class="control-label">Logo</label>
                                            <input type="file" name="ba_image" class="form-control mb-5" size="20">
                                        </div>
                                        <div class="col-sm-12">
                                            <label class="control-label">Status <span class="text-danger">*</span></label>
                                            <select placeholder="Status" name="ba_status" class="form-control select_box" data-width="100%" required>
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input  type="submit" name="btn_addBank" class="btn btn-primary"  value="Add Bank"/>
                        </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
		<!-- /Add Tm -->
		<!-- Edit Bank modal -->
        <div id="edit_bank" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h6 class="modal-title">Edit Bank</h6>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open_multipart(base_url('admin/accounts/edit_bank'),array('class'=>"form-horizontal form")); ?>
						<div class="modal-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
										<input type="hidden" name="B_ID" id="b_id">
                                        <input type="hidden" name="A_ID" id="a_id">
                                        <div class="col-sm-12">
                                            <label class="control-label">Name <span class="text-danger">*</span></label>
                                            <input type="text" name="B_NAME" id="b_name" placeholder="Name" class="form-control mb-5" required>
                                        </div>
                                        <div class="col-sm-12">
                                            <label class="control-label">Logo</label>
                                            <input type="file" name="B_LOGO" id="b_logo" class="form-control mb-5" size="20">
                                            <input type="hidden" name="hdn_logo_name" id="hdn_logo_name">
                                        </div>
                                        <div class="col-sm-12">
                                            <label class="control-label">Status <span class="text-danger">*</span></label>
                                            <Select name="B_STATUS" id="b_status" class="form-control select_box" data-width="100%" required>
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input  type="submit" name="btn_editBank" class="btn btn-primary"  value="Edit Bank"/>
                        </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
		<!-- /Edit Bank Modal -->
		<!-- edit select Bank id script -->
        <script type='text/javascript'>
            function getEditBank(id)
            {
                $('document').ready(function(){
                    var controlmodal=$('#edit_bank');
                    var option={
                        "backdrop" : "static"
                    }
                    controlmodal.modal(option);
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url("admin/accounts/ajax_edit_bank");?>'+"/"+id,
                        data:false,
                        contentType: false,
                        dataType:"json",
                        success:function(data){
                            $('#b_id').val(data['B_ID']);
                            $('#b_name').val(data['B_NAME']);
                            $('#hdn_logo_name').val(data['B_LOGO']);
                            $('#b_status').val(data['B_STATUS']);
                            $('#b_status').select2({});
                            $('#a_id').val(data['A_ID']);
                        },
                        error:function(data){
                            console.log(data.responseText + "not work")
                        },
                    });
                });
            }
        </script>
		<!-- edit select Bank id script -->
		<!--Delete Bank Script-->
        <script type="text/javascript">
            $('.delete').click(function () {
                if(confirm("Are you sure you want to delete this bank?")){
                    $.ajax({
                        type:"POST",
                        url:"<?php echo base_url('admin/accounts/delete_bank'); ?>"+"/"+$(this).val(),
                        dataType:'JSON',
                        success:function(data){
                            if(data>0){
                                setTimeout(function(){location.href="<?php echo base_url('admin/accounts/banks'); ?>"} , 2000);
                                $.jGrowl('Bank has been Deleted', {
                                    header: 'Successfully',
                                    theme: 'bg-danger alert-styled-left alert-styled-check'
                                });
                            }
                        }
                    });
                }else {
                    return false;
                }
            });
        </script>
		<!--/Delete Bank Script-->