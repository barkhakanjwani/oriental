<section class="content-header">
    <div class="row">
        <div class="col-md-12">
			<span class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('general_journal_list') ?></span> <a href="#" data-toggle="modal" data-target="#add_entries" class="btn btn-danger btn-xs" style="vertical-align: text-bottom;"><?= lang('add_new') ?></a>
		</div>
    </div>
</section>
    <!-- Content area -->
    <div class="content">
        <!-- Highlighting rows and columns -->
        <!--<div class="panel panel-flat">
            <div class="panel-heading">
                <?php /*echo form_open(base_url('admin/accounts/general_journal'),array('class'=>"form-horizontal", "id"=>"form_label")); */?>
                <div class="row">
                    <div class="col-xs-2">
                        <input type="text" name="date" class="form-control datepicker" placeholder="Date" autocomplete="off" value="<?/*= (!empty($date))?$date:'' */?>" required>
                    </div>
                    <div class="col-xs-2">
                        <button class="btn btn-sm btn-success"><i class="fa fa-search"></i></button>
                    </div>
                </div>
                <?php /*echo form_close(); */?>
            </div>
            <div class="panel-body">
                <div class="row">
                    <table class="table table-bordered table-responsive table-hover">
                        <thead>
                        <tr>
                            <th width="20%">Date</th>
                            <th width="50%">Description</th>
                            <th width="10%">Debit</th>
                            <th width="10%">Credit</th>
                            <th width="10%">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php /*if(!empty($entries)): */?>
                            <?php /*foreach ($entries as $entry){ */?>
                                <tr>
                                    <?php /*$debit_transactions=read_debit_transactions($entry->T_ID);
                                    $credit_transactions=read_credit_transactions($entry->T_ID);
                                    */?>
                                    <td><?php /*echo date('d-m-Y',strtotime($entry->T_DATE)); */?></td>
                                    <td>
                                        <?php /*foreach ($debit_transactions as $d){ */?>
                                            <span style="float:left;"><?php /*echo $d->A_NAME; */?></span><br>
                                        <?php /*} */?>
                                        <?php /*foreach ($credit_transactions as $c){ */?>
                                            <span style="float:right;"><?php /*echo $c->A_NAME; */?></span><br>
                                        <?php /*} */?>
                                        <?php /*foreach ($debit_transactions as $d){
                                            if($d->PARTICULARS!=""):
                                                */?>
                                                <span style="float:left;"><?php /*echo $d->PARTICULARS; */?></span><br>
                                            <?php /*endif;
                                        } */?>
                                        <?php /*foreach ($credit_transactions as $c){
                                            if($c->PARTICULARS!=""):
                                                */?>
                                                <span style="float:left;"><?php /*echo $c->PARTICULARS; */?></span><br>
                                            <?php /*endif;
                                        } */?>
                                    </td>
                                    <td style="vertical-align: top;">
                                        <?php /*foreach ($debit_transactions as $d){ */?>
                                            <span style="float:left;"><?php /*echo number_format($d->TM_AMOUNT,2); */?></span><br>
                                        <?php /*} */?>
                                    </td>
                                    <td style="vertical-align: top;">
                                        <?php /*foreach ($debit_transactions as $d){ */?>
                                            <br>
                                        <?php /*} */?>
                                        <?php /*foreach ($credit_transactions as $c){ */?>
                                            <span style="float:right;"><?php /*echo number_format($c->TM_AMOUNT,2); */?></span><br>
                                        <?php /*} */?>
                                    </td>
                                    <td>
                                        <button class="btn btn-xs btn-primary" onclick="getEditEntries(<?/*= $entry->T_ID; */?>)" ><i class="fa fa-edit"></i></button>
                                    </td>
                                </tr>
                            <?php /*} */?>
                        <?php /*endif; */?>
                        </tbody>
                    </table>
                </div>
                    <button class="btn btn-sm pull-right" style="color: #fff;background-color: #26a69a" id="btnExport">Export to Excel</button>
            </div>
        </div>-->
        <!-- /highlighting rows and columns -->

        <!-- Add General Journal modal -->
        <div id="add_entries" class="modal fade">
            <div class="modal-dialog modal-full">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h6 class="modal-title">Create General Journal</h6>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open(base_url('admin/accounts/add_entries'),array('class'=>"form-horizontal",'id'=>'form')); ?>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row sm-12 mb-20">
                                            <div class="col-sm-2">
                                                <label class="control-label">Date <span class="text-danger">*</span></label>
                                                <input type="text" name="date" class="form-control datepicker" required>
                                            </div>
                                        </div>
                                        <div class="row col-sm-6">
											<div class="add_debit_account_value">
												<div class="col-sm-12">
													<label class="control-label"><b>Debit Account</b></label>
												</div>
												<div class="col-sm-5">
													<label class="control-label">Account <span class="text-danger">*</span></label>
													<select name="debit_account[]" class="form-control select_box" data-width="100%" required>
														<?php if(!empty($accounts)):
															foreach ($accounts as $account):
                                                                $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")";
                                                                ?>
																<option value="<?php echo $account->A_ID; ?>"><?php echo $account->A_NAME.$client; ?></option>
															<?php endforeach; ?>
														<?php endif; ?>
													</select>
												</div>
												<div class="col-sm-3">
													<label class="control-label">Particular</label>
													<input type="text" name="debit_particular[]" class="form-control">
												</div>
												<div class="col-sm-3">
													<label class="control-label">Amount <span class="text-danger">*</span></label>
													<input type="number" name="debit_amount[]" onkeyup="sum_debit_amount()" onblur="sum_debit_amount()" class="form-control debit_amount" min="0" step="any" required>
												</div>
												<div class="col-sm-1">
													 <label class="control-label">Add</label>
												   <button type="button" class="btn btn-primary btn-icon btn-sm add_debit_account"><i class="fa fa-plus"></i></button>
												</div>
											</div>
											<div class="col-sm-12 mt-20 text-center">
												<label class="control-label">Total Debit Amount</label>
												<input type="text" name="total_debit" id="total_debit_amount" class="form-control" readonly>
											</div>
											
										</div>
                                        <div class="row col-sm-6" style="float:right;">
											<div class="add_credit_account_value">
												<div class="col-sm-12">
													<label class="control-label"><b>Credit Account</b></label>
												</div>
												<div class="col-sm-5">
													<label class="control-label">Account <span class="text-danger">*</span></label>
													<select name="credit_account[]" class="form-control select_box" data-width="100%" required>
														<?php if(!empty($accounts)):
															foreach ($accounts as $account):
                                                                $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")";
                                                                ?>
																<option value="<?php echo $account->A_ID; ?>"><?php echo $account->A_NAME.$client; ?></option>
															<?php endforeach; ?>
														<?php endif; ?>
													</select>
												</div>
												 <div class="col-sm-3">
													<label class="control-label">Particular</label>
													<input type="text" name="credit_particular[]" class="form-control">
												</div>
												<div class="col-sm-3">
													<label class="control-label">Amount <span class="text-danger">*</span></label>
													<input type="number" name="credit_amount[]" onkeyup="sum_credit_amount()" onblur="sum_credit_amount()" class="form-control credit_amount" min="0" step="any" required>
												</div>
												<div class="col-sm-1">
													 <label class="control-label">Add</label>
												   <button type="button" class="btn btn-primary btn-icon btn-sm add_credit_account"><i class="fa fa-plus"></i></button>
												</div>
											</div>
											<div class="col-sm-12 mt-20 text-center">
												<label class="control-label">Total Credit Amount</label>
												<input type="text" name="total_credit_amount" id="total_credit_amount" class="form-control" readonly>
											</div>
                                        </div>
										
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input  type="submit" class="btn btn-primary" id="btn_create"  value="Create General Journal" disabled/>

                        </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Add General Journal modal-->
		<!--Start toggle For Add more General Journal Debit Account-->
		<script type="text/javascript">
		   $('.add_debit_account').on('click', function() {
				$('.add_debit_account_value').append('<div class="exercise rotateIn animated"><div class="col-sm-5"> <label class="control-label">Account</label> <select name="debit_account[]" class="form-control select_box" data-width="100%" required><?php if(!empty($accounts)):foreach ($accounts as $account): $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")";?><option value="<?php echo $account->A_ID; ?>"><?php echo $account->A_NAME.$client; ?></option><?php endforeach; ?> <?php endif; ?> </select></div><div class="col-sm-3"> <label class="control-label">Particular</label>  <input type="text" name="debit_particular[]" class="form-control required mb-5"> </div><div class="col-sm-3"> <label class="control-label">Amount</label> <input type="number" name="debit_amount[]"  onkeyup="sum_debit_amount()" onblur="sum_debit_amount()" class="form-control required mb-5 debit_amount" min="0" step="any"></div><div class="col-md-1" style="margin-top:24px;"><div class="form-group text-center"><button onclick="$(this).parent().parent().parent().slideUp(function(){ $(this).remove(); sum_debit_amount(); }); return false" class="remove btn btn-danger btn-icon btn-sm"><i class="fa fa-minus"></i></button></div></div></div>');
               	$( '.select_box' ).select2({});
				return false;
			});
		</script>
		<!--End toggle For Add more General Journal Debit Account-->
		
		<!--Start toggle For Add more General Journal Credit Account-->
		<script type="text/javascript">
            $('.add_credit_account').on('click', function() {
				$('.add_credit_account_value').append('<div class="exercise rotateIn animated"><div class="col-sm-5"> <label class="control-label">Account</label> <select name="credit_account[]" class="form-control select_box" data-width="100%" required><?php if(!empty($accounts)):foreach ($accounts as $account): $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")"; ?><option value="<?php echo $account->A_ID; ?>"><?php echo $account->A_NAME.$client; ?></option><?php endforeach; ?> <?php endif; ?> </select></div><div class="col-sm-3"> <label class="control-label">Particular</label>  <input type="text" name="credit_particular[]" class="form-control required mb-5"> </div><div class="col-sm-3"> <label class="control-label">Amount</label> <input type="number" name="credit_amount[]" onkeyup="sum_credit_amount()" onblur="sum_credit_amount()" class="form-control required mb-5 credit_amount" min="0" step="any"></div><div class="col-md-1" style="margin-top:24px;"><div class="form-group text-center"><button onclick="$(this).parent().parent().parent().slideUp(function(){ $(this).remove(); sum_credit_amount(); }); return false" class="remove btn btn-danger btn-icon btn-sm"><i class="fa fa-minus"></i></button></div></div></div>');
                $( '.select_box' ).select2({});
				return false;
			});
		</script>
		<!--End toggle For Add more General Journal Credit Account-->

        <!-- Edit General Journal modal -->
        <div id="edit_entries" class="modal fade">
            <div class="modal-dialog modal-full">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h6 class="modal-title">Edit General Journal</h6>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open(base_url('admin/accounts/update_entries'),array('class'=>"form-horizontal form")); ?>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="hidden" name="tr_id" id="tr_id">
                                        <div class="row sm-12 mb-20">
                                            <div class="col-sm-2">
                                                <label class="control-label">Date <span class="text-danger">*</span></label>
                                                <input type="text" name="date" id="tr_date" class="form-control datepicker" autocomplete="off" required>
                                            </div>
                                        </div>
                                        <div class="row col-sm-6">
                                            <div class="edit_debit_account_value">
                                                <div class="col-sm-12">
                                                    <label class="control-label"><b>Debit Account</b></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 mt-20 text-center">
                                                <label class="control-label">Total Debit Amount</label>
                                                <input type="text" name="total_debit" id="total_debit_amount1" class="form-control" readonly>
                                            </div>
                                        </div>
                                        <div class="row col-sm-6" style="float:right;">
                                            <div class="edit_credit_account_value">
                                                <div class="col-sm-12">
                                                    <label class="control-label"><b>Credit Account</b></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 mt-20 text-center">
                                                <label class="control-label">Total Credit Amount</label>
                                                <input type="text" name="total_credit_amount" id="total_credit_amount1" class="form-control" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input  type="submit" class="btn btn-primary" id="btn_edit"  value="Edit General Journal"/>
                        </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Edit General Journal modal-->

        <!-- edit select General Journal id script -->
        <script type='text/javascript'>
            function getEditEntries(id) {
                var controlmodal = $('#edit_entries');
                var option = {
                    "backdrop": "static"
                }
                controlmodal.modal(option);
                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url("admin/accounts/ajax_edit_entries");?>' +"/"+ id,
                    data: false,
                    contentType: false,
                    dataType: "json",
                    success: function (data) {
                        $('#tr_id').val(id);
                        $('#tr_date').val(data.tr['T_DATE']);
                        $('.edit_debit_account_value').empty();
                        $('.edit_credit_account_value').empty();
                        var debit_sum = 0;
                        var credit_sum = 0;
                        var row = '';
                        $.each(data.tr_debit, function (index, value) {
                            if (index == 0) {
                                row += '<div><input type="hidden" name="trmd_id[]" value="'+value.TM_ID+'" ><div class="col-sm-5"><label class="control-label">Account <span class="text-danger">*</span></label><select name="debit_account[]" id="debit_account'+index+'" class="form-control select_box" data-width="100%" required><?php foreach ($accounts as $account){ $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")"; ?><option value="<?php echo $account->A_ID; ?>" ><?php echo $account->A_NAME.$client; ?></option><?php } ?></select></div><div class="col-sm-3"><label class="control-label">Particular</label><input type="text" name="debit_particular[]" class="form-control" value="'+value.PARTICULARS+'"></div><div class="col-sm-3"><label class="control-label">Amount <span class="text-danger">*</span></label><input type="number" name="debit_amount[]" onkeyup="sum_debit_amount()" onblur="sum_debit_amount()" class="form-control debit_amount1" min="0" step="any"  value="'+value.TM_AMOUNT+'" required></div><div class="col-sm-1"><label class="control-label">Add</label><button type="button" class="btn btn-primary btn-icon btn-sm" onclick="edit_more_debit_rows(); return false;"><i class="fa fa-plus"></i></button></div></div></div>';
                            } else if (index > 0) {
                                row += '<div><input type="hidden" name="trmd_id[]" value="'+value.TM_ID+'" ><div class="col-sm-5"><label class="control-label">Account <span class="text-danger">*</span></label><select name="debit_account[]" id="debit_account'+index+'" class="form-control select_box" data-width="100%" required><?php foreach ($accounts as $account){ $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")"; ?><option value="<?php echo $account->A_ID; ?>"><?php echo $account->A_NAME.$client; ?></option><?php } ?></select></div><div class="col-sm-3"><label class="control-label">Particular</label><input type="text" name="debit_particular[]" class="form-control" value="'+value.PARTICULARS+'"></div><div class="col-sm-3"><label class="control-label">Amount <span class="text-danger">*</span></label><input type="number" name="debit_amount[]" onkeyup="sum_debit_amount()" onblur="sum_debit_amount()" class="form-control debit_amount1" min="0" step="any"  value="'+value.TM_AMOUNT+'" required></div><div class="col-sm-1"><label class="control-label">Add</label><button style="margin-top: 8px;" onclick="delete_entry('+value.TM_ID+'); return false" id="btn_remove'+value.TM_ID+'" class="remove btn btn-danger btn-sm"><i class="fa fa-minus"></i></button></div></div></div>';
                            }
                            debit_sum += +value.TM_AMOUNT;
                        });
                        $('#total_debit_amount1').val(debit_sum);
                        $('.edit_debit_account_value').append(row);

                        $.each(data.tr_debit, function (index, value) {
                            $('#debit_account'+index).val(value.A_ID);
                        });

                        var row1 = '';
                        $.each(data.tr_credit, function (index, value) {
                            if (index == 0) {
                                row1 += '<div><input type="hidden" name="trmc_id[]" value="'+value.TM_ID+'" ><div class="col-sm-5"><label class="control-label">Account <span class="text-danger">*</span></label><select name="credit_account[]" id="credit_account'+index+'" class="form-control select_box" data-width="100%" required><?php if(!empty($accounts)):foreach ($accounts as $account): $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")"; ?><option value="<?php echo $account->A_ID; ?>"><?php echo $account->A_NAME.$client; ?></option><?php endforeach; ?><?php endif; ?></select> </div> <div class="col-sm-3"> <label class="control-label">Particular</label> <input type="text" name="credit_particular[]" class="form-control" value="'+value.PARTICULARS+'"></div> <div class="col-sm-3"> <label class="control-label">Amount <span class="text-danger">*</span></label> <input type="number" name="credit_amount[]" onkeyup="sum_credit_amount()" onblur="sum_credit_amount()" class="form-control credit_amount1" min="0" step="any" value="'+value.TM_AMOUNT+'" required> </div> <div class="col-sm-1"> <label class="control-label">Add</label> <button type="button" class="btn btn-primary btn-icon btn-sm" onclick="edit_more_credit_rows(); return false;"><i class="fa fa-plus"></i></button> </div> </div></div>';
                            } else if (index > 0) {
                                row1 += '<div><input type="hidden" name="trmc_id[]" value="'+value.TM_ID+'" ><div class="col-sm-5"><label class="control-label">Account <span class="text-danger">*</span></label><select name="credit_account[]" id="credit_account'+index+'" class="form-control select_box" data-width="100%" required><?php if(!empty($accounts)):foreach ($accounts as $account): $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")"; ?><option value="<?php echo $account->A_ID; ?>"><?php echo $account->A_NAME.$client; ?></option><?php endforeach; ?><?php endif; ?></select> </div> <div class="col-sm-3"> <label class="control-label">Particular</label> <input type="text" name="credit_particular[]" class="form-control" value="'+value.PARTICULARS+'"></div> <div class="col-sm-3"> <label class="control-label">Amount <span class="text-danger">*</span></label> <input type="number" name="credit_amount[]" onkeyup="sum_credit_amount()" onblur="sum_credit_amount()" class="form-control credit_amount1" min="0" step="any" value="'+value.TM_AMOUNT+'" required> </div> <div class="col-sm-1"> <label class="control-label">Add</label> <button style="margin-top: 8px;" onclick="delete_entry('+value.TM_ID+'); return false" id="btn_remove'+value.TM_ID+'" class="remove btn btn-danger btn-sm"><i class="fa fa-minus"></i></button></div></div></div>';
                            }
                            credit_sum += +value.TM_AMOUNT;
                        });
                        $('#total_credit_amount1').val(credit_sum);
                        $('.edit_credit_account_value').append(row1);
                        $( '.select_box' ).select2({});

                        $.each(data.tr_credit, function (index, value) {
                            $('#credit_account'+index).val(value.A_ID);
                        });
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            }
        </script>
        <!-- edit select General Journal id script -->
        <!--Start toggle For Edit more General Journal Debit Account-->
        <script type="text/javascript">
            function edit_more_debit_rows() {
                $('.edit_debit_account_value').append('<div class="exercise rotateIn animated"><div class="col-sm-5"> <label class="control-label">Account <span class="text-danger">*</span></label> <select name="debit_account[]" class="form-control select_box" data-width="100%" required><?php if(!empty($accounts)):foreach ($accounts as $account): $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")"; ?><option value="<?php echo $account->A_ID; ?>"><?php echo $account->A_NAME.$client; ?></option><?php endforeach; ?> <?php endif; ?> </select></div><div class="col-sm-3"> <label class="control-label">Particular</label>  <input type="text" name="debit_particular[]" class="form-control require mb-5"> </div><div class="col-sm-3"> <label class="control-label">Amount</label> <input type="number" name="debit_amount[]"  onkeyup="sum_debit_amount()" onblur="sum_debit_amount()" class="form-control require mb-5 debit_amount1" min="0" step="any"></div><div class="col-md-1" style="margin-top:24px;"><div class="form-group  text-center"><button onclick="$(this).parent().parent().parent().slideUp(function(){ $(this).remove(); sum_debit_amount(); }); return false" class="remove btn btn-danger btn-sm"><i class="fa fa-minus"></i></button></div></div></div>');
                $( '.select_box' ).select2({});
            }
        </script>
        <!--End toggle For Edit more General Journal Debit Account-->

        <!--Start toggle For Edit more General Journal Credit Account-->
        <script type="text/javascript">
            function edit_more_credit_rows() {
                $('.edit_credit_account_value').append('<div class="exercise rotateIn animated"><div class="col-sm-5"> <label class="control-label">Account <span class="text-danger">*</span></label> <select name="credit_account[]" class="form-control select_box" data-width="100%" required><?php if(!empty($accounts)):foreach ($accounts as $account): $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")"; ?><option value="<?php echo $account->A_ID; ?>"><?php echo $account->A_NAME.$client; ?></option><?php endforeach; ?> <?php endif; ?> </select></div><div class="col-sm-3"> <label class="control-label">Particular</label>  <input type="text" name="credit_particular[]" class="form-control require mb-5"> </div><div class="col-sm-3"> <label class="control-label">Amount</label> <input type="number" name="credit_amount[]" onkeyup="sum_credit_amount()" onblur="sum_credit_amount()" class="form-control require mb-5 credit_amount1" min="0" step="any"></div><div class="col-md-1" style="margin-top:24px;"><div class="form-group text-center"><button onclick="$(this).parent().parent().parent().slideUp(function(){ $(this).remove(); sum_credit_amount(); }); return false" class="remove btn btn-danger btn-sm"><i class="fa fa-minus"></i></button></div></div></div>');
                $( '.select_box' ).select2({});
            }
        </script>
        <!--End toggle For Edit more General Journal Credit Account-->

        <!-- Start Sum Amount and Show-->
        <script type="text/javascript">
            function sum_debit_amount() {
                var debit=0;
                var debit_amount="";
                var total_debit_amount="";
                if($('#tr_id').val()!='' && $('#edit_entries').is(':visible')){
                    debit_amount=$('.debit_amount1');
                    total_debit_amount=$('#total_debit_amount1');
                }else {
                    debit_amount=$('.debit_amount');
                    total_debit_amount=$('#total_debit_amount');
                }
                debit_amount.each(function(){
                        debit += parseFloat($(this).val());
                });
                total_debit_amount.val(parseFloat(debit));
                total_checking();
            }
            function sum_credit_amount() {
                var credit=0;
                var credit_amount="";
                var total_credit_amount="";
                if($('#tr_id').val()!='' && $('#edit_entries').is(':visible')){
                    credit_amount=$('.credit_amount1');
                    total_credit_amount=$('#total_credit_amount1');
                }else {
                    credit_amount=$('.credit_amount');
                    total_credit_amount=$('#total_credit_amount');
                }
                credit_amount.each(function () {
                   credit += parseFloat($(this).val());
                });
                total_credit_amount.val(parseFloat(credit));
                total_checking();
            }
            function total_checking() {
                var total_debit_id ="";
                var total_credit_id ="";
                var btn_id = "";
                if($('#tr_id').val()!='' && $('#edit_entries').is(':visible')){
                    total_debit_id = $('#total_debit_amount1');
                    total_credit_id = $('#total_credit_amount1');
                    btn_id = $('#btn_edit');
                }else{
                    total_debit_id = $('#total_debit_amount');
                    total_credit_id = $('#total_credit_amount');
                    btn_id = $('#btn_create');
                }
                var total_debit=total_debit_id.val();
                var total_credit=total_credit_id.val();
                if(total_debit==total_credit){
                    btn_id.removeAttr( "disabled" );
                }else{
                    btn_id.attr( "disabled","disabled" );
                }
            }
        </script>
        <!-- End Sum Amount and Show-->


        <!--Jquery Datepicker-->
        <script>
            $( function() {
                $( ".datepicker" ).datepicker({dateFormat: 'dd-mm-yy'});
            } );
        </script>
        <!--Jquery Datepicker-->

        <!--Delete Transaction Script-->
        <script type="text/javascript">
            $('.delete').click(function () {
                if(confirm("Are you sure you want to delete these Entries?")){
                    $.ajax({
                        type:"POST",
                        url:"<?php echo base_url('admin/accounts/delete_transaction'); ?>"+"/"+$(this).val(),
                        dataType:'JSON',
                        success:function(data){
                            if(data>0){
                                setTimeout(function(){location.href="<?php echo base_url('admin/accounts/general_journal'); ?>"} , 2000);
                            }
                        }
                    });
                }else {
                    return false;
                }
            });

            function delete_entry(value_id) {
                if(confirm("Are you sure you want to delete these Entries?")){
                    $.ajax({
                        type:"POST",
                        url:"<?php echo base_url('admin/accounts/delete_trm'); ?>"+"/"+value_id,
                        dataType:'JSON',
                        success:function(data){
                            if(data>0){
                                $('#btn_remove'+value_id).parent().parent().slideUp(function(){ $('#btn_remove'+value_id).remove(); });
                            }
                        }
                    });
                }else {
                    return false;
                }
            }
        </script>
        <!--/Delete Transaction Script-->

        <?php echo form_open(base_url('admin/accounts/general_journal/'),array('class'=>"form-horizontal")); ?>

        <div class="row">
            <div class="col-lg-12">
                <section class="panel panel-default">
                    <header class="panel-heading  "><?= lang('search_by'); ?></header>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-xs-2">
                                <input type="text" id="date" name="date" class="form-control datepicker" placeholder="Date" autocomplete="off" value="<?= (!empty($date))?$date:'' ?>" required>
                            </div>

                            <div class="col-xs-2">
                                <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> <?= lang('search') ?></button>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <?php echo form_close(); ?>
        <input type="hidden" id="payment_type" value="<?= (!empty($payment_mode))?$payment_mode:'All' ?>">
        <section class="panel panel-default">

            <header class="panel-heading"><?= lang('general_journal_list') ?></header>

            <div class="panel-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive" id="customers">
                            <table class="table table-bordered data-table-list dtHorizontalExample" id="tab_customers">
                                <thead>
                                <th width="20%">Date</th>
                                <th width="50%">Description</th>
                                <th width="10%">Debit</th>
                                <th width="10%">Credit</th>
                                <th width="10%">Action</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <button class="btn btn-sm pull-left" style="color: #fff;background-color: #26a69a" id="btnExport">Export to Excel</button>
            </div>

        </section>

        <script>
            var date=$('#date').val();
            var data_url='<?= base_url('admin/accounts/general_journal_lists/') ?>'+date;
        </script>

        <!-- Export to Excel-->
        <div id="export_table" style="display: none;">
            <table style="border: 1px solid;">
                <thead>
                <tr>
                    <th style="border: 1px black">Date</th>
                    <th style="border: 1px black">Description</th>
                    <th style="border: 1px black">Debit</th>
                    <th style="border: 1px black">Credit</th>
                </tr>
                </thead>
                <tbody>
                <?php if(!empty($entries)): ?>
                    <?php foreach ($entries as $entry){ ?>
                        <tr>
                            <?php $debit_transactions=read_debit_transactions($entry->T_ID);
                            $credit_transactions=read_credit_transactions($entry->T_ID);
                            ?>
                            <td style="border: 1px black"><?php echo $entry->T_DATE; ?></td>
                            <td style="border: 1px black">
                                <?php foreach ($debit_transactions as $d){ ?>
                                    <span style="float:left;"><?php echo $d->A_NAME; ?></span><br>
                                <?php } ?>
                                <?php foreach ($credit_transactions as $c){ ?>
                                    <span style="float:right;"><?php echo $c->A_NAME; ?></span><br>
                                <?php } ?>
                                <?php foreach ($debit_transactions as $d){
                                    if($d->PARTICULARS!=""):
                                        ?>
                                        <span style="float:left;"><?php echo $d->PARTICULARS; ?></span><br>
                                    <?php endif;
                                } ?>
                                <?php foreach ($credit_transactions as $c){
                                    if($c->PARTICULARS!=""):
                                        ?>
                                        <span style="float:left;"><?php echo $c->PARTICULARS; ?></span><br>
                                    <?php endif;
                                } ?>
                            </td>
                            <td style="vertical-align: top;border: 1px black">
                                <?php foreach ($debit_transactions as $d){ ?>
                                    <span style="float:left;"><?php echo $d->TM_AMOUNT; ?></span><br>
                                <?php } ?>
                            </td>
                            <td style="vertical-align: top;border: 1px black">
                                <?php foreach ($debit_transactions as $d){ ?>
                                    <br>
                                <?php } ?>
                                <?php foreach ($credit_transactions as $c){ ?>
                                    <span style="float:right;"><?php echo $c->TM_AMOUNT; ?></span><br>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <script>
            $("#btnExport").click(function (e) {
                window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('#export_table').html() ));
                e.preventDefault();
            });
        </script>
        <!-- /Export to Excel-->