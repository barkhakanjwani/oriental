
<section class="content-header">
    <?php
    if ($advance_info->expense_type=='Job'){
        $client_id=$advance_info->client_id;
    }elseif ($advance_info->expense_type=='Misc'){
        $advance_d = $this->invoice_model->check_by(array('ap_id' => $advance_info->ap_id), 'tbl_advance_payments');
        $client_id=$advance_d->client_id;
    }
    $client_info = $this->invoice_model->check_by(array('client_id' => $client_id), 'tbl_client');
    ?>
    <div class="row">
        <div class="col-sm-8">
        </div>
    </div>
</section>
<section class="content">
    <!-- Start Display Details -->
    <!-- Main content -->
    <div class="row" >
        <section class="invoice" id="print_invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <img style="width: 60px;width: 60px;margin-top: -10px;margin-right: 10px;" src="<?= base_url() . config_item('invoice_logo') ?>" ><?= config_item('company_name') ?>
                    </h2>
                </div><!-- /.col -->
            </div>
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th colspan="4">Payment</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($advance_info->expense_type=='Job'){ ?>
                        <tr>
                            <td><b><?= lang('payment_date') ?></b></td>
                            <td><?= strftime(config_item('date_format'), strtotime($advance_info->payment_date)) ?></td>
                            <td width="20%"><b><?= lang('client') ?></b></td>
                            <td width="30%" class="text-right"><?= $client_info->name ?></td>
                        </tr>
                        <tr>
                            <td width="20%"><b><?= lang('reference_no') ?></b></td>
                            <td width="30%"><?= $this->invoice_model->job_no_creation($advance_info->invoices_id) ?></td>
                            <td><b><?= lang('payment_mode') ?></b></td>
                            <td class="text-right"><?php echo $advance_info->payment_method; ?></td>
                        </tr>
                            <?php if ($advance_info->payment_method == 'Bank'){
                                $branch_info = $this->invoice_model->check_by(array('A_ID' => $advance_info->debit_account), 'branches');
                                $bank_info = $this->invoice_model->check_by(array('B_ID' => $branch_info->B_ID), 'banks');
                                ?>
                                <tr>
                                    <td><b><?= lang('bank') ?></b></td>
                                    <td><?= $bank_info->B_NAME; ?></td>
                                    <td><b>Branch</b></td>
                                    <td class="text-right"><?= $branch_info->BR_NAME; ?></td>
                                </tr>
                            <?php } elseif($advance_info->payment_method == 'Third Party'){ ?>
                                <tr>
                                    <td colspan="4">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <td colspan="2"><b><?= lang('title') ?></b></td>
                                                    <td><b><?= lang('bank') ?></b></td>
                                                    <td><b>Branch</b></td>
                                                    <td class="text-right"><b><?= lang('amount') ?></b></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($advance_details as $advance_detail){ ?>
                                                <tr>
                                                    <td colspan="2"><?= $advance_detail->apd_title ?></td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td class="text-right"><?= number_format($advance_detail->apd_amount,2) ?></td>
                                                </tr>
                                            <?php } ?>
                                            <tr>
                                                <td colspan="4"><b><?= lang('total_amount') ?></b></td>
                                                <td class="text-right"><?= number_format(($advance_info->amount),2) ?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php if ($advance_info->payment_method != 'Third Party'){ ?>
                        <tr>
                            <td><b><?= lang('amount') ?></b></td>
                            <td><?= number_format(($advance_info->amount),2) ?></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td><b><?= lang('description') ?></b></td>
                            <td colspan="3"><?= $advance_info->description ?></td>
                        </tr>
                        <?php
                        if($advance_info->payment_method == 'Cash' && !empty($transfer_detail)){ ?>
                            <tr>
                                <td colspan="4">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <td colspan="3"><b>Transfer Details</b></td>
                                        </tr>
                                        <tr>
                                            <td><b><?= lang('bank') ?></b></td>
                                            <td><b>Branch</b></td>
                                            <td class="text-right"><b><?= lang('amount') ?></b></td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><?= $transfer_detail->B_NAME ?></td>
                                            <td><?= $transfer_detail->BR_NAME ?></td>
                                            <td class="text-right"><?= number_format($transfer_detail->amount,2) ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        <?php } ?>

                        <?php }elseif ($advance_info->expense_type=='Misc'){ ?>
                            <tr>
                                <td width="20%"><b><?= lang('client') ?></b></td>
                                <td width="30%" class="text-right"><?= $client_info->name ?></td>
                                <td width="20%"><b><?= lang('advance_date') ?></b></td>
                                <td width="30%" class="text-right"><?= strftime(config_item('date_format'), strtotime($advance_info->payment_date)) ?></td>
                            </tr>
                            <tr>
                                <td><b><?= lang('payment_mode') ?></b></td>
                                <td class="text-right"><?php echo $advance_info->payment_method; ?></td>
                                <td><b><?= lang('amount') ?></b></td>
                                <td class="text-right"><?= number_format(($advance_info->amount),2) ?></td>
                            </tr>
                            <?php if ($advance_info->payment_method == 'Bank'){
                                $branch_info = $this->invoice_model->check_by(array('A_ID' => $advance_info->debit_account), 'branches');
                                $bank_info = $this->invoice_model->check_by(array('B_ID' => $branch_info->B_ID), 'banks');
                                ?>
                                <tr>
                                    <td><b><?= lang('bank') ?></b></td>
                                    <td class="text-right"><?= $bank_info->B_NAME; ?></td>
                                    <td><b>Branch</b></td>
                                    <td class="text-right"><?= $branch_info->BR_NAME; ?></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><b><?= lang('description') ?></b></td>
                                <td class="text-right" colspan="3"><?= $advance_info->description ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </section>
        <section style="display: none;" class="invoice" id="print_invoice_new">
        </section>
    </div>

</section>
<script type="text/javascript">
    function print_invoice(print_invoice_new) {
        var printContents = document.getElementById(print_invoice_new).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>