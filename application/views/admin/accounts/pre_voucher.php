<!-- Main content -->

<div class="content-wrapper" style="margin: auto;">



    <!-- Page header -->

    <!-- <div class="page-header">



        <div class="breadcrumb-line">

            <ul class="breadcrumb">

                <li><a href="<?php echo base_url('admin/home');?>"><i class="fa fa-home"></i> Home</a></li>

                <li class="active">Voucher</li>

            </ul>

        </div>

    </div> -->

    <!-- /page header -->



    <!-- Content area -->

    <div class="content">



        <div class="row">



            <div class="col-sm-12">

                <div class="panel panel-flat thumbnail">



                    <div class="panel-heading no-print">

                        <div class="btn-group">

                            <button type="button" onclick="printDiv('PrintMe')" class="btn btn-default" ><i class="fa fa-print"></i></button>

                            <script>

                                function printDiv(divName) {

                                    var printContents = document.getElementById(divName).innerHTML;

                                    var originalContents = document.body.innerHTML;

                                    document.body.innerHTML = printContents;

                                    window.print();

                                    document.body.innerHTML = originalContents;

                                }

                            </script>

                        </div>

                    </div>



                    <div id="PrintMe">

                        <style>

                            .small-box{

                                width: 150px;

                                border: 1px solid;

                                padding: 10px;

                                padding-right: 70px;

                            }

                            .table-main>tbody>tr>td,.table-main>tfoot>tr>td,.table-main>tr>td{

                                border: none;

                            }

                        </style>

                        <div class="panel-heading">

                            <div class="text-center">

                                <!-- <h4>Debit Voucher</h4> -->

                                <h1><?= config_item('company_name') ?></h1>

                                <h4></h4>

                                <!-- <h4>Karachi</h4> -->

                                <h4>Debit Voucher</h4>

                            </div>

                        </div>



                        <div class="panel-body">

                            <div class="row">

                                <div class="col-sm-12">

                                    <table class="table table-main">

                                        <tr>

                                            <td><b><input type="text" class="small-box" value="V. No." readonly></b></td>

                                            <td><?php echo $voucher->voucher_id; ?></td>

                                            <td colspan="4"></td>

                                            <td><b><input type="text" class="small-box" value="Date" readonly></b></td>

                                            <td><?php echo date('d-m-Y',strtotime($voucher->payment_date)); ?></td>

                                        </tr>

                                        <!--<tr>

                                            <td><b><input type="text" class="small-box" value="Credit To :" readonly></b></td>

                                            <td><?php /*echo $voucher->credit_account; */?></td>

                                            <td colspan="4"></td>

                                            <td><b><input type="text" class="small-box" value="Credit Code" readonly></b></td>

                                            <td><?php /*echo $voucher->credit_account; */?></td>

                                        </tr>-->

                                        <tr>

                                            <td><b><input type="text" class="small-box" value="Paid to :" readonly></b></td>

                                            <td><?php echo $voucher->paid_to; ?></td>

                                            <td colspan="4"></td>

                                            <td><b><input type="text" class="small-box" value="Type" readonly></b></td>

                                            <td>Pre Shipment Voucher</td>

                                        </tr>

                                       <!-- <tr>

                                            <td colspan="6"><b><input type="text" style="border:1px solid;padding: 10px;padding-right: 400px;" value="Particulars" readonly></b></td>

                                            <td colspan="2"><b><input type="text" style="border:1px solid;padding: 10px;padding-right: 169px;" value="Amount" readonly></b></td>

                                        </tr>-->

                                       <!-- <tr>

                                            <td colspan="6">
                                                <textarea style="border:1px solid;padding: 10px;text-align:right ;" rows="5" cols="82" readonly>
                                                    <?php
/*                                                        $voucher_details = $this->invoice_model->check_by_all(array('voucher_id'=>$voucher->voucher_id), 'tbl_voucher_details');
                                                        foreach($voucher_details as $voucher_detail) {
                                                            $job_info = $this->invoice_model->check_by(array('invoices_id'=>$voucher_detail->invoices_id), 'tbl_invoices');
                                                            */?>
                                                        <?/*= $job_info->reference_no */?> <?/*= read_subHead($voucher_detail->debit_account)->A_NAME ."\n" */?>
                                                            <?php
/*                                                        }
                                                    */?>
                                                </textarea>
                                            </td>
                                            <td colspan="2"><textarea style="border:1px solid;padding: 5px;" rows="5" cols="45" readonly>
                                                    <?php
/*                                                    $total = 0;
                                                    foreach($voucher_details as $voucher_detail) {
                                                        $total += $voucher_detail->amount;
                                                        */?>
                                                        <?/*= number_format($voucher_detail->amount,2)."\n" */?>
                                                        <?php
/*                                                    }
                                                    */?>
                                                </textarea></td>

                                        </tr>-->
                                        <tr>
                                            <th colspan="2" style="border: 1px solid black;">Job No</th>
                                            <th colspan="3" style="border: 1px solid black;text-align:center;">Title</th>
                                            <th colspan="3" style="border: 1px solid black;text-align:right;">Amount</th>
                                        </tr>
                                        <?php
                                        $voucher_details = $this->invoice_model->check_by_all(array('voucher_id'=>$voucher->voucher_id), 'tbl_voucher_details');
                                        $total = 0;
                                        foreach($voucher_details as $voucher_detail) {
                                        $job_info = $this->invoice_model->check_by(array('invoices_id'=>$voucher_detail->invoices_id), 'tbl_invoices');
                                            $total += $voucher_detail->amount;
                                        ?>
                                        <tr>
                                            <td colspan="2" style="border: 1px solid black;">
                                                <?= $job_info->reference_no ?>
                                            </td>

                                            <td colspan="3" style="border: 1px solid black;text-align:center;">
                                                <?= read_subHead($voucher_detail->debit_account)->A_NAME ?>
                                            </td>

                                            <td colspan="3" style="border: 1px solid black;text-align:right;">
                                                <?= number_format($voucher_detail->amount,2) ?>
                                            </td>
                                        </tr>
                                            <?php
                                        }
                                        ?>

                                       <!-- <tr>


                                            <td><b><input type="text" class="small-box" value="In Words : <?php /*echo $this->invoice_model->convert_number($total); */?>" readonly></b></td>

                                            <td colspan="4"><input type="text" style="border:1px solid;padding: 10px;width: 220px;" value="<?php /*echo $this->invoice_model->convert_number($total); */?>" readonly></td>

                                            <td><b><input type="text" style="border:1px solid;padding: 10px;width: 100px;" value="Total" readonly></b></td>

                                            <td colspan="2"><input type="text" style="border:1px solid;padding: 10px;padding-right: 169px;" value="<?php /*echo number_format($total,2); */?>" readonly></td>

                                        </tr>-->

                                        <tr>
                                            <td colspan="5" style="border: 1px solid black;text-align:center"><b>
                                                In Words : <?php echo $this->invoice_model->convert_number($total); ?>
                                                </b></td>
                                            <td colspan="3"style="border: 1px solid black;text-align:right;"><b>
                                                Total : <?php echo number_format($total,2); ?>
                                                </b></td>
                                        </tr>

                                    </table>



                                </div>



                            </div>



                        </div>



                        <div class="panel-footer" style="padding-top: 0px; margin-top: 40px;">

                            <table class="table table-main" style="padding-top: 50px;">

                                <tr>

                                    <td>____________________ <br> <h4>Cashier</h4> </td>

                                    <td>____________________ <br> <h4>Deptt. Head</h4> </td>

                                    <td>_________________________ <br> <h4>Owner Manager</h4> </td>

                                    <td>_________________________ <br> <h4>Receiver Signature</h4> </td>

                                </tr>

                            </table>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>