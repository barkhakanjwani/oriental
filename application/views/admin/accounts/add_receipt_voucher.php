<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('manage_receipt_voucher') ?></h1>
<div class="row">
    <div class="col-lg-12">
        <form role="form" enctype="multipart/form-data" id="form" action="<?php echo base_url(); ?>admin/accounts/save_receipt_voucher/<?= (!empty($advance_info))?$advance_info->ap_id:'' ?>" method="post" class="form-horizontal  ">
            <section class="panel panel-default">
                <header class="panel-heading"><?= $title ?></header>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-lg-4 control-label"></div>
                        <div class="col-lg-3">
                            <input type="radio" value="Job" name="expense_type" class="expense_type" required onclick="advance_payment_list('Job')" checked/> Job
                            <input type="radio" value="Misc" name="expense_type" class="expense_type" required onclick="advance_payment_list('Misc')"/> Client
                            <input type="radio" value="Others" name="expense_type" class="expense_type" required/> Others
                        </div>
                    </div>
                    <div id="job_expense">
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('reference_no') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="invoice_id" class="form-control select_box required_select mb-5" data-width="100%" id="invoice_id" onchange="showClientName(this.value),advance_payment_list('Job')">
                                    <option value="">---</option>
                                    <?php if(!empty($all_invoices)):
                                        foreach ($all_invoices as $invoice): ?>
                                            <option value="<?php echo $invoice->invoices_id; ?>"><?php echo $this->invoice_model->job_no_creation($invoice->invoices_id); ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('client') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="text" id="client_name" readonly class="form-control required mb-5">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('payment_mode') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="payment_type" class="form-control select_box required_select" id="type" data-width="100%">
                                    <option value="">---</option>
                                    <option value="Cash">Cash</option>
                                    <option value="Bank">Bank</option>
                                    <option value="Third Party">Third Party</option>
                                </select>
                            </div>
                        </div>
                        <div id="cash_at_bank" style="display:none;">
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Banks <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <select name="bank" class="form-control cash_at_bank select_box required_select" onchange="selectBranches(this.value)" style="width:100%;">
                                        <option value="">---</option>
                                        <?php foreach ($banks as $bank): ?>
                                            <option value="<?php echo $bank->B_ID; ?>"><?php echo $bank->B_NAME; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div id="branches">
                            </div>
                            <input type="hidden" id="account_id" name="account_id" />
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= 'Instrument Type' ?> </label>
                                <div class="col-lg-3">
                                    <select name="instrument_type" class="form-control select_box required_select" id="instrument_type" data-width="100%">
                                        <option value="">---</option>
                                        <option value="Pay Order">Pay Order</option>
                                        <option value="Demand Draft">Demand Draft</option>
                                        <option value="T.T">T.T</option>
                                        <option value="Cheque">Cheque</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= lang('cheque_payorder') ?> </label>
                                <div class="col-lg-3">
                                    <input type="number" name="cheque_payorder" class="form-control mb-5">
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="form-group pay_order" style="display: none">
                                <label class="col-lg-4 control-label"><?= lang('title') ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <select class="form-control select_box required title" name="pay_order_title[]" data-width="100%">
                                        <option value="">---</option>
                                        <option value="Security Deposit">Security Deposit</option>
                                        <option value="Collector of Custom">Collector of Custom</option>
                                        <option value="Excise and Taxation">Excise and Taxation</option>
                                        <option value="Delivery Order">Delivery Order</option>
                                        <option value="Terminal">Terminal</option>
                                        <option value="LOLO">LOLO</option>
                                        <option value="Late DO">Late DO</option>
                                        <option value="Container Detention">Container Detention</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" id="amount">
                                <label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <input type="number" name="amount" class="form-control required mb-5" min="0" step="any">
                                </div>
                            </div>
                            <div class="form-group pay_order" style="display: none">
                                <label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <input type="number" name="pay_order_amount[]" class="form-control required mb-5" min="0" step="any">
                                </div>
                            </div>
                            <div class=" self_cash_at_bank" style="display: none;">
                                <div class="form-group">
                                    <label class="col-lg-4 control-label">Banks <span class="text-danger">*</span></label>
                                    <div class="col-lg-3">
                                        <select name="self_bank[]" class="form-control select_box required_select self_bank" onchange="selectSelfBranches(0,this.value)" style="width:100%;">
                                            <option value="">---</option>
                                            <?php foreach ($banks as $bank): ?>
                                                <option value="<?php echo $bank->B_ID; ?>"><?php echo $bank->B_NAME; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="self_branches">
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">Branches <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <select name="self_branch[]" class="form-control select_box self_branch" id="self_branch0" onchange="getSelfAccountId(0,this.value)" data-width="100%">
                                                <option value="">Choose Branch</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="self_account_id0" name="self_account_id[]" />
                            </div>
                            <div class="form-group  pay_order" style="display: none">
                                <label class="col-lg-4 control-label"><?= lang('cheque_payorder') ?></label>
                                <div class="col-lg-3">
                                    <input type="text" name="pay_order_no[]" class="form-control mb-5">
                                </div>
                                <div class="col-lg-2">
                                    <button type="button" class="btn btn-primary btn-xs" id="add"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                        <div id="new"></div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('date') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="text" name="date" class="form-control datepicker" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('description') ?></label>
                            <div class="col-lg-3">
                                <textarea name="desc" class="form-control mb-5"></textarea>
                            </div>
                        </div>
                    </div>
                    <div id="misc_expense" style="display: none;">
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('client') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="client_id" id="client_id" class="form-control select_box required_select mb-5" data-width="100%" onchange="advance_payment_list('Misc')">
                                    <option value="">---</option>
                                    <?php if(!empty($all_clients)):
                                        foreach ($all_clients as $client): ?>
                                            <option value="<?php echo $client->client_id; ?>"><?php echo $client->name; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('payment_mode') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="misc_payment_type" class="form-control select_box required_select" id="misc_type" data-width="100%">
                                    <option value="">---</option>
                                    <option value="Cash">Cash</option>
                                    <option value="Bank">Bank</option>
                                </select>
                            </div>
                        </div>
                        <div id="misc_cash_at_bank" style="display:none;">
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Banks <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <select name="misc_bank" class="form-control misc_cash_at_bank select_box required_select" onchange="selectMiscBranches(this.value)" style="width:100%;">
                                        <option value="">-</option>
                                        <?php foreach ($banks as $bank): ?>
                                            <option value="<?php echo $bank->B_ID; ?>"><?php echo $bank->B_NAME; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div id="misc_branches">
                            </div>
                            <input type="hidden" id="misc_account_id" name="misc_account_id" />
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="number" name="misc_amount" class="form-control required mb-5" min="0" step="any">
                            </div>
                        </div>
                        <div id="misc_instrument" style="display:none;">
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= 'Instrument Type' ?> </label>
                                <div class="col-lg-3">
                                    <select name="misc_instrument_type" class="form-control select_box required_select" id="instrument_type" data-width="100%">
                                        <option value="">---</option>
                                        <option value="Pay Order">Pay Order</option>
                                        <option value="Demand Draft">Demand Draft</option>
                                        <option value="T.T">T.T</option>
                                        <option value="Cheque">Cheque</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= lang('cheque_payorder') ?> </label>
                                <div class="col-lg-3">
                                    <input type="number" name="misc_pay_order" class="form-control mb-5">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('date') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="text" name="misc_date" class="form-control datepicker" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('description') ?></label>
                            <div class="col-lg-3">
                                <textarea name="misc_desc" class="form-control mb-5"></textarea>
                            </div>
                        </div>
                    </div>
                    <div id="others_receipt" style="display: none;">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= lang('received_from') ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <select name="paid_accounts"  class="form-control required_select mb-5 heads select_box" id="paid_accounts" data-width="100%">
                                        <optgroup label="Assets"></optgroup>
                                        <?php $sub_heads=$this->invoice_model->check_by_all(array('HEAD_TYPE'=>'Assets'),'sub_heads');
                                        foreach ($sub_heads as $sb) {
                                            if (count_accounts_by_sub_head($sb->SUB_HEAD_ID)>0) { ?>
                                            <optgroup label="<?= '&nbsp;&nbsp;' . $sb->NAME ?>">
                                                <?php $accounts_head = account_head_by_subId($sb->SUB_HEAD_ID);
                                                foreach ($accounts_head as $ah) {
                                                    if ($ah->H_ID != 4) {
                                                        $accounts = account_by_head($ah->H_ID);
                                                        if (!empty($accounts)) { ?>
                                                            <optgroup label="<?= '&nbsp;&nbsp;&nbsp;&nbsp;' . $ah->H_NAME ?>"><?php
                                                            foreach ($accounts as $account) {
                                                                $account1 = accounts_by_self($account->A_ID);
                                                                if (!empty($account1)) { ?>
                                                                    <optgroup label="<?= $account->A_NAME ?>"><?php
                                                                    foreach ($account1 as $ac1) {
                                                                        $account2 = accounts_by_self($ac1->A_ID);
                                                                        if (!empty($account2)) { ?>
                                                                            <optgroup label="<?= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $ac1->A_NAME ?>"><?php
                                                                            foreach ($account2 as $ac2) { ?>
                                                                                <option value="<?php echo $ac2->A_ID; ?>"><?php echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $ac2->A_NAME; ?></option><?php
                                                                            } ?></optgroup><?php
                                                                        } else { ?>
                                                                            <option value="<?php echo $ac1->A_ID; ?>"><?php echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $ac1->A_NAME; ?></option><?php
                                                                        }
                                                                    } ?>
                                                                    </optgroup><?php
                                                                } else { ?>
                                                                    <option value="<?php echo $account->A_ID; ?>"><?php echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $account->A_NAME; ?></option><?php
                                                                }
                                                            } ?>
                                                            </optgroup><?php
                                                        }
                                                    }
                                                } ?></optgroup><?php
                                            }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= lang('payment_mode') ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <select name="others_payment_type" class="form-control select_box required_select" id="type" data-width="100%" onchange="showOtherBanks(this.value)">
                                        <option value="Cash">Cash</option>
                                        <option value="Bank">Bank</option>
                                    </select>
                                </div>
                            </div>
                            <div id="other_cash_at_bank" style="display:none;">
                                <div class="form-group">
                                    <label class="col-lg-4 control-label">Banks <span class="text-danger">*</span></label>
                                    <div class="col-lg-3">
                                        <select name="bank" class="form-control cash_at_bank select_box required_select" onchange="selectOtherBranches(this.value)" style="width:100%;">
                                            <option value="">-</option>
                                            <?php foreach ($banks as $bank): ?>
                                                <option value="<?php echo $bank->B_ID; ?>"><?php echo $bank->B_NAME; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div id="other_branches">
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">Branches <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <select name="branch_id" class="form-control cash_at_bank select_box" onchange="getAccountId(this.value)" data-width="100%">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="debit_account_id" name="debit_account_id" />
                                <div class="form-group">
                                    <label class="col-lg-4 control-label"><?= lang('cheque_payorder') ?> <span class="text-danger">*</span></label>
                                    <div class="col-lg-3">
                                        <input type="text" name="chq_no"class="form-control required mb-5">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <input type="number" name="others_amount" class="form-control" autocomplete="off" step="any" min="0">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= lang('date') ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <input type="text" name="others_date" class="form-control datepicker" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= lang('description') ?></label>
                                <div class="col-lg-3">
                                    <textarea name="other_desc" class="form-control mb-5"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"></label>
                        <div class="col-lg-4">
                            <button type="submit" class="btn btn-sm btn-success pull-right"><i class="fa fa-check"></i><?php echo lang('create_voucher'); ?></button>
                        </div>
                    </div>
                </div>
                <div class="panel-body" id="history_detail">
                    <div class="row col-md-8 col-md-offset-2">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th><?= lang('created_date') ?></th>
                                <th><?= lang('time') ?></th>
                                <th>Payment Date</th>
                                <th><?= lang('reference_no') ?></th>
                                <th><?= lang('client') ?></th>
                                <th>Payment Method</th>
                                <th>Bank</th>
                                <th>Branch</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                            <tbody id="dynamic_rows">
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </form>
    </div>
</div>
<script type="text/javascript">
    $('.expense_type').click(function () {
        $('.pageloader').show();
        var val = $(this).val();
        if (val == 'Misc'){
            $('#job_expense').hide();
            $('#misc_expense').show();
            $('#history_detail').show();
            $('#others_receipt').hide();
        }else if (val=='Job') {
            $('#misc_expense').hide();
            $('#others_receipt').hide();
            $('#job_expense').show();
            $('#history_detail').show();
        }else {
            $('#others_receipt').show();
            $('#misc_expense').hide();
            $('#job_expense').hide();
            $('#history_detail').hide();
        }
        $('.pageloader').hide();
    });
    $('#type').on("change", function() {
        $('.pageloader').show();
        var type = $(this).val();
        if(type == ''){
            $('.pay_order').hide();
            $('#cash_at_bank').hide();
            $('.self_cash_at_bank').hide();
        }
        else if(type == 'Third Party'){
            $('#amount').hide();
            $('.pay_order').show();
            $('#cash_at_bank').hide();
        }else if(type == 'Cash'){
            $('.pay_order').hide();
            $('#cash_at_bank').hide();
            $('.self_cash_at_bank').hide();
            $('#amount').show();
        }
        else{
            $('.pay_order').hide();
            $('.self_cash_at_bank').hide();
            $('#amount').show();
            $('#cash_at_bank').show();
        }
        $('.pageloader').hide();
    });
    $('#transfer_check').click(function () {
        if ($(this).prop('checked')==true){
            $('#transfer_div').show();
        }else {
            $('#transfer_div').hide();
        }
    });
    $('#misc_type').on("change", function() {
        var type = $(this).val();
        if(type == 'Cash'){
            $('#misc_cash_at_bank').hide();
            $('#misc_instrument').hide();
        }
        else{
            $('#misc_cash_at_bank').show();
            $('#misc_instrument').show();
        }
    });

    function showOtherBanks(val) {
        if (val=='Bank'){
            $('#other_cash_at_bank').show();
        } else {
            $('#other_cash_at_bank').hide();
        }
    }

    function selectOtherBranches(val) {
        var option="";
        if(val == '') {
            $('#other_branches').hide();
        }
        else{
            $('.pageloader').show();
            $.getJSON("<?php echo site_url('admin/accounts/ajax_select_branches') ?>/" + val, function (result) {
                $.each(result, function (index, value) {
                    option = ('<option value="' + value.BR_ID + '">' + value.BR_NAME + '</option>')+option;
                });
                var drop = '<div class="form-group"><label class="col-md-4 control-label">Branches <span class="text-danger">*</span></label><div class="col-lg-3" id="other_branches"><select class="form-control select_box required_select" onchange="getOtherAccountId(this.value)"><option value="">-</option>' + option + '</select></div></div>';
                $('#other_branches').html(drop).hide().fadeIn(500);
                $('.select_box').select2({});
                $('.pageloader').hide();
                getOtherAccountId(result[0].BR_ID);
            });
        }
    }

    function getOtherAccountId(val){
        var account_id=$('#debit_account_id');
        $.getJSON( "<?php echo site_url('admin/accounts/ajax_get_branch_aid') ?>/"+val, function(result){
            (result != null)?account_id.val(result.A_ID):'';
        });
    }

    function selectBranches(val) {
        var option="";
        if(val == '') {
            $('#branches').hide();
        }
        else{
            $('.pageloader').show();
            $.getJSON("<?php echo site_url('admin/accounts/ajax_select_branches') ?>" + "/" + val, function (result) {
                $.each(result, function (index, value) {
                    option = ('<option value="' + value.BR_ID + '">' + value.BR_NAME + '</option>')+option;
                });
                var drop = '<div class="form-group"><label class="col-md-4 control-label">Branches <span class="text-danger">*</span></label><div class="col-lg-3" id="branches"><select class="form-control select_box required_select" onchange="getAccountId(this.value)"><option value="">-</option>' + option + '</select></div></div>';
                $('#branches').html(drop).hide().fadeIn(500);
                $('.select_box').select2({});
                $('.pageloader').hide();
                getAccountId();
            });
        }
    }

    function selectMiscBranches(val) {
        var option="";
        if(val == '') {
            $('#misc_branches').hide();
        }
        else{
            $('.pageloader').show();
            $.getJSON("<?php echo site_url('admin/accounts/ajax_select_branches') ?>" + "/" + val, function (result) {
                $.each(result, function (index, value) {
                    option = ('<option value="' + value.BR_ID + '">' + value.BR_NAME + '</option>')+option;
                });
                var drop = '<div class="form-group"><label class="col-md-4 control-label">Branches <span class="text-danger">*</span></label><div class="col-lg-3" id="misc_branches"><select class="form-control select_box required_select" onchange="getMiscAccountId(this.value)"><option value="">-</option>' + option + '</select></div></div>';
                $('#misc_branches').html(drop).hide().fadeIn(500);
                $('.select_box').select2({});
                $('.pageloader').hide();
                getMiscAccountId();
            });
        }
    }

    function getAccountId(val){
        var account_id=$('#account_id');
        $.getJSON( "<?php echo site_url('admin/accounts/ajax_get_branch_aid') ?>"+"/"+val, function(result) {
            (result != null)?account_id.val(result.A_ID):'';
        });
    }

    function getMiscAccountId(val){
        $.getJSON( "<?php echo site_url('admin/accounts/ajax_get_branch_aid') ?>"+"/"+val, function(result) {
            (result != null)?$('#misc_account_id').val(result.A_ID):'';
        });
    }

    function showClientName(invoice) {
        $.getJSON("<?php echo site_url('admin/security_deposit/get_client_name') ?>" + "/" + invoice, function (result) {
            $('#client_name').val(result);
        });
    }

    $(document).ready(function () {
        showClientName($('#invoice_id').val());
    });

    $(document).ready(function () {
        showClientName($('#invoice_id').val());
        var count=1;
        $("#add").click(function() {
            var columns = '<div><div class="form-group pay_order">';
            columns += '<label class="col-lg-4 control-label"><?= lang("title") ?> <span class="text-danger">*</span></label>';
            columns += '<div class="col-lg-3">';
            columns += '<select class="form-control select_box required title" name="pay_order_title[]" data-width="100%">';
            columns += '<option value="">---</option>';
            columns += '<option value="Security Deposit">Security Deposit</option>';
            columns += '';
            columns += '<option value="Collector of Custom">Collector of Custom</option>';
            columns += '<option value="Excise and Taxation">Excise and Taxation</option>';
            columns += '<option value="Delivery Order">Delivery Order</option>';
            columns += '<option value="Terminal">Terminal</option>';
            columns += '<option value="LOLO">LOLO</option>';
            columns += '<option value="Late DO">Late DO</option>';
            columns += '<option value="Container Detention">Container Detention</option>';
            columns += '</select>';
            columns += '</div>';
            columns += '</div>';
            columns += '<div class="form-group pay_order">';
            columns += '<label class="col-lg-4 control-label"><?= lang('amount') ?> <span class="text-danger">*</span></label>';
            columns += '<div class="col-lg-3">';
            columns += '<input type="number" name="pay_order_amount[]" class="form-control required mb-5" min="0" step="any">';
            columns += '</div>';
            columns += '</div>';
            columns += ' <div class="self_cash_at_bank" style="display: none;">';
            columns += '<div class="form-group">';
            columns += '<label class="col-lg-4 control-label">Banks <span class="text-danger">*</span></label>';
            columns += '<div class="col-lg-3">';
            columns += '<select name="self_bank[]" class="form-control select_box required_select self_bank" onchange="selectSelfBranches('+count+',this.value)" style="width:100%;">';
            columns += '<option value="">---</option>';
            columns += '<?php foreach ($banks as $bank): ?>';
            columns += '<option value="<?php echo $bank->B_ID; ?>"><?php echo $bank->B_NAME; ?></option>';
            columns += '<?php endforeach; ?>';
            columns += '</select>';
            columns += '</div>';
            columns += '</div>';
            columns += '<div class="self_branches">';
            columns += '<div class="form-group">';
            columns += '<label class="col-lg-4 control-label">Branches <span class="text-danger">*</span></label>';
            columns += ' <div class="col-lg-3">';
            columns += '<select name="self_branch[]" class="form-control select_box self_branch" id="self_branch'+count+'" onchange="getSelfAccountId('+count+',this.value)" data-width="100%">';
            columns += '<option value="">Choose Branch</option>';
            columns += '</select>';
            columns += '</div>';
            columns += '</div>';
            columns += '</div>';
            columns += '<input type="hidden" id="self_account_id'+count+'" name="self_account_id[]" />';
            columns += '</div>';
            columns += '<div class="form-group pay_order">';
            columns += '<label class="col-lg-4 control-label"><?= lang('cheque_payorder') ?></label>';
            columns += '<div class="col-lg-3">';
            columns += '<input type="text" name="pay_order_no[]" class="form-control mb-5">';
            columns += '</div>';
            columns += '<div class="col-lg-2">';
            columns += '<button type="button" class="btn btn-danger btn-xs remCF"><i class="fa fa-minus"></i></button>';
            columns += '</div>';
            columns += '</div>';
            columns += '</div>';
            var add_new = $(columns);
            count++;
            $("#new").append(add_new.hide().fadeIn(1000));
            $( '.select_box' ).select2({});
        });
        $("#new").on('click', '.remCF', function() {
            $(this).fadeOut(300, function() { $(this).parent().parent().parent().remove(); })
        });
        $(document).on('focus', '.datepicker', function() {
            $( this ).datepicker({format: 'yyyy-mm-dd',autoclose: true});
        });
        $(".remCF2").on('click', function() {
            $(this).fadeOut(300, function() { $(this).parent().parent().parent().remove(); })
        });
    });
    /*** SELF ***/
    $(document).on('change', ".title",function () {
        $('.pageloader').show();
        $(this).each(function() {
            if ($(this).val().length > 0) {
                if ($(this).val() == 'Self') {
                    $(this).parent().parent().parent().find('.self_cash_at_bank').show();
                }else{
                    $(this).parent().parent().parent().find('.self_cash_at_bank').hide();
                }
            }else{
                $(this).parent().parent().parent().find('.self_cash_at_bank').hide();
            }
            $('.pageloader').hide();
        });
    });

    function selectSelfBranches(counter,val){
        $('.pageloader').show();
        $('#self_branch'+counter).empty();
        $.getJSON("<?php echo site_url('admin/accounts/ajax_select_branches') ?>" + "/" + val, function (result) {
            var option="";
            $.each(result, function (index, value) {
                option = ('<option value="' + value.BR_ID + '">' + value.BR_NAME + '</option>')+option;
            });
            var drop = '<div class="form-group"><label class="col-md-4 control-label">Branches <span class="text-danger">*</span></label><div class="col-lg-3" id="branches"><select class="form-control select_box required_select" onchange="getAccountId(this.value)"><option value="">-</option>' + option + '</select></div></div>';
            $('#self_branch'+counter).html(drop).hide().fadeIn(500);
            $('.pageloader').hide();
            $('.select_box').select2({});
            getSelfAccountId();
        });
    }
    function getSelfAccountId(counter,val){
        account_id=$('#self_account_id'+counter);
        $.getJSON( "<?php echo site_url('admin/accounts/ajax_get_branch_aid') ?>"+"/"+val, function(result) {
            (result != null)?account_id.val(result.A_ID):'';
        });
    }
    /*** END SELF ***/

    function advance_payment_list(type) {
        $.ajax({
            url : "<?= base_url('admin/accounts/payment_by_invoice_client'); ?>",
            type : "POST",
            dataType : "json",
            data : {"exp_type" : type,"client":$('#client_id').val(),"invoice":$('#invoice_id').val()},
            success : function(data) {
                $('#dynamic_rows').empty();
                $('#dynamic_rows').append(data);
            },
            error : function(data) {
                console.log('error'+data);
            }
        });
    }

</script>