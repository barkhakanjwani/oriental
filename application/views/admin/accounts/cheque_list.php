<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<section class="content-header">
    <div class="row">
		<div class="col-md-12">
			<span class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('cheques_list') ?></span> <a href="#" data-toggle="modal" data-target="#add_cheque" class="btn btn-danger btn-xs" style="vertical-align: text-bottom;"><?= lang('add_new') ?></a>
		</div>
    </div>
</section>
	<!-- Content area -->
    <div class="content">
        <!-- Highlighting rows and columns -->
        <section class="panel panel-default">
            <header class="panel-heading"><?= lang('cheques_list') ?></header>
            <div class="panel-body" id="export_table">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered data-table-list">
                                <thead>
                                <th>Bank</th>
                                <th>Branch</th>
                                <th>Debit Account</th>
                                <th>Payee</th>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Created By</th>
                                <th class="text-center">Action</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--<div class="panel panel-flat">
            <table class="table table-bordered table-hover DataTables " id="DataTables">
                <thead>
                <tr>
                    <th>Bank</th>
                    <th>Branch</th>
                    <th>Debit Account</th>
                    <th>Payee</th>
                    <th>Amount</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Created By</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php /*if(!empty($cheques)): */?>
                    <?php /*foreach ($cheques as $cheque){
                        $client = ($cheque->CLIENT_ID == "")?'':" (".client_name($cheque->CLIENT_ID).")";
                        */?>
                        <tr>
                            <td><?php /*echo $cheque->B_NAME; */?></td>
                            <td><?php /*echo $cheque->BR_NAME; */?></td>
                            <td><?php /*echo $cheque->A_NAME.$client; */?></td>
                            <td><?php /*echo $cheque->C_PAY; */?></td>
                            <td><?php /*echo number_format($cheque->C_AMOUNT,2); */?></td>
                            <td><?php /*echo date('d-m-Y',strtotime($cheque->C_DATE)); */?></td>
                            <td><?php /*echo $cheque->C_STATUS; */?></td>
                            <td><?php /*echo UCFIRST($cheque->username); */?></td>
                            <td class="text-center">
                                <a target="_blank" href="<?php /*echo base_url('admin/accounts/view_cheque/'.$cheque->C_ID); */?>" type="button" data-popup="tooltip" title="" data-container="body" data-original-title="View Cheque" class="btn btn-default btn-xs"><i class="fa fa-print"></i></a>
                                <button type="button" onclick="getEditCheque(this.value)" value="<?php /*echo $cheque->C_ID;*/?>" data-popup="tooltip" data-container="body" data-original-title="Edit Cheque" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#" ><i class="fa fa-edit"></i></button>
                            </td>
                        </tr>
                    <?php /*} */?>
                <?php /*endif; */?>
                </tbody>
            </table>
        </div>-->
        <!-- /highlighting rows and columns -->

        <!-- Add Cheque modal -->
        <div id="add_cheque" class="modal fade">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h6 class="modal-title">Create Cheque</h6>
                    </div>

                    <div class="modal-body">
                        <?php echo form_open(base_url('admin/accounts/add_cheque'),array('class'=>"form-horizontal",'id'=>'form')); ?>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Bank Name <span class="text-danger">*</span></label>
                                                <select name="bank_name" id="bankname1" onchange="selectBranches(this.value)" class="form-control mb-5 select_box" data-width="100%" required>
                                                    <option>Select Bank</option>
                                                    <?php foreach ($banks as $bank): ?>
                                                        <option value="<?php echo $bank->B_ID; ?>"><?php echo $bank->B_NAME; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">Branch Name <span class="text-danger">*</span></label>
                                                <select name="branch_name" id="branch_name" onchange="getAccountId()" class="form-control mb-5 select_box" data-width="100%" required>
                                                </select>
                                                <input type="hidden" name="account_id" id="ac_id">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Payee <span class="text-danger">*</span></label>
                                                <input type="text" name="ch_user" class="form-control mb-5" required>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">Debit Account <span class="text-danger">*</span></label>
                                                <!--<select name="debit_account" class="form-control mb-5 select_box" data-width="100%" required>
                                                    <?php /*if(!empty($accounts)):
                                                        foreach ($accounts as $account):
                                                            $client = ($account->CLIENT_ID == "")?'':" (".client_name($account->CLIENT_ID).")";
                                                            */?>
                                                            <option value="<?php /*echo $account->A_ID; */?>"><?php /*echo $account->A_NAME.$client; */?></option>
                                                        <?php /*endforeach; */?>
                                                    <?php /*endif; */?>
                                                </select>-->
												<select class="form-control filterAccounts" name="debit_account"  data-width="100%" required>
												 	<option value="">---</option>
												</select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Cheque No <span class="text-danger">*</span></label>
                                                <input type="text" name="ch_no"class="form-control mb-5" required>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">Amount <span class="text-danger">*</span></label>
                                                <input type="number" name="ch_amount" onkeyup="ch_amountWords.value=numberToEnglish(this.value)" class="form-control mb-5" min="0" step="any" required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Amount in Words</label>
                                                <input type="text" name="ch_amountWords" id="ch_amountWords" class="form-control mb-5" readonly>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">Date <span class="text-danger">*</span></label>
                                                <input type="text" name="ch_date" class="form-control datepicker" required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Description</label>
                                                <textarea name="ch_desc" class="form-control mb-5"></textarea>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>&nbsp;</label>
                                                <div>
                                                    <input name="ch_payees" type="checkbox" class="custom-checkbox" value="1">  A/C Payees Only
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input  type="submit" class="btn btn-primary"  value="Create Cheque"/>

                        </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Add Cheque -->
        <!-- Edit Cheque modal -->
        <div id="edit_cheque" class="modal fade">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h6 class="modal-title">Edit Cheque</h6>
                    </div>

                    <div class="modal-body">
                        <?php echo form_open(base_url('admin/accounts/update_cheque_status'),array('class'=>"form-horizontal form",'id'=>'')); ?>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="hidden" name="ch_id" id="ch_id">
                                        <input type="hidden" name="tr_id" id="tr_id">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Bank Name <span class="text-danger">*</span></label>
                                                <select name="bank_name" id="b_name" onchange="selectBranches(this.value)" class="form-control select_box" data-width="100%" required>
                                                    <?php foreach ($banks as $bank): ?>
                                                        <option value="<?php echo $bank->B_ID; ?>"><?php echo $bank->B_NAME; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">Branch Name <span class="text-danger">*</span></label>
                                                <select name="branch_name" id="br_name" class="form-control select_box" data-width="100%" required>
                                                </select>
                                                <input type="hidden" name="account_id" id="ac_id1">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Payee <span class="text-danger">*</span></label>
                                                <input type="text" name="ch_user" id="ch_user" class="form-control mb-5" required>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">Debit Account <span class="text-danger">*</span></label>
												<select class="form-control filterAccounts selectedAccount" name="debit_account"  data-width="100%" required>
												 	<option value="">---</option>
												</select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Cheque No <span class="text-danger">*</span></label>
                                                <input type="text" name="ch_no" id="ch_no" class="form-control mb-5" required>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">Amount <span class="text-danger">*</span></label>
                                                <input type="number" name="ch_amount" id="ch_amount" onkeyup="chamountWords.value=numberToEnglish(this.value)" class="form-control mb-5" min="0" step="any" required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Amount in Words</label>
                                                <input type="text" name="ch_amountWords" id="chamountWords" class="form-control mb-5" readonly>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">Date <span class="text-danger">*</span></label>
                                                <input type="text" name="ch_date" id="ch_date" class="form-control datepicker" required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Description</label>
                                                <textarea name="ch_desc" id="ch_desc" class="form-control mb-5"></textarea>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>&nbsp;</label>
                                                <div>
                                                    <input name="ch_payees" id="ch_payees" type="checkbox" class="custom-checkbox" value="1"> A/C Payees Only
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="control-label">Status <span class="text-danger">*</span></label>
                                                <div class="form-check">
                                                    <label class="radio-inline"><input type="radio" name="ch_status" id="ch_status" value="Pending" required>Pending</label>
                                                    <label class="radio-inline"><input type="radio" name="ch_status" id="ch_status1" value="Approved" required>Approved</label>
                                                    <label class="radio-inline"><input type="radio" name="ch_status" id="ch_status2" value="Cancel" required>Cancel</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input  type="submit" class="btn btn-primary"  value="Edit Cheque"/>
                        </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Edit Cheque Modal -->
        <!-- edit select Cheque id script -->
        <script type='text/javascript'>
            function getEditCheque(id)
            {
                $('document').ready(function(){
                    var controlmodal=$('#edit_cheque');
                    var option={
                        "backdrop" : "static"
                    }
                    controlmodal.modal(option);
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url("admin/accounts/ajax_edit_cheque");?>'+"/"+id,
                        data:false,
                        contentType: false,
                        dataType:"json",
                        success:function(data){
                            $('#br_name').empty();
                            $.each(data.branch, function (index, value) {
                                $('#br_name').append('<option value="' + value.BR_ID + '">' + value.BR_NAME + '</option>');
                            });
                            $('#ch_id').val(data.ch_data['C_ID']);
                            $('#tr_id').val(data.ch_data['T_ID']);
                            $('#b_name').val(data.ch_data['B_ID']);
                            $('#br_name').val(data.ch_data['BR_ID']);
                            $('#ch_user').val(data.ch_data['C_PAY']);
                            $('#debit_account').val(data.ch_data['DB_ACCOUNTID']);
                            $('#ch_no').val(data.ch_data['C_NO']);
                            $('#ch_amount').val(data.ch_data['C_AMOUNT']);
                            $('#chamountWords').val(data.ch_data['C_AMOUNTWORDS']);
                            $('#ch_date').val(data.ch_data['C_DATE']);
                            $('#ch_desc').val(data.ch_data['C_DESC']);
                             if(data.ch_data['AC_PAYEES']==1){
                                $('#ch_payees').prop('checked',true);
                            }else{
                                $('#ch_payees').prop('checked',false);
                            }
                            if(data.ch_data['C_STATUS']=='Pending'){
                                $('#ch_status').prop('checked',true);
                            }else if(data.ch_data['C_STATUS']=='Approved'){
                                $('#ch_status1').prop('checked',true);
                            }else if(data.ch_data['C_STATUS']=='Cancel'){
                                $('#ch_status2').prop('checked',true);
                            }
                            $('.select_box').select2({});
                            getAccountId('edit');
                            getSelectedValue(data.ch_data['DB_ACCOUNTID']);
                        },
                        error:function(data){
                            console.log(data.responseText + "not work");
                        },
                    });
                });
            }
        </script>
        <!-- edit select Cheque id script -->
        <!-- Select Branch on bank basis -->
        <script type="text/javascript">
            function selectBranches(val) {
                var option="";
                if($('#ch_id').val()!=''&&$('#edit_cheque').is(':visible')){
                    option=$('#br_name');
                }else {
                    option=$('#branch_name');
                }
                $.getJSON( "<?php echo site_url('admin/accounts/ajax_select_branches') ?>"+"/"+val, function(result) {
                    option.empty();
                    $.each(result, function (index, value) {
                        option.append('<option value="' + value.BR_ID + '">' + value.BR_NAME + '</option>');
                    });
                    $('.select_box').select2({});
                    getAccountId();
                });
            }
        </script>
        <!-- /Select Branch on bank basis -->
        <!-- Select Bank Account Id-->
        <script type="text/javascript">
            function getAccountId(type){
                var option="";
                var account_id="";
                if(type == 'edit'){
                   option=$('#br_name');
                    account_id=$('#ac_id1');
                }else {
                    option=$('#branch_name');
                    account_id=$('#ac_id');
                }
                $.getJSON( "<?php echo site_url('admin/accounts/ajax_get_branch_aid') ?>"+"/"+option.val(), function(result) {
                    if(result != null){
                        account_id.val(result.A_ID);
                    }
                });
            }
        </script>
        <!-- /Select Bank Account Id-->
        <script>
            var data_url='<?= base_url('admin/accounts/cheques_list/') ?>';
        </script>
		