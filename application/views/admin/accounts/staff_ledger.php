<!-- style form css -->
<style>
    .redborder1{
        border:1px solid red;
    }
    .redborder2{
        border:1px solid red;
    }
</style>
<!-- /style form css -->
<!-- Content area -->
<div class="content">
    <!-- Highlighting rows and columns -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <?php echo form_open(base_url('admin/accounts/staff_ledgers'),array('class'=>"form-horizontal")); ?>
            <div class="row">
                <div class="col-xs-2">
                    <input type="text" name="start_date" class="form-control datepicker" placeholder="From Date" autocomplete="off">
                </div>
                <div class="col-xs-2">
                    <input type="text" name="end_date" class="form-control datepicker" placeholder="To Date" autocomplete="off">
                </div>
                <div class="col-xs-2">
                    <select name="accounts" class="form-control select_box" style="width: 100%">
                        <?php if(!empty($all_staffs)):
                            foreach ($all_staffs as $staff): ?>
                                <option value="<?php echo $staff->account_id; ?>"><?php echo $staff->staff_name; ?></option>
                            <?php endforeach;
                            endif; ?>
                    </select>
                </div>
                <div class="col-xs-2">
                    <label class="checkbox-inline" style="margin-top: 5px;"><input type="checkbox" name="opening"> Opening Balance</label>
                </div>
                <div class="col-xs-2">
                    <button class="btn btn-sm btn-success">Filter</button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
        <div class="panel-body">
            <div class="row" id="export_table">
                <table class="table table-bordered table-responsive table-hover">
                    <thead>
                    <tr>
                        <th colspan="8" style="text-align: center"><?= config_item('company_name') ?></th>
                    </tr>
                    <tr>
                        <th colspan="8" style="text-align: center">Staff Ledger of  <?= (!empty($staff_name))?$staff_name:'' ?> (<?= (!empty($ac))?'Include Opening':'Exclude Opening' ?>)</th>
                    </tr>
                    <tr>
                        <th colspan="8" style="text-align: center"><?php
                            if (!empty($start_date)){
                                echo date('d-m-Y',strtotime($start_date)).' UPTO '.date('d-m-Y',strtotime($end_date));
                            }else{
                                echo date('d-m-Y').' UPTO '.date('d-m-Y');
                            }
                            ?> </th>
                    </tr>
                    <tr style="border-top: 1px solid #dddddd;">
                        <th width="10%">Date</th>
                        <th width="10%">Voucher Type</th>
                        <th width="10%">Voucher Number</th>
                        <th width="15%">Account Name</th>
                        <th width="25%">Particulars</th>
                        <th width="10%">Debit</th>
                        <th width="10%">Credit</th>
                        <th width="10%">Balance</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $sum_credit=0; $sum_debit=0;
                    foreach ($ac as $ac):
                        #------Opening Balance Section---#
                        $opening_balance=0;
                        if($ac->A_OPENINGTYPE=='Debit'){
                            $opening_balance+=$ac->A_OPENINGBALANCE;
                        }elseif($ac->A_OPENINGTYPE=='Credit'){
                            $opening_balance-=$ac->A_OPENINGBALANCE;
                        }
                        $debit_transactions = transaction_date($ac->A_ID,$start_date,"Debit");
                        $credit_transactions = transaction_date($ac->A_ID,$start_date,"Credit");

                        $opening_balance=$opening_balance+$debit_transactions->amount-$credit_transactions->amount;

                        #------Opening Balance Section---#
                        if($opening_balance): ?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Opening Balance</td>
                                <td><?php if($opening_balance>0){
                                        echo number_format($opening_balance,2);
                                        $sum_debit+=$opening_balance;
                                    } ?></td>
                                <td><?php if($opening_balance<0){
                                        echo number_format(abs($opening_balance),2);
                                        $sum_credit-=$opening_balance;
                                    } ?></td>
                                <td><?= number_format(abs($sum_debit-$sum_credit),2);
                                    if($sum_debit-$sum_credit<0){
                                        echo "<span style='float:right;'>Cr</span>";
                                    }elseif($sum_debit-$sum_credit>0){
                                        echo "<span style='float:right;'>Dr</span>";
                                    }?></td>
                            </tr>
                        <?php endif;
                        endforeach;
                    if(!empty($general_ledgers)):
                        foreach ($general_ledgers as $g){ ?>
                            <tr>
                                <td><?php echo date('d-m-Y',strtotime($g->T_DATE)); ?></td>
                                <td><?php
                                    $voucher_type=$g->T_TYPE;
                                    if (strpos($g->T_TYPE, ' - Misc') !== false) {
                                        $voucher_type = str_replace(' - Misc', '', $g->T_TYPE);
                                    }elseif (strpos($g->T_TYPE, ' - Job') !== false){
                                        $voucher_type = str_replace(' - Job', '', $g->T_TYPE);
                                    }
                                    echo $voucher_type; ?></td>
                                <td><?= get_voucher_no($voucher_type,$g->T_ID); ?></td>
                                <td><?php if($g->TM_TYPE=='Credit') {
                                        if(!empty(read_debit_transactions($g->T_ID))){
                                            echo read_debit_transactions($g->T_ID)[0]->A_NAME;
                                        }
                                    }elseif($g->TM_TYPE=='Debit'){
                                        if(!empty(read_credit_transactions($g->T_ID))){
                                            echo read_credit_transactions($g->T_ID)[0]->A_NAME;
                                        }
                                    } ?></td>
                                <td><?php echo $g->PARTICULARS; ?></td>
                                <td>
                                    <?php if($g->TM_TYPE=='Debit'){
                                        echo number_format($g->TM_AMOUNT,2);
                                        $sum_debit+=$g->TM_AMOUNT;
                                    } ?>
                                </td>
                                <td>
                                    <?php if($g->TM_TYPE=='Credit'){
                                        echo number_format($g->TM_AMOUNT,2);
                                        $sum_credit+=$g->TM_AMOUNT;
                                    } ?>
                                </td>
                                <td><?= number_format(abs($sum_debit-$sum_credit),2);
                                    if($sum_debit-$sum_credit<0){
                                        echo "<span style='float:right;'>Cr</span>";
                                    }elseif($sum_debit-$sum_credit>0){
                                        echo "<span style='float:right;'>Dr</span>";
                                    }
                                    ?></td>
                            </tr>
                        <?php }
                     endif; ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="5"></td>
                        <td><?php echo number_format($sum_debit,2); ?></td>
                        <td><?php echo number_format($sum_credit,2); ?></td>
                        <td><?php echo number_format(abs($sum_debit-$sum_credit),2);
                            if($sum_debit-$sum_credit<0){
                                echo "<span style='float:right;'>Cr</span>";
                            }elseif($sum_debit-$sum_credit>0){
                                echo "<span style='float:right;'>Dr</span>";
                            }
                            ?></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!--<button class="btn btn-sm pull-right" style="color: #fff;background-color: #26a69a" id="btnExport">Export to Excel</button>-->
        </div>
    </div>
    <!-- /highlighting rows and columns -->

    <!--Jquery Datepicker-->
    <script>
        $( function() {
            $( ".datepicker" ).datepicker({dateFormat: 'dd-mm-yy'});
        } );
    </script>
    <!--Jquery Datepicker-->

    <script>
        $("#btnExport").click(function (e) {
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('#export_table').html() ));
            e.preventDefault();
        });
    </script>

