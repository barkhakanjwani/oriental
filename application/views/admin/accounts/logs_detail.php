<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="<?php echo base_url('admin/home');?>"><i class="fa fa-home"></i> Home</a></li>
                <li class="active">Logs Detail</li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <div class="panel panel-flat">
            <?php echo form_open(base_url('admin/logs_detail'),array('class'=>"form-horizontal",'id'=>'logs_detail')); ?>
            <div class="row panel-heading">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-sm-2">
                            <label>Select Month</label>
                            <select class="form-control" name="month">
                                <option value="Jan">January</option>
                                <option value="Feb">February</option>
                                <option value="Mar">March</option>
                                <option value="Apr">April</option>
                                <option value="May">May</option>
                                <option value="Jun">June</option>
                                <option value="Jul">July</option>
                                <option value="Aug">August</option>
                                <option value="Sep">September</option>
                                <option value="Oct">October</option>
                                <option value="Nov">November</option>
                                <option value="Dec">December</option>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <label>Select Year</label>
                            <select class="form-control" name="year">
                                <?php for($i=2017;$i<=date('Y');$i++){ ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <input type="submit" style="color: #fff;background-color: #26a69a; margin-top:27px;" class="btn btn-xs" value="Filter"/>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close();?>
        </div>

        <!-- Highlighting rows and columns -->
        <div class="panel panel-flat">
            <table class="table table-bordered table-hover datatable-basic">
                <thead>
                <tr>
                    <th>User Name</th>
                    <th>Time</th>
                    <th>IP Address</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php if(!empty($logs_detail)): ?>
                    <?php foreach ($logs_detail as $log){ ?>
                        <tr style="white-space: nowrap;">
                            <td><?php echo $log->U_NAME; ?></td>
                            <td><?php echo $log->SL_DATETIME; ?></td>
                            <td><?php echo $log->IP_ADDRESS; ?></td>
                            <td><?php echo $log->SL_ACTION; ?></td>
                        </tr>
                    <?php } ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <!-- /highlighting rows and columns -->