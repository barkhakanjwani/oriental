<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<!-- /style form css -->
<!-- Content area -->
<div class="content">

    <!-- Highlighting rows and columns -->
    <div class="panel panel-flat">
        <table class="table table-bordered table-hover DataTables" id="DataTables">
            <thead>
            <tr>
                <th><?= lang('reference_no') ?></th>
                <th><?= lang('client') ?></th>
                <th>Cheque/Payorder No</th>
                <th>Date</th>
                <th>Amount</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php if(!empty($advances)): ?>
                <?php foreach ($advances as $advance){
                    $job_info = $this->invoice_model->check_by(array('invoices_id'=>$advance->invoices_id), 'tbl_invoices');
                    $client_info = $this->invoice_model->check_by(array('client_id'=>$job_info->client_id), 'tbl_client');
                    ?>
                    <tr style="white-space: nowrap;">
                        <td><?php echo $job_info->reference_no; ?></td>
                        <td><?php echo $client_info->name; ?></td>
                        <td><?php echo $advance->cheque_payorder; ?></td>
                        <td><?php echo date('d-m-Y',strtotime($advance->payment_date)); ?></td>
                        <td><?php echo number_format($advance->amount,2); ?></td>
                        <td>
                            <?= btn_edit('admin/accounts/manage_direct_payment/edit/'.$advance->ap_id) ?>
                            <?= btn_view('admin/accounts/manage_direct_payment/view/'.$advance->ap_id) ?>
                        </td>
                    </tr>
                <?php } ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
    <!-- /highlighting rows and columns -->
