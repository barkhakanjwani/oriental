<?php echo message_box('success'); ?>

<?php echo message_box('error'); ?>
<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= $title ?></h1>
<div class="row">
    <div class="col-lg-12">
        <form role="form" enctype="multipart/form-data" id="form_v" action="<?php echo base_url(); ?>admin/accounts/save_reversal_form" method="post" class="form-horizontal  ">
            <section class="panel panel-default">
                <header class="panel-heading"><?= $title ?></header>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><?= lang('voucher_type') ?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <select name="type" class="form-control select_box" id="type" data-width="100%" required>
                                <option value="">---</option>
                                <option value="1">Payment Voucher</option>
                                <option value="2">Receipt Voucher</option>
                                <option value="3">General Expense</option>
                                <!--<option value="4">Job Expense</option>-->
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><?= lang('voucher_no') ?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <input type="text" name="voucher_no" id="voucher_no" class="form-control mb-5" autocomplete="off" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><?= lang('description') ?></label>
                        <div class="col-lg-3">
                            <textarea name="desc" class="form-control mb-5"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"></label>
                        <div class="col-lg-4">
                            <button type="button" class="btn btn-sm btn-success pull-right" onclick="GetDetails()"><?= lang('get_details') ?></button>
                        </div>
                    </div>
                    <div id="show_details">
                    </div>
                </div>
            </section>
        </form>
    </div>
</div>
<script>
    function GetDetails() {
        $.ajax({
            type: 'POST',
            url: '<?= base_url('admin/accounts/getEntryDetails') ?>',
            data: {"voucher_type":$('#type').val(),"voucher_no":$('#voucher_no').val()},
            dataType: 'JSON',
            success: function (result) {
                if (result != ''){
                    $('#show_details').empty();
                    $('#show_details').append(result);
                }
            },
            error: function (error) {
                console.log('Error'+error.responseText);
            }
        });
    }
</script>