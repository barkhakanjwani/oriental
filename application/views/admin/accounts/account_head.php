<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<section class="content-header">
    <div class="row">
		<div class="col-md-12">
			 <span class="header-default" style="">Head of Accounts List</span> <a href="#" data-toggle="modal" data-target="#add_heads" class="btn btn-danger btn-xs" style="vertical-align: text-bottom;"><?= lang('add_new') ?></a>
		</div>
    </div>
</section>
    <!-- Content area -->
    <div class="content">
        <!-- Highlighting rows and columns -->
        <div class="panel panel-flat">
            <table class="table table-bordered table-hover DataTables " id="DataTables">
                <thead>
                <tr>
                    <th>Type - Sub Type</th>
                    <th>Head Name</th>
                    <th>Head No</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php if(!empty($accountsHead)): ?>
                    <?php foreach ($accountsHead as $ah){ ?>
                        <tr style="white-space: nowrap;">
                            <td><?php echo $ah->H_TYPE." - ".$ah->NAME; ?></td>
                            <td><?php echo $ah->H_NAME; ?></td>
                            <td><?php echo $ah->H_NO; ?></td>
                            <td class="text-center"><?php echo ($ah->H_STATUS==1)?"<label class='label label-success'>Active</label>":"<label class='label label-danger'>Inactive</label>"; ?></td>
                            <td class="text-center">
                                <button type="button" onclick="getEditHead(this.value)" value="<?php echo $ah->H_ID;?>" data-popup="tooltip" title="" data-container="body" data-original-title="Edit Account Head" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#" ><i class="fa fa-edit"></i></button>
                                <!--<button type="button" value="<?php echo $ah->H_ID;?>" data-popup="tooltip" title="" data-container="body" data-original-title="Delete Account Head" class="btn btn-danger btn-xs delete"><i class="fa fa-trash"></i></button>-->
                            </td>
                        </tr>
                    <?php } ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <!-- /highlighting rows and columns -->
        <!-- Add Head modal -->
		<div class="modal fade" id="add_heads">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Account Head</h4>
              </div>
                <?php echo form_open(base_url('admin/accounts/add_head'),array('class'=>"form-horizontal",'id'=>'form')); ?>
              <div class="modal-body">
				  <div class="row">
						<div class="col-md-12">
							<div class="col-sm-12">
								<label class="control-label">Name <span class="text-danger">*</span></label>
								<input type="text" name="h_name" placeholder="Name" class="form-control mb-5" required>
							</div>
							<div class="col-sm-12">
								<label class="control-label">Type <span class="text-danger">*</span></label>
								<select name="h_type" class="form-control mb-5 select_box" data-width="100%" onchange="ReadSubType(this.value)" id="head_type" required>
									<option value="Assets">Assets</option>
									<option value="Liabilities">Liabilities</option>
									<option value="Income">Income</option>
									<option value="Expense">Expense</option>
									<option value="Equity">Equity</option>
								</select>
							</div>
							<div class="col-sm-12">
								<label class="control-label">Sub Type <span class="text-danger">*</span></label>
								<select name="SUB_HEAD_ID"  class="form-control mb-5 select_box" data-width="100%" id="subType1" required>
								</select>
							</div>
							<div class="col-sm-12">
								<label class="control-label">Status <span class="text-danger">*</span></label>
								<Select name="h_status"  class="form-control mb-5 select_box" data-width="100%" required>
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>
							</div>
						</div>
				  </div>
              </div>
              <div class="modal-footer">
				  <div class="col-md-12">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<input  type="submit" name="btn_addHead" class="btn btn-primary"  value="Add Account Head" />
				  </div>
              </div>
                <?php echo form_close();?>
            </div>
          </div>
        </div>
        <!-- /Add Head modal -->
        <!-- Edit Head modal -->
		<div class="modal fade" id="edit_head">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Account Head</h4>
              </div>
                <?php echo form_open(base_url('admin/accounts/edit_head'),array('class'=>"form-horizontal form",'id'=>'')); ?>
				<div class="modal-body">
				  <div class="row">
						<div class="col-md-12">
							<input type="hidden" name="H_ID" id="h_id">
							<div class="col-sm-12">
								<label class="control-label">Name <span class="text-danger">*</span></label>
								<input type="text" name="H_NAME" id="h_name" placeholder="Name" class="form-control mb-5" required>
							</div>
							<div class="col-sm-12">
								<label class="control-label">Type <span class="text-danger">*</span></label>
								<select name="H_TYPE" id="h_type" class="form-control mb-5 select_box" data-width="100%" onchange="ReadSubType(this.value)" required>
									<option value="Assets">Assets</option>
									<option value="Liabilities">Liabilities</option>
									<option value="Income">Income</option>
									<option value="Expense">Expense</option>
									<option value="Equity">Equity</option>
								</select>
							</div>
							<div class="col-sm-12">
								<label class="control-label">Sub Type <span class="text-danger">*</span></label>
								<select name="SUB_HEAD_ID"  class="form-control mb-5 select_box" data-width="100%" id="subType2" required>
								</select>
							</div>
							<div class="col-sm-12">
								<label class="control-label">Status <span class="text-danger">*</span></label>
								<select name="H_STATUS" id="h_status"  class="form-control mb-5 select_box" data-width="100%" required>
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>
							</div>
						</div>
				  </div>
              </div>
              <div class="modal-footer">
				  <div class="col-md-12">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					  	<input  type="submit" name="btn_editBank" class="btn btn-primary"  value="Edit Account Head"/>
				  </div>
              </div>
                <?php echo form_close();?>
            </div>
          </div>
        </div>
        <!-- /Edit Head Modal -->
        <!-- edit select Head id script -->
        <script type='text/javascript'>
            function getEditHead(id)
            {
                $('document').ready(function(){
                    var controlmodal=$('#edit_head');
                    var option={
                        "backdrop" : "static"
                    }
                    controlmodal.modal(option);
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url("admin/accounts/ajax_edit_head");?>'+"/"+id,
                        data:false,
                        contentType: false,
                        dataType:"json",
                        success:function(data){
                            $('#subType2').empty();
                            $.each(data.dropdown, function (index, value) {
                                $('#subType2').append('<option value="' + value.SUB_HEAD_ID + '">' + value.NAME + '</option>');
                            });
                            $('#h_id').val(data.edit_data['H_ID']);
                            $('#h_name').val(data.edit_data['H_NAME']);
                            $('#h_type').val(data.edit_data['H_TYPE']);
                            $('#subType2').val(data.edit_data['SUB_HEAD_ID']);
                            $('#h_status').val(data.edit_data['H_STATUS']);
                            $('.select_box').select2({});
                        },
                        error:function(data){
                            console.log(data.responseText + "not work")
                        },
                    });
                });
            }
        </script>
        <!-- edit select Head id script -->
        <!-- Select Sub Type on Head Type-->
        <script type="text/javascript">
            $(document).ready(function () {
                ReadSubType($('#head_type').val());
            });
            function ReadSubType(val) {
                var option="";
                if($('#h_id').val()!='' && $('#edit_head').is(':visible')){
                    option=$('#subType2');
                }else {
                    option=$('#subType1');
                }
                $.getJSON( "<?php echo site_url('admin/accounts/select_sub_head') ?>"+"/"+val, function(result) {
                    option.empty();
                    $.each(result, function (index, value) {
                        option.append('<option value="' + value.SUB_HEAD_ID + '">' + value.NAME + '</option>');
                    });
                    $('.select_box').select2({});
                });
            }
        </script>
        <!--/Select Sub Type on Head Type-->