<section class="content-header">
    <div class="row">
        <div class="col-md-12">
            <span class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('reversal_list') ?></span> <a href="<?= base_url('admin/accounts/reversal_form')?>" class="btn btn-danger btn-xs" style="vertical-align: text-bottom;"><?= lang('add_new') ?></a>
        </div>
    </div>
</section>
<!-- Content area -->
<div class="content">
    <!-- Highlighting rows and columns -->
    <!--<div class="panel panel-flat">
        <div class="panel-body">
            <table class="table table-bordered table-hover DataTables" id="DataTables">
                <thead>
                <tr>
                    <th><?/*= lang('voucher_type') */?></th>
                    <th><?/*= lang('voucher_no') */?></th>
                    <th><?/*= lang('reverse_date') */?></th>
                    <th><?/*= lang('description') */?></th>
                </tr>
                </thead>
                <tbody>
                <?php /*if(!empty($reversal_entries)): */?>
                    <?php /*foreach ($reversal_entries as $reversal_entry){
                        if ($reversal_entry->voucher_type==1){
                            $voucher_type='Payment Voucher';
                            $payment_voucher = $this->invoice_model->check_by(array('PV_ID'=>$reversal_entry->voucher_id),'payment_vouchers');
                            $voucher_no=$payment_voucher->PV_VOUCHER_NO;
                        }elseif ($reversal_entry->voucher_type==2){
                            $voucher_type='Receipt Voucher';
                            $receipt_voucher = $this->invoice_model->check_by(array('ap_id'=>$reversal_entry->voucher_id),'tbl_advance_payments');
                            $voucher_no=$receipt_voucher->voucher_no;
                        }elseif ($reversal_entry->voucher_type==3){
                            $voucher_type='General Expense';
                            $receipt_voucher = $this->invoice_model->check_by(array('PC_ID'=>$reversal_entry->voucher_id),'petty_cash');
                            $voucher_no=$receipt_voucher->PC_VOUCHER_NO;
                        } */?>
                        <tr style="white-space: nowrap;">
                            <td><?/*= $voucher_type */?></td>
                            <td><?/*= $voucher_no */?></td>
                            <td><?/*= $reversal_entry->created_at */?></td>
                            <td><?/*= $reversal_entry->description */?></td>
                        </tr>
                    <?php /*} */?>
                <?php /*endif; */?>
                </tbody>
            </table>
        </div>
    </div>-->
    <!-- /highlighting rows and columns -->
    <script>
        $('#type').change(function () {
            var type=$(this).val();
            location.href= '<?= base_url('admin/accounts/payment_voucher_list/') ?>'+type;
        });
    </script>

    <section class="panel panel-default">

        <header class="panel-heading"><?= lang('reversal_list') ?></header>

        <div class="panel-body" id="export_table">

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive" id="customers">
                        <table class="table table-bordered data-table-list dtHorizontalExample" id="tab_customers">
                            <thead>
                            <th><?= lang('voucher_type') ?></th>
                            <th><?= lang('voucher_no') ?></th>
                            <th><?= lang('reverse_date') ?></th>
                            <th><?= lang('description') ?></th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <script>
        var data_url='<?= base_url('admin/accounts/reversal_lists') ?>';
    </script>