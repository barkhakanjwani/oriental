<!-- Content area -->
<div class="content">

    <!-- Highlighting rows and columns -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <?php echo form_open(base_url('admin/accounts/trial_balance'),array('class'=>"form-horizontal")); ?>
            <div class="row">
                <div class="col-xs-2">
                    <input type="text" name="start_date" class="form-control datepicker" placeholder="From Date" autocomplete="off">
                </div>
                <div class="col-xs-2">
                    <input type="text" name="end_date" class="form-control datepicker" placeholder="To Date" autocomplete="off">
                </div>
                <div class="col-xs-2">
                    <label class="checkbox-inline">
                        <input type="checkbox" name="opening" value="1"> Opening Balance
                    </label>
                </div>
                <div class="col-xs-2">
                    <button class="btn btn-sm btn-success"><i class="fa fa-search"></i></button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
        <div class="panel-body">
            <div class="row" id="export_table">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th width="25%">Account Name</th>
                        <th width="15%">Account No</th>
                        <?php if($opening_value==1): ?>
                            <th width="15%">Opening Balance</th>
                        <?php endif; ?>
                        <th width="15%">Debit</th>
                        <th width="15%">Credit</th>
                        <th width="15%">Closing Balance</th>
                        <th width="5%">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($sub_types)): ?>
                        <?php $total_opening=0; $total_debit=0; $total_credit=0;$opening_balance=0; $balance=0; ?>
                        <?php foreach ($sub_types as $sb){
                            ?>
                            <tr>
                                <th><?= $sb->NAME; ?></th>
                                <th><?= $sb->SUB_NO; ?></th>
                                <?php $sub_op=0;
                                if($opening_value==1):
                                    $op_bal_debit=opening_bal_by_sub_head($sb->SUB_HEAD_ID,"Debit")->opening_balance;
                                    $op_bal_credit=opening_bal_by_sub_head($sb->SUB_HEAD_ID,"Credit")->opening_balance;
                                    $head_debit_tr=transaction_date_by_subhead($sb->SUB_HEAD_ID,$start_date,"Debit")->amount;
                                    $head_credit_tr=transaction_date_by_subhead($sb->SUB_HEAD_ID,$start_date,"Credit")->amount;
                                    if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                        $sub_op+=($op_bal_debit-$op_bal_credit)-$head_debit_tr+$head_credit_tr;
                                    }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                        $sub_op+=($op_bal_debit-$op_bal_credit)+$head_debit_tr-$head_credit_tr;
                                    }
                                    $total_opening+=$sub_op;
                                    ?>
                                    <th><?= number_format(abs($sub_op),2);
                                        if($sub_op<0){
                                            $opening_balance-=$sub_op;
                                            echo "<span style='float:right;'>Cr</span>";
                                        }elseif($sub_op>0){
                                            $opening_balance+=$sub_op;
                                            echo "<span style='float:right;'>Dr</span>";
                                        }
                                        ?></th>
                                <?php endif;  ?>
                                <?php $sub_head_debit=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Debit")->amount;
                                $sub_head_credit=transactions_by_sub_type($sb->SUB_HEAD_ID,$start_date,$end_date,"Credit")->amount; ?>
                                <th><?= number_format(abs($sub_head_debit),2);
                                    $total_debit+=$sub_head_debit;
                                    ?></th>
                                <th><?= number_format(abs($sub_head_credit),2);
                                    $total_credit+=$sub_head_credit;
                                    ?></th>
                                <th><?php $sub_head_bal=0;
                                    if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                        $sub_head_bal+=$sub_op-$sub_head_debit+$sub_head_credit;
                                    }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                        $sub_head_bal+=$sub_op+$sub_head_debit-$sub_head_credit;
                                    }
                                    echo number_format(abs($sub_head_bal),2); ?></th>
                                <th><?php if($sub_head_bal<0){
                                        $balance-=$sub_head_bal;
                                        echo "Cr";
                                    }elseif($sub_head_bal>0){
                                        $balance-=$sub_head_bal;
                                        echo "Dr";
                                    }
                                    ?></th>
                            </tr>
                            <?php $account_head=account_head_by_subId($sb->SUB_HEAD_ID);?>

                            <?php foreach ($account_head as $ah){ ?>

                                <?php $accounts = account_by_head($ah->H_ID); ?>
                                <tr>
                                    <td><?= $ah->H_NAME; ?></td>
                                    <td><?= $ah->H_NO; ?></td>
                                    <?php $head_op=0;
                                    if($opening_value==1):
                                        $head_op_debit=opening_bal_by_head($ah->H_ID,"Debit")->opening_balance;
                                        $head_op_credit=opening_bal_by_head($ah->H_ID,"Credit")->opening_balance;
                                        $Ahead_debit_tr=transaction_date_by_head($ah->H_ID,$start_date,"Debit")->amount;
                                        $Ahead_credit_tr=transaction_date_by_head($ah->H_ID,$start_date,"Credit")->amount;
                                        if($ah->H_TYPE=='Liabilities' || $ah->H_TYPE=='Equity' || $ah->H_TYPE=='Income'){
                                            $head_op+=($head_op_debit-$head_op_credit)-$Ahead_debit_tr+$Ahead_credit_tr;
                                        }elseif ($ah->H_TYPE=='Assets' || $ah->H_TYPE=='Expense'){
                                            $head_op+=($head_op_debit-$head_op_credit)+$Ahead_debit_tr-$Ahead_credit_tr;
                                        }
                                        ?>
                                        <td><?= number_format(abs($head_op),2);
                                            if($head_op<0){
                                                echo "<span style='float:right;'>Cr</span>";
                                            }elseif($head_op>0){
                                                echo "<span style='float:right;'>Dr</span>";
                                            }
                                            ?></td>
                                    <?php endif; ?>
                                    <?php $head_debit=transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Debit")->amount; ?>
                                    <?php $head_credit=transactions_by_account_head($ah->H_ID,$start_date,$end_date,"Credit")->amount; ?>
                                    <td><?= number_format(abs($head_debit),2) ?></td>
                                    <td><?= number_format(abs($head_credit),2) ?></td>
                                    <td><?php $head_bal=0;
                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                            $head_bal+=$head_op-$head_debit+$head_credit;
                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                            $head_bal+=$head_op+$head_debit-$head_credit;
                                        }
                                        echo number_format(abs($head_bal),2);?></td>
                                    <td><?php if($head_bal<0){
                                            echo "Cr";
                                        }elseif($head_bal>0){
                                            echo "Dr";
                                        }
                                        ?></td>
                                </tr>
                                <?php foreach ($accounts as $a){
                                    $account_balance=0;
                                    if($a->A_SELFHEAD_ID==0){ ?>
                                        <tr>
                                            <td><?= $a->A_NAME; ?></td>
                                            <td><?= $a->A_NO; ?></td>
                                            <?php $opening=0;
                                            if($opening_value==1): ?>
                                                <td><?php
                                                    #------Opening Balance Section---#
                                                    $opening+=accounts_by_ids($a->A_ID,"Debit")->opening_balance;
                                                    $opening-=accounts_by_ids($a->A_ID,"Credit")->opening_balance;
                                                    $pre_debit_transactions = transaction_date($a->A_ID,$start_date,"Debit");
                                                    $pre_credit_transactions = transaction_date($a->A_ID,$start_date,"Credit");

                                                    if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                        $opening=$opening-$pre_debit_transactions->amount+$pre_credit_transactions->amount;
                                                    }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                        $opening=$opening+$pre_debit_transactions->amount-$pre_credit_transactions->amount;
                                                    }
                                                    echo number_format(abs($opening),2);
                                                    $account_balance+=$opening;
                                                    if($opening<0){
                                                        echo "<span style='float:right;'>Cr</span>";
                                                    }elseif($opening>0){
                                                        echo "<span style='float:right;'>Dr</span>";
                                                    }
                                                    #------Opening Balance Section---#
                                                    ?></td>
                                            <?php endif; ?>
                                            <td><?php
                                                $debit_transactions=transaction_date_by_self($a->A_ID,$start_date,$end_date,"Debit");
                                                echo number_format($debit_transactions->amount,2);
                                                if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                    $account_balance-=$debit_transactions->amount;
                                                }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                    $account_balance+=$debit_transactions->amount;
                                                }
                                                ?></td>
                                            <td><?php
                                                $credit_transactions=transaction_date_by_self($a->A_ID,$start_date,$end_date,"Credit");
                                                echo number_format($credit_transactions->amount,2);
                                                if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                    $account_balance+=$credit_transactions->amount;
                                                }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                    $account_balance-=$credit_transactions->amount;
                                                }
                                                ?></td>
                                            <td><?= number_format(abs($account_balance),2); ?></td>
                                            <td><?php if($account_balance<0){
                                                    echo "Cr";
                                                }elseif($account_balance>0){
                                                    echo "Dr";
                                                }
                                                ?></td>
                                        </tr>
                                        <?php foreach ($accounts as $ac){
                                            $account_balance=0;
                                            if($ac->A_SELFHEAD_ID==$a->A_ID){ ?>
                                                <tr>
                                                    <td><?= $ac->A_NAME; ?></td>
                                                    <td><?= $ac->A_NO; ?></td>
                                                    <?php $opening=0;
                                                    if($opening_value==1): ?>
                                                        <td><?php
                                                            #------Opening Balance Section---#
                                                            $opening+=accounts_by_ids($ac->A_ID,"Debit")->opening_balance;
                                                            $opening-=accounts_by_ids($ac->A_ID,"Credit")->opening_balance;
                                                            $pre_debit_transactions = transaction_date($ac->A_ID,$start_date,"Debit");
                                                            $pre_credit_transactions = transaction_date($ac->A_ID,$start_date,"Credit");

                                                            if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                                $opening=$opening-$pre_debit_transactions->amount+$pre_credit_transactions->amount;
                                                            }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                                $opening=$opening+$pre_debit_transactions->amount-$pre_credit_transactions->amount;
                                                            }

                                                            echo number_format(abs($opening),2);
                                                            $account_balance+=$opening;
                                                            if($opening<0){
                                                                echo "<span style='float:right;'>Cr</span>";
                                                            }elseif($opening>0){
                                                                echo "<span style='float:right;'>Dr</span>";
                                                            }
                                                            #------Opening Balance Section---#
                                                            ?></td>
                                                    <?php endif; ?>
                                                    <td><?php
                                                        $debit_transactions=transaction_date_by_self($ac->A_ID,$start_date,$end_date,"Debit");
                                                        echo number_format($debit_transactions->amount,2);
                                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                            $account_balance-=$debit_transactions->amount;
                                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                            $account_balance+=$debit_transactions->amount;
                                                        }
                                                        ?></td>
                                                    <td><?php
                                                        $credit_transactions=transaction_date_by_self($ac->A_ID,$start_date,$end_date,"Credit");
                                                        echo number_format($credit_transactions->amount,2);
                                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                            $account_balance+=$credit_transactions->amount;
                                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                            $account_balance-=$credit_transactions->amount;
                                                        }
                                                        ?></td>
                                                    <td><?= number_format(abs($account_balance),2); ?></td>
                                                    <td><?php if($account_balance<0){
                                                            echo "Cr";
                                                        }elseif($account_balance>0){
                                                            echo "Dr";
                                                        }?></td>
                                                </tr>
                                                <?php foreach ($accounts as $account){
                                                    $account_balance=0;
                                                    if($account->A_SELFHEAD_ID==$ac->A_ID){ ?>
                                                        <tr>
                                                            <td><?= $account->A_NAME; ?></td>
                                                            <td><?= $account->A_NO; ?></td>
                                                            <?php $opening=0;
                                                            if($opening_value==1): ?>
                                                                <td><?php
                                                                    #------Opening Balance Section---#
                                                                    $opening+=accounts_by_ids($account->A_ID,"Debit")->opening_balance;
                                                                    $opening-=accounts_by_ids($account->A_ID,"Credit")->opening_balance;
                                                                    $pre_debit_transactions = transaction_date($account->A_ID,$start_date,"Debit");
                                                                    $pre_credit_transactions = transaction_date($account->A_ID,$start_date,"Credit");
                                                                    if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                                        $opening=$opening-$pre_debit_transactions->amount+$pre_credit_transactions->amount;
                                                                    }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                                        $opening=$opening+$pre_debit_transactions->amount-$pre_credit_transactions->amount;
                                                                    }

                                                                    echo number_format(abs($opening),2);
                                                                    $account_balance+=$opening;
                                                                    if($opening<0){
                                                                        echo "<span style='float:right;'>Cr</span>";
                                                                    }elseif($opening>0){
                                                                        echo "<span style='float:right;'>Dr</span>";
                                                                    }
                                                                    #------Opening Balance Section---#
                                                                    ?></td>
                                                            <?php endif; ?>
                                                            <td><?php
                                                                $debit_transactions=transaction_date_by_self($account->A_ID,$start_date,$end_date,"Debit");
                                                                echo number_format($debit_transactions->amount,2);
                                                                if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                                    $account_balance-=$debit_transactions->amount;
                                                                }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                                    $account_balance+=$debit_transactions->amount;
                                                                }
                                                                ?></td>
                                                            <td><?php
                                                                $credit_transactions=transaction_date_by_self($account->A_ID,$start_date,$end_date,"Credit");
                                                                echo number_format($credit_transactions->amount,2);
                                                                if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                                    $account_balance+=$credit_transactions->amount;
                                                                }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                                    $account_balance-=$credit_transactions->amount;
                                                                }
                                                                ?></td>
                                                            <td><?= number_format(abs($account_balance),2);?></td>
                                                            <td><?php if($account_balance<0){
                                                                    echo "Cr";
                                                                }elseif($account_balance>0){
                                                                    echo "Dr";
                                                                }?></td>
                                                        </tr>
                                                        <?php foreach ($accounts as $acc){
                                                            $account_balance=0;
                                                            if($acc->A_SELFHEAD_ID==$account->A_ID){ ?>
                                                                <tr>
                                                                    <td><?= $acc->A_NAME; ?></td>
                                                                    <td><?= $acc->A_NO; ?></td>
                                                                    <?php $opening=0;
                                                                    if($opening_value==1): ?>
                                                                        <td><?php
                                                                            #------Opening Balance Section---#
                                                                            $opening+=accounts_by_ids($acc->A_ID,"Debit")->opening_balance;
                                                                            $opening-=accounts_by_ids($acc->A_ID,"Credit")->opening_balance;
                                                                            $pre_debit_transactions = transaction_date($acc->A_ID,$start_date,"Debit");
                                                                            $pre_credit_transactions = transaction_date($acc->A_ID,$start_date,"Credit");
                                                                            if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                                                $opening=$opening-$pre_debit_transactions->amount+$pre_credit_transactions->amount;
                                                                            }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                                                $opening=$opening+$pre_debit_transactions->amount-$pre_credit_transactions->amount;
                                                                            }

                                                                            echo number_format(abs($opening),2);
                                                                            $account_balance+=$opening;
                                                                            if($opening<0){
                                                                                echo "<span style='float:right;'>Cr</span>";
                                                                            }elseif($opening>0){
                                                                                echo "<span style='float:right;'>Dr</span>";
                                                                            }
                                                                            #------Opening Balance Section---#
                                                                            ?></td>
                                                                    <?php endif; ?>
                                                                    <td><?php
                                                                        $debit_transactions=transaction_date_by_self($acc->A_ID,$start_date,$end_date,"Debit");
                                                                        echo number_format($debit_transactions->amount,2);
                                                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                                            $account_balance-=$debit_transactions->amount;
                                                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                                            $account_balance+=$debit_transactions->amount;
                                                                        }
                                                                        ?></td>
                                                                    <td><?php
                                                                        $credit_transactions=transaction_date_by_self($acc->A_ID,$start_date,$end_date,"Credit");
                                                                        echo number_format($credit_transactions->amount,2);
                                                                        if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                                            $account_balance+=$credit_transactions->amount;
                                                                        }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                                            $account_balance-=$credit_transactions->amount;
                                                                        }
                                                                        ?></td>
                                                                    <td><?= number_format(abs($account_balance),2);?></td>
                                                                    <td><?php if($account_balance<0){
                                                                            echo "Cr";
                                                                        }elseif($account_balance>0){
                                                                            echo "Dr";
                                                                        }?></td>
                                                                </tr>
                                                                <?php foreach ($accounts as $acct){
                                                                    $account_balance=0;
                                                                    if($acct->A_SELFHEAD_ID==$acc->A_ID){ ?>
                                                                        <tr>
                                                                            <td><?= $acct->A_NAME; ?></td>
                                                                            <td><?= $acct->A_NO; ?></td>
                                                                            <?php $opening=0;
                                                                            if($opening_value==1): ?>
                                                                                <td><?php
                                                                                    #------Opening Balance Section---#
                                                                                    $opening+=accounts_by_ids($acct->A_ID,"Debit")->opening_balance;
                                                                                    $opening-=accounts_by_ids($acct->A_ID,"Credit")->opening_balance;
                                                                                    $pre_debit_transactions = transaction_date($acct->A_ID,$start_date,"Debit");
                                                                                    $pre_credit_transactions = transaction_date($acct->A_ID,$start_date,"Credit");
                                                                                    if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                                                        $opening=$opening-$pre_debit_transactions->amount+$pre_credit_transactions->amount;
                                                                                    }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                                                        $opening=$opening+$pre_debit_transactions->amount-$pre_credit_transactions->amount;
                                                                                    }

                                                                                    echo number_format(abs($opening),2);
                                                                                    $account_balance+=$opening;
                                                                                    if($opening<0){
                                                                                        echo "<span style='float:right;'>Cr</span>";
                                                                                    }elseif($opening>0){
                                                                                        echo "<span style='float:right;'>Dr</span>";
                                                                                    }
                                                                                    #------Opening Balance Section---#
                                                                                    ?></td>
                                                                            <?php endif; ?>
                                                                            <td><?php
                                                                                $debit_transactions=transaction_date_by_self($acct->A_ID,$start_date,$end_date,"Debit");
                                                                                echo number_format($debit_transactions->amount,2);
                                                                                if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                                                    $account_balance-=$debit_transactions->amount;
                                                                                }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                                                    $account_balance+=$debit_transactions->amount;
                                                                                }
                                                                                ?></td>
                                                                            <td><?php
                                                                                $credit_transactions=transaction_date_by_self($acct->A_ID,$start_date,$end_date,"Credit");
                                                                                echo number_format($credit_transactions->amount,2);
                                                                                if($sb->HEAD_TYPE=='Liabilities' || $sb->HEAD_TYPE=='Equity' || $sb->HEAD_TYPE=='Income'){
                                                                                    $account_balance+=$credit_transactions->amount;
                                                                                }elseif ($sb->HEAD_TYPE=='Assets' || $sb->HEAD_TYPE=='Expense'){
                                                                                    $account_balance-=$credit_transactions->amount;
                                                                                }
                                                                                ?></td>
                                                                            <td><?= number_format(abs($account_balance),2);?></td>
                                                                            <td><?php if($account_balance<0){
                                                                                    echo "Cr";
                                                                                }elseif($account_balance>0){
                                                                                    echo "Dr";
                                                                                }?></td>
                                                                        </tr>
                                                                    <?php }
                                                                } ?>
                                                            <?php }
                                                        } ?>
                                                    <?php }
                                                } ?>
                                            <?php }
                                        } ?>
                                    <?php }
                                } ?>
                            <?php }
                        } ?>
                    <?php endif; ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="2"><b style="float: right;">Total</b></td>
                        <?php if($opening_value==1): ?>
                            <td><b><?php echo number_format(abs($opening_balance),2);
                                    if($opening_balance<0){
                                        echo "<span style='float:right;'>Cr</span>";
                                    }elseif($opening_balance>0){
                                        echo "<span style='float:right;'>Dr</span>";
                                    }
                                    ?></b></td>
                        <?php endif; ?>
                        <td><b><?= number_format(abs($total_debit),2); ?></b></td>
                        <td><b><?= number_format(abs($total_credit),2); ?></b></td>
                        <td><b><?php $curent_balance=$opening_balance+$total_debit-$total_credit;
                                echo number_format(abs($curent_balance),2);?></b></td>
                        <td><b><?php if($curent_balance<0){
                                    echo "Cr";
                                }elseif($curent_balance>0){
                                    echo "Dr";
                                }
                                ?></b></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <button class="btn btn-sm pull-right" style="color: #fff;background-color: #26a69a" id="btnExport">Export to Excel</button>
        </div>
    </div>
    <!-- /highlighting rows and columns -->


    <!-- Export to Excel-->
    <script>
        $("#btnExport").click(function (e) {
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('#export_table').html() ));
            e.preventDefault();
        });
    </script>
    <!-- /Export to Excel-->