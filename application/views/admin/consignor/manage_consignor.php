<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('consignor_list') ?></h1>
<!--<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">
        <div class="wrap-fpanel">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        <strong><?/*= lang('consignor_list') */?></strong>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped DataTables " id="DataTables">
                            <thead>
                            <tr>
                                <th><?/*= lang('name') */?> </th>
                                <th><?/*= lang('contacts') */?></th>
                                <th><?/*= lang('email') */?> </th>
                                <th><?/*= lang('client') */?> </th>
                                <th><?/*= lang('status') */?> </th>
                                <th class=""><?/*= lang('action') */?></th>
                            </tr> </thead> <tbody>
                            <?php
/*                            if (!empty($all_consignor_info)) {
                                foreach ($all_consignor_info as $consignor_details) {
                                    */?>
                                    <tr>
                                        <td><?/*= $consignor_details->name */?></td>
                                        <td><?/*= $consignor_details->mobile */?></td>
                                        <td><?/*= $consignor_details->email */?></td>
                                        <td><?/*= client_name($consignor_details->client_id) */?></td>
                                        <td>
                                            <?php /*if ($consignor_details->consignor_status == 1): */?>
                                                <a data-toggle="tooltip" data-placement="top" title="Click to Deactive" href="<?php /*echo base_url() */?>admin/consignor/change_status/0/<?php /*echo encrypt($consignor_details->consignor_id); */?>"><?/*= lang('active') */?></a>
                                            <?php /*else: */?>
                                                <a data-toggle="tooltip" data-placement="top" title="Click to Active "  href="<?php /*echo base_url() */?>admin/consignor/change_status/1/<?php /*echo encrypt($consignor_details->consignor_id); */?>"><?/*= lang('deactive') */?></a>
                                            <?php /*endif; */?>
                                        </td>
                                        <td>
                                            <?php /*echo btn_edit('admin/consignor/new_consignor/' . encode($consignor_details->consignor_id)) */?>
                                        </td>
                                    </tr>
                                    <?php
/*                                }
                            } else {
                                */?>
                                <tr><td colspan="7">
                                        There is no data to display
                                    </td></tr>
                            <?php /*}
                            */?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>-->

<section class="panel panel-default">

    <header class="panel-heading"><?= lang('consignor_list') ?></header>

    <div class="panel-body" id="export_table">

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive" id="customers">
                    <table class="table table-bordered data-table-list dtHorizontalExample" id="tab_customers">
                        <thead>
                        <th><?= lang('name') ?> </th>
                        <th><?= lang('contacts') ?></th>
                        <th><?= lang('email') ?> </th>
                        <th><?= lang('client') ?> </th>
                        <th><?= lang('status') ?> </th>
                        <th><?= lang('action') ?></th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

</section>

<script>
    var data_url='<?= base_url('admin/consignor/manage_consignors') ?>';
</script>