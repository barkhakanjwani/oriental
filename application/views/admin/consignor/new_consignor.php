<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/kendo.default.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/kendo.common.min.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/kendo.all.min.js"></script>
<style>
	.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td{vertical-align:middle;}
</style>
<!-- Content Header (Page header) -->
<form role="form" enctype="multipart/form-data" id="form_v" action="<?php echo base_url(); ?>admin/consignor/save_consignor/<?php
if (!empty($consignor_info)) {
    echo encrypt($consignor_info->consignor_id);
}
?>" method="post" class="form-horizontal  ">
<div class="row">
	<div class="col-md-12">
        <h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= (!empty($consignor_info))?lang('edit_consignor'):lang('add_consignor') ?></h1>
		<section class="panel panel-default">
            <header class="panel-heading  "><?= $title ?></header>
            <div class="panel-body">
				<div class="col-md-6">
					<h4 style="padding: 8px;color: #ffffff;text-transform: uppercase;font-weight: bold;text-align: center;background: #527e92;">general Information</h4>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('name') ?><span class="text-danger"> *</span></label>
						<div class="col-lg-9">
							<input type="text" class="form-control person required mb-5"  value="<?php
								if (!empty($consignor_info->name)) {
									echo $consignor_info->name;
								}
							?>" name="name" required/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('email') ?><span class="text-danger"> *</span></label>
						<div class="col-lg-9">
							<input type="email" class="form-control person" id="email"  value="<?php
								if (!empty($consignor_info->email)) {
									echo $consignor_info->email;
								}
							?>" name="email" autocomplete="off" required>
							<span class="text-danger" id="email_error"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('email') ?></label>
						<div class="col-lg-9">
							<input type="email" class="form-control person" placeholder="Optional" value="<?php
								if (!empty($consignor_info->email_2)) {
									echo $consignor_info->email_2;
								}
							?>" name="email_2">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('email') ?></label>
						<div class="col-lg-9">
							<input type="email" class="form-control person" placeholder="Optional" value="<?php
								if (!empty($consignor_info->email_3)) {
									echo $consignor_info->email_3;
								}
							?>" name="email_3">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('client') ?> <span class="text-danger">*</span></label>
						<div class="col-lg-9">
							<select name="client_id" class="form-control select_box" required>
								<option value="">-</option>
                                <?php
                                    if(!empty($all_client_info)) {
                                        foreach($all_client_info as $client) {
                                            ?>
											<option value="<?= $client->client_id ?>" <?php
                                                if(!empty($consignor_info)){
                                                    if ($consignor_info->client_id != 0){
                                                        echo ($consignor_info->client_id == $client->client_id)?'selected':'';
                                                    }
                                                }
                                            ?>><?= $client->name ?></option>
                                            <?php
                                        }
                                    }
                                ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('short_note') ?></label>
						<div class="col-lg-9">
							<textarea class="form-control person" name="short_note"><?php
                                    if (!empty($consignor_info->short_note)) {
                                        echo $consignor_info->short_note;
                                    }
                                ?>
							</textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('profile_photo') ?></label>
						<div class="col-lg-7" >
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 210px;" >
									<?php if (!empty($consignor_info->profile_photo)) : ?>
										<img src="<?php echo base_url() . $consignor_info->profile_photo; ?>" >
									<?php else: ?>
										<img src="http://placehold.it/350x260" alt="Please Connect Your Internet">
									<?php endif; ?>
								</div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="width: 210px;" ></div>
								<div>
											<span class="btn btn-default btn-file">
												<span class="fileinput-new">
													<input class="person" type="file" name="profile_photo" data-buttonText="<?= lang('choose_file') ?>" id="myImg" accept="image/*">
													<span class="fileinput-exists"><?= lang('change') ?></span>
												</span>
												<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput"><?= lang('remove') ?></a>
											</span>
								</div>
								<div id="valid_msg" style="color: #e11221"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
								<h4 style="padding: 8px;color: #ffffff;text-transform: uppercase;font-weight: bold;text-align: center;background: #527e92;">contact Information</h4>
					<?php
						if(!empty($consignor_persons)) {
                            $count = 0;
                            foreach ($consignor_persons as $person) {
                                ?>
								<div>
								<div class="form-group">
						<label class="col-lg-3 control-label">Contact Person</label>
						<div class="col-lg-4">
							<input type="text" class="form-control person" name="contact_p_name[]" placeholder="Name *"
								   value="<?= $person->consignor_person_name ?>" required/>
						</div>
						<div class="col-lg-5">
							<input type="text" class="form-control person" name="contact_p_email[]" placeholder="Email"
								   value="<?= $person->consignor_person_email ?>"/>
						</div>
					</div>
								<div class="form-group">
						<label class="col-lg-3 control-label"></label>
						<div class="col-lg-4">
							<input type="number" class="form-control person" name="contact_p_contact[]"
								   placeholder="Contact" value="<?= $person->consignor_person_contact ?>"/>
						</div>
						<div class="col-lg-5">
							<input type="text" class="form-control person" name="contact_p_designation[]"
								   placeholder="Designation" value="<?= $person->consignor_person_designation ?>"/>
							<?php
								if($count == 0) {
                                    ?>
									<a class="pull-right" id="add_more" style="cursor:pointer;"><b><i class="fa fa-plus"></i>Add More</b></a>
                                    <?php
                                }else {
                                    ?>
									<a class="pull-right remCF text-danger" style="cursor:pointer;"><b><i class="fa fa-minus"></i>Remove</b></a>
                                    <?php
                                }
							?>
						</div>
					</div>
								</div>
                                <?php
								$count++;
                            }
                        }else{
                                ?>
					<div class="form-group">
						<label class="col-lg-3 control-label">Contact Person</label>
						<div class="col-lg-4">
							<input type="text" class="form-control person" name="contact_p_name[]" placeholder="Name *" required/>
						</div>
						<div class="col-lg-5">
							<input type="text" class="form-control person" name="contact_p_email[]" placeholder="Email" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"></label>
						<div class="col-lg-4">
							<input type="number" class="form-control person" name="contact_p_contact[]" placeholder="Contact" />
						</div>
						<div class="col-lg-5">
							<input type="text" class="form-control person" name="contact_p_designation[]" placeholder="Designation" />
							<a class="pull-right" id="add_more" style="cursor:pointer;"><b><i class="fa fa-plus"></i>Add More</b></a>
						</div>
					</div>
                    <?php
                    }
					?>
					<div id="more_persons"></div>
					
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('phone') ?></label>
						<div class="col-lg-9">
							<input type="text" class="form-control person" value="<?php
								if (!empty($consignor_info->phone)) {
									echo $consignor_info->phone;
								}
							?>" name="phone">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('mobile') ?><span class="text-danger"> *</span></label>
						<div class="col-lg-9">
							<input type="text" class="form-control person" value="<?php
								if (!empty($consignor_info->mobile)) {
									echo $consignor_info->mobile;
								}
							?>" name="mobile" required="required">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('fax') ?></label>
						<div class="col-lg-9">
							<input type="text" class="form-control person" value="<?php
								if (!empty($consignor_info->fax)) {
									echo $consignor_info->fax;
								}
							?>" name="fax">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('city') ?></label>
						<div class="col-lg-9">
							<input type="text" class="form-control person" value="<?php
								if (!empty($consignor_info->city)) {
									echo $consignor_info->city;
								}
							?>" name="city">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('country') ?></label>
						<div class="col-lg-9">
							<select  name="country" class="form-control person select_box" style="width: 100%">
								<optgroup label="Default Country">
									<option value="<?= $this->config->item('company_country') ?>"><?= $this->config->item('company_country') ?></option>
								</optgroup>
								<optgroup label="<?= lang('other_countries') ?>">
									<?php if (!empty($countries)): foreach ($countries as $country): ?>
										<option value="<?= $country->value ?>"><?= $country->value ?></option>
									<?php
									endforeach;
									endif;
									?>
								</optgroup>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('zipcode') ?></label>
						<div class="col-lg-9">
							<input type="text" class="form-control person" value="<?php
								if (!empty($consignor_info->zipcode)) {
									echo $consignor_info->zipcode;
								}
							?>" name="zipcode">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">NTN</label>
						<div class="col-lg-9">
							<input type="text" class="form-control person" value="<?php
								if (!empty($consignor_info->ntn)) {
									echo $consignor_info->ntn;
								}
							?>" name="ntn">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">STRN</label>
						<div class="col-lg-9">
							<input type="text" class="form-control person" value="<?php
								if (!empty($consignor_info->strn)) {
									echo $consignor_info->strn;
								}
							?>" name="strn">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('address') ?></label>
						<div class="col-lg-9">
							<textarea class="form-control person" name="address"><?php
								if (!empty($consignor_info->address)) {
									echo $consignor_info->address;
								}
								?>
							</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-12 form-group">
					<h4 style="padding: 8px;color: #ffffff;text-transform: uppercase;font-weight: bold;text-align: center;background: #527e92;">Document Section</h4>
					<div class="table-responsive">
                        <table class="table table-bordered" style="margin:0px;">
                           <tr>
                              <th>Document Title</th>
                              <th>Date</th>
                              <th class="text-center">File</th>
                              <th>Comment</th>
                              <th class="text-center">Action</th>
                           </tr>
							<?php
								if(!empty($consignor_documents)) {
									$count = 0;
									foreach($consignor_documents as $document) {
                                        ?>
                                 <tr>
									<td class="col-md-3"><input type="text" class="form-control" name="document_title[]"
																placeholder="Title" value="<?= $document->consignor_document_title ?>" /></td>
								<td class="col-md-2"><input type="text" class="form-control datepicker"
															placeholder="Y-m-d" autocomplete="off"
															name="document_date[]" value="<?= $document->consignor_document_date ?>" /></td>
                              <td class="text-center col-md-2">
                                 <input type="file" name="document_file[]" />
                                  <?php
                                      if (!empty($document->consignor_document_file)) {
            
                                          ?>
										  <a href="<?= base_url('') . $document->consignor_document_file ?>"
											 target="_blank"><?php $explode = explode('/', $document->consignor_document_file);
                                                  echo $explode[2]; ?> </a>
                                          <?php
                                      }
    
                                  ?>
								  <input type="hidden" name="document_file_2[]" value="<?= $document->consignor_document_file ?>"  />
                              </td>
                              <td class="col-md-3">
                                 <textarea class="form-control" placeholder="Your comment goes here..."
										   name="document_comment[]"><?= $document->consignor_document_comment ?></textarea>
                              </td>
                              <?php
                              	if($count == 0){
                              ?>
                              <td class="text-center col-md-1"><button type="button"
																	   class="btn btn-primary btn-xs"
																	   id="add_document"
																	   style="border-radius:12px;"><i
											  class="fa fa-plus"></i></button></td>
                               <?php
                               	}else{
                               ?>
									<td class="text-center col-md-1"><button type="button"
																			 class="btn btn-danger btn-xs remCF2"
																			 style="border-radius:12px;"><i
													class="fa fa-minus"></i></button></td>
                                    <?php
                               }
                               ?>
								 </tr>
                                        <?php
										$count++;
                                    }
								}else {
                                    ?>
									<tr>
								<td class="col-md-3"><input type="text" class="form-control" name="document_title[]"
															placeholder="Title"/></td>
								<td class="col-md-2"><input type="text" class="form-control datepicker"
															placeholder="Y-m-d" autocomplete="off"
															name="document_date[]"/></td>
                              <td class="text-center col-md-2">
                                 <input type="file" name="document_file[]"/>
                              </td>
                              <td class="col-md-3">
                                 <textarea class="form-control" placeholder="Your comment goes here..."
										   name="document_comment[]"></textarea>
                              </td>
                              <td class="text-center col-md-1"><button type="button"
																	   class="btn btn-primary btn-xs"
																	   id="add_document"
																	   style="border-radius:12px;"><i
											  class="fa fa-plus"></i></button></td>
							</tr>
                                    <?php
                                }
							?>
						</table>
						<div id="add_new_document"></div>
					</div>
				</div>
				<div class="col-lg-12 text-center">
					<button type="submit" id="submit" class="btn btn-sm btn-primary"><?= lang('save_changes') ?></button>
				</div>
			</div>
		</section>
	</div>
    
    <!-- End Form -->
</div>
</form>

<script>
    /*** CHECK EMAIL AVAILABILITY ***/
    $('#email').bind('change paste keyup', function() {
        /*$(document).on('keyup paste', "#email",function () {*/
        if ($(this).val().length > 0) {
            $(this).addClass('spinner');
            var value = $(this).val();
            var consignor = '';
            <?php
            if(!empty($consignor_info)){
            ?>
            consignor = '<?= encrypt($consignor_info->consignor_id) ?>';
            <?php
            }
            ?>
            $.ajax({
                url : "<?php echo base_url(); ?>admin/consignor/check_consignor_email/"+consignor,
                type : "POST",
                dataType : "json",
                data : {"email" : value},
                success : function(data) {
                    if(data){
                        var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
                        var email = document.getElementById("email");
                        if (regexEmail.test(email.value)) {
                            $('#email_error').text("");
                            $('#submit').removeAttr('disabled');
                            $('#email').removeClass('spinner');
                        } else {
                            $('#submit').attr('disabled','disabled');
                            $('#email').removeClass('spinner');
                        }
                    }
                    else {
                        $('#email_error').text("Email already exist");
                        $('#submit').attr('disabled','disabled');
                        $('#email').removeClass('spinner');
                    }
                },
                error : function(data) {
                    console.log('error');
                }
            });
        }
        else{
            $('#email_error').text("");
            $('#submit').removeAttr('disabled');
            $(this).removeClass('spinner')
        }
    });
</script>
<script>
    /*** DOCUMENT UPLOADING ***/
    $("#add_document").click(function () {
    
        var columns = '<table class="table table-bordered" style="margin:0px;">';
    
        columns += '<tr>';
    
        columns += '<td class="col-md-3"><input type="text" class="form-control" name="document_title[]" placeholder="Title" /></td>';
    
        columns += '<td class="col-md-2"><input type="text" class="form-control datepicker"  placeholder="Y-m-d" autocomplete="off" name="document_date[]" /></td>';
    
        columns += '<td class="text-center col-md-2">';
    
        columns += '<input type="file" name="document_file[]" />';
    
        columns += '</td>';
		
        columns += '<td class="col-md-3">';
    
        columns += '<textarea class="form-control" placeholder="Your comment goes here..." name="document_comment[]"></textarea>';
    
        columns += '</td>';
    
        columns += '<td class="text-center col-md-1"><button type="button" class="btn btn-danger btn-xs remCF" style="border-radius:12px;"><i class="fa fa-minus"></i></button></td>';
    
        columns += '</tr>';
    
        columns += '</table>';
    
        var add_new = $(columns);
    
        $("#add_new_document").append(add_new.hide().fadeIn(1000));
    
        $('.select_box').select2({});
    
    });


    $("#add_new_document").on('click', '.remCF', function () {
    
        $(this).fadeOut(300, function () {
            $(this).parent().parent().remove();
        })
    
    });
    
    $(document).on('focus', '.datepicker', function () {
    
        $(this).datepicker({format: 'yyyy-mm-dd', autoclose: true});
    
        $('.select_box').select2({});
    
    });
    
    $(".remCF2").on('click', function() {
        $(this).fadeOut(300, function() { $(this).parent().parent().remove(); })
    });

    /*** CONTACT PERSONS ***/
    $("#add_more").click(function () {
    var columns = '<div><div class="form-group">';
		columns += '<label class="col-lg-3 control-label">Contact Person</label>';
        columns += '<div class="col-lg-4">';
        columns += '<input type="text" class="form-control person" name="contact_p_name[]" placeholder="Name *" required />';
        columns += '</div>';
        columns += '<div class="col-lg-5">';
        columns += '<input type="text" class="form-control person" name="contact_p_email[]" placeholder="Email" />';
        columns += '</div>';
        columns += '</div>';
        columns += '<div class="form-group">';
        columns += '<label class="col-lg-3 control-label"></label>';
        columns += '<div class="col-lg-4">';
        columns += '<input type="number" class="form-control person" name="contact_p_contact[]" placeholder="Contact" />';
        columns += '</div>';
        columns += '<div class="col-lg-5">';
        columns += '<input type="text" class="form-control person" name="contact_p_designation[]" placeholder="Designation" />';
        columns += '<a class="pull-right text-danger remCF" style="cursor:pointer;"><b><i class="fa fa-minus"></i>Remove</b></a>';
        columns += '</div>';
        columns += '</div>';
        columns += '</div>';
    
        var add_new = $(columns);
    
        $("#more_persons").append(add_new.hide().fadeIn(1000));
    });


    $("#more_persons").on('click', '.remCF', function () {
    
        $(this).fadeOut(300, function () {
            $(this).parent().parent().parent().remove();
        })
    
    });
    $(".remCF").on('click', function() {
        $(this).fadeOut(300, function() { $(this).parent().parent().parent().remove(); })
    });
</script>