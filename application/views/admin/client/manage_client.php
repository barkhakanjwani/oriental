<?php echo message_box('success'); ?>

<?php echo message_box('error'); ?>


<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('client_list') ?></h1> 
<!--<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">
        <div class="wrap-fpanel">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <div class="panel-title">
                        <strong><?/*= lang('client_list') */?></strong>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped DataTables " id="DataTables">
                            <thead>
                            <tr>
                                <th><?/*= lang('name') */?> </th>
                                <th><?/*= lang('contacts') */?></th>
                                <th><?/*= lang('email') */?> </th>
                                <th><?/*= lang('refered_by') */?> </th>
                                <th><?/*= lang('status') */?> </th>
                                <th class=""><?/*= lang('action') */?></th>
                            </tr> </thead> <tbody>
                            <?php
/*                            if (!empty($all_client_info)) {
                                foreach ($all_client_info as $client_details) {
                                    $client_name='-';
                                    if ($client_details->refered_by != 0){
                                        $client_info = $this->invoice_model->check_by(array('client_id'=>$client_details->refered_by),'tbl_client');
                                        $client_name=$client_info->name;
                                    }
                                    */?>
                                    <tr>
                                        <td><?/*= $client_details->name */?></td>
                                        <td><?/*= $client_details->mobile */?></td>
                                        <td><?/*= $client_details->email */?></td>
                                        <td><?/*= $client_name */?></td>
                                        <td>
                                            <?php /*if ($client_details->client_status == 1): */?>
                                                <a data-toggle="tooltip" data-placement="top" title="Click to Deactive" href="<?php /*echo base_url() */?>admin/client/change_status/0/<?php /*echo encrypt($client_details->client_id); */?>"><?/*= lang('active') */?></a>
                                            <?php /*else: */?>
                                                <a data-toggle="tooltip" data-placement="top" title="Click to Active "  href="<?php /*echo base_url() */?>admin/client/change_status/1/<?php /*echo encrypt($client_details->client_id); */?>"><?/*= lang('deactive') */?></a>
                                            <?php /*endif; */?>
                                        </td>
                                        <td>
                                            <?php /*echo btn_edit('admin/client/new_client/' . encode($client_details->client_id)) */?>
                                        </td>
                                    </tr>
                                    <?php
/*                                }
                            } else {
                                */?>
                                <tr><td colspan="7">
                                        There is no data to display
                                    </td></tr>
                            <?php /*}
                            */?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>-->

<section class="panel panel-default">

    <header class="panel-heading"><?= lang('client_list') ?></header>

    <div class="panel-body" id="export_table">

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive" id="customers">
                    <table class="table table-bordered data-table-list dtHorizontalExample" id="tab_customers">
                        <thead>
                        <th><?= lang('name') ?> </th>
                        <th><?= lang('contacts') ?></th>
                        <th><?= lang('email') ?> </th>
                        <th><?= lang('refered_by') ?> </th>
                        <th><?= lang('status') ?> </th>
                        <th><?= lang('action') ?></th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

</section>

<script>
    var data_url='<?= base_url('admin/client/manage_clients') ?>';
</script>