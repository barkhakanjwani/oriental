<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/kendo.default.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/kendo.common.min.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/kendo.all.min.js"></script>
<style>
	.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td{vertical-align:middle;}
</style>
<!-- Content Header (Page header) -->
<?php echo message_box('success') ?>
<?php echo message_box('error'); ?>
<form role="form" enctype="multipart/form-data" id="form_v" action="<?php echo base_url(); ?>admin/client/save_client/<?php
if (!empty($client_info)) {
    echo encrypt($client_info->client_id);
}
?>" method="post" class="form-horizontal  ">
<div class="row">
    <!-- Start Form -->
	<div class="col-md-12">
        <h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= (!empty($client_info))?lang('edit_client'):lang('add_client') ?></h1>
		<section class="panel panel-default">
            <header class="panel-heading  "><?= $title ?></header>
            <div class="panel-body">
				<div class="col-md-6">
					<h4 style="padding: 8px;color: #ffffff;text-transform: uppercase;font-weight: bold;text-align: center;background: #527e92;">general Information</h4>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?= lang('name') ?><span class="text-danger"> *</span></label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control person required mb-5"  value="<?php
                            if (!empty($client_info->name)) {
                                echo $client_info->name;
                            }
                            ?>" name="name" required/>
                        </div>
                    </div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('email') ?><span class="text-danger"> *</span></label>
						<div class="col-lg-9">
							<input type="email" class="form-control person" id="email"  value="<?php
								if (!empty($client_info->email)) {
									echo $client_info->email;
								}
							?>" name="email" autocomplete="off" required>
							<span class="text-danger" id="email_error"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Create Login</label>
						<div class="col-lg-3">
							<input type="checkbox" value="Yes" class="minimal" name="chkpass" id="chkPass"/> Yes
						</div>
					</div>
					<div class="dvPass" style="display: none">
						<div class="form-group">
							<label class="col-lg-3 control-label"><strong><?= lang('password') ?> </strong><span class="text-danger">*</span></label>
							<div class="col-lg-9">
								<input type="password" value="" id="password" placeholder="<?= lang('password') ?>" name="password"  class="input-sm form-control" required>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-lg-3 control-label"><strong><?= lang('confirm_password') ?> </strong><span class="text-danger">*</span></label>
							<div class="col-lg-9">
								<input type="password" placeholder="<?= lang('confirm_password') ?>"  name="confirm_password"  class="input-sm form-control" required>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('email') ?></label>
						<div class="col-lg-9">
							<input type="email" class="form-control person" placeholder="Optional" value="<?php
								if (!empty($client_info->email_2)) {
									echo $client_info->email_2;
								}
							?>" name="email_2">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('email') ?></label>
						<div class="col-lg-9">
							<input type="email" class="form-control person" placeholder="Optional" value="<?php
								if (!empty($client_info->email_3)) {
									echo $client_info->email_3;
								}
							?>" name="email_3">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('refered_by') ?></label>
						<div class="col-lg-9">
							<select name="refered_by" class="form-control select_box">
								<option value="">-</option>
                                <?php
                                    if(!empty($all_client_info)) {
                                        foreach($all_client_info as $client) {
                                            ?>
											<option value="<?= $client->client_id ?>" <?php
                                                if(!empty($client_info)){
                                                    if ($client_info->refered_by != 0){
                                                        echo ($client_info->refered_by == $client->client_id)?'selected':'';
                                                    }
                                                }
                                            ?>><?= $client->name ?></option>
                                            <?php
                                        }
                                    }
                                ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('short_note') ?></label>
						<div class="col-lg-9">
							<textarea class="form-control person" name="short_note"><?php
                                    if (!empty($client_info->short_note)) {
                                        echo $client_info->short_note;
                                    }
                                ?>
							</textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('profile_photo') ?></label>
						<div class="col-lg-7" >
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 210px;" >
									<?php if (!empty($client_info->profile_photo)) : ?>
										<img src="<?php echo base_url() . $client_info->profile_photo; ?>" >
									<?php else: ?>
										<img src="http://placehold.it/350x260" alt="Please Connect Your Internet">
									<?php endif; ?>
								</div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="width: 210px;" ></div>
								<div>
											<span class="btn btn-default btn-file">
												<span class="fileinput-new">
													<input class="person" type="file" name="profile_photo" data-buttonText="<?= lang('choose_file') ?>" id="myImg" accept="image/*">
													<span class="fileinput-exists"><?= lang('change') ?></span>
												</span>
												<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput"><?= lang('remove') ?></a>
											</span>
								</div>
								<div id="valid_msg" style="color: #e11221"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<h4 style="padding: 8px;color: #ffffff;text-transform: uppercase;font-weight: bold;text-align: center;background: #527e92;">contact Information</h4>
					<div class="form-group">
						<label class="col-lg-3 control-label">Contact Person</label>
						<div class="col-lg-4">
							<input type="text" class="form-control person" name="contact_p_name" placeholder="Name" value="<?php
                                if (!empty($client_info->contact_p_name)) {
                                    echo $client_info->contact_p_name;
                                }
                            ?>" />
						</div>
						<div class="col-lg-5">
							<input type="text" class="form-control person" name="contact_p_email" placeholder="Email" value="<?php
                                if (!empty($client_info->contact_p_email)) {
                                    echo $client_info->contact_p_email;
                                }
                            ?>" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"></label>
						<div class="col-lg-4">
							<input type="number" class="form-control person" name="contact_p_contact" placeholder="Contact" value="<?php
                                if (!empty($client_info->contact_p_contact)) {
                                    echo $client_info->contact_p_contact;
                                }
                            ?>" />
						</div>
						<div class="col-lg-5">
							<input type="text" class="form-control person" name="contact_p_designation" placeholder="Designation" value="<?php
                                if (!empty($client_info->contact_p_designation)) {
                                    echo $client_info->contact_p_designation;
                                }
                            ?>" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('phone') ?></label>
						<div class="col-lg-9">
							<input type="text" class="form-control person" value="<?php
								if (!empty($client_info->phone)) {
									echo $client_info->phone;
								}
							?>" name="phone">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('mobile') ?><span class="text-danger"> *</span></label>
						<div class="col-lg-9">
							<input type="text" class="form-control person" value="<?php
								if (!empty($client_info->mobile)) {
									echo $client_info->mobile;
								}
							?>" name="mobile" required="required">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('fax') ?></label>
						<div class="col-lg-9">
							<input type="text" class="form-control person" value="<?php
								if (!empty($client_info->fax)) {
									echo $client_info->fax;
								}
							?>" name="fax">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('city') ?></label>
						<div class="col-lg-9">
							<input type="text" class="form-control person" value="<?php
								if (!empty($client_info->city)) {
									echo $client_info->city;
								}
							?>" name="city">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('country') ?></label>
						<div class="col-lg-9">
							<select  name="country" class="form-control person select_box" style="width: 100%">
								<optgroup label="Default Country">
									<option value="<?= $this->config->item('company_country') ?>"><?= $this->config->item('company_country') ?></option>
								</optgroup>
								<optgroup label="<?= lang('other_countries') ?>">
									<?php if (!empty($countries)): foreach ($countries as $country): ?>
										<option value="<?= $country->value ?>"><?= $country->value ?></option>
									<?php
									endforeach;
									endif;
									?>
								</optgroup>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('zipcode') ?></label>
						<div class="col-lg-9">
							<input type="text" class="form-control person" value="<?php
								if (!empty($client_info->zipcode)) {
									echo $client_info->zipcode;
								}
							?>" name="zipcode">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">NTN</label>
						<div class="col-lg-9">
							<input type="text" class="form-control person" value="<?php
								if (!empty($client_info->ntn)) {
									echo $client_info->ntn;
								}
							?>" name="ntn">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">STRN</label>
						<div class="col-lg-9">
							<input type="text" class="form-control person" value="<?php
								if (!empty($client_info->strn)) {
									echo $client_info->strn;
								}
							?>" name="strn">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label"><?= lang('address') ?></label>
						<div class="col-lg-9">
							<textarea class="form-control person" name="address"><?php
								if (!empty($client_info->address)) {
									echo $client_info->address;
								}
								?>
							</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-12 form-group">
					<h4 style="padding: 8px;color: #ffffff;text-transform: uppercase;font-weight: bold;text-align: center;background: #527e92;">Document Section</h4>
					<div class="table-responsive">
                        <table class="table table-bordered" style="margin:0px;">
                           <tr>
                              <th>Document Title</th>
                              <th>Date</th>
                              <th class="text-center">File</th>
                              <th>Comment</th>
                              <th class="text-center">Action</th>
                           </tr>
							<?php
								if(!empty($client_documents)) {
									$count = 0;
									foreach($client_documents as $document) {
                                        ?>
                                 <tr>
									<td class="col-md-3"><input type="text" class="form-control" name="document_title[]"
																placeholder="Title" value="<?= $document->client_document_title ?>" /></td>
								<td class="col-md-2"><input type="text" class="form-control datepicker"
															placeholder="Y-m-d" autocomplete="off"
															name="document_date[]" value="<?= $document->client_document_date ?>" /></td>
                              <td class="text-center col-md-2">
                                 <input type="file" name="document_file[]" />
                                  <?php
                                      if (!empty($document->client_document_file)) {
            
                                          ?>
										  <a href="<?= base_url('') . $document->client_document_file ?>"
											 target="_blank"><?php $explode = explode('/', $document->client_document_file);
                                                  echo $explode[2]; ?> </a>
                                          <?php
                                      }
    
                                  ?>
								  <input type="hidden" name="document_file_2[]" value="<?= $document->client_document_file ?>"  />
                              </td>
                              <td class="col-md-3">
                                 <textarea class="form-control" placeholder="Your comment goes here..."
										   name="document_comment[]"><?= $document->client_document_comment ?></textarea>
                              </td>
                              <?php
                              	if($count == 0){
                              ?>
                              <td class="text-center col-md-1"><button type="button"
																	   class="btn btn-primary btn-xs"
																	   id="add_document"
																	   style="border-radius:12px;"><i
											  class="fa fa-plus"></i></button></td>
                               <?php
                               	}else{
                               ?>
									<td class="text-center col-md-1"><button type="button"
																			 class="btn btn-danger btn-xs remCF2"
																			 style="border-radius:12px;"><i
													class="fa fa-minus"></i></button></td>
                                    <?php
                               }
                               ?>
								 </tr>
                                        <?php
										$count++;
                                    }
								}else {
                                    ?>
									<tr>
								<td class="col-md-3"><input type="text" class="form-control" name="document_title[]"
															placeholder="Title"/></td>
								<td class="col-md-2"><input type="text" class="form-control datepicker"
															placeholder="Y-m-d" autocomplete="off"
															name="document_date[]"/></td>
                              <td class="text-center col-md-2">
                                 <input type="file" name="document_file[]"/>
                              </td>
                              <td class="col-md-3">
                                 <textarea class="form-control" placeholder="Your comment goes here..."
										   name="document_comment[]"></textarea>
                              </td>
                              <td class="text-center col-md-1"><button type="button"
																	   class="btn btn-primary btn-xs"
																	   id="add_document"
																	   style="border-radius:12px;"><i
											  class="fa fa-plus"></i></button></td>
							</tr>
                                    <?php
                                }
							?>
						</table>
						<div id="add_new_document"></div>
					</div>
				</div>
				<div class="col-lg-12 text-center">
					<button type="submit" id="submit" class="btn btn-sm btn-primary"><?= lang('save_changes') ?></button>
				</div>
			</div>
		</section>
	</div>
    
    <!-- End Form -->
</div>
</form>

<script>
    /*** CHECK EMAIL AVAILABILITY ***/
    $('#email').bind('change paste keyup', function() {
        /*$(document).on('keyup paste', "#email",function () {*/
        if ($(this).val().length > 0) {
            $(this).addClass('spinner');
            var value = $(this).val();
            var client = '';
            <?php
            if(!empty($client_info)){
            ?>
            client = '<?= encrypt($client_info->client_id) ?>';
            <?php
            }
            ?>
            $.ajax({
                url : "<?php echo base_url(); ?>admin/client/check_client_email/"+client,
                type : "POST",
                dataType : "json",
                data : {"email" : value},
                success : function(data) {
                    if(data){
                        var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
                        var email = document.getElementById("email");
                        if (regexEmail.test(email.value)) {
                            $('#email_error').text("");
                            $('#submit').removeAttr('disabled');
                            $('#email').removeClass('spinner');
                        } else {
                            $('#submit').attr('disabled','disabled');
                            $('#email').removeClass('spinner');
                        }
                    }
                    else {
                        $('#email_error').text("Email already exist");
                        $('#submit').attr('disabled','disabled');
                        $('#email').removeClass('spinner');
                    }
                },
                error : function(data) {
                    console.log('error');
                }
            });
        }
        else{
            $('#email_error').text("");
            $('#submit').removeAttr('disabled');
            $(this).removeClass('spinner')
        }
    });

    /*show checked node IDs on datasource change*/
    function onCheck() {
        console.log($('input:checkbox:checked').length);
        if($('input:checkbox:checked').length > 0){
            $('#submit').removeAttrs('disabled','disabled');
        }
        else{
            $('#submit').attr('disabled',true);
        }
    }

</script>
<script>
    /*** DOCUMENT UPLOADING ***/
    $("#add_document").click(function () {
    
        var columns = '<table class="table table-bordered" style="margin:0px;">';
    
        columns += '<tr>';
    
        columns += '<td class="col-md-3"><input type="text" class="form-control" name="document_title[]" placeholder="Title" /></td>';
    
        columns += '<td class="col-md-2"><input type="text" class="form-control datepicker"  placeholder="Y-m-d" autocomplete="off" name="document_date[]" /></td>';
    
        columns += '<td class="text-center col-md-2">';
    
        columns += '<input type="file" name="document_file[]" />';
    
        columns += '</td>';
		
        columns += '<td class="col-md-3">';
    
        columns += '<textarea class="form-control" placeholder="Your comment goes here..." name="document_comment[]"></textarea>';
    
        columns += '</td>';
    
        columns += '<td class="text-center col-md-1"><button type="button" class="btn btn-danger btn-xs remCF" style="border-radius:12px;"><i class="fa fa-minus"></i></button></td>';
    
        columns += '</tr>';
    
        columns += '</table>';
    
        var add_new = $(columns);
    
        $("#add_new_document").append(add_new.hide().fadeIn(1000));
    
        $('.select_box').select2({});
    
    });


    $("#add_new_document").on('click', '.remCF', function () {
    
        $(this).fadeOut(300, function () {
            $(this).parent().parent().remove();
        })
    
    });
    
    $(document).on('focus', '.datepicker', function () {
    
        $(this).datepicker({format: 'yyyy-mm-dd', autoclose: true});
    
        $('.select_box').select2({});
    
    });
    
    $(".remCF2").on('click', function() {
        $(this).fadeOut(300, function() { $(this).parent().parent().remove(); })
    });

    $(document).ready(function(){
            $(document).on('ifChanged', "#chkPass", function (e) {
            console.log('abc');
            if ($(this).is(":checked")) {
                $(".dvPass").show();
            } else {
                $(".dvPass").hide();
            }
        });
    });


    /** Fancy Checkbox & Radio**/
    $(function () {
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue'
        })
    })
</script>