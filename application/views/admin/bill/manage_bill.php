<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{
		font-size:13px;
		padding:4px;
		vertical-align: middle;
	}
</style>
<section class="content-header">
    <div class="row">
        <div class="col-md-4">
            <a class="btn btn-sm btn-default" href="<?= base_url() ?>admin/bill/search_bill/" title="Back"><i class="fa fa-arrow-left"></i> Back</a>
        </div>
    </div>
</section>
<section class="content">
    <h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('create_bill') ?></h1>
	<!-- Start Display Details -->
	<!-- Main content -->
    <div class="row" >
        <section class="invoice" id="print_invoice">

            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <img style="width: 60px;width: 60px;margin-top: -10px;margin-right: 10px;" src="<?=base_url() . config_item('invoice_logo') ?>" ><?= config_item('company_name') ?>
                    </h2>
                </div><!-- /.col -->
            </div>
            <form enctype="multipart/form-data" id="form_label" action="<?php echo base_url(); ?>admin/bill/save_bill/<?php
                if (!empty($bill_info)) {
                    echo encrypt($bill_info->bill_id);
                }
            ?>" method="post" class="form-horizontal ">
                <input type="hidden" value="<?= $job_info->invoices_id ?>" name="invoices_id" />
                <input type="hidden" value="<?= (!empty($requisition_info))?$requisition_info->requisition_id:'' ?>" name="requisition_id" />

                <div class="row">
                    <div class="col-xs-12 table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th colspan="10">Description of Consignment</th>
                            </tr>
                            </thead>
                            <tr class="text-center">
                                <td>Bill No <span class="text-danger">*</span></td>
                                <td colspan="2"><input type="text" class="form-control" name="bill_no" value="<?php
                                        if (!empty($bill_info)) {
                                            echo $bill_info->bill_no;
                                        }else{
                                            echo $this->invoice_model->job_no_creation($job_info->invoices_id);
                                        }
                                    ?>" autocomplete="off" placeholder="Bill Number" required="required" /></td>
                                <td>Bill Date <span class="text-danger">*</span></td>
                                <td colspan="2"><input type="text" name="delivery_date"  class="form-control datepicker calculator" value="<?php
                                        if (!empty($bill_info->delivery_date)) {
                                            echo ($bill_info->delivery_date != '0000-00-00') ? $bill_info->delivery_date:'';
                                        }
                                    ?>" data-date-format="<?= config_item('date_picker_format'); ?>" placeholder="Y-m-d" autocomplete="off" required="required"></td>
                                <td><?= lang('duty_1') ?></td>
                                <td> <b><?= number_format($this->invoice_model->get_duty_1($job_info->invoices_id),2) ?></b></td>
                                <td><?= lang('duty_2') ?></td>
                                <td>
                                    <b><?= number_format($this->invoice_model->get_duty_2($job_info->invoices_id),2) ?></b></td>
                            </tr>
                        </table>
                    </div>
                    
                    <!-- RECEIPT VOUCHER -->
                    <div class="col-xs-12 table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th colspan="10">Pay Order</th>
                            </tr>
                            </thead>
                            <tr class="text-center">
                                <td><b>Title</b></td>
                                <td><b>Amount</b></td>
                            </tr>
                            <?php
                            foreach ($receipt_voucher_info as $receipt) {
                                ?>
                                <tr class="text-center">
                                    <td><?= $receipt->apd_title; ?></td>
                                    <td><?= $receipt->apd_amount; ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                    </div>
                    
                    
					<!--- GET DYNAMICALLY ACCOUNTS BY CLIENT ID --->
                    <?php
        
                        /** Advances from Clients **/
                        $adc_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Liabilities', 'NAME' => 'Current Liabilities'), 'sub_heads');
                        $adc_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $adc_head->SUB_HEAD_ID, 'H_NAME' => 'Advances from clients'), 'accounts_head');
                        $adc_account_id = $this->invoice_model->check_by(array('H_ID' => $adc_sub_head->H_ID, 'CLIENT_ID' => $job_info->client_id,'A_NAME'=>client_name($job_info->client_id).' (Advances from clients)'), 'accounts');
                        /*Getting Advances */
                        $received_advances=$this->accounts_model->advances_by_client($adc_account_id->A_ID,'Credit')->TM_AMOUNT;
                        $adjust_advances=$this->accounts_model->advances_by_client($adc_account_id->A_ID,'Debit')->TM_AMOUNT;
                        $remaining_advances=$received_advances-$adjust_advances;

                        if(isset($advances)){
                            $remaining_advances = $advances;
                        }
                        if(!empty($adc_account_id)){
                            if(empty($bill_info)) {
                                ?>
								<input type="hidden" name="A_ID[]" value="<?= $adc_account_id->A_ID ?>"/>
								<input type="hidden" name="TM_AMOUNT[]" value="" class="grand_total_bills">
								<input type="hidden" name="TM_TYPE[]" value="Debit"/>
                                <?php
                            }
                            else {
                                $adc_account_id = $this->invoice_model->check_by(array('A_ID'=>$adc_account_id->A_ID, 'T_ID'=>$bill_info->transaction_id, 'PAYMENT_ID'=>NULL),'transactions_meta');
                                $remaining_advances - $remaining_advances - $adc_account_id->TM_AMOUNT;

                                ?>
								<input type="hidden" name="TM_ID[]" value="<?= $adc_account_id->TM_ID ?>"/>
								<input type="hidden" name="TM_AMOUNT[]" class="grand_total_bills" value="<?= $adc_account_id->TM_AMOUNT; ?>" />
                                <?php
                            }
                        }
        
                        /** TURNOVER SERVICES **/
                        $services_head = $this->invoice_model->check_by(array('HEAD_TYPE'=>'Income', 'NAME'=>'Sales Income'),'sub_heads');
                        $services_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID'=>$services_head->SUB_HEAD_ID, 'H_NAME'=>'Turnover Services'),'accounts_head');
                        $services_account_id = $this->invoice_model->check_by(array('SUB_HEAD_ID'=>$services_sub_head->SUB_HEAD_ID, 'H_ID'=>$services_sub_head->H_ID,'CLIENT_ID'=>$job_info->client_id, 'A_NAME'=>client_name($job_info->client_id). ' (Turnover Services)'),'accounts');
                        if(!empty($services_account_id)){
                            if(empty($bill_info)) {
                                ?>
								<input type="hidden" name="A_ID[]" value="<?= $services_account_id->A_ID ?>"/>
								<input type="hidden" name="TM_AMOUNT[]" value="" class="turnover_total"/>
								<input type="hidden" name="TM_TYPE[]" value="Credit"/>
                                <?php
                            }else {
                                $services_account_id = $this->invoice_model->check_by(array('A_ID'=>$services_account_id->A_ID, 'T_ID'=>$bill_info->transaction_id, 'PAYMENT_ID'=>NULL),'transactions_meta');
                                ?>
								<input type="hidden" name="TM_ID[]" value="<?= $services_account_id->TM_ID ?>"/>
								<input type="hidden" name="TM_AMOUNT[]" value="<?= $services_account_id->TM_AMOUNT ?>" class="turnover_total"/>
                                <?php
                            }
                        }
        

                        /** COST OF SALES **/
                        $expense_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Expense', 'NAME' => 'Cost of Sales'), 'sub_heads');
                        $expense_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $expense_head->SUB_HEAD_ID, 'H_NAME' => 'Client Expenses'), 'accounts_head');
                        $expense_account_id = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $expense_sub_head->SUB_HEAD_ID, 'H_ID' => $expense_sub_head->H_ID, 'CLIENT_ID' => $job_info->client_id, 'A_NAME'=>client_name($job_info->client_id).' (Client Expenses)'), 'accounts');

                        if(!empty($expense_account_id)){
                            if(empty($bill_info)) {
                                ?>
								<input type="hidden" name="A_ID[]" value="<?= $expense_account_id->A_ID ?>"/>
								<input type="hidden" name="TM_AMOUNT[]" value="" class="client_expense" />
								<input type="hidden" name="TM_TYPE[]" value="Credit"/>
                                <?php
                            }else {
                                $expense_account_id = $this->invoice_model->check_by(array('A_ID'=>$expense_account_id->A_ID, 'T_ID'=>$bill_info->transaction_id, 'PAYMENT_ID'=>NULL),'transactions_meta');
                                ?>
								<input type="hidden" name="TM_ID[]" value="<?= $expense_account_id->TM_ID ?>"/>
								<input type="hidden" name="TM_AMOUNT[]" value="<?= $expense_account_id->TM_AMOUNT ?>" />
                                <?php
                            }
                        }
                        $counter = 1;
                        $total=0;
                    ?>
					<div class="col-xs-12 table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Description</th>
                                <th>Charge to Customer</th>
                                <th class="text-right">Actual Paid</th>
                            </tr>
                            </thead>
                            <tbody>
                    <?php
                        if (!empty($job_expenses)){
                            $counter = 1;
                            $total_job_expenses = 0;
                            foreach ($job_expenses as $job_expense){
                                if($job_expense->job_expense_title == '1st Duty & Taxes'){
                                    $value = round($this->invoice_model->get_duty_1($job_info->invoices_id),2);
                                    $readonly = "readonly";
                                }elseif($job_expense->job_expense_title == '2nd Duty & Taxes + (If No Invoice fine)'){
                                    $value = round($this->invoice_model->get_duty_2($job_info->invoices_id),2);
                                    $readonly = "readonly";
                                }elseif($job_expense->job_expense_title == 'WEBOC Token'){
                                    $value = $this->config->item('weboc_token');
                                    $readonly = "readonly";
                                }elseif($job_expense->job_expense_title == '1st CESS' || $job_expense->job_expense_title == '2nd CESS'){
                                    $value = $this->invoice_model->get_excise_taxation($job_info->invoices_id);
                                    $readonly = 'readonly';
                                }
                                else{
                                    $value = $this->invoice_model->job_expenses_by_invoice_account($job_info->invoices_id,$job_expense->job_expense_id)->amount;
                                    $readonly = "readonly";
                                }
                                $total_job_expenses += $value;
                                $expense_amount = '';
                                $expense_amount = ($value > 0)?round($value,2):'';
                                if(empty($bill_info)) { ?>
									<tr>
                                    <td><?= $job_expense->job_expense_title ?><input type="hidden" name="account_id[]" value="<?= $job_expense->job_expense_id ?>"/></td>
                                        <td>
                                            <input type="number" min="0" placeholder="Bill"
                                                   class="form-control bill_<?= '0' ?> calculator"
                                                   name="bill_amount[]" data="<?= $counter ?>" autocomplete="off" value="<?= $expense_amount ?>" />
                                        </td>
                                        <td colspan="2" class="text-right">
                                            <?= ($value > 0)?"PKR ".number_format($value,2):'' ?>
                                        </td>
                                    </tr>
                                    <?php
                                }else {
                                    $account = $this->invoice_model->check_by(array('bill_id'=>$bill_info->bill_id, 'job_expense_id'=>$job_expense->job_expense_id),'tbl_bill_details');
                                    if(!empty($account)) {
                                        $total += $account->bill_amount;
                                        ?>
										<tr>
                                                <td><?= job_expense_title($account->job_expense_id) ?>
													<input type="hidden" name="bill_detail_id[]" value="<?= $account->bill_detail_id ?>"/>
                                                    <input type="hidden" name="account_id[]" value="<?= $account->job_expense_id ?>"/>
                                                </td>
                                                <td>
                                                    <input type="number" min="0" placeholder="Bill"
														   class="form-control bill_<?= '0' ?> calculator"
														   name="bill_amount[]"
														   value="<?= ($account->bill_amount != '0') ? round($account->bill_amount,2) : '' ?>"
														   data="<?= $counter ?>"
														   autocomplete="off" />
                                                </td>
                                                <td colspan="2" class="text-right">
                                                    <?= ($value > 0)?"PKR ".number_format($value,2):'' ?>
                                                </td>
                                            </tr>
                                        <?php
                                    }
                                }
                                $counter++;
                            }
                        }
                    ?>
                            </tbody>
                        </table>
                    </div>
					<!--- End GET DYNAMICALLY ACCOUNTS BY Client ID --->
                    <div class="col-xs-12">
                        <div class="col-xs-6 table-responsive">
                            <table class="table table-bordered text-right">
                                <tbody>
                                <tr>
                                    <td colspan="2">Sub Total</td>
                                    <td>PKR <b id="total_<?= '0' ?>"><?= number_format($total,2) ?></b></td>
                                </tr>
                                <tr>
                                    <td colspan="2">Amount Of this Bill</td>
                                    <td>PKR <b id="amount_bill"><?php
                                                if (!empty($bill_info)) {
                                                    echo number_format($this->invoice_model->get_bill_total($bill_info->bill_id),2);
                                                }
                                                else{
                                                    echo "0.00";
                                                }
                                            ?></b></td>
                                </tr>


                                <tr>
                                    <td colspan="2">With Holding Tax</td>
                                    <?php
                                    $accounts_heads = $this->invoice_model->check_by(array('H_TYPE'=>'Assets', 'H_NAME'=>'Account Receivables'),'accounts_head');
                                    $account = $this->invoice_model->check_by(array('H_ID'=>$accounts_heads->H_ID,'A_NAME'=>'Witholding tax'),'accounts');
                                        if(empty($bill_info)){ ?>
                                    <td>
                                        <input type="hidden" name="A_ID[]" value="<?= $account->A_ID;?>" />
                                        <input type="hidden" name="TM_AMOUNT[]" value="" class="with_hold" />
                                        <b><input type="number" class="form-control calculator text-right" min="0" name="withholding_tax" id="withholding_tax" value="<?= (!empty($bill_info))?$bill_info->withholding_tax:'' ?>" /></b>
                                        <input type="hidden" name="TM_TYPE[]" value="Debit" />
                                    </td>


                                    <?php }else{
                                            $with_hold_account_id = $this->invoice_model->check_by(array('A_ID'=>$account->A_ID, 'T_ID'=>$bill_info->transaction_id, 'PAYMENT_ID'=>NULL),'transactions_meta');?>
                                            <td>
                                            <input type="hidden" name="TM_ID[]" value="<?= $account->A_ID;?>" />
                                            <input type="hidden" name="TM_AMOUNT[]" value="" class="with_hold" />
                                            <b><input type="number" class="form-control calculator text-right" min="0" name="withholding_tax" id="withholding_tax" value="<?= (!empty($bill_info))?$bill_info->withholding_tax:'' ?>" /></b>
                                            <input type="hidden" name="TM_TYPE[]" value="Debit" />
                                    </td>
                                        <?php } ?>

                                </tr>

                                <tr>
                                    <td colspan="2">Advances</td>
                                    <td>PKR <b id="advances"><?= ($remaining_advances > 0) ? $remaining_advances : '0.00'; ?></b></td>
                                </tr>
                                <!--<tr>
                                    <td colspan="2">Balance Dues</td>
                                    <td>PKR <b id="balance_dues"><?php
/*                                            if (!empty($bill_info)) {
                                                echo number_format($this->invoice_model->get_balance_dues($bill_info->bill_id),2);
                                            }
                                            else{
                                                echo "0.00";
                                            }
                                            */?></b></td>
                                </tr>-->
                                </tbody>
                            </table>
                        </div>

                        <div class="col-xs-6 table-responsive">
                            <table class="table table-bordered text-right">
                                <tbody>
                                <?php
                                    $accounts_heads = $this->invoice_model->check_by(array('H_TYPE'=>'Income', 'H_NAME'=>'Commission'),'accounts_head');
                                    $account = $this->invoice_model->check_by(array('CLIENT_ID'=>$job_info->client_id, 'H_ID'=>$accounts_heads->H_ID, 'A_NAME'=>client_name($job_info->client_id). ' (Service Charges)'),'accounts');
                                    if(empty($bill_info)) {
                                        ?>
										<tr>
                                        <td><?= lang('agency_commission') ?><input type="hidden" name="A_ID[]" value="<?= $account->A_ID ?>"/>
                                        </td>
                                        <td>
                                            <input type="number" min="0" id="SC" class="form-control calculator commission_<?= $account->A_ID ?> text-center" name="TM_AMOUNT[]" autocomplete="off" />
											<input type="hidden" class="SC" name="service_charges" />
                                            <input type="hidden" name="TM_TYPE[]" value="Credit" />
                                        </td>
                                    </tr>
                                        <?php
                                    }else {
                                        $account = $this->invoice_model->check_by(array('A_ID'=>$account->A_ID, 'T_ID'=>$bill_info->transaction_id, 'PAYMENT_ID'=>NULL),'transactions_meta');
                                        ?>
										<tr>
                                        <td><?= read_subHead($account->A_ID)->A_NAME ?><input type="hidden" name="TM_ID[]"
																							  value="<?= $account->TM_ID ?>"/>
                                        </td>
                                        <td>
                                            <input type="number" min="0" id="SC" class="form-control calculator commission_<?= $account->A_ID ?> text-center" name="TM_AMOUNT[]" value="<?= ($account->TM_AMOUNT != '0')?$account->TM_AMOUNT:'' ?>" autocomplete="off" />
											<input type="hidden" class="SC" name="service_charges" value="<?= round($bill_info->service_charges,2) ?>" />
                                        </td>
                                    </tr>
                                        <?php
                                    }
                                ?>
                                <?php

                                    $accounts_heads = $this->invoice_model->check_by(array('H_TYPE'=>'Liabilities', 'H_NAME'=>'Payable Tax'),'accounts_head');
                                    $account = $this->invoice_model->check_by(array('H_ID'=>$accounts_heads->H_ID,'A_NAME'=>'Sales Tax'),'accounts');
                                    if(empty($bill_info)) {
                                        ?>
										<tr>
                                        <td>GST (%)</td>
                                        <td>
                                            <input type="number" min="0" max="100" class="form-control text-center calculator" name="gst" id="gst" autocomplete="off" />
                                        </td>
                                    </tr>
										<tr>
                                        <td><?= $account->A_NAME ?><input type="hidden" name="A_ID[]" value="<?= $account->A_ID ?>"/>
                                        </td>
                                        <td>
                                            <input class="tax_<?= $account->A_ID ?>" type="hidden" value="" />
                                            <input type="text" class="form-control text-center sales_tax_amount" name="TM_AMOUNT[]" readonly/>
                                            <input type="hidden" name="TM_TYPE[]" value="Credit"/>


                                        </td>
                                    </tr>
                                        <?php
                                    }else {
                                        $account = $this->invoice_model->check_by(array('A_ID'=>$account->A_ID, 'T_ID'=>$bill_info->transaction_id, 'PAYMENT_ID'=>NULL),'transactions_meta');

                                        ?>
										<tr>
                                        <td>GST</td>
                                        <td>
                                            <input type="text" class="form-control text-center calculator" name="gst" id="gst"  value="<?= round($bill_info->gst,2) ?>" autocomplete="off"/>
                                        </td>
                                    </tr>
										<tr>
                                        <td><?= read_subHead($account->A_ID)->A_NAME ?> <strong>(<?= round($bill_info->gst,2) ?> %)</strong>
                                            <input type="hidden" name="TM_ID[]" value="<?= $account->TM_ID ?>"/>
                                        </td>
                                        <td>
                                            <input class="tax_<?= $account->A_ID ?>"
												   type="hidden" value="<?= $bill_info->gst ?>" />

                                            <input type="text" class="form-control text-center sales_tax_amount" name="TM_AMOUNT[]" id="tax_bill_<?= $account->A_ID ?>"
												   value="<?= ($account->TM_AMOUNT != '0')?$account->TM_AMOUNT:'' ?>" readonly/>

                                        </td>
                                    </tr>
                                        <?php
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-12 text-center">
                        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i>
                            <?php
                                if(!empty($bill_info)){
                                    echo lang('update_bill');
                                }else{
                                    echo lang('create_bill');
                                }
                            ?>
                        </button>
                    </div>
                </div>
            </form>
        </section>
    </div>

</section>
<script type="text/javascript">
    $(document).ready(function() {
        calculator();
        /*Duty Calculator*/
        function calculator(){
            var invoice_value = <?= round($this->invoice_model->get_invoice_value($job_info->invoices_id),2) ?>;
            var exchange_rate = $('#exchange_rate').val() == '' ? 0.00 : parseFloat($('#exchange_rate').val());
            if(exchange_rate == ""){
                $('.exchange_rate').show();
                $('html, body').animate({
                    scrollTop: ($('#exchange_rate').offset().top)
                },500);
            }
            else {
                $('.exchange_rate').hide();
            
                var total = 0;
                var sum_0 = 0;
                $('.bill_0').each(function () {
                    if ($(this).val() != '') {
                        var id = $(this).attr('data');
                        $('.bill_payable_'+id).val($(this).val());
                        sum_0 = sum_0 + parseFloat($(this).val());
                    }
                });
                total += sum_0;
                $('#total_0').text(sum_0.toFixed(2));
                $('.sub_total_bills').val(total.toFixed(2));
                $('.client_expense').val(<?= $total_job_expenses ?>);
                var turnover_total = total - <?= $total_job_expenses ?>;
                $('.turnover_total').val(turnover_total.toFixed(2));
            
                /*** SALES TAX ***/
                var service_charges = $('#SC').val() == '' ? 0.00 : parseFloat($('#SC').val());
                var sales_tax = $('#gst').val() == '' ? 0.00 : parseFloat($('#gst').val());
                $('.SC').val(service_charges.toFixed(2));
                var gst = service_charges/100 * sales_tax;
                $('.sales_tax_amount').val(gst.toFixed(2));
                /*** End Sales Tax ***/
                total = total+service_charges;
                total = total+gst;
                /*** WITHHOLDING TAX ***/
                var withholding_tax = $('#withholding_tax').val();
                total = total-withholding_tax;
                $('.grand_total_bills').val(total.toFixed(2));
                var amount_received = $('#advances').text();
                var balance_dues = total - amount_received;
                balance_dues = balance_dues - withholding_tax;
                $('#amount_bill').text(total.toFixed(2));
                $('.with_hold').val(withholding_tax);
                $('#balance_dues').text(balance_dues.toFixed(2));
            }
        }
    
        $('.calculator').keyup(function() {
            calculator();
        });
        $('.calculator').change(function(){
            calculator();
        });
    
    });

</script>