<?= message_box('success') ?>

<?php echo form_open(base_url('admin/bill/all_bills'),array('class'=>"form-horizontal")); ?>

<div class="row">
<div class="col-lg-12">
    <section class="panel panel-default">
        <header class="panel-heading  "><?= lang('search_by'); ?></header>
        <div class="panel-body">
            <div class="form-group">
                <div class="col-xs-2">
                    <select class="form-control select_box client" style="width: 100%"  name="client_id">
                        <option value="">Choose Client</option>
                        <?php
                        if (!empty($all_client)) {
                            foreach ($all_client as $v_client) {
                                ?>
                                <option value="<?= $v_client->client_id ?>" <?php
                                if(isset($client_id)){
                                    echo ($v_client->client_id == $client_id)?'selected':'';
                                }  ?>><?= $v_client->name ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-xs-2">
                    <input type="text" class="form-control" name="reference_no" placeholder="<?= lang('reference_no') ?>" value="<?= (!empty($reference_no))?$reference_no:'' ?>" />
                </div>
                <div class="col-xs-2">
                    <input type="text" class="form-control" name="bl_no" placeholder="<?= lang('bl_no') ?>" value="<?= (!empty($bl_no))?$bl_no:'' ?>" />
                </div>
                <div class="col-xs-2">
                    <input type="text" class="form-control" name="commodity" placeholder="<?= lang('commodity') ?>" value="<?= (!empty($commodity))?$commodity:'' ?>" />
                </div>
                <div class="col-xs-2">
                    <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> <?= lang('search') ?></button>
                </div>
            </div>
        </div>
    </section>
</div>
</div>
<?php echo form_close(); ?>
<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('list_bill') ?></h1>
<!--<section class="panel panel-default">
    <header class="panel-heading"><?/*= lang('list_bill') */?> </header>
    <div class="panel-body">
        <table class="table table-striped DataTables " id="DataTables">
            <thead>
            <tr>
                <th><?/*= lang('reference_no') */?></th>
                <th><?/*= lang('client') */?></th>
                <th class="col-date"><?/*= lang('created_date') */?></th>
                <th class="col-currency"><?/*= lang('invoice_value') */?></th>
                <th class="col-options no-sort" ><?/*= lang('action') */?></th>
            </tr>
            </thead>
            <tbody>
            <?php
/*            if (!empty($invoices_info)) {
                foreach ($invoices_info as $job_info) {
                    $bill_info = $this->invoice_model->check_by(array('invoices_id' => $job_info->invoices_id), 'tbl_bills');
                    $client_info = $this->invoice_model->check_by(array('client_id' => $job_info->client_id), 'tbl_client');
                    */?>
                    <tr>
                        <td><a class="text-info" href="<?/*= base_url() */?>admin/invoice/manage_invoice/invoice_details/<?/*= encode($job_info->invoices_id) */?>"><?/*= $this->invoice_model->job_no_creation($job_info->invoices_id) */?></a>
                        </td>
                        <td><?/*= $client_info->name */?></td>
                        <td><?/*= strftime(config_item('date_format'), strtotime($job_info->created_date)) */?></td>
                        <td><?/*= $job_info->currency */?> <?/*= number_format($job_info->invoice_value,2) */?></td>
                        <td>
                            <div class="btn-group">
                                <button class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                    Action
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <?php
/*                                    if (!empty($bill_info)) {
                                        */?>
                                        <li>
                                            <a href="<?/*= base_url() */?>admin/bill/manage_bill/edit_bill/<?/*= encode($bill_info->bill_id) */?>" target="_blank">Edit
                                                Bill</a></li>
                                        <li>
                                            <a href="<?/*= base_url() */?>admin/bill/manage_bill/bill_details/<?/*= encode($bill_info->bill_id) */?>" target="_blank">Preview
                                                Bill</a></li>
                                        <li>
                                            <a href="<?/*= base_url() */?>admin/bill/manage_bill/sales_tax_invoice/<?/*= encode($bill_info->bill_id) */?>" target="_blank">Sales
                                                Tax Invoice</a></li>
                                        <li>
                                            <a href="<?/*= base_url() */?>admin/invoice/manage_invoice/payment/<?/*= encode($job_info->invoices_id) */?>/<?/*= encode($job_info->client_id) */?>" target="_blank"><?/*= lang('payment') */?></a>
                                        </li>
                                        <?php
/*                                    } else {
                                        */?>
                                        <li>
                                            <a href="<?/*= base_url() */?>admin/bill/manage_bill/create_bill/<?/*= encode($job_info->invoices_id) */?>">Create
                                                Bill</a></li>
                                        <?php
/*                                    }
                                    */?>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <?php
/*                }
            }
            */?>
            </tbody>
        </table>
    </div>
</section>-->
<input type="hidden" id="client_id" value="<?= (!empty($client_id))?$client_id:'All' ?>">
<input type="hidden" id="reference_no" value="<?= (!empty($reference_no))?$reference_no:'All' ?>">
<input type="hidden" id="bl_no" value="<?= (!empty($bl_no))?$bl_no:'All' ?>">
<input type="hidden" id="commodity" value="<?= (!empty($commodities))?$commodities:'All' ?>">
<section class="panel panel-default">
    <header class="panel-heading"><?= $title; ?></header>
    <div class="panel-body" id="export_table">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered data-table-list">
                        <thead>
                        <th><?= lang('reference_no') ?></th>
                        <th><?= lang('client') ?></th>
                        <th><?= lang('created_date') ?></th>
                        <th><?= lang('invoice_value') ?></th>
                        <th class="col-options no-sort" ><?= lang('action') ?></th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var client_id=$('#client_id').val();
    var reference_no=$('#reference_no').val();
    var bl_no=$('#bl_no').val();
    var commodity=$('#commodity').val();
    var data_url='<?= base_url('admin/bill/all_bill/') ?>'+client_id+'/'+reference_no+'/'+bl_no+'/'+commodity;
</script>