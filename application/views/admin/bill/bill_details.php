
<section class="content-header">
    <div class="row">
        <div class="col-sm-8">

        </div>
        <div class="col-sm-4 pull-right">
            <a onclick="print_invoice('print_invoice_new')"style="margin-right: 5px"  href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Print" class="btn btn-sm btn-danger pull-right"  >
                Print All
            </a>
            <a onclick="print_invoice_receipted('print_invoice_receipted')"style="margin-right: 5px"  href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Print Receipted" class="btn btn-sm btn-danger pull-right"  >
                Print Receipted
            </a>
            <a onclick="print_invoice_unreceipted('print_invoice_unreceipted')" style="margin-right: 5px"  href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Print Unreceipted" class="btn btn-sm btn-danger pull-right"  >
                Print Unreceipted
            </a>
        </div>
        <!-- <div class="col-sm-4 pull-right">
            <a href="<?/*= base_url() */?>admin/po/manage_po/pdf_po/<?/*= $po_info->po_id */?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="PDF" class="btn btn-sm btn-warning pull-right" >
                <i class="fa fa-file-pdf-o"></i>
            </a>
        </div>-->

    </div>
</section>
<section class="content">

    <!-- Start Display Details -->
    <!-- Main content -->
    <div class="row" >
<h1 class="header-<?= config_item('sidebar_theme'); ?>" style="border-bottom: 0px solid #ecf0f5; margin: 0px 0px 0px 15px"><?= lang('prev_bill') ?></h1>
        <section class="invoice" id="print_invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-2">
                    <!--<h2 class="page-header">
                        <img style="width: 60px;width: 60px;margin-top: -10px;margin-right: 10px;" src="<?/*= base_url() . config_item('invoice_logo') */?>" ><?/*= config_item('company_name') */?>
                    </h2>-->
                </div><!-- /.col -->
                <div class="col-xs-8">
                    <h1 style="font-size:50px;font-family: fantasy;letter-spacing: 3px;text-align: center;"><?= config_item('company_name') ?></h1>
                    <h5 style="font-size: 14px;font-family: sans-serif;text-align: center;"><?= config_item('company_address') ?>, <?= config_item('company_city') ?>, <?= config_item('company_country') ?></h5>
                    <h5 style="font-size: 14px;font-family: sans-serif;text-align: center;"><strong>Tel:</strong> <?= config_item('company_phone') ?></h5>
                    <h5 style="font-size: 14px;font-family: sans-serif;text-align: center;"><strong>Email:</strong> <?= config_item('company_email') ?>, <strong>Website:</strong> <?= config_item('company_domain') ?></h5>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h3 style="font-size:25px;font-family: initial;text-decoration-line: underline;letter-spacing: 5px;text-align: center;"><strong>EXPENSE DETAIL</strong></h3>
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-6">
                        <h4 style="text-decoration-line: underline;font-family: serif;"><strong>JOB #</strong> <?= $this->invoice_model->job_no_creation($job_info->invoices_id) ?></h4>
                    </div>
                    <div class="col-xs-6 text-right">
                        <h4 style="text-decoration-line: underline;font-family: serif;"><strong>DATE:</strong> <?= strftime(config_item('date_format'), strtotime($bill_info->delivery_date)) ?></h4>
                    </div>
                </div>
                <div class="col-xs-12 table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td><b>Bill to</b></td>
                            <td><?= ucfirst($job_info->name) ?></td>
                            <td><b>Contact Person Details</b></td>
                            <td><?= ucfirst($job_info->contact_p_name) ?></td>
                        </tr>
                        <tr>
                            <td><b>Address:</b></td>
                            <td colspan="3"> <?= $job_info->address ?></td>
                        </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td><b>Consignee/Consignor Name:</b></td>
                            <td colspan="5"><?= $job_info->consignor_name ?></td>
                        </tr>
                        <tr>
                            <td><b>Address:</b></td>
                            <td colspan="5"><?= $job_info->consignor_address ?></td>
                        </tr>
                        <tr>
                            <td><b>I.G.M / VIR#</b></td>
                            <td><?= $job_info->igm_no ?></td>
                            <td><b>Index</b></td>
                            <td><?= $job_info->index_no ?></td>
                            <td><b>BL/AWB with Dt:</b></td>
                            <td><?= $job_info->bl_no ?></td>
                        </tr>
                        <tr>
                            <td><b>Loading Port</b></td>
                            <td><?= $job_info->p_o_l ?></td>
                           <!-- <td><b>GD #</b></td>
                            <td><?/*= $job_info->gd_machine */?></td>-->
                        </tr>
                        <tr>
                            <td><b>Discharge Port</b></td>
                            <td><?= $job_info->p_o_d ?></td>
                            <td><b>Declared Value:</b></td>
                            <td><?= number_format($this->invoice_model->get_declared_value($job_info->invoices_id),2) ?></td>
                            <td><b>LC Number & Dt:</b></td>
                            <td><?= $job_info->lc_no ?></td>
                        </tr>
                        <tr>
                            <td><b>Forwarder Name:</b></td>
                            <td><?= $job_info->forwarding_agent ?></td>
                            <td><b>Assessed Value:</b></td>
                            <td><?= number_format($this->invoice_model->get_assessable_value($job_info->invoices_id),2) ?></td>
                        </tr>
                        <tr>
                            <td><b>Shipping/Air Line</b></td>
                            <td><?= shipping_line($job_info->shipping_line) ?></td>
                            <td><b>Consignment Type</b></td>
                            <td><?= $job_info->consignment_type ?></td>
                            <td><b>Pkges</b></td>
                            <td><?= $packages ?></td>
                        </tr>
                        <tr>
                            <td><b>Item Detail:</b></td>
                            <td>
                                <?php
                                $item_info = $this->invoice_model->check_by_all(array('invoices_id'=>$job_info->invoices_id), 'tbl_saved_commodities');
                                if(!empty($item_info)){
                                    foreach($item_info as $item){
                                        echo $item->commodity.", ";
                                    }
                                }
                                ?>
                            </td>
                            <td><b>Net. Wt:</b></td>
                            <td><?= ($job_info->net_weight == 0)?'':$job_info->net_weight ?></td>
                            <td><b>Gross Wt.</b></td>
                            <td><?= ($job_info->gross_weight == 0)?'':$job_info->gross_weight ?></td>
                        </tr>
                        <tr>
                            <td><b>Container #/Size detail:</b></td>
                            <td colspan="5">
                                <?php
                                $container_info = $this->invoice_model->check_by_all(array('invoices_id'=>$job_info->invoices_id), 'tbl_containers');
                                if(!empty($container_info)){
                                    foreach($container_info as $container){
                                        echo $container->container_no."/".$container->container_ft.", ";
                                    }
                                }
                                ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    
                    <!-- RECEIPT VOUCHER -->
                    <div class="col-xs-12 table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th colspan="10">Pay Order</th>
                            </tr>
                            </thead>
                            <tr class="text-center">
                                <td><b>Title</b></td>
                                <td><b>Amount</b></td>
                            </tr>
                            <?php
                            foreach ($receipt_voucher_info as $receipt) {
                                ?>
                                <tr class="text-center">
                                    <td><?= $receipt->apd_title; ?></td>
                                    <td><?= $receipt->apd_amount; ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                    </div>
                    
                    
                    <div class="col-xs-12 table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <th style="text-align: center;">CHARGE TO CUSTOMER</th>
                            <th style="text-align: center;">ACTUAL PAID</th>
                            <th colspan="4" style="text-align: center;">DESCRIPTION</th>
                            </thead>
                            <tbody>
                            <?php
                            $total = 0;
                            $count = 1;
                            foreach($job_expenses as $job_expense) {
                                $account = $this->invoice_model->check_by(array('bill_id' => $bill_info->bill_id, 'job_expense_id'=>$job_expense->job_expense_id), 'tbl_bill_details');
                                if (!empty($account)) {
                                    $value = $this->invoice_model->job_expenses_by_invoice_account($job_info->invoices_id,$job_expense->job_expense_id)->amount;
                                    $total += $account->bill_amount;
                                    if($account->bill_amount == 0 && $account->bill_amount == 0) {

                                    }
                                    else{
                                        ?>
                                        <tr>
                                            <td class="text-center">PKR <?= number_format(($account->bill_amount), 2) ?></td>
                                            <td class="text-center"><?= ($value > 0)?'PKR '.$value:'' ?></td>
                                            <td colspan="4"><?= job_expense_title($account->job_expense_id) ?>&nbsp;&nbsp;
                                                <?php
                                                if(job_expense_title($account->job_expense_id) == 'Local Transport of Container / Cargo From' || job_expense_title($account->job_expense_id) == 'Transportation of Container / Cargo From') {
                                                    ?>
                                                    <b style="text-decoration: underline"><?= $account->bill_from ?></b>
                                                    <span class="pull-right">Destination&nbsp;&nbsp; <b
                                                                style="text-decoration: underline"><?= $account->bill_to ?></b></span>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                $count++;
                            }
                            $commission_heads = $this->invoice_model->check_by(array('H_TYPE'=>'Income', 'H_NAME'=>'Commission'),'accounts_head');
                            $commissions = $this->invoice_model->check_by(array('CLIENT_ID'=>$job_info->client_id, 'H_ID'=>$commission_heads->H_ID),'accounts');
                            $commission = $this->invoice_model->check_by(array('A_ID'=>$commissions->A_ID, 'T_ID'=>$bill_info->transaction_id),'transactions_meta');
							$st_heads = $this->invoice_model->check_by(array('H_TYPE'=>'Liabilities', 'H_NAME'=>'Payable Tax'),'accounts_head');
							$sts = $this->invoice_model->check_by(array('H_ID'=>$st_heads->H_ID,'A_NAME'=>'Sales Tax'),'accounts');
                            $st = $this->invoice_model->check_by(array('A_ID'=>$sts->A_ID, 'T_ID'=>$bill_info->transaction_id),'transactions_meta');
                            $received_advances = $bill_info->current_advance;
                            $total_bill = $bill_info->total_bill;
                            $balance_dues = $received_advances - $total_bill;
                            ?>
                            <tr>
                                <td class="text-center">PKR <?= number_format(($commission->TM_AMOUNT),2) ?></td>
                                <td class="text-center"></td>
                                <td colspan="4">Service Charges</td>
                            </tr>
                            <tr>
                                <td class="text-center">PKR <?= number_format(($st->TM_AMOUNT), 2) ?></td>
                                <td class="text-center"></td>
                                <td colspan="4">Sales Tax on Custom Clearing Services Rs: <?= number_format(($commission->TM_AMOUNT),2) ?> Services @ 13 %</td>
                            </tr>
                            <tr>
                                <td class="text-center"><b>PKR <?= number_format(($total),2) ?></b></td>
                                <td></td>
                                <td><b>Sub Total</b></td>
                            </tr>
                            <tr>
                                <td class="text-center"><b>PKR <?= number_format(($total_bill),2) ?></b></td>
                                <td></td>
                                <td><b>Total Bill Amount</b></td>
                            </tr>
                            <tr>
                                <?php

                                ?>
                                <td class="text-center"><b>PKR <?= ($received_advances > 0)?number_format($received_advances,2):'0.00' ?></b></td>
                                <td></td>
                                <td><b>Advances</b></td>
                            </tr>
                            <tr>
                                <td class="text-center"><b>PKR <?= number_format($bill_info->withholding_tax,2) ?></b></td>
                                <td></td>
                                <td><b>Withholding Tax</b></td>
                            </tr>
                           <!-- <tr>
                                <td class="text-center"><b>PKR <?/*= ($balance_dues>0)? '-'.number_format($balance_dues,2) : number_format($balance_dues,2); */?></b></td>
                                <td></td>
                                <td><b>Outstanding Balance</b></td>
                            </tr>-->
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </section>
        <section style="display: none;" class="invoice" id="print_invoice_new">
            <?php $this->load->view('admin/bill/pdf_bill'); ?>
        </section>
        <section style="display: none;" class="invoice" id="print_invoice_receipted">
            <?php $this->load->view('admin/bill/pdf_bill_receipted'); ?>
        </section>
        <section style="display: none;" class="invoice" id="print_invoice_unreceipted">
            <?php $this->load->view('admin/bill/pdf_bill_unreceipted'); ?>
        </section>
    </div>

</section>
<script type="text/javascript">
    function print_invoice(print_invoice_new) {
        var printContents = document.getElementById(print_invoice_new).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
    function print_invoice_receipted(print_invoice_receipted) {
        var printContents = document.getElementById(print_invoice_receipted).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
    function print_invoice_unreceipted(print_invoice_unreceipted) {
        var printContents = document.getElementById(print_invoice_unreceipted).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>