<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html lang=en>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Invoice Bill</title>
    <style>
        html{/*margin:3px 15px !important;*/}
        body {
            background: white;
            color: black;
            margin: 0px;
        }


        #h11 {
            color: #000;
            background: none;
            font-weight: bold;
            font-size: 2.5em;
            margin: 10px 0 0 0;
            text-align: center;
            font-size:32px;
        }
        #h12
        {
            color: #000;
            background: none;
            font-weight: bold;
            font-size: 2em;
            margin: 10px 0 0 0;
            text-align: center;
            font-size:25px;
        }

        h2 {
            color: #00008b;
            background: none;
            font-weight: bold
        }

        h3 {
            color: #000;
            background: none;
            margin-left: 4%;
            margin-right: 4%;
            font-weight: bold;
            margin: 10px 0 0 0;
            text-align: center;
        }

        h4 {
            margin-left: 6%;
            margin-right: 6%;
            font-weight: bold
        }

        h5 {
            margin-left: 6%;
            margin-right: 6%;
            font-weight: bold
        }

        ul, ol, dl, p {
            margin-left: 6%;
            margin-right: 6%
        }

        ul ul, table ol, table ul, dl ol, dl ul {
            margin-left: 1.2em;
            margin-right: 1%;
            padding-left: 0
        }

        pre {
            margin-left: 10%;
            white-space: pre
        }

        /* table caption {
          font-size: larger;
          font-weight: bolder
        } */

        /* table p, table dl, ol p, ul p, dl p, blockquote p, .note p, .note ul, .note ol, .note dl, li pre, dd pre {
          margin-left: 2%;
          margin-right: 2%;
        } */
        td
        {
            align: right;
        }

        p.top {
            margin-left: 1%;
            margin-right: 1%
        }

        blockquote {
            margin-left: 8%;
            margin-right: 8%;
            border: thin ridge #dc143c
        }

        blockquote pre {
            margin-left: 1%;
            margin-right: 1%
        }


        .css {
            color: #800000;
            background: none
        }

        .javascript {
            color: #008000;
            background: none
        }

        .example { margin-left: 10% }

        dfn {
            font-style: normal;
            font-weight: bolder
        }

        var sub { font-style: normal }

        .note {
            font-size: 85%;
            margin-left: 10%
        }

        .SMA {
            color: fuchsia;
            background: none;
            font-family: Kids, "Comic Sans MS", Jester
        }

        .oops {
            font-family: Jester, "Comic Sans MS"
        }

        .author {
            font-style: italic
        }

        .copyright {
            font-size: smaller;
            text-align: right;
            clear: right
        }

        .toolbar {
            text-align: center
        }

        .toolbar IMG {
            float: right
        }

        .error {
            color: #DC143C;
            background: none;
            text-decoration: none
        }

        .warning {
            color: #FF4500;
            background: none;
            text-decoration: none
        }

        .error strong {
            color: #DC143C;
            background: #FFD700;
            text-decoration: none
        }

        .warning strong {
            color: #FF4500;
            background: #FFD700;
            text-decoration: none
        }


        colgroup.entity { text-align: center }

        .default { text-decoration: underline; font-style: normal }
        .required { font-weight: bold }
        td li.transitional, .elements li.transitional {
            font-weight: lighter;
            color: #696969;
            background: none
        }
        td li.frameset, .elements li.frameset {
            font-weight: lighter;
            color: #808080;
            background: none
        }

        .footer, .checkedDocument {

            width:93%;
            margin-left:3%;
            border-top: solid thin black
        }

        strong.legal {
            font-weight: normal;
            text-transform: uppercase
        }

        @media print {
            input#toggler, .toolbar, { display: none; }
            html, body {
                height:100vh;
                margin: 0 !important;
                padding: 0 !important;
                overflow: hidden;
            }
            .table-padding td{
                padding:5px;
            }
            td {
                height:1px;
                font-size:12px;
                padding:0px 0px 0px 10px;
                font-family: 'bitstream cyberbit', sans-serif;
            }
            th{
                text-align:center;
                padding:5px;
            }
        }

        table { width: 100%; }

    </style>
<meta name="author" content="">
<meta name="description" content="">
<meta name="keywords" content="">
<body style="margin-left:3%; margin-right:3%;">
<div class="col-xs-12">
    <div class="col-xs-6">
        <table class="table-padding2" style="margin-top:150px;">
            <thead>
            </thead>
            <?php $client_info = $this->invoice_model->check_by(array('client_id'=>$job_info->client_id), 'tbl_client'); ?>
            <tr>
                <td style="border:1px solid #8b8b8b;"><b><?= lang('created_date') ?> : </strong> <?= strftime(config_item('date_format'), strtotime($job_info->created_date)) ?></b></td>
            </tr>
            <tr>
                <td style="border:1px solid #8b8b8b;">Bill No.: <b><?= $bill_info->bill_no ?></b></td>
            </tr>
            <tr>
                <td style="border:1px solid #8b8b8b;"><?= lang('reference_no') ?>: <b><?= $this->invoice_model->job_no_creation($job_info->invoices_id) ?></b></td>
            </tr>
            <tr>
                <td style="border:1px solid #8b8b8b;">To Messer's :  <b><?= ucfirst($client_info->name) ?></b></td>
            </tr>
            <tr>
                <td style="border:1px solid #8b8b8b;">Address :  <b><?= ucfirst($client_info->address) ?></b></td>
            </tr>
        </table>
        <table class="table-padding2" style="margin-top:20px;">
            <tbody>
            <tr style="border:1px solid #8b8b8b;">
                <td><?= lang('lc_no') ?></td>
                <td class="text-right"><b><?= $job_info->lc_no ?></b></td>
            </tr>
            <tr style="border:1px solid #8b8b8b;">
                <td>Invoice Value</td>
                <td class="text-right"><b><?= $job_info->currency ?> <?= number_format($this->invoice_model->get_invoice_value($job_info->invoices_id),2) ?></b></td>
            </tr>
            <tr style="border:1px solid #8b8b8b;">
                <td>Assessable Value</td>
                <td class="text-right"><b>PKR <?= number_format(($this->invoice_model->get_invoice_value($job_info->invoices_id)*$job_info->exchange_rate),2) ?></b></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-xs-6">
        <table style="margin-top:220px;">
            <thead>
            <tr style="border:1px solid #8b8b8b;">
                <th colspan="3" style="text-align:left;"><b>Description of consignment</b></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $commodities = $this->invoice_model->check_by_all(array('invoices_id'=>$job_info->invoices_id), 'tbl_saved_commodities');
            if(!empty($commodities)) {
                foreach ($commodities as $commodity) {
                    ?>
                    <tr>
                        <td style="border:1px solid #8b8b8b;"><?= lang('commodity') ?></td>
                        <td style="border:1px solid #8b8b8b;"><?= $commodity->commodity ?></td>
                    </tr>
                    <?php
                }
            }
            ?>
            <tr>
                <td style="border:1px solid #8b8b8b;">Net Weight</td>
                <td style="border:1px solid #8b8b8b;"><b><?php
                        if($job_info->net_weight != 0.00){
                            echo $job_info->net_weight ." KGS";
                        } ?></b></td>
            </tr>
            <tr>
                <td style="border:1px solid #8b8b8b;">Gross Weight</td>
                <td style="border:1px solid #8b8b8b;"><b><?php
                        if($job_info->gross_weight != 0.00){
                            echo $job_info->gross_weight ." KGS";
                        } ?></b></td>
            </tr>
            <!--<tr>
                <td style="border:1px solid #8b8b8b;">Vessel</td>
                <td style="border:1px solid #8b8b8b;"><b><?/*= $job_info->vessel */?></b></td>
            </tr>-->
            <tr>
                <td style="border:1px solid #8b8b8b;">From/To</td>
                <td style="border:1px solid #8b8b8b;"><?= $job_info->p_o_l.' '.$job_info->port.", ".$job_info->p_o_d ?></td>
			
            </tr>
            <tr>
                <td style="border:1px solid #8b8b8b;">IGM</td>
                <td style="border:1px solid #8b8b8b;"><b><?= $job_info->igm_no ?></b></td>
            </tr>
            <tr>
                <td style="border:1px solid #8b8b8b;">Bill/Index</td>
                <td style="border:1px solid #8b8b8b;"><b><?= $job_info->index_no ?></b></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
  <br>
	<div>
		<div class="col-xs-12" style="margin-top:50px;">
        <table class="table-padding">
			<thead>
				<tr style="border:1px solid;">
					<th colspan="4" class="text-center"><h4>Details of Sales Tax Payable</h4></th>
				</tr>
			</thead>
			<tbody>
                <?php
                $agency_commission = $bill_info->service_charges;
                    ?>
                    <tr>
                        <td class="text-right"><?= lang('agency_commission') ?></td>
                        <td class="text-center">PKR <b><?= number_format(($agency_commission),2) ?></b>
                        </td>
                    </tr>
                    <tr >
                        <td class="text-right" > Sales Tax Amount @ <?= $bill_info->gst ?> % </td >
                        <td class="text-center" >PKR <b class="tax">
                                <?php
                                $tax_per = $agency_commission / 100 * $bill_info->gst;
                                echo number_format(($tax_per), 2)
                                ?></b>
                        </td>
                    </tr>
				<tr>
                    <td colspan="3">In Words: <strong>Rupees <strong class="convert_amount"><?= $this->invoice_model->convert_number($tax_per) ?></strong> ONLY</strong></td>
				</tr>
			</tbody>
		</table>
        </div>
	</div>

</body>
</html>
