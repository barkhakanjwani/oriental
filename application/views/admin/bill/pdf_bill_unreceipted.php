<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html lang=en>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Invoice Bill</title>
    <style>
        html{/*margin:3px 15px !important;*/}
        body {
            background: white;
            color: black;
            margin: 0px;
        }
        table { width: 100%; }
        @media print {
            table{font-size:15px;}
        }

    </style>
    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
<body style="margin-left:3%; margin-right:3%;">
<div class="col-xs-12">
    <h1 style="font-size:55px;font-family: fantasy;letter-spacing: 3px;text-align: center;"><?= config_item('company_name') ?></h1>
    <p style="font-size:15px;font-family: sans-serif;text-align: center;"><strong>Head Office:</strong> <?= config_item('company_address') ?>, <?= config_item('company_city') ?>, <?= config_item('company_country') ?></p>
    <p style="font-size:15px;font-family: sans-serif;text-align: center;"><strong>Tel:</strong> <?= config_item('company_phone') ?></p>
    <p style="font-size:15px;font-family: sans-serif;text-align: center;"><strong>Email:</strong> <?= config_item('company_email') ?>, <strong>Website:</strong> <?= config_item('company_domain') ?></p>
</div>
<div class="col-xs-12">
    <h2 style="font-size:25px;font-family: initial;text-decoration-line: underline;letter-spacing: 5px;text-align: center;"><strong>EXPENSE DETAIL</strong></h2>
</div>
<div class="col-xs-12 form-group">
    <div class="col-xs-6 text-left">
        <h4 style="text-decoration-line: underline;font-family: serif;"><strong>JOB #</strong> <?= $this->invoice_model->job_no_creation($job_info->invoices_id) ?></h4>
    </div>
    <div class="col-xs-6 text-right">
        <h4 style="text-decoration-line: underline;font-family: serif;"><strong>DATE:</strong> <?= strftime(config_item('date_format'), strtotime($bill_info->delivery_date)) ?></h4>
    </div>
</div>

<div class="col-xs-12">
    <div class="col-xs-12">
        <table class="table" style="border: 2px solid black;">
            <tbody style="border: 2px solid black;">
            <tr style="border: 2px solid black;">
                <td><b>Bill to</b></td>
                <td><b><?= ucfirst($job_info->name) ?></b></td>
                <td><b>Contact Person Details</b></td>
                <td><b><?= ucfirst($job_info->contact_p_name) ?></b></td>
            </tr>
            <tr style="border: 2px solid black;">
                <td><b>Address:</b></td>
                <td colspan="3"> <b><?= $job_info->address ?></b></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-xs-12">
        <table class="table" style="border: 2px solid black;">
            <tbody style="border: 2px solid black;">
            <tr style="border: 2px solid black;">
                <td><b><?= lang('consignee_consignor') ?></b></td>
                <td colspan="5"><b><?= $job_info->consignor_name ?></b></td>
            </tr>
            <tr style="border: 2px solid black;">
                <td><b><?= lang('address') ?></b></td>
                <td colspan="5"><b><?= $job_info->consignor_address ?></b></td>
            </tr>
            <tr style="border: 2px solid black;">
                <td><b>I.G.M / VIR#</b></td>
                <td><b><?= $job_info->igm_no ?></b></td>
                <td><b>Index</b></td>
                <td><b><?= $job_info->index_no ?></b></td>
                <td><b>BL/AWB with Dt:</b></td>
                <td><b><?= $job_info->bl_no ?></b></td>
            </tr>
            <tr style="border: 2px solid black;">
                <td><b>Loading Port</b></td>
                <td><b><?= $job_info->p_o_l ?></b></td>
               <!-- <td><b>GD #</b></td>
                <td><b><?/*= $job_info->gd_machine */?></b></td>-->
            </tr>
            <tr style="border: 2px solid black;">
                <td><b>Discharge Port</b></td>
                <td><b><?= $job_info->p_o_d ?></b></td>
                <td><b>Declared Value:</b></td>
                <td><b><?= number_format($this->invoice_model->get_declared_value($job_info->invoices_id),2) ?></b></td>
                <td><b>LC Number & Dt:</b></td>
                <td><b><?= $job_info->lc_no ?></b></td>
            </tr>
            <tr style="border: 2px solid black;">
                <td><b>Forwarder Name:</b></td>
                <td><b><?= $job_info->forwarding_agent ?></b></td>
                <td><b>Assessed Value:</b></td>
                <td><b><?= number_format($this->invoice_model->get_assessable_value($job_info->invoices_id),2) ?></b></td>
            </tr>
            <tr style="border: 2px solid black;">
                <td><b>Shipping/Air Line</b></td>
                <td><b><?= $job_info->shipping_line ?></b></td>
                <td><b>Consignment Type</b></td>
                <td><b><?= $job_info->consignment_type ?></b></td>
                <td><b>Pkges</b></td>
                <td><b><?= $packages ?></b></td>
            </tr>
            <tr style="border: 2px solid black;">
                <td><b>Item Detail:</b></td>
                <td><b>
                        <?php
                        $item_info = $this->invoice_model->check_by_all(array('invoices_id'=>$job_info->invoices_id), 'tbl_saved_commodities');
                        if(!empty($item_info)){
                            foreach($item_info as $item){
                                echo $item->commodity.", ";
                            }
                        }
                        ?></b>
                </td>
                <td><b>Net. Wt:</b></td>
                <td><b><?= ($job_info->net_weight == 0)?'':$job_info->net_weight ?></b></td>
                <td><b>Gross Wt.</b></td>
                <td><b><?= ($job_info->gross_weight == 0)?'':$job_info->gross_weight ?></b></td>
            </tr>
            <tr style="border: 2px solid black;">
                <td><b>Container #/Size detail:</b></td>
                <td colspan="5"><b>
                        <?php
                        $container_info = $this->invoice_model->check_by_all(array('invoices_id'=>$job_info->invoices_id), 'tbl_containers');
                        if(!empty($container_info)){
                            foreach($container_info as $container){
                                echo $container->container_no."/".$container->container_ft.", ";
                            }
                        }
                        ?>
                    </b></td>
            </tr>
            </tbody>
        </table>
    </div>
    <?php
    /** COST OF SALES **/
    $sub_head_id = $this->invoice_model->check_by(array('HEAD_TYPE'=>'Expense', 'NAME'=>'Cost of Sales'),'sub_heads');
    $account_heads = $this->invoice_model->check_by_all(array('SUB_HEAD_ID'=>$sub_head_id->SUB_HEAD_ID),'accounts_head');
    foreach($account_heads as $head) {
    $accounts = $this->invoice_model->check_by_all(array('CLIENT_ID'=>$job_info->client_id, 'H_ID'=>$head->H_ID),'accounts');
    ?>
    <div class="col-xs-12">
        <table class="table" style="border: 2px solid black;">
            <thead style="border-bottom: 2px solid black !important;">
            <th style="text-align: center;">AMOUNT</th>
            <th colspan="4">DESCRIPTION</th>
            </thead>
            <tbody style="border:2px solid black">
            <?php
            $total = 0;
            $count = 1;
            foreach($job_expenses as $job_expense) {
                $account = $this->invoice_model->check_by(array('bill_id' => $bill_info->bill_id, 'job_expense_id'=>$job_expense->job_expense_id), 'tbl_bill_details');
                if (!empty($account)) {
                    $value = $this->invoice_model->job_expenses_by_invoice_account($job_info->invoices_id,$job_expense->job_expense_id)->amount;
                    if($account->bill_amount == 0) {

                    }
                    else{
                        if($job_expense->job_expense_type == 1) {
                            $total += $account->bill_amount;
                            ?>
                            <tr>
                                <td class="text-center">PKR <?= number_format(($account->bill_amount), 2) ?></td>
                                <td colspan="4"><?= job_expense_title($account->job_expense_id) ?>&nbsp;&nbsp;
                                    <?php
                                    if (job_expense_title($account->job_expense_id) == 'Local Transport of Container / Cargo From' || job_expense_title($account->job_expense_id) == 'Transportation of Container / Cargo From') {
                                        ?>
                                        <b style="text-decoration: underline"><?= $account->bill_from ?></b>
                                        <span class="pull-right">Destination&nbsp;&nbsp; <b
                                                    style="text-decoration: underline"><?= $account->bill_to ?></b></span>
                                        <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                }
                $count++;
            }
            ?>
            <tr>
                <td class="text-center"><b>PKR <?= number_format(($total),2) ?></b></td>
                <td><b>Sub Total</b></td>
            </tr>
            </tbody>
        </table>

        <?php
        }
        ?>
    </div>

    <div class="col-xs-12">
        <p><strong>Remarks:</strong>________________________________________________________________________________________<strong>Signature</strong>___________________________________________________________________________</p>
        </br>
        <p>____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________</p>
    </div>
    <div class="col-xs-12">
        <h4 style="letter-spacing: 2px;"><strong>COUNTRY-WIDE OFFICES:</strong> <b>RAWALPINDI - KARACHI - SIALKOT - LAHORE - FAISALABAD - PESHAWAR</b></h4>
    </div>
    <div class="col-xs-12" style="margin-top:20px;">
        <p class="pull-right"><?= config_item('default_terms') ?></p>
    </div>

</div>
</body>
</html>
