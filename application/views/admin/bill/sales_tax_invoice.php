<style>
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{
		font-size:13px;
		padding:4px;
		vertical-align: middle;
	}
</style>
<section class="content-header">
    <div class="row">
        <div class="col-sm-8">

        </div>
        <div class="col-sm-4 pull-right">
            <a onclick="print_invoice('print_invoice_new')" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Print" class="btn btn-sm btn-danger pull-right"  >
                <i class="fa fa-print"></i>
            </a>
        </div>
    </div>
</section>
<h1 class="header-<?= config_item('sidebar_theme'); ?>" style="margin: 0px 0px 0px 15px;"><?= lang('sales_text') ?></h1>
<section class="content">
    <!-- Start Display Details -->
    <!-- Main content -->
    <div class="row" >
        <section class="invoice" id="print_invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <img style="width: 60px;width: 60px;margin-top: -10px;margin-right: 10px;" src="<?=base_url() . config_item('invoice_logo') ?>" ><?= config_item('company_name') ?>
                    </h2>
                </div><!-- /.col -->
            </div>
            <div class="row">
				<div class="col-lg-8">
					S/Tax Invoice No : <strong><?= $bill_info->bill_id ?></strong> <br />
					Bill No : <strong><?= $bill_info->bill_no ?></strong> <br />
					Reference No / Job No : <strong><?= $this->invoice_model->job_no_creation($job_info->invoices_id) ?></strong> <br />
					Reference No L/C : <strong><?= $job_info->lc_no ?></strong> <br />
					M/S : <?= $job_info->name ?>
				</div>
				<div class="col-lg-4 text-right" >
					Dated : <?= strftime(config_item('date_format'), strtotime($job_info->created_date)); ?>
				</div>
                <div class="col-xs-12 table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th colspan="4" class="text-center"><h4>Particulars of Consignment</h4></th>
							</tr>
						</thead>
						<tbody>
                        <?php
                        $commodities = $this->invoice_model->check_by_all(array('invoices_id'=>$job_info->invoices_id), 'tbl_saved_commodities');
                        if(!empty($commodities)) {
                            foreach ($commodities as $commodity) {
                                ?>
                                <tr>
                                    <td><?= lang('commodity') ?></td>
                                    <td colspan="2"><?= $commodity->commodity ?></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
							<tr>
								<td>From/To</td>
								<td><?= $job_info->p_o_l ?></td>
                                <td><?= $job_info->port.", ".$job_info->p_o_d ?></td>
							</tr>
							<!--<tr>
								<td>EX/Per</td>
								<td colspan="2"><?/*= $job_info->vessel */?></td>
							</tr>-->
							<tr>
								<td>IGM No/Date</td>
								<td><?= $job_info->igm_no ?></td>
								<td><?= ($job_info->igm_date != '0000-00-00')?strftime(config_item('date_format'), strtotime($job_info->igm_date)):'' ?></td>
							</tr>
							<tr>
								<td>Index No</td>
								<td colspan="2"><?= $job_info->index_no ?></td>
							</tr>
							<tr>
								<td>Invoice Value </td>
								<td colspan="2"><?= $job_info->currency ?> <?= number_format(($this->invoice_model->get_invoice_value($job_info->invoices_id)),2) ?></td>
							</tr>
							<tr>
								<td>I/E Value</td>
								<td colspan="2"> PKR <?= number_format(($this->invoice_model->get_invoice_value($job_info->invoices_id)*$job_info->exchange_rate),2) ?></td>
							</tr>
						</tbody>
					</table>
                </div>
				<div class="col-xs-12 table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th colspan="4" class="text-center"><h4>Details of Sales Tax Payable</h4></th>
							</tr>
						</thead>
						<tbody>
                            <?php
                            $agency_commission = $bill_info->service_charges;
                                ?>
                            <tr>
                                <td class="text-right"><?= lang('agency_commission') ?></td>
                                <td class="text-center">PKR <b><?= number_format(($agency_commission),2) ?></b>
                                </td>
                            </tr>
                            <tr >
								<td class="text-right" > <?= lang('sales_tax') ?> Amount @ <?= number_format($bill_info->gst,2) ?> % </td >
								<td class="text-center" >PKR <b class="tax">
                                    <?php
                                    $tax_per = $agency_commission / 100 * $bill_info->gst;
                                    echo number_format(($tax_per), 2)
                                    ?></b>
                                </td>
                            </tr>
							<tr>
								<td colspan="3">In Words: <strong>Rupees <strong class="convert_amount"><?= $this->invoice_model->convert_number($tax_per); ?></strong> ONLY</strong></td>
							</tr>
						</tbody>
					</table>
                </div>
            </div>
        </section>
        <section style="display: none;" class="invoice" id="print_invoice_new">
        <?php $this->load->view('admin/bill/pdf_sales_tax_invoice'); ?>
        </section>
    </div>

</section>
<script type="text/javascript">
    function print_invoice(print_invoice_new) {
        var printContents = document.getElementById(print_invoice_new).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        return window.location.reload(true);
        document.body.innerHTML = originalContents;
    }
</script>

