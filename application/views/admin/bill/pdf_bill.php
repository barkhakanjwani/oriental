<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html lang=en>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Invoice Bill</title>
    <style>
        html{/*margin:3px 15px !important;*/}
        body {
            background: white;
            color: black;
            margin: 0px;
        }
        table { width: 100%; }
        @media print {
            table{font-size:15px;}
        }

    </style>
    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
<body style="margin-left:3%; margin-right:3%;">
<div class="col-xs-12">
    <h1 style="font-size:55px;font-family: fantasy;letter-spacing: 3px;text-align: center;"><?= config_item('company_name') ?></h1>
    <p style="font-size:15px;font-family: sans-serif;text-align: center;"><strong>Head Office:</strong> <?= config_item('company_address') ?>, <?= config_item('company_city') ?>, <?= config_item('company_country') ?></p>
    <p style="font-size:15px;font-family: sans-serif;text-align: center;"><strong>Tel:</strong> <?= config_item('company_phone') ?></p>
    <p style="font-size:15px;font-family: sans-serif;text-align: center;"><strong>Email:</strong> <?= config_item('company_email') ?>, <strong>Website:</strong> <?= config_item('company_domain') ?></p>
</div>
<div class="col-xs-12">
    <h2 style="font-size:25px;font-family: initial;text-decoration-line: underline;letter-spacing: 5px;text-align: center;"><strong>EXPENSE DETAIL</strong></h2>
</div>
<div class="col-xs-12 form-group">
    <div class="col-xs-6 text-left">
        <h4 style="text-decoration-line: underline;font-family: serif;"><strong>JOB #</strong> <?= $this->invoice_model->job_no_creation($job_info->invoices_id) ?></h4>
    </div>
    <div class="col-xs-6 text-right">
        <h4 style="text-decoration-line: underline;font-family: serif;"><strong>DATE:</strong> <?= strftime(config_item('date_format'), strtotime($bill_info->delivery_date)) ?></h4>
    </div>
</div>

<div class="col-xs-12">
    <div class="col-xs-12">
        <table class="table" style="border: 2px solid black;">
            <tbody style="border: 2px solid black;">
            <tr style="border: 2px solid black;">
                <td><b>Bill to</b></td>
                <td><b><?= ucfirst($job_info->name) ?></b></td>
                <td><b>Contact Person Details</b></td>
                <td><b><?= ucfirst($job_info->contact_p_name) ?></b></td>
            </tr>
            <tr style="border: 2px solid black;">
                <td><b>Address:</b></td>
                <td colspan="3"> <b><?= $job_info->address ?></b></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-xs-12">
        <table class="table" style="border: 2px solid black;">
            <tbody style="border: 2px solid black;">
            <tr style="border: 2px solid black;">
                <td><b><?= lang('consignee_consignor') ?></b></td>
                <td colspan="5"><b><?= $job_info->consignor_name ?></b></td>
            </tr>
            <tr style="border: 2px solid black;">
                <td><b><?= lang('address') ?></b></td>
                <td colspan="5"><b><?= $job_info->consignor_address ?></b></td>
            </tr>
            <tr style="border: 2px solid black;">
                <td><b>I.G.M / VIR#</b></td>
                <td><b><?= $job_info->igm_no ?></b></td>
                <td><b>Index</b></td>
                <td><b><?= $job_info->index_no ?></b></td>
                <td><b>BL/AWB with Dt:</b></td>
                <td><b><?= $job_info->bl_no ?></b></td>
            </tr>
            <tr style="border: 2px solid black;">
                <td><b>Loading Port</b></td>
                <td><b><?= $job_info->p_o_l ?></b></td>
                <td><b>Discharge Port</b></td>
                <td><b><?= $job_info->p_o_d ?></b></td>
                <td><b>Declared Value:</b></td>
                <td><b><?= number_format($this->invoice_model->get_declared_value($job_info->invoices_id),2) ?></b></td>
            </tr>
            <tr style="border: 2px solid black;">
                <td><b>Forwarder Name:</b></td>
                <td><b><?= $job_info->forwarding_agent ?></b></td>
                <td><b>Assessed Value:</b></td>
                <td><b><?= number_format($this->invoice_model->get_assessable_value($job_info->invoices_id),2) ?></b></td>
                <td><b>Shipping/Air Line</b></td>
                <td><b><?= shipping_line($job_info->shipping_line) ?></b></td>
            </tr>
            <tr style="border: 2px solid black;">

                <td><b>Consignment Type</b></td>
                <td><b><?= $job_info->consignment_type ?></b></td>
                <td><b>Pkges</b></td>
                <td><b><?= $packages ?></b></td>
                <td></td>
                <td></td>
            </tr>
            <tr style="border: 2px solid black;">
                <td><b>Item Detail:</b></td>
                <td><b>
                        <?php
                        $item_info = $this->invoice_model->check_by_all(array('invoices_id'=>$job_info->invoices_id), 'tbl_saved_commodities');
                        if(!empty($item_info)){
                            foreach($item_info as $item){
                                echo $item->commodity.", ";
                            }
                        }
                        ?></b>
                </td>
                <td><b>Net. Wt:</b></td>
                <td><b><?= ($job_info->net_weight == 0)?'':$job_info->net_weight ?></b></td>
                <td><b>Gross Wt.</b></td>
                <td><b><?= ($job_info->gross_weight == 0)?'':$job_info->gross_weight ?></b></td>
            </tr>
            <tr style="border: 2px solid black;">
                <td><b>Container #/Size detail:</b></td>
                <td colspan="5"><b>
                        <?php
                        $container_info = $this->invoice_model->check_by_all(array('invoices_id'=>$job_info->invoices_id), 'tbl_containers');
                        if(!empty($container_info)){
                            foreach($container_info as $container){
                                echo $container->container_no."/".$container->container_ft.", ";
                            }
                        }
                        ?>
                    </b></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-xs-12">
        <table class="table" style="border: 2px solid black;">
            <tbody style="border: 2px solid black;">
                <tr style="border: 2px solid black;">
                    <td colspan="10" class="text-center" ><b>Pay Order</b></td>
                </tr>
                <tr style="border: 2px solid black;">
                    <td><b>Title</b></td>
                    <td class="text-right"><b>Amount</b></td>
                </tr>
            <?php
            foreach ($receipt_voucher_info as $receipt) {
                ?>
                <tr style="border: 2px solid black;">
                    <td><?= $receipt->apd_title; ?></td>
                    <td class="text-right"><?= $receipt->apd_amount; ?></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
    <?php
    /** COST OF SALES **/
    $sub_head_id = $this->invoice_model->check_by(array('HEAD_TYPE'=>'Expense', 'NAME'=>'Cost of Sales'),'sub_heads');
    $account_heads = $this->invoice_model->check_by_all(array('SUB_HEAD_ID'=>$sub_head_id->SUB_HEAD_ID),'accounts_head');
    foreach($account_heads as $head) {
    $accounts = $this->invoice_model->check_by_all(array('CLIENT_ID'=>$job_info->client_id, 'H_ID'=>$head->H_ID),'accounts');
    ?>
    <div class="col-xs-12">
        <table class="table" style="border: 2px solid black;">
            <thead style="border: 2px solid black;">
                <th colspan="6">DESCRIPTION</th>
                <th class="text-right">AMOUNT</th>
            </thead>
            <tbody style="border:2px solid black;">
            <?php
            $total = 0;
            $count = 1;
            foreach($job_expenses as $job_expense) {
                $account = $this->invoice_model->check_by(array('bill_id' => $bill_info->bill_id, 'job_expense_id'=>$job_expense->job_expense_id), 'tbl_bill_details');
                if (!empty($account)) {
                    $value = $this->invoice_model->job_expenses_by_invoice_account($job_info->invoices_id,$job_expense->job_expense_id)->amount;
                    $total += $account->bill_amount;
                    if($account->bill_amount == 0) {

                    }
                    else{
                        ?>
                        <tr style="border: 2px solid black;">
                            <td colspan="6"><?= job_expense_title($account->job_expense_id) ?>&nbsp;&nbsp;
                                <?php
                                if(job_expense_title($account->job_expense_id) == 'Local Transport of Container / Cargo From' || job_expense_title($account->job_expense_id) == 'Transportation of Container / Cargo From') {
                                    ?>
                                    <b style="text-decoration: underline"><?= $account->bill_from ?></b>
                                    <span class="pull-right">Destination&nbsp;&nbsp; <b
                                                style="text-decoration: underline"><?= $account->bill_to ?></b></span>
                                    <?php
                                }
                                ?>
                            </td>
                            <td class="text-center">PKR <?= number_format(($account->bill_amount), 2) ?></td>
                        </tr>
                        <?php
                    }
                }
                $count++;
            }
            $commission_heads = $this->invoice_model->check_by(array('H_TYPE'=>'Income', 'H_NAME'=>'Commission'),'accounts_head');
            $commissions = $this->invoice_model->check_by(array('CLIENT_ID'=>$job_info->client_id, 'H_ID'=>$commission_heads->H_ID),'accounts');
            $commission = $this->invoice_model->check_by(array('A_ID'=>$commissions->A_ID, 'T_ID'=>$bill_info->transaction_id),'transactions_meta');
            $st_heads = $this->invoice_model->check_by(array('H_TYPE'=>'Liabilities', 'H_NAME'=>'Payable Tax'),'accounts_head');
            $sts = $this->invoice_model->check_by(array('H_ID'=>$st_heads->H_ID,'A_NAME'=>'Sales Tax'),'accounts');
            $st = $this->invoice_model->check_by(array('A_ID'=>$sts->A_ID, 'T_ID'=>$bill_info->transaction_id),'transactions_meta');
            $received_advances = $bill_info->current_advance;
            $total_bill = $bill_info->total_bill;
            $balance_dues = $received_advances - $total_bill;

            ?>
            <tr style="border: 2px solid black;">
                <td colspan="6">Service Charges</td>
                <td class="text-right">PKR <?= number_format(($commission->TM_AMOUNT),2) ?></td>
            </tr>
            <tr style="border: 2px solid black;">
                <td colspan="6">Sales Tax on Custom Clearing Services Rs: <?= number_format(($commission->TM_AMOUNT),2) ?> Services @ 13 %</td>
                <td class="text-right">PKR <?= number_format(($st->TM_AMOUNT), 2) ?></td>
            </tr>
            <tr style="border: 2px solid black;">
                <td colspan="6"><b>Sub Total</b></td>
                <td class="text-right"><b>PKR <?= number_format(($total),2) ?></b></td>
            </tr>
            <tr style="border: 2px solid black;">
                <td colspan="6"><b>Total Bill Amount</b></td>
                <td class="text-right"><b>PKR <?= number_format(($total_bill),2) ?></b></td>
            </tr>
            <tr style="border: 2px solid black;">
                <td colspan="6"><b>Advances</b></td>
                <td class="text-right"><b>PKR <?= ($received_advances > 0)?number_format($received_advances-$deposit_amount,2):'0.00' ?></b></td>
            </tr>
            <tr style="border: 2px solid black;">
                <?php if($received_advances > 0){ ?>
                    <td colspan="6"><b>Advances</b></td>
                    <td class="text-right"><b>Bill is adjusted from Advances</b></td>
                <?php }else{?>
                    <td colspan="6"><b>Outstanding Balance</b></td>
                    <td class="text-right"><b>PKR <?= number_format(($received_advances),2) ?></b></td>
                <?php }?>
            </tr>
            </tbody>
        </table>

        <?php
        }
        ?>
    </div>

    <div class="col-xs-12">
        <p><strong>Remarks:</strong>________________________________________________________________________________________<strong>Signature</strong>___________________________________________________________________________</p>
        </br>
        <p>____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________</p>
    </div>
    <!--<div class="col-xs-12">
        <h4 style="letter-spacing: 2px;"><strong>COUNTRY-WIDE OFFICES:</strong> <b>RAWALPINDI - KARACHI - SIALKOT - LAHORE - FAISALABAD - PESHAWAR</b></h4>
    </div>-->
    <div class="col-xs-12" style="margin-top:20px;">
        <p class="pull-right"><?= config_item('default_terms') ?></p>
    </div>

</div>
</body>
</html>
