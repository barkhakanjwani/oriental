
<section class="content-header">
    <?php
    $client_info = $this->invoice_model->check_by(array('client_id' => $shipping_info->client_id), 'tbl_client');
    ?>
    <div class="row">
        <div class="col-sm-8">

        </div>
        <div class="col-sm-4 pull-right">
            <a onclick="print_invoice('print_invoice_new')" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Print" class="btn btn-sm btn-danger pull-right"  >
                <i class="fa fa-print"></i>
            </a>
           <!-- <a style="margin-right: 5px" href="<?/*= base_url() */?>admin/security_deposit/manage_deposit/pdf_deposit/<?/*= $deposit_info->invoices_id */?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="PDF" class="btn btn-sm btn-warning pull-right" >
                <i class="fa fa-file-pdf-o"></i>
            </a>-->
        </div>
    </div>
</section>
<section class="content">
    <!-- Start Display Details -->
    <!-- Main content -->
    <div class="row" >
        <section class="invoice" id="print_invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <img style="width: 60px;width: 60px;margin-top: -10px;margin-right: 10px;" src="<?= base_url() . config_item('invoice_logo') ?>" ><?= config_item('company_name') ?>
                    </h2>
                </div><!-- /.col -->
            </div>
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th colspan="4">Description Of Shipping</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><b><?= lang('job_no') ?></b></td>
                            <td><?= $this->invoice_model->job_no_creation($shipping_info->invoices_id) ?></td>
                            <td><b><?= lang('client') ?></b></td>
                            <td><?= ucfirst($client_info->name) ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('shipping_company') ?></b></td>
                            <td><?= ucfirst($shipping_info->shipping_company) ?></td>
                            <td><b><?= lang('container') ?></b></td>
                            <td><?= ucfirst($shipping_info->number_container) ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('rent_per_day') ?></b></td>
                            <td><?= ucfirst($shipping_info->rent_per_day) ?></td>
                            <td><b><?= lang('totals') ?></b></td>
                            <td><?= ucfirst($shipping_info->totals) ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('exchange_rates') ?></b></td>
                            <td><?= ucfirst($shipping_info->exchange_rates) ?></td>
                            <td><b><?= lang('per_day_rent_total') ?></b></td>
                            <td><?= ucfirst($shipping_info->per_day_rent_total) ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('free_day') ?></b></td>
                            <td><?= ucfirst($shipping_info->free_day) ?></td>
                            <td><b><?= lang('igm_dates') ?></b></td>
                            <td><?= strftime(config_item('date_format'), strtotime($shipping_info->igm_dates)) ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('shipping_rent_date') ?></b></td>
                            <td><?= strftime(config_item('date_format'), strtotime($shipping_info->shipping_rent_date)) ?></td>
                            <td><b><?= lang('total_days') ?></b></td>
                            <td><?= ucfirst($shipping_info->total_days) ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('total_rent_days') ?></b></td>
                            <td><?= ucfirst($shipping_info->per_day_rent_total) ?></td>
                            <td><b><?= lang('total_in_usd') ?></b></td>
                            <td><?= ucfirst($shipping_info->total_in_usd) ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('total_in_pkr') ?></b></td>
                            <td><?= ucfirst($shipping_info->total_in_pkr) ?></td>
                            <td><b><?= lang('er_date') ?></b></td>
                            <td><?= strftime(config_item('date_format'), strtotime($shipping_info->er_date)) ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('additional_rent_days') ?></b></td>
                            <td><?= ucfirst($shipping_info->additional_rent_days) ?></td>
                            <td><b><?= lang('additional_rent_in_usd') ?></b></td>
                            <td><?= ucfirst($shipping_info->additional_rent_in_usd) ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('total_amount_in_pkr') ?></b></td>
                            <td><?= ucfirst($shipping_info->total_amount_in_pkr) ?></td>
                            <td><b></b></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </section>
        <section style="display: none;" class="invoice" id="print_invoice_new">
            <?php $this->load->view('admin/security_deposit/pdf_shipping'); ?>
        </section>
    </div>

</section>
<script type="text/javascript">
    function print_invoice(print_invoice_new) {
        var printContents = document.getElementById(print_invoice_new).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>