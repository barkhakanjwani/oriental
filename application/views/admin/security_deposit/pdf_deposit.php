<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">

<html lang=en>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <title><?= lang('requisition') ?></title>

    <style>

        html{/*margin:3px 15px !important;*/}

        body {

            background: white;

            color: black;

            margin: 0px;

        }





        #h11 {

            color: #000;

            background: none;

            font-weight: bold;

            font-size: 2.5em;

            margin: 10px 0 0 0;

            text-align: center;

            font-size:32px;

        }

        #h12

        {

            color: #000;

            background: none;

            font-weight: bold;

            font-size: 2em;

            margin: 10px 0 0 0;

            text-align: center;

            font-size:25px;

        }



        h2 {

            color: #00008b;

            background: none;

            font-weight: bold

        }



        h3 {

            color: #000;

            background: none;

            margin-left: 4%;

            margin-right: 4%;

            font-weight: bold;

            margin: 10px 0 0 0;

            text-align: center;

        }



        h4 {

            margin-left: 6%;

            margin-right: 6%;

            font-weight: bold

        }



        h5 {

            margin-left: 6%;

            margin-right: 6%;

            font-weight: bold

        }



        ul, ol, dl, p {

            margin-left: 6%;

            margin-right: 6%

        }



        ul ul, table ol, table ul, dl ol, dl ul {

            margin-left: 1.2em;

            margin-right: 1%;

            padding-left: 0

        }



        pre {

            margin-left: 10%;

            white-space: pre

        }



        /* table caption {

          font-size: larger;

          font-weight: bolder

        } */



        /* table p, table dl, ol p, ul p, dl p, blockquote p, .note p, .note ul, .note ol, .note dl, li pre, dd pre {

          margin-left: 2%;

          margin-right: 2%;

        } */

        td

        {

            align: right;

        }



        p.top {

            margin-left: 1%;

            margin-right: 1%

        }



        blockquote {

            margin-left: 8%;

            margin-right: 8%;

            border: thin ridge #dc143c

        }



        blockquote pre {

            margin-left: 1%;

            margin-right: 1%

        }





        .css {

            color: #800000;

            background: none

        }



        .javascript {

            color: #008000;

            background: none

        }



        .example { margin-left: 10% }



        dfn {

            font-style: normal;

            font-weight: bolder

        }



        var sub { font-style: normal }



        .note {

            font-size: 85%;

            margin-left: 10%

        }



        .SMA {

            color: fuchsia;

            background: none;

            font-family: Kids, "Comic Sans MS", Jester

        }



        .oops {

            font-family: Jester, "Comic Sans MS"

        }



        .author {

            font-style: italic

        }



        .copyright {

            font-size: smaller;

            text-align: right;

            clear: right

        }



        .toolbar {

            text-align: center

        }



        .toolbar IMG {

            float: right

        }



        .error {

            color: #DC143C;

            background: none;

            text-decoration: none

        }



        .warning {

            color: #FF4500;

            background: none;

            text-decoration: none

        }



        .error strong {

            color: #DC143C;

            background: #FFD700;

            text-decoration: none

        }



        .warning strong {

            color: #FF4500;

            background: #FFD700;

            text-decoration: none

        }





        colgroup.entity { text-align: center }



        .default { text-decoration: underline; font-style: normal }

        .required { font-weight: bold }

        td li.transitional, .elements li.transitional {

            font-weight: lighter;

            color: #696969;

            background: none

        }

        td li.frameset, .elements li.frameset {

            font-weight: lighter;

            color: #808080;

            background: none

        }



        .footer, .checkedDocument {



            width:93%;

            margin-left:3%;

            border-top: solid thin black

        }



        strong.legal {

            font-weight: normal;

            text-transform: uppercase

        }



        @media print {

            input#toggler, .toolbar, { display: none; }

            html, body {

                height:100vh;

                margin: 0 !important;

                padding: 0 !important;

                overflow: hidden;

            }

            td {

                height:1px;

                font-size:12px;

                padding:3px;

                font-family: 'bitstream cyberbit', sans-serif;

            }

            th{

                text-align:center;

                padding:5px;

            }

        }



        table { width: 100%; }



    </style>

    <meta name="author" content="">

    <meta name="description" content="">

    <meta name="keywords" content="">

<body style="margin-left:3%; margin-right:3%;">

<?php

    $client_info = $this->invoice_model->check_by(array('client_id' => $deposit_info->client_id), 'tbl_client');

?>

<h1 id="h11"><?= config_item('company_name') ?></h1><br>



<p style="text-align: center;">

    <?php

    if ($client_info->client_status == 1) {

        $status = 'Person';

    } else {

        $status = 'Company';

    }

    ?>

    <span style="font-size:11px;">

    <?= config_item('company_address') ?>

        <?= config_item('company_city') ?>, <?= config_item('company_country') ?> <br />

        Phone: <?= config_item('company_phone') ?><br />

        Email: <?= config_item('company_email') ?><br />

        Web: <?= config_item('company_domain') ?><br />

        <span>

</p>

<div class="footer"></div>

<br>

<p>To Messer's: <b><?= ucfirst($client_info->name) ?></b></p>

<p>Address: <b><?= ucfirst($client_info->address) ?></b></p>

<div style="margin-left:3%; margin-right:3%;">

    <div class="col-xs-12">

    <table class="table-bordered">

        <!--<thead>

            <tr>

                <th colspan="4">Invoice Info</th>

            </tr>

        </thead>-->

        <tbody>

        <tr>

            <th colspan="4">Description Of Consignment</th>

        </tr>

        </thead>

        <tbody>

        <tr>

                            <th colspan="4">Description Of Consignment</th>

                        </tr>
	
		</thead>
	
		<tbody>

                        <tr>

                            <td><b><?= lang('reference_no') ?></b></td>

                            <td><?= $this->invoice_model->job_no_creation($deposit_info->invoices_id) ?></td>

                            <td><b><?= lang('client') ?></b></td>

                            <td class="text-right"><?= $client_info->name ?></td>

                        </tr>

                        <tr>

                            <!--<td><b><?/*= lang('vessel') */?></b></td>

                            <td><?/*= $deposit_info->vessel */?></td>-->

                            <td><b><?= lang('shipping_line') ?></b></td>

                            <td class="text-right"><?= read_subHead($deposit_info->shipping_account)->A_NAME ?></td>

                        </tr>



                        <tr>

                            <td><b><?= lang('container') ?></b></td>

                            <td><?= $deposit_info->container_no ?></td>

                            <td><b><?= lang('bl_no') ?></b></td>

                            <td><?= $deposit_info->bl_no ?></td>

                        </tr>

                        <tr>
                            <td><b><?= lang('arrival') ?></b></td>

                            <td><?= $deposit_info->arrival ?></td>
                            
                            <td><b><?= lang('amount_deposit') ?></b></td>

                            <td class="text-right"><?= number_format(($deposit_info->deposit_amount),2) ?></td>

                        </tr>

                        <tr>

                            <td><b><?= lang('deposit_date') ?></b></td>

                            <td><?= strftime(config_item('date_format'), strtotime($deposit_info->created_date)) ?></td>

                            <td><b><?= lang('delivery_date_shipment') ?></b></td>

                            <td class="text-right"><?= strftime(config_item('date_format'), strtotime($deposit_info->delivery_date_shipment)) ?></td>

                        </tr>

                        <tr>

                            <td><b><?= lang('advance_rent') ?></b></td>

                            <td><?= number_format(($deposit_info->advance_rent),2) ?></td>

                            <td><b><?= lang('remaining_rent') ?></b></td>

                            <td class="text-right"><?= number_format(($deposit_info->remaining_rent),2) ?></td>

                        </tr>

                        <tr>

                            <td><b><?= lang('final_rent') ?></b></td>

                            <td><?= number_format(($deposit_info->final_rent),2) ?></td>

                            <td><b><?= lang('refund_amount') ?></b></td>

                            <td class="text-right"><?= number_format(($deposit_info->refund_amount),2)  ?></td>

                        </tr>

                        <tr>

                            <td><b><?= lang('expected_date_of_refund') ?></b></td>

                            <td><?= strftime(config_item('date_format'), strtotime($deposit_info->expected_date_of_refund)) ?></td>

                            <td><b><?= lang('deposit_return') ?></b></td>

                            <td class="text-right"><?= $deposit_info->deposit_return ?></td>

                        </tr>

                        <?php
    
                            if($deposit_info->deposit_return == 'Yes') {
        
                                ?>
		
								<tr>

                                    <td><b><?= lang('return_amount') ?></b></td>

                                    <td><?= $deposit_info->deposit_return_amount; ?></td>

                                    <td><b><?= lang('return_date') ?></b></td>

                                    <td class="text-right"><?= strftime(config_item('date_format'), strtotime($deposit_info->return_date_container)) ?></td>

                                </tr>
        
                                <?php
        
                            }

                        ?>

        </tbody>

    </table>

    </div>

</div>



</body>

</html>

