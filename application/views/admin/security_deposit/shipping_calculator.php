<div class="row">
    <div class="col-lg-12">
        <form role="form" enctype="multipart/form-data" id="form_v" class="form-horizontal" method="post" action="<?php echo base_url(); ?>admin/security_deposit/save_shipping/<?= (!empty($shipping_info))?$shipping_info->sc_id:'' ?>">
            <section class="panel panel-default">
                <header class="panel-heading"><?= lang('shipping_calculator') ?></header>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?= lang('client') ?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <select name="client_id" class="form-control select_box" onchange="selectJobno(this.value)" style="width: 100%" required>
                                <option value="">Choose Client</option>
                                <?php if(!empty($all_clients)) {
                                    foreach ($all_clients as $client) { ?>
                                        <option value="<?php echo $client->client_id; ?>" <?php
                                        if (!empty($shipping_info)) {
                                            echo ($shipping_info->client_id == $client->client_id) ? 'selected' : '';
                                        } ?>><?php echo $client->name; ?></option>
                                    <?php }
                                }?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="job_no">
                        <label class="col-lg-3 control-label"><?= lang('reference_no') ?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <select name="invoices_id" id="invoices_id" class="form-control select_box" onchange="getJobsDetail(this.value)" style="width: 100%" required>
                                <option value="">Choose Job</option>
                                <?php if (!empty($shipping_info)){
                                    $all_jobs = $this->invoice_model->check_by_all(array('client_id'=>$shipping_info->client_id), 'tbl_invoices');
                                    foreach ($all_jobs as $inv){?>
                                        <option value="<?php $inv->invoices_id ?>"<?= ($shipping_info->invoices_id == $inv->invoices_id) ? 'selected' : ''; ?>><?php echo $this->invoice_model->job_no_creation($inv->invoices_id);?></option>
                                    <?php }
                                    }?>
                            </select>
                        </div>
                    </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label"><?= lang('shipping_company') ?>:</label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" id="shipping_company" name="shipping_company" placeholder="Shipping Company" readonly value="<?= (!empty($shipping_info))?$shipping_info->shipping_company:'' ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"><?= lang('number_container') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control calculator" id="number_container" name="number_container" placeholder="Enter Container No" autocomplete="off" value="<?= (!empty($shipping_info))?$shipping_info->number_container:'' ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"><?= lang('rent_per_day') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control calculator" id="rent_per_day" name="rent_per_day" value="<?= (!empty($shipping_info))?$shipping_info->rent_per_day:'' ?>"
                                       placeholder="Enter Rent Per Day" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"><?= lang('totals') ?>:</label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" id="totals" name="totals" placeholder="Total" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"><?= lang('exchange_rates') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control calculator" id="exchange_rates" name="exchange_rates" value="<?= (!empty($shipping_info))?$shipping_info->exchange_rates:'' ?>"
                                       placeholder="Enter Exchange Rate" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"><?= lang('per_day_rent_total') ?></label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" id="per_day_rent_total"
                                       name="per_day_rent_total" placeholder="Enter Per Day Rent Total PKR" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"><?= lang('free_day') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control calculator" id="free_day" name="free_day" value="<?= (!empty($shipping_info))?$shipping_info->free_day:'' ?>"
                                       placeholder="Enter Free Days" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"><?= lang('igm_dates') ?></label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" id="igm_dates" name="igm_dates" placeholder="Enter IGM No" readonly value="<?php
                                if(!empty($shipping_info)){
                                    echo ($shipping_info->igm_dates != '0000-00-00')?$shipping_info->igm_dates:'';
                                } ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"><?= lang('shipping_rent_date') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control datepicker calculator" id="shipping_rent_date" name="shipping_rent_date" placeholder="yyyy-mm-dd" autocomplete="off" value="<?php
                                if(!empty($shipping_info)){
                                    echo ($shipping_info->shipping_rent_date != '0000-00-00')?$shipping_info->shipping_rent_date:'';
                                } ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"><?= lang('total_days') ?></label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" id="total_days" name="total_days" placeholder="Enter Total Days" readonly>
                            </div>
                        </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?= lang('total_rent_days') ?></label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control" id="total_rent_days" name="total_rent_days" placeholder="Total Days" readonly>
                        </div>
                    </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"><?= lang('total_in_usd') ?></label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" id="total_in_usd" name="total_in_usd" placeholder="Enter Total in USD" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"><?= lang('total_in_pkr') ?></label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" id="total_in_pkr" name="total_in_pkr" placeholder="Total in PKR"  readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"><?= lang('er_date') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control datepicker calculator" id="er_date" name="er_date" autocomplete="off" value="<?php
                                if(!empty($shipping_info)){
                                    echo ($shipping_info->er_date != '0000-00-00')?$shipping_info->er_date:'';
                                } ?>" placeholder="Y-m-d" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"><?= lang('additional_rent_days') ?>:</label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control calculator" id="additional_rent_days"
                                       name="additional_rent_days" placeholder="Additional Rent Days" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"><?= lang('additional_rent_in_usd') ?>:</label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control calculator" id="additional_rent_in_usd"
                                       name="additional_rent_in_usd" placeholder="Additional Rent in USD" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"><?= lang('total_amount_in_pkr') ?>:</label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control calculator" id="total_amount_in_pkr"
                                       name="total_amount_in_pkr" placeholder="Total Amount in PKR" readonly>
                            </div>
                        </div>
                        <label class="col-lg-3 control-label"></label>
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i>
                                <?php
                                if(!empty($shipping_info)){
                                    echo lang('update_shipping');
                                }else{
                                    echo lang('create_shipping');
                                }
                                ?>
                            </button>
                        </div>
                </div>
            </section>
        </form>
    </div>
</div>
<script type="text/javascript">
    function selectJobno(client_id) {
        var option="";
        var option_empty = '<option value="" selected>Choose Job</option>';
        $('#shipping_company').val('');
        $('#igm_dates').val('');
        if(client_id == '') {
            $('#invoices_id').html(option_empty).hide().fadeIn(500);
            $('.select_box').select2({});
        }
        else{
            $.getJSON("<?php echo site_url('admin/security_deposit/ajax_get_jobs_by_client/') ?>" + "/" + client_id, function (result) {
                $.each(result, function (index, value) {
                    var date= new Date(value.created_date);
                    var year = date.getFullYear();
                    var type;
                    var sep;
                    if(value.type == 'import') {
                        type = '<?= config_item('invoice_prefix') ?>';
                        sep = '<?= config_item('invoice_number_separator') ?>';
                    }
                    else {
                        type = '<?= config_item('invoice_prefix_export') ?>';
                        sep = '<?= config_item('invoice_number_separator_export') ?>';
                    }
                    option = ('<option value="' + value.invoices_id + '">' +type+  sep + value.reference_no +'</option>')+option;
                });
                $('#invoices_id').html(option_empty+option).hide().fadeIn(500);
                $('.select_box').select2({});
            });
        }
    }

    function getJobsDetail(invoice_id) {
        if(invoice_id == '') {
            $('#shipping_company').val('');
            $('#igm_dates').val('');
        }else{
            $.getJSON("<?php echo site_url('admin/security_deposit/ajax_get_invoice_detail') ?>/"+invoice_id, function (result) {
                    $('#shipping_company').val(result.shipping_name);
                    $('#igm_dates').val(result.igm_date);
            });
        }
    }

</script>
<script type="text/javascript">
	$(document).ready(function() {
        multiplication1();
        multiplication2();
        multiplication5();
        multiplication6();
        multiplication7();
        multiplication8();
        multiplication9();
        dates_difference();
        date_diff();
    });
    function multiplication1(){
        var number_container = $('#number_container').val() == '' ? 0 : parseFloat($('#number_container').val());
        var rent_per_day = $('#rent_per_day').val() == '' ? 0 : parseFloat($('#rent_per_day').val());
        var result = number_container*rent_per_day;
		result = (result > 0)?result:"";
        $('#totals').val(result);
    }

    function multiplication2(){
        var totals = $('#totals').val() == '' ? 0 : parseFloat($('#totals').val());
        var exchange_rates = $('#exchange_rates').val() == '' ? 0 : parseFloat($('#exchange_rates').val());
        var result = totals*exchange_rates;
        result = (result > 0)?result:"";
        $('#per_day_rent_total').val(result);
    }

    function multiplication5(){
        var free_day1 = $('#free_day').val() == '' ? 0 : parseFloat($('#free_day').val());
        var total_days1 = $('#total_days').val() == '' ? 0 : parseFloat($('#total_days').val());
        var result = total_days1 - free_day1;
        result = (result > 0)?result:"";
        $('#total_rent_days').val(result);
    }
    function multiplication6(){
        var totals = $('#totals').val() == '' ? 0 : parseFloat($('#totals').val());
        var total_rent_days = $('#total_rent_days').val() == '' ? 0 : parseFloat($('#total_rent_days').val());
        var result = totals*total_rent_days;
        result = (result > 0)?result:"";
        $('#total_in_usd').val(result);

    }
    function multiplication7(){
        var exchange_rates = $('#exchange_rates').val() == '' ? 0 : parseFloat($('#exchange_rates').val());
        var total_in_usd = $('#total_in_usd').val() == '' ? 0 : parseFloat($('#total_in_usd').val());
        var result = exchange_rates*total_in_usd;
        result = (result > 0)?result:"";
        $('#total_in_pkr').val(result);

    }
    function multiplication8(){
        var rent_per_day = $('#rent_per_day').val() == '' ? 0 : parseFloat($('#rent_per_day').val());
        var additional_rent_days = $('#additional_rent_days').val() == '' ? 0 : parseFloat($('#additional_rent_days').val());
        var result = rent_per_day*additional_rent_days;
        result = (result > 0)?result:"";
        $('#additional_rent_in_usd').val(result);

    }
    function multiplication9(){
        var exchange_rates = $('#exchange_rates').val() == '' ? 0 : parseFloat($('#exchange_rates').val());
        var additional_rent_in_usd = $('#additional_rent_in_usd').val() == '' ? 0 : parseFloat($('#additional_rent_in_usd').val());
        var result = exchange_rates*additional_rent_in_usd;
        result = (result > 0)?result:"";
        $('#total_amount_in_pkr').val(result);

    }

    function dates_difference(){
        var dateFirst = new Date(document.getElementById("igm_dates").value);
        var dateSecond = new Date(document.getElementById("shipping_rent_date").value);
        console.log(dateSecond);
		if(dateSecond == 'Invalid Date'){
		    return false;
		}
        else {
            var timeDiff = Math.abs(dateSecond.getTime() - dateFirst.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            document.getElementById("total_days").value = diffDays;
        }
    }
    function date_diff(){
        var dateFirst = new Date(document.getElementById("shipping_rent_date").value);
        var dateSecond = new Date(document.getElementById("er_date").value);

        // time difference
        var timeDiff = Math.abs(dateSecond.getTime() - dateFirst.getTime());

        // days difference
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if(dateSecond == 'Invalid Date'){
            return false;
        }
        else {
            document.getElementById("additional_rent_days").value = diffDays;
        }
        /*$('#n12').val(result);*/

    }

    $('.calculator').keyup(function() {
        multiplication1();
        multiplication2();
        multiplication5();
        multiplication6();
        multiplication7();
        multiplication8();
        multiplication9();
        dates_difference();
        date_diff();
    });
    $('.calculator').change(function() {
        multiplication1();
        multiplication2();
        multiplication5();
        multiplication6();
        multiplication7();
        multiplication8();
        multiplication9();
        dates_difference();
        date_diff();
    });
</script>