<link href="<?php echo base_url() ?>asset/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?php echo base_url() ?>asset/js/bootstrap-toggle.min.js"></script>
<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
	<!--<div class="row">
	<div class="col-lg-12">
        <form role="form" enctype="multipart/form-data" id="form" action="<?php /*echo base_url(); */?>admin/security_deposit/search_deposit" method="post" class="form-horizontal  ">
<h1 class="header-<?/*= config_item('sidebar_theme'); */?>"><?/*= lang('search_job_no') */?></h1>
            <section class="panel panel-default">
                <header class="panel-heading  "><?/*= lang('search_job_no') */?></header>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?/*= lang('search_by') */?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <select class="form-control select_box" style="width: 100%" id="search_by"  required>
                                <option value=""><?/*= lang('search_by'); */?></option>
                                <option value="job_no" <?/*= (isset($reference_no))?'selected':'' */?>><?/*= lang('reference_no'); */?></option>
                                <option value="client" <?/*= (isset($client_id))?'selected':'' */?>><?/*= lang('client'); */?></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="<?/*= (isset($client_id))?'display:block;':'display:none;' */?>" id="client">
                        <label class="col-lg-3 control-label"><?/*= lang('client') */?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <select class="form-control select_box client" style="width: 100%"  name="client_id" required <?/*= (isset($client_id))?'':'disabled' */?>>
                                <option value="">Choose Client</option>
                                <?php
/*                                if (!empty($all_client)) {
                                    foreach ($all_client as $v_client) {
                                        */?>
                                        <option value="<?/*= $v_client->client_id */?>" <?php
/*                                        if(isset($client_id)){
                                            echo ($v_client->client_id == $client_id)?'selected':'';
                                        }  */?>><?/*= $v_client->name */?></option>
                                        <?php
/*                                    }
                                }
                                */?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="<?/*= (isset($reference_no))?'display:block;':'display:none;' */?>" id="job_no">
                        <label class="col-lg-3 control-label"><?/*= lang('reference_no') */?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control job_no" name="reference_no" placeholder="<?/*= lang('reference_no') */?>" value="<?/*= (isset($reference_no))?$reference_no:'' */?>" <?/*= (isset($reference_no))?'':'disabled' */?> required autocomplete="off" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"></label>
                        <div class="col-lg-3">
                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-search"></i> <?/*= lang('search') */?></button>
                        </div>
                    </div>
                </div>
            </section>
        </form>
		</div>
	</div>-->
	<?php
/*		if(isset($reference_no) || isset($client_id)){

	*/?>

	<!--<div class="row">
		<div class="col-lg-12">
<h1 class="header-<?/*= config_item('sidebar_theme'); */?>"><?/*= lang('deposit_list') */?></h1>
			<section class="panel panel-default">
                <header class="panel-heading  "><?/*= lang('manage_deposit') */?></header>
                <div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped DataTables " id="DataTables">
                    <thead>
                        <tr>
                            <th><?/*= lang('job_no') */?></th>
                            <th><?/*= lang('deposit_date') */?></th>
                            <th><?/*= lang('client') */?></th>
                            <th><?/*= lang('representative') */?></th>
                            <th><?/*= lang('container') */?></th>
                            <th><?/*= lang('amount_deposit') */?></th>
                            <th class="col-options no-sort" ><?/*= lang('action') */?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
/*                            $counter = 1;
							if (!empty($deposit_info)) {
								foreach ($deposit_info as $v_deposit) {
								    $client_info = $this->invoice_model->check_by(array('client_id'=>$v_deposit->client_id), 'tbl_client');
									*/?>
                                    <tr>
                                        <td><a class="text-info" href="<?/*= base_url() */?>admin/invoice/manage_invoice/invoice_details/<?/*= $v_deposit->invoices_id */?>"><?/*= $v_deposit->reference_no */?></a></td>
                                        <td><?/*= strftime(config_item('date_format'), strtotime($v_deposit->created_date)) */?></td>
                                        <td><?/*= ucfirst($client_info->name) */?></td>
                                        <td><?/*= $v_deposit->name_representative */?></td>
                                        <td><?/*= $v_deposit->container_no */?></td>
                                        <td><?/*= number_format(($v_deposit->deposit_amount),2) */?></td>
                                        <td>
                                            <?/*= btn_view('admin/security_deposit/manage_deposit/deposit_details/' . $v_deposit->deposit_id) */?>
                                            <?/*= btn_edit('admin/security_deposit/manage_deposit/create_deposit/' . $v_deposit->deposit_id) */?>
                                        </td>
                                    </tr>
									<?php
/*                                    $counter++;
								}
							}
							*/?>
                    </tbody>
                </table>
			</div>
		</section>
	</div>
	</div>-->
	<?php
/*		}
	*/?>
    <script>
        /*$(".client").attr("disabled", true);
        $(".job_no").attr("disabled", true);*/
        /*$('#search_by').on("change", function() {
            var type = $(this).val();
            if(type == 'client'){
                $('#client').show();
                $('#job_no').hide();
                $(".client").removeAttr("disabled");
                $(".job_no").attr("disabled","disabled");
            }
            else if (type == 'job_no'){
                $('#job_no').show();
                $('#client').hide();
                $(".job_no").removeAttr("disabled");
                $(".client").attr("disabled","disabled");
            }
            else{
                $('#client').hide();
                $('#job_no').hide();
                $(".client").removeAttr("disabled");
                $(".job_no").removeAttr("disabled");
            }
        });*/
    </script>
<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('search_job_no') ?></h1>
<?php echo form_open(base_url('admin/security_deposit/search_deposit'),array('class'=>"form-horizontal")); ?>

<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-default">
            <header class="panel-heading  "><?= lang('search_by'); ?></header>
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-xs-2">
                        <select class="form-control select_box client" style="width: 100%"  name="client_id">
                            <option value="">Choose Client</option>
                            <?php
                            if (!empty($all_client)) {
                                foreach ($all_client as $v_client) {
                                    ?>
                                    <option value="<?= $v_client->client_id ?>" <?php
                                    if(isset($client_id)){
                                        echo ($v_client->client_id == $client_id)?'selected':'';
                                    }  ?>><?= $v_client->name ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-xs-2">
                        <input type="text" class="form-control" name="reference_no" placeholder="<?= lang('reference_no') ?>" value="<?= (!empty($reference_no))?$reference_no:'' ?>" />
                    </div>
                    <div class="col-xs-2">
                        <input type="text" class="form-control" name="bl_no" placeholder="<?= lang('bl_no') ?>" value="<?= (!empty($bl_no))?$bl_no:'' ?>" />
                    </div>
                    <div class="col-xs-2">
                        <input type="text" class="form-control" name="commodity" placeholder="<?= lang('commodity') ?>" value="<?= (!empty($commodity))?$commodity:'' ?>" />
                    </div>
                    <div class="col-xs-2">
                        <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> <?= lang('search') ?></button>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php echo form_close(); ?>
<input type="hidden" id="client_id" value="<?= (!empty($client_id))?$client_id:'All' ?>">
<input type="hidden" id="reference_no" value="<?= (!empty($reference_no))?$reference_no:'All' ?>">
<input type="hidden" id="bl_no" value="<?= (!empty($bl_no))?$bl_no:'All' ?>">
<input type="hidden" id="commodity" value="<?= (!empty($commodities))?$commodities:'All' ?>">
<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('deposit_list') ?></h1>
<section class="panel panel-default">
    <header class="panel-heading"><?= lang('deposit_list') ?></header>
    <div class="panel-body" id="export_table">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered data-table-list">
                        <thead>
                        <th><?= lang('job_no') ?></th>
                        <th><?= lang('deposit_date') ?></th>
                        <th><?= lang('client') ?></th>
                        <th><?= lang('representative') ?></th>
                        <th><?= lang('container') ?></th>
                        <th><?= lang('amount_deposit') ?></th>
                        <th class="col-options no-sort" ><?= lang('action') ?></th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    var client_id=$('#client_id').val();
    var reference_no=$('#reference_no').val();
    var bl_no=$('#bl_no').val();
    var commodity=$('#commodity').val();
    var data_url='<?= base_url('admin/security_deposit/search_deposits/') ?>'+client_id+'/'+reference_no+'/'+bl_no+'/'+commodity;
</script>

