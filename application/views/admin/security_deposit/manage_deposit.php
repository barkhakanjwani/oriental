<link href="<?php echo base_url() ?>asset/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?php echo base_url() ?>asset/js/bootstrap-toggle.min.js"></script>
<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('create_deposit') ?></h1>
	<div class="row">
		<div class="col-lg-12">
            <form role="form" enctype="multipart/form-data" id="form" action="<?php echo base_url(); ?>admin/security_deposit/save_deposit/<?= (!empty($deposit_info))?$deposit_info->deposit_id:'' ?>" method="post" class="form-horizontal  ">
                <section class="panel panel-default">
                    <header class="panel-heading"><?= lang('create_deposit') ?></header>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('reference_no') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="invoices_id" class="form-control select_box mb-5" data-width="100%" id="invoice_id" onchange="showClientName(this.value)" required>
                                    <option value="">---</option>
                                    <?php if(!empty($all_invoices)):
                                        foreach ($all_invoices as $invoice): ?>
                                            <option value="<?php echo $invoice->invoices_id; ?>" <?php
                                            if(!empty($deposit_info)){
                                                echo ($deposit_info->invoices_id == $invoice->invoices_id)?'selected':'';
                                            }
                                            ?>><?php echo $this->invoice_model->job_no_creation($invoice->invoices_id); ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('client') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="text" id="client_name" readonly class="form-control required mb-5">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('shipping_company') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <select name="shipping_account" class="form-control select_box required_select mb-5" data-width="100%" required>
                                    <option value="">---</option>
                                    <?php if(!empty($shipping_company_list)):
                                        foreach ($shipping_company_list as $shipping_company): ?>
                                            <option value="<?php echo $shipping_company->shipping_id; ?>" <?php
                                            if(!empty($deposit_info)){
                                                echo ($deposit_info->shipping_account == $shipping_company->shipping_id)?'selected':'';
                                            }
                                            ?>><?php echo $shipping_company->shipping_name; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('representative') ?> </label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" name="name_representative" value="<?= (!empty($deposit_info))?$deposit_info->name_representative:'' ?>" autocomplete="off">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('container') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" name="container_no" id="container_no" autocomplete="off" value="<?= (!empty($deposit_info))?$deposit_info->container_no:'' ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('arrival') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control datepicker" name="arrival" autocomplete="off" value="<?= (!empty($deposit_info))?$deposit_info->arrival:'' ?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('amount_deposit') ?></label>
                            <div class="col-lg-3">
                                <input type="number" min="0" class="form-control" name="deposit_amount" id="deposit_amount" autocomplete="off" value="<?= !empty($deposit_info)?round($deposit_info->deposit_amount,2):'' ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('advance_rent') ?></label>
                            <div class="col-lg-3">
                                <input type="number" min="0" class="form-control" name="advance_rent" id="advance_rent" autocomplete="off" value="<?= !empty($deposit_info)?round($deposit_info->advance_rent,2):'' ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('remaining_rent') ?></label>
                            <div class="col-lg-3">
                                <input type="number" min="0" class="form-control" name="remaining_rent" id="remaining_rent" autocomplete="off" value="<?= !empty($deposit_info)?round($deposit_info->remaining_rent,2):'' ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('final_rent') ?></label>
                            <div class="col-lg-3">
                                <input type="number" min="0" class="form-control" name="final_rent" id="final_rent" autocomplete="off" value="<?= !empty($deposit_info)?round($deposit_info->final_rent,2):'' ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">Total <?= lang('container_detention') ?></label>
                            <div class="col-lg-3">
                                <input type="number" min="0" class="form-control" name="container_detention" id="container_detention" autocomplete="off" value="<?= !empty($deposit_info)?round($deposit_info->container_detention,2):'' ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('refund_amount') ?></label>
                            <div class="col-lg-3">
                                <input type="number" min="0" class="form-control" name="refund_amount" autocomplete="off" value="<?php
                                if(!empty($deposit_info)){
                                   echo ($deposit_info->refund_amount != 0.00)?round($deposit_info->refund_amount,2):'';
                                } ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('delivery_date_shipment') ?></label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control datepicker" name="delivery_date_shipment" id="delivery_date_shipment" autocomplete="off" value="<?php
                                if(!empty($deposit_info)){
                                    echo ($deposit_info->delivery_date_shipment != '0000-00-00')?$deposit_info->delivery_date_shipment:'';
                                } ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('expected_date_of_refund') ?></label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control datepicker" name="expected_date_of_refund" autocomplete="off" value="<?php
                                if(!empty($deposit_info)){
                                    echo ($deposit_info->expected_date_of_refund != '0000-00-00')?$deposit_info->expected_date_of_refund:'';
                                } ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('deposit_file') ?></label>
                            <div class="col-lg-3">
                                <input type="file" name="deposit_file">
                                <?php
                                if(!empty($deposit_info)) {
                                    if (!empty($deposit_info->deposit_file)) {
                                        ?>
                                        <a href="<?= base_url('') . $deposit_info->deposit_file ?>"
                                           target="_blank"><b><?php $explode = explode('/', $deposit_info->deposit_file);
                                            echo $explode[2]; ?></b></a>
                                        <?php
                                    }
                                }
                                            ?>
                            </div>
                        </div>
                        <?php
                        if(!empty($deposit_info)) {
                            ?>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"><?= lang('deposit_return') ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <input type="checkbox" value="Yes" name="deposit_return" id="deposit_return" <?= ($deposit_info->deposit_return == 'Yes')?'checked':'' ?> />
                            </div>
                        </div>
                        <div id="return"
                             style="<?= ($deposit_info->deposit_return == 'Yes') ? 'display:block;' : 'display:none;' ?>">
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= lang('return_date') ?> <span
                                            class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control datepicker"
                                           name="return_date_container" autocomplete="off"
                                           value="<?= ($deposit_info->deposit_return == 'Yes') ? $deposit_info->return_date_container : '' ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?= lang('return_amount') ?> <span
                                            class="text-danger">*</span></label>
                                <div class="col-lg-3">
                                    <input type="number" min="0" class="form-control"
                                           name="deposit_return_amount" autocomplete="off"
                                           value="<?= ($deposit_info->deposit_return == 'Yes') ? round($deposit_info->deposit_return_amount, 2) : '' ?>">
                                </div>
                            </div>
                        </div>
                                <?php
                            }
                        ?>
					<div class="form-group">
					    <label class="col-lg-3 control-label"></label>
						<div class="col-lg-4">
							<button type="submit" class="btn btn-sm btn-success pull-right"><i class="fa fa-check"></i>
                                <?php
                                if(!empty($deposit_info)){
                                    echo lang('update_deposit');
                                }else{
                                    echo lang('create_deposit');
                                }
                                ?>
							</button>
						</div>
					</div>
				</div>
				</section>
            </form>
		</div>
	</div>

<script>
    $("#deposit_return").change(function() {
        if(this.checked) {
            $('#return').fadeIn(300).show();
        }
        else{
            $('#return').hide();
        }
    });

    $('#type').on("change", function() {
        var type = $(this).val();
        if(type == '1'){
            $('#cash_at_bank').fadeOut(500, function() { $(this).hide(); });
            $('#branches').fadeOut(500, function() { $(this).hide(); });
            $(".cash_at_bank").attr("disabled","disabled");
        }
        else{
            $('#cash_at_bank').fadeIn(500).show();
            $(".cash_at_bank").removeAttr("disabled");
        }
    });
    function selectBranches(val) {
        var option="";
        if(val == '') {
            $('#branches').hide();
        }
        else{
            $.getJSON("<?php echo site_url('admin/accounts/ajax_select_branches') ?>" + "/" + val, function (result) {
                $.each(result, function (index, value) {
                    option = ('<option value="' + value.BR_ID + '">' + value.BR_NAME + '</option>')+option;
                });
                var drop = '<div class="form-group"><label class="col-md-4 control-label">Branches <span class="text-danger">*</span></label><div class="col-lg-3" id="branches"><select name="branch_id" class="form-control cash_at_bank select_box required_select" onchange="getAccountId(this.value)"><option value="">-</option>' + option + '</select></div></div>';
                $('#branches').html(drop).hide().fadeIn(500);
                $('.select_box').select2({});
                getAccountId();
            });
        }
    }

    function getAccountId(val){
        account_id=$('#account_id');
        $.getJSON( "<?php echo site_url('admin/accounts/ajax_edit_branch') ?>"+"/"+val, function(result) {
            account_id.val(result.A_ID);
        });
    }
    
    function showClientName(invoice) {
        $.getJSON("<?php echo site_url('admin/security_deposit/get_client_name') ?>" + "/" + invoice, function (result) {
            $('#client_name').val(result);
        });
    }

    $(document).ready(function () {
       showClientName($('#invoice_id').val());
    });
</script>