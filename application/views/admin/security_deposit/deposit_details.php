

<section class="content-header">

    <?php

    $client_info = $this->invoice_model->check_by(array('client_id' => $deposit_info->client_id), 'tbl_client');

    ?>

    <div class="row">

        <div class="col-sm-8">



        </div>

        <div class="col-sm-4 pull-right">

            <a onclick="print_invoice('print_invoice_new')" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Print" class="btn btn-sm btn-danger pull-right"  >

                <i class="fa fa-print"></i>

            </a>

           <!-- <a style="margin-right: 5px" href="<?/*= base_url() */?>admin/security_deposit/manage_deposit/pdf_deposit/<?/*= $deposit_info->invoices_id */?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="PDF" class="btn btn-sm btn-warning pull-right" >

                <i class="fa fa-file-pdf-o"></i>

            </a>-->

        </div>

    </div>

</section>
<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('security_deposit_detail') ?></h1>
<section class="content">

    <!-- Start Display Details -->

    <!-- Main content -->

    <div class="row" >

        <section class="invoice" id="print_invoice">

            <!-- title row -->

            <div class="row">

                <div class="col-xs-12">

                    <h2 class="page-header">

                        <img style="width: 60px;width: 60px;margin-top: -10px;margin-right: 10px;" src="<?= base_url() . config_item('invoice_logo') ?>" ><?= config_item('company_name') ?>

                    </h2>

                </div><!-- /.col -->

            </div>

            <div class="row">

                <div class="col-xs-12 table-responsive">

                    <table class="table table-bordered">

                        <thead>

                        <tr>

                            <th colspan="4">Description Of Consignment</th>

                        </tr>

                        </thead>

                        <tbody>

                        <tr>

                            <td><b><?= lang('reference_no') ?></b></td>

                            <td><?= $this->invoice_model->job_no_creation($deposit_info->invoices_id) ?></td>

                            <td><b><?= lang('client') ?></b></td>

                            <td class="text-right"><?= $client_info->name ?></td>

                        </tr>

                        <tr>

                           <!-- <td><b><?/*= lang('vessel') */?></b></td>

                            <td><?/*= $deposit_info->vessel */?></td>-->

                            <td><b><?= lang('shipping_line') ?></b></td>

                            <td class="text-right"><?= read_subHead($deposit_info->shipping_account)->A_NAME ?></td>

                        </tr>



                        <tr>

                            <td><b><?= lang('container') ?></b></td>

                            <td><?= $deposit_info->container_no ?></td>

                            <td><b><?= lang('bl_no') ?></b></td>

                            <td><?= $deposit_info->bl_no ?></td>

                        </tr>

                        <tr>
                            <td><b><?= lang('arrival') ?></b></td>

                            <td><?= $deposit_info->arrival ?></td>
                            
                            <td><b><?= lang('amount_deposit') ?></b></td>

                            <td class="text-right"><?= number_format(($deposit_info->deposit_amount),2) ?></td>

                        </tr>

                        <tr>

                            <td><b><?= lang('deposit_date') ?></b></td>

                            <td><?= strftime(config_item('date_format'), strtotime($deposit_info->created_date)) ?></td>

                            <td><b><?= lang('delivery_date_shipment') ?></b></td>

                            <td class="text-right"><?= strftime(config_item('date_format'), strtotime($deposit_info->delivery_date_shipment)) ?></td>

                        </tr>

                        <tr>

                            <td><b><?= lang('advance_rent') ?></b></td>

                            <td><?= number_format(($deposit_info->advance_rent),2) ?></td>

                            <td><b><?= lang('remaining_rent') ?></b></td>

                            <td class="text-right"><?= number_format(($deposit_info->remaining_rent),2) ?></td>

                        </tr>

                        <tr>

                            <td><b><?= lang('final_rent') ?></b></td>

                            <td><?= number_format(($deposit_info->final_rent),2) ?></td>

                            <td><b><?= lang('refund_amount') ?></b></td>

                            <td class="text-right"><?= number_format(($deposit_info->refund_amount),2)  ?></td>

                        </tr>

                        <tr>

                            <td><b><?= lang('expected_date_of_refund') ?></b></td>

                            <td><?= strftime(config_item('date_format'), strtotime($deposit_info->expected_date_of_refund)) ?></td>

                            <td><b><?= lang('deposit_return') ?></b></td>

                            <td class="text-right"><?= $deposit_info->deposit_return ?></td>

                        </tr>

                        <?php

                            if($deposit_info->deposit_return == 'Yes') {

                                ?>

                                <tr>

                                    <td><b><?= lang('return_amount') ?></b></td>

                                    <td><?= $deposit_info->deposit_return_amount; ?></td>

                                    <td><b><?= lang('return_date') ?></b></td>

                                    <td class="text-right"><?= strftime(config_item('date_format'), strtotime($deposit_info->return_date_container)) ?></td>

                                </tr>

                                <?php

                            }

                        ?>

                        </tbody>

                    </table>



                </div>

            </div>

        </section>

        <section style="display: none;" class="invoice" id="print_invoice_new">

            <?php $this->load->view('admin/security_deposit/pdf_deposit'); ?>

        </section>

    </div>



</section>

<script type="text/javascript">

    function print_invoice(print_invoice_new) {

        var printContents = document.getElementById(print_invoice_new).innerHTML;

        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;

    }

</script>