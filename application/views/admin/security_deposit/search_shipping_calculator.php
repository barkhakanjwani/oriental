<link href="<?php echo base_url() ?>asset/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?php echo base_url() ?>asset/js/bootstrap-toggle.min.js"></script>
<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
	<div class="row">
	<div class="col-lg-12">
        <form role="form" enctype="multipart/form-data" id="form" action="<?php echo base_url(); ?>admin/security_deposit/search_shipping_calculator" method="post" class="form-horizontal  ">
            <section class="panel panel-default">
                <header class="panel-heading  "><?= lang('search_job_no') ?></header>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?= lang('search_by') ?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <select class="form-control select_box" style="width: 100%" id="search_by"  required>
                                <option value=""><?= lang('search_by'); ?></option>
                                <option value="job_no" <?= (isset($reference_no))?'selected':'' ?>><?= lang('reference_no'); ?></option>
                                <option value="client" <?= (isset($client_id))?'selected':'' ?>><?= lang('client'); ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="<?= (isset($client_id))?'display:block;':'display:none;' ?>" id="client">
                        <label class="col-lg-3 control-label"><?= lang('client') ?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <select class="form-control select_box client" style="width: 100%"  name="client_id" required <?= (isset($client_id))?'':'disabled' ?>>
                                <option value="">Choose Client</option>
                                <?php
                                if (!empty($all_client)) {
                                    foreach ($all_client as $v_client) {
                                        ?>
                                        <option value="<?= $v_client->client_id ?>" <?php
                                        if(isset($client_id)){
                                            echo ($v_client->client_id == $client_id)?'selected':'';
                                        }  ?>><?= $v_client->name ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="<?= (isset($reference_no))?'display:block;':'display:none;' ?>" id="job_no">
                        <label class="col-lg-3 control-label"><?= lang('reference_no') ?> <span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control job_no" name="reference_no" placeholder="<?= lang('reference_no') ?>" value="<?= (isset($reference_no))?$reference_no:'' ?>" <?= (isset($reference_no))?'':'disabled' ?> required autocomplete="off" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"></label>
                        <div class="col-lg-3">
                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-search"></i> <?= lang('search') ?></button>
                        </div>
                    </div>
                </div>
            </section>
        </form>
		</div>
	</div>
	<?php
/*		if(isset($reference_no) || isset($client_id)){
	*/?>
	<div class="row">
		<div class="col-lg-12">
			<section class="panel panel-default">
                <header class="panel-heading  "><?= lang('manage_shipping_detail') ?></header>
                <div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped DataTables " id="DataTables">
                    <thead>
                        <tr>
                            <!--<th>#</th>-->
                            <th><?= lang('job_no') ?></th>
                            <th><?= lang('client') ?></th>
                            <th><?= lang('shipping_company') ?></th>
                            <th><?= lang('container') ?></th>
                            <!--<th><?/*= lang('rent_per_day') */?></th>
                            <th><?/*= lang('totals') */?></th>-->
                            <th><?= lang('exchange_rates') ?></th>
                           <!-- <th><?/*= lang('per_day_rent_total') */?></th>-->
                            <th><?= lang('free_day') ?></th>
                            <th><?= lang('igm_dates') ?></th>
                            <th><?= lang('shipping_rent_date') ?></th>
                            <!--<th><?/*= lang('total_days') */?></th>
                            <th><?/*= lang('total_rent_days') */?></th>
                            <th><?/*= lang('total_in_usd') */?></th>
                            <th><?/*= lang('total_in_pkr') */?></th>-->
                            <th><?= lang('er_date') ?></th>
                            <!--<th><?/*= lang('additional_rent_days') */?></th>
                            <th><?/*= lang('additional_rent_in_usd') */?></th>
                            <th><?/*= lang('total_amount_in_pkr') */?></th>-->
                            <th class="col-options no-sort" ><?= lang('action') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $counter = 1;
							if (!empty($shipping_info)) {
								foreach ($shipping_info as $s_calculate) {
								    $client_info = $this->invoice_model->check_by(array('client_id'=>$s_calculate->client_id), 'tbl_client');
									?>
                                    <tr>
                                        <td><?= $this->invoice_model->job_no_creation($s_calculate->invoices_id) ?></td>
                                        <td><?= ucfirst($client_info->name) ?></td>
                                        <td><?= ucfirst($s_calculate->shipping_company) ?></td>
                                        <td><?= ucfirst($s_calculate->number_container) ?></td>
                                        <!--<td><?/*= ucfirst($s_calculate->rent_per_day) */?></td>
                                        <td><?/*= ucfirst($s_calculate->totals) */?></td>-->
                                        <td><?= ucfirst($s_calculate->exchange_rates) ?></td>
                                        <!--<td><?/*= ucfirst($s_calculate->per_day_rent_total) */?></td>-->
                                        <td><?= ucfirst($s_calculate->free_day) ?></td>
                                        <td><?= strftime(config_item('date_format'), strtotime($s_calculate->igm_dates)) ?></td>
                                        <td><?= strftime(config_item('date_format'), strtotime($s_calculate->shipping_rent_date)) ?></td>
                                        <!--<td><?/*= ucfirst($s_calculate->total_days) */?></td>
                                        <td><?/*= ucfirst($s_calculate->per_day_rent_total) */?></td>
                                        <td><?/*= ucfirst($s_calculate->total_in_usd) */?></td>
                                        <td><?/*= ucfirst($s_calculate->total_in_pkr) */?></td>-->
                                        <td><?= strftime(config_item('date_format'), strtotime($s_calculate->er_date)) ?></td>
                                       <!-- <td><?/*= ucfirst($s_calculate->additional_rent_days) */?></td>
                                        <td><?/*= ucfirst($s_calculate->additional_rent_in_usd) */?></td>
                                        <td><?/*= ucfirst($s_calculate->total_amount_in_pkr) */?></td>-->
                                        <td>
                                            <?= btn_view('admin/security_deposit/manage_shipping/shipping_details/' . encode($s_calculate->sc_id)) ?><!--
                                            <?/*= btn_edit('admin/security_deposit/manage_shipping/shipping_calculator/' . encode($s_calculate->sc_id)) */?>
                                            --><?= btn_delete('admin/security_deposit/delete_shipping/' . encode($s_calculate->sc_id)) ?>
                                        </td>
                                    </tr>
									<?php
                                    $counter++;
								}
							}
							?>
                    </tbody>
                </table>
			</div>
		</section>
	</div>
	</div>
	<?php
/*		}
	*/?>
    <script>
        /*$(".client").attr("disabled", true);
        $(".job_no").attr("disabled", true);*/
        $('#search_by').on("change", function() {
            var type = $(this).val();
            if(type == 'client'){
                $('#client').show();
                $('#job_no').hide();
                $(".client").removeAttr("disabled");
                $(".job_no").attr("disabled","disabled");
            }
            else if (type == 'job_no'){
                $('#job_no').show();
                $('#client').hide();
                $(".job_no").removeAttr("disabled");
                $(".client").attr("disabled","disabled");
            }
            else{
                $('#client').hide();
                $('#job_no').hide();
                $(".client").removeAttr("disabled");
                $(".job_no").removeAttr("disabled");
            }
        });
    </script>
