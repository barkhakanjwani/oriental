
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>asset/js/menu.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>asset/js/custom-validation.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>asset/js/jquery.validate.js" type="text/javascript"></script>
<!-- Jasny Bootstrap for NIce Image Change -->
<script src="<?php echo base_url() ?>asset/js/jasny-bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>asset/js/bootstrap-datepicker.js" ></script>
<script src="<?php echo base_url() ?>asset/js/timepicker.js" ></script>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>asset/js/plugins/metisMenu/metisMenu.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>asset/js/plugins/dataTables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>asset/js/plugins/dataTables/dataTables.bootstrap.js" type="text/javascript"></script>
<!--select 2 -->
<script src="<?php echo base_url() ?>asset/js/select2.js"></script>
<!-- Bootstrap Slider -->
<script src="<?php echo base_url() ?>plugins/bootstrap-slider/bootstrap-slider.js" type="text/javascript"></script>
<!-- icheck -->
<script src="<?php echo base_url() ?>plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<!-- Easypichart -->
<script src="<?php echo base_url() ?>plugins/charts/easypiechart/jquery.easy-pie-chart.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url() ?>asset/js/bootstrap-wysihtml5.js"></script>
<!-- toastr -->
<script src="<?php echo base_url() ?>asset/js/toastr.min.js"></script>
<!-- summernote Editor -->
<script src="<?php echo base_url() ?>plugins/summernote/summernote.min.js"></script>
<script src="<?php echo base_url() ?>plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>asset/js/datatable-list.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("[id^=DataTables]").dataTable();
        $('#form').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });
    });
    $('.textarea').summernote({
        codemirror: {// codemirror options
            theme: 'monokai'
        }
    });
    $('.note-toolbar .note-fontsize,.note-toolbar .note-help,.note-toolbar .note-fontname,.note-toolbar .note-height,.note-toolbar .note-table').remove();
</script>
<!-- CK Editor -->
<script type="text/javascript">
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        //bootstrap WYSIHTML5 - text editor
        $(".wysihtml5").wysihtml5();
    });
</script>
<!--- Filter Accounts --->
<script type="text/javascript">
	function getSelectedValue(id){
		if(id != null){
			$.getJSON( "<?php echo site_url('admin/accounts/getAccountName') ?>"+"/"+id, function(result) {
				$(".selectedAccount").html('<option value="' + id + '" selected>'+result+'</option>').change();
			});
		}
		
	}
	$(document).ready(function(){
		getSelectedValue();
		(function ($) {
			var autocompleteInput = $(".filterAccounts");
			autocompleteInput.select2({
				placeholder: 'Search Account',
				minimumInputLength: 2,
				ajax: {
					url: '<?= base_url('admin/accounts/filterAccounts') ?>',
					type: "post",
					dataType: 'json',
					delay: 250,
					data: function (params) {
						return {
							searchTerm: params.term
						};
					},
					processResults: function (response) {
						return {
							results: response
						};
					},
					cache: false
				}
			});
		})(jQuery);
	});
</script>
<!-- Convert Amount in words -->
        <script type="text/javascript">
            function numberToEnglish( n ) {
                if(n === ''){
                    return '';
                }
                var string = n.toString(), units, tens, scales, start, end, chunks, chunksLen, chunk, ints, i, word, words, and = '';
    
                /* Is number zero? */
                if( parseInt( string ) === 0 ) {
                    return 'Zero';
                }
    
                /* Array of units as words */
                units = [ '', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen' ];
    
                /* Array of tens as words */
                tens = [ '', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety' ];
    
                /* Array of scales as words */
                scales = [ '', 'Thousand', 'Million', 'Billion', 'Trillion', 'Quadrillion', 'Quintillion', 'Sextillion', 'Septillion', 'octillion', 'Nonillion', 'Decillion', 'Undecillion', 'Duodecillion', 'Tredecillion', 'Quatttuor-decillion', 'Quindecillion', 'Sexdecillion', 'Septen-decillion', 'Octodecillion', 'Novemdecillion', 'Vigintillion', 'Centillion' ];
    
                /* Split user arguemnt into 3 digit chunks from right to left */
                start = string.length;
                chunks = [];
                while( start > 0 ) {
                    end = start;
                    chunks.push( string.slice( ( start = Math.max( 0, start - 3 ) ), end ) );
                }
    
                /* Check if function has enough scale words to be able to stringify the user argument */
                chunksLen = chunks.length;
                if( chunksLen > scales.length ) {
                    return '';
                }
    
                /* Stringify each integer in each chunk */
                words = [];
                for( i = 0; i < chunksLen; i++ ) {
        
                    chunk = parseInt( chunks[i] );
        
                    if( chunk ) {
            
                        /* Split chunk into array of individual integers */
                        ints = chunks[i].split( '' ).reverse().map( parseFloat );
            
                        /* If tens integer is 1, i.e. 10, then add 10 to units integer */
                        if( ints[1] === 1 ) {
                            ints[0] += 10;
                        }
            
                        /* Add scale word if chunk is not zero and array item exists */
                        if( ( word = scales[i] ) ) {
                            words.push( word );
                        }
            
                        /* Add unit word if array item exists */
                        if( ( word = units[ ints[0] ] ) ) {
                            words.push( word );
                        }
            
                        /* Add tens word if array item exists */
                        if( ( word = tens[ ints[1] ] ) ) {
                            words.push( word );
                        }
            
                        /* Add 'and' string after units or tens integer if: */
                        if( ints[0] || ints[1] ) {
                
                            /* Chunk has a hundreds integer or chunk is the first of multiple chunks */
                            if( ints[2] || ! i && chunksLen ) {
                                words.push( and );
                            }
                
                        }
            
                        /* Add hundreds word if array item exists */
                        if( ( word = units[ ints[2] ] ) ) {
                            words.push( word + ' Hundred' );
                        }
            
                    }
        
                }
    
                return words.reverse().join( ' ' )+' Only';
    
            }

            var table = $('.data-table-list1').DataTable({
                "processing": true,
                "responsive": true,
                "serverSide": true,
                "order": [],
                "ajax": {
                    "url": data_url,
                    "type": "POST"
                },
                drawCallback:function(data)
                {
                    $('#total_sales').html(data.json.total);
                }
            });
        </script>
        <!-- /Convert Amount in words -->

</body>
</html>