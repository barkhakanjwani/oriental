
        <link href="<?= base_url(); ?>asset/button-datatable/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
        <script src="<?= base_url(); ?>asset/button-datatable/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>asset/button-datatable/dataTables.buttons.min.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>asset/button-datatable/jszip.min.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>asset/button-datatable/pdfmake.min.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>asset/button-datatable/vfs_fonts.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>asset/button-datatable/buttons.html5.min.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>asset/button-datatable/buttons.print.min.js"></script>
        <script>
            $(document).ready(function() {
                var a = '<?= base_url().config_item('company_logo') ?>';
                var b = '<?= config_item('company_legal_name') ?>';
                var c = '<?= $title ?>';
                var d = '<?= date('d-m-Y',strtotime($start_date))." TO ".date('d-m-Y',strtotime($end_date)) ?>';
                $('#table').DataTable( {
                    dom: 'Bfrtip',
                    "paging": false,
                    "ordering": false,
                    buttons: [
                        'copyHtml5',
                        {
                            extend: 'excelHtml5',
                            title:c+" - "+d,
                            filename:c,
                            customize: function ( xlsx ){
                                var sheet = xlsx.xl.worksheets['sheet1.xml'];

                                /*jQuery selector to add a border*/
                                $('row c[r*="2"]', sheet).attr( 's', '25' );
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            title: b,
                            filename:c,
                            customize: function (csv) {
                                return c+" - "+d+"\n"+csv;
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            title: '',
                            filename:c,
                            customize: function ( doc ) {
                                doc.content.splice(0, 0, {
                                    text: b+" ( "+c+" )\n"+d+"\n",
                                    alignment:'center'
                                });
                                doc.defaultStyle.fontSize = 8;
                                doc.styles.tableHeader.fontSize = 8;
                            }
                        },
                        {
                            extend: 'print',
                            title: '<h3 style="text-align:center;">'+b+'<br /><br /><span style="font-size:12px;">'+c+'</span><br /><span style="font-size:12px;">'+d+'</span></h3>',
                            customize: function ( win ) {
                                $(win.document.body)
                                    .css( 'font-size', '6pt' )
                                    .prepend(
                                        '<img src="'+a+'" style="position:absolute; top:50%; left:25%;opacity:0.4;" />'
                                    );

                                $(win.document.body).find( 'table' )
                                    .addClass( 'compact' )
                                    .css( 'font-size', 'inherit' );
                            }
                        }
                    ]
                } );
            } );

        </script>