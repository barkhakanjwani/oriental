<header class="main-header">
    <?php
    $user_id = $this->session->userdata('user_id');
    $profile_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
    $user_info = $this->db->where('user_id', $user_id)->get('tbl_users')->row();
    $this->db->select('*');
    $this->db->from('tbl_invoices');
    $this->db->where('eta', date('Y-m-d'));
    $consignment_arrival = $this->db->get()->result();

    $this->db->select('*')->from('tbl_invoices');
    $this->db->where('`invoices_id` NOT IN (SELECT `invoices_id` FROM `tbl_requisitions`)', NULL, FALSE);
    $client_quotes = $this->db->get()->result();

    $this->db->select('*')->from('tbl_requisitions');
    $this->db->where('`invoices_id` NOT IN (SELECT `invoices_id` FROM `tbl_bills`)', NULL, FALSE);
    $bill_submitting = $this->db->get()->result();

    $this->db->select('*');
    $this->db->from('tbl_invoices');
    $this->db->join('tbl_bills', 'tbl_invoices.invoices_id = tbl_bills.invoices_id');
    $all_jobs = $this->db->get()->result();

    $this->db->select('*');
    $this->db->from('tbl_job_activity');
    $this->db->join('tbl_job_status', 'tbl_job_activity.job_activity_status = tbl_job_status.job_status_id');
    $this->db->where('tbl_job_activity.job_activity_date', date('Y-m-d'));
    $this->db->where('tbl_job_activity.job_activity_notification', 'Yes');
    $all_job_activity = $this->db->get()->result();

    $this->db->select('*');
    $this->db->from('tbl_security_deposit');
    $this->db->where('deposit_return','No');
    $this->db->where('expected_date_of_refund <=',date('Y-m-d'));
    $security_deposit = $this->db->get()->result();
    $count_notify = count($consignment_arrival)+count($client_quotes)+count($bill_submitting)+count($all_job_activity)+count($security_deposit);
    ?>
    <?php $display = config_item('logo_or_icon'); ?>

    <a href="<?= base_url() ?>" class="logo">
        <?php $display = config_item('logo_or_icon'); ?>
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <?php if ($display == 'logo' || $display == 'logo_title') { ?>
            <span class="logo-mini"><img style="width: 48px;height: 48px;border-radius: 50px" src="<?= base_url() . config_item('company_logo') ?>" class="m-r-sm"></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">
                <img style="width: 50px;height: 40px;background: #fff;border-radius: 50%;" src="<?= base_url() . config_item('company_logo') ?>" >
            </span>
        <?php }
        ?>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>        
        <div class="pull-left hidden-xs">
            <ul class="nav">
                <li style="">
                    <a href="" class="text-center" style="vertical-align: middle;color: #FFFFFF;font-size: 20px;"><?php
                        if ($display == 'logo_title' || $display == 'icon_title') {
                            if (config_item('website_name') == '') {
                                echo config_item('company_name');
                            } else {
                                echo config_item('website_name');
                            }
                        }
                        ?></a>        
                </li>
            </ul>
        </div>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-danger"><?=  $count_notify ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header" style="padding: 11px">Reminders</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <?php
                                if(!empty($consignment_arrival)) {
                                    foreach ($consignment_arrival as $job) {
                                        ?>
                                        <li>
                                            <a href="#">
                                                <h4 style="margin: 0px">
                                                    <span style="font-size: 12px"> Consignment Arrival </span>
                                                    <!--<small><i class="fa fa-clock-o"></i>
                                                        12
                                                    </small>-->
                                                </h4>
                                                <p style="margin:0px;font-size:10px;"> <?= $this->invoice_model->job_no_creation($job->invoices_id) ?> arriving today</p>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                }
                                if(!empty($client_quotes)) {
                                    foreach ($client_quotes as $job) {
                                        ?>
                                        <li>
                                            <a href="#">
                                                <h4 style="margin: 0px">
                                                    <span style="font-size: 12px"> Client Quote </span>
                                                    <!--<small><i class="fa fa-clock-o"></i>
                                                        12
                                                    </small>-->
                                                </h4>
                                                <p style="margin:0px;font-size:10px;"> <?= $this->invoice_model->job_no_creation($job->invoices_id) ?> requisition not created</p>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                }
                                if(!empty($bill_submitting)) {
                                    foreach ($bill_submitting as $job) {
                                        ?>
                                        <li>
                                            <a href="#">
                                                <h4 style="margin: 0px">
                                                    <span style="font-size: 12px"> Client Bill Submitting </span>
                                                    <!--<small><i class="fa fa-clock-o"></i>
                                                        12
                                                    </small>-->
                                                </h4>
                                                <p style="margin:0px;font-size:10px;"> <?= $this->invoice_model->job_no_creation($job->invoices_id) ?> invoice not created</p>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                }
                                if(!empty($all_jobs)) {
                                    foreach ($all_jobs as $job) {
                                        $due = $this->invoice_model->calculate_to('invoice_due', $job->invoices_id);
                                        if ($due > 0) {
                                            ?>
                                            <li>
                                                <a href="#">
                                                    <h4 style="margin: 0px">
                                                        <span style="font-size: 12px"> Bill Amount Receivable </span>
                                                        <!--<small><i class="fa fa-clock-o"></i>
                                                            12
                                                        </small>-->
                                                    </h4>
                                                    <p style="margin:0px;font-size:10px;"> <?= $this->invoice_model->job_no_creation($job->invoices_id) ?>
                                                        invoice amount not received</p>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                    }
                                }
                                $currentDate = date('Y-m-d');
                                $stDate = date('Y-m-09');
                                if($currentDate == $stDate) {
                                    if (!empty($all_jobs)) {
                                        foreach ($all_jobs as $job) {
                                            $due = $this->invoice_model->calculate_to('invoice_due', $job->invoices_id);
                                            if ($due > 0) {
                                                ?>
                                                <li>
                                                    <a href="#">
                                                        <h4 style="margin: 0px">
                                                            <span style="font-size: 12px"> Sales Tax Payable </span>
                                                            <!--<small><i class="fa fa-clock-o"></i>
                                                                12
                                                            </small>-->
                                                        </h4>
                                                        <p style="margin:0px;font-size:10px;"> <?= $this->invoice_model->job_no_creation($job->invoices_id) ?>
                                                            sales tax not paid</p>
                                                    </a>
                                                </li>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                if (!empty($all_job_activity)) {
                                    foreach ($all_job_activity as $job_activity) {
                                            ?>
                                            <li>
                                                <a href="#">
                                                    <h4 style="margin: 0px">
                                                        <span style="font-size: 12px"> Job Activity </span>
                                                        <!--<small><i class="fa fa-clock-o"></i>
                                                            12
                                                        </small>-->
                                                    </h4>
                                                    <p style="margin:0px;font-size:10px;"> <?= $this->invoice_model->job_no_creation($job_activity->invoices_id) ?> (<?= $job_activity->job_status_title ?>)
                                                    </p>
                                                </a>
                                            </li>
                                            <?php
                                    }
                                }
                                if (!empty($security_deposit)) {
                                    foreach ($security_deposit as $deposit) {
                                        ?>
                                        <li>
                                            <a href="#">
                                                <h4 style="margin: 0px">
                                                    <span style="font-size: 12px"> <?= lang('security_deposit') ?> </span>
                                                    <!--<small><i class="fa fa-clock-o"></i>
                                                        12
                                                    </small>-->
                                                </h4>
                                                <p style="margin:0px;font-size:10px;"> <?= $this->invoice_model->job_no_creation($deposit->invoices_id) ?> has been due.
                                                </p>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </li>
                        <!--<li class="footer"><a href="<?/*= base_url() */?>admin/mailbox"><?/*= lang('view_all') */?></a></li>-->
                    </ul>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= base_url() . $profile_info->avatar ?>" class="user-image" alt="User Image" />
                        <span class="hidden-xs"><?= $profile_info->fullname ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= base_url() . $profile_info->avatar ?>" class="img-circle" alt="User Image" />
                            <p>
                                <?= $profile_info->fullname ?>
                                <small><?= lang('last_login') . ':' ?>
                                    <?php
                                    if ($user_info->last_login == '0000-00-00 00:00:00') {
                                        $login_time = "-";
                                    } else {
                                        $login_time = strftime(config_item('date_format') . " %H:%M:%S", strtotime($user_info->last_login));
                                    }
                                    echo $login_time;
                                    ?>
                                </small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="col-xs-4 text-center">                                                                
                                <a href="<?= base_url() ?>admin/settings/activities" ><?= lang('activities') ?></a>
                            </div>
                            <div class="col-xs-4 text-center">

                            </div>
                            <div class="col-xs-4 text-center">                                
                                <a href="<?= base_url() ?>locked/lock_screen" ><?= lang('lock_screen') ?></a>
                            </div>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?= base_url() ?>admin/settings/update_profile" class="btn btn-default btn-flat"><?= lang('update_profile') ?></a>
                            </div>
                            <div class="pull-right">
                                <a href="<?= base_url() ?>login/logout" class="btn btn-default btn-flat"><?= lang('logout') ?></a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->

                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-bars"></i>
                        <span class="label label-danger"><?php
                            $user = $this->session->userdata('user_id');
                            $this->db->where('user_id', $user);
                            $this->db->where('status', 0);
                            $query = $this->db->get('tbl_todo');

                            $incomplete_todo_number = $query->num_rows();
                            if ($incomplete_todo_number > 0) {
                                echo $incomplete_todo_number;
                            }
                            ?></span>
                    </a>
                </li>
            </ul>
        </div>

    </nav>
</header>
<!-- Control Sidebar -->
<?php
$opened = $this->session->userdata('opened');
$this->session->unset_userdata('opened');
?>
<aside class="control-sidebar control-sidebar-dark <?php
if (!empty($opened)) {
    echo 'control-sidebar-open';
}
?>">
    <style>
        .active{
            background:none;
        }
    </style>
    <!-- Create the tabs -->    
    <!-- Tab panes -->
    <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane active" style="background:none;" id="control-sidebar-home-tab">
            <h2 style="color: #EFF3F4;font-weight: 100;text-align: center;">
                <?php echo date("l"); ?>
                <br />
                <?php echo date("jS F, Y"); ?>
            </h2>
            <form action="<?= base_url() ?>admin/user/todo/add" method="post" class="form-horizontal form-groups" style="margin-top: 40px">
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-1">
                        <textarea class="form-control" type="text" name="title" placeholder="+<?= lang('add_todo') ?>" 
                                  style="background-color: #364559;border: 1px solid #4F595E;color: rgba(170,170,170 ,1);"
                                  data-validate="required"></textarea>
                    </div>
                    <input type="submit" value="<?= lang('add') ?>" class="btn btn-success btn-xs"  />
                </div>
            </form>
            <table style="width: 83%;margin-left: 22px;">
                <?php
                $this->db->where('user_id', $user_id);
                $this->db->order_by('order', 'asc');
                $todos = $this->db->get('tbl_todo')->result_array();
                foreach ($todos as $row):
                    ?>
                    <tr>
                        <td>
                    <li id="todo_1" 
                        style="<?php if ($row['status'] == 1): ?>text-decoration: line-through;<?php endif; ?>font-size: 13px;
                        <?php if ($row['status'] == 0): ?>color: #fff;<?php endif; ?>;">
                        <?php echo $row['title']; ?>
                    </li>
                    </td>
                    <td style="text-align:right;">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-sm dropdown-toggle " data-toggle="dropdown"
                                    style="padding:0px;background-color: #303641;border: 0px;-ms-transform: rotate(90deg); /* IE 9 */
                                    -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
                                    transform: rotate(90deg);">
                                <i class="entypo-dot-2" style="color:#B4BCBE;"></i> 
                                <span class="" style="visibility:hidden; width:0px;"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-default pull-right" role="menu" style="text-align:left;">
                                <li>
                                    <?php if ($row['status'] == 0): ?>
                                        <a href="<?= base_url() ?>admin/user/todo/mark_as_done/<?php echo $row['todo_id']; ?>">
                                            <i class="entypo-check"></i>
                                            <?php echo lang('mark_completed'); ?>
                                        </a>
                                    <?php endif; ?>
                                    <?php if ($row['status'] == 1): ?>
                                        <a href="<?= base_url() ?>admin/user/todo/mark_as_undone/<?php echo $row['todo_id']; ?>">
                                            <i class="entypo-cancel"></i>
                                            <?php echo lang('mark_incomplete'); ?>
                                        </a>
                                    <?php endif; ?>
                                </li>


                                <li>
                                    <a href="<?= base_url() ?>admin/user/todo/swap/<?php echo $row['todo_id']; ?>/up">
                                        <i class="entypo-up"></i>
                                        <?php echo lang('move_up'); ?>
                                    </a>
                                    <a href="<?= base_url() ?>admin/user/todo/swap/<?php echo $row['todo_id']; ?>/down">
                                        <i class="entypo-down"></i>
                                        <?php echo lang('move_down'); ?>
                                    </a>
                                </li>
                                <li class="divider"></li>


                                <li>
                                    <a href="<?= base_url() ?>admin/user/todo/delete/<?php echo $row['todo_id']; ?>">
                                        <i class="entypo-trash"></i>
                                        <?= lang('delete'); ?>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </td>
                    </tr>
                <?php endforeach; ?>
            </table>

            <div id="idCalculadora"></div>

        </div><!-- /.tab-pane -->                
    </div>
</aside><!-- /.control-sidebar -->
<link rel="stylesheet" href="<?= base_url() ?>asset/js/plugins/calculator/SimpleCalculadorajQuery.css">
<script src="<?= base_url() ?>asset/js/plugins/calculator/SimpleCalculadorajQuery.js"></script>
<script>
    $("#idCalculadora").Calculadora({'EtiquetaBorrar': 'Clear', TituloHTML: ''});
</script>