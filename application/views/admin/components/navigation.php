<!-- Left side column. contains the logo and sidebar -->
<?php
$user_permission = $_SESSION["user_roll"];

foreach ($user_permission as $v_permission) {
    $user_roll[$v_permission->menu_id] = $v_permission->menu_id;
}
?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <?php
    $user_id = $this->session->userdata('user_id');
    $profile_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
    $user_info = $this->db->where('user_id', $user_id)->get('tbl_users')->row();
    ?>
    <section class="sidebar">    
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= base_url() . $profile_info->avatar ?>" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p><?= $profile_info->fullname ?></p>
                <a href="#"> <?= $this->session->userdata('dept_name'); ?></a>
            </div>
        </div>        
        <br/>
        <!-- sidebar menu: : style can be found in sidebar.less -->        

        <?php
            echo $this->menu->dynamicMenu();
            if($this->session->userdata('role_id') == 5) {
            }else{
                ?>
                <ul class="sidebar-menu">
                    <li class="<?php
                    if (!empty($page)) {
                        echo $page == 'message' ? 'active' : '';
                    }
                    ?>">
                        <a href="<?php echo base_url(); ?>admin/message/" class="sidebar-margin"> <i
                                    class="sidebar-icon fa fa-envelope"></i><span><?= lang('private_chat') ?></span></a>
                    </li>
                    <?php
                    $user_id = $this->session->userdata('user_id');
                    $role = $this->admin_model->check_by(array('user_id' => $user_id), 'tbl_users');
                    ?>

                    <?php
                    $online_user = $this->db->where(array('online_status' => '1'))->get('tbl_users')->result();

                    if (!empty($online_user)):
                        ?>
                        <li class="content-header"
                            style=";font-weight: bold;color: #fff;font-size: 14px;"><?= lang('online') ?></li>
                        <?php
                        foreach ($online_user as $v_online_user):
                            if ($v_online_user->user_id != $this->session->userdata('user_id')) {
                                if ($v_online_user->role_id == 1) {
                                    $user = 'Admin';
                                } elseif ($v_online_user->role_id == 3) {
                                    $user = 'Staff';
                                }else {
                                    $user = 'Client';
                                }
                                ?>
                                <li class="">
                                    <a title="<?php echo $user ?>" data-placement="top" data-toggle="tooltip"
                                       class="dker"
                                       href="<?php echo base_url(); ?>admin/message/get_chat/<?php echo $v_online_user->user_id ?>">
                                        <?php echo $v_online_user->username ?>
                                        <b class="label label-success pull-right"> <i
                                                    class="fa fa-dot-circle-o fa-spin"></i></b>
                                    </a>
                                </li>
                                <?php
                            }
                        endforeach;
                        ?>
                    <?php endif ?>
                </ul>
                <?php
            }
        ?>
    </section>
    <!-- /.sidebar -->
</aside>