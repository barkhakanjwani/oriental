<?= message_box('success'); ?>
<?= message_box('error'); ?>
<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('manage_staffs') ?></h1>
<div class="nav-tabs-custom">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage" data-toggle="tab"><?= lang('all_staffs') ?></a></li>
        <li class="<?= $active == 2 ? 'active' : ''; ?>"><a href="#new" data-toggle="tab"><?= lang('add_staff') ?></a></li>
    </ul>
    <div class="tab-content no-padding">
        <!-- ************** general *************-->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">
            <div class="table-responsive" id="customers">
                <table class="table table-bordered data-table-list dtHorizontalExample" id="tab_customers">
                    <thead>
                    <th><?= lang('created_date') ?></th>
                    <th><?= lang('name') ?></th>
                    <th><?= lang('email') ?></th>
                    <th><?= lang('contact') ?></th>
                    <th><?= lang('status') ?></th>
                    <th><?= lang('action') ?></th>
                    </thead>
                </table>
            </div>
            <!--<table class="table table-striped" id="DataTables">
                <thead>
                <tr>
                    <th><?/*= lang('created_date') */?></th>
                    <th><?/*= lang('name') */?></th>
                    <th><?/*= lang('email') */?></th>
                    <th><?/*= lang('contact') */?></th>
                    <th><?/*= lang('status') */?></th>
                    <th class="text-center"><?/*= lang('action') */?></th>

                </tr>
                </thead>
                <tbody>
                <?php
/*                if (!empty($all_staffs)): foreach ($all_staffs as $staff) :
                    */?>
                    <tr>
                        <td><?/*= strftime(config_item('date_format'), strtotime($staff->staff_date)); */?></td>
                        <td><?php /*echo ucfirst($staff->staff_name) */?></td>
                        <td><?/*= $staff->staff_email */?></td>
                        <td><?/*= $staff->staff_contact */?></td>
                        <td>
                            <?php /*if ($staff->staff_status == 1): */?>
                                <a data-toggle="tooltip" data-placement="top" title="Click to Deactive" href="<?php /*echo base_url() */?>admin/staff/change_status/0/<?php /*echo $staff->staff_id; */?>"><?/*= lang('active') */?></a>
                            <?php /*else: */?>
                                <a data-toggle="tooltip" data-placement="top" title="Click to Active "  href="<?php /*echo base_url() */?>admin/staff/change_status/1/<?php /*echo $staff->staff_id; */?>"><?/*= lang('deactive') */?></a>
                            <?php /*endif; */?>
                        </td>
                        <td class="text-center">
                            <?php /*echo btn_edit('admin/staff/manage_staff/edit_staff/' . encode($staff->staff_id)); */?>
                        </td>
                    </tr>
                <?php
/*                endforeach;
                    */?>
                <?php /*endif; */?>
                </tbody>
            </table>-->
        </div>
        <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="new">
            <form role="form" id="form_v" enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/staff/save_staff/<?= (!empty($staff_info))?encrypt($staff_info->staff_id):'' ?>" method="post" class="form-horizontal">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><strong><?= lang('staff_name') ?> </strong><span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="input-sm form-control" value="<?php
                                if (!empty($staff_info)) {
                                    echo $staff_info->staff_name;
                                }
                                ?>" name="staff_name" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><strong><?= lang('staff_email') ?></strong></label>
                            <div class="col-sm-8">
                                <input type="email" class="input-sm form-control" value="<?php
                                if (!empty($staff_info)) {
                                    echo $staff_info->staff_email;
                                }
                                ?>" name="staff_email" id="email" autocomplete="off">
                                <span class="text-danger" id="email_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><strong><?= lang('staff_contact') ?> </strong></label>
                            <div class="col-sm-8">
                                <input type="number" class="input-sm form-control" value="<?php
                                if (!empty($staff_info)) {
                                    echo $staff_info->staff_contact;
                                }
                                ?>" name="staff_contact" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group pull-right">
                            <div class="col-sm-8">
                                <button class="btn btn-success" id="submit" type="submit"><?= (!empty($staff_info))?lang('update_staff'):lang('create_staff'); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    /*** CHECK EMAIL AVAILABILITY ***/
    $('#email').bind('change paste keyup', function() {
        /*$(document).on('keyup paste', "#email",function () {*/
        if ($(this).val().length > 0) {
            $(this).addClass('spinner');
            var value = $(this).val();
            var staff = '';
            <?php
            if(!empty($staff_info)){
            ?>
            staff = <?= $staff_info->staff_id ?>;
            <?php
            }
            ?>
            $.ajax({
                url : "<?php echo base_url(); ?>admin/staff/check_staff_email/"+staff,
                type : "POST",
                dataType : "json",
                data : {"email" : value},
                success : function(data) {
                    if(data){
                        var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
                        var email = document.getElementById("email");
                        if (regexEmail.test(email.value)) {
                            $('#email_error').text("");
                            $('#submit').removeAttr('disabled');
                            $('#email').removeClass('spinner');
                        } else {
                            $('#submit').attr('disabled','disabled');
                            $('#email').removeClass('spinner');
                        }
                    }
                    else {
                        $('#email_error').text("Email already exist");
                        $('#submit').attr('disabled','disabled');
                        $('#email').removeClass('spinner');
                    }
                },
                error : function(data) {
                    console.log('error');
                }
            });
        }
        else{
            $('#email_error').text("");
            $('#submit').removeAttr('disabled');
            $(this).removeClass('spinner')
        }
    });
</script>
<script>
    var data_url='<?= base_url('admin/staff/manage_staffs') ?>';
</script>
