<?= message_box('success'); ?>
<div class="nav-tabs-custom col-sm-12">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage" data-toggle="tab"><?= lang('gd_types') ?></a></li>
        <li class="<?= $active == 2 ? 'active' : ''; ?>"><a href="#new" data-toggle="tab"><?= lang('new_gd_type') ?></a></li>
    </ul>
    <div class="tab-content no-padding">
        <!-- ************** general *************-->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">
            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables">
                    <thead>
                        <tr>
                            <th ><?= lang('title') ?></th>
                            <th class="text-center"><?= lang('action') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($all_gd_type_info)) {
                            foreach ($all_gd_type_info as $v_gd_type_info) {
                                ?>
                                <tr>
                                    <td><?= $v_gd_type_info->gd_type ?></td>
                                    <td class="text-center">
                                        <?= btn_edit('admin/settings/gd_type/edit_gd_type/' . encode($v_gd_type_info->gd_type_id)) ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="new">
            <form method="post" action="<?= base_url() ?>admin/settings/gd_type/update_gd_type/<?php
            if (!empty($gd_type_info)) {
                echo encrypt($gd_type_info->gd_type_id);
            }
            ?>" class="form-horizontal">
                <div class="form-group">
                    <label class="col-lg-3 control-label"><?= lang('title') ?> <span class="text-danger">*</span></label>
                    <div class="col-lg-5">
                        <input type="text" name="gd_type"  value="<?php
                        if (!empty($gd_type_info)) {
                            echo $gd_type_info->gd_type;
                        }
                        ?>" class="form-control" placeholder="<?= lang('gd_type') ?>" required>
                    </div>
                    <div class="col-lg-2">
                        <button type="submit" class="btn btn-sm btn-primary"><?= lang('save') ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>