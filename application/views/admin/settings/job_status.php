<?= message_box('success'); ?>
<div class="nav-tabs-custom col-sm-12">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage" data-toggle="tab"><?= lang('job_status') ?></a></li>
        <li class="<?= $active == 2 ? 'active' : ''; ?>"><a href="#new" data-toggle="tab"><?= lang('new_job_status') ?></a></li>
    </ul>
    <div class="tab-content no-padding">
        <!-- ************** general *************-->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">
            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables">
                    <thead>
                        <tr>
                            <th ><?= lang('title') ?></th>
                            <th class="text-center"><?= lang('action') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($all_job_status_info)) {
                            foreach ($all_job_status_info as $v_job_status_info) {
                                ?>
                                <tr>
                                    <td><?= $v_job_status_info->job_status_title ?></td>
                                    <td class="text-center">
                                        <?= btn_edit('admin/settings/job_status/edit_job_status/' . encode($v_job_status_info->job_status_id)) ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>    
        <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="new">
            <form method="post" action="<?= base_url() ?>admin/settings/job_status/update_job_status/<?php
            if (!empty($job_status_info)) {
                echo encrypt($job_status_info->job_status_id);
            }
            ?>" class="form-horizontal">
                <div class="form-group">
                    <label class="col-lg-2 control-label"><?= lang('title') ?> <span class="text-danger">*</span></label>
                    <div class="col-lg-6">
                        <input type="text" name="job_status_title"  value="<?php
                        if (!empty($job_status_info)) {
                            echo $job_status_info->job_status_title;
                        }
                        ?>" class="form-control" placeholder="<?= lang('title') ?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-8">
                        <button type="submit" class="btn btn-sm btn-primary pull-right"><?= lang('save') ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>