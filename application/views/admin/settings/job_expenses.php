<?= message_box('success'); ?>
<div class="nav-tabs-custom col-sm-12">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage" data-toggle="tab"><?= lang('job_expenses') ?></a></li>
        <li class="<?= $active == 2 ? 'active' : ''; ?>"><a href="#new" data-toggle="tab"><?= lang('new_job_expense') ?></a></li>
    </ul>
    <div class="tab-content no-padding">
        <!-- ************** general *************-->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">
            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables">
                    <thead>
                    <tr>
                        <th ><?= lang('title') ?></th>
                        <th ><?= lang('expense') ?></th>
                        <th class="text-center"><?= lang('action') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (!empty($expenses)) {
                        foreach ($expenses as $expense) {
                            ?>
                            <tr>
                                <td><?= $expense->job_expense_title ?></td>
                                <td><?= ($expense->job_expense_type == 0)?'Receipted':'Unreceipted' ?></td>
                                <td class="text-center">
                                    <?= btn_edit('admin/settings/job_expenses/edit_expense/' . encode($expense->job_expense_id)) ?>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="new">
            <form method="post" action="<?= base_url() ?>admin/settings/job_expenses/update_expense/<?php
            if (!empty($expense_info)) {
                echo encrypt($expense_info->job_expense_id);
            }
            ?>" class="form-horizontal">
                <div class="form-group">
                    <label class="col-lg-2 control-label"><?= lang('title') ?> <span class="text-danger">*</span></label>
                    <div class="col-lg-6">
                        <input type="text" name="job_expense_title"  value="<?php
                        if (!empty($expense_info)) {
                            echo $expense_info->job_expense_title;
                        }
                        ?>" class="form-control" placeholder="<?= lang('title') ?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label"></label>
                    <div class="col-lg-6">
                        <input type="radio" name="job_expense_type" <?php
                        if (!empty($expense_info)) {
                            echo ($expense_info->job_expense_type == 0)?'checked':'';
                        }
                        ?> value="0" required> Receipted
                        <input type="radio" name="job_expense_type" <?php
                        if (!empty($expense_info)) {
                            echo ($expense_info->job_expense_type == 1)?'checked':'';
                        }
                        ?>  value="1" required> Unreceipted
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-8">
                        <button type="submit" class="btn btn-sm btn-primary pull-right"><?= lang('save') ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>