<?= message_box('success'); ?>
<div class="nav-tabs-custom col-sm-12">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage" data-toggle="tab"><?= lang('document_types') ?></a></li>
        <li class="<?= $active == 2 ? 'active' : ''; ?>"><a href="#new" data-toggle="tab"><?= lang('new_document_type') ?></a></li>
    </ul>
    <div class="tab-content no-padding">
        <!-- ************** general *************-->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">
            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables">
                    <thead>
                        <tr>
                            <th ><?= lang('document_title') ?></th>
                            <th class="text-center"><?= lang('action') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($all_document_types)) {
                            foreach ($all_document_types as $v_document_type) {
                                ?>
                                <tr>
                                    <td><?= $v_document_type->document_title ?></td>
                                    <td class="text-center">
                                        <?= btn_edit('admin/settings/document_type/edit_document_type/' . encode($v_document_type->document_type_id)) ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>    
        <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="new">
            <form method="post" action="<?= base_url() ?>admin/settings/document_type/update_document_type/<?php
            if (!empty($document_type_info)) {
                echo encrypt($document_type_info->document_type_id);
            }
            ?>" class="form-horizontal">
                <div class="form-group">
                    <label class="col-lg-2 control-label"><?= lang('document_title') ?> <span class="text-danger">*</span></label>
                    <div class="col-lg-6">
                        <input type="text" name="document_title"  value="<?php
                        if (!empty($document_type_info)) {
                            echo $document_type_info->document_title;
                        }
                        ?>" class="form-control" placeholder="<?= lang('document_title') ?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-8">
                        <button type="submit" class="btn btn-sm btn-primary pull-right"><?= lang('save') ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>