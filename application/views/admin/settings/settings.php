<div class="row">
<h1 class="header-<?= config_item('sidebar_theme'); ?>" style="margin: 0px 0px 5px 30px;"><?= lang('settings_menu') ?></h1>
    <div class="col-lg-12">

        <div class="col-md-3">

            <ul class="nav holiday_navbar">

                <li class="<?php echo ($load_setting == 'general') ? 'active' : ''; ?>">

                    <a href="<?= base_url() ?>admin/settings">

                        <i class="fa fa-fw fa-info-circle"></i>

                        <?php echo lang('company_details') ?>

                    </a>

                </li>

                <li class="<?php echo ($load_setting == 'invoice') ? 'active' : ''; ?>">

                    <a href="<?= base_url() ?>admin/settings/invoice">

                        <i class="fa fa-fw fa-money"></i>

                        Job Settings

                    </a>

                </li>
				
				<li class="<?php echo ($load_setting == 'bill') ? 'active' : ''; ?>">

                    <a href="<?= base_url() ?>admin/settings/bill">

                        <i class="fa fa-fw fa-money"></i>

                        Bill Settings

                    </a>

                </li>

                <li class="<?php echo ($load_setting == 'theme') ? 'active' : ''; ?>">

                    <a href="<?= base_url() ?>admin/settings/theme">

                        <i class="fa fa-fw fa-code"></i>

                        <?php echo lang('theme_settings') ?>

                    </a>

                </li>

                <li class="<?php echo ($load_setting == 'department') ? 'active' : ''; ?>">

                    <a href="<?= base_url() ?>admin/settings/department">

                        <i class="fa fa-fw fa-list-alt"></i>

                        <?php echo lang('department') ?>

                    </a>

                </li>

                <li class="<?php echo ($load_setting == 'job_status') ? 'active' : ''; ?>">

                    <a href="<?= base_url() ?>admin/settings/job_status">

                        <i class="fa fa-fw fa-list-alt"></i>

                        <?php echo lang('job_status') ?>

                    </a>

                </li>

                <li class="<?php echo ($load_setting == 'general_expenses') ? 'active' : ''; ?>">

                    <a href="<?= base_url() ?>admin/settings/general_expenses">

                        <i class="fa fa-fw fa-money"></i>

                        <?php echo lang('general_expenses') ?>

                    </a>

                </li>

                <li class="<?php echo ($load_setting == 'job_expenses') ? 'active' : ''; ?>">

                    <a href="<?= base_url() ?>admin/settings/job_expenses">

                        <i class="fa fa-fw fa-money"></i>

                        <?php echo lang('job_expenses') ?>

                    </a>

                </li>

                <li class="<?php echo ($load_setting == 'requisition_expenses') ? 'active' : ''; ?>">

                    <a href="<?= base_url() ?>admin/settings/requisition_expenses">

                        <i class="fa fa-fw fa-money"></i>

                        <?php echo lang('requisition_expenses') ?>

                    </a>

                </li>

                <li class="<?php echo ($load_setting == 'document_type') ? 'active' : ''; ?>">

                    <a href="<?= base_url() ?>admin/settings/document_type">

                        <i class="fa fa-fw fa-file-o"></i>

                        <?php echo lang('document_types') ?>

                    </a>

                </li>

                <li class="<?php echo ($load_setting == 'commodity_type') ? 'active' : ''; ?>">

                    <a href="<?= base_url() ?>admin/settings/commodity_type">

                        <i class="fa fa-fw fa-file-o"></i>

                        <?php echo lang('commodity_types') ?>

                    </a>

                </li>
                <li class="<?php echo ($load_setting == 'shipping_line') ? 'active' : ''; ?>">

                    <a href="<?= base_url() ?>admin/settings/shipping_line">

                        <i class="fa fa-fw fa-list-alt"></i>

                        <?php echo lang('shipping_lines') ?>

                    </a>

                </li>
                <li class="<?php echo ($load_setting == 'yard') ? 'active' : ''; ?>">

                    <a href="<?= base_url() ?>admin/settings/yard">

                        <i class="fa fa-fw fa-list-alt"></i>
    
                        <?php echo lang('yards') ?>

                    </a>

                </li>
                <li class="<?php echo ($load_setting == 'shed') ? 'active' : ''; ?>">

                    <a href="<?= base_url() ?>admin/settings/shed">

                        <i class="fa fa-fw fa-list-alt"></i>
    
                        <?php echo lang('sheds') ?>

                    </a>

                </li>
                <li class="<?php echo ($load_setting == 'incoterm') ? 'active' : ''; ?>">

                    <a href="<?= base_url() ?>admin/settings/incoterm">

                        <i class="fa fa-fw fa-list-alt"></i>
    
                        <?php echo lang('incoterms') ?>

                    </a>

                </li>
                <li class="<?php echo ($load_setting == 'weight_unit') ? 'active' : ''; ?>">

                    <a href="<?= base_url() ?>admin/settings/weight_unit">

                        <i class="fa fa-fw fa-list-alt"></i>
    
                        <?php echo lang('weight_units') ?>

                    </a>

                </li>
                <li class="<?php echo ($load_setting == 'gd_type') ? 'active' : ''; ?>">

                    <a href="<?= base_url() ?>admin/settings/gd_type">

                        <i class="fa fa-fw fa-list-alt"></i>
    
                        <?php echo lang('gd_types') ?>

                    </a>

                </li>

            </ul>

        </div>



        <section class="col-sm-9">

            <div class="col-sm-8  ">



                <?php if ($load_setting == 'email') { ?>

                    <div style="margin-bottom: 10px;margin-left: -15px" class="<?php

                    if ($load_setting != 'email') {

                        echo 'hidden';

                    }

                    ?>">

                        <a href="<?= base_url() ?>admin/settings/email&view=alerts" class="btn btn-info"><i class="fa fa fa-inbox text"></i>

                            <span class="text"><?php echo lang('alert_settings') ?></span>

                        </a>

                    </div>

                <?php } ?>



            </div>

            <section class="">

                <!-- Load the settings form in views -->

                <?= $this->load->view('admin/settings/' . $load_setting, '', TRUE) ?>

                <!-- End of settings Form -->

            </section>

        </section>

    </div>

</div>

