<div class="row">
    <!-- Start Form -->
    <form action="<?php echo base_url() ?>admin/settings/save_bill" enctype="multipart/form-data" class="form-horizontal" method="post">
        <div class="panel panel-default">
            <header class="panel-heading  "><?= lang('bill_settings') ?></header>
            <div class="panel-body">
                <input type="hidden" name="settings" value="<?= $load_setting ?>">
                <div class="form-group">
                    <label class="col-lg-2 control-label"><?= lang('invoice_logo') ?></label>
                    <div class="col-lg-7" >
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 210px;" >
                                <?php if (config_item('invoice_logo') != '') : ?>
                                    <img src="<?php echo base_url() . config_item('invoice_logo'); ?>" >
                                <?php else: ?>
                                    <img src="http://placehold.it/350x260" alt="Please Connect Your Internet">
                                <?php endif; ?>
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="width: 210px;" ></div>
                            <div>
                        <span class="btn btn-default btn-file">
                        <span class="fileinput-new">
                        <input type="file" name="invoice_logo"  value="upload"  data-buttonText="<?= lang('choose_file') ?>" id="myImg" />
                        <span class="fileinput-exists"><?= lang('change') ?></span>
                        </span>
                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput"><?= lang('remove') ?></a>
                            </div>
                            <div id="valid_msg" style="color: #e11221"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group terms">
                    <label class="col-lg-2 control-label"><?= lang('default_terms') ?></label>
                    <div class="col-lg-9">
                        <textarea class="form-control textarea" name="default_terms"><?= config_item('default_terms') ?></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-3 control-label"></div>
                <div class="col-lg-6">
                    <button type="submit" class="btn btn-sm btn-primary"><?= lang('save_changes') ?></button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- End Form -->
<script>
    /*$('input[type=checkbox][name=job_number_auto]').change(function() {
        if($(this).is(':checked')){
            $('.job_auto_field').removeAttrs('disabled','disabled');
        }else{
            $('.job_auto_field').attr('disabled',true);
        }
    });*/
    $('input[type=checkbox][name=job_number_auto]').change(function() {
        if($(this).is(':checked')){
            $('.job_auto_field').show();
        }else{
            $('.job_auto_field').hide();
        }
    });

    /*** OTHER SECTION ***/

    $("#add_other").click(function () {

    var columns = '<div class="form-group">';
        columns += '<label class="col-lg-2 control-label">Job Type<span class="text-danger">*</span></label>';
        columns += '<div class="col-lg-2">';
        columns += '<input type="text"  name="job_type[]" class="form-control">';
        columns += '</div>';
        columns += '<label class="col-lg-1 control-label">Prefix <span class="text-danger">*</span></label>';
        columns += '<div class="col-lg-1">';
        columns += '<input type="text"  name="job_prefix[]" class="form-control ">';
        columns += '</div>';
        columns += '<label class="col-lg-1 control-label"><?= lang('separator') ?> <span class="text-danger">*</span></label>';
        columns += '<div class="col-lg-1">';
        columns += '<input type="text"  name="job_separator[]" class="form-control">';
        columns += '</div>';
        columns += '<label class="col-lg-1 control-label"><?= lang('invoice_start') ?> <span class="text-danger">*</span></label>';
        columns += '<div class="col-lg-2">';
        columns += '<input type="text"  name="job_start[]" class="form-control">';
        columns += '</div>';
        columns += '<div class="col-lg-1">';
        columns += '<button type="button" class="btn btn-danger btn-xs remCF" id="add_other" style="margin-top: 5px;"><i class="fa fa-minus"></i></button>';
        columns += '</div>';
        columns += '</div>';
        var add_new = $(columns);

        $("#add_new_other").append(add_new.hide().fadeIn(1000));

    });


    $("#add_new_other").on('click', '.remCF', function () {

        $(this).fadeOut(300, function () {
            $(this).parent().parent().remove();
        })

    });

    /*** END OTHER SECTION ***/
</script>