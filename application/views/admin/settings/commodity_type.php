<?= message_box('success'); ?>
<div class="nav-tabs-custom col-sm-12">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage" data-toggle="tab"><?= lang('commodity_types') ?></a></li>
        <li class="<?= $active == 2 ? 'active' : ''; ?>"><a href="#new" data-toggle="tab"><?= lang('new_commodity_type') ?></a></li>
    </ul>
    <div class="tab-content no-padding">
        <!-- ************** general *************-->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">
            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables">
                    <thead>
                        <tr>
                            <th ><?= lang('commodity_title') ?></th>
                            <th class="text-center"><?= lang('action') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($all_commodity_types)) {
                            foreach ($all_commodity_types as $v_commodity_type) {
                                ?>
                                <tr>
                                    <td><?= $v_commodity_type->commodity_type ?></td>
                                    <td class="text-center">
                                        <?= btn_edit('admin/settings/commodity_type/edit_commodity_type/' . encode($v_commodity_type->commodity_type_id)) ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>    
        <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="new">
            <form method="post" action="<?= base_url() ?>admin/settings/commodity_type/update_commodity_type/<?php
            if (!empty($commodity_type_info)) {
                echo encrypt($commodity_type_info->commodity_type_id);
            }
            ?>" class="form-horizontal">
                <div class="form-group">
                    <label class="col-lg-2 control-label"><?= lang('commodity_type') ?> <span class="text-danger">*</span></label>
                    <div class="col-lg-6">
                        <input type="text" name="commodity_type"  value="<?php
                        if (!empty($commodity_type_info)) {
                            echo $commodity_type_info->commodity_type;
                        }
                        ?>" class="form-control" placeholder="<?= lang('commodity_type') ?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-8">
                        <button type="submit" class="btn btn-sm btn-primary pull-right"><?= lang('save') ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>