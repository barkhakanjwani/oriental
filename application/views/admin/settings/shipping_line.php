<?= message_box('success'); ?>
<div class="nav-tabs-custom col-sm-12">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage" data-toggle="tab"><?= lang('shipping_lines') ?></a></li>
        <li class="<?= $active == 2 ? 'active' : ''; ?>"><a href="#new" data-toggle="tab"><?= lang('new_shipping_line') ?></a></li>
    </ul>
    <div class="tab-content no-padding">
        <!-- ************** general *************-->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">
            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables">
                    <thead>
                        <tr>
                            <th ><?= lang('name') ?></th>
                            <th class="text-center"><?= lang('action') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($all_shipping_info)) {
                            foreach ($all_shipping_info as $v_shipping_info) {
                                ?>
                                <tr>
                                    <td><?= $v_shipping_info->shipping_name ?></td>
                                    <td class="text-center">
                                        <?= btn_edit('admin/settings/shipping_line/edit_shipping/' . encode($v_shipping_info->shipping_id)) ?>
                                        <?php /*btn_delete('admin/settings/delete_shipping/' . encrypt($v_shipping_info->shipping_id)) */?>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="new">
            <form method="post" action="<?= base_url() ?>admin/settings/shipping_line/update_shipping/<?php
            if (!empty($shipping_info)) {
                echo encrypt($shipping_info->shipping_id);
            }
            ?>" class="form-horizontal">
                <div class="form-group">
                    <label class="col-lg-3 control-label"><?= lang('name') ?> <span class="text-danger">*</span></label>
                    <div class="col-lg-5">
                        <input type="text" name="shipping_name"  value="<?php
                        if (!empty($shipping_info)) {
                            echo $shipping_info->shipping_name;
                        }
                        ?>" class="form-control" placeholder="<?= lang('name') ?>" required>
                    </div>
                    <div class="col-lg-2">
                        <button type="submit" class="btn btn-sm btn-primary"><?= lang('save') ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>