<div class="row">
    <!-- Start Form -->
    <form action="<?php echo base_url() ?>admin/settings/save_invoice" enctype="multipart/form-data" class="form-horizontal" method="post">
        <div class="panel panel-default">
            <header class="panel-heading  "><?= lang('job_settings') ?></header>
            <div class="panel-body">
                <input type="hidden" name="settings" value="<?= $load_setting ?>">
                <div class="form-group">
                    <label class="col-lg-2 control-label">Job Number Auto</label>
                    <div class="col-lg-3">
                        <input type="checkbox" name="job_number_auto" value="Yes" data-style="ios" data-toggle="toggle" data-on=" " data-off=" " data-onstyle="success" data-offstyle="default" <?= (config_item('job_number_auto') == 'Yes')?'checked':'' ?>  <?php
                            if(!empty($invoices)){
                                echo 'disabled';
                            }else{
                                echo (config_item('job_number_auto') == 'Yes')?'checked':'';
                            } ?> >
                    </div>
                </div>
                <?php
                if(!empty($job_info)) {
                    $counter = 0;
                    foreach ($job_info as $job) {
                ?>
                <div class="form-group job_auto_field">
                    <label class="col-lg-2 control-label">Job Type <span class="text-danger">*</span></label>
                    <div class="col-lg-2">
                        <input type="hidden" name="job_id[]" value="<?= $job->Iid ?>">
                        <input type="text" name="job_type_update[]" class="form-control job_auto_field"
                               value="<?= $job->VCJobType ?>" <?php
                        if (!empty($invoices)) {
                            echo 'readonly';
                        } else {
                            if(config_item('job_number_auto') == 'Yes'){
                                echo ($job->VCJobType == 'import' || $job->VCJobType == 'export')?'readonly':'';
                            }else{
                                echo 'readonly';
                            }
                        } ?>>
                    </div>
                    <label class="col-lg-1 control-label">Prefix <span class="text-danger">*</span></label>
                    <div class="col-lg-1">
                        <input type="text" name="job_prefix_update[]" class="form-control job_auto_field"
                               value="<?= $job->VCPrefix ?>" <?php
                        if (!empty($invoices)) {
                            echo 'readonly';
                        } else {
                            echo (config_item('job_number_auto') == 'Yes') ? '' : 'readonly';
                        } ?> >
                    </div>
                    <label class="col-lg-1 control-label"><?= lang('separator') ?> <span
                                class="text-danger">*</span></label>
                    <div class="col-lg-1">
                        <input type="text" name="job_separator_update[]" class="form-control job_auto_field"
                               value="<?= $job->VCSeparator ?>" <?php
                        if (!empty($invoices)) {
                            echo 'readonly';
                        } else {
                            echo (config_item('job_number_auto') == 'Yes') ? '' : 'readonly';
                        } ?> >
                    </div>
                    <label class="col-lg-1 control-label"><?= lang('invoice_start') ?> <span
                                class="text-danger">*</span></label>
                    <div class="col-lg-2">
                        <input type="text" name="job_start_update[]" class="form-control job_auto_field"
                               value="<?= $job->VCStartFrom ?>" <?php
                        if (!empty($invoices)) {
                            echo 'readonly';
                        } else {
                            echo (config_item('job_number_auto') == 'Yes') ? '' : 'readonly';
                        } ?> >
                    </div>
                    <?php
                        if (empty($invoices)) {
                            if ($counter == 0) {
                                ?>
                                <div>
                                    <button type="button" class="btn btn-primary btn-xs" id="add_other"
                                            style="margin-top: 5px;margin-left: 15px;"><i class="fa fa-plus"></i>
                                    </button>
                                </div>
                                <?php
                            } else {
                                if ($job->VCJobType != 'export') {
                                    ?>

                                    <div>
                                        <a href="<?= base_url('admin/settings/delete_job_type/'.$job->Iid)?>" style="margin-top: 5px;margin-left: 15px;" class="btn btn-danger btn-xs"><i class="fa fa-minus"></i></a>
                                    </div>
                                    <?php
                                }
                            }
                        }
                    ?>
                </div>
                <?php
                        $counter++;
                }
                ?>
                    <div id="add_new_other"></div>
                    <!--<div class="form-group">
                        <div class="col-lg-11"></div>
                    <div class="col-lg-1">
                        <button type="button" class="btn btn-primary btn-xs" id="add_other"
                                style="margin-top: 5px;"><i class="fa fa-plus"></i></button>
                    </div>
                    <div id="add_new_other"></div>
                    </div>-->
                    <?php
                    }
                    ?>
            </div>

            <?php
                        if (empty($invoices)) {
                            ?>
                            <div class="form-group">
                                <div class="col-lg-3 control-label"></div>
                                <div class="col-lg-6">
                                    <button type="submit"
                                            class="btn btn-sm btn-primary"><?= lang('save_changes') ?></button>
                                </div>
                            </div>
                            <?php
                        }
            ?>
        </div>
    </form>
</div>
<!-- End Form -->
<script>
    /*$('input[type=checkbox][name=job_number_auto]').change(function() {
        if($(this).is(':checked')){
            $('.job_auto_field').removeAttrs('disabled','disabled');
        }else{
            $('.job_auto_field').attr('disabled',true);
        }
    });*/
    $('input[type=checkbox][name=job_number_auto]').change(function() {
        if($(this).is(':checked')){
            $('.job_auto_field').show();
        }else{
            $('.job_auto_field').hide();
        }
    });

    /*** OTHER SECTION ***/

    $("#add_other").click(function () {

    var columns = '<div class="form-group">';
        columns += '<label class="col-lg-2 control-label">Job Type<span class="text-danger">*</span></label>';
        columns += '<div class="col-lg-2">';
        columns += '<input type="text"  name="job_type[]" class="form-control">';
        columns += '</div>';
        columns += '<label class="col-lg-1 control-label">Prefix <span class="text-danger">*</span></label>';
        columns += '<div class="col-lg-1">';
        columns += '<input type="text"  name="job_prefix[]" class="form-control ">';
        columns += '</div>';
        columns += '<label class="col-lg-1 control-label"><?= lang('separator') ?> <span class="text-danger">*</span></label>';
        columns += '<div class="col-lg-1">';
        columns += '<input type="text"  name="job_separator[]" class="form-control">';
        columns += '</div>';
        columns += '<label class="col-lg-1 control-label"><?= lang('invoice_start') ?> <span class="text-danger">*</span></label>';
        columns += '<div class="col-lg-2">';
        columns += '<input type="text"  name="job_start[]" class="form-control">';
        columns += '</div>';
        columns += '<div class="col-lg-1">';
        columns += '<button type="button" class="btn btn-danger btn-xs remCF" id="add_other" style="margin-top: 5px;"><i class="fa fa-minus"></i></button>';
        columns += '</div>';
        columns += '</div>';
        var add_new = $(columns);

        $("#add_new_other").append(add_new.hide().fadeIn(1000));

    });


    $("#add_new_other").on('click', '.remCF', function () {

        $(this).fadeOut(300, function () {
            $(this).parent().parent().remove();
        })

    });

    /*** END OTHER SECTION ***/
</script>