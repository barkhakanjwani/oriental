<?= message_box('success'); ?>
<div class="nav-tabs-custom col-sm-12">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage" data-toggle="tab"><?= lang('exchange_rate') ?></a></li>
        <li class="<?= $active == 2 ? 'active' : ''; ?>"><a href="#new" data-toggle="tab"><?= lang('new_exchange_rate') ?></a></li>
    </ul>
    <div class="tab-content no-padding">
        <!-- ************** general *************-->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">
            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables">
                    <thead>
                        <tr>
                            <th ><?= lang('currency') ?></th>
                            <th ><?= lang('exchange_rate') ?></th>
                            <th ><?= lang('action') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($all_exchange_rate_info)) {
                            foreach ($all_exchange_rate_info as $v_exchange_rate_info) {
                                ?>
                                <tr>
                                    <td><?= $v_exchange_rate_info->exchange_rate_currency ?></td>
                                    <td><?= $v_exchange_rate_info->exchange_rate ?></td>
                                    <td>
                                        <?= btn_edit('admin/settings/exchange_rate/edit_exchange_rate/' . $v_exchange_rate_info->exchange_rate_id) ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>    
        <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="new">
            <form method="post" action="<?= base_url() ?>admin/settings/exchange_rate/update_exchange_rate/<?php
            if (!empty($exchange_rate_info)) {
                echo $exchange_rate_info->exchange_rate_id;
            }
            ?>" class="form-horizontal"> 
                <div class="form-group">
                    <label class="col-lg-3 control-label"><?= lang('currency') ?> <span class="text-danger">*</span></label>
                    <div class="col-lg-5">
                        <select class="form-control select_box" style="width: 100%"  name="exchange_rate_currency"  required>
                            <option value="">Choose Currency</option>
                            <?php
                            if (!empty($all_currencies)) {
                                foreach ($all_currencies as $v_currency) {
                                    ?>
                                    <option value="<?= $v_currency->code ?>"
                                        <?php
                                        if (!empty($exchange_rate_info)) {
                                            echo ($v_currency->code == $exchange_rate_info->exchange_rate_currency)?"selected":'';
                                        }
                                        ?>
                                    ><?= $v_currency->name ?> (<?= $v_currency->code ?>)</option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><?= lang('exchange_rate') ?> <span class="text-danger">*</span></label>
                    <div class="col-lg-5">
                        <input type="text" name="exchange_rate"  value="<?php
                        if (!empty($exchange_rate_info)) {
                            echo $exchange_rate_info->exchange_rate;
                        }
                        ?>" class="form-control" placeholder="<?= lang('exchange_rate') ?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-8">
                        <button type="submit" class="btn btn-sm btn-primary pull-right"><?= lang('save') ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>