<?= message_box('success'); ?>
<div class="nav-tabs-custom col-sm-12">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage" data-toggle="tab"><?= lang('yards') ?></a></li>
        <li class="<?= $active == 2 ? 'active' : ''; ?>"><a href="#new" data-toggle="tab"><?= lang('new_yard') ?></a></li>
    </ul>
    <div class="tab-content no-padding">
        <!-- ************** general *************-->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">
            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables">
                    <thead>
                        <tr>
                            <th ><?= lang('title') ?></th>
                            <th class="text-center"><?= lang('action') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($all_yard_info)) {
                            foreach ($all_yard_info as $v_yard_info) {
                                ?>
                                <tr>
                                    <td><?= $v_yard_info->yard ?></td>
                                    <td class="text-center">
                                        <?= btn_edit('admin/settings/yard/edit_yard/' . encode($v_yard_info->yard_id)) ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="new">
            <form method="post" action="<?= base_url() ?>admin/settings/yard/update_yard/<?php
            if (!empty($yard_info)) {
                echo encrypt($yard_info->yard_id);
            }
            ?>" class="form-horizontal">
                <div class="form-group">
                    <label class="col-lg-3 control-label"><?= lang('title') ?> <span class="text-danger">*</span></label>
                    <div class="col-lg-5">
                        <input type="text" name="yard"  value="<?php
                        if (!empty($yard_info)) {
                            echo $yard_info->yard;
                        }
                        ?>" class="form-control" placeholder="<?= lang('yard') ?>" required>
                    </div>
                    <div class="col-lg-2">
                        <button type="submit" class="btn btn-sm btn-primary"><?= lang('save') ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>