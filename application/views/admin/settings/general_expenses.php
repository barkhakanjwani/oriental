<?= message_box('success'); ?>
<div class="nav-tabs-custom col-sm-12">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage" data-toggle="tab"><?= lang('general_expenses') ?></a></li>
        <li class="<?= $active == 2 ? 'active' : ''; ?>"><a href="#new" data-toggle="tab"><?= lang('new_expense') ?></a></li>
    </ul>
    <div class="tab-content no-padding">
        <!-- ************** general *************-->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">
            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables">
                    <thead>
                    <tr>
                        <th ><?= lang('title') ?></th>
                        <th class="text-center"><?= lang('action') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (!empty($expenses)) {
                        foreach ($expenses as $expense) {
                            ?>
                            <tr>
                                <td><?= $expense->A_NAME ?></td>
                                <td class="text-center">
                                    <?= btn_edit('admin/settings/general_expenses/edit_expense/' . encode($expense->A_ID)) ?>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="new">
            <form method="post" action="<?= base_url() ?>admin/settings/general_expenses/update_expense/<?php
            if (!empty($expense_info)) {
                echo encrypt($expense_info->A_ID);
            }
            ?>" class="form-horizontal">
                <div class="form-group">
                    <label class="col-lg-2 control-label"><?= lang('title') ?> <span class="text-danger">*</span></label>
                    <div class="col-lg-6">
                        <input type="hidden" value="<?= $head_id ?>" name="head_id" />
                        <input type="text" name="h_name"  value="<?php
                        if (!empty($expense_info)) {
                            echo $expense_info->A_NAME;
                        }
                        ?>" class="form-control" placeholder="<?= lang('title') ?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-8">
                        <button type="submit" class="btn btn-sm btn-primary pull-right"><?= lang('save') ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>