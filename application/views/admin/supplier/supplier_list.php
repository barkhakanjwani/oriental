<?= message_box('success'); ?>
<?= message_box('error'); ?>
<h1 class="header-<?= config_item('sidebar_theme'); ?>"><?= lang('manage_suppliers') ?></h1>
<div class="nav-tabs-custom">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage" data-toggle="tab"><?= lang('all_suppliers') ?></a></li>
        <li class="<?= $active == 2 ? 'active' : ''; ?>"><a href="#new" data-toggle="tab"><?= lang('new_supplier') ?></a></li>
    </ul>
    <div class="tab-content no-padding">
        <!-- ************** general *************-->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">
                <div class="table-responsive" id="customers">
                    <table class="table table-bordered data-table-list dtHorizontalExample" id="tab_customers">
                        <thead>
                        <th><?= lang('created_date') ?></th>
                        <th><?= lang('name') ?></th>
                        <th><?= lang('email') ?></th>
                        <th><?= lang('contact') ?></th>
                        <th><?= lang('status') ?></th>
                        <th><?= lang('action') ?></th>
                        </thead>
                    </table>
                </div>

            <!--<table class="table table-striped" id="DataTables">
                <thead>
                <tr>
                    <th><?/*= lang('created_date') */?></th>
                    <th><?/*= lang('name') */?></th>
                    <th><?/*= lang('email') */?></th>
                    <th><?/*= lang('contact') */?></th>
                    <th><?/*= lang('status') */?></th>
                    <th class="text-center"><?/*= lang('action') */?></th>
                </tr>
                </thead>
                <tbody>
                <?php
/*                    if (!empty($all_suppliers)): foreach ($all_suppliers as $supplier) :
                        */?>
						<tr>
                        <td><?/*= strftime(config_item('date_format'), strtotime($supplier->supplier_date)); */?></td>
                        <td><?php /*echo ucfirst($supplier->supplier_name) */?></td>
                        <td><?/*= $supplier->supplier_email */?></td>
                        <td><?/*= $supplier->supplier_contact */?></td>
                        <td>
                            <?php /*if ($supplier->supplier_status == 1): */?>
								<a data-toggle="tooltip" data-placement="top" title="Click to Deactive" href="<?php /*echo base_url() */?>admin/supplier/change_status/0/<?php /*echo encrypt($supplier->supplier_id); */?>"><?/*= lang('active') */?></a>
                            <?php /*else: */?>
								<a data-toggle="tooltip" data-placement="top" title="Click to Active "  href="<?php /*echo base_url() */?>admin/supplier/change_status/1/<?php /*echo encrypt($supplier->supplier_id); */?>"><?/*= lang('deactive') */?></a>
                            <?php /*endif; */?>
                        </td>
                        <td class="text-center">
                            <?php /*echo btn_edit('admin/supplier/manage_supplier/edit_supplier/' . encode($supplier->supplier_id)); */?>
                        </td>
                    </tr>
                    <?php
/*                    endforeach;
                        */?>
                    <?php /*endif; */?>
                </tbody>
            </table>-->
        </div>



        <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="new">
                <form role="form" enctype="multipart/form-data" id="form_v" action="<?php echo base_url(); ?>admin/supplier/save_supplier/<?= (!empty($supplier_info))?encrypt($supplier_info->supplier_id):'' ?>" method="post" class="form-horizontal  ">
				<div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><strong><?= lang('supplier_name') ?> </strong><span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="input-sm form-control" value="<?php
                                    if (!empty($supplier_info)) {
                                        echo $supplier_info->supplier_name;
                                    }
                                ?>" name="supplier_name" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><strong><?= lang('supplier_email') ?> </strong></label>
                            <div class="col-sm-8">
                                <input type="email" class="input-sm form-control" value="<?php
                                    if (!empty($supplier_info)) {
                                        echo $supplier_info->supplier_email;
                                    }
                                ?>" name="supplier_email" id="email" autocomplete="off">
                                <span class="text-danger" id="email_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><strong><?= lang('supplier_contact') ?> </strong></label>
                            <div class="col-sm-8">
                                <input type="number" class="input-sm form-control" value="<?php
                                    if (!empty($supplier_info)) {
                                        echo $supplier_info->supplier_contact;
                                    }
                                ?>" name="supplier_contact" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group pull-right">
                            <div class="col-sm-8">
                                <button class="btn btn-success" id="submit" type="submit"><?= (!empty($supplier_info))?lang('update_supplier'):lang('create_supplier'); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    /*** CHECK EMAIL AVAILABILITY ***/
    $('#email').bind('change paste keyup', function() {
        /*$(document).on('keyup paste', "#email",function () {*/
        if ($(this).val().length > 0) {
            $(this).addClass('spinner');
            var value = $(this).val();
            var supplier = '';
            <?php
            if(!empty($supplier_info)){
            ?>
            supplier = '<?= encrypt($supplier->supplier_id) ?>';
            <?php
            }
            ?>
            $.ajax({
                url : "<?php echo base_url(); ?>admin/supplier/check_supplier_email/"+supplier,
                type : "POST",
                dataType : "json",
                data : {"email" : value},
                success : function(data) {
                    if(data){
                        var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
                        var email = document.getElementById("email");
                        if (regexEmail.test(email.value)) {
                            $('#email_error').text("");
                            $('#submit').removeAttr('disabled');
                            $('#email').removeClass('spinner');
                        } else {
                            $('#submit').attr('disabled','disabled');
                            $('#email').removeClass('spinner');
                        }
                    }
                    else {
                        $('#email_error').text("Email already exist");
                        $('#submit').attr('disabled','disabled');
                        $('#email').removeClass('spinner');
                    }
                },
                error : function(data) {
                    console.log('error');
                }
            });
        }
        else{
            $('#email_error').text("");
            $('#submit').removeAttr('disabled');
            $(this).removeClass('spinner')
        }
    });
</script>

<script>
    var data_url='<?= base_url('admin/supplier/manage_suppliers') ?>';
</script>