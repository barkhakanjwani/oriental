<link href="<?php echo base_url() ?>asset/css/fullcalendar.css" rel="stylesheet" type="text/css" >
<style type="text/css">
    .datepicker{z-index:1151 !important;}
</style>
<?php
    echo message_box('success');
    echo message_box('error');
    $curency = $this->admin_model->check_by(array('code' => config_item('currency')), 'tbl_currencies');
?>
<div class="dashboard row" >
    <div class="container-fluid">
        <div class="row">
            <div class="clearfix visible-sm-block"></div>
            <div class="col-sm-12">
                <div class="col-md-4">
                    <div class="info-box">
                        <span class="info-box-icon bg-sucees"><i class="fa fa-users"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Users</span>
                            <span class="info-box-number"><?= count($this->db->get('tbl_users')->result()); ?> </span>
                            <a href = "<?php base_url();?>user/user_list" class = "small-box-footer">More info <i class = "fa fa-arrow-circle-right"></i></a>
                        </div><!--/.info-box-content -->
                    </div><!--/.info-box -->
                </div>
                <div class = "col-md-4">
                    <div class = "info-box">
                        <span class = "info-box-icon bg-yellow"><i class="fa fa-user"></i></span>
                        <div class = "info-box-content">
                            <span class = "info-box-text">Clients</span>
                            <span class="info-box-number"><?= count($this->db->get('tbl_client')->result()); ?> </span></span>
							<a href="<?php base_url();?>client/manage_client" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->
                </div>
                <div class="col-md-4">
                    <div class="info-box">
                        <span class="info-box-icon bg-blue"><i class="fa fa-briefcase"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Jobs</span>
                            <span class="info-box-number"><?= count($this->db->get('tbl_invoices')->result()); ?> </span></span>
							<a href="<?php base_url();?>invoice/all_invoices" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-md-4">
                    <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="fa fa-file-text-o"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Payment Receipt</span>
                            <span class="info-box-number"><?= count($this->db->get('tbl_advance_payments')->result()); ?> </span>
                            <a href = "<?php base_url();?>accounts/manage_receipt_voucher" class = "small-box-footer">Add Receipt <i class = "fa fa-arrow-circle-right"></i></a>
                        </div><!--/.info-box-content -->
                    </div><!--/.info-box -->
                </div>
                <div class = "col-md-4">
                    <div class = "info-box">
                        <span class = "info-box-icon bg-green"><i class="fa fa-credit-card"></i></span>
                        <div class = "info-box-content">
                            <span class = "info-box-text">Payment Voucher</span>
                            <span class="info-box-number"><?= count($this->db->get('payment_vouchers')->result()); ?> </span></span>
                            <a href="<?php base_url();?>accounts/manage_payment_voucher" class="small-box-footer">Add payment <i class="fa fa-arrow-circle-right"></i></a>
                        </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->
                </div>
                <div class="col-md-4">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="fa fa-newspaper-o"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Job Expenses</span>
                            <span class="info-box-number"><?= count($this->db->get('tbl_vouchers')->result()); ?> </span></span>
                            <a href="<?php base_url();?>accounts/manage_job_expense" class="small-box-footer">Add Expense <i class="fa fa-arrow-circle-right"></i></a>
                        </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->
                </div>
            </div>
            <div class="col-sm-6" style="margin-top: 20px;">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background: #D8D8D8;border: 1px solid #D8D8D8"></div>
                    <div id="calendar"></div>
                </div>
            </div>
            <div class="col-md-6" style="margin-top: 20px;">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h4 class="box-title">Job Statuses</h4>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body" style="padding: 41px 0;">
                        <div class="text-center">
                        </div>
						<!--<canvas id="yearly_report" class="col-sm-12" style="height: 440px; padding-top: 35px;"></canvas>-->
                        <canvas id="pieChart" style="height:250px; margin: 57px 1px;"></canvas>
                    </div><!-- ./box-body -->
                </div>
            </div>
			<!--- FINISH JOBS --->
            <div class="col-md-8" style="margin-top: 20px;">
                <section class="box">
                    <header class="box-header with-border">
                        <h3 class="box-title">Jobs Status</h3>
                    </header>
                    <aside class="nav-tabs-custom box-header">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#unfinish" data-toggle="tab">Unfinish Jobs</a></li>
                            <li><a href="#finish" data-toggle="tab">Finish Jobs</a></li>
                        </ul>
                        <section class="scrollable box-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="unfinish">
                                    <table class="table table-striped m-b-none text-sm">
                                        <thead>
                                        <tr>
                                            <th><?= lang('job_no') ?></th>
                                            <th><?= lang('client_name') ?></th>
                                            <th><?= lang('due_amount') ?></th>
                                            <th><?= lang('paid_amount') ?></th>
                                            <th><?= lang('status') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $this->db->select('*')->from('tbl_invoices');
                                            $this->db->or_where('(`consignment_type = "FCL") AND (`invoices_id` NOT IN (SELECT `invoices_id` FROM `tbl_security_deposit` WHERE `deposit_return`= "Yes"))');
                                            $this->db->or_where('(`consignment_type = "LCL") AND (`invoices_id` NOT IN (SELECT `invoices_id` FROM `tbl_bills`))');
                                            $all_invoices_info = $this->db->get()->result();
                                            if (!empty($all_invoices_info)) {
                                                foreach ($all_invoices_info as $v_invoices) {
                                                    if($this->invoice_model->get_payment_status($v_invoices->invoices_id) == lang('fully_paid')){
                                                        $invoice_status = lang('fully_paid');
                                                        $label = "success";
                                                    }
													elseif($this->invoice_model->get_payment_status($v_invoices->invoices_id) == lang('partially_paid')){
                                                        $invoice_status = lang('partially_paid');
                                                        $label = "info";
                                                    }
                                                    else{
                                                        $invoice_status = lang('not_paid');
                                                        $label = "danger";
                                                    }
                                                    ?>
													<tr>
                                                    <td><a class="text-info" href="<?= base_url() ?>admin/invoice/manage_invoice/invoice_details/<?= encode($v_invoices->invoices_id) ?>"><?= $this->invoice_model->job_no_creation($v_invoices->invoices_id) ?></a></td>
                                                        <?php
                                                            $client_info = $this->invoice_model->check_by(array('client_id' => $v_invoices->client_id), 'tbl_client');
                                                        ?>
														<td><?= $client_info->name; ?></td>
                                                    <td>PKR <?= number_format($this->invoice_model->calculate_to('invoice_due', $v_invoices->invoices_id), 2) ?></td>
                                                    <td>PKR <?= number_format($this->invoice_model->calculate_to('paid_amount', $v_invoices->invoices_id), 2) ?></td>
                                                    <td>
                                                        <span class="label label-<?= $label ?>"><?= $invoice_status ?></span>
                                                    </td>
                                                </tr>
                                                    <?php
                                                }
                                            } else {
                                                ?>
												<tr>
                                                <td colspan="5"><?= lang('nothing_to_display') ?></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="finish">
                                    <table class="table table-striped m-b-none text-sm">
                                        <thead>
                                        <tr>
                                            <th><?= lang('job_no') ?></th>
                                            <th><?= lang('client_name') ?></th>
                                            <th><?= lang('due_amount') ?></th>
                                            <th><?= lang('paid_amount') ?></th>
                                            <th><?= lang('status') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $this->db->select('*')->from('tbl_invoices');
                                            $this->db->or_where('(`consignment_type = "FCL") AND (`invoices_id` IN (SELECT `invoices_id` FROM `tbl_security_deposit` WHERE `deposit_return`= "Yes"))');
                                            $this->db->or_where('(`consignment_type = "LCL") AND (`invoices_id` IN (SELECT `invoices_id` FROM `tbl_bills`))');
                                            $all_invoices_info = $this->db->get()->result();
                                            if (!empty($all_invoices_info)) {
                                                foreach ($all_invoices_info as $v_invoices) {
                                                    if($this->invoice_model->get_payment_status($v_invoices->invoices_id) == lang('fully_paid')){
                                                        $invoice_status = lang('fully_paid');
                                                        $label = "success";
                                                    }
													elseif($this->invoice_model->get_payment_status($v_invoices->invoices_id) == lang('partially_paid')){
                                                        $invoice_status = lang('partially_paid');
                                                        $label = "info";
                                                    }
                                                    else{
                                                        $invoice_status = lang('not_paid');
                                                        $label = "danger";
                                                    }
                                                    ?>
													<tr>
                                                    <td><a class="text-info" href="<?= base_url() ?>admin/invoice/manage_invoice/invoice_details/<?= encode($v_invoices->invoices_id) ?>"><?= $this->invoice_model->job_no_creation($v_invoices->invoices_id) ?></a></td>
                                                        <?php
                                                            $client_info = $this->invoice_model->check_by(array('client_id' => $v_invoices->client_id), 'tbl_client');
                                                        ?>
														<td><?= $client_info->name; ?></td>
                                                    <td>PKR <?= number_format($this->invoice_model->calculate_to('invoice_due', $v_invoices->invoices_id), 2) ?></td>
                                                    <td>PKR <?= number_format($this->invoice_model->calculate_to('paid_amount', $v_invoices->invoices_id), 2) ?></td>
                                                    <td>
                                                        <span class="label label-<?= $label ?>"><?= $invoice_status ?></span>
                                                    </td>
                                                </tr>
                                                    <?php
                                                }
                                            } else {
                                                ?>
												<tr>
                                                <td colspan="5"><?= lang('nothing_to_display') ?></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </aside>
                </section>
            </div>
            <div class="wrap-fpanel col-sm-4" style="margin-top: 20px;">
                <section class="box box-primary">
                    <header class="box-header with-border">
                        <h3 class="box-title"><?= lang('recently_paid_invoices') ?></h3>
                    </header>
                    <div class="panel-body inv-slim-scroll">
                        <div class="list-group bg-white" >
                            <?php
                                $total_receipt = 0;
                                $recently_paid = $this->db
                                    ->order_by('created_date', 'desc')
                                    ->get('tbl_payments', 5)
                                    ->result();
                                if (!empty($recently_paid)) {
                                    foreach ($recently_paid as $key => $v_paid) {
                                        $invoices_info = $this->db->where(array('invoices_id' => $v_paid->invoices_id))->get('tbl_invoices')->row();
                                        if ($v_paid->payment_method == '1') {
                                            $label = 'success';
                                        } elseif ($v_paid->payment_method == '2') {
                                            $label = 'danger';
                                        } else {
                                            $label = 'dark';
                                        }
                                        $total_receipt += $v_paid->amount;
                                        ?>
										<a href="<?= base_url() ?>admin/invoice/manage_invoice/invoice_details/<?= encode($v_paid->invoices_id) ?>" class="list-group-item">
                                        <?= $this->invoice_model->job_no_creation($invoices_info->invoices_id) ?> - <small class="text-muted">PKR <?= $v_paid->amount ?> <span class="label label-<?= $label ?> pull-right"><?= $v_paid->payment_type; ?></span></small>
                                    </a>
                                        <?php
                                    }
                                }
                            ?>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <small>Total : <strong>
                                <?php
                                    if (!empty($total_receipt)) {
                                        echo number_format(($total_receipt),2);
                                    } else {
                                        echo '0.00';
                                    }
                                ?>
                            </strong></small>
                    </div>
                </section>
            </div>
			<!--- END FINISH JOBS --->
            <div class="wrap-fpanel col-sm-6 " style="margin-top: 20px;">
                <section class="box box-primary">
                    <header class="box-header with-border">
                        <h3 class="box-title"><?= lang('recent_activities') ?></h3></header>
                    <div class="panel-body">
                        <section class="comment-list block">
                            <section class="slim-scroll" style="height:400px;overflow-x: scroll" >
                                <?php
                                    $activities = $this->db
                                        ->order_by('activity_date', 'desc')
                                        ->get('tbl_activities', 10)
                                        ->result();
                                    if (!empty($activities)) {
                                        foreach ($activities as $v_activities) {
                                            $profile_info = $this->db->where(array('user_id' => $v_activities->user))->get('tbl_account_details')->row();
                                            ?>
											<article id="comment-id-1" class="comment-item" style="font-size: 11px;">
                                            <div class="pull-left recect_task  ">
                                                <a class="pull-left recect_task  ">
                                                    <?php if (!empty($profile_info)) {
                                                        ?>
														<img style="width: 30px;margin-left: 18px;
                                                             height: 29px;
                                                             border: 1px solid #aaa;" src="<?= base_url() . $profile_info->avatar ?>" class="img-circle">
                                                    <?php } ?>
                                                </a>
                                            </div>
                                            <section class="comment-body m-b-lg">
                                                <header class=" ">
                                                    <strong>
                                                        <?= $profile_info->fullname ?></strong>
                                                    <span class="text-muted text-xs"> <?php
                                                            $today = time();
                                                            $activity_day = strtotime($v_activities->activity_date);
                                                            echo $this->admin_model->get_time_different($today, $activity_day);
                                                        ?> <?= lang('ago') ?>
                                                    </span>
                                                </header>
                                                <div>
                                                    <?= $v_activities->activity ?>  <strong> <?= $v_activities->value1 . ' ' . $v_activities->value2 ?></strong>
                                                </div>
                                                <hr/>
                                            </section>
                                        </article>
                                            <?php
                                        }
                                    }
                                ?>
                            </section>
                        </section>
                    </div>
                </section>
            </div>
            <div class="col-md-6" style="margin-top: 20px;">
                <section class="box">
                    <aside class="nav-tabs-custom">
                        <ul class="nav nav-tabs box-header">
                            <li class="active"><a href="#invoice" data-toggle="tab"><?= lang('recent_invoice') ?></a></li>
                        </ul>
                        <section class="scrollable box-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="invoice">
                                    <table class="table table-striped m-b-none text-sm">
                                        <thead>
                                        <tr>
                                            <th><?= lang('invoice') ?></th>
                                            <th class="col-date"><?= lang('due_date') ?></th>
                                            <th><?= lang('client_name') ?></th>
                                            <th class="col-currency"><?= lang('due_amount') ?></th>
                                            <th><?= lang('status') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            /* $all_invoices_info = $this->db->limit(5)->get('tbl_invoices')->result(); */
                                            $this->db->select('*');
                                            $this->db->from('tbl_invoices');
                                            $this->db->join('tbl_bills', 'tbl_bills.invoices_id = tbl_invoices.invoices_id');
                                            $this->db->limit(5);
                                            $all_invoices_info = $this->db->get()->result();
                                            if (!empty($all_invoices_info)) {
                                                foreach ($all_invoices_info as $v_invoices) {
                                                    if($this->invoice_model->get_payment_status($v_invoices->invoices_id) == lang('fully_paid')){
                                                        $invoice_status = lang('fully_paid');
                                                        $label = "success";
                                                    }
													elseif($this->invoice_model->get_payment_status($v_invoices->invoices_id) == lang('partially_paid')){
                                                        $invoice_status = lang('partially_paid');
                                                        $label = "info";
                                                    }
                                                    else{
                                                        $invoice_status = lang('not_paid');
                                                        $label = "danger";
                                                    }
                                                    ?>
													<tr>
                                                    <td><a class="text-info" href="<?= base_url() ?>admin/invoice/manage_invoice/invoice_details/<?= encode($v_invoices->invoices_id) ?>"><?= $this->invoice_model->job_no_creation($v_invoices->invoices_id) ?></a></td>
                                                    <td><?= (!empty($v_invoices->due_date))?strftime(config_item('date_format'), strtotime($v_invoices->due_date)):'-' ?></td>
                                                        <?php
                                                            $client_info = $this->invoice_model->check_by(array('client_id' => $v_invoices->client_id), 'tbl_client');
                                                        ?>
														<td><?= $client_info->name ?></td>
                                                    <td>PKR <?= number_format($this->invoice_model->calculate_to('invoice_due', $v_invoices->invoices_id), 2) ?></td>
                                                    <td>
                                                        <span class="label label-<?= $label ?>"><?= $invoice_status ?></span>
                                                    </td>
                                                </tr>
                                                    <?php
                                                }
                                            } else {
                                                ?>
												<tr>
                                                <td colspan="4"><?= lang('nothing_to_display') ?></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </aside>
                </section>
            </div>
        </div>
		<!-- Morris.js charts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="<?= base_url() ?>/plugins/morris/morris.min.js" type="text/javascript"></script>
		<!-- FastClick -->
        <script src="<?= base_url() ?>/plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
		<!--Calendar-->
        <script type="text/javascript">
            $(document).ready(function() {
                if ($('#calendar').length) {
                    var date = new Date();
                    var d = date.getDate();
                    var m = date.getMonth(); var y = date.getFullYear();
                    var calendar = $('#calendar').fullCalendar({
                        header: {
                            center: 'prev title next',
                            left: '',
                            right: ''
                        },
                        buttonText: {
                            prev: '<i class="fa fa-angle-left" />',
                            next: '<i class="fa fa-angle-right" />'
                        },
                        selectable: true,
                        selectHelper: true,
                        select: function(start, end, allDay) {
                            var endtime = $.fullCalendar.formatDate(end, 'h:mm tt');
                            var starttime = $.fullCalendar.formatDate(start, 'yyyy/MM/dd');
                            var mywhen = starttime + ' - ' + endtime;
                            $('#event_modal #apptStartTime').val(starttime);
                            $('#event_modal #apptEndTime').val(starttime);
                            $('#event_modal #apptAllDay').val(allDay);
                            $('#event_modal #when').text(mywhen);
                            $('#event_modal').modal('show');
                        },
                        events: [
                            <?php
                            if ($role == 1) {
                            $payments_info = $this->db->get('tbl_payments')->result();
                            if (!empty($payments_info)) {
                            foreach ($payments_info as $v_payments) :
                            $start_day = date('d', strtotime($v_payments->payment_date));
                            $smonth = date('n', strtotime($v_payments->payment_date));
                            $start_month = $smonth - 1;
                            $start_year = date('Y', strtotime($v_payments->payment_date));
                            $end_year = date('Y', strtotime($v_payments->payment_date));
                            $end_day = date('d', strtotime($v_payments->payment_date));
                            $emonth = date('n', strtotime($v_payments->payment_date));
                            $end_month = $emonth - 1;
                            $invoice = $this->db->where(array('invoices_id' => $v_payments->invoices_id))->get('tbl_invoices')->row();
                            $client_info = $this->db->where(array('client_id' => $invoice->client_id))->get('tbl_client')->row();
                            ?>
                            {
                                title  : '<?= $client_info->name ." (".$this->invoice_model->job_no_creation($invoice->invoices_id).")"?>\n<?= "PKR ". $v_payments->amount ?>',
                                start: new Date(<?php echo $start_year . ',' . $start_month . ',' . $start_day; ?>),
                                end: new Date(<?php echo $end_year . ',' . $end_month . ',' . $end_day; ?>),
                                color  : '#78ae54',
                                url: '<?= base_url() ?>admin/invoice/manage_invoice/payments_details/<?= encode($v_payments->payments_id) ?>/<?= encode($invoice->invoices_id) ?>'
                            },
                            <?php
                            endforeach;
                            }
                            $invoice_info = $this->db->get('tbl_invoices')->result();
                            if (!empty($invoice_info)) {
                            foreach ($invoice_info as $v_invoice) :
                            $start_day = date('d', strtotime($v_invoice->due_date));
                            $smonth = date('n', strtotime($v_invoice->due_date));
                            $start_month = $smonth - 1;
                            $start_year = date('Y', strtotime($v_invoice->due_date));
                            $end_year = date('Y', strtotime($v_invoice->due_date));
                            $end_day = date('d', strtotime($v_invoice->due_date));
                            $emonth = date('n', strtotime($v_invoice->due_date));
                            $end_month = $emonth - 1;
                            $bill_info = $this->invoice_model->check_by(array('invoices_id'=>$v_invoice->invoices_id), 'tbl_bills');
                            if(!empty($bill_info)){
                                $url = base_url('admin/bill/manage_bill/bill_details/'.encode($bill_info->bill_id));
                                $color = '#3c8dbc';
                            }
                            else{
                                $url = base_url('admin/invoice/manage_invoice/invoice_details/'.encode($v_invoice->invoices_id));
                                $color = '#DE4E6C';
                            }
                            ?>
                            {
                                title  : '<?php echo $this->invoice_model->job_no_creation($v_invoice->invoices_id) ?>',
                                start: new Date(<?php echo $start_year . ',' . $start_month . ',' . $start_day; ?>),
                                end: new Date(<?php echo $end_year . ',' . $end_month . ',' . $end_day; ?>),
                                color  : '<?= $color ?>',
                                url: '<?= $url ?>'
                            },
                            <?php
                            endforeach;
                            }
                            }
                            ?>
                        ],
                        eventColor: '#3A87AD',
                    });
                }
            });</script>
        <script src="<?php echo base_url(); ?>asset/js/fullcalendar.js"></script>
        <script src="<?php echo base_url(); ?>asset/js/jquery-ui.min.js"></script>
		<!-- / Chart.js Script -->
        <script src="<?php echo base_url(); ?>asset/js/chart.min.js" type="text/javascript"></script>
        <script>
            // line chart data
            var buyerData = {
                labels: [
                    <?php
                    // yearle result name = month name
                    for ($i = 1; $i <= 12; $i++) {
                    $month_name = date('F', strtotime($year . '-' . $i)); // get full name of month by date query
                    ?>
                    "<?php echo $month_name; ?>", // echo the whole month of the year
                    <?php }; ?>
                ],
                datasets: [
                    {
                        fillColor: "rgba(172,194,132,0.4)",
                        strokeColor: "#ACC26D",
                        pointColor: "#fff",
                        pointStrokeColor: "#9DB86D",
                        data: [
                            <?php
                            // get monthly result report
                            foreach ($yearly_overview as $v_overview):
                            ?>
                            "<?php
                                echo $v_overview; // view the total report in a  month
                                ?>",
                            <?php
                            endforeach;
                            ?>
                        ]
                    }
                ]
            }
            // get line chart canvas
            var buyers = document.getElementById('yearly_report').getContext('2d');
            // draw line chart
            new Chart(buyers).Line(buyerData);
            console.log(buyerData);
            $(document).ready(function () {
                $(".consignment_btn").click(function(){
                    $(".consignment_text").toggle();
                });
                $(".client_quote_btn").click(function(){
                    $(".client_quote_text").toggle();
                });
                $(".client_bill_btn").click(function(){
                    $(".client_bill_text").toggle();
                });
                $(".bill_amount_btn").click(function(){
                    $(".bill_amount_text").toggle();
                });
                $(".sales_tax_btn").click(function(){
                    $(".sales_tax_text").toggle();
                });
            });
        </script>
        <script>
            function getRandomColor() {
                var letters = '789ABCD'.split('');
                var color = '#';
                for (var i = 0; i < 6; i++ ) {
                    color += letters[Math.round(Math.random() * 6)];
                }
                return color;
            }
            var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
            var pieChart       = new Chart(pieChartCanvas)
            var PieData        = [<?php for($i=0; $i<count($pie_chart); $i++){ echo"{";?>
                value    : <?php echo $pie_chart[$i]['count'];?>,
            color    : getRandomColor(),
                highlight: getRandomColor(),
                label    : '<?php echo $pie_chart[$i]['job_status_title']?>'
            <?php echo"},"; }?>
            ]
            var pieOptions     = {
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke    : true,
                //String - The colour of each segment stroke
                segmentStrokeColor   : '#fff',
                //Number - The width of each segment stroke
                segmentStrokeWidth   : 2,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 50, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps       : 100,
                //String - Animation easing effect
                animationEasing      : 'easeOutBounce',
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate        : true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale         : false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive           : true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio  : true,
                //String - A legend template
                legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
            }
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            pieChart.Doughnut(PieData, pieOptions)
        </script>