<?php
class Accounts_model extends CI_Model {

    /*Read Active Sub Type*/
    public function read_sub_type()
    {
        return $this->db->where('SUB_STATUS',1)->get('sub_heads')->result();
    }

    /*Insert Account Head Function*/
    public function create_head($data)
    {
        return $this->db->insert('accounts_head',$data);
    }

    /*Read Account Head Function */
    public function read_heads()
    {
        return $this->db->select('*')
            ->from('accounts_head ah')
            ->join('sub_heads sh','ah.SUB_HEAD_ID=sh.SUB_HEAD_ID')
            ->where(array(
                'sh.SUB_STATUS' => 1
            ))
            ->order_by('ah.H_NO','ASC')
            ->get()->result();
    }

    /*Update Head*/
    public function update_head($data)
    {
        return $this->db->where('H_ID',$data['H_ID'])
            ->update('accounts_head',$data);
    }

    /*Read By Head Id*/
    public function select_edit_head($head_id)
    {
        return $this->db->where('H_ID',$head_id)->get('accounts_head')->row();
    }

    /*Delete Account Head Function*/
    public function delete_head($head_id)
    {
        $this->db->where('H_ID',$head_id)
            ->delete('accounts_head');

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    /*Insert Account Function*/
    public function create_account($data)
    {
        return $this->db->insert('accounts',$data);
    }

    public function create_account_batch($data)
    {
        return $this->db->insert_batch('accounts',$data);
    }

    /*Read Account Function */
    public function read_accounts()
    {
        return $this->db->select('*')
            ->from('accounts a')
            ->join('accounts_head ah','a.H_ID=ah.H_ID')
            ->join('sub_heads sh','ah.SUB_HEAD_ID=sh.SUB_HEAD_ID')
            ->order_by('sh.HEAD_TYPE','asc')
            ->get()->result();
    }

    /*Read By Account Id with Head*/
    public function select_edit_account($account_id)
    {
        return $this->db->select('a.*,ah.H_TYPE,ah.SUB_HEAD_ID')
                ->from('accounts a')
                ->join('accounts_head ah','a.H_ID=ah.H_ID')
                ->where('a.A_ID',$account_id)->get()->row();
    }

    /*Read By Account Id*/
    public function read_account_by_id($account_id)
    {
        return $this->db->select('*')
            ->from('accounts')
            ->where('A_ID',$account_id)->get()->row();
    }

    /*Read Accounts Categories Wise*/
    public function account_categories_wise($account_id)
    {
        return $this->db->select('*')
            ->from('accounts')
            ->group_start()
                ->where('A_ID',$account_id)
                ->or_where('A_SELFHEAD_ID',$account_id)
                ->or_where_in('A_SELFHEAD_ID',"select A_ID from accounts where A_SELFHEAD_ID=$account_id",false)
            ->group_end()
            ->get()->result();
    }

    /*Read Account No By Head No*/
    public function account_no_by_heads($head_id)
    {
        return $this->db->select('A_NO')
            ->from('accounts a')
            ->join('accounts_head ah','a.H_ID=ah.H_ID')
            ->where('A_SELFHEAD_ID',0)
            ->where('a.H_ID',$head_id)
            ->order_by('a.A_ID','DESC')
            ->get()->row();
    }

    public function account_no_by_head($head_id)
    {
        return $this->db->select_max('A_NO')
            ->from('accounts a')
            ->join('accounts_head ah','a.H_ID=ah.H_ID')
            ->where('A_SELFHEAD_ID',0)
            ->where('a.H_ID',$head_id)->get()->row();
    }

    public function max_account_no_by_head($head_id)
    {
        return $this->db->select('*')
            ->from('accounts a')
            ->join('accounts_head ah','a.H_ID=ah.H_ID')
            ->where('A_SELFHEAD_ID',0)
            ->where('a.H_ID',$head_id)
            ->order_by('a.A_NO','DESC')
            ->get()->row();
    }

    /*Read Account No By Self Head Id*/
    public function account_no_by_selfHead($id,$aid=null)
    {
        $this->db->select_max('A_NO')
            ->from('accounts')
            ->or_where('A_SELFHEAD_ID',$id);
        if(!empty($aid)) {
            $this->db->where('A_ID !=', $aid);
        }
        return $this->db->get()->row();
        
    }

    public function max_account_no_self_head($id)
    {
        return $this->db->select('*')
            ->from('accounts')
            ->or_where('A_SELFHEAD_ID',$id)
            ->order_by('A_NO','DESC')
            ->get()->row();
    }

    /*Update Account*/
    public function update_account($data)
    {
        return $this->db->where('A_ID',$data['A_ID'])
            ->update('accounts',$data);
    }

    /*Delete Account Function*/
    public function delete_account($account_id)
    {
        $this->db->where('A_ID',$account_id)
            ->delete('accounts');
        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    /*Read Head By Type where Function */
    public function read_head_where($id)
    {
        return $this->db->select('H_ID,H_NAME')
            ->from('accounts_head')
            ->where(array('SUB_HEAD_ID'=>$id,'H_STATUS'=>1))
            ->get()->result();
    }

    /*Read accounts on head Id*/
    public function ajax_select_accounts($where_data)
    {
        return $this->db->select('*')
            ->from('accounts')
            ->where($where_data)
            ->get()->result();
    }

    /*Read Sub Head on Basis of Head Type*/
    public function select_sub_heads($type){
        return $this->db->where(array('HEAD_TYPE'=>$type,'SUB_STATUS'=>1))->get('sub_heads')->result();
    }

    /*Read Sub Head No on basis of Sub Id*/
    public function sub_heads_no($id){
        return $this->db->select_max('H_NO')
            ->from('accounts_head ah')
            ->join('sub_heads sh','ah.SUB_HEAD_ID=sh.SUB_HEAD_ID')
            ->where('ah.SUB_HEAD_ID',$id)
            ->get()->row();
    }

    /*Read Sub Head on Basis of Head Id*/
    public function sub_heads_by_id($id){
        return $this->db->select('*')
            ->from('sub_heads')
            ->where('SUB_HEAD_ID',$id)
            ->get()->row();
    }
    
    /*Read Voucher Data*/
    public function read_general_voucher($type = NULL)
    {
        $this->db->select('e.*,da.A_NAME as DEBIT_NAME,ca.A_NAME as CREDIT_NAME')
            ->from('expense e')
            ->join('accounts da','da.A_ID=e.DEBIT_ACCOUNT')
            ->join('accounts ca','ca.A_ID=e.CREDIT_ACCOUNT')
            ->where('e.IS_ACTIVE',1);
            if($type != NULL){
                $this->db->where('e.VOUCHER_TYPE', $type);
            }
        return $this->db->get()->result();
    }

    /*Read Expense Data*/
    public function read_expense($type = NULL)
    {
         $this->db->select('e.*,da.A_NAME as DEBIT_NAME,ca.A_NAME as CREDIT_NAME, inv.*')
            ->from('expense e')
            ->join('accounts da','da.A_ID=e.DEBIT_ACCOUNT')
            ->join('accounts ca','ca.A_ID=e.CREDIT_ACCOUNT')
            ->join('tbl_invoices inv','inv.invoices_id=e.INVOICES_ID')
            ->where('e.IS_ACTIVE',1);
            if($type != NULL){
                $this->db->where('e.VOUCHER_TYPE', $type);
            }
        return $this->db->get()->result();
    }

    /*Read Expense Data By Id*/
    public function read_expense_by_id($voucher_id)
    {
        return $this->db->select('e.*,da.A_NAME as DEBIT_NAME,ca.A_NAME as CREDIT_NAME')
            ->from('expense e')
            ->join('accounts da','da.A_ID=e.DEBIT_ACCOUNT')
            ->join('accounts ca','ca.A_ID=e.CREDIT_ACCOUNT')
            ->where('e.E_ID',$voucher_id)
            ->get()->row();
    }

    /*Read Expense By Account Id and date*/
    public function read_expense_by_account($account_id,$date)
    {
        return $this->db->select('e.*,da.A_NAME as DEBIT_NAME,ca.A_NAME as CREDIT_NAME')
            ->from('expense e')
            ->join('accounts da','da.A_ID=e.DEBIT_ACCOUNT')
            ->join('accounts ca','ca.A_ID=e.CREDIT_ACCOUNT')
            ->group_start()
                ->where('e.DEBIT_ACCOUNT',$account_id)
                ->or_where('e.CREDIT_ACCOUNT',$account_id)
            ->group_end()
            ->where('E_DATE',$date)
            ->where('e.IS_ACTIVE',1)
            ->get()->result();
    }

    /*Insert Expense Function*/
    public function create_expense($data)
    {
        return $this->db->insert('expense',$data);
    }

    /*Select Expense Data By ID*/
    public function ajax_select_expense_by_id($ex_id)
    {
        return $this->db->where('E_ID',$ex_id)->get('expense')->row();
    }

    /*Select Expense Data By User*/
    public function ajax_select_expense_by_user($user_id)
    {
        return $this->db->select('e.*,da.A_NAME as DEBIT_NAME,ca.A_NAME as CREDIT_NAME')
            ->from('expense e')
            ->join('accounts da','da.A_ID=e.DEBIT_ACCOUNT')
            ->join('accounts ca','ca.A_ID=e.CREDIT_ACCOUNT')
            ->where(array(
                'e.IS_ACTIVE'  => 1,
                'e.CREATED_BY' => $user_id
            ))
            ->get()->result();
    }

    /*Update Expense*/
    public function update_expense($data)
    {
        return $this->db->where('E_ID',$data['E_ID'])
            ->update('expense',$data);
    }

    /*Delete Expense Function*/
    public function delete_voucher($expense_id)
    {
        $this->db->where('E_ID',$expense_id)->delete('expense');

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    /*Insert Transaction Function*/
    public function create_transaction($data)
    {
        return $this->db->insert('transactions',$data);
    }

    /*Update Transaction Function*/
    public function update_transaction($data)
    {
        return $this->db->where('T_ID',$data['T_ID'])
            ->update('transactions',$data);
    }

    /*Insert Transaction Meta Function*/
    public function create_transaction_meta($data)
    {
        return $this->db->insert_batch('transactions_meta',$data);
    }

    /*Update Transaction Meta Function*/
    public function update_transaction_meta($data)
    {
        return $this->db->where('TM_TYPE',$data['TM_TYPE'])
            ->where('T_ID',$data['T_ID'])
            ->update('transactions_meta',$data);
    }
    
    /*Update Transaction Meta By Id Function*/
    public function update_trm_by_id($data)
    {
        return $this->db->update_batch('transactions_meta',$data,'TM_ID');
    }

    /*Select Transaction*/
    public function read_transaction()
    {
        return $this->db->where('T_TYPE','general')->order_by('T_DATE','ASC')->get('transactions')->result();
    }

    /*Select Transaction By Date*/
    public function read_transaction_by_date($date)
    {
        return $this->db->where('T_DATE',$date)
            ->where('T_TYPE','General Journal')
            ->get('transactions')->result();
    }
    
    /*Select Transaction Data By ID*/
    public function transaction_by_id($tr_id)
    {
        return $this->db->where('T_ID',$tr_id)->get('transactions')->row();
    }

    /*Delete Transaction Function*/
    public function delete_transaction($tr_id)
    {
        $this->db->where('T_ID',$tr_id)
            ->delete('transactions_meta');

        $this->db->where('T_ID',$tr_id)
            ->delete('transactions');


        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }
    
    /*Delete Transaction Meta Function*/
    public function delete_transaction_meta($trm_id)
    {
        $this->db->where('TM_ID',$trm_id)
            ->delete('transactions_meta');
        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    /*Select General Ledgers Data*/
    public function general_ledgers_data($start_date,$end_date,$account)
    {
        return $this->db->select('*')
            ->from('transactions t')
            ->join('transactions_meta tm','t.T_ID=tm.T_ID')
            ->join('accounts a','tm.A_ID=a.A_ID')
            ->group_start()
                ->where('a.A_ID',$account)
                ->or_where('a.A_SELFHEAD_ID',$account)
                ->or_where_in('a.A_SELFHEAD_ID',"select A_ID from accounts where A_SELFHEAD_ID=$account",false)
            ->group_end()
			->where(array(
                't.T_DATE >='     => $start_date,
                't.T_DATE <='     => $end_date
            ))
            ->order_by('t.T_DATE','ASC')
            ->get()->result();
    }

    public function client_ledgers_data($start_date,$end_date,$client_id)
    {
        return $this->db->select('*')
            ->from('tbl_invoices')
            ->where('client_id',$client_id)
            ->where(array(
                'created_date >=' => $start_date,
                'created_date <=' => $end_date
            ))
            ->get()->result();
    }
    
    public function client_ledgers($start_date,$end_date,$client_id){
        return $this->db->select('*')
            ->from('client_ledgers1')
            ->where(array(
                'client'    => $client_id,
                'date >='  => $start_date,
                'date <='   => $end_date
            ))
            ->order_by('date','asc')
            ->get()->result();
    }
    /* End General Ledgers Function */

    /*Insert Expense Function*/
    public function create_cashAndBank($data)
    {
        return $this->db->insert('cash_and_bank',$data);
    }

    /*Select Cash and Bank*/
    public function read_cashAndBank($account,$date)
    {
        return $this->db->select('cb.*,ab.A_NAME as debit_account,ac.A_NAME as credit_account,')
            ->from('cash_and_bank cb')
            ->join('accounts ab','cb.CB_DEBITACCOUNT=ab.A_ID')
            ->join('accounts ac','cb.CB_CREDITACCOUNT=ac.A_ID')
            ->group_start()
                ->where('cb.CB_DEBITACCOUNT',$account)
                ->or_where('cb.CB_CREDITACCOUNT',$account)
            ->group_end()
            ->where('cb.CB_DATE',$date)
            ->order_by('cb.CB_ID','desc')
            ->get()->result();
    }

    /*Select Cash and Bank By Id*/
    public function read_cashAndBank_id($id){
        return $this->db->where('CB_ID',$id)
                ->get('cash_and_bank')->row();
    }

    /*Update Cash and Bank Function*/
    public function update_cashAndBank($data)
    {
        return $this->db->where('CB_ID',$data['CB_ID'])
            ->update('cash_and_bank',$data);
    }

    /*Delete Cash and Bank Function*/
    public function delete_cashAndBank($id)
    {
        $this->db->where('CB_ID',$id)
            ->delete('cash_and_bank');

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    /*Read Cheques By Account Id and Date*/
    public function read_cheques_by_account($account_id,$date)
    {
       return $this->db->select('*')
           ->from('cheques c')
           ->join('branches br','c.BR_ID=br.BR_ID')
           ->join('accounts a','c.DB_ACCOUNTID=a.A_ID')
           ->group_start()
               ->where('br.A_ID',$account_id)
               ->or_where('c.DB_ACCOUNTID',$account_id)
           ->group_end()
           ->where('c.C_DATE',$date)
           ->where('c.C_STATUS',"Approved")
           ->get()->result();
    }
	
	/*Read Banks Function */
    public function read_banks()
    {
        return $this->db->get('banks')->result();
    }

    /*Insert Banks Function*/
    public function create_bank($data)
    {
        return $this->db->insert('banks',$data);
    }

    /*Read Banks By Column where Function */
    public function read_bank_where($column,$value)
    {
        return $this->db->where($column,$value)
            ->get('banks')->result();
    }
    
    /*Read By Bank Id*/
    public function select_edit_bank($bank_id)
    {
        return $this->db->where('B_ID',$bank_id)->get('banks')->row();
    }

    /*Update Bank*/
    public function update_bank($data)
    {
        return $this->db->where('B_ID',$data['B_ID'])
            ->update('banks',$data);
    }

    /*Delete banks Function*/
    public function delete_bank($bank_id)
    {
        $this->db->where('B_ID',$bank_id)
            ->delete('banks');

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    /*Insert Branch Function*/
    public function create_branch($data)
    {
        return $this->db->insert('branches',$data);
    }

    /*Read Branches Function */
    public function read_branches()
    {
        return $this->db->select('*')
            ->from('branches br')
            ->join('banks b','br.B_ID=b.B_ID')
            ->get()->result();
    }

    /*Read By Branch Id*/
    public function select_edit_branch($branch_id)
    {
        return $this->db->where('BR_ID',$branch_id)->get('branches')->row();
    }

    /*Update Branch*/
    public function update_branch($data)
    {
        return $this->db->where('BR_ID',$data['BR_ID'])
            ->update('branches',$data);
    }

    /*Delete Branch Function*/
    public function delete_branch($branch_id)
    {
        $this->db->where('BR_ID',$branch_id)
            ->delete('branches');

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }
	
	/*Read Branch By Column where Function */
	public function read_branch_where($bank_id)
    {
        return $this->db->select('*')
            ->from('branches')
            ->where(array('B_ID'=>$bank_id,'BR_STATUS'=>1))
            ->get()->result();
    }
	
	/*Read Cheques Function */
    public function read_cheques()
    {
        return $this->db->select('*')
            ->from('cheques c')
            ->join('branches br','c.BR_ID=br.BR_ID')
            ->join('banks b','br.B_ID=b.B_ID')
            ->join('tbl_users u','c.CREATED_BY=u.user_id')
            ->join('accounts a','c.DB_ACCOUNTID=a.A_ID')
            ->get()->result();
    }

    /*Delete Cheque Function*/
    public function delete_cheque($cheque_id)
    {
        $this->db->where('C_ID',$cheque_id)
            ->delete('cheques');

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }
	
	 /*Insert Cheque Function*/
    public function create_cheque($data)
    {
        return $this->db->insert('cheques',$data);
    }
	
	/*Read By Cheque Id*/
    public function select_edit_cheque($cheque_id)
    {
        return $this->db->select('*')
            ->from('cheques c')
            ->join('branches br','c.BR_ID=br.BR_ID')
            ->join('banks b','br.B_ID=b.B_ID')
            ->where('c.C_ID',$cheque_id)->get()->row();
    }

    /*Read branches on User basis for highchart*/
    public function read_branches_user($data)
    {
        return $this->db->select('br.BR_ID,B_NAME,BR_NAME')
            ->from('branches br')
            ->join('cheques c','br.BR_ID=c.BR_ID')
            ->join('banks b','br.B_ID=b.B_ID')
            ->where($data)
            ->group_by('br.BR_ID')
            ->get()->result();
    }
	
	/*Update Cheque*/
    public function update_cheque($data)
    {
        return $this->db->where('C_ID',$data['C_ID'])
            ->update('cheques',$data);
    }

    /*search client*/
    public function ajax_select_client($where_data)
    {
        return $this->db->select('*')
            ->from('tbl_client')
            ->where($where_data)
            ->get()->result();
    }

    public function advance_amount_by_invoice($invoice_id){
        return $this->db->select('tr.T_DATE as date,SUM(tm.TM_AMOUNT) as amount,tm.PARTICULARS')
            ->from('transactions_meta tm')
            ->join('transactions tr','tm.T_ID=tr.T_ID')
            ->join('accounts a','tm.A_ID=a.A_ID')
            /*->join('tbl_requisitions rq','tm.T_ID=rq.transaction_id')*/
            ->where(array(
                'rq.invoices_id' => $invoice_id,
                'a.H_ID'    => 53
            ))->get()->row();
    }

    public function pre_advance_by_invoice($invoice_id){
        return $this->db->select('SUM(amount) as amount, payment_date')
            ->from('tbl_advance_payments')
            ->where('invoices_id',$invoice_id)
            ->get()->row();
    }

    public function security_deposit_by_invoice($invoice_id){
        return $this->db->select('SUM(deposit_amount) as deposit, created_date')
            ->from('tbl_security_deposit')
            ->where('invoices_id',$invoice_id)
            ->get()->row();
    }

    public function memos_amount_by_invoice($invoice_id){
        return $this->db->select('*')
            ->from('tbl_memos')
            ->join('tbl_memo_charges', 'tbl_memos.memo_id = tbl_memo_charges.memo_id')
            ->where('tbl_memos.invoices_id', $invoice_id)
            ->order_by('tbl_memos.created_date', 'DESC')
            ->get()->result();
    }

    public function total_bill_by_invoice($invoice_id){
        return $this->db->select('tr.T_DATE as date,SUM(tm.TM_AMOUNT) as amount,tm.TM_TYPE,tm.PARTICULARS,bl.bill_no')
            ->from('transactions_meta tm')
            ->join('transactions tr','tm.T_ID=tr.T_ID')
            ->join('accounts a','tm.A_ID=a.A_ID')
            ->join('tbl_bills bl','tm.T_ID=bl.transaction_id')
            ->where(array(
                'bl.invoices_id' => $invoice_id,
                'a.H_ID'    => 17,
                'tm.TM_TYPE' => 'Debit'
            ))->get()->row();
    }

    public function received_amount_by_invoice($invoice_id){
        /*return $this->db->select('tr.T_DATE as date,tm.TM_AMOUNT as amount,pay.cheque_payorder_no')
            ->from('transactions_meta tm')
            ->join('transactions tr','tm.T_ID=tr.T_ID')
            ->join('accounts a','tm.A_ID=a.A_ID')
            ->join('tbl_payments pay','tm.PAYMENT_ID=pay.payments_id')
            ->where(array(
                'pay.invoices_id' => $invoice_id,
                'a.H_ID'    => 17,
                'tm.TM_TYPE' => 'Credit'
            ))
            ->order_by('tr.T_DATE','asc')
            ->get()->result();*/
        return $this->db->select('*')
            ->from('tbl_advance_payments')
            ->where('invoices_id', $invoice_id)
            ->order_by('payment_date', 'DESC')
            ->get()->result();
    }

    public function expenses_amount_by_invoice($invoice_id){
        return $this->db->select('*')
            ->from('expense')
            ->where('INVOICES_ID', $invoice_id)
            ->where('VOUCHER_TYPE', 'Exp')
            ->order_by('E_DATE', 'DESC')
            ->get()->result();
    }

    public function pre_job_advances_by_id($id){
        $this->db->select('ap.*,ti.client_id,ti.reference_no');
        $this->db->from('tbl_advance_payments ap');
        $this->db->join('tbl_invoices ti','ti.invoices_id=ap.invoices_id','left');
        $this->db->where('ap.ap_id',$id);
        return $this->db->get()->row();
    }

    public function job_advances_details_by_id($id){
        $this->db->select('apd.*');
        $this->db->from('tbl_advance_payment_details apd');
        /*$this->db->join('banks b','b.B_ID=apd.apd_bank','left');
        $this->db->join('branches br','br.BR_ID=apd.apd_branch','left');*/
        $this->db->where('apd.ap_id',$id);
        return $this->db->get()->result();
    }

    public function transfer_details_by_paymentId($id){
        $this->db->select('tr.*,b.B_NAME,br.BR_NAME');
        $this->db->from('tbl_transfer_amount tr');
        $this->db->join('branches br','br.A_ID=tr.debit_account','left');
        $this->db->join('banks b','b.B_ID=br.B_ID','left');
        $this->db->where('tr.advance_payment_id',$id);
        return $this->db->get()->row();
    }

    public function direct_payment_by_id($id){
        $this->db->select('dp.*,ti.client_id,ti.reference_no');
        $this->db->from('tbl_direct_payments dp');
        $this->db->join('tbl_invoices ti','ti.invoices_id=dp.invoices_id');
        $this->db->where('dp.ap_id',$id);
        return $this->db->get()->row();
    }

    public function self_accounts(){
        return $this->db->select('A_ID,A_NAME,CLIENT_ID')
            ->from('accounts')
            ->where(array(
                'A_STATUS'  => 1,
                'A_SELFHEAD_ID'=> 0
            ))->get()->result();
    }
    /*** PETTY CASH ***/
    public function read_petty_expense()
    {
        return $this->db->select('pc.*,a.A_NAME as staff_name,u.username')
            ->from('petty_cash pc')
            ->join('accounts a','pc.PC_PAID_TO=a.A_ID')
            ->join('tbl_users u','pc.PC_PAID_BY=u.user_id')
            ->where('pc.IS_ACTIVE',1)->get()->result();
    }

    public function read_petty_cash_by_id($id){
        return $this->db->select('pc.*,a.A_NAME as staff_name,u.username')
            ->from('petty_cash pc')
            ->join('accounts a','pc.PC_PAID_TO=a.A_ID')
            ->join('tbl_users u','pc.PC_PAID_BY=u.user_id')
            ->where('PC_ID',$id)
            ->get()->row();
    }
    /*** END PETTY CASH ***/
    /*** PAYMENT VOUCHER ***/
    public function read_payment_voucher()
    {
        return $this->db->select('pv.*,st.staff_name,u.username')
            ->from('payment_vouchers pv')
            ->join('tbl_staff st','pv.PV_PAID_TO=st.account_id')
            ->join('tbl_users u','pv.CREATED_BY=u.user_id')
            ->where('pv.IS_ACTIVE',1)->get()->result();
    }
    
    public function read_payment_voucher_by_id($id){
        return $this->db->select('pv.*,st.staff_name,u.username')
            ->from('payment_vouchers pv')
            ->join('tbl_staff st','pv.PV_PAID_TO=st.account_id')
            ->join('tbl_users u','pv.CREATED_BY=u.user_id')
            ->where('PV_ID',$id)
            ->get()->row();
    }
    /*** END PAYMENT VOUCHER ***/

    public function expenses_accounts_by_invoice($id){
        return $this->db->select('a.A_ID as id,a.A_NAME as name')
            ->from('accounts a')
            ->join('tbl_client cl','a.CLIENT_ID=cl.client_id')
            ->join('tbl_invoices iv','iv.client_id=cl.client_id')
            ->where(array(
                'iv.invoices_id'    => $id,
                'a.SUB_HEAD_ID'     => 17
            ))->order_by('a.A_ID','asc')
            ->get()->result();
    }

    public function job_expenses_by_invoice($id){
        return $this->db->select('*')
            ->from('transactions_meta tm')
            ->join('transactions t','tm.T_ID=t.T_ID')
            ->join('accounts a','tm.A_ID=a.A_ID')
            ->join('tbl_invoices inv','a.CLIENT_ID=inv.client_id')
            ->where(array(
                'a.SUB_HEAD_ID' => 17,
                't.T_TYPE'  => 'Job Expenses',
                'inv.invoices_id' => $id
            ))->get()->result();
    }

    /*Start Job Ledger Functions*/
    public function job_ledgers($invoice_id){
        return $this->db->select('*')
            ->from('job_ledgers')
            ->where('invoice_id',$invoice_id)
            ->order_by('date','asc')
            ->get()->result();
    }
    /*End Job Ledger Functions*/

    public function advances_by_client($account_id,$type){
        return $this->db->select_sum('TM_AMOUNT')
            ->where(array(
                'A_ID'=>$account_id,
                'TM_TYPE'=>$type
                ))->get('transactions_meta')->row();
    }
    
    /*Job Expenses By multiple Ids*/
    public function job_expenses_by_multipleIds($invoices_ids){
        return $this->db->select('vd.invoices_id,v.created_date,vd.job_expense_id,v.payment_date,v.payment_method,vd.amount')
            ->from('tbl_voucher_details vd')
            ->join('tbl_vouchers v','v.voucher_id=vd.voucher_id')
            ->where_in('vd.invoices_id',$invoices_ids)
            ->get()->result();
    }

    /*Start Payment Voucher Functions*/
    public function payment_accounts(){
        $types=array('Assets','Expense','Liabilities');
        return $this->db->select('A_ID,A_NAME')
            ->from('accounts a')
            ->join('accounts_head ah','a.H_ID=ah.H_ID')
            ->where_in('ah.H_TYPE',$types)
            ->where_not_in('ah.H_ID',4)
            ->get()->result();
    }

    public function max_payment_voucher(){
        return $this->db->select_max('PV_ID')
            ->get('payment_vouchers')->row();
    }

    public function payment_vouchers($payment_mode){
        $this->db->select('pv.*,da.A_NAME as paid_account,ca.A_NAME as credit_account')
            ->from('payment_vouchers pv')
            ->join('accounts da','pv.PAID_TO_ACCOUNT=da.A_ID')
            ->join('accounts ca','pv.ACCOUNT_CREDIT=ca.A_ID');
        if ($payment_mode!=''){
            $this->db->where('pv.PAYMENT_MODE',$payment_mode);
        }
        return $this->db->get()->result();
    }

    public function payment_voucher_by_id($id){
        return $this->db->select('pv.*,da.A_NAME as paid_account,ca.A_NAME as credit_account')
            ->from('payment_vouchers pv')
            ->join('accounts da','pv.PAID_TO_ACCOUNT=da.A_ID')
            ->join('accounts ca','pv.ACCOUNT_CREDIT=ca.A_ID')
            ->where('pv.PV_ID',$id)
            ->get()->row();
    }
    /*End Payment Voucher Functions*/

    /*Start Receipt Voucher*/
    public function max_receipt_voucher(){
        return $this->db->select_max('ap_id')
            ->get('tbl_advance_payments')->row();
    }

    public function receiving_accounts(){
        $types=array('Assets','Liabilities');
        return $this->db->select('A_ID,A_NAME,CLIENT_ID')
            ->from('accounts a')
            ->join('accounts_head ah','a.H_ID=ah.H_ID')
            ->where_in('ah.H_TYPE',$types)
            ->where_not_in('ah.H_ID',4)
            ->get()->result();
    }
    /*End Receipt Voucher*/

    public function last_pcref_no(){
        return $this->db->select_max('PC_ID')
            ->get('petty_cash')->row();
    }

    public function last_voucher_no(){
        return $this->db->select_max('voucher_id')
            ->get('tbl_vouchers')->row();
    }

    /*START EXPENSES VOUCHER*/
    public function exp_voucher_list_rows($search_array){
        $this->db->select('pc.*,a.A_NAME as staff_name,u.username')
            ->from('petty_cash pc')
            ->join('accounts a','pc.PC_PAID_TO=a.A_ID')
            ->join('tbl_users u','pc.PC_PAID_BY=u.user_id');
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        return $this->db->where('pc.IS_ACTIVE',1)->count_all_results();
    }

    public function exp_voucher_list($search_array,$length,$start){

        $this->db->select("pc.*,a.A_NAME as staff_name,u.username,(SELECT GROUP_CONCAT(a.A_NAME SEPARATOR ', ')
	        FROM petty_cash_details pcd
            JOIN accounts a
	        ON pcd.A_ID = a.A_ID
            WHERE pcd.PC_ID = pc.PC_ID)as A_NAME,
            (SELECT GROUP_CONCAT(pcd.PCD_AMOUNT SEPARATOR ', ')
	        FROM petty_cash_details pcd
            JOIN accounts a
	        ON pcd.A_ID = a.A_ID
            WHERE pcd.PC_ID = pc.PC_ID)as PCD_AMOUNT")
            ->from('petty_cash pc')
            ->join('accounts a','pc.PC_PAID_TO=a.A_ID')
            ->join('tbl_users u','pc.PC_PAID_BY=u.user_id');
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        if ($length != -1){
            $this->db->limit($length,$start);
        }
        $this->db->order_by('pc.PC_VOUCHER_NO','asc');
        return $this->db->where('pc.IS_ACTIVE',1)->get()->result();
    }
    /*END EXPENSES VOUCHER*/
    /*START JOB EXPENSES VOUCHER*/
    public function job_voucher_list_rows($search_array){
        $this->db->select('*')
            ->from('tbl_vouchers v')
            ->join('tbl_voucher_details vd','vd.voucher_id=v.voucher_id');
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        return $this->db->where('v.voucher_type','Exp')->count_all_results();
    }

    public function job_voucher_list($search_array,$length,$start){

        $this->db->select('*')
            ->from('tbl_vouchers v')
            ->join('tbl_voucher_details vd','vd.voucher_id=v.voucher_id');
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        if ($length != -1){
            $this->db->limit($length,$start);
        }
        return $this->db->where('v.voucher_type','Exp')->get()->result();
    }
    /*END JOB EXPENSES VOUCHER*/
    /*START VOUCHER LIST*/
    public function vouchers_list_rows($search_array){
        $this->db->select('e.*,da.A_NAME as DEBIT_NAME,ca.A_NAME as CREDIT_NAME')
            ->from('expense e')
            ->join('accounts da','da.A_ID=e.DEBIT_ACCOUNT')
            ->join('accounts ca','ca.A_ID=e.CREDIT_ACCOUNT')
            ->where('e.IS_ACTIVE',1);
        if('e.VOUCHER_TYPE' != NULL) {
            $this->db->where('e.VOUCHER_TYPE', 'Gen');
        }
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        return $this->db->count_all_results();
    }

    public function vouchers_list($search_array,$length,$start){
        $this->db->select('e.*,da.A_NAME as DEBIT_NAME,ca.A_NAME as CREDIT_NAME')
            ->from('expense e')
            ->join('accounts da','da.A_ID=e.DEBIT_ACCOUNT')
            ->join('accounts ca','ca.A_ID=e.CREDIT_ACCOUNT')
            ->where('e.IS_ACTIVE',1);
        if('e.VOUCHER_TYPE' != NULL) {
            $this->db->where('e.VOUCHER_TYPE', 'Gen');
        }
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        if ($length != -1){
            $this->db->limit($length,$start);
        }
        return $this->db->get()->result();
    }
    /*END VOUCHER LIST*/
    /*START REVERSAL LIST*/
    public function reverse_searching_list($search_array,$length,$start){
        $this->db->select('*')
            ->from('reverse_entries re')
            ->join('payment_vouchers pv','pv.PV_ID=re.voucher_id','left')
            ->join('tbl_advance_payments ap','ap.ap_id=re.voucher_id','left')
            ->join('petty_cash pc','pc.PC_ID=re.voucher_id','left');
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        if ($length != -1){
            $this->db->limit($length,$start);
        }
        return $this->db->group_by('re.voucher_id')->get()->result();
    }

    public function reverse_searching_list_rows($search_array){
        $this->db->select('*')
            ->from('reverse_entries re')
            ->join('payment_vouchers pv','pv.PV_ID=re.voucher_id','left')
            ->join('tbl_advance_payments ap','ap.ap_id=re.voucher_id','left')
            ->join('petty_cash pc','pc.PC_ID=re.voucher_id','left');
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        return $this->db->count_all_results();
    }

    /*END REVERSAL LIST*/

    /*START RECEIPT VOUCHER LIST*/
    public function receipt_voucher_searching_list($search_array,$length,$start,$payment_mode){
        $mode='';
        if ($payment_mode==1){
            $mode='Cash';
        }elseif ($payment_mode==2){
            $mode='Bank';
        }elseif ($payment_mode==3){
            $mode='Third Party';
        }
        $this->db->select('*')->from('tbl_advance_payments ad');
        if ($payment_mode!=0){
            $this->db->where('ad.payment_method', $mode);
        }
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        if ($length != -1){
            $this->db->limit($length,$start);
        }
        return $this->db->get()->result();
        /*return $this->db->group_by('inv.invoices_id')->get()->result();*/
    }

    public function receipt_voucher_list_rows($search_array,$payment_mode){
        $mode='';
        if ($payment_mode==1){
            $mode='Cash';
        }elseif ($payment_mode==2){
            $mode='Bank';
        }elseif ($payment_mode==3){
            $mode='Third Party';
        }
        $this->db->select('*')->from('tbl_advance_payments ad');
        if ($payment_mode!=0){
            $this->db->where('ad.payment_method', $mode);
        }
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        return $this->db->count_all_results();
    }
    /*END RECEIPT VOUCHER LIST*/
    /*START PAYMENTS VOUCHER LIST*/
    public function payment_voucher_searching_list($search_array,$length,$start,$payment_mode){
        $this->db->select('pv.*,da.A_NAME as paid_account,ca.A_NAME as credit_account')
            ->from('payment_vouchers pv')
            ->join('accounts da','pv.PAID_TO_ACCOUNT=da.A_ID')
            ->join('accounts ca','pv.ACCOUNT_CREDIT=ca.A_ID');
        if ($payment_mode!=0){
            $this->db->where('pv.PAYMENT_MODE', $payment_mode);
        }
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        if ($length != -1){
            $this->db->limit($length,$start);
        }
        return $this->db->get()->result();
        /*return $this->db->group_by('inv.invoices_id')->get()->result();*/
    }

    public function payment_voucher_list_rows($search_array,$payment_mode){
        $this->db->select('pv.*,da.A_NAME as paid_account,ca.A_NAME as credit_account')
            ->from('payment_vouchers pv')
            ->join('accounts da','pv.PAID_TO_ACCOUNT=da.A_ID')
            ->join('accounts ca','pv.ACCOUNT_CREDIT=ca.A_ID');
        if ($payment_mode!=0){
            $this->db->where('pv.PAYMENT_MODE', $payment_mode);
        }
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        return $this->db->count_all_results();
    }
    /*END PAYMENTS VOUCHER LIST*/
    /*START GENERAL JOURNAL LIST*/
    public function read_transaction_searching_list($search_array,$length,$start,$date){
         $this->db->from('transactions')
             ->where('T_DATE',$date)
            ->where('T_TYPE','General Journal')
            ->order_by('T_DATE','ASC');
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        if ($length != -1){
            $this->db->limit($length,$start);
        }
        return $this->db->get()->result();
    }

    public function read_transaction_list_rows($search_array,$date){
        $this->db->from('transactions')
            ->where('T_DATE',$date)
            ->where('T_TYPE','General Journal')
            ->order_by('T_DATE','ASC');
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        return $this->db->count_all_results();
    }
    /*END GENERAL JOURNAL LIST*/
    /*START CHEQUES LIST*/
    public function cheque_searching_list($search_array,$length,$start){
        $this->db->select('*')
            ->from('cheques c')
            ->join('branches br','c.BR_ID=br.BR_ID')
            ->join('banks b','br.B_ID=b.B_ID')
            ->join('tbl_users u','c.CREATED_BY=u.user_id')
            ->join('accounts a','c.DB_ACCOUNTID=a.A_ID');
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        if ($length != -1){
            $this->db->limit($length,$start);
        }
        return $this->db->get()->result();
    }

    public function cheque_list_rows($search_array){
        $this->db->select('*')
            ->from('cheques c')
            ->join('branches br','c.BR_ID=br.BR_ID')
            ->join('banks b','br.B_ID=b.B_ID')
            ->join('tbl_users u','c.CREATED_BY=u.user_id')
            ->join('accounts a','c.DB_ACCOUNTID=a.A_ID');
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        return $this->db->count_all_results();
    }
    /*END CHEQUES LIST*/

    public function getVocherAmount($vocher_id){
        return $this->db->select('amount')
            ->from('tbl_advance_payments')
            ->where('ap_id',$vocher_id)
            ->get()->row();
    }

    public function getSuspenseVocher($id){
        return $this->db->select('*')
            ->from('tbl_advance_payments')
            ->where(array('credit_account'=> $id,'transferred' => NULL))
            ->get()->result();
    }
    
    public function receipt_info($id){
        return $this->db->select('ap.ap_id, ap.payment_method, apd.apd_id, apd.apd_title, apd.apd_amount')
            ->from('tbl_advance_payments ap')
            ->join('tbl_advance_payment_details apd','ap.ap_id = apd.ap_id')
            ->where(array('ap.invoices_id' => $id, 'ap.payment_method' => 'Third Party'))
            ->get()->result();
    }
}