<?php

class Invoice_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    public function __construct() {
        parent::__construct();

        $this->load->model('accounts_model');

    }


    public function get_payment_status($invoice_id) {
        $this->db->select('bill_id');
        $this->db->where('invoices_id', $invoice_id);
        $this->db->from('tbl_bills');
        $query_result = $this->db->get();
        $bill = $query_result->row();
        if(!empty($bill)){
            $invoice_cost = $this->get_bill_total($bill->bill_id);
            $payment_made = $this->get_invoice_paid_amount($invoice_id);
            $advances = round($this->pre_job_advances($invoice_id), 2);
            $due = round(($invoice_cost - $advances - $payment_made));
            if ($payment_made < 1) {
                return lang('not_paid');
            } elseif ($due <= 0) {
                return lang('fully_paid');
            } else {
                return lang('partially_paid');
            }
        }
        else{
            return lang('not_paid');
        }
    }
    public function getInvoiceAndLC($id){
        $this->db->Select('*');
        $this->db->from('tbl_invoices inv');
        $this->db->join('tbl_financial_information fin', 'inv.invoices_id = fin.invoices_id');
        $this->db->where('inv.invoices_id', $id);
        return $this->db->get()->row();
    }
    
    public function invoice_perc($invoice) {
        $invoice_payment = $this->invoice_payment($invoice);
        $invoice_payable = $this->invoice_payable($invoice);
        if ($invoice_payable < 1 OR $invoice_payment < 1) {
            $perc_paid = 0;
        } else {
            $perc_paid = ($invoice_payment / $invoice_payable) * 100;
        }
        return round($perc_paid);
    }

    public function invoice_payment($invoice) {
        $this->ci->db->where('invoice', $invoice);
        $this->ci->db->select_sum('amount');
        $query = $this->ci->db->get('payments');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->amount;
        }
    }

    function ordered_items_by_id($id, $type = 'invoices') {
        $table = ($type == 'invoices' ? '' : 'estimate_') . 'tbl_items';
        $result = $this->db->where($type . '_id', $id)->order_by('item_order', 'asc')->get($table)->result();
        return $result;
    }

    function calculate_to($invoice_value, $invoice_id) {
        switch ($invoice_value) {
            case 'invoice_cost':
                return $this->get_invoice_cost($invoice_id);
                break;
            case 'tax':
                return $this->get_invoice_tax_amount($invoice_id);
                break;
            case 'discount':
                return $this->get_invoice_discount($invoice_id);
                break;
            case 'paid_amount':
                return $this->get_invoice_paid_amount($invoice_id);
                break;
            case 'invoice_due':
                return $this->get_invoice_due_amount($invoice_id);
                break;
        }
    }

    function get_invoice_cost($invoice_id) {
        $this->db->select_sum('total_cost');
        $this->db->where('invoices_id', $invoice_id);
        $this->db->from('tbl_items');
        $query_result = $this->db->get();
        $cost = $query_result->row();
        if (!empty($cost->total_cost)) {
            $result = $cost->total_cost;
        } else {
            $result = '0';
        }
        return $result;
    }


    public function get_invoice_tax_amount($invoice_id) {
        $invoice_cost = $this->get_invoice_cost($invoice_id);
        $invoice_info = $this->check_by(array('invoices_id' => $invoice_id), 'tbl_invoices');
        $tax = $invoice_info->tax;
        return ($tax / 100) * $invoice_cost;
    }

    public function get_invoice_discount($invoice_id) {
        $invoice_cost = $this->get_invoice_cost($invoice_id);
        $invoice_info = $this->check_by(array('invoices_id' => $invoice_id), 'tbl_invoices');
        $discount = $invoice_info->discount;
        return ($discount / 100) * $invoice_cost;
    }

    public function get_invoice_paid_amount($invoice_id) {
        $bill_info = $this->invoice_model->check_by(array('invoices_id' => $invoice_id), 'tbl_bills');
        if(isset($bill_info)){
            if($bill_info->current_advance >= $bill_info->total_bill || $bill_info->current_advance <= '-1.00'){
                $amount =  $bill_info->total_bill;
            }else{

                $client = $this->invoice_model->check_by(array('invoices_id'=>$invoice_id), 'tbl_invoices');
                $adc_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Liabilities', 'NAME' => 'Current Liabilities'), 'sub_heads');
                $adc_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $adc_head->SUB_HEAD_ID, 'H_NAME' => 'Advances from clients'), 'accounts_head');
                $adc_account_id = $this->invoice_model->check_by(array('H_ID' => $adc_sub_head->H_ID, 'CLIENT_ID' => $client->client_id,'A_NAME'=>client_name($client->client_id).' (Advances from clients)'), 'accounts');
                /*Getting Advances */
                $received_advances=$this->accounts_model->advances_by_client($adc_account_id->A_ID,'Credit')->TM_AMOUNT;
                $adjust_advances=$this->accounts_model->advances_by_client($adc_account_id->A_ID,'Debit')->TM_AMOUNT;
                $remaining_advances = $adjust_advances - $received_advances;
                $amount = $bill_info->total_bill - $remaining_advances;
                $query = $this->db->query("UPDATE `tbl_bills` SET `current_advance` = '".($remaining_advances-1)."' WHERE `tbl_bills`.`bill_id` = '".$bill_info->bill_id."' ");
            }
            $result = $amount;

            $this->db->select_sum('amount');
            $this->db->where('invoices_id', $invoice_id);
            $this->db->from('tbl_payments');
            $query_result = $this->db->get();
            $amount = $query_result->row();
            if (!empty($amount->amount)) {
                $result = $result+$amount->amount;
            } else {
                $result = $result+'0';
            }
        }
          else{
              $result = 0;
          }

        return $result;
    }

    public function get_invoice_due_amount($invoice_id) {
        $this->db->select('bill_id');
        $this->db->where('invoices_id', $invoice_id);
        $this->db->from('tbl_bills');
        $query_result = $this->db->get();
        $bill = $query_result->row();
        $invoice_cost = (!empty($bill))?$this->get_bill_total($bill->bill_id):0;
        $payment_made = $this->get_invoice_paid_amount($invoice_id);
        $received_amount = $this->pre_job_advances($invoice_id);
        $deposit_amount=$this->security_deposit_amount($invoice_id);
        $due_amount = 	$invoice_cost - $received_amount - $payment_made - (($received_amount>0)?$deposit_amount:0);
        if ($due_amount <= 0) {
            $due_amount = 0;
        }
        return $due_amount;
    }

    function all_invoice_amount() {
        $invoices = $this->db->get('tbl_invoices')->result();
        $cost[] = array();
        foreach ($invoices as $invoice) {
            $tax = round($this->get_invoice_tax_amount($invoice->invoices_id));
            $discount = round($this->get_invoice_discount($invoice->invoices_id));
            $invoice_cost = round($this->get_invoice_cost($invoice->invoices_id));

            $cost[] = ($invoice_cost + $tax) - $discount;
        }
        if (is_array($cost)) {
            return round(array_sum($cost), 2);
        } else {
            return 0;
        }
    }

    function all_outstanding() {
        $invoices = $this->db->get('tbl_invoices')->result();
        $due[] = array();
        foreach ($invoices as $invoice) {
            $due[] = $this->get_invoice_due_amount($invoice->invoices_id);
        }
        if (is_array($due)) {
            return round(array_sum($due), 2);
        } else {
            return 0;
        }
    }

    function client_outstanding($client_id) {
        $due[] = array();
        $invoices_info = $this->db->where('client_id', $client_id)->get('tbl_invoices')->result();

        foreach ($invoices_info as $v_invoice) {
            $due[] = $this->get_invoice_due_amount($v_invoice->invoices_id);
        }
        if (is_array($due)) {
            return round(array_sum($due), 2);
        } else {
            return 0;
        }
    }


    /*Calculate Duties*/

    function calculate_duty($invoice_value, $requisition_id) {
        switch ($invoice_value) {
            case 'declared_value':
                return $this->get_declared_value($requisition_id);
                break;
            case 'declared_item_total':
                return $this->get_declared_item_total($requisition_id);
                break;
            case 'assessable_item_total':
                return $this->get_assessable_item_total($requisition_id);
                break;
            case 'invoice_cd':
                return $this->get_invoice_cd($requisition_id);
                break;
            case 'assessable_cd':
                return $this->get_assessable_cd($requisition_id);
                break;
            case 'invoice_fed':
                return $this->get_invoice_fed($requisition_id);
                break;
            case 'invoice_acd':
                return $this->get_invoice_acd($requisition_id);
                break;
            case 'assessable_acd':
                return $this->get_assessable_acd($requisition_id);
                break;
            case 'invoice_rd':
                return $this->get_invoice_rd($requisition_id);
                break;
            case 'assessable_rd':
                return $this->get_assessable_rd($requisition_id);
                break;
            case 'invoice_st':
                return $this->get_invoice_st($requisition_id);
                break;
            case 'assessable_st':
                return $this->get_assessable_st($requisition_id);
                break;
            case 'invoice_ast':
                return $this->get_invoice_ast($requisition_id);
                break;
            case 'assessable_ast':
                return $this->get_assessable_ast($requisition_id);
                break;
            case 'invoice_it':
                return $this->get_invoice_it($requisition_id);
                break;
            case 'assessable_it':
                return $this->get_assessable_it($requisition_id);
                break;
            case 'declared_duty_total':
                return $this->get_declared_duty_total($requisition_id);
                break;
            case 'assessable_duty_total':
                return $this->get_assessable_duty_total($requisition_id);
                break;
            /*case 'invoice_excise_taxation':
                return $this->get_excise_taxation($requisition_id);
                break;*/
            case 'other_expenses_total':
                return $this->get_other_expenses($requisition_id);
                break;
            case 'declared_grand_total_duties':
                return $this->get_declared_grand_duty($requisition_id);
                break;
            case 'assessable_grand_total_duties':
                return $this->get_assessable_grand_duty($requisition_id);
                break;
            case 'declared_grand_total':
                return $this->get_declared_grand_total($requisition_id);
                break;
            case 'assessable_grand_total':
                return $this->get_assessable_grand_total($requisition_id);
                break;
        }
    }
    
    function get_invoice_value($invoices_id){
        $financial_info = $this->invoice_model->check_by(array('invoices_id'=>$invoices_id), 'tbl_financial_information');
        $total = 0;
        if($financial_info->incoterm == 1){
            $total = $financial_info->fob_value+$financial_info->freight;
        }else{
            $commodity_info = $this->invoice_model->check_by_all(array('invoices_id'=>$invoices_id), 'tbl_saved_commodities');
            foreach($commodity_info as $commodity){
                $total += $commodity->unit_value_assessed*$commodity->no_of_units;
            }
        }
        return $total;
    }
    
    function get_invoice_value_pkr($invoices_id){
        $financial_info = $this->invoice_model->check_by(array('invoices_id'=>$invoices_id), 'tbl_financial_information');
        $total = 0;
        if($financial_info->incoterm == 1){
            $total = $financial_info->fob_value+$financial_info->freight*$financial_info->exchange_rate;
        }else{
            $commodity_info = $this->invoice_model->check_by_all(array('invoices_id'=>$invoices_id), 'tbl_saved_commodities');
            foreach($commodity_info as $commodity){
                $total += $commodity->unit_value_assessed*$commodity->no_of_units*$financial_info->exchange_rate;
            }
        }
        return $total;
    }

    function get_declared_item_total($custom_duty_id) {
        $this->db->select('*');
        $this->db->from('tbl_invoices');
        $this->db->join('tbl_requisitions', 'tbl_invoices.invoices_id = tbl_requisitions.invoices_id');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->join('tbl_saved_commodities', 'tbl_saved_commodities.id = tbl_custom_duties.commodity_id');
        $this->db->where('tbl_custom_duties.custom_duty_id',$custom_duty_id);
        $query = $this->db->get();
        $requisition_info = $query->row();
        if (!empty($requisition_info)) {
            $result = $requisition_info->no_of_units*$requisition_info->unit_value*$requisition_info->exchange_rate;
            if ($requisition_info->insurance_type == "Percent") {
                $percentage = $result / 100 * $requisition_info->insurance_value;
                $result = $percentage + $result;
            } else {
                $result = $result + $requisition_info->insurance_value;
            }
            if ($requisition_info->landing_charges == "Yes") {
                $percentage = $result / 100;
                $result = $percentage + $result;
            }
        }
        else {
            $result = 0;
        }
        return $result;
    }

    function get_assessable_item_total($commodity_id) {
        $this->db->select('*');
        $this->db->from('tbl_requisitions');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->where('tbl_custom_duties.commodity_id',$commodity_id);
        $this->db->where('tbl_requisitions.requisition_type','Assessable');
        $query = $this->db->get();
        $assessable_info = $query->row();

        $this->db->select('*');
        $this->db->from('tbl_requisitions');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->join('tbl_saved_commodities', 'tbl_saved_commodities.id = tbl_custom_duties.commodity_id');
        $this->db->where('tbl_requisitions.invoices_id',$assessable_info->invoices_id);
        $this->db->where('tbl_custom_duties.commodity_id',$commodity_id);
        $this->db->where('tbl_requisitions.requisition_type','Declared');
        $query = $this->db->get();
        $declared_info = $query->row();

        $result = ($declared_info->weight/$declared_info->no_of_units)*$assessable_info->value_per_unit;
        if ($declared_info->insurance_type == "Percent") {
            $percentage = $result / 100 * $declared_info->insurance_value;
            $result = $percentage + $result;
        } else {
            $result = $result + $declared_info->insurance_value;
        }
        if ($declared_info->landing_charges == "Yes") {
            $percentage = $result / 100;
            $result = $percentage + $result;
        }
        return $result;
    }

    function get_declared_value($invoices_id) {
        $requisition_id = $this->invoice_model->get_any_field('tbl_requisitions',array('invoices_id', $invoices_id), 'requisition_id');
        if (!empty($requisition_id)) {
            $result = $this->get_declared_grand_duty($requisition_id);
        }
        else {
            $result = '0';
        }
        /*$this->db->select('*');
        $this->db->from('tbl_invoices');
        $this->db->join('tbl_requisitions', 'tbl_invoices.invoices_id = tbl_requisitions.invoices_id');
        $this->db->where('tbl_requisitions.invoices_id',$invoices_id);
        $query = $this->db->get();
        $requisition_info = $query->row();
        if (!empty($requisition_info)) {
            $result = $requisition_info->no_of_units*$requisition_info->unit_value*$requisition_info->exchange_rate;
            if ($requisition_info->insurance_type == "Percent") {
                $percentage = $result / 100 * $requisition_info->insurance_value;
                $result = $percentage + $result;
            } else {
                $result = $result + $requisition_info->insurance_value;
            }
            if ($requisition_info->landing_charges == "Yes") {
                $percentage = $result / 100;
                $result = $percentage + $result;
            }
        }
        else {
            $result = '0';
        }*/
        return $result;
    }
    function get_assessable_value($invoices_id) {
        $this->db->select('*');
        $this->db->from('tbl_invoices');
        $this->db->join('tbl_requisitions', 'tbl_invoices.invoices_id = tbl_requisitions.invoices_id');
        $this->db->where('tbl_requisitions.requisition_type','Assessable');
        $this->db->where('tbl_requisitions.invoices_id',$invoices_id);
        $query = $this->db->get();
        $requisition_info = $query->row();
        if (!empty($requisition_info)) {
            $result = $requisition_info->assessable_value;
        }
        else {
            $result = '0';
        }
        return $result;
    }
    function get_invoice_cd($custom_duty_id) {
        $this->db->select('*');
        $this->db->from('tbl_invoices');
        $this->db->join('tbl_requisitions', 'tbl_invoices.invoices_id = tbl_requisitions.invoices_id');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->join('tbl_saved_commodities', 'tbl_saved_commodities.id = tbl_custom_duties.commodity_id');
        $this->db->where('tbl_custom_duties.custom_duty_id',$custom_duty_id);
        $query = $this->db->get();
        $requisition_info = $query->row();
        if (!empty($requisition_info)) {
            $result = $requisition_info->no_of_units*$requisition_info->unit_value*$requisition_info->exchange_rate;
            if ($requisition_info->insurance_type == "Percent") {
                $percentage = $result / 100 * $requisition_info->insurance_value;
                $result = $percentage + $result;
            } else {
                $result = $result + $requisition_info->insurance_value;
            }
            if ($requisition_info->landing_charges == "Yes") {
                $percentage = $result / 100;
                $result = $percentage + $result;
            }
            if($requisition_info->duty_cd != 0.00){
                $result = $result / 100 * $requisition_info->duty_cd;
            }
            else{
                $result = 0;
            }
        }
        else {
            $result = 0;
        }
        return $result;
    }

    function get_assessable_cd($commodity_id) {
        $this->db->select('*');
        $this->db->from('tbl_requisitions');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->where('tbl_custom_duties.commodity_id',$commodity_id);
        $this->db->where('tbl_requisitions.requisition_type','Assessable');
        $query = $this->db->get();
        $assessable_info = $query->row();

        $this->db->select('*');
        $this->db->from('tbl_requisitions');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->join('tbl_saved_commodities', 'tbl_saved_commodities.id = tbl_custom_duties.commodity_id');
        $this->db->where('tbl_requisitions.invoices_id',$assessable_info->invoices_id);
        $this->db->where('tbl_custom_duties.commodity_id',$commodity_id);
        $this->db->where('tbl_requisitions.requisition_type','Declared');
        $query = $this->db->get();
        $declared_info = $query->row();

        $result = ($declared_info->weight/$declared_info->no_of_units)*$assessable_info->value_per_unit;
        if ($declared_info->insurance_type == "Percent") {
            $percentage = $result / 100 * $declared_info->insurance_value;
            $result = $percentage + $result;
        } else {
            $result = $result + $declared_info->insurance_value;
        }
        if ($declared_info->landing_charges == "Yes") {
            $percentage = $result / 100;
            $result = $percentage + $result;
        }
        if($declared_info->duty_cd != 0.00){
            $result = $result / 100 * $declared_info->duty_cd;
        }
        else{
            $result = 0;
        }
        return $result;
    }
    
    function get_invoice_fed($custom_duty_id) {
        $this->db->select('*');
        $this->db->from('tbl_invoices');
        $this->db->join('tbl_requisitions', 'tbl_invoices.invoices_id = tbl_requisitions.invoices_id');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->join('tbl_saved_commodities', 'tbl_saved_commodities.id = tbl_custom_duties.commodity_id');
        $this->db->where('tbl_custom_duties.custom_duty_id',$custom_duty_id);
        $query = $this->db->get();
        $requisition_info = $query->row();
        if (!empty($requisition_info)) {
            $result = $requisition_info->no_of_units*$requisition_info->unit_value*$requisition_info->exchange_rate;
            if ($requisition_info->insurance_type == "Percent") {
                $percentage = $result / 100 * $requisition_info->insurance_value;
                $result = $percentage + $result;
            } else {
                $result = $result + $requisition_info->insurance_value;
            }
            if ($requisition_info->landing_charges == "Yes") {
                $percentage = $result / 100;
                $result = $percentage + $result;
            }
            if($requisition_info->duty_cd != 0.00){
                $result = $result / 100 * $requisition_info->duty_fed;
            }
            else{
                $result = 0;
            }
        }
        else {
            $result = 0;
        }
        return $result;
    }

    function get_invoice_acd($custom_duty_id) {
        $this->db->select('*');
        $this->db->from('tbl_invoices');
        $this->db->join('tbl_requisitions', 'tbl_invoices.invoices_id = tbl_requisitions.invoices_id');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->join('tbl_saved_commodities', 'tbl_saved_commodities.id = tbl_custom_duties.commodity_id');
        $this->db->where('tbl_custom_duties.custom_duty_id',$custom_duty_id);
        $query = $this->db->get();
        $requisition_info = $query->row();
        $exchange_rate = $requisition_info->exchange_rate;
        if (!empty($requisition_info) && !empty($exchange_rate)) {
            $result = $requisition_info->no_of_units*$requisition_info->unit_value*$requisition_info->exchange_rate;
            if ($requisition_info->insurance_type == "Percent") {
                $percentage = $result / 100 * $requisition_info->insurance_value;
                $result = $percentage + $result;
            } else {
                $result = $result + $requisition_info->insurance_value;
            }
            if ($requisition_info->landing_charges == "Yes") {
                $percentage = $result / 100;
                $result = $percentage + $result;
            }
            if($requisition_info->duty_acd != 0.00){
                $result = $result / 100 * $requisition_info->duty_acd;

            }
            else{
                $result = 0;
            }
        }
        else {
            $result = 0;
        }
        return $result;
    }

    function get_assessable_acd($commodity_id) {
        $this->db->select('*');
        $this->db->from('tbl_requisitions');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->where('tbl_custom_duties.commodity_id',$commodity_id);
        $this->db->where('tbl_requisitions.requisition_type','Assessable');
        $query = $this->db->get();
        $assessable_info = $query->row();

        $this->db->select('*');
        $this->db->from('tbl_requisitions');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->join('tbl_saved_commodities', 'tbl_saved_commodities.id = tbl_custom_duties.commodity_id');
        $this->db->where('tbl_requisitions.invoices_id',$assessable_info->invoices_id);
        $this->db->where('tbl_custom_duties.commodity_id',$commodity_id);
        $this->db->where('tbl_requisitions.requisition_type','Declared');
        $query = $this->db->get();
        $declared_info = $query->row();

        $result = ($declared_info->weight/$declared_info->no_of_units)*$assessable_info->value_per_unit;
        if ($declared_info->insurance_type == "Percent") {
            $percentage = $result / 100 * $declared_info->insurance_value;
            $result = $percentage + $result;
        } else {
            $result = $result + $declared_info->insurance_value;
        }
        if ($declared_info->landing_charges == "Yes") {
            $percentage = $result / 100;
            $result = $percentage + $result;
        }
        if($declared_info->duty_acd != 0.00){
            $result = $result / 100 * $declared_info->duty_acd;
        }
        else{
            $result = 0;
        }
        return $result;
    }

    function get_invoice_rd($custom_duty_id) {
        $this->db->select('*');
        $this->db->from('tbl_invoices');
        $this->db->join('tbl_requisitions', 'tbl_invoices.invoices_id = tbl_requisitions.invoices_id');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->join('tbl_saved_commodities', 'tbl_saved_commodities.id = tbl_custom_duties.commodity_id');
        $this->db->where('tbl_custom_duties.custom_duty_id',$custom_duty_id);
        $query = $this->db->get();
        $requisition_info = $query->row();
        $exchange_rate = $requisition_info->exchange_rate;
        if (!empty($requisition_info) && !empty($exchange_rate)) {
            $result = $requisition_info->no_of_units*$requisition_info->unit_value*$requisition_info->exchange_rate;
            if ($requisition_info->insurance_type == "Percent") {
                $percentage = $result / 100 * $requisition_info->insurance_value;
                $result = $percentage + $result;
            } else {
                $result = $result + $requisition_info->insurance_value;
            }
            if ($requisition_info->landing_charges == "Yes") {
                $percentage = $result / 100;
                $result = $percentage + $result;
            }
            if($requisition_info->duty_rd != 0.00){
                $result = $result / 100 * $requisition_info->duty_rd;

            }
            else{
                $result = 0;
            }
        }
        else {
            $result = 0;
        }
        return $result;
    }

    function get_assessable_rd($commodity_id) {
        $this->db->select('*');
        $this->db->from('tbl_requisitions');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->where('tbl_custom_duties.commodity_id',$commodity_id);
        $this->db->where('tbl_requisitions.requisition_type','Assessable');
        $query = $this->db->get();
        $assessable_info = $query->row();

        $this->db->select('*');
        $this->db->from('tbl_requisitions');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->join('tbl_saved_commodities', 'tbl_saved_commodities.id = tbl_custom_duties.commodity_id');
        $this->db->where('tbl_requisitions.invoices_id',$assessable_info->invoices_id);
        $this->db->where('tbl_custom_duties.commodity_id',$commodity_id);
        $this->db->where('tbl_requisitions.requisition_type','Declared');
        $query = $this->db->get();
        $declared_info = $query->row();

        $result = ($declared_info->weight/$declared_info->no_of_units)*$assessable_info->value_per_unit;
        if ($declared_info->insurance_type == "Percent") {
            $percentage = $result / 100 * $declared_info->insurance_value;
            $result = $percentage + $result;
        } else {
            $result = $result + $declared_info->insurance_value;
        }
        if ($declared_info->landing_charges == "Yes") {
            $percentage = $result / 100;
            $result = $percentage + $result;
        }
        if($declared_info->duty_rd != 0.00){
            $result = $result / 100 * $declared_info->duty_rd;
        }
        else{
            $result = 0;
        }
        return $result;
    }

    function get_invoice_st($custom_duty_id) {
        $this->db->select('*');
        $this->db->from('tbl_invoices');
        $this->db->join('tbl_requisitions', 'tbl_invoices.invoices_id = tbl_requisitions.invoices_id');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->join('tbl_saved_commodities', 'tbl_saved_commodities.id = tbl_custom_duties.commodity_id');
        $this->db->where('tbl_custom_duties.custom_duty_id',$custom_duty_id);
        $query = $this->db->get();
        $requisition_info = $query->row();
        $exchange_rate = $requisition_info->exchange_rate;
        if (!empty($requisition_info) && !empty($exchange_rate)) {
            $result = $requisition_info->no_of_units*$requisition_info->unit_value*$requisition_info->exchange_rate;
            if ($requisition_info->insurance_type == "Percent") {
                $percentage = $result / 100 * $requisition_info->insurance_value;
                $result = $percentage + $result;
            } else {
                $result = $result + $requisition_info->insurance_value;
            }
            if ($requisition_info->landing_charges == "Yes") {
                $percentage = $result / 100;
                $result = $percentage + $result;
            }
            $result_cd = $this->get_invoice_cd($custom_duty_id);
            $result_acd = $this->get_invoice_acd($custom_duty_id);
            $result_rd = $this->get_invoice_rd($custom_duty_id);
            if($requisition_info->duty_st != 0.00){
                $st_duty = $result_cd + $result_acd + $result_rd + $result;
                $result = $st_duty / 100 * $requisition_info->duty_st;
            }
            else{
                $result = 0;
            }
        }
        else {
            $result = 0;
        }
        return $result;
    }

    function get_assessable_st($commodity_id) {
        $this->db->select('*');
        $this->db->from('tbl_requisitions');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->where('tbl_custom_duties.commodity_id',$commodity_id);
        $this->db->where('tbl_requisitions.requisition_type','Assessable');
        $query = $this->db->get();
        $assessable_info = $query->row();

        $this->db->select('*');
        $this->db->from('tbl_requisitions');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->join('tbl_saved_commodities', 'tbl_saved_commodities.id = tbl_custom_duties.commodity_id');
        $this->db->where('tbl_requisitions.invoices_id',$assessable_info->invoices_id);
        $this->db->where('tbl_custom_duties.commodity_id',$commodity_id);
        $this->db->where('tbl_requisitions.requisition_type','Declared');
        $query = $this->db->get();
        $declared_info = $query->row();

        $result = ($declared_info->weight/$declared_info->no_of_units)*$assessable_info->value_per_unit;
        if ($declared_info->insurance_type == "Percent") {
            $percentage = $result / 100 * $declared_info->insurance_value;
            $result = $percentage + $result;
        } else {
            $result = $result + $declared_info->insurance_value;
        }
        if ($declared_info->landing_charges == "Yes") {
            $percentage = $result / 100;
            $result = $percentage + $result;
        }
        $result_cd = $this->get_assessable_cd($commodity_id);
        $result_acd = $this->get_assessable_acd($commodity_id);
        $result_rd = $this->get_assessable_rd($commodity_id);
        if($declared_info->duty_st != 0.00){
            $st_duty = $result_cd + $result_acd + $result_rd + $result;
            $result = $st_duty / 100 * $declared_info->duty_st;
        }
        else{
            $result = 0;
        }
        return $result;
    }

    function get_invoice_ast($custom_duty_id) {
        $this->db->select('*');
        $this->db->from('tbl_invoices');
        $this->db->join('tbl_requisitions', 'tbl_invoices.invoices_id = tbl_requisitions.invoices_id');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->join('tbl_saved_commodities', 'tbl_saved_commodities.id = tbl_custom_duties.commodity_id');
        $this->db->where('tbl_custom_duties.custom_duty_id',$custom_duty_id);
        $query = $this->db->get();
        $requisition_info = $query->row();
        $exchange_rate = $requisition_info->exchange_rate;
        if (!empty($requisition_info) && !empty($exchange_rate)) {
            $result = $requisition_info->no_of_units*$requisition_info->unit_value*$requisition_info->exchange_rate;
            if ($requisition_info->insurance_type == "Percent") {
                $percentage = $result / 100 * $requisition_info->insurance_value;
                $result = $percentage + $result;
            } else {
                $result = $result + $requisition_info->insurance_value;
            }
            if ($requisition_info->landing_charges == "Yes") {
                $percentage = $result / 100;
                $result = $percentage + $result;
            }
            $result_cd = $this->get_invoice_cd($custom_duty_id);
            $result_acd = $this->get_invoice_acd($custom_duty_id);
            $result_rd = $this->get_invoice_rd($custom_duty_id);
            if($requisition_info->duty_ast != 0.00){
                $ast_duty = $result_cd + $result_acd + $result_rd + $result;
                $result = $ast_duty / 100 * $requisition_info->duty_ast;
            }
            else{
                $result = 0;
            }
        }
        else {
            $result = 0;
        }
        return $result;
    }

    function get_assessable_ast($commodity_id) {
        $this->db->select('*');
        $this->db->from('tbl_requisitions');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->where('tbl_custom_duties.commodity_id',$commodity_id);
        $this->db->where('tbl_requisitions.requisition_type','Assessable');
        $query = $this->db->get();
        $assessable_info = $query->row();

        $this->db->select('*');
        $this->db->from('tbl_requisitions');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->join('tbl_saved_commodities', 'tbl_saved_commodities.id = tbl_custom_duties.commodity_id');
        $this->db->where('tbl_requisitions.invoices_id',$assessable_info->invoices_id);
        $this->db->where('tbl_custom_duties.commodity_id',$commodity_id);
        $this->db->where('tbl_requisitions.requisition_type','Declared');
        $query = $this->db->get();
        $declared_info = $query->row();

        $result = ($declared_info->weight/$declared_info->no_of_units)*$assessable_info->value_per_unit;
        if ($declared_info->insurance_type == "Percent") {
            $percentage = $result / 100 * $declared_info->insurance_value;
            $result = $percentage + $result;
        } else {
            $result = $result + $declared_info->insurance_value;
        }
        if ($declared_info->landing_charges == "Yes") {
            $percentage = $result / 100;
            $result = $percentage + $result;
        }
        $result_cd = $this->get_assessable_cd($commodity_id);
        $result_acd = $this->get_assessable_acd($commodity_id);
        $result_rd = $this->get_assessable_rd($commodity_id);
        if($declared_info->duty_ast != 0.00){
            $st_duty = $result_cd + $result_acd + $result_rd + $result;
            $result = $st_duty / 100 * $declared_info->duty_ast;
        }
        else{
            $result = 0;
        }
        return $result;
    }

    function get_invoice_it($custom_duty_id) {
        $this->db->select('*');
        $this->db->from('tbl_invoices');
        $this->db->join('tbl_requisitions', 'tbl_invoices.invoices_id = tbl_requisitions.invoices_id');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->join('tbl_saved_commodities', 'tbl_saved_commodities.id = tbl_custom_duties.commodity_id');
        $this->db->where('tbl_custom_duties.custom_duty_id',$custom_duty_id);
        $query = $this->db->get();
        $requisition_info = $query->row();
        $exchange_rate = $requisition_info->exchange_rate;
        if (!empty($requisition_info) && !empty($exchange_rate)) {
            $result = $requisition_info->no_of_units*$requisition_info->unit_value*$requisition_info->exchange_rate;
            if ($requisition_info->insurance_type == "Percent") {
                $percentage = $result / 100 * $requisition_info->insurance_value;
                $result = $percentage + $result;
            } else {
                $result = $result + $requisition_info->insurance_value;
            }
            if ($requisition_info->landing_charges == "Yes") {
                $percentage = $result / 100;
                $result = $percentage + $result;
            }
            $result_cd = $this->get_invoice_cd($custom_duty_id);
            $result_acd = $this->get_invoice_acd($custom_duty_id);
            $result_rd = $this->get_invoice_rd($custom_duty_id);
            $result_st = $this->get_invoice_st($custom_duty_id);
            $result_ast = $this->get_invoice_ast($custom_duty_id);
            if($requisition_info->duty_it != 0.00){
                $it_duty = $result_cd + $result_acd + $result_rd + $result_st + $result_ast + $result;
                $result = $it_duty / 100 * $requisition_info->duty_it;

            }
            else{
                $result = 0;
            }
        }
        else {
            $result = 0;
        }
        return $result;
    }

    function get_assessable_it($commodity_id) {
        $this->db->select('*');
        $this->db->from('tbl_requisitions');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->where('tbl_custom_duties.commodity_id',$commodity_id);
        $this->db->where('tbl_requisitions.requisition_type','Assessable');
        $query = $this->db->get();
        $assessable_info = $query->row();

        $this->db->select('*');
        $this->db->from('tbl_requisitions');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->join('tbl_saved_commodities', 'tbl_saved_commodities.id = tbl_custom_duties.commodity_id');
        $this->db->where('tbl_requisitions.invoices_id',$assessable_info->invoices_id);
        $this->db->where('tbl_custom_duties.commodity_id',$commodity_id);
        $this->db->where('tbl_requisitions.requisition_type','Declared');
        $query = $this->db->get();
        $declared_info = $query->row();

        $result = ($declared_info->weight/$declared_info->no_of_units)*$assessable_info->value_per_unit;
        if ($declared_info->insurance_type == "Percent") {
            $percentage = $result / 100 * $declared_info->insurance_value;
            $result = $percentage + $result;
        } else {
            $result = $result + $declared_info->insurance_value;
        }
        if ($declared_info->landing_charges == "Yes") {
            $percentage = $result / 100;
            $result = $percentage + $result;
        }
        $result_cd = $this->get_assessable_cd($commodity_id);
        $result_acd = $this->get_assessable_acd($commodity_id);
        $result_rd = $this->get_assessable_rd($commodity_id);
        $result_st = $this->get_assessable_st($commodity_id);
        $result_ast = $this->get_assessable_ast($commodity_id);
        if($declared_info->duty_it != 0.00){
            $st_duty = $result_cd + $result_acd + $result_rd + $result_st + $result_ast + $result;
            $result = $st_duty / 100 * $declared_info->duty_it;
        }
        else{
            $result = 0;
        }
        return $result;
    }

    function get_declared_duty_total($custom_duty_id) {
        $result_cd = $this->get_invoice_cd($custom_duty_id);
        $result_fed = $this->get_invoice_fed($custom_duty_id);
        $result_acd = $this->get_invoice_acd($custom_duty_id);
        $result_rd = $this->get_invoice_rd($custom_duty_id);
        $result_st = $this->get_invoice_st($custom_duty_id);
        $result_ast = $this->get_invoice_ast($custom_duty_id);
        $result_it = $this->get_invoice_it($custom_duty_id);
        $result = $result_cd + $result_fed + $result_acd + $result_rd + $result_st + $result_ast + $result_it;
        return $result;
    }

    function get_assessable_duty_total($commodity_id) {
        $result_cd = $this->get_assessable_cd($commodity_id);
        $result_acd = $this->get_assessable_acd($commodity_id);
        $result_rd = $this->get_assessable_rd($commodity_id);
        $result_st = $this->get_assessable_st($commodity_id);
        $result_ast = $this->get_assessable_ast($commodity_id);
        $result_it = $this->get_assessable_it($commodity_id);
        $result = $result_cd + $result_acd + $result_rd + $result_st + $result_ast + $result_it;
        return $result;
    }

    function get_declared_grand_duty($requisition_id) {
        $this->db->select('*');
        $this->db->from('tbl_invoices');
        $this->db->join('tbl_requisitions', 'tbl_invoices.invoices_id = tbl_requisitions.invoices_id');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->join('tbl_saved_commodities', 'tbl_saved_commodities.id = tbl_custom_duties.commodity_id');
        $this->db->where('tbl_requisitions.requisition_id',$requisition_id);
        $query = $this->db->get();
        $requisition_info = $query->result();
        if (!empty($requisition_info)) {
            $total_duties = 0;
            foreach($requisition_info as $requisitions_info){
                $total_duties += $this->get_declared_duty_total($requisitions_info->custom_duty_id);
            }

        } else {
            $total_duties = 0;
        }
        return $total_duties;
    }

    function get_assessable_grand_duty($requisition_id) {
        $this->db->select('*');
        $this->db->from('tbl_invoices');
        $this->db->join('tbl_requisitions', 'tbl_invoices.invoices_id = tbl_requisitions.invoices_id');
        $this->db->join('tbl_custom_duties', 'tbl_custom_duties.requisition_id = tbl_requisitions.requisition_id');
        $this->db->join('tbl_saved_commodities', 'tbl_saved_commodities.id = tbl_custom_duties.commodity_id');
        $this->db->where('tbl_requisitions.requisition_id',$requisition_id);
        $query = $this->db->get();
        $requisition_info = $query->result();
        if (!empty($requisition_info)) {
            $total_duties = 0;
            foreach($requisition_info as $requisition_info){
                $total_duties += $this->get_assessable_duty_total($requisition_info->commodity_id);
            }

        } else {
            $total_duties = 0;
        }
        return $total_duties;
    }

    public function get_excise_taxation($invoices_id) {
        $this->db->select('*');
        $this->db->from('tbl_requisitions');
        $this->db->join('tbl_other_expenses', 'tbl_other_expenses.requisition_id = tbl_requisitions.requisition_id');
        $this->db->join('tbl_requisition_expenses', 'tbl_other_expenses.requisition_expense_id = tbl_requisition_expenses.requisition_expense_id');
        $this->db->where('tbl_requisitions.invoices_id',$invoices_id);
        $this->db->where('tbl_requisitions.requisition_type','Declared');
        $this->db->where('tbl_requisition_expenses.requisition_expense_title','Excise & Taxation');
        $query = $this->db->get();
        $requisition_info = $query->row();
        $result = 0;
        if(!empty($requisition_info)){
            $result = $requisition_info->other_expense_amount;
        }
        return $result;
    }

    public function get_other_expenses($invoices_id) {
        $transaction_info = $this->invoice_model->check_by_all(array('invoices_id'=>$invoices_id), 'tbl_other_expenses');
        $total = 0;
        foreach($transaction_info as $transc){
            $total += $transc->other_expense_amount;
        }
        return $total;
    }

    function get_declared_grand_total($requisition_id) {
        $requisition_info = $this->db->where('requisition_id', $requisition_id)->get('tbl_requisitions')->row();
        $total_duties = $this->get_declared_grand_duty($requisition_id);
        $total_other_expenses = $this->get_other_expenses($requisition_info->invoices_id);
        return $total_duties+$total_other_expenses;
    }

    function get_assessable_grand_total($requisition_id) {
        $requisition_info = $this->db->where('requisition_id', $requisition_id)->get('tbl_requisitions')->row();
        $total_duties = $this->get_assessable_grand_duty($requisition_id);
        $total_other_expenses = $this->get_other_expenses($requisition_info->invoices_id);
        return $total_duties+$total_other_expenses;
    }

    public function get_duty_1($invoices_id){
        $requisition_info = $this->db->where(array('invoices_id'=>$invoices_id,'requisition_type'=>'Declared'))->get('tbl_requisitions')->row();
        $result = (!empty($requisition_info))?$this->get_declared_grand_duty($requisition_info->requisition_id):0;
        return $result;
    }
    public function get_duty_2($invoices_id){
        $requisition_info = $this->db->where(array('invoices_id'=>$invoices_id,'requisition_type'=>'Assessable'))->get('tbl_requisitions')->row();
        if(!empty($requisition_info)) {
            $duty_1 = $this->get_duty_1($invoices_id);
            $duty_2 = $this->get_assessable_grand_duty($requisition_info->requisition_id);
            $sum = $duty_2 - $duty_1;
            $result = ($sum > 0) ? $sum : 0;
        }else{
            $result = 0;
        }
        return $result;
    }

    /*** End Calculate Duties ***/
    /*** BILL CALCULATIONS ***/
    function calculate_bill($bill_value, $bill_id) {
        switch ($bill_value) {
            case 'bill_grand_total':
                return $this->get_bill_total($bill_id);
                break;
            case 'bill_balance_dues':
                return $this->get_balance_dues($bill_id);
                break;
        }
    }
    function get_bill_total($bill_id) {
        $bill_info = $this->invoice_model->check_by(array('bill_id' => $bill_id), 'tbl_bills');
        $invoice_info = $this->invoice_model->check_by(array('invoices_id' => $bill_info->invoices_id), 'tbl_invoices');
        $rec_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Liabilities', 'NAME' => 'Current Liabilities'), 'sub_heads');
        $rec_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $rec_head->SUB_HEAD_ID, 'H_NAME' => 'Advances from clients'), 'accounts_head');
        $rec_account_id = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $rec_sub_head->SUB_HEAD_ID, 'H_ID' => $rec_sub_head->H_ID, 'CLIENT_ID' => $invoice_info->client_id, 'A_NAME'=>client_name($invoice_info->client_id). ' (Advances from clients)'), 'accounts');
        $rec_account_id = $this->invoice_model->check_by(array('A_ID'=>$rec_account_id->A_ID, 'T_ID'=>$bill_info->transaction_id),'transactions_meta');
        return $rec_account_id->TM_AMOUNT;
    }

    function get_cost_total($bill_id) {
        $bill_info = $this->invoice_model->check_by(array('bill_id' => $bill_id), 'tbl_bills');
        $invoice_info = $this->invoice_model->check_by(array('invoices_id' => $bill_info->invoices_id), 'tbl_invoices');
        $rec_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Liabilities', 'NAME' => 'Current Liabilities'), 'sub_heads');
        $rec_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $rec_head->SUB_HEAD_ID, 'H_NAME' => 'Advances from clients'), 'accounts_head');
        $rec_account_id = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $rec_sub_head->SUB_HEAD_ID, 'H_ID' => $rec_sub_head->H_ID, 'CLIENT_ID' => $invoice_info->client_id, 'A_NAME'=>client_name($invoice_info->client_id). ' (Advances from clients)'), 'accounts');
        $rec_account_id = $this->invoice_model->check_by(array('A_ID'=>$rec_account_id->A_ID, 'T_ID'=>$bill_info->transaction_id),'transactions_meta');
        return $rec_account_id->TM_AMOUNT;
    }

    function get_balance_dues($bill_id) {
        $bill_info = $this->invoice_model->check_by(array('bill_id' => $bill_id), 'tbl_bills');
        $total_amount = $this->get_bill_total($bill_id);
        /*$received_amount = $this->pre_job_advances($bill_info->invoices_id);
        $deposit_amount = $this->invoice_model->security_deposit_amount($bill_info->invoices_id);*/
        $invoice_info = $this->invoice_model->check_by(array('invoices_id' => $bill_info->invoices_id), 'tbl_invoices');
        $rec_head = $this->invoice_model->check_by(array('HEAD_TYPE' => 'Liabilities', 'NAME' => 'Current Liabilities'), 'sub_heads');
        $rec_sub_head = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $rec_head->SUB_HEAD_ID, 'H_NAME' => 'Advances from clients'), 'accounts_head');
        $rec_account_id = $this->invoice_model->check_by(array('SUB_HEAD_ID' => $rec_sub_head->SUB_HEAD_ID, 'H_ID' => $rec_sub_head->H_ID, 'CLIENT_ID' => $invoice_info->client_id, 'A_NAME'=>client_name($invoice_info->client_id). ' (Advances from clients)'), 'accounts');

        $tr_amount = $this->invoice_model->check_by(array('A_ID'=>$rec_account_id->A_ID, 'T_ID'=>$bill_info->transaction_id,'TM_TYPE'=>'debit'),'transactions_meta');
        return $total_amount - $tr_amount->TM_AMOUNT;
    }

    function get_agency_commission($invoices_id) {
        $this->db->select('*');
        $this->db->from('tbl_invoices');
        if($this->config->item("requisition_optional") == 'No') {
            $this->db->join('tbl_requisitions', 'tbl_invoices.invoices_id = tbl_requisitions.invoices_id');
            $this->db->where('tbl_requisitions.requisition_type', 'declared');
        }
        $this->db->where('tbl_invoices.invoices_id',$invoices_id);
        $query = $this->db->get();
        $requisition_info = $query->row();
        if (!empty($requisition_info)) {
            $result = $requisition_info->exchange_rate * $this->invoice_model->get_invoice_value($requisition_info->invoices_id);
            if($this->config->item("requisition_optional") == 'No') {
                if ($requisition_info->insurance_type == "Percent") {
                    $percentage = $result / 100 * $requisition_info->insurance_value;
                    $result = $percentage + $result;
                } else {
                    $result = $result + $requisition_info->insurance_value;
                }
                if ($requisition_info->landing_charges == "Yes") {
                    $percentage = $result / 100;
                    $result = $percentage + $result;
                }
            }
            if($requisition_info->commission_type == 'Percentage'){
                $result = $result / 100 * $requisition_info->commission_amount;
            }
            elseif($requisition_info->commission_type == 'Receipted Unreceipted'){
                $result = $requisition_info->commission_amount+$requisition_info->unreceipted_amount;
            }
            else{
                $result = $requisition_info->commission_amount;
            }
        }
        else {
            $result = "";
        }
        return $result;

    }
    /*** END AGENCY COMMISSION ***/
    /*** SALES TAX ***/
    function get_sales_tax($invoices_id) {
        $job_info = $this->invoice_model->check_by(array('invoices_id'=>$invoices_id), 'tbl_invoices');
        $result = $this->get_agency_commission($invoices_id);
        if ($result > 0) {
            $result = $result/100*$job_info->gst;
        }
        else {
            $result = 0;
        }
        return $result;

    }
    /*** END SALES TAX ***/
    /*** GET WEBOC ***/
    /*public function get_weboc($requisition_id){
        $this->db->select('*');
        $this->db->from('tbl_requisitions');
        $this->db->join('tbl_advances', 'tbl_requisitions.requisition_id = tbl_advances.requisition_id');
        $this->db->join('accounts', 'accounts.A_ID = tbl_advances.account_id');
        $this->db->where('tbl_requisitions.requisition_id',$requisition_id);
        $this->db->where('accounts.A_ID',1291);
        $query = $this->db->get()->row()->advance_amount;
        return $query;
    }*/
    /*** END GET WEBOC ***/

    public function bill_head_charge($type, $invoices_id, $bill_id){
        $invoice_info = $this->invoice_model->check_by(array('invoices_id'=>$invoices_id), 'tbl_invoices');
        $account_info = $this->invoice_model->check_by(array('CLIENT_ID'=>$invoice_info->client_id, 'A_NAME'=>$type), 'accounts');
        $bill_info = $this->invoice_model->check_by(array('bill_id'=>$bill_id), 'tbl_bills');
        $trasac_info = $this->invoice_model->check_by(array('T_ID'=>$bill_info->transaction_id, 'A_ID'=>$account_info->A_ID), 'transactions_meta');
        return $trasac_info->TM_BILL;
    }

    function convert_number($number) {
        if (($number < 0) || ($number > 999999999)) {
            throw new Exception("Number is out of range");
        }
        $Gn = floor($number / 1000000);
        /* Millions (giga) */
        $number -= $Gn * 1000000;
        $kn = floor($number / 1000);
        /* Thousands (kilo) */
        $number -= $kn * 1000;
        $Hn = floor($number / 100);
        /* Hundreds (hecto) */
        $number -= $Hn * 100;
        $Dn = floor($number / 10);
        /* Tens (deca) */
        $n = $number % 10;
        /* Ones */
        $res = "";
        if ($Gn) {
            $res .= $this->convert_number($Gn) .  "Million";
        }
        if ($kn) {
            $res .= (empty($res) ? "" : " ") .$this->convert_number($kn) . " Thousand";
        }
        if ($Hn) {
            $res .= (empty($res) ? "" : " ") .$this->convert_number($Hn) . " Hundred";
        }
        $ones = array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", "Nineteen");
        $tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eigthy", "Ninety");
        if ($Dn || $n) {
            if (!empty($res)) {
                $res .= " and ";
            }
            if ($Dn < 2) {
                $res .= $ones[$Dn * 10 + $n];
            } else {
                $res .= $tens[$Dn];
                if ($n) {
                    $res .= "-" . $ones[$n];
                }
            }
        }
        if (empty($res)) {
            $res = "zero";
        }
        return $res;
    }


    public function received_payorder_by_invoice($id){
        $this->db->select_sum('po_amount');
        $this->db->from('tbl_pay_orders');
        $this->db->where('invoices_id',$id);
        return $this->db->get()->row();
    }

    public function pre_job_advances($id){
        $this->db->select_sum('amount');
        $this->db->from('tbl_advance_payments ap');
        $this->db->join('tbl_invoices inv', 'inv.invoices_id = ap.invoices_id');
        $this->db->join('tbl_client cl', 'inv.client_id = cl.client_id');
        $this->db->where('ap.invoices_id',$id);
        $this->db->where('ap.client_id',null);
        return $this->db->get()->row()->amount;
    }

    public function security_deposit_amount($id){
        $this->db->select_sum('apd_amount');
        $this->db->from('tbl_advance_payment_details');
        $this->db->where('invoices_id',$id);
        $this->db->where('apd_title','Security Deposit');
        return $this->db->get()->row()->apd_amount;
    }

    public function received_security_by_id($id){
        $this->db->select_sum('apd_amount');
        $this->db->from('tbl_advance_payment_details');
        $this->db->where('ap_id',$id);
        $this->db->where('apd_title','Security Deposit');
        return $this->db->get()->row()->apd_amount;
    }

    public function direct_payments($id){
        $this->db->select_sum('amount');
        $this->db->from('tbl_direct_payments');
        $this->db->where('invoices_id',$id);
        return $this->db->get()->row()->amount;
    }

    public function job_expenses($id){
        $this->db->select_sum('vd.amount');
        $this->db->from('tbl_voucher_details vd');
        $this->db->join('tbl_vouchers v','v.voucher_id = vd.voucher_id');
        $this->db->join('accounts a','a.A_ID = vd.debit_account');
        $this->db->where('vd.invoices_id',$id);
        return $this->db->get()->row()->amount;
    }

    public function job_expenses_by_invoice_account($invoice_id,$job_expense_id){
        return $this->db->select('sum(amount) as amount')
            ->from('tbl_voucher_details')
            ->where(array(
                'invoices_id' => $invoice_id,
                'job_expense_id' => $job_expense_id
            ))->get()->row();
    }
    
    /*Invoices Query*/
    public function invoice_searching_list($search_array,$length,$start,$client_id,$reference_no,$bl_no,$commodity){
        $this->db->select('inv.invoices_id,inv.reference_no,inv.type,inv.client_id,cl.name,inv.currency,inv.created_date,inv.invoice_value')
            ->from('tbl_invoices inv')
            ->join('tbl_saved_commodities comm', 'inv.invoices_id = comm.invoices_id','left')
            ->join('tbl_client cl', 'inv.client_id = cl.client_id','left');
        if ($client_id!='All'){
            $this->db->where('cl.client_id', $client_id);
        }
        if ($reference_no!='All'){
            $this->db->where('inv.reference_no', $reference_no);
        }
        if ($bl_no!='All'){
            $this->db->where('inv.bl_no', $bl_no);
        }
        if ($commodity!='All'){
            $this->db->where('comm.commodity', $commodity);
        }
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        if ($length != -1){
            $this->db->limit($length,$start);
        }
        /*return $this->db->get()->result();*/
        return $this->db->group_by('inv.invoices_id')->get()->result();
    }

    public function invoice_searching_list_rows($search_array,$client_id,$reference_no,$bl_no,$commodity){
        $this->db->select('inv.invoices_id,inv.reference_no,inv.type,inv.client_id,cl.name,inv.currency,inv.created_date,inv.invoice_value')
            ->from('tbl_invoices inv')
            ->join('tbl_saved_commodities comm', 'inv.invoices_id = comm.invoices_id','left')
            ->join('tbl_client cl', 'inv.client_id = cl.client_id','left');
        if ($client_id!='All'){
            $this->db->where('cl.client_id', $client_id);
        }
        if ($reference_no!='All'){
            $this->db->where('inv.reference_no', $reference_no);
        }
        if ($bl_no!='All'){
            $this->db->where('inv.bl_no', $bl_no);
        }
        if ($commodity!='All'){
            $this->db->where('comm.commodity', $commodity);
        }
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        return $this->db->group_by('inv.invoices_id')->count_all_results();
    }

    /*Requisitions Query*/
    public function requisition_searching_list($search_array,$length,$start,$client_id,$reference_no,$bl_no,$commodity){
        $this->db->select('inv.invoices_id,inv.reference_no,inv.type,inv.created_date,req.requisition_id,req.requisition_type,inv.currency,inv.invoice_value,inv.exchange_rate')
            ->from('tbl_requisitions req')
            ->join('tbl_invoices inv', 'inv.invoices_id = req.invoices_id')
            ->join('tbl_client cl', 'inv.client_id = cl.client_id')
            ->join('tbl_saved_commodities comm', 'inv.invoices_id = comm.invoices_id');
        if ($client_id!='All'){
            $this->db->where('cl.client_id', $client_id);
        }
        if ($reference_no!='All'){
            $this->db->where('inv.reference_no', $reference_no);
        }
        if ($bl_no!='All'){
            $this->db->where('inv.bl_no', $bl_no);
        }
        if ($commodity!='All'){
            $this->db->where('comm.commodity', $commodity);
        }
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        if ($length != -1){
            $this->db->limit($length,$start);
        }
        /*return $this->db->get()->result();*/
        return $this->db->group_by('req.requisition_id')->get()->result();
    }

    public function requisition_searching_list_rows($search_array,$client_id,$reference_no,$bl_no,$commodity){
        $this->db->select('inv.invoices_id,inv.reference_no,inv.type,inv.created_date,req.requisition_id,req.requisition_type,inv.currency,inv.invoice_value,inv.exchange_rate')
            ->from('tbl_invoices inv')
            ->join('tbl_client cl', 'inv.client_id = cl.client_id')
            ->join('tbl_saved_commodities comm', 'inv.invoices_id = comm.invoices_id')
            ->join('tbl_requisitions req', 'inv.invoices_id = req.invoices_id');
        if ($client_id!='All'){
            $this->db->where('cl.client_id', $client_id);
        }
        if ($reference_no!='All'){
            $this->db->where('inv.reference_no', $reference_no);
        }
        if ($bl_no!='All'){
            $this->db->where('inv.bl_no', $bl_no);
        }
        if ($commodity!='All'){
            $this->db->where('comm.commodity', $commodity);
        }
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }

        return $this->db->group_by('req.requisition_id')->count_all_results();
    }

    /*Bill Query*/
    public function bill_searching_list($search_array,$length,$start,$client_id,$reference_no,$bl_no,$commodity){
        $this->db->select('inv.invoices_id,inv.reference_no,inv.type,inv.client_id,cl.name,inv.currency,inv.created_date,inv.invoice_value')
            ->from('tbl_invoices inv')
            ->join('tbl_client cl', 'inv.client_id = cl.client_id')
            /*->join('tbl_requisitions req', 'req.invoices_id=inv.invoices_id','left')*/
            ->join('tbl_saved_commodities comm', 'inv.invoices_id = comm.invoices_id');
        if ($client_id!='All'){
            $this->db->where('cl.client_id', $client_id);
        }
        if ($reference_no!='All'){
            $this->db->where('inv.reference_no', $reference_no);
        }
        if ($bl_no!='All'){
            $this->db->where('inv.bl_no', $bl_no);
        }
        if ($commodity!='All'){
            $this->db->where('comm.commodity', $commodity);
        }
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        if ($length != -1){
            $this->db->limit($length,$start);
        }
        /*return $this->db->get()->result();*/
        return $this->db->group_by('inv.invoices_id')->get()->result();
    }

    public function bill_searching_list_rows($search_array,$client_id,$reference_no,$bl_no,$commodity){
        $this->db->select('inv.invoices_id,inv.reference_no,inv.type,inv.client_id,cl.name,inv.currency,inv.created_date,inv.invoice_value')
            ->from('tbl_invoices inv')
            ->join('tbl_client cl', 'inv.client_id = cl.client_id')
            ->join('tbl_saved_commodities comm', 'inv.invoices_id = comm.invoices_id');
            /*->join('tbl_requisitions req', 'inv.invoices_id = req.invoices_id');*/
        if ($client_id!='All'){
            $this->db->where('cl.client_id', $client_id);
        }
        if ($reference_no!='All'){
            $this->db->where('inv.reference_no', $reference_no);
        }
        if ($bl_no!='All'){
            $this->db->where('inv.bl_no', $bl_no);
        }
        if ($commodity!='All'){
            $this->db->where('comm.commodity', $commodity);
        }
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        return $this->db->group_by('inv.invoices_id')->count_all_results();
    }

    /*All Payments Query*/
    public function payment_searching_list($search_array,$length,$start){
        $this->db->select('*')
            ->from('tbl_payments pay')
            ->where('pay.invoices_id !=', 0);
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        if ($length != -1){
            $this->db->limit($length,$start);
        }
        return $this->db->get()->result();
        /*return $this->db->group_by('inv.invoices_id')->get()->result();*/
    }

    public function payment_searching_list_rows($search_array){
        $this->db->select('*')
            ->from('tbl_payments pay')
            ->where('pay.invoices_id !=', 0);
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        return $this->db->count_all_results();
    }

    /*Security Deposit Query*/
    public function deposit_searching_list($search_array,$length,$start,$client_id,$reference_no,$bl_no,$commodity){
        $this->db->select('*')
            ->from('tbl_invoices inv')
            ->join('tbl_saved_commodities comm', 'inv.invoices_id = comm.invoices_id','left')
            ->join('tbl_client cl', 'inv.client_id = cl.client_id','left')
            ->join('tbl_security_deposit sd', 'inv.invoices_id = sd.invoices_id');
        if ($client_id!='All'){
            $this->db->where('cl.client_id', $client_id);
        }
        if ($reference_no!='All'){
            $this->db->where('inv.reference_no', $reference_no);
        }
        if ($bl_no!='All'){
            $this->db->where('inv.bl_no', $bl_no);
        }
        if ($commodity!='All'){
            $this->db->where('comm.commodity', $commodity);
        }
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        if ($length != -1){
            $this->db->limit($length,$start);
        }
        /*return $this->db->get()->result();*/
        return $this->db->group_by('inv.invoices_id')->get()->result();
    }

    public function deposit_searching_list_rows($search_array,$client_id,$reference_no,$bl_no,$commodity){
        $this->db->select('*')
            ->from('tbl_invoices inv')
            ->join('tbl_saved_commodities comm', 'inv.invoices_id = comm.invoices_id','left')
            ->join('tbl_client cl', 'inv.client_id = cl.client_id','left')
            ->join('tbl_security_deposit sd', 'inv.invoices_id = sd.invoices_id','left');
        if ($client_id!='All'){
            $this->db->where('cl.client_id', $client_id);
        }
        if ($reference_no!='All'){
            $this->db->where('inv.reference_no', $reference_no);
        }
        if ($bl_no!='All'){
            $this->db->where('inv.bl_no', $bl_no);
        }
        if ($commodity!='All'){
            $this->db->where('comm.commodity', $commodity);
        }
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        return $this->db->count_all_results();
    }

    /*Delivery Section Query*/
    public function delivery_searching_list($search_array,$length,$start,$client_id,$reference_no){
        $this->db->select('stbds.Iid, inv.invoices_id,inv.reference_no,inv.type,inv.client_id,cl.name,stbds.IInvoicesId')
            ->from('stbdeliverysection stbds')
            ->join('tbl_invoices inv', 'inv.invoices_id = stbds.IInvoicesId')
            ->join('tbl_client cl', 'inv.client_id = cl.client_id','left');
        if ($client_id!='All'){
            $this->db->where('cl.client_id', $client_id);
        }
        if ($reference_no!='All'){
            $this->db->where('inv.reference_no', $reference_no);
        }
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        if ($length != -1){
            $this->db->limit($length,$start);
        }
        /*return $this->db->get()->result();*/
        return $this->db->group_by('inv.invoices_id')->get()->result();
    }

    public function delivery_searching_list_rows($search_array,$client_id,$reference_no){
        $this->db->select('stbds.Iid, inv.invoices_id, inv.reference_no, inv.type, inv.client_id, cl.name, stbds.IInvoicesId')
            ->from('stbdeliverysection stbds')
            ->join('tbl_invoices inv', 'inv.invoices_id = stbds.IInvoicesId','left')
            ->join('tbl_client cl', 'inv.client_id = cl.client_id','left');
        if ($client_id!='All'){
            $this->db->where('cl.client_id', $client_id);
        }
        if ($reference_no!='All'){
            $this->db->where('inv.reference_no', $reference_no);
        }
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        return $this->db->group_by('inv.invoices_id')->count_all_results();
    }

    public function get_reference_no(){
        $this->db->select('inv.invoices_id, inv.reference_no');
        $this->db->from('tbl_invoices inv');
        $this->db->where('inv.`invoices_id` NOT IN (SELECT `IInvoicesId` FROM `stbdeliverysection`)', NULL, FALSE);
        return $this->db->get()->result();
    }

    /*Requisitions Query*/
}
