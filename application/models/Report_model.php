<?php

/**
 * Description of Project_Model
 *
 * @author NaYeM
 */
class Report_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    public function get_report_by_date($start_date, $end_date) {
        $this->db->select('*');
        $this->db->from('tbl_transactions');
        $this->db->where('date >=', $start_date);
        $this->db->where('date <=', $end_date);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    /*Sales Tax Query*/
    public function sales_tax_report_searching_list($search_array,$length,$start,$date){
        $this->db->select('*')
            ->from('tbl_invoices inv')
            ->join('tbl_client cl', 'cl.client_id = inv.client_id')
            ->join('tbl_financial_information fin', 'inv.invoices_id = fin.invoices_id')
            ->join('tbl_bills b', 'b.invoices_id = inv.invoices_id');
        if ($date!='All'){
            /*$this->db->where('b.bill_date', $date);*/
            $this->db->like('b.bill_date', $date);
        }
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        if ($length != -1){
            $this->db->limit($length,$start);
        }
        return $this->db->get()->result();
        /*return $this->db->group_by('inv.invoices_id')->get()->result();*/
    }

    public function sales_tax_report_searching_list_rows($search_array,$date){
        $this->db->select('*')
            ->from('tbl_invoices inv')
            ->join('tbl_client cl', 'cl.client_id = inv.client_id')
            ->join('tbl_financial_information fin', 'inv.invoices_id = fin.invoices_id')
            ->join('tbl_bills b', 'b.invoices_id = inv.invoices_id');
        if ($date!='All'){
            /*$this->db->where('b.bill_date', $date);*/
            $this->db->like('b.bill_date', $date);
        }
        if(!empty($search_array)){
            $this->db->or_like($search_array);
        }
        return $this->db->count_all_results();
    }
    

}
