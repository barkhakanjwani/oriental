-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 15, 2019 at 02:18 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clearing_master`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `A_ID` int(11) NOT NULL,
  `H_ID` int(11) NOT NULL,
  `SUB_HEAD_ID` int(11) NOT NULL,
  `CLIENT_ID` int(11) DEFAULT NULL,
  `A_NAME` varchar(100) NOT NULL,
  `A_DESC` text,
  `A_OPENINGTYPE` varchar(10) DEFAULT NULL,
  `A_OPENINGBALANCE` float NOT NULL DEFAULT '0',
  `A_SELFHEAD_ID` int(11) DEFAULT NULL,
  `A_NO` varchar(50) NOT NULL,
  `A_STATUS` tinyint(4) NOT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_BY` int(11) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



-- --------------------------------------------------------

--
-- Table structure for table `accounts_head`
--

CREATE TABLE `accounts_head` (
  `H_ID` int(11) NOT NULL,
  `SUB_HEAD_ID` int(11) NOT NULL,
  `H_TYPE` varchar(20) NOT NULL,
  `H_NAME` varchar(100) NOT NULL,
  `H_NO` varchar(50) NOT NULL,
  `H_STATUS` tinyint(4) NOT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_BY` int(11) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `B_ID` int(11) NOT NULL,
  `B_NAME` varchar(100) NOT NULL,
  `B_LOGO` varchar(100) DEFAULT NULL,
  `B_STATUS` tinyint(4) NOT NULL,
  `B_DATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_BY` int(11) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `A_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `BR_ID` int(11) NOT NULL,
  `B_ID` int(11) NOT NULL,
  `BR_NAME` varchar(100) NOT NULL,
  `BR_CODE` varchar(100) NOT NULL,
  `BR_ADDRESS` text,
  `BR_CITY` int(11) NOT NULL,
  `BR_OPENINGTYPE` varchar(10) DEFAULT NULL,
  `BR_OPENINGBALANCE` float NOT NULL DEFAULT '0',
  `BR_STATUS` tinyint(4) NOT NULL,
  `BR_DATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_BY` int(11) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `A_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cash_and_bank`
--

CREATE TABLE `cash_and_bank` (
  `CB_ID` int(11) NOT NULL,
  `B_ID` int(11) DEFAULT NULL,
  `CB_DATE` date NOT NULL,
  `CB_DEBITACCOUNT` int(11) NOT NULL,
  `CB_CREDITACCOUNT` int(11) NOT NULL,
  `CB_CHEQUENO` varchar(50) DEFAULT NULL,
  `CB_PAY` varchar(20) DEFAULT NULL,
  `CB_AMOUNT` float NOT NULL,
  `CB_TYPE` varchar(10) DEFAULT NULL,
  `CB_PARTICULARS` text,
  `T_ID` int(11) NOT NULL,
  `IS_ACTIVE` tinyint(4) NOT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_BY` int(11) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cheques`
--

CREATE TABLE `cheques` (
  `C_ID` int(11) NOT NULL,
  `BR_ID` int(11) NOT NULL,
  `DB_ACCOUNTID` int(11) NOT NULL,
  `C_PAY` varchar(100) DEFAULT NULL,
  `C_NO` varchar(20) DEFAULT NULL,
  `C_AMOUNT` float NOT NULL,
  `C_AMOUNTWORDS` text NOT NULL,
  `C_DATE` date NOT NULL,
  `C_DESC` text,
  `T_ID` int(11) NOT NULL,
  `AC_PAYEES` tinyint(4) DEFAULT '0',
  `C_STATUS` varchar(20) NOT NULL,
  `C_DATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_BY` int(11) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `client_ledgers1`
-- (See below for the actual view)
--
CREATE TABLE `client_ledgers1` (
`invoice_id` int(11)
,`prefix` varchar(3)
,`bill_no` varchar(100)
,`reference` varchar(100)
,`client` int(11)
,`date` varchar(32)
,`amount` double
,`type` varchar(6)
);

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE `expense` (
  `E_ID` int(11) NOT NULL,
  `INVOICES_ID` int(11) NOT NULL,
  `DEBIT_ACCOUNT` int(11) NOT NULL,
  `CREDIT_ACCOUNT` int(11) NOT NULL,
  `EXPENSE_TYPE` int(11) NOT NULL,
  `VOUCHER_TYPE` enum('Post','Pre','Exp','Gen') DEFAULT NULL,
  `PAID_BY` varchar(50) NOT NULL,
  `PAID_TO` varchar(50) NOT NULL,
  `CHEQUE_NO` varchar(50) DEFAULT NULL,
  `DESCRIPTION` text,
  `AMOUNT` float NOT NULL,
  `AMOUNT_IN_WORDS` text NOT NULL,
  `E_DATE` date NOT NULL,
  `T_ID` int(11) NOT NULL DEFAULT '0',
  `E_STATUS` varchar(20) NOT NULL,
  `IS_ACTIVE` tinyint(4) NOT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `installer`
--

CREATE TABLE `installer` (
  `id` int(1) NOT NULL,
  `installer_flag` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `job_ledgers`
-- (See below for the actual view)
--
CREATE TABLE `job_ledgers` (
`invoice_id` int(11)
,`prefix` varchar(16)
,`reference_no` varchar(100)
,`amount` decimal(33,2)
,`date` date
,`type` varchar(6)
);

-- --------------------------------------------------------

--
-- Table structure for table `payment_vouchers`
--

CREATE TABLE `payment_vouchers` (
  `PV_ID` int(11) NOT NULL,
  `U_ID` int(11) NOT NULL,
  `PAYMENT_MODE` tinyint(1) NOT NULL,
  `ACCOUNT_CREDIT` int(11) NOT NULL,
  `PAID_TO_ACCOUNT` int(11) NOT NULL,
  `PV_REF_TYPE` varchar(5) NOT NULL,
  `PV_REF_NO` varchar(20) NOT NULL,
  `PV_VOUCHER_NO` varchar(20) DEFAULT NULL,
  `PV_INSTRUMENT` varchar(50) DEFAULT NULL,
  `PV_AMOUNT` double NOT NULL,
  `PV_DATE` date NOT NULL,
  `PV_DESC` text NOT NULL,
  `T_ID` int(11) NOT NULL,
  `PV_CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `petty_cash`
--

CREATE TABLE `petty_cash` (
  `PC_ID` int(11) NOT NULL,
  `PC_DATE` date NOT NULL,
  `PC_REF_NO` varchar(20) NOT NULL,
  `PC_VOUCHER_NO` varchar(20) DEFAULT NULL,
  `PC_PAYMENT_MODE` int(11) NOT NULL,
  `PC_PAID_TO` int(11) NOT NULL,
  `PC_PAID_BY` int(11) NOT NULL,
  `PC_DESC` text,
  `T_ID` int(11) NOT NULL,
  `PC_STATUS` enum('Pending','Approved','Cancel') NOT NULL,
  `IS_ACTIVE` tinyint(1) NOT NULL,
  `PC_DATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `petty_cash_details`
--

CREATE TABLE `petty_cash_details` (
  `PCD_ID` int(11) NOT NULL,
  `PC_ID` int(11) NOT NULL,
  `A_ID` int(11) NOT NULL,
  `PCD_AMOUNT` double NOT NULL,
  `PCD_STATUS` tinyint(1) NOT NULL,
  `PCD_DATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reverse_entries`
--

CREATE TABLE `reverse_entries` (
  `reverse_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `voucher_type` varchar(5) NOT NULL,
  `description` text,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sub_heads`
--

CREATE TABLE `sub_heads` (
  `SUB_HEAD_ID` int(11) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `HEAD_TYPE` varchar(20) NOT NULL,
  `SUB_NO` varchar(50) NOT NULL,
  `SUB_STATUS` tinyint(4) NOT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_BY` int(11) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `tbl_account_details`
--

CREATE TABLE `tbl_account_details` (
  `account_details_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `fullname` varchar(160) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(64) COLLATE utf8_unicode_ci DEFAULT '-',
  `phone` varchar(32) COLLATE utf8_unicode_ci DEFAULT '-',
  `mobile` varchar(32) COLLATE utf8_unicode_ci DEFAULT '',
  `skype` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `departments_id` int(11) DEFAULT '0',
  `avatar` varchar(200) COLLATE utf8_unicode_ci DEFAULT 'uploads/default_avatar.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;


-- --------------------------------------------------------

--
-- Table structure for table `tbl_activities`
--

CREATE TABLE `tbl_activities` (
  `activities_id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `module` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module_field_id` int(11) DEFAULT NULL,
  `activity` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activity_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `icon` varchar(32) COLLATE utf8_unicode_ci DEFAULT 'fa-coffee',
  `value1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_advance_payments`
--

CREATE TABLE `tbl_advance_payments` (
  `ap_id` int(11) NOT NULL,
  `expense_type` enum('Job','Misc','Others') NOT NULL DEFAULT 'Job',
  `client_id` int(11) DEFAULT NULL,
  `invoices_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) NOT NULL,
  `debit_account` int(11) NOT NULL,
  `credit_account` int(11) NOT NULL,
  `ref_type` varchar(5) NOT NULL,
  `ref_no` varchar(20) NOT NULL,
  `voucher_no` varchar(20) DEFAULT NULL,
  `payment_method` varchar(50) NOT NULL,
  `instrument_type` varchar(50) DEFAULT NULL,
  `cheque_payorder` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `payment_date` date NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_advance_payment_details`
--

CREATE TABLE `tbl_advance_payment_details` (
  `apd_id` int(11) NOT NULL,
  `ap_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `apd_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `apd_title` varchar(50) NOT NULL,
  `apd_pay_order` varchar(50) NOT NULL,
  `apd_bank` int(11) DEFAULT NULL,
  `apd_branch` int(11) DEFAULT NULL,
  `apd_account_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bills`
--

CREATE TABLE `tbl_bills` (
  `bill_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `bill_no` varchar(100) NOT NULL,
  `delivery_date` date NOT NULL,
  `bill_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bill_details`
--

CREATE TABLE `tbl_bill_details` (
  `bill_detail_id` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL,
  `job_expense_id` int(11) NOT NULL,
  `bill_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `bill_from` varchar(50) NOT NULL,
  `bill_to` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cities`
--

CREATE TABLE `tbl_cities` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_client`
--

CREATE TABLE `tbl_client` (
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_p_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_p_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_p_contact` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_p_designation` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refered_by` int(11) DEFAULT NULL,
  `short_note` text COLLATE utf8_unicode_ci,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ntn` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `strn` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_status` int(11) NOT NULL DEFAULT '1',
  `profile_photo` text COLLATE utf8_unicode_ci,
  `created_by` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_client_documents`
--

CREATE TABLE `tbl_client_documents` (
  `client_document_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `client_document_title` varchar(100) NOT NULL,
  `client_document_date` date NOT NULL,
  `client_document_file` text,
  `client_document_comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_commodity_types`
--

CREATE TABLE `tbl_commodity_types` (
  `commodity_type_id` int(11) NOT NULL,
  `commodity_type` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_saved` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_config`
--

CREATE TABLE `tbl_config` (
  `config_key` varchar(255) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `tbl_consignors`
--

CREATE TABLE `tbl_consignors` (
  `consignor_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `short_note` text COLLATE utf8_unicode_ci,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ntn` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `strn` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `consignor_status` int(11) NOT NULL DEFAULT '1',
  `profile_photo` text COLLATE utf8_unicode_ci,
  `created_by` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_consignor_documents`
--

CREATE TABLE `tbl_consignor_documents` (
  `consignor_document_id` int(11) NOT NULL,
  `consignor_id` int(11) NOT NULL,
  `consignor_document_title` varchar(100) NOT NULL,
  `consignor_document_date` date NOT NULL,
  `consignor_document_file` text,
  `consignor_document_comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_consignor_persons`
--

CREATE TABLE `tbl_consignor_persons` (
  `consignor_person_id` int(11) NOT NULL,
  `consignor_id` int(11) NOT NULL,
  `consignor_person_name` varchar(50) NOT NULL,
  `consignor_person_email` varchar(100) DEFAULT NULL,
  `consignor_person_contact` varchar(50) DEFAULT NULL,
  `consignor_person_designation` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_containers`
--

CREATE TABLE `tbl_containers` (
  `id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `container_type` varchar(100) NOT NULL,
  `container_no` varchar(50) NOT NULL,
  `container_ft` enum('20ft','40ft') NOT NULL,
  `packages` decimal(10,2) NOT NULL DEFAULT '0.00',
  `delivery_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_countries`
--

CREATE TABLE `tbl_countries` (
  `id` int(6) NOT NULL,
  `value` varchar(250) CHARACTER SET latin1 NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `tbl_currencies`
--

CREATE TABLE `tbl_currencies` (
  `code` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `symbol` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `xrate` decimal(12,5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------

--
-- Table structure for table `tbl_custom_duties`
--

CREATE TABLE `tbl_custom_duties` (
  `custom_duty_id` int(11) NOT NULL,
  `requisition_id` int(11) NOT NULL,
  `commodity_id` int(11) NOT NULL,
  `value_per_unit` decimal(10,2) NOT NULL DEFAULT '0.00',
  `duty_cd` decimal(10,2) NOT NULL DEFAULT '0.00',
  `duty_acd` decimal(10,2) NOT NULL DEFAULT '0.00',
  `duty_rd` decimal(10,2) NOT NULL DEFAULT '0.00',
  `duty_st` decimal(10,2) NOT NULL DEFAULT '0.00',
  `duty_ast` decimal(10,2) NOT NULL DEFAULT '0.00',
  `duty_it` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_departments`
--

CREATE TABLE `tbl_departments` (
  `departments_id` int(10) NOT NULL,
  `deptname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------

--
-- Table structure for table `tbl_document_types`
--

CREATE TABLE `tbl_document_types` (
  `document_type_id` int(11) NOT NULL,
  `document_title` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_saved` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_due_dates`
--

CREATE TABLE `tbl_due_dates` (
  `due_date_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `due_date` date DEFAULT NULL,
  `due_time` varchar(100) DEFAULT NULL,
  `due_comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_exchange_rates`
--

CREATE TABLE `tbl_exchange_rates` (
  `exchange_rate_id` int(11) NOT NULL,
  `exchange_rate` decimal(10,2) NOT NULL DEFAULT '0.00',
  `exchange_rate_currency` varchar(50) NOT NULL,
  `exchange_rate_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_invoices`
--

CREATE TABLE `tbl_invoices` (
  `invoices_id` int(11) NOT NULL,
  `type` enum('import','export','others') COLLATE utf8_unicode_ci DEFAULT NULL,
  `job_no` enum('Auto','Manual') COLLATE utf8_unicode_ci NOT NULL,
  `reference_no` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `consignor_id` int(11) NOT NULL,
  `port` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insurance_covernote` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ioco_item` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mode_options` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `origin` int(11) DEFAULT NULL,
  `p_o_l` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `p_o_d` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `vessel` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gross_weight` decimal(10,2) NOT NULL DEFAULT '0.00',
  `net_weight` decimal(10,2) NOT NULL DEFAULT '0.00',
  `unit` enum('KGS','TONS') COLLATE utf8_unicode_ci NOT NULL,
  `igm_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `igm_date` date DEFAULT NULL,
  `index_no` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lc_no` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lc_date` date DEFAULT NULL,
  `bl_no` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `bl_date` date DEFAULT NULL,
  `shipping_line` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forwarding_agent` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_value` decimal(10,2) NOT NULL DEFAULT '0.00',
  `exchange_rate` decimal(10,2) NOT NULL DEFAULT '0.00',
  `incoterm` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eif` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gd_machine` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `paid_packages_charges` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_caa_per_kgs` decimal(10,2) NOT NULL DEFAULT '0.00',
  `paid_commission` decimal(10,2) NOT NULL DEFAULT '0.00',
  `city` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `yard` int(11) DEFAULT NULL,
  `gst` decimal(10,2) NOT NULL DEFAULT '0.00',
  `eta` date NOT NULL,
  `consignor_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `consignor_address` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `commission_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commission_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `unreceipted_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `consignment_type` enum('FCL','LCL','Break Bulk') COLLATE utf8_unicode_ci NOT NULL,
  `break_bulk` enum('Half','Full') COLLATE utf8_unicode_ci DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `currency` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'USD',
  `status` enum('Unpaid','Paid') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Unpaid',
  `job_status` tinyint(4) NOT NULL DEFAULT '1',
  `date_saved` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `show_client` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Yes',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_invoice_documents`
--

CREATE TABLE `tbl_invoice_documents` (
  `document_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `document_title` int(11) NOT NULL,
  `document_date` date DEFAULT NULL,
  `document_file` text,
  `document_type` varchar(50) DEFAULT NULL,
  `document_comment` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_activity`
--

CREATE TABLE `tbl_job_activity` (
  `job_activity_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `job_activity_date` date DEFAULT NULL,
  `job_activity_status` int(11) NOT NULL,
  `job_activity_notification` enum('Yes','No') NOT NULL DEFAULT 'No',
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_expenses`
--

CREATE TABLE `tbl_job_expenses` (
  `job_expense_id` int(11) NOT NULL,
  `job_expense_title` varchar(100) NOT NULL,
  `job_expense_type` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `date_saved` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_status`
--

CREATE TABLE `tbl_job_status` (
  `job_status_id` int(11) NOT NULL,
  `job_status_title` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_saved` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_memos`
--

CREATE TABLE `tbl_memos` (
  `memo_id` int(11) NOT NULL,
  `memo_type` enum('Debit','Credit') NOT NULL,
  `memo_no` varchar(50) NOT NULL,
  `client_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_memo_charges`
--

CREATE TABLE `tbl_memo_charges` (
  `mc_id` int(11) NOT NULL,
  `memo_id` int(11) NOT NULL,
  `mc_title` varchar(100) NOT NULL,
  `mc_amount` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `menu_id` int(11) NOT NULL,
  `label` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `tbl_other_expenses`
--

CREATE TABLE `tbl_other_expenses` (
  `other_expense_id` int(11) NOT NULL,
  `requisition_expense_id` int(11) NOT NULL,
  `requisition_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `other_expense_amount` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payments`
--

CREATE TABLE `tbl_payments` (
  `payments_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `trans_id` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cheque_payorder_no` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payer_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_method` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_type` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL,
  `currency` varchar(64) COLLATE utf8_unicode_ci DEFAULT 'USD',
  `received_by` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_date` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `month_paid` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `year_paid` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paid_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_private_message_send`
--

CREATE TABLE `tbl_private_message_send` (
  `private_message_send_id` int(11) NOT NULL,
  `send_user_id` int(11) NOT NULL,
  `receive_user_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `message_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_requisitions`
--

CREATE TABLE `tbl_requisitions` (
  `requisition_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `landing_charges` enum('Yes','No') NOT NULL DEFAULT 'No',
  `insurance_value` decimal(10,2) NOT NULL DEFAULT '0.00',
  `insurance_type` enum('Percent','Amount') NOT NULL DEFAULT 'Amount',
  `requisition_type` enum('Declared','Assessable') NOT NULL DEFAULT 'Declared',
  `assessable_value` decimal(10,2) NOT NULL DEFAULT '0.00',
  `requisition_note` text,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_requisition_expenses`
--

CREATE TABLE `tbl_requisition_expenses` (
  `requisition_expense_id` int(11) NOT NULL,
  `requisition_expense_title` varchar(100) NOT NULL,
  `requisition_expense_status` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `date_saved` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_saved_commodities`
--

CREATE TABLE `tbl_saved_commodities` (
  `id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `commodity_type` int(11) NOT NULL,
  `commodity` varchar(100) NOT NULL,
  `hs_code` varchar(50) NOT NULL,
  `weight` decimal(10,2) NOT NULL DEFAULT '0.00',
  `weight_unit` varchar(50) NOT NULL,
  `no_of_units` decimal(10,2) NOT NULL DEFAULT '0.00',
  `unit_value` decimal(10,2) NOT NULL DEFAULT '0.00',
  `import_mode` enum('Temporary','Permanent','Re-export') NOT NULL DEFAULT 'Permanent',
  `sro` enum('Yes','No') DEFAULT 'No',
  `sro_no` varchar(50) NOT NULL,
  `serial_no` varchar(50) DEFAULT NULL,
  `sro_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_security_deposit`
--

CREATE TABLE `tbl_security_deposit` (
  `deposit_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `shipping_account` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `name_representative` varchar(255) DEFAULT NULL,
  `container_no` int(11) DEFAULT NULL,
  `deposit_amount` decimal(10,2) DEFAULT '0.00',
  `delivery_date_shipment` date DEFAULT NULL,
  `deposit_return` enum('Yes','No') NOT NULL DEFAULT 'No',
  `deposit_return_amount` decimal(10,2) DEFAULT '0.00',
  `return_transaction_id` int(11) DEFAULT NULL,
  `return_date_container` date DEFAULT NULL,
  `arrival` date DEFAULT NULL,
  `advance_rent` decimal(10,2) NOT NULL DEFAULT '0.00',
  `remaining_rent` decimal(10,2) NOT NULL DEFAULT '0.00',
  `final_rent` decimal(10,2) NOT NULL DEFAULT '0.00',
  `container_detention` decimal(10,2) NOT NULL DEFAULT '0.00',
  `refund_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `expected_date_of_refund` date DEFAULT NULL,
  `credit_account` int(11) DEFAULT NULL,
  `cheque_no` varchar(50) DEFAULT NULL,
  `deposit_file` text,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shipping_calculator`
--

CREATE TABLE `tbl_shipping_calculator` (
  `sc_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `shipping_company` varchar(50) NOT NULL,
  `number_container` int(11) NOT NULL,
  `rent_per_day` decimal(10,2) NOT NULL,
  `totals` decimal(10,2) NOT NULL,
  `exchange_rates` decimal(10,2) NOT NULL,
  `per_day_rent_total` decimal(10,2) NOT NULL,
  `free_day` int(11) NOT NULL,
  `igm_dates` date NOT NULL,
  `shipping_rent_date` date NOT NULL,
  `total_days` int(11) NOT NULL,
  `total_in_usd` decimal(10,2) NOT NULL,
  `total_in_pkr` decimal(10,2) NOT NULL,
  `er_date` date NOT NULL,
  `additional_rent_days` int(11) NOT NULL,
  `additional_rent_in_usd` decimal(10,2) NOT NULL,
  `total_amount_in_pkr` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shipping_line`
--

CREATE TABLE `tbl_shipping_line` (
  `shipping_id` int(11) NOT NULL,
  `shipping_name` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_saved` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staff`
--

CREATE TABLE `tbl_staff` (
  `staff_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `staff_name` varchar(100) NOT NULL,
  `staff_email` varchar(100) DEFAULT NULL,
  `staff_contact` varchar(64) DEFAULT NULL,
  `staff_status` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `staff_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status`
--

CREATE TABLE `tbl_status` (
  `status_id` int(11) NOT NULL,
  `status` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `tbl_supplier`
--

CREATE TABLE `tbl_supplier` (
  `supplier_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_email` varchar(100) DEFAULT NULL,
  `supplier_contact` varchar(64) DEFAULT NULL,
  `supplier_status` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `supplier_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_todo`
--

CREATE TABLE `tbl_todo` (
  `todo_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `user_id` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transfer_amount`
--

CREATE TABLE `tbl_transfer_amount` (
  `tb_id` int(11) NOT NULL,
  `advance_payment_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `credit_account` int(11) NOT NULL,
  `debit_account` int(11) NOT NULL,
  `cheque_payorder` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `transfer_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '2',
  `client_id` int(11) DEFAULT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `banned` tinyint(4) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `online_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = online 0 = offline '
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_role`
--

CREATE TABLE `tbl_user_role` (
  `user_role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_vouchers`
--

CREATE TABLE `tbl_vouchers` (
  `voucher_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `voucher_type` enum('Post','Pre','Exp','Gen') NOT NULL,
  `payment_method` int(11) NOT NULL,
  `ref_no` varchar(20) NOT NULL,
  `voucher_no` varchar(20) DEFAULT NULL,
  `payment_date` date NOT NULL,
  `credit_account` int(11) NOT NULL,
  `paid_to` varchar(32) DEFAULT NULL,
  `cheque_no` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_voucher_details`
--

CREATE TABLE `tbl_voucher_details` (
  `voucher_detail_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `job_expense_id` int(11) NOT NULL,
  `debit_account` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_yards`
--

CREATE TABLE `tbl_yards` (
  `yard_id` int(11) NOT NULL,
  `yard` varchar(100) NOT NULL,
  `yard_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `T_ID` int(11) NOT NULL,
  `T_DATE` date NOT NULL,
  `T_PAY` varchar(200) DEFAULT NULL,
  `T_TYPE` varchar(50) DEFAULT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_BY` int(11) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transactions_meta`
--

CREATE TABLE `transactions_meta` (
  `TM_ID` int(11) NOT NULL,
  `T_ID` int(11) NOT NULL,
  `A_ID` int(11) NOT NULL,
  `PAYMENT_ID` int(11) DEFAULT NULL,
  `PARTICULARS` text,
  `TM_AMOUNT` double DEFAULT NULL,
  `TM_TYPE` varchar(10) NOT NULL,
  `IS_ACTIVE` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stbjobtype`
--

CREATE TABLE `stbjobtype` (
  `Iid` int(11) NOT NULL,
  `VCJobType` varchar(50) DEFAULT NULL,
  `VCPrefix` varchar(50) NOT NULL,
  `VCSeparator` varchar(50) NOT NULL,
  `VCStartFrom` varchar(50) NOT NULL,
  `DTCreatedDate` datetime NOT NULL DEFAULT current_timestamp(),
  `ICreatedBy` int(11) DEFAULT NULL,
  `DTUpdatedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `IUpdatedBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `tbl_financial_information` (
  `financial_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `invoice_no` varchar(22) NOT NULL,
  `invoice_date` date DEFAULT NULL,
  `payment_term` enum('LC','Without LC') NOT NULL,
  `lc_no` varchar(22) DEFAULT NULL,
  `lc_date` date DEFAULT NULL,
  `lc_bank` varchar(50) DEFAULT NULL,
  `incoterm` int(11) DEFAULT NULL,
  `currency` varchar(22) NOT NULL,
  `fob_value` double(10,2) NOT NULL DEFAULT '0.00',
  `freight` decimal(10,2) NOT NULL DEFAULT '0.00',
  `other_charges` decimal(10,2) NOT NULL DEFAULT '0.00',
  `exchange_rate` decimal(10,2) NOT NULL DEFAULT '0.00',
  `eif_no` varchar(22) DEFAULT NULL,
  `bank` varchar(22) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tbl_sheds` (
  `shed_id` int(11) NOT NULL,
  `shed` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Structure for view `client_ledgers1`
--
DROP TABLE IF EXISTS `client_ledgers1`;

CREATE VIEW `client_ledgers1`  AS  select `bl`.`invoices_id` AS `invoice_id`,'BL ' AS `prefix`,`bl`.`bill_no` AS `bill_no`,'' AS `reference`,`inv`.`client_id` AS `client`,`bl`.`delivery_date` AS `date`,`tr`.`TM_AMOUNT` AS `amount`,'Debit' AS `type` from (((`tbl_bills` `bl` join `tbl_invoices` `inv` on(`bl`.`invoices_id` = `inv`.`invoices_id`)) join `transactions_meta` `tr` on(`bl`.`transaction_id` = `tr`.`T_ID`)) join `accounts` `ac` on(`tr`.`A_ID` = `ac`.`A_ID`)) where `ac`.`A_NAME` = 'Account Receivables' and `tr`.`TM_TYPE` = 'Debit' union select `pay`.`invoices_id` AS `invoice_id`,'' AS `prefix`,'' AS `bill_no`,'Pay Receipt' AS `reference`,`inv`.`client_id` AS `client`,`pay`.`payment_date` AS `date`,`pay`.`amount` AS `amount`,'Credit' AS `type` from (`tbl_payments` `pay` join `tbl_invoices` `inv` on(`pay`.`invoices_id` = `inv`.`invoices_id`)) union select `ap`.`invoices_id` AS `invoice_id`,'' AS `prefix`,'' AS `bill_no`,'Payment Received' AS `reference`,if(`inv`.`client_id` <> '',`inv`.`client_id`,`cl`.`client_id`) AS `client`,`ap`.`payment_date` AS `date`,if((select sum(`tbl_advance_payment_details`.`apd_amount`) from `tbl_advance_payment_details` where `tbl_advance_payment_details`.`apd_title` = 'Security Deposit' and `tbl_advance_payment_details`.`ap_id` = `ap`.`ap_id`) <> '',`ap`.`amount` - (select sum(`tbl_advance_payment_details`.`apd_amount`) from `tbl_advance_payment_details` where `tbl_advance_payment_details`.`apd_title` = 'Security Deposit' and `tbl_advance_payment_details`.`ap_id` = `ap`.`ap_id`),`ap`.`amount`) AS `amount`,'Credit' AS `type` from ((`tbl_advance_payments` `ap` left join `tbl_invoices` `inv` on(`ap`.`invoices_id` = `inv`.`invoices_id`)) left join `tbl_client` `cl` on(`ap`.`client_id` = `cl`.`client_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `job_ledgers`
--
DROP TABLE IF EXISTS `job_ledgers`;

CREATE VIEW `job_ledgers`  AS  select `ap`.`invoices_id` AS `invoice_id`,'Payment Received' AS `prefix`,`ap`.`payment_method` AS `reference_no`,if(((select sum(`tbl_advance_payment_details`.`apd_amount`) from `tbl_advance_payment_details` where ((`tbl_advance_payment_details`.`apd_title` = 'Security Deposit') and (`tbl_advance_payment_details`.`ap_id` = `ap`.`ap_id`))) <> ''),(`ap`.`amount` - (select sum(`tbl_advance_payment_details`.`apd_amount`) from `tbl_advance_payment_details` where ((`tbl_advance_payment_details`.`apd_title` = 'Security Deposit') and (`tbl_advance_payment_details`.`ap_id` = `ap`.`ap_id`)))),`ap`.`amount`) AS `amount`,`ap`.`payment_date` AS `date`,'Credit' AS `type` from (`tbl_advance_payments` `ap` join `tbl_invoices` `inv` on((`ap`.`invoices_id` = `inv`.`invoices_id`))) union select `vd`.`invoices_id` AS `invoice_id`,'' AS `prefix`,`je`.`job_expense_title` AS `reference_no`,`vd`.`amount` AS `amount`,`v`.`payment_date` AS `date`,'Debit' AS `type` from ((`tbl_voucher_details` `vd` join `tbl_vouchers` `v` on((`v`.`voucher_id` = `vd`.`voucher_id`))) join `tbl_job_expenses` `je` on((`vd`.`job_expense_id` = `je`.`job_expense_id`))) ;

--
-- Indexes for dumped tables
--

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
