-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 18, 2020 at 11:04 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nbs`
--

-- --------------------------------------------------------

--
-- Table structure for table `stbdeliverysection`
--

CREATE TABLE `stbdeliverysection` (
  `Iid` int(11) NOT NULL,
  `IInvoicesId` int(11) DEFAULT NULL,
  `IContainerId` int(11) DEFAULT NULL,
  `VCTruckNo` varchar(50) DEFAULT NULL,
  `VCBilty` varchar(50) DEFAULT NULL,
  `DTDeliveryDate` date DEFAULT NULL,
  `DTDateReceiptSite` date DEFAULT NULL,
  `DCWeightDeclare` decimal(10,2) DEFAULT 0.00,
  `DCWeightFound` decimal(10,2) NOT NULL DEFAULT 0.00,
  `VCTransporter` varchar(50) DEFAULT NULL,
  `VCPkgsReceive` varchar(50) DEFAULT NULL,
  `TRemarks` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `stbdeliverysection`
--
ALTER TABLE `stbdeliverysection`
  ADD PRIMARY KEY (`Iid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `stbdeliverysection`
--
ALTER TABLE `stbdeliverysection`
  MODIFY `Iid` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
