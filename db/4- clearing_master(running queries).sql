/*** YARD ***/
ALTER TABLE `tbl_yards` DROP `yard_date`;

ALTER TABLE `tbl_yards` ADD `created_by` INT NOT NULL AFTER `yard`, ADD `created_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_by`, ADD `updated_by` INT NULL AFTER `created_date`, ADD `updated_at` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP AFTER `updated_by`;

/*** INCOTERM ***/
CREATE TABLE `tbl_incoterms` (
  `incoterm_id` int(11) NOT NULL,
  `incoterm` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*** WEIGHT UNIT ***/
CREATE TABLE `tbl_weight_units` (
  `weight_unit_id` int(11) NOT NULL,
  `weight_unit` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*** GD TYPE ***/
CREATE TABLE `tbl_gd_types` (
  `gd_type_id` int(11) NOT NULL,
  `gd_type` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*** TBL_INVOICES ***/
ALTER TABLE `tbl_invoices` CHANGE `mode_options` `mode_options` INT NULL DEFAULT NULL, CHANGE `unit` `unit` INT NULL DEFAULT NULL, CHANGE `incoterm` `incoterm` INT NULL DEFAULT NULL;

/*** tbl_saved_commodities ***/
ALTER TABLE `tbl_saved_commodities` CHANGE `weight_unit` `weight_unit` INT NULL DEFAULT NULL;



/*** TBL_INVOICES ***/
ALTER TABLE `tbl_invoices` DROP `gst`, DROP `commission_type`, DROP `commission_amount`, DROP `unreceipted_amount`;

/*** TBL_BILLS ***/
ALTER TABLE `tbl_bills` ADD `service_charges` DECIMAL(10,2) NOT NULL DEFAULT '0.00' AFTER `delivery_date`, ADD `gst` DECIMAL(10,2) NOT NULL DEFAULT '0.00' AFTER `service_charges`;

ALTER TABLE `tbl_advance_payments` ADD `payments_id` INT NULL DEFAULT NULL AFTER `transaction_id`;

ALTER TABLE `tbl_incoterms` ADD PRIMARY KEY(`incoterm_id`);
ALTER TABLE `tbl_incoterms` CHANGE `incoterm_id` `incoterm_id` INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `tbl_gd_types` ADD PRIMARY KEY(`gd_type_id`);
ALTER TABLE `tbl_gd_types` CHANGE `gd_type_id` `gd_type_id` INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `tbl_weight_units` ADD PRIMARY KEY(`weight_unit_id`);
ALTER TABLE `tbl_weight_units` CHANGE `weight_unit_id` `weight_unit_id` INT(11) NOT NULL AUTO_INCREMENT;

INSERT INTO `tbl_menu` (`menu_id`, `label`, `link`, `icon`, `parent`, `sort`) VALUES (NULL, 'sales_tax_report', 'admin/report/sales_tax_report', 'fa fa-circle-o', '84', '4');

ALTER TABLE `tbl_advance_payments` ADD `created_by` INT NOT NULL AFTER `payment_date`;

ALTER TABLE `tbl_shipping_calculator` CHANGE `rent_per_day` `rent_per_day` DOUBLE NOT NULL, CHANGE `totals` `totals` DOUBLE NOT NULL, CHANGE `exchange_rates` `exchange_rates` DOUBLE NOT NULL, CHANGE `per_day_rent_total` `per_day_rent_total` DOUBLE NOT NULL, CHANGE `total_in_usd` `total_in_usd` DOUBLE NOT NULL, CHANGE `total_in_pkr` `total_in_pkr` DOUBLE NOT NULL, CHANGE `additional_rent_in_usd` `additional_rent_in_usd` DOUBLE NOT NULL, CHANGE `total_amount_in_pkr` `total_amount_in_pkr` DOUBLE NOT NULL;

ALTER TABLE `tbl_custom_duties` ADD `landing_charges` ENUM('Yes', 'No') NOT NULL DEFAULT 'No' AFTER `commodity_id`, ADD `insurance_value` DECIMAL(10,2) NOT NULL DEFAULT '0.00' AFTER `landing_charges`, ADD `insurance_type` ENUM('Percent', 'Amount') NOT NULL DEFAULT 'Amount' AFTER `insurance_value`;
ALTER TABLE `tbl_custom_duties` ADD `duty_fed` DECIMAL(10,2) NOT NULL DEFAULT '0.00' AFTER `duty_cd`;

ALTER TABLE `tbl_requisitions`
  DROP `landing_charges`,
  DROP `insurance_value`,
  DROP `insurance_type`,
  DROP `assessable_value`;

ALTER TABLE `tbl_bills` ADD `withholding_tax` DECIMAL(10,2) NOT NULL DEFAULT '0.00' AFTER `gst`;
ALTER TABLE `tbl_invoices` CHANGE `type` `type` INT NOT NULL;
INSERT INTO `tbl_incoterms` (`incoterm_id`, `incoterm`, `created_by`, `created_date`, `updated_by`, `updated_at`) VALUES (NULL, 'FOB', '1', CURRENT_TIMESTAMP, NULL, NULL);

ALTER TABLE `tbl_containers` DROP `delivery_date`;
ALTER TABLE `tbl_containers` ADD `container_shed` INT NULL AFTER `packages`, ADD `container_gross_weight` DECIMAL(10,2) NOT NULL DEFAULT '0.00' AFTER `container_shed`, ADD `container_net_weight` DECIMAL(10,2) NOT NULL DEFAULT '0.00' AFTER `container_gross_weight`, ADD `container_total_weight` DECIMAL(10,2) NOT NULL DEFAULT '0.00' AFTER `container_net_weight`;
ALTER TABLE `tbl_containers` CHANGE `container_ft` `container_ft` ENUM('20ft','40ft','45ft','Open Top') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

ALTER TABLE `tbl_saved_commodities` DROP `weight`;
ALTER TABLE `tbl_saved_commodities` DROP `import_mode`;
ALTER TABLE `tbl_saved_commodities` ADD `no_of_pcs` DECIMAL(10,2) NOT NULL DEFAULT '0.00' AFTER `unit_value`;
ALTER TABLE `tbl_saved_commodities` ADD `sro_remarks` TEXT NULL AFTER `sro_date`;
ALTER TABLE `tbl_saved_commodities` ADD `temporary_import` ENUM('Yes', 'No') NOT NULL DEFAULT 'No' AFTER `sro_remarks`, ADD `security` ENUM('PDC','BG','IB','CG') NULL DEFAULT NULL AFTER `temporary_import`, ADD `instrument_no` VARCHAR(22) NOT NULL AFTER `security`, ADD `temporary_amount` DECIMAL(10,2) NOT NULL DEFAULT '0.00' AFTER `instrument_no`, ADD `date_of_issue` DATE NULL DEFAULT NULL AFTER `temporary_amount`, ADD `date_of_expiry` DATE NULL DEFAULT NULL AFTER `date_of_issue`, ADD `shipping_bill` VARCHAR(22) NOT NULL AFTER `date_of_expiry`, ADD `shipping_date` DATE NULL DEFAULT NULL AFTER `shipping_bill`, ADD `permanent_retention` VARCHAR(22) NOT NULL AFTER `shipping_date`;
ALTER TABLE `tbl_saved_commodities` ADD `temporary_remarks` TEXT NULL DEFAULT NULL AFTER `permanent_retention`;
ALTER TABLE `tbl_saved_commodities` ADD `unit_value_assessed` DECIMAL(10,2) NOT NULL DEFAULT '0.00' AFTER `unit_value`;
ALTER TABLE `tbl_saved_commodities` CHANGE `commodity` `commodity` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

ALTER TABLE `tbl_invoices` CHANGE `consignment_type` `consignment_type` ENUM('FCL','LCL','Break Bulk','By Air') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE `tbl_invoices`
  DROP `insurance_covernote`,
  DROP `ioco_item`,
  DROP `origin`,
  DROP `vessel`,
  DROP `unit`,
  DROP `lc_no`,
  DROP `lc_date`,
  DROP `gd_machine`,
  DROP `paid_packages_charges`,
  DROP `total_caa_per_kgs`,
  DROP `paid_commission`,
  DROP `city`,
  DROP `yard`,
  DROP `description`,
  DROP `break_bulk`;

  ALTER TABLE `tbl_invoices` ADD `collectorate` VARCHAR(22) NULL AFTER `consignor_id`;
  ALTER TABLE `tbl_invoices` ADD `project_name` VARCHAR(50) NULL DEFAULT NULL AFTER `collectorate`;
  ALTER TABLE `tbl_invoices` ADD `total_pkgs` DECIMAL(10,2) NOT NULL DEFAULT '0.00' AFTER `net_weight`;
  ALTER TABLE `tbl_invoices` DROP `incoterm`;

  INSERT INTO `tbl_menu` (`menu_id`, `label`, `link`, `icon`, `parent`, `sort`) VALUES (NULL, 'enter_delivery', 'admin/invoice/manage_delivery', 'fa fa-circle-o', '62', '4'), (NULL, 'list_delivery', 'admin/invoice/all_delivery', 'fa fa-circle-o', '62', '5');
  INSERT INTO `tbl_menu` (`menu_id`, `label`, `link`, `icon`, `parent`, `sort`) VALUES (NULL, 'suspense_payment', 'admin/accounts/manage_suspense_payment', 'fa fa-circle-o', '111', '19');
