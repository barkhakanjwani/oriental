--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`A_ID`),
  ADD KEY `H_ID` (`H_ID`),
  ADD KEY `SUB_HEAD_ID` (`SUB_HEAD_ID`),
  ADD KEY `CLIENT_ID` (`CLIENT_ID`),
  ADD KEY `CREATED_BY` (`CREATED_BY`),
  ADD KEY `UPDATED_BY` (`UPDATED_BY`);

--
-- Indexes for table `accounts_head`
--
ALTER TABLE `accounts_head`
  ADD PRIMARY KEY (`H_ID`),
  ADD KEY `SUB_HEAD_ID` (`SUB_HEAD_ID`),
  ADD KEY `CREATED_BY` (`CREATED_BY`),
  ADD KEY `UPDATED_BY` (`UPDATED_BY`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`B_ID`),
  ADD KEY `A_ID` (`A_ID`),
  ADD KEY `CREATED_BY` (`CREATED_BY`),
  ADD KEY `UPDATED_BY` (`UPDATED_BY`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`BR_ID`),
  ADD KEY `A_ID` (`A_ID`),
  ADD KEY `B_ID` (`B_ID`),
  ADD KEY `BR_CITY` (`BR_CITY`),
  ADD KEY `CREATED_BY` (`CREATED_BY`),
  ADD KEY `UPDATED_BY` (`UPDATED_BY`);

--
-- Indexes for table `cash_and_bank`
--
ALTER TABLE `cash_and_bank`
  ADD PRIMARY KEY (`CB_ID`),
  ADD KEY `T_ID` (`T_ID`),
  ADD KEY `CB_CREDITACCOUNT` (`CB_CREDITACCOUNT`),
  ADD KEY `CB_DEBITACCOUNT` (`CB_DEBITACCOUNT`),
  ADD KEY `B_ID` (`B_ID`),
  ADD KEY `CREATED_BY` (`CREATED_BY`),
  ADD KEY `UPDATED_BY` (`UPDATED_BY`);

--
-- Indexes for table `cheques`
--
ALTER TABLE `cheques`
  ADD PRIMARY KEY (`C_ID`),
  ADD KEY `BR_ID` (`BR_ID`),
  ADD KEY `DB_ACCOUNTID` (`DB_ACCOUNTID`),
  ADD KEY `T_ID` (`T_ID`),
  ADD KEY `CREATED_BY` (`CREATED_BY`),
  ADD KEY `UPDATE_BY` (`UPDATED_BY`);

--
-- Indexes for table `expense`
--
ALTER TABLE `expense`
  ADD PRIMARY KEY (`E_ID`),
  ADD KEY `CREDIT_ACCOUNT` (`CREDIT_ACCOUNT`),
  ADD KEY `DEBIT_ACCOUNT` (`DEBIT_ACCOUNT`),
  ADD KEY `T_ID` (`T_ID`),
  ADD KEY `INVOICES_ID` (`INVOICES_ID`);

--
-- Indexes for table `installer`
--
ALTER TABLE `installer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_vouchers`
--
ALTER TABLE `payment_vouchers`
  ADD PRIMARY KEY (`PV_ID`);

--
-- Indexes for table `petty_cash`
--
ALTER TABLE `petty_cash`
  ADD PRIMARY KEY (`PC_ID`);

--
-- Indexes for table `petty_cash_details`
--
ALTER TABLE `petty_cash_details`
  ADD PRIMARY KEY (`PCD_ID`);

--
-- Indexes for table `reverse_entries`
--
ALTER TABLE `reverse_entries`
  ADD PRIMARY KEY (`reverse_id`);

--
-- Indexes for table `sub_heads`
--
ALTER TABLE `sub_heads`
  ADD PRIMARY KEY (`SUB_HEAD_ID`),
  ADD KEY `CREATED_BY` (`CREATED_BY`),
  ADD KEY `UPDATED_BY` (`UPDATED_BY`);

--
-- Indexes for table `tbl_account_details`
--
ALTER TABLE `tbl_account_details`
  ADD PRIMARY KEY (`account_details_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tbl_activities`
--
ALTER TABLE `tbl_activities`
  ADD PRIMARY KEY (`activities_id`);

--
-- Indexes for table `tbl_advance_payments`
--
ALTER TABLE `tbl_advance_payments`
  ADD PRIMARY KEY (`ap_id`);

--
-- Indexes for table `tbl_advance_payment_details`
--
ALTER TABLE `tbl_advance_payment_details`
  ADD PRIMARY KEY (`apd_id`);

--
-- Indexes for table `tbl_bills`
--
ALTER TABLE `tbl_bills`
  ADD PRIMARY KEY (`bill_id`),
  ADD KEY `transaction_id` (`transaction_id`),
  ADD KEY `invoices_id` (`invoices_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_bill_details`
--
ALTER TABLE `tbl_bill_details`
  ADD PRIMARY KEY (`bill_detail_id`),
  ADD KEY `bill_id` (`bill_id`),
  ADD KEY `job_expense_id` (`job_expense_id`);

--
-- Indexes for table `tbl_cities`
--
ALTER TABLE `tbl_cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_client`
--
ALTER TABLE `tbl_client`
  ADD PRIMARY KEY (`client_id`),
  ADD UNIQUE KEY `Client Email` (`email`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `refered_by` (`refered_by`);

--
-- Indexes for table `tbl_client_documents`
--
ALTER TABLE `tbl_client_documents`
  ADD PRIMARY KEY (`client_document_id`),
  ADD KEY `client_id` (`client_id`);

--
-- Indexes for table `tbl_commodity_types`
--
ALTER TABLE `tbl_commodity_types`
  ADD PRIMARY KEY (`commodity_type_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_config`
--
ALTER TABLE `tbl_config`
  ADD PRIMARY KEY (`config_key`);

--
-- Indexes for table `tbl_consignors`
--
ALTER TABLE `tbl_consignors`
  ADD PRIMARY KEY (`consignor_id`),
  ADD UNIQUE KEY `Client Email` (`email`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `refered_by` (`client_id`);

--
-- Indexes for table `tbl_consignor_documents`
--
ALTER TABLE `tbl_consignor_documents`
  ADD PRIMARY KEY (`consignor_document_id`),
  ADD KEY `client_id` (`consignor_id`);

--
-- Indexes for table `tbl_consignor_persons`
--
ALTER TABLE `tbl_consignor_persons`
  ADD PRIMARY KEY (`consignor_person_id`),
  ADD KEY `consignor_id` (`consignor_id`);

--
-- Indexes for table `tbl_containers`
--
ALTER TABLE `tbl_containers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_id` (`invoices_id`);

--
-- Indexes for table `tbl_countries`
--
ALTER TABLE `tbl_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_currencies`
--
ALTER TABLE `tbl_currencies`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `tbl_custom_duties`
--
ALTER TABLE `tbl_custom_duties`
  ADD PRIMARY KEY (`custom_duty_id`),
  ADD KEY `invoices_id` (`requisition_id`),
  ADD KEY `commodity_id` (`commodity_id`);

--
-- Indexes for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  ADD PRIMARY KEY (`departments_id`);

--
-- Indexes for table `tbl_document_types`
--
ALTER TABLE `tbl_document_types`
  ADD PRIMARY KEY (`document_type_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_due_dates`
--
ALTER TABLE `tbl_due_dates`
  ADD PRIMARY KEY (`due_date_id`),
  ADD KEY `invoices_id` (`invoices_id`);

--
-- Indexes for table `tbl_exchange_rates`
--
ALTER TABLE `tbl_exchange_rates`
  ADD PRIMARY KEY (`exchange_rate_id`);

--
-- Indexes for table `tbl_invoices`
--
ALTER TABLE `tbl_invoices`
  ADD PRIMARY KEY (`invoices_id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `supplier_id` (`supplier_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_invoice_documents`
--
ALTER TABLE `tbl_invoice_documents`
  ADD PRIMARY KEY (`document_id`),
  ADD KEY `invoices_id` (`invoices_id`),
  ADD KEY `document_title` (`document_title`);

--
-- Indexes for table `tbl_job_activity`
--
ALTER TABLE `tbl_job_activity`
  ADD PRIMARY KEY (`job_activity_id`),
  ADD KEY `invoices_id` (`invoices_id`),
  ADD KEY `job_activity_status` (`job_activity_status`);

--
-- Indexes for table `tbl_job_expenses`
--
ALTER TABLE `tbl_job_expenses`
  ADD PRIMARY KEY (`job_expense_id`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `tbl_job_status`
--
ALTER TABLE `tbl_job_status`
  ADD PRIMARY KEY (`job_status_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_memos`
--
ALTER TABLE `tbl_memos`
  ADD PRIMARY KEY (`memo_id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `invoices_id` (`invoices_id`);

--
-- Indexes for table `tbl_memo_charges`
--
ALTER TABLE `tbl_memo_charges`
  ADD PRIMARY KEY (`mc_id`),
  ADD KEY `memo_id` (`memo_id`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `tbl_other_expenses`
--
ALTER TABLE `tbl_other_expenses`
  ADD PRIMARY KEY (`other_expense_id`),
  ADD KEY `invoices_id` (`invoices_id`),
  ADD KEY `requisition_id` (`requisition_id`),
  ADD KEY `requisition_expense_id` (`requisition_expense_id`);

--
-- Indexes for table `tbl_payments`
--
ALTER TABLE `tbl_payments`
  ADD PRIMARY KEY (`payments_id`),
  ADD KEY `invoices_id` (`invoices_id`),
  ADD KEY `paid_by` (`paid_by`);

--
-- Indexes for table `tbl_private_message_send`
--
ALTER TABLE `tbl_private_message_send`
  ADD PRIMARY KEY (`private_message_send_id`);

--
-- Indexes for table `tbl_requisitions`
--
ALTER TABLE `tbl_requisitions`
  ADD PRIMARY KEY (`requisition_id`),
  ADD KEY `invoices_id` (`invoices_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_requisition_expenses`
--
ALTER TABLE `tbl_requisition_expenses`
  ADD PRIMARY KEY (`requisition_expense_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_saved_commodities`
--
ALTER TABLE `tbl_saved_commodities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_id` (`invoices_id`),
  ADD KEY `commodity_type` (`commodity_type`);

--
-- Indexes for table `tbl_security_deposit`
--
ALTER TABLE `tbl_security_deposit`
  ADD PRIMARY KEY (`deposit_id`),
  ADD KEY `invoices_id` (`invoices_id`);

--
-- Indexes for table `tbl_shipping_calculator`
--
ALTER TABLE `tbl_shipping_calculator`
  ADD PRIMARY KEY (`sc_id`);

--
-- Indexes for table `tbl_shipping_line`
--
ALTER TABLE `tbl_shipping_line`
  ADD PRIMARY KEY (`shipping_id`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
  ADD PRIMARY KEY (`staff_id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_status`
--
ALTER TABLE `tbl_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  ADD PRIMARY KEY (`supplier_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_todo`
--
ALTER TABLE `tbl_todo`
  ADD PRIMARY KEY (`todo_id`);

--
-- Indexes for table `tbl_transfer_amount`
--
ALTER TABLE `tbl_transfer_amount`
  ADD PRIMARY KEY (`tb_id`),
  ADD KEY `transaction_id` (`transaction_id`),
  ADD KEY `advance_payment_id` (`advance_payment_id`),
  ADD KEY `credit_account` (`credit_account`),
  ADD KEY `debit_account` (`debit_account`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `tbl_user_role`
--
ALTER TABLE `tbl_user_role`
  ADD PRIMARY KEY (`user_role_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `menu_id` (`menu_id`);

--
-- Indexes for table `tbl_vouchers`
--
ALTER TABLE `tbl_vouchers`
  ADD PRIMARY KEY (`voucher_id`);

--
-- Indexes for table `tbl_voucher_details`
--
ALTER TABLE `tbl_voucher_details`
  ADD PRIMARY KEY (`voucher_detail_id`);

--
-- Indexes for table `tbl_yards`
--
ALTER TABLE `tbl_yards`
  ADD PRIMARY KEY (`yard_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`T_ID`),
  ADD KEY `CREATED_BY` (`CREATED_BY`),
  ADD KEY `UPDATED_BY` (`UPDATED_BY`);

--
-- Indexes for table `transactions_meta`
--
ALTER TABLE `transactions_meta`
  ADD PRIMARY KEY (`TM_ID`),
  ADD KEY `T_ID` (`T_ID`),
  ADD KEY `transactions_meta_ibfk_2` (`A_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `A_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `accounts_head`
--
ALTER TABLE `accounts_head`
  MODIFY `H_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `B_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `BR_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cash_and_bank`
--
ALTER TABLE `cash_and_bank`
  MODIFY `CB_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cheques`
--
ALTER TABLE `cheques`
  MODIFY `C_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expense`
--
ALTER TABLE `expense`
  MODIFY `E_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_vouchers`
--
ALTER TABLE `payment_vouchers`
  MODIFY `PV_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `petty_cash`
--
ALTER TABLE `petty_cash`
  MODIFY `PC_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `petty_cash_details`
--
ALTER TABLE `petty_cash_details`
  MODIFY `PCD_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reverse_entries`
--
ALTER TABLE `reverse_entries`
  MODIFY `reverse_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_heads`
--
ALTER TABLE `sub_heads`
  MODIFY `SUB_HEAD_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tbl_account_details`
--
ALTER TABLE `tbl_account_details`
  MODIFY `account_details_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_activities`
--
ALTER TABLE `tbl_activities`
  MODIFY `activities_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_advance_payments`
--
ALTER TABLE `tbl_advance_payments`
  MODIFY `ap_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_advance_payment_details`
--
ALTER TABLE `tbl_advance_payment_details`
  MODIFY `apd_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_bills`
--
ALTER TABLE `tbl_bills`
  MODIFY `bill_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_bill_details`
--
ALTER TABLE `tbl_bill_details`
  MODIFY `bill_detail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_cities`
--
ALTER TABLE `tbl_cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `tbl_client`
--
ALTER TABLE `tbl_client`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_client_documents`
--
ALTER TABLE `tbl_client_documents`
  MODIFY `client_document_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_commodity_types`
--
ALTER TABLE `tbl_commodity_types`
  MODIFY `commodity_type_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_consignors`
--
ALTER TABLE `tbl_consignors`
  MODIFY `consignor_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_consignor_documents`
--
ALTER TABLE `tbl_consignor_documents`
  MODIFY `consignor_document_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_consignor_persons`
--
ALTER TABLE `tbl_consignor_persons`
  MODIFY `consignor_person_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_containers`
--
ALTER TABLE `tbl_containers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_countries`
--
ALTER TABLE `tbl_countries`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=242;

--
-- AUTO_INCREMENT for table `tbl_custom_duties`
--
ALTER TABLE `tbl_custom_duties`
  MODIFY `custom_duty_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  MODIFY `departments_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_document_types`
--
ALTER TABLE `tbl_document_types`
  MODIFY `document_type_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_due_dates`
--
ALTER TABLE `tbl_due_dates`
  MODIFY `due_date_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_exchange_rates`
--
ALTER TABLE `tbl_exchange_rates`
  MODIFY `exchange_rate_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_invoices`
--
ALTER TABLE `tbl_invoices`
  MODIFY `invoices_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_invoice_documents`
--
ALTER TABLE `tbl_invoice_documents`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_job_activity`
--
ALTER TABLE `tbl_job_activity`
  MODIFY `job_activity_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_job_expenses`
--
ALTER TABLE `tbl_job_expenses`
  MODIFY `job_expense_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_job_status`
--
ALTER TABLE `tbl_job_status`
  MODIFY `job_status_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_memos`
--
ALTER TABLE `tbl_memos`
  MODIFY `memo_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_memo_charges`
--
ALTER TABLE `tbl_memo_charges`
  MODIFY `mc_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `tbl_other_expenses`
--
ALTER TABLE `tbl_other_expenses`
  MODIFY `other_expense_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_payments`
--
ALTER TABLE `tbl_payments`
  MODIFY `payments_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_private_message_send`
--
ALTER TABLE `tbl_private_message_send`
  MODIFY `private_message_send_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_requisitions`
--
ALTER TABLE `tbl_requisitions`
  MODIFY `requisition_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_requisition_expenses`
--
ALTER TABLE `tbl_requisition_expenses`
  MODIFY `requisition_expense_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_saved_commodities`
--
ALTER TABLE `tbl_saved_commodities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_security_deposit`
--
ALTER TABLE `tbl_security_deposit`
  MODIFY `deposit_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_shipping_calculator`
--
ALTER TABLE `tbl_shipping_calculator`
  MODIFY `sc_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_shipping_line`
--
ALTER TABLE `tbl_shipping_line`
  MODIFY `shipping_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
  MODIFY `staff_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_status`
--
ALTER TABLE `tbl_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_todo`
--
ALTER TABLE `tbl_todo`
  MODIFY `todo_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_transfer_amount`
--
ALTER TABLE `tbl_transfer_amount`
  MODIFY `tb_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_user_role`
--
ALTER TABLE `tbl_user_role`
  MODIFY `user_role_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_vouchers`
--
ALTER TABLE `tbl_vouchers`
  MODIFY `voucher_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_voucher_details`
--
ALTER TABLE `tbl_voucher_details`
  MODIFY `voucher_detail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_yards`
--
ALTER TABLE `tbl_yards`
  MODIFY `yard_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `T_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transactions_meta`
--
ALTER TABLE `transactions_meta`
  MODIFY `TM_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`H_ID`) REFERENCES `accounts_head` (`H_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `accounts_ibfk_2` FOREIGN KEY (`SUB_HEAD_ID`) REFERENCES `sub_heads` (`SUB_HEAD_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `accounts_ibfk_3` FOREIGN KEY (`CREATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `accounts_ibfk_4` FOREIGN KEY (`UPDATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `accounts_ibfk_5` FOREIGN KEY (`CLIENT_ID`) REFERENCES `tbl_client` (`client_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `accounts_head`
--
ALTER TABLE `accounts_head`
  ADD CONSTRAINT `accounts_head_ibfk_1` FOREIGN KEY (`SUB_HEAD_ID`) REFERENCES `sub_heads` (`SUB_HEAD_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `accounts_head_ibfk_2` FOREIGN KEY (`CREATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `accounts_head_ibfk_3` FOREIGN KEY (`UPDATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `banks`
--
ALTER TABLE `banks`
  ADD CONSTRAINT `banks_ibfk_1` FOREIGN KEY (`A_ID`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `banks_ibfk_2` FOREIGN KEY (`CREATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `banks_ibfk_3` FOREIGN KEY (`UPDATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `branches`
--
ALTER TABLE `branches`
  ADD CONSTRAINT `branches_ibfk_1` FOREIGN KEY (`A_ID`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `branches_ibfk_2` FOREIGN KEY (`B_ID`) REFERENCES `banks` (`B_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `branches_ibfk_3` FOREIGN KEY (`BR_CITY`) REFERENCES `tbl_cities` (`id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `branches_ibfk_4` FOREIGN KEY (`CREATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `branches_ibfk_5` FOREIGN KEY (`UPDATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `cash_and_bank`
--
ALTER TABLE `cash_and_bank`
  ADD CONSTRAINT `cash_and_bank_ibfk_1` FOREIGN KEY (`T_ID`) REFERENCES `transactions` (`T_ID`),
  ADD CONSTRAINT `cash_and_bank_ibfk_2` FOREIGN KEY (`CB_CREDITACCOUNT`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `cash_and_bank_ibfk_3` FOREIGN KEY (`CB_DEBITACCOUNT`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `cash_and_bank_ibfk_4` FOREIGN KEY (`B_ID`) REFERENCES `banks` (`B_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `cash_and_bank_ibfk_5` FOREIGN KEY (`CREATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `cash_and_bank_ibfk_6` FOREIGN KEY (`UPDATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `cheques`
--
ALTER TABLE `cheques`
  ADD CONSTRAINT `cheques_ibfk_1` FOREIGN KEY (`BR_ID`) REFERENCES `branches` (`BR_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `cheques_ibfk_2` FOREIGN KEY (`DB_ACCOUNTID`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `cheques_ibfk_3` FOREIGN KEY (`T_ID`) REFERENCES `transactions` (`T_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `cheques_ibfk_4` FOREIGN KEY (`CREATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `cheques_ibfk_5` FOREIGN KEY (`UPDATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `expense`
--
ALTER TABLE `expense`
  ADD CONSTRAINT `expense_ibfk_1` FOREIGN KEY (`CREDIT_ACCOUNT`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `expense_ibfk_2` FOREIGN KEY (`DEBIT_ACCOUNT`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `expense_ibfk_3` FOREIGN KEY (`T_ID`) REFERENCES `transactions` (`T_ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `sub_heads`
--
ALTER TABLE `sub_heads`
  ADD CONSTRAINT `sub_heads_ibfk_1` FOREIGN KEY (`CREATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `sub_heads_ibfk_2` FOREIGN KEY (`UPDATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_account_details`
--
ALTER TABLE `tbl_account_details`
  ADD CONSTRAINT `tbl_account_details_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_bills`
--
ALTER TABLE `tbl_bills`
  ADD CONSTRAINT `tbl_bills_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`T_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_bills_ibfk_2` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_bills_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_bills_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_bill_details`
--
ALTER TABLE `tbl_bill_details`
  ADD CONSTRAINT `tbl_bill_details_ibfk_1` FOREIGN KEY (`bill_id`) REFERENCES `tbl_bills` (`bill_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_bill_details_ibfk_2` FOREIGN KEY (`job_expense_id`) REFERENCES `tbl_job_expenses` (`job_expense_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_client`
--
ALTER TABLE `tbl_client`
  ADD CONSTRAINT `tbl_client_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_client_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_client_ibfk_3` FOREIGN KEY (`refered_by`) REFERENCES `tbl_client` (`client_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_client_documents`
--
ALTER TABLE `tbl_client_documents`
  ADD CONSTRAINT `tbl_client_documents_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `tbl_client` (`client_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_commodity_types`
--
ALTER TABLE `tbl_commodity_types`
  ADD CONSTRAINT `tbl_commodity_types_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_commodity_types_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_consignors`
--
ALTER TABLE `tbl_consignors`
  ADD CONSTRAINT `tbl_consignors_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `tbl_client` (`client_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_consignors_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_consignors_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_consignor_documents`
--
ALTER TABLE `tbl_consignor_documents`
  ADD CONSTRAINT `tbl_consignor_documents_ibfk_1` FOREIGN KEY (`consignor_id`) REFERENCES `tbl_consignors` (`consignor_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_consignor_persons`
--
ALTER TABLE `tbl_consignor_persons`
  ADD CONSTRAINT `tbl_consignor_persons_ibfk_1` FOREIGN KEY (`consignor_id`) REFERENCES `tbl_consignors` (`consignor_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_containers`
--
ALTER TABLE `tbl_containers`
  ADD CONSTRAINT `tbl_containers_ibfk_1` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_custom_duties`
--
ALTER TABLE `tbl_custom_duties`
  ADD CONSTRAINT `tbl_custom_duties_ibfk_1` FOREIGN KEY (`requisition_id`) REFERENCES `tbl_requisitions` (`requisition_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_custom_duties_ibfk_2` FOREIGN KEY (`commodity_id`) REFERENCES `tbl_saved_commodities` (`id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_document_types`
--
ALTER TABLE `tbl_document_types`
  ADD CONSTRAINT `tbl_document_types_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_document_types_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_due_dates`
--
ALTER TABLE `tbl_due_dates`
  ADD CONSTRAINT `tbl_due_dates_ibfk_1` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_invoices`
--
ALTER TABLE `tbl_invoices`
  ADD CONSTRAINT `tbl_invoices_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `tbl_client` (`client_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_invoices_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `tbl_supplier` (`supplier_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_invoices_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_invoices_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_invoice_documents`
--
ALTER TABLE `tbl_invoice_documents`
  ADD CONSTRAINT `tbl_invoice_documents_ibfk_1` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_invoice_documents_ibfk_2` FOREIGN KEY (`document_title`) REFERENCES `tbl_document_types` (`document_type_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_job_activity`
--
ALTER TABLE `tbl_job_activity`
  ADD CONSTRAINT `tbl_job_activity_ibfk_1` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_job_activity_ibfk_2` FOREIGN KEY (`job_activity_status`) REFERENCES `tbl_job_status` (`job_status_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_job_expenses`
--
ALTER TABLE `tbl_job_expenses`
  ADD CONSTRAINT `tbl_job_expenses_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_job_expenses_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_job_status`
--
ALTER TABLE `tbl_job_status`
  ADD CONSTRAINT `tbl_job_status_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_job_status_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_memos`
--
ALTER TABLE `tbl_memos`
  ADD CONSTRAINT `tbl_memos_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `tbl_client` (`client_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_memos_ibfk_2` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_memo_charges`
--
ALTER TABLE `tbl_memo_charges`
  ADD CONSTRAINT `tbl_memo_charges_ibfk_1` FOREIGN KEY (`memo_id`) REFERENCES `tbl_memos` (`memo_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_other_expenses`
--
ALTER TABLE `tbl_other_expenses`
  ADD CONSTRAINT `tbl_other_expenses_ibfk_1` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_other_expenses_ibfk_2` FOREIGN KEY (`requisition_id`) REFERENCES `tbl_requisitions` (`requisition_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_other_expenses_ibfk_3` FOREIGN KEY (`requisition_expense_id`) REFERENCES `tbl_requisition_expenses` (`requisition_expense_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_payments`
--
ALTER TABLE `tbl_payments`
  ADD CONSTRAINT `tbl_payments_ibfk_1` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_requisitions`
--
ALTER TABLE `tbl_requisitions`
  ADD CONSTRAINT `tbl_requisitions_ibfk_1` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_requisitions_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_requisitions_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_requisition_expenses`
--
ALTER TABLE `tbl_requisition_expenses`
  ADD CONSTRAINT `tbl_requisition_expenses_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_requisition_expenses_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_saved_commodities`
--
ALTER TABLE `tbl_saved_commodities`
  ADD CONSTRAINT `tbl_saved_commodities_ibfk_1` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_saved_commodities_ibfk_2` FOREIGN KEY (`commodity_type`) REFERENCES `tbl_commodity_types` (`commodity_type_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_security_deposit`
--
ALTER TABLE `tbl_security_deposit`
  ADD CONSTRAINT `tbl_security_deposit_ibfk_1` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_shipping_line`
--
ALTER TABLE `tbl_shipping_line`
  ADD CONSTRAINT `tbl_shipping_line_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_shipping_line_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
  ADD CONSTRAINT `tbl_staff_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_staff_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_staff_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  ADD CONSTRAINT `tbl_supplier_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_supplier_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_transfer_amount`
--
ALTER TABLE `tbl_transfer_amount`
  ADD CONSTRAINT `tbl_transfer_amount_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`T_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_transfer_amount_ibfk_3` FOREIGN KEY (`credit_account`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_transfer_amount_ibfk_4` FOREIGN KEY (`debit_account`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_user_role`
--
ALTER TABLE `tbl_user_role`
  ADD CONSTRAINT `tbl_user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_user_role_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `tbl_menu` (`menu_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`CREATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `transactions_ibfk_2` FOREIGN KEY (`UPDATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `transactions_meta`
--
ALTER TABLE `transactions_meta`
  ADD CONSTRAINT `transactions_meta_ibfk_1` FOREIGN KEY (`T_ID`) REFERENCES `transactions` (`T_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `transactions_meta_ibfk_2` FOREIGN KEY (`A_ID`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION;

  --
-- Indexes for table `stbjobtype`
--
ALTER TABLE `stbjobtype`
  ADD PRIMARY KEY (`Iid`);

--
-- AUTO_INCREMENT for table `stbjobtype`
--
ALTER TABLE `stbjobtype`
  MODIFY `Iid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

  --
-- Indexes for table `tbl_financial_information`
--
ALTER TABLE `tbl_financial_information`
  ADD PRIMARY KEY (`financial_id`);

--
-- AUTO_INCREMENT for table `tbl_financial_information`
--
ALTER TABLE `tbl_financial_information`
  MODIFY `financial_id` int(11) NOT NULL AUTO_INCREMENT;

  --
-- Indexes for table `tbl_sheds`
--
ALTER TABLE `tbl_sheds`
  ADD PRIMARY KEY (`shed_id`);

--
-- AUTO_INCREMENT for table `tbl_sheds`
--
ALTER TABLE `tbl_sheds`
  MODIFY `shed_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;