-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2020 at 12:17 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nbs`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `A_ID` int(11) NOT NULL,
  `H_ID` int(11) NOT NULL,
  `SUB_HEAD_ID` int(11) NOT NULL,
  `CLIENT_ID` int(11) DEFAULT NULL,
  `A_NAME` varchar(100) NOT NULL,
  `A_DESC` text DEFAULT NULL,
  `A_OPENINGTYPE` varchar(10) DEFAULT NULL,
  `A_OPENINGBALANCE` float NOT NULL DEFAULT 0,
  `A_SELFHEAD_ID` int(11) DEFAULT NULL,
  `A_NO` varchar(50) NOT NULL,
  `A_STATUS` tinyint(4) NOT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT current_timestamp(),
  `UPDATED_BY` int(11) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`A_ID`, `H_ID`, `SUB_HEAD_ID`, `CLIENT_ID`, `A_NAME`, `A_DESC`, `A_OPENINGTYPE`, `A_OPENINGBALANCE`, `A_SELFHEAD_ID`, `A_NO`, `A_STATUS`, `CREATED_BY`, `CREATED_AT`, `UPDATED_BY`, `UPDATED_AT`) VALUES
(1, 4, 1, NULL, 'Cash In Hand', '', NULL, 0, 0, '1001.1.1', 1, 1, '2018-02-28 10:46:24', NULL, '2019-11-05 13:38:05'),
(2, 4, 1, NULL, 'Cash at Bank', '', NULL, 0, 0, '1001.1.2', 1, 1, '2018-02-28 10:46:53', NULL, NULL),
(3, 48, 16, NULL, 'Service Charges', '', NULL, 0, 0, '4004.2.1', 1, 1, '2018-09-04 21:58:05', NULL, NULL),
(4, 49, 3, NULL, 'Sales Tax', '', NULL, 0, 0, '3001.3.1', 1, 1, '2018-09-04 21:58:56', NULL, NULL),
(5, 17, 1, NULL, 'Witholding tax', '', 'Debit', 0, 0, '1001.2.2', 1, 1, '2020-01-20 11:15:13', NULL, '2020-01-20 16:15:32'),
(6, 17, 1, NULL, 'Suspense Payment', '', NULL, 0, 0, '1001.2.2', 1, 1, '2020-01-22 12:22:14', NULL, '2020-01-22 17:24:32'),
(33, 4, 1, NULL, 'Meezan', '', '', 0, 2, '1001.1.2.1', 1, 1, '2020-01-18 11:30:46', NULL, NULL),
(34, 4, 1, NULL, 'Meezan - Meezan branch Saddar', '', 'Debit', 0, 33, '1001.1.2.1.1', 1, 1, '2020-01-18 11:31:21', NULL, NULL),
(35, 17, 1, 3, 'Barkha (Account Receivables)', NULL, NULL, 0, 0, '1001.2.1', 1, 1, '2020-01-18 11:31:53', NULL, NULL),
(36, 57, 12, 3, 'Barkha (Sales Tax Expense)', NULL, NULL, 0, 0, '5004.1.1', 1, 1, '2020-01-18 11:31:53', NULL, NULL),
(37, 50, 17, 3, 'Barkha (Client Expenses)', NULL, NULL, 0, 0, '5006.1.1', 1, 1, '2020-01-18 11:31:53', NULL, NULL),
(38, 47, 16, 3, 'Barkha (Turnover Services)', NULL, NULL, 0, 0, '4004.1.1', 1, 1, '2020-01-18 11:31:53', NULL, NULL),
(39, 48, 16, 3, 'Barkha (Service Charges)', NULL, NULL, 0, 3, '4004.2.1.1', 1, 1, '2020-01-18 11:31:53', NULL, NULL),
(40, 52, 3, 3, 'Barkha (Account Payables)', NULL, NULL, 0, 0, '3001.4.1', 1, 1, '2020-01-18 11:31:53', NULL, NULL),
(41, 53, 3, 3, 'Barkha (Advances from clients)', NULL, NULL, 0, 0, '3001.5.1', 1, 1, '2020-01-18 11:31:53', NULL, NULL),
(42, 56, 1, NULL, 'Staff', '', '', 0, 0, '1001.6.1', 1, 1, '2020-01-18 11:38:42', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `accounts_head`
--

CREATE TABLE `accounts_head` (
  `H_ID` int(11) NOT NULL,
  `SUB_HEAD_ID` int(11) NOT NULL,
  `H_TYPE` varchar(20) NOT NULL,
  `H_NAME` varchar(100) NOT NULL,
  `H_NO` varchar(50) NOT NULL,
  `H_STATUS` tinyint(4) NOT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` datetime NOT NULL DEFAULT current_timestamp(),
  `UPDATED_BY` int(11) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts_head`
--

INSERT INTO `accounts_head` (`H_ID`, `SUB_HEAD_ID`, `H_TYPE`, `H_NAME`, `H_NO`, `H_STATUS`, `CREATED_BY`, `CREATED_AT`, `UPDATED_BY`, `UPDATED_AT`) VALUES
(4, 1, 'Assets', 'Cash and Bank', '1001.1', 1, 1, '2019-09-02 11:35:21', 1, '2019-09-04 18:48:14'),
(17, 1, 'Assets', 'Account Receivables', '1001.2', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(18, 2, 'Assets', 'Immoveable Assets', '1002.1', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(19, 2, 'Assets', 'Moveable Assets', '1002.2', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(22, 10, 'Expense', 'Office and Misc Expenses', '5002.1', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(23, 10, 'Expense', 'Fuel Expenses', '5002.2', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(24, 10, 'Expense', 'Conveyance Expenses', '5002.3', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(30, 3, 'Liabilities', 'Associated Concerns', '3001.1', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(31, 10, 'Expense', 'Rent Expense', '5002.4', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(32, 10, 'Expense', 'Utility Expenses', '5002.5', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(34, 10, 'Expense', 'Internet Expense', '5002.6', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(38, 10, 'Expense', 'HR related Expenses', '5002.7', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(39, 10, 'Expense', 'Marketing Expenses', '5002.8', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(40, 10, 'Expense', 'Repair and Maintenance Expenses', '5002.9', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(41, 1, 'Assets', 'Advances and Loans', '1001.4', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(42, 10, 'Expense', 'Professional and Legal Fees', '5002.10', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(43, 5, 'Equity', 'Share Capital', '2001.1', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(45, 10, 'Expense', 'Salaries and Allownces', '5002.11', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(46, 3, 'Liabilities', 'Other Payables', '3001.2', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(47, 16, 'Income', 'Turnover Services', '4004.1', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(48, 16, 'Income', 'Commission', '4004.2', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(49, 3, 'Liabilities', 'Payable Tax', '3001.3', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(50, 17, 'Expense', 'Client Expenses', '5006.1', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(52, 3, 'Liabilities', 'Account Payables', '3001.4', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(53, 3, 'Liabilities', 'Advances from clients', '3001.5', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(54, 1, 'Assets', 'Short Term Security Deposit', '1001.5', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(55, 10, 'Expense', 'General Expenses', '5002.12', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(56, 1, 'Assets', 'Outdoor Staff Advances', '1001.6', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11'),
(57, 12, 'Expense', 'Sales Tax Expense', '5004.1', 1, 1, '2019-09-02 11:35:21', NULL, '2019-09-02 11:36:11');

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `B_ID` int(11) NOT NULL,
  `B_NAME` varchar(100) NOT NULL,
  `B_LOGO` varchar(100) DEFAULT NULL,
  `B_STATUS` tinyint(4) NOT NULL,
  `B_DATETIME` timestamp NOT NULL DEFAULT current_timestamp(),
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` datetime NOT NULL DEFAULT current_timestamp(),
  `UPDATED_BY` int(11) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `A_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `BR_ID` int(11) NOT NULL,
  `B_ID` int(11) NOT NULL,
  `BR_NAME` varchar(100) NOT NULL,
  `BR_CODE` varchar(100) NOT NULL,
  `BR_ADDRESS` text DEFAULT NULL,
  `BR_CITY` int(11) NOT NULL,
  `BR_OPENINGTYPE` varchar(10) DEFAULT NULL,
  `BR_OPENINGBALANCE` float NOT NULL DEFAULT 0,
  `BR_STATUS` tinyint(4) NOT NULL,
  `BR_DATETIME` timestamp NOT NULL DEFAULT current_timestamp(),
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` datetime NOT NULL DEFAULT current_timestamp(),
  `UPDATED_BY` int(11) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `A_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cash_and_bank`
--

CREATE TABLE `cash_and_bank` (
  `CB_ID` int(11) NOT NULL,
  `B_ID` int(11) DEFAULT NULL,
  `CB_DATE` date NOT NULL,
  `CB_DEBITACCOUNT` int(11) NOT NULL,
  `CB_CREDITACCOUNT` int(11) NOT NULL,
  `CB_CHEQUENO` varchar(50) DEFAULT NULL,
  `CB_PAY` varchar(20) DEFAULT NULL,
  `CB_AMOUNT` float NOT NULL,
  `CB_TYPE` varchar(10) DEFAULT NULL,
  `CB_PARTICULARS` text DEFAULT NULL,
  `T_ID` int(11) NOT NULL,
  `IS_ACTIVE` tinyint(4) NOT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` datetime NOT NULL DEFAULT current_timestamp(),
  `UPDATED_BY` int(11) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cheques`
--

CREATE TABLE `cheques` (
  `C_ID` int(11) NOT NULL,
  `BR_ID` int(11) NOT NULL,
  `DB_ACCOUNTID` int(11) NOT NULL,
  `C_PAY` varchar(100) DEFAULT NULL,
  `C_NO` varchar(20) DEFAULT NULL,
  `C_AMOUNT` float NOT NULL,
  `C_AMOUNTWORDS` text NOT NULL,
  `C_DATE` date NOT NULL,
  `C_DESC` text DEFAULT NULL,
  `T_ID` int(11) NOT NULL,
  `AC_PAYEES` tinyint(4) DEFAULT 0,
  `C_STATUS` varchar(20) NOT NULL,
  `C_DATETIME` timestamp NOT NULL DEFAULT current_timestamp(),
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` datetime NOT NULL DEFAULT current_timestamp(),
  `UPDATED_BY` int(11) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `client_ledgers1`
-- (See below for the actual view)
--
CREATE TABLE `client_ledgers1` (
`invoice_id` int(11)
,`prefix` varchar(3)
,`bill_no` varchar(100)
,`reference` varchar(16)
,`client` int(11)
,`date` varchar(32)
,`amount` double
,`type` varchar(6)
);

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE `expense` (
  `E_ID` int(11) NOT NULL,
  `INVOICES_ID` int(11) NOT NULL,
  `DEBIT_ACCOUNT` int(11) NOT NULL,
  `CREDIT_ACCOUNT` int(11) NOT NULL,
  `EXPENSE_TYPE` int(11) NOT NULL,
  `VOUCHER_TYPE` enum('Post','Pre','Exp','Gen') DEFAULT NULL,
  `PAID_BY` varchar(50) NOT NULL,
  `PAID_TO` varchar(50) NOT NULL,
  `CHEQUE_NO` varchar(50) DEFAULT NULL,
  `DESCRIPTION` text DEFAULT NULL,
  `AMOUNT` float NOT NULL,
  `AMOUNT_IN_WORDS` text NOT NULL,
  `E_DATE` date NOT NULL,
  `T_ID` int(11) NOT NULL DEFAULT 0,
  `E_STATUS` varchar(20) NOT NULL,
  `IS_ACTIVE` tinyint(4) NOT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `installer`
--

CREATE TABLE `installer` (
  `id` int(1) NOT NULL,
  `installer_flag` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `job_ledgers`
-- (See below for the actual view)
--
CREATE TABLE `job_ledgers` (
`invoice_id` int(11)
,`prefix` varchar(16)
,`reference_no` varchar(100)
,`amount` decimal(33,2)
,`date` date
,`type` varchar(6)
);

-- --------------------------------------------------------

--
-- Table structure for table `payment_vouchers`
--

CREATE TABLE `payment_vouchers` (
  `PV_ID` int(11) NOT NULL,
  `U_ID` int(11) NOT NULL,
  `PAYMENT_MODE` tinyint(1) NOT NULL,
  `ACCOUNT_CREDIT` int(11) NOT NULL,
  `PAID_TO_ACCOUNT` int(11) NOT NULL,
  `PV_REF_TYPE` varchar(5) NOT NULL,
  `PV_REF_NO` varchar(20) NOT NULL,
  `PV_VOUCHER_NO` varchar(20) DEFAULT NULL,
  `PV_INSTRUMENT` varchar(50) DEFAULT NULL,
  `PV_AMOUNT` double NOT NULL,
  `PV_DATE` date NOT NULL,
  `PV_DESC` text NOT NULL,
  `T_ID` int(11) NOT NULL,
  `PV_CREATED_AT` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `petty_cash`
--

CREATE TABLE `petty_cash` (
  `PC_ID` int(11) NOT NULL,
  `PC_DATE` date NOT NULL,
  `PC_REF_NO` varchar(20) NOT NULL,
  `PC_VOUCHER_NO` varchar(20) DEFAULT NULL,
  `PC_PAYMENT_MODE` int(11) NOT NULL,
  `PC_PAID_TO` int(11) NOT NULL,
  `PC_PAID_BY` int(11) NOT NULL,
  `PC_DESC` text DEFAULT NULL,
  `T_ID` int(11) NOT NULL,
  `PC_STATUS` enum('Pending','Approved','Cancel') NOT NULL,
  `IS_ACTIVE` tinyint(1) NOT NULL,
  `PC_DATETIME` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `petty_cash_details`
--

CREATE TABLE `petty_cash_details` (
  `PCD_ID` int(11) NOT NULL,
  `PC_ID` int(11) NOT NULL,
  `A_ID` int(11) NOT NULL,
  `PCD_AMOUNT` double NOT NULL,
  `PCD_STATUS` tinyint(1) NOT NULL,
  `PCD_DATETIME` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reverse_entries`
--

CREATE TABLE `reverse_entries` (
  `reverse_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `voucher_type` varchar(5) NOT NULL,
  `description` text DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stbdeliverysection`
--

CREATE TABLE `stbdeliverysection` (
  `Iid` int(11) NOT NULL,
  `IInvoicesId` int(11) DEFAULT NULL,
  `IContainerId` int(11) DEFAULT NULL,
  `VCTruckNo` varchar(50) DEFAULT NULL,
  `VCBilty` varchar(50) DEFAULT NULL,
  `DTDeliveryDate` date DEFAULT NULL,
  `DTDateReceiptSite` date DEFAULT NULL,
  `DCWeightDeclare` decimal(10,2) DEFAULT 0.00,
  `DCWeightFound` decimal(10,2) NOT NULL DEFAULT 0.00,
  `VCTransporter` varchar(50) DEFAULT NULL,
  `VCPkgsReceive` varchar(50) DEFAULT NULL,
  `TRemarks` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stbjobtype`
--

CREATE TABLE `stbjobtype` (
  `Iid` int(11) NOT NULL,
  `VCJobType` varchar(50) DEFAULT NULL,
  `VCPrefix` varchar(50) NOT NULL,
  `VCSeparator` varchar(50) NOT NULL,
  `VCStartFrom` varchar(50) NOT NULL,
  `DTCreatedDate` datetime NOT NULL DEFAULT current_timestamp(),
  `ICreatedBy` int(11) DEFAULT NULL,
  `DTUpdatedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `IUpdatedBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `stbjobtype`
--

INSERT INTO `stbjobtype` (`Iid`, `VCJobType`, `VCPrefix`, `VCSeparator`, `VCStartFrom`, `DTCreatedDate`, `ICreatedBy`, `DTUpdatedDate`, `IUpdatedBy`) VALUES
(1, 'import', 'imp', '-', '0001', '2019-12-13 19:22:54', NULL, NULL, NULL),
(2, 'export', 'exp', '-', '0001', '2019-12-13 19:22:54', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_heads`
--

CREATE TABLE `sub_heads` (
  `SUB_HEAD_ID` int(11) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `HEAD_TYPE` varchar(20) NOT NULL,
  `SUB_NO` varchar(50) NOT NULL,
  `SUB_STATUS` tinyint(4) NOT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` datetime NOT NULL DEFAULT current_timestamp(),
  `UPDATED_BY` int(11) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_heads`
--

INSERT INTO `sub_heads` (`SUB_HEAD_ID`, `NAME`, `HEAD_TYPE`, `SUB_NO`, `SUB_STATUS`, `CREATED_BY`, `CREATED_AT`, `UPDATED_BY`, `UPDATED_AT`) VALUES
(1, 'Current Assets', 'Assets', '1001', 1, 1, '2019-09-02 11:38:10', NULL, '2019-09-02 11:38:50'),
(2, 'Non-Current Assets', 'Assets', '1002', 1, 1, '2019-09-02 11:38:10', NULL, '2019-09-02 11:38:50'),
(3, 'Current Liabilities', 'Liabilities', '3001', 1, 1, '2019-09-02 11:38:10', NULL, '2019-09-02 11:38:50'),
(4, 'Non-Current Liabilities', 'Liabilities', '3002', 1, 1, '2019-09-02 11:38:10', NULL, '2019-09-02 11:38:50'),
(5, 'Capital', 'Equity', '2001', 1, 1, '2019-09-02 11:38:10', NULL, '2019-09-02 11:38:50'),
(7, 'Income', 'Income', '4001', 1, 1, '2019-09-02 11:38:10', NULL, '2019-09-02 11:38:50'),
(8, 'Other Income', 'Income', '4002', 1, 1, '2019-09-02 11:38:10', NULL, '2019-09-02 11:38:50'),
(9, 'Expense', 'Expense', '5001', 1, 1, '2019-09-02 11:38:10', NULL, '2019-09-02 11:38:50'),
(10, 'Operating Expense', 'Expense', '5002', 1, 1, '2019-09-02 11:38:10', NULL, '2019-09-02 11:38:50'),
(11, 'Finance Cost', 'Expense', '5003', 1, 1, '2019-09-02 11:38:10', NULL, '2019-09-02 11:38:50'),
(12, 'Income Tax', 'Expense', '5004', 1, 1, '2019-09-02 11:38:10', NULL, '2019-09-02 11:38:50'),
(13, 'Drawing', 'Equity', '5003', 1, 1, '2019-09-02 11:38:10', NULL, '2019-09-02 11:38:50'),
(14, 'Other Expenses', 'Expense', '5005', 1, 1, '2019-09-02 11:38:10', NULL, '2019-09-02 11:38:50'),
(15, 'Customer Sales', 'Income', '4003', 1, 1, '2019-09-02 11:38:10', NULL, '2019-09-02 11:38:50'),
(16, 'Sales Income', 'Income', '4004', 1, 1, '2019-09-02 11:38:10', NULL, '2019-09-02 11:38:50'),
(17, 'Cost of Sales', 'Expense', '5006', 1, 1, '2019-09-02 11:38:10', NULL, '2019-09-02 11:38:50');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_account_details`
--

CREATE TABLE `tbl_account_details` (
  `account_details_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `fullname` varchar(160) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(64) COLLATE utf8_unicode_ci DEFAULT '-',
  `phone` varchar(32) COLLATE utf8_unicode_ci DEFAULT '-',
  `mobile` varchar(32) COLLATE utf8_unicode_ci DEFAULT '',
  `skype` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `departments_id` int(11) DEFAULT 0,
  `avatar` varchar(200) COLLATE utf8_unicode_ci DEFAULT 'uploads/default_avatar.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_account_details`
--

INSERT INTO `tbl_account_details` (`account_details_id`, `user_id`, `fullname`, `company`, `city`, `country`, `address`, `phone`, `mobile`, `skype`, `departments_id`, `avatar`) VALUES
(1, 1, 'YY Technologies', '-', 'RWP', NULL, '-', '0321', '0172361125', 'skype', 0, 'uploads/user_male.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_activities`
--

CREATE TABLE `tbl_activities` (
  `activities_id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `module` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module_field_id` int(11) DEFAULT NULL,
  `activity` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activity_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `icon` varchar(32) COLLATE utf8_unicode_ci DEFAULT 'fa-coffee',
  `value1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_advance_payments`
--

CREATE TABLE `tbl_advance_payments` (
  `ap_id` int(11) NOT NULL,
  `expense_type` enum('Job','Misc','Others') NOT NULL DEFAULT 'Job',
  `client_id` int(11) DEFAULT NULL,
  `invoices_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) NOT NULL,
  `payments_id` int(11) DEFAULT NULL,
  `debit_account` int(11) NOT NULL,
  `credit_account` int(11) NOT NULL,
  `ref_type` varchar(5) NOT NULL,
  `ref_no` varchar(20) NOT NULL,
  `voucher_no` varchar(20) DEFAULT NULL,
  `payment_method` varchar(50) NOT NULL,
  `instrument_type` varchar(50) DEFAULT NULL,
  `cheque_payorder` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `payment_date` date NOT NULL,
  `transferred` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_advance_payment_details`
--

CREATE TABLE `tbl_advance_payment_details` (
  `apd_id` int(11) NOT NULL,
  `ap_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `apd_amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `apd_title` varchar(50) NOT NULL,
  `apd_pay_order` varchar(50) NOT NULL,
  `apd_bank` int(11) DEFAULT NULL,
  `apd_branch` int(11) DEFAULT NULL,
  `apd_account_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bills`
--

CREATE TABLE `tbl_bills` (
  `bill_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `bill_no` varchar(100) NOT NULL,
  `delivery_date` date NOT NULL,
  `service_charges` decimal(10,2) NOT NULL DEFAULT 0.00,
  `gst` decimal(10,2) NOT NULL DEFAULT 0.00,
  `withholding_tax` decimal(10,2) NOT NULL DEFAULT 0.00,
  `bill_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `current_advance` double(10,2) NOT NULL,
  `total_bill` decimal(10,2) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bill_details`
--

CREATE TABLE `tbl_bill_details` (
  `bill_detail_id` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL,
  `job_expense_id` int(11) NOT NULL,
  `bill_amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `bill_from` varchar(50) NOT NULL,
  `bill_to` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cities`
--

CREATE TABLE `tbl_cities` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_cities`
--

INSERT INTO `tbl_cities` (`id`, `name`) VALUES
(1, 'Karachi'),
(2, 'Lahore'),
(3, 'Faisalabad'),
(4, 'Rawalpindi'),
(5, 'Multan'),
(6, 'Hyderabad'),
(7, 'Gujranwala'),
(8, 'Peshawar'),
(9, 'Islamabad'),
(10, 'Bahawalpur'),
(11, 'Sargodha'),
(12, 'Sialkot'),
(13, 'Quetta'),
(14, 'Sukkur'),
(15, 'Jhang'),
(16, 'Shekhupura'),
(17, 'Mardan'),
(18, 'Gujrat'),
(19, 'Larkana'),
(20, 'Kasur'),
(21, 'Rahim Yar Khan'),
(22, 'Sahiwal'),
(23, 'Okara'),
(24, 'Wah Cantonment'),
(25, 'Dera Ghazi Khan'),
(26, 'Mingora'),
(27, 'Mirpur Khas'),
(28, 'Chiniot'),
(29, 'Nawabshah'),
(30, 'K?moke'),
(31, 'Burewala'),
(32, 'Jhelum'),
(33, 'Sadiqabad'),
(34, 'Khanewal'),
(35, 'Hafizabad'),
(36, 'Kohat'),
(37, 'Jacobabad'),
(38, 'Shikarpur'),
(39, 'Muzaffargarh'),
(40, 'Khanpur'),
(41, 'Gojra'),
(42, 'Bahawalnagar'),
(43, 'Abbottabad'),
(44, 'Muridke'),
(45, 'Pakpattan'),
(46, 'Khuzdar'),
(47, 'Jaranwala'),
(48, 'Chishtian'),
(49, 'Daska'),
(50, 'Bhalwal'),
(51, 'Mandi Bahauddin'),
(52, 'Ahmadpur East'),
(53, 'Kamalia'),
(54, 'Tando Adam'),
(55, 'Khairpur'),
(56, 'Dera Ismail Khan'),
(57, 'Vehari'),
(58, 'Nowshera'),
(59, 'Dadu'),
(60, 'Wazirabad'),
(61, 'Khushab'),
(62, 'Charsada'),
(63, 'Swabi'),
(64, 'Chakwal'),
(65, 'Mianwali'),
(66, 'Tando Allahyar'),
(67, 'Kot Adu'),
(68, 'Turbat');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_client`
--

CREATE TABLE `tbl_client` (
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_p_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_p_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_p_contact` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_p_designation` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refered_by` int(11) DEFAULT NULL,
  `short_note` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ntn` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `strn` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_status` int(11) NOT NULL DEFAULT 1,
  `profile_photo` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_client`
--

INSERT INTO `tbl_client` (`client_id`, `name`, `email`, `email_2`, `email_3`, `contact_p_name`, `contact_p_email`, `contact_p_contact`, `contact_p_designation`, `refered_by`, `short_note`, `phone`, `mobile`, `fax`, `ntn`, `strn`, `address`, `city`, `zipcode`, `country`, `client_status`, `profile_photo`, `created_by`, `date_added`, `updated_by`, `updated_at`) VALUES
(3, 'Barkha', 'barkha@yytechnologies.com', '', '', '', '', '', '', NULL, '							', '', '03337247533', '', '', '', '							', '', '', 'Pakistan', 1, NULL, 1, '2020-01-18 11:31:52', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_client_documents`
--

CREATE TABLE `tbl_client_documents` (
  `client_document_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `client_document_title` varchar(100) NOT NULL,
  `client_document_date` date NOT NULL,
  `client_document_file` text DEFAULT NULL,
  `client_document_comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_commodity_types`
--

CREATE TABLE `tbl_commodity_types` (
  `commodity_type_id` int(11) NOT NULL,
  `commodity_type` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_saved` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_commodity_types`
--

INSERT INTO `tbl_commodity_types` (`commodity_type_id`, `commodity_type`, `created_by`, `date_saved`, `updated_by`, `updated_at`) VALUES
(1, 'Live Animals', 1, '2019-12-17 16:43:30', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_config`
--

CREATE TABLE `tbl_config` (
  `config_key` varchar(255) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_config`
--

INSERT INTO `tbl_config` (`config_key`, `value`) VALUES
('2checkout_private_key', 'privatekey'),
('2checkout_publishable_key', 'checkoutkey'),
('2checkout_seller_id', 'seled id'),
('2checkout_status', 'active'),
('accounting_snapshot', '0'),
('allowed_files', 'gif|png|jpeg|jpg|pdf|doc|txt|docx|xls|zip|rar|xls|mp4'),
('allow_client_registration', 'TRUE'),
('authorize', 'login id'),
('authorize_status', 'active'),
('authorize_transaction_key', 'transfer key'),
('automatic_email_on_recur', 'TRUE'),
('bank_cash', '0'),
('bitcoin_address', 'Bitcoin Address'),
('bitcoin_status', 'active'),
('braintree_default_account', 'Braintree Defual allcount'),
('braintree_live_or_sandbox', 'Braintree Live or Sandbox'),
('braintree_merchant_id', 'Braintree Merchant ID'),
('braintree_private_key', 'Braintree Private Key'),
('braintree_public_key', 'Braintree Defual allcount'),
('braintree_status', 'active'),
('build', '0'),
('ccavenue_key', 'CCAvenue Working Key'),
('ccavenue_merchant_id', 'CCAvenue Merchant ID'),
('ccavenue_status', 'active'),
('company_address', 'I , I 0/6, SHANJAH TRADE CENTAE, ALIAF HUSSA N AOAD. NEW CHALLI'),
('company_city', 'Karachi'),
('company_country', 'Pakistan'),
('company_domain', 'www.yytechnologies.com'),
('company_email', 'shahbaz@yytechnologies.com'),
('company_legal_name', 'FRAGO'),
('company_logo', 'uploads/frago-logosss_(1).png'),
('company_name', 'FRAGO'),
('company_phone', '021212121212, 0212120212, 021212122'),
('company_phone_2', ''),
('company_vat', '0000-00'),
('company_zip_code', '75850'),
('contact_person', 'Usama Shakeel'),
('country', '0'),
('cron_key', '34WI2L12L87I1A65M90M9A42N41D08A26I'),
('currency', 'PKR'),
('date_format', '%d-%m-%Y'),
('date_php_format', 'd-m-Y'),
('date_picker_format', 'dd-mm-yyyy'),
('decimal_separator', '0'),
('default_currency', 'PKR'),
('default_currency_symbol', '$'),
('default_language', 'english'),
('default_tax', '0.00'),
('default_terms', '<span style=\"background-color: yellow;\">This is computer generated invoice signature not required</span>'),
('demo_mode', 'FALSE'),
('developer', 'ig63Yd/+yuA8127gEyTz9TY4pnoeKq8dtocVP44+BJvtlRp8Vqcetwjk51dhSB6Rx8aVIKOPfUmNyKGWK7C/gg=='),
('display_estimate_badge', '0'),
('display_invoice_badge', 'FALSE'),
('email_account_details', 'TRUE'),
('email_estimate_message', 'Hi {CLIENT}<br>Thanks for your business inquiry. <br>The estimate EST {REF} is attached with this email. <br>Estimate Overview:<br>Estimate # : EST {REF}<br>Amount: {CURRENCY} {AMOUNT}<br> You can view the estimate online at:<br>{LINK}<br>Best Regards,<br>{COMPANY}'),
('email_invoice_message', 'Hello {CLIENT}<br>Here is the invoice of {CURRENCY} {AMOUNT}<br>You can view the invoice online at:<br>{LINK}<br>Best Regards,<br>{COMPANY}'),
('email_staff_tickets', 'TRUE'),
('enable_languages', 'FALSE'),
('estimate_color', '0'),
('estimate_language', 'en'),
('estimate_prefix', 'EST'),
('estimate_terms', 'Hey Looking forward to doing business with you.'),
('file_max_size', '80000'),
('gcal_api_key', ''),
('gcal_id', ''),
('increment_invoice_number', 'TRUE'),
('installed', 'TRUE'),
('invoices_due_after', '10'),
('invoice_color', '#53B567'),
('invoice_language', 'en'),
('invoice_logo', 'uploads/frago-logosss_(1).png'),
('invoice_number_separator_export', '-'),
('invoice_number_separator_import', '-'),
('invoice_prefix', '0'),
('invoice_prefix_export', 'exp'),
('invoice_prefix_import', 'imp'),
('invoice_start_export', '0001'),
('invoice_start_import', '0001'),
('invoice_start_no', ''),
('job_number_auto', 'Yes'),
('language', 'english'),
('languages', 'spanish'),
('last_check', '1436363002'),
('last_seen_activities', '0'),
('locale', 'en_US'),
('login_bg', 'bg-login.jpg'),
('logofile', '0'),
('logo_or_icon', 'logo_title'),
('notify_bug_assignment', 'TRUE'),
('notify_bug_comments', 'TRUE'),
('notify_bug_status', 'TRUE'),
('notify_message_received', 'TRUE'),
('notify_project_assignments', 'TRUE'),
('notify_project_comments', 'TRUE'),
('notify_project_files', 'TRUE'),
('notify_task_assignments', 'TRUE'),
('paypal_cancel_url', 'paypal/cancel'),
('paypal_email', 'example@paypal.com'),
('paypal_ipn_url', 'paypal/t_ipn/ipn'),
('paypal_live', 'TRUE'),
('paypal_status', 'active'),
('paypal_success_url', 'paypal/success'),
('pdf_engine', 'invoicr'),
('postmark_api_key', ''),
('postmark_from_address', ''),
('project_prefix', 'PRO'),
('protocol', 'mail'),
('purchase_code', ''),
('recurring_invoice', '0'),
('reminder_message', 'Hello {CLIENT}<br>This is a friendly reminder to pay your invoice of {CURRENCY} {AMOUNT}<br>You can view the invoice online at:<br>{LINK}<br>Best Regards,<br>{COMPANY}'),
('requisition_optional', 'Yes'),
('reset_key', '34WI2L12L87I1A65M90M9A42N41D08A26I'),
('rows_per_table', '25'),
('security_token', '5027133599'),
('settings', 'theme'),
('show_estimate_tax', 'on'),
('show_invoice_tax', 'TRUE'),
('show_item_tax', '0'),
('show_login_image', 'TRUE'),
('show_only_logo', 'FALSE'),
('sidebar_theme', 'default'),
('site_appleicon', 'logo.png'),
('site_author', 'William M.'),
('site_desc', 'Freelancer Office is a Web based PHP application for Freelancers - buy it on Codecanyon'),
('site_favicon', 'logo.png'),
('site_icon', 'fa-flask'),
('smtp_host', 'mail.bacs.com'),
('smtp_pass', 'abdulkahharnayeem1q2w3e4r'),
('smtp_port', '25'),
('smtp_user', 'info@bacs.com'),
('stripe_private_key', 'pk_test_ARblMczqDw61NusMMs7o1RVK'),
('stripe_public_key', 'pk_test_ARblMczqDw61NusMMs7o1RVK'),
('stripe_status', 'active'),
('system_font', 'roboto_condensed'),
('thousand_separator', ','),
('timezone', 'Asia/Karachi'),
('use_gravatar', 'TRUE'),
('use_postmark', 'FALSE'),
('valid_license', 'TRUE'),
('webmaster_email', 'support@example.com'),
('weboc_token', '250'),
('website_name', 'FRAGO');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_consignors`
--

CREATE TABLE `tbl_consignors` (
  `consignor_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `short_note` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ntn` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `strn` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `consignor_status` int(11) NOT NULL DEFAULT 1,
  `profile_photo` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_consignors`
--

INSERT INTO `tbl_consignors` (`consignor_id`, `name`, `email`, `email_2`, `email_3`, `client_id`, `short_note`, `phone`, `mobile`, `fax`, `ntn`, `strn`, `address`, `city`, `zipcode`, `country`, `consignor_status`, `profile_photo`, `created_by`, `date_added`, `updated_by`, `updated_at`) VALUES
(3, 'Barkha', 'barkha@yytechnologies.com', '', '', 3, '							', '', '03335745127', '', '', '', '							', '', '', 'Pakistan', 1, NULL, 1, '2020-01-18 11:52:26', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_consignor_documents`
--

CREATE TABLE `tbl_consignor_documents` (
  `consignor_document_id` int(11) NOT NULL,
  `consignor_id` int(11) NOT NULL,
  `consignor_document_title` varchar(100) NOT NULL,
  `consignor_document_date` date NOT NULL,
  `consignor_document_file` text DEFAULT NULL,
  `consignor_document_comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_consignor_persons`
--

CREATE TABLE `tbl_consignor_persons` (
  `consignor_person_id` int(11) NOT NULL,
  `consignor_id` int(11) NOT NULL,
  `consignor_person_name` varchar(50) NOT NULL,
  `consignor_person_email` varchar(100) DEFAULT NULL,
  `consignor_person_contact` varchar(50) DEFAULT NULL,
  `consignor_person_designation` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_containers`
--

CREATE TABLE `tbl_containers` (
  `id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `container_type` varchar(100) NOT NULL,
  `container_no` varchar(50) NOT NULL,
  `container_ft` enum('20ft','40ft','45ft','Open Top') NOT NULL,
  `packages` decimal(10,2) NOT NULL DEFAULT 0.00,
  `container_shed` int(11) DEFAULT NULL,
  `container_gross_weight` decimal(10,2) NOT NULL DEFAULT 0.00,
  `container_net_weight` decimal(10,2) NOT NULL DEFAULT 0.00,
  `container_total_weight` decimal(10,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_countries`
--

CREATE TABLE `tbl_countries` (
  `id` int(6) NOT NULL,
  `value` varchar(250) CHARACTER SET latin1 NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_countries`
--

INSERT INTO `tbl_countries` (`id`, `value`) VALUES
(1, 'Afghanistan'),
(2, 'Aringland Islands'),
(3, 'Albania'),
(4, 'Algeria'),
(5, 'American Samoa'),
(6, 'Andorra'),
(7, 'Angola'),
(8, 'Anguilla'),
(9, 'Antarctica'),
(10, 'Antigua and Barbuda'),
(11, 'Argentina'),
(12, 'Armenia'),
(13, 'Aruba'),
(14, 'Australia'),
(15, 'Austria'),
(16, 'Azerbaijan'),
(17, 'Bahamas'),
(18, 'Bahrain'),
(19, 'Bangladesh'),
(20, 'Barbados'),
(21, 'Belarus'),
(22, 'Belgium'),
(23, 'Belize'),
(24, 'Benin'),
(25, 'Bermuda'),
(26, 'Bhutan'),
(27, 'Bolivia'),
(28, 'Bosnia and Herzegovina'),
(29, 'Botswana'),
(30, 'Bouvet Island'),
(31, 'Brazil'),
(32, 'British Indian Ocean territory'),
(33, 'Brunei Darussalam'),
(34, 'Bulgaria'),
(35, 'Burkina Faso'),
(36, 'Burundi'),
(37, 'Cambodia'),
(38, 'Cameroon'),
(39, 'Canada'),
(40, 'Cape Verde'),
(41, 'Cayman Islands'),
(42, 'Central African Republic'),
(43, 'Chad'),
(44, 'Chile'),
(45, 'China'),
(46, 'Christmas Island'),
(47, 'Cocos (Keeling) Islands'),
(48, 'Colombia'),
(49, 'Comoros'),
(50, 'Congo'),
(51, 'Congo'),
(52, ' Democratic Republic'),
(53, 'Cook Islands'),
(54, 'Costa Rica'),
(55, 'Ivory Coast (Ivory Coast)'),
(56, 'Croatia (Hrvatska)'),
(57, 'Cuba'),
(58, 'Cyprus'),
(59, 'Czech Republic'),
(60, 'Denmark'),
(61, 'Djibouti'),
(62, 'Dominica'),
(63, 'Dominican Republic'),
(64, 'East Timor'),
(65, 'Ecuador'),
(66, 'Egypt'),
(67, 'El Salvador'),
(68, 'Equatorial Guinea'),
(69, 'Eritrea'),
(70, 'Estonia'),
(71, 'Ethiopia'),
(72, 'Falkland Islands'),
(73, 'Faroe Islands'),
(74, 'Fiji'),
(75, 'Finland'),
(76, 'France'),
(77, 'French Guiana'),
(78, 'French Polynesia'),
(79, 'French Southern Territories'),
(80, 'Gabon'),
(81, 'Gambia'),
(82, 'Georgia'),
(83, 'Germany'),
(84, 'Ghana'),
(85, 'Gibraltar'),
(86, 'Greece'),
(87, 'Greenland'),
(88, 'Grenada'),
(89, 'Guadeloupe'),
(90, 'Guam'),
(91, 'Guatemala'),
(92, 'Guinea'),
(93, 'Guinea-Bissau'),
(94, 'Guyana'),
(95, 'Haiti'),
(96, 'Heard and McDonald Islands'),
(97, 'Honduras'),
(98, 'Hong Kong'),
(99, 'Hungary'),
(100, 'Iceland'),
(101, 'India'),
(102, 'Indonesia'),
(103, 'Iran'),
(104, 'Iraq'),
(105, 'Ireland'),
(106, 'Israel'),
(107, 'Italy'),
(108, 'Jamaica'),
(109, 'Japan'),
(110, 'Jordan'),
(111, 'Kazakhstan'),
(112, 'Kenya'),
(113, 'Kiribati'),
(114, 'Korea (north)'),
(115, 'Korea (south)'),
(116, 'Kuwait'),
(117, 'Kyrgyzstan'),
(118, 'Lao People\'s Democratic Republic'),
(119, 'Latvia'),
(120, 'Lebanon'),
(121, 'Lesotho'),
(122, 'Liberia'),
(123, 'Libyan Arab Jamahiriya'),
(124, 'Liechtenstein'),
(125, 'Lithuania'),
(126, 'Luxembourg'),
(127, 'Macao'),
(128, 'Macedonia'),
(129, 'Madagascar'),
(130, 'Malawi'),
(131, 'Malaysia'),
(132, 'Maldives'),
(133, 'Mali'),
(134, 'Malta'),
(135, 'Marshall Islands'),
(136, 'Martinique'),
(137, 'Mauritania'),
(138, 'Mauritius'),
(139, 'Mayotte'),
(140, 'Mexico'),
(141, 'Micronesia'),
(142, 'Moldova'),
(143, 'Monaco'),
(144, 'Mongolia'),
(145, 'Montserrat'),
(146, 'Morocco'),
(147, 'Mozambique'),
(148, 'Myanmar'),
(149, 'Namibia'),
(150, 'Nauru'),
(151, 'Nepal'),
(152, 'Netherlands'),
(153, 'Netherlands Antilles'),
(154, 'New Caledonia'),
(155, 'New Zealand'),
(156, 'Nicaragua'),
(157, 'Niger'),
(158, 'Nigeria'),
(159, 'Niue'),
(160, 'Norfolk Island'),
(161, 'Northern Mariana Islands'),
(162, 'Norway'),
(163, 'Oman'),
(164, 'Pakistan'),
(165, 'Palau'),
(166, 'Palestinian Territories'),
(167, 'Panama'),
(168, 'Papua New Guinea'),
(169, 'Paraguay'),
(170, 'Peru'),
(171, 'Philippines'),
(172, 'Pitcairn'),
(173, 'Poland'),
(174, 'Portugal'),
(175, 'Puerto Rico'),
(176, 'Qatar'),
(177, 'Runion'),
(178, 'Romania'),
(179, 'Russian Federation'),
(180, 'Rwanda'),
(181, 'Saint Helena'),
(182, 'Saint Kitts and Nevis'),
(183, 'Saint Lucia'),
(184, 'Saint Pierre and Miquelon'),
(185, 'Saint Vincent and the Grenadines'),
(186, 'Samoa'),
(187, 'San Marino'),
(188, 'Sao Tome and Principe'),
(189, 'Saudi Arabia'),
(190, 'Senegal'),
(191, 'Serbia and Montenegro'),
(192, 'Seychelles'),
(193, 'Sierra Leone'),
(194, 'Singapore'),
(195, 'Slovakia'),
(196, 'Slovenia'),
(197, 'Solomon Islands'),
(198, 'Somalia'),
(199, 'South Africa'),
(200, 'South Georgia and the South Sandwich Islands'),
(201, 'Spain'),
(202, 'Sri Lanka'),
(203, 'Sudan'),
(204, 'Suriname'),
(205, 'Svalbard and Jan Mayen Islands'),
(206, 'Swaziland'),
(207, 'Sweden'),
(208, 'Switzerland'),
(209, 'Syria'),
(210, 'Taiwan'),
(211, 'Tajikistan'),
(212, 'Tanzania'),
(213, 'Thailand'),
(214, 'Togo'),
(215, 'Tokelau'),
(216, 'Tonga'),
(217, 'Trinidad and Tobago'),
(218, 'Tunisia'),
(219, 'Turkey'),
(220, 'Turkmenistan'),
(221, 'Turks and Caicos Islands'),
(222, 'Tuvalu'),
(223, 'Uganda'),
(224, 'Ukraine'),
(225, 'United Arab Emirates'),
(226, 'United Kingdom'),
(227, 'United States of America'),
(228, 'Uruguay'),
(229, 'Uzbekistan'),
(230, 'Vanuatu'),
(231, 'Vatican City'),
(232, 'Venezuela'),
(233, 'Vietnam'),
(234, 'Virgin Islands (British)'),
(235, 'Virgin Islands (US)'),
(236, 'Wallis and Futuna Islands'),
(237, 'Western Sahara'),
(238, 'Yemen'),
(239, 'Zaire'),
(240, 'Zambia'),
(241, 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_currencies`
--

CREATE TABLE `tbl_currencies` (
  `code` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `symbol` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `xrate` decimal(12,5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_currencies`
--

INSERT INTO `tbl_currencies` (`code`, `name`, `symbol`, `xrate`) VALUES
('AUD', 'Australian Dollar', '$', NULL),
('BRL', 'Brazilian Real', 'R$', NULL),
('CAD', 'Canadian Dollar', '$', NULL),
('CHF', 'Swiss Franc', 'Fr', NULL),
('CLP', 'Chilean Peso', '$', NULL),
('CNY', 'Chinese Yuan', '?', NULL),
('CZK', 'Czech Koruna', 'K??', NULL),
('DHM', 'United Arab Emirates', NULL, NULL),
('DK', 'Denmark', NULL, NULL),
('DKK', 'Danish Krone', 'kr', NULL),
('EUR', 'Euro', '?', NULL),
('GBP', 'British Pound', '?', NULL),
('HKD', 'Hong Kong Dollar', '$', NULL),
('HUF', 'Hungarian Forint', 'Ft', NULL),
('IDR', 'Indonesian Rupiah', 'Rp', NULL),
('ILS', 'Israeli New Shekel', '?', NULL),
('INR', 'Indian Rupee', 'INR', NULL),
('JPY', 'Japanese Yen', '?', NULL),
('KRW', 'Korean Won', '?', NULL),
('KWD', 'Kuwait', NULL, NULL),
('MXN', 'Mexican Peso', '$', NULL),
('MYR', 'Malaysian Ringgit', 'RM', NULL),
('NOK', 'Norwegian Krone', 'kr', NULL),
('NZD', 'New Zealand Dollar', '$', NULL),
('PHP', 'Philippine Peso', '?', NULL),
('PKR', 'Pakistan Rupee', 'PKR', NULL),
('PLN', 'Polish Zloty', 'zl', NULL),
('RUB', 'Russian Ruble', '?', NULL),
('SAR', 'Saudia Arabia', NULL, NULL),
('SEK', 'Swedish Krona', 'kr', NULL),
('SFR', 'Switzerland', NULL, NULL),
('SGD', 'Singapore Dollar', '$', NULL),
('STG', 'United Kingdom', NULL, NULL),
('THB', 'Thai Baht', '?', NULL),
('TRY', 'Turkish Lira', '?', NULL),
('TWD', 'Taiwan Dollar', '$', NULL),
('USD', 'US Dollar', '$', '1.00000'),
('VEF', 'Bol?var Fuerte', 'Bs.', NULL),
('WON', 'South Korea', NULL, NULL),
('ZAR', 'South African Rand', 'R', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_custom_duties`
--

CREATE TABLE `tbl_custom_duties` (
  `custom_duty_id` int(11) NOT NULL,
  `requisition_id` int(11) NOT NULL,
  `commodity_id` int(11) NOT NULL,
  `landing_charges` enum('Yes','No') NOT NULL DEFAULT 'No',
  `insurance_value` decimal(10,2) NOT NULL DEFAULT 0.00,
  `insurance_type` enum('Percent','Amount') NOT NULL DEFAULT 'Amount',
  `value_per_unit` decimal(10,2) NOT NULL DEFAULT 0.00,
  `duty_cd` decimal(10,2) NOT NULL DEFAULT 0.00,
  `duty_fed` decimal(10,2) NOT NULL DEFAULT 0.00,
  `duty_acd` decimal(10,2) NOT NULL DEFAULT 0.00,
  `duty_rd` decimal(10,2) NOT NULL DEFAULT 0.00,
  `duty_st` decimal(10,2) NOT NULL DEFAULT 0.00,
  `duty_ast` decimal(10,2) NOT NULL DEFAULT 0.00,
  `duty_it` decimal(10,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_departments`
--

CREATE TABLE `tbl_departments` (
  `departments_id` int(10) NOT NULL,
  `deptname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_departments`
--

INSERT INTO `tbl_departments` (`departments_id`, `deptname`) VALUES
(1, 'Administrator'),
(2, 'Officer'),
(3, 'Manager'),
(4, 'Assistant Manager'),
(6, 'Accounts'),
(7, 'Finance'),
(10, 'test2');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_document_types`
--

CREATE TABLE `tbl_document_types` (
  `document_type_id` int(11) NOT NULL,
  `document_title` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_saved` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_document_types`
--

INSERT INTO `tbl_document_types` (`document_type_id`, `document_title`, `created_by`, `date_saved`, `updated_by`, `updated_at`) VALUES
(1, 'ADC', 1, '2019-12-17 16:43:19', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_due_dates`
--

CREATE TABLE `tbl_due_dates` (
  `due_date_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `due_date` date DEFAULT NULL,
  `due_time` varchar(100) DEFAULT NULL,
  `due_comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_exchange_rates`
--

CREATE TABLE `tbl_exchange_rates` (
  `exchange_rate_id` int(11) NOT NULL,
  `exchange_rate` decimal(10,2) NOT NULL DEFAULT 0.00,
  `exchange_rate_currency` varchar(50) NOT NULL,
  `exchange_rate_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_financial_information`
--

CREATE TABLE `tbl_financial_information` (
  `financial_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `invoice_no` varchar(22) NOT NULL,
  `invoice_date` date DEFAULT NULL,
  `payment_term` enum('LC','Without LC') NOT NULL,
  `lc_no` varchar(22) DEFAULT NULL,
  `lc_date` date DEFAULT NULL,
  `lc_bank` varchar(50) DEFAULT NULL,
  `incoterm` int(11) DEFAULT NULL,
  `currency` varchar(22) NOT NULL,
  `fob_value` double(10,2) NOT NULL DEFAULT 0.00,
  `freight` decimal(10,2) NOT NULL DEFAULT 0.00,
  `other_charges` decimal(10,2) NOT NULL DEFAULT 0.00,
  `exchange_rate` decimal(10,2) NOT NULL DEFAULT 0.00,
  `eif_no` varchar(22) DEFAULT NULL,
  `bank` varchar(22) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gd_types`
--

CREATE TABLE `tbl_gd_types` (
  `gd_type_id` int(11) NOT NULL,
  `gd_type` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_incoterms`
--

CREATE TABLE `tbl_incoterms` (
  `incoterm_id` int(11) NOT NULL,
  `incoterm` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_invoices`
--

CREATE TABLE `tbl_invoices` (
  `invoices_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `job_no` enum('Auto','Manual') COLLATE utf8_unicode_ci NOT NULL,
  `reference_no` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `consignor_id` int(11) NOT NULL,
  `collectorate` varchar(22) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `port` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mode_options` int(11) DEFAULT NULL,
  `p_o_l` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `p_o_d` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `gross_weight` decimal(10,2) NOT NULL DEFAULT 0.00,
  `net_weight` decimal(10,2) NOT NULL DEFAULT 0.00,
  `total_pkgs` decimal(10,2) NOT NULL DEFAULT 0.00,
  `igm_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `igm_date` date DEFAULT NULL,
  `index_no` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bl_no` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `bl_date` date DEFAULT NULL,
  `shipping_line` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forwarding_agent` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_value` decimal(10,2) NOT NULL DEFAULT 0.00,
  `exchange_rate` decimal(10,2) NOT NULL DEFAULT 0.00,
  `eif` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eta` date NOT NULL,
  `consignor_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `consignor_address` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `consignment_type` enum('FCL','LCL','Break Bulk','By Air') COLLATE utf8_unicode_ci NOT NULL,
  `due_date` date DEFAULT NULL,
  `currency` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'USD',
  `status` enum('Unpaid','Paid') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Unpaid',
  `job_status` tinyint(4) NOT NULL DEFAULT 1,
  `date_saved` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `show_client` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Yes',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_invoice_documents`
--

CREATE TABLE `tbl_invoice_documents` (
  `document_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `document_title` int(11) NOT NULL,
  `document_date` date DEFAULT NULL,
  `document_file` text DEFAULT NULL,
  `document_type` varchar(50) DEFAULT NULL,
  `document_comment` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_activity`
--

CREATE TABLE `tbl_job_activity` (
  `job_activity_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `job_activity_date` date DEFAULT NULL,
  `job_activity_status` int(11) NOT NULL,
  `job_activity_notification` enum('Yes','No') NOT NULL DEFAULT 'No',
  `datetime` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_expenses`
--

CREATE TABLE `tbl_job_expenses` (
  `job_expense_id` int(11) NOT NULL,
  `job_expense_title` varchar(100) NOT NULL,
  `job_expense_type` int(11) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `date_saved` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_job_expenses`
--

INSERT INTO `tbl_job_expenses` (`job_expense_id`, `job_expense_title`, `job_expense_type`, `created_by`, `date_saved`, `updated_by`, `updated_at`) VALUES
(1, 'Lifter Charges', 0, 1, '2019-12-23 12:51:57', NULL, NULL),
(2, 'Civil Aviation', 0, 1, '2019-12-23 12:52:12', NULL, NULL),
(3, 'Misc Charges', 1, 1, '2019-12-23 12:52:43', NULL, NULL),
(4, 'Other Charges', 1, 1, '2019-12-23 12:52:49', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_status`
--

CREATE TABLE `tbl_job_status` (
  `job_status_id` int(11) NOT NULL,
  `job_status_title` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_saved` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_job_status`
--

INSERT INTO `tbl_job_status` (`job_status_id`, `job_status_title`, `created_by`, `date_saved`, `updated_by`, `updated_at`) VALUES
(1, 'Delivered', 1, '2019-12-17 16:43:04', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_memos`
--

CREATE TABLE `tbl_memos` (
  `memo_id` int(11) NOT NULL,
  `memo_type` enum('Debit','Credit') NOT NULL,
  `memo_no` varchar(50) NOT NULL,
  `client_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_memo_charges`
--

CREATE TABLE `tbl_memo_charges` (
  `mc_id` int(11) NOT NULL,
  `memo_id` int(11) NOT NULL,
  `mc_title` varchar(100) NOT NULL,
  `mc_amount` decimal(10,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `menu_id` int(11) NOT NULL,
  `label` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`menu_id`, `label`, `link`, `icon`, `parent`, `sort`) VALUES
(1, 'dashboard', 'admin/dashboard', 'fa fa-dashboard', 0, 1),
(4, 'client_supplier', '#', 'fa fa-users', 0, 8),
(24, 'user', 'admin/user/user_list', 'fa fa-users', 0, 12),
(25, 'settings', 'admin/settings', 'fa fa-cogs', 0, 13),
(26, 'database_backup', 'admin/settings/database_backup', 'fa fa-database', 0, 14),
(27, 'new_client', 'admin/client/new_client', 'fa fa-circle-o', 4, 1),
(28, 'manage_client', 'admin/client/manage_client', 'fa fa-circle-o', 4, 2),
(54, 'duty', '#', 'fa fa-paste', 0, 5),
(61, 'create_duties', 'admin/duty/create_duty', 'fa fa-circle-o', 54, 1),
(62, 'job_no', '#', 'fa fa-file', 0, 4),
(63, 'enter_job', 'admin/invoice/manage_invoice', 'fa fa-circle-o', 62, 1),
(64, 'manage_job', 'admin/invoice/search_invoice', 'fa fa-circle-o', 62, 2),
(65, 'manage_duties', 'admin/duty/search_duty', 'fa fa-circle-o', 54, 2),
(66, 'bill', '#', 'fa fa-file', 0, 6),
(67, 'chart_of_accounts', '#', 'fa fa-money', 0, 16),
(68, 'accounts_head', 'admin/accounts/accountHead', 'fa fa-circle-o', 67, 1),
(69, 'sub_accounts', 'admin/accounts', 'fa fa-circle-o', 67, 2),
(70, 'general_journal', 'admin/accounts/general_journal', 'fa fa-circle-o', 111, 10),
(71, 'general_ledgers', 'admin/accounts/general_ledgers', 'fa fa-circle-o', 84, 6),
(72, 'trial_balance', 'admin/accounts/trial_balance', 'fa fa-circle-o', 84, 7),
(73, 'balance_sheet', 'admin/accounts/balance_sheet', 'fa fa-circle-o', 84, 8),
(74, 'income_statement', 'admin/accounts/income_statement', 'fa fa-circle-o', 84, 9),
(76, 'banks', 'admin/accounts/banks', 'fa fa-circle-o', 78, 1),
(77, 'branches', 'admin/accounts/branches', 'fa fa-circle-o', 78, 2),
(78, 'bank_cash', '#', 'fa fa-money', 0, 18),
(79, 'cash_at_bank', 'admin/accounts/cash_atBank', 'fa fa-circle-o', 78, 4),
(80, 'cash_in_hand', 'admin/accounts/cash_inHand', 'fa fa-circle-o', 78, 5),
(82, 'cheques', 'admin/accounts/cheques', 'fa fa-circle-o', 78, 3),
(83, 'payments', 'admin/invoice/all_payments', 'fa fa-credit-card', 0, 7),
(84, 'report', '#', 'fa fa-bar-chart', 0, 19),
(85, 'sales_report', 'admin/report/sales_report', 'fa fa-circle-o', 84, 2),
(88, 'manage_supplier', 'admin/supplier/manage_supplier', 'fa fa-circle-o', 4, 4),
(89, 'invoice_report', 'admin/report/invoice_report', 'fa fa-circle-o', 84, 3),
(90, 'all_jobs', 'admin/invoice/all_invoices', 'fa fa-circle-o', 62, 3),
(92, 'security_deposit', '#', 'fa fa-money', 0, 20),
(94, 'manage_deposit', 'admin/security_deposit/search_deposit', 'fa fa-circle-o', 92, 2),
(101, 'client_ledgers', 'admin/accounts/client_ledgers', 'fa fa-circle-o', 84, 4),
(107, 'exp_voucher_list', 'admin/accounts/exp_voucher_list', 'fa fa-circle-o', 111, 6),
(108, 'create_deposit', 'admin/security_deposit/manage_deposit', 'fa fa-circle-o', 92, 1),
(109, 'job_ledgers', 'admin/accounts/job_ledgers', 'fa fa-circle-o', 84, 5),
(110, 'general_voucher', 'admin/accounts/voucher_list', 'fa fa-circle-o', 111, 9),
(111, 'entry_book', '#', 'fa fa-money', 0, 17),
(115, 'list_job_expenses', 'admin/accounts/job_expenses_list', 'fa fa-circle-o', 111, 8),
(116, 'job_report', 'admin/report/job_report', 'fa fa-circle-o', 84, 1),
(117, 'staff_ledgers', 'admin/accounts/staff_ledgers', 'fa fa-circle-o', 84, 6),
(118, 'manage_staff', 'admin/staff/manage_staff', 'fa fa-circle-o', 4, 5),
(119, 'shipping_calculator', 'admin/security_deposit/shipping_calculator', 'fa fa-circle-o', 92, 3),
(120, 'manage_shipping_calculator', 'admin/security_deposit/search_shipping_calculator', 'fa fa-circle-o', 92, 4),
(123, 'new_consignor', 'admin/consignor/new_consignor', 'fa fa-circle-o', 4, 3),
(124, 'manage_consignor', 'admin/consignor/manage_consignor', 'fa fa-circle-o', 4, 4),
(125, 'receipt_voucher_list', 'admin/accounts/receipt_voucher_list', 'fa fa-circle-o', 111, 16),
(127, 'payment_voucher_list', 'admin/accounts/payment_voucher_list', 'fa fa-circle-o', 111, 14),
(130, 'reversal_list', 'admin/accounts/reversal_list', 'fa fa-circle-o', 111, 18),
(131, 'all_duty', 'admin/duty/all_duties', 'fa fa-circle-o', 54, 3),
(132, 'create_bill', 'admin/bill/search_bill', 'fa fa-circle-o', 66, 1),
(133, 'list_bill', 'admin/bill/all_bills', 'fa fa-circle-o', 66, 2),
(134, 'sales_tax_report', 'admin/report/sales_tax_report', 'fa fa-circle-o', 84, 4),
(135, 'enter_delivery', 'admin/invoice/manage_delivery', 'fa fa-circle-o', 62, 4),
(136, 'list_delivery', 'admin/invoice/all_delivery', 'fa fa-circle-o', 62, 5);
INSERT INTO `tbl_menu` (`menu_id`, `label`, `link`, `icon`, `parent`, `sort`) VALUES (NULL, ''suspense_payment'', ''admin/accounts/manage_suspense_payment'', ''fa fa-circle-o'', ''111'', ''19'');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_other_expenses`
--

CREATE TABLE `tbl_other_expenses` (
  `other_expense_id` int(11) NOT NULL,
  `requisition_expense_id` int(11) NOT NULL,
  `requisition_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `other_expense_amount` decimal(10,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payments`
--

CREATE TABLE `tbl_payments` (
  `payments_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `trans_id` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cheque_payorder_no` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payer_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_method` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_type` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL,
  `currency` varchar(64) COLLATE utf8_unicode_ci DEFAULT 'USD',
  `received_by` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_date` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `month_paid` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `year_paid` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paid_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_private_message_send`
--

CREATE TABLE `tbl_private_message_send` (
  `private_message_send_id` int(11) NOT NULL,
  `send_user_id` int(11) NOT NULL,
  `receive_user_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `message_time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_requisitions`
--

CREATE TABLE `tbl_requisitions` (
  `requisition_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `requisition_type` enum('Declared','Assessable') NOT NULL DEFAULT 'Declared',
  `requisition_note` text DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_requisition_expenses`
--

CREATE TABLE `tbl_requisition_expenses` (
  `requisition_expense_id` int(11) NOT NULL,
  `requisition_expense_title` varchar(100) NOT NULL,
  `requisition_expense_status` tinyint(4) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `date_saved` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_saved_commodities`
--

CREATE TABLE `tbl_saved_commodities` (
  `id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `commodity_type` int(11) NOT NULL,
  `commodity` text NOT NULL,
  `hs_code` varchar(50) NOT NULL,
  `weight_unit` int(11) DEFAULT NULL,
  `no_of_units` decimal(10,2) NOT NULL DEFAULT 0.00,
  `unit_value` decimal(10,2) NOT NULL DEFAULT 0.00,
  `unit_value_assessed` decimal(10,2) NOT NULL DEFAULT 0.00,
  `no_of_pcs` decimal(10,2) NOT NULL DEFAULT 0.00,
  `sro` enum('Yes','No') DEFAULT 'No',
  `sro_no` varchar(50) NOT NULL,
  `serial_no` varchar(50) DEFAULT NULL,
  `sro_date` date DEFAULT NULL,
  `sro_remarks` text DEFAULT NULL,
  `temporary_import` enum('Yes','No') NOT NULL DEFAULT 'No',
  `security` enum('PDC','BG','IB','CG') DEFAULT NULL,
  `instrument_no` varchar(22) NOT NULL,
  `temporary_amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `date_of_issue` date DEFAULT NULL,
  `date_of_expiry` date DEFAULT NULL,
  `shipping_bill` varchar(22) NOT NULL,
  `shipping_date` date DEFAULT NULL,
  `permanent_retention` varchar(22) NOT NULL,
  `temporary_remarks` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_security_deposit`
--

CREATE TABLE `tbl_security_deposit` (
  `deposit_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `shipping_account` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `name_representative` varchar(255) DEFAULT NULL,
  `container_no` int(11) DEFAULT NULL,
  `deposit_amount` decimal(10,2) DEFAULT 0.00,
  `delivery_date_shipment` date DEFAULT NULL,
  `deposit_return` enum('Yes','No') NOT NULL DEFAULT 'No',
  `deposit_return_amount` decimal(10,2) DEFAULT 0.00,
  `return_transaction_id` int(11) DEFAULT NULL,
  `return_date_container` date DEFAULT NULL,
  `arrival` date DEFAULT NULL,
  `advance_rent` decimal(10,2) NOT NULL DEFAULT 0.00,
  `remaining_rent` decimal(10,2) NOT NULL DEFAULT 0.00,
  `final_rent` decimal(10,2) NOT NULL DEFAULT 0.00,
  `container_detention` decimal(10,2) NOT NULL DEFAULT 0.00,
  `refund_amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `expected_date_of_refund` date DEFAULT NULL,
  `credit_account` int(11) DEFAULT NULL,
  `cheque_no` varchar(50) DEFAULT NULL,
  `deposit_file` text DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sheds`
--

CREATE TABLE `tbl_sheds` (
  `shed_id` int(11) NOT NULL,
  `shed` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sheds`
--

INSERT INTO `tbl_sheds` (`shed_id`, `shed`, `created_by`, `created_date`, `updated_by`, `updated_at`) VALUES
(1, 'Shed1', 1, '2019-12-17 19:14:53', NULL, '2019-12-17 19:14:53'),
(2, 'Shed2', 1, '2019-12-17 19:14:58', NULL, '2019-12-17 19:14:58');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shipping_calculator`
--

CREATE TABLE `tbl_shipping_calculator` (
  `sc_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `shipping_company` varchar(50) NOT NULL,
  `number_container` int(11) NOT NULL,
  `rent_per_day` double NOT NULL,
  `totals` double NOT NULL,
  `exchange_rates` double NOT NULL,
  `per_day_rent_total` double NOT NULL,
  `free_day` int(11) NOT NULL,
  `igm_dates` date NOT NULL,
  `shipping_rent_date` date NOT NULL,
  `total_days` int(11) NOT NULL,
  `total_in_usd` double NOT NULL,
  `total_in_pkr` double NOT NULL,
  `er_date` date NOT NULL,
  `additional_rent_days` int(11) NOT NULL,
  `additional_rent_in_usd` double NOT NULL,
  `total_amount_in_pkr` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shipping_line`
--

CREATE TABLE `tbl_shipping_line` (
  `shipping_id` int(11) NOT NULL,
  `shipping_name` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_saved` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staff`
--

CREATE TABLE `tbl_staff` (
  `staff_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `staff_name` varchar(100) NOT NULL,
  `staff_email` varchar(100) DEFAULT NULL,
  `staff_contact` varchar(64) DEFAULT NULL,
  `staff_status` int(11) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `staff_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status`
--

CREATE TABLE `tbl_status` (
  `status_id` int(11) NOT NULL,
  `status` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_status`
--

INSERT INTO `tbl_status` (`status_id`, `status`) VALUES
(1, 'answered'),
(2, 'closed'),
(3, 'open'),
(5, 'in_progress');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supplier`
--

CREATE TABLE `tbl_supplier` (
  `supplier_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_email` varchar(100) DEFAULT NULL,
  `supplier_contact` varchar(64) DEFAULT NULL,
  `supplier_status` int(11) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `supplier_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_supplier`
--

INSERT INTO `tbl_supplier` (`supplier_id`, `account_id`, `supplier_name`, `supplier_email`, `supplier_contact`, `supplier_status`, `created_by`, `supplier_date`, `updated_by`, `updated_at`) VALUES
(1, 16, 'Supplier 1', '', '', 1, 1, '2019-12-19 12:18:50', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_todo`
--

CREATE TABLE `tbl_todo` (
  `todo_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `user_id` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transfer_amount`
--

CREATE TABLE `tbl_transfer_amount` (
  `tb_id` int(11) NOT NULL,
  `advance_payment_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `credit_account` int(11) NOT NULL,
  `debit_account` int(11) NOT NULL,
  `cheque_payorder` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `transfer_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT 2,
  `client_id` int(11) DEFAULT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT 0,
  `banned` tinyint(4) NOT NULL DEFAULT 0,
  `ban_reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `online_status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1 = online 0 = offline '
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `username`, `password`, `email`, `role_id`, `client_id`, `activated`, `banned`, `ban_reason`, `new_password_key`, `new_password_requested`, `new_email`, `new_email_key`, `last_ip`, `last_login`, `created`, `modified`, `online_status`) VALUES
(1, 'admin', 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec', 'shahbaz@yytechnologies.com', 1, NULL, 1, 0, '', NULL, NULL, NULL, NULL, '::1', '2019-12-24 14:40:28', '2018-08-01 03:16:17', '2020-01-01 08:48:11', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_role`
--

CREATE TABLE `tbl_user_role` (
  `user_role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_vouchers`
--

CREATE TABLE `tbl_vouchers` (
  `voucher_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `voucher_type` enum('Post','Pre','Exp','Gen') NOT NULL,
  `payment_method` int(11) NOT NULL,
  `ref_no` varchar(20) NOT NULL,
  `voucher_no` varchar(20) DEFAULT NULL,
  `payment_date` date NOT NULL,
  `credit_account` int(11) NOT NULL,
  `paid_to` varchar(32) DEFAULT NULL,
  `cheque_no` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_voucher_details`
--

CREATE TABLE `tbl_voucher_details` (
  `voucher_detail_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `invoices_id` int(11) NOT NULL,
  `job_expense_id` int(11) NOT NULL,
  `debit_account` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_weight_units`
--

CREATE TABLE `tbl_weight_units` (
  `weight_unit_id` int(11) NOT NULL,
  `weight_unit` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_weight_units`
--

INSERT INTO `tbl_weight_units` (`weight_unit_id`, `weight_unit`, `created_by`, `created_date`, `updated_by`, `updated_at`) VALUES
(1, 'KGS', 1, '2019-12-17 16:44:09', NULL, NULL),
(2, 'TONS', 1, '2019-12-17 16:44:15', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_yards`
--

CREATE TABLE `tbl_yards` (
  `yard_id` int(11) NOT NULL,
  `yard` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_yards`
--

INSERT INTO `tbl_yards` (`yard_id`, `yard`, `created_by`, `created_date`, `updated_by`, `updated_at`) VALUES
(3, 'KICT', 1, '2019-12-17 16:43:49', NULL, '2019-12-17 16:43:49');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `T_ID` int(11) NOT NULL,
  `T_DATE` date NOT NULL,
  `T_PAY` varchar(200) DEFAULT NULL,
  `T_TYPE` varchar(50) DEFAULT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` datetime NOT NULL DEFAULT current_timestamp(),
  `UPDATED_BY` int(11) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transactions_meta`
--

CREATE TABLE `transactions_meta` (
  `TM_ID` int(11) NOT NULL,
  `T_ID` int(11) NOT NULL,
  `A_ID` int(11) NOT NULL,
  `PAYMENT_ID` int(11) DEFAULT NULL,
  `PARTICULARS` text DEFAULT NULL,
  `TM_AMOUNT` double DEFAULT NULL,
  `TM_TYPE` varchar(10) NOT NULL,
  `IS_ACTIVE` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure for view `client_ledgers1`
--
DROP TABLE IF EXISTS `client_ledgers1`;

CREATE VIEW `client_ledgers1`  AS  select `bl`.`invoices_id` AS `invoice_id`,'BL ' AS `prefix`,`bl`.`bill_no` AS `bill_no`,'' AS `reference`,`inv`.`client_id` AS `client`,`bl`.`delivery_date` AS `date`,`tr`.`TM_AMOUNT` AS `amount`,'Debit' AS `type` from (((`tbl_bills` `bl` join `tbl_invoices` `inv` on(`bl`.`invoices_id` = `inv`.`invoices_id`)) join `transactions_meta` `tr` on(`bl`.`transaction_id` = `tr`.`T_ID`)) join `accounts` `ac` on(`tr`.`A_ID` = `ac`.`A_ID`)) where `ac`.`A_NAME` LIKE '%(Account Receivables)' and `tr`.`TM_TYPE` = 'Debit' union select `pay`.`invoices_id` AS `invoice_id`,'' AS `prefix`,'' AS `bill_no`,'Pay Receipt' AS `reference`,`inv`.`client_id` AS `client`,`pay`.`payment_date` AS `date`,`pay`.`amount` AS `amount`,'Credit' AS `type` from (`tbl_payments` `pay` join `tbl_invoices` `inv` on(`pay`.`invoices_id` = `inv`.`invoices_id`)) union select `ap`.`invoices_id` AS `invoice_id`,'' AS `prefix`,'' AS `bill_no`,'Payment Received' AS `reference`,if(`inv`.`client_id` <> '',`inv`.`client_id`,`cl`.`client_id`) AS `client`,`ap`.`payment_date` AS `date`,if((select sum(`tbl_advance_payment_details`.`apd_amount`) from `tbl_advance_payment_details` where `tbl_advance_payment_details`.`apd_title` = 'Security Deposit' and `tbl_advance_payment_details`.`ap_id` = `ap`.`ap_id`) <> '',`ap`.`amount` - (select sum(`tbl_advance_payment_details`.`apd_amount`) from `tbl_advance_payment_details` where `tbl_advance_payment_details`.`apd_title` = 'Security Deposit' and `tbl_advance_payment_details`.`ap_id` = `ap`.`ap_id`),`ap`.`amount`) AS `amount`,'Credit' AS `type` from ((`tbl_advance_payments` `ap` left join `tbl_invoices` `inv` on(`ap`.`invoices_id` = `inv`.`invoices_id`)) left join `tbl_client` `cl` on(`ap`.`client_id` = `cl`.`client_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `job_ledgers`
--
DROP TABLE IF EXISTS `job_ledgers`;

CREATE VIEW `job_ledgers`  AS  select `ap`.`invoices_id` AS `invoice_id`,'Payment Received' AS `prefix`,`ap`.`payment_method` AS `reference_no`,if((select sum(`tbl_advance_payment_details`.`apd_amount`) from `tbl_advance_payment_details` where `tbl_advance_payment_details`.`apd_title` = 'Security Deposit' and `tbl_advance_payment_details`.`ap_id` = `ap`.`ap_id`) <> '',`ap`.`amount` - (select sum(`tbl_advance_payment_details`.`apd_amount`) from `tbl_advance_payment_details` where `tbl_advance_payment_details`.`apd_title` = 'Security Deposit' and `tbl_advance_payment_details`.`ap_id` = `ap`.`ap_id`),`ap`.`amount`) AS `amount`,`ap`.`payment_date` AS `date`,'Credit' AS `type` from (`tbl_advance_payments` `ap` join `tbl_invoices` `inv` on(`ap`.`invoices_id` = `inv`.`invoices_id`)) union select `vd`.`invoices_id` AS `invoice_id`,'' AS `prefix`,`je`.`job_expense_title` AS `reference_no`,`vd`.`amount` AS `amount`,`v`.`payment_date` AS `date`,'Debit' AS `type` from ((`tbl_voucher_details` `vd` join `tbl_vouchers` `v` on(`v`.`voucher_id` = `vd`.`voucher_id`)) join `tbl_job_expenses` `je` on(`vd`.`job_expense_id` = `je`.`job_expense_id`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`A_ID`),
  ADD KEY `H_ID` (`H_ID`),
  ADD KEY `SUB_HEAD_ID` (`SUB_HEAD_ID`),
  ADD KEY `CLIENT_ID` (`CLIENT_ID`),
  ADD KEY `CREATED_BY` (`CREATED_BY`),
  ADD KEY `UPDATED_BY` (`UPDATED_BY`);

--
-- Indexes for table `accounts_head`
--
ALTER TABLE `accounts_head`
  ADD PRIMARY KEY (`H_ID`),
  ADD KEY `SUB_HEAD_ID` (`SUB_HEAD_ID`),
  ADD KEY `CREATED_BY` (`CREATED_BY`),
  ADD KEY `UPDATED_BY` (`UPDATED_BY`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`B_ID`),
  ADD KEY `A_ID` (`A_ID`),
  ADD KEY `CREATED_BY` (`CREATED_BY`),
  ADD KEY `UPDATED_BY` (`UPDATED_BY`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`BR_ID`),
  ADD KEY `A_ID` (`A_ID`),
  ADD KEY `B_ID` (`B_ID`),
  ADD KEY `BR_CITY` (`BR_CITY`),
  ADD KEY `CREATED_BY` (`CREATED_BY`),
  ADD KEY `UPDATED_BY` (`UPDATED_BY`);

--
-- Indexes for table `cash_and_bank`
--
ALTER TABLE `cash_and_bank`
  ADD PRIMARY KEY (`CB_ID`),
  ADD KEY `T_ID` (`T_ID`),
  ADD KEY `CB_CREDITACCOUNT` (`CB_CREDITACCOUNT`),
  ADD KEY `CB_DEBITACCOUNT` (`CB_DEBITACCOUNT`),
  ADD KEY `B_ID` (`B_ID`),
  ADD KEY `CREATED_BY` (`CREATED_BY`),
  ADD KEY `UPDATED_BY` (`UPDATED_BY`);

--
-- Indexes for table `cheques`
--
ALTER TABLE `cheques`
  ADD PRIMARY KEY (`C_ID`),
  ADD KEY `BR_ID` (`BR_ID`),
  ADD KEY `DB_ACCOUNTID` (`DB_ACCOUNTID`),
  ADD KEY `T_ID` (`T_ID`),
  ADD KEY `CREATED_BY` (`CREATED_BY`),
  ADD KEY `UPDATE_BY` (`UPDATED_BY`);

--
-- Indexes for table `expense`
--
ALTER TABLE `expense`
  ADD PRIMARY KEY (`E_ID`),
  ADD KEY `CREDIT_ACCOUNT` (`CREDIT_ACCOUNT`),
  ADD KEY `DEBIT_ACCOUNT` (`DEBIT_ACCOUNT`),
  ADD KEY `T_ID` (`T_ID`),
  ADD KEY `INVOICES_ID` (`INVOICES_ID`);

--
-- Indexes for table `installer`
--
ALTER TABLE `installer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_vouchers`
--
ALTER TABLE `payment_vouchers`
  ADD PRIMARY KEY (`PV_ID`);

--
-- Indexes for table `petty_cash`
--
ALTER TABLE `petty_cash`
  ADD PRIMARY KEY (`PC_ID`);

--
-- Indexes for table `petty_cash_details`
--
ALTER TABLE `petty_cash_details`
  ADD PRIMARY KEY (`PCD_ID`);

--
-- Indexes for table `reverse_entries`
--
ALTER TABLE `reverse_entries`
  ADD PRIMARY KEY (`reverse_id`);

--
-- Indexes for table `stbdeliverysection`
--
ALTER TABLE `stbdeliverysection`
  ADD PRIMARY KEY (`Iid`);

--
-- Indexes for table `stbjobtype`
--
ALTER TABLE `stbjobtype`
  ADD PRIMARY KEY (`Iid`);

--
-- Indexes for table `sub_heads`
--
ALTER TABLE `sub_heads`
  ADD PRIMARY KEY (`SUB_HEAD_ID`),
  ADD KEY `CREATED_BY` (`CREATED_BY`),
  ADD KEY `UPDATED_BY` (`UPDATED_BY`);

--
-- Indexes for table `tbl_account_details`
--
ALTER TABLE `tbl_account_details`
  ADD PRIMARY KEY (`account_details_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tbl_activities`
--
ALTER TABLE `tbl_activities`
  ADD PRIMARY KEY (`activities_id`);

--
-- Indexes for table `tbl_advance_payments`
--
ALTER TABLE `tbl_advance_payments`
  ADD PRIMARY KEY (`ap_id`);

--
-- Indexes for table `tbl_advance_payment_details`
--
ALTER TABLE `tbl_advance_payment_details`
  ADD PRIMARY KEY (`apd_id`);

--
-- Indexes for table `tbl_bills`
--
ALTER TABLE `tbl_bills`
  ADD PRIMARY KEY (`bill_id`),
  ADD KEY `transaction_id` (`transaction_id`),
  ADD KEY `invoices_id` (`invoices_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_bill_details`
--
ALTER TABLE `tbl_bill_details`
  ADD PRIMARY KEY (`bill_detail_id`),
  ADD KEY `bill_id` (`bill_id`),
  ADD KEY `job_expense_id` (`job_expense_id`);

--
-- Indexes for table `tbl_cities`
--
ALTER TABLE `tbl_cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_client`
--
ALTER TABLE `tbl_client`
  ADD PRIMARY KEY (`client_id`),
  ADD UNIQUE KEY `Client Email` (`email`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `refered_by` (`refered_by`);

--
-- Indexes for table `tbl_client_documents`
--
ALTER TABLE `tbl_client_documents`
  ADD PRIMARY KEY (`client_document_id`),
  ADD KEY `client_id` (`client_id`);

--
-- Indexes for table `tbl_commodity_types`
--
ALTER TABLE `tbl_commodity_types`
  ADD PRIMARY KEY (`commodity_type_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_config`
--
ALTER TABLE `tbl_config`
  ADD PRIMARY KEY (`config_key`);

--
-- Indexes for table `tbl_consignors`
--
ALTER TABLE `tbl_consignors`
  ADD PRIMARY KEY (`consignor_id`),
  ADD UNIQUE KEY `Client Email` (`email`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `refered_by` (`client_id`);

--
-- Indexes for table `tbl_consignor_documents`
--
ALTER TABLE `tbl_consignor_documents`
  ADD PRIMARY KEY (`consignor_document_id`),
  ADD KEY `client_id` (`consignor_id`);

--
-- Indexes for table `tbl_consignor_persons`
--
ALTER TABLE `tbl_consignor_persons`
  ADD PRIMARY KEY (`consignor_person_id`),
  ADD KEY `consignor_id` (`consignor_id`);

--
-- Indexes for table `tbl_containers`
--
ALTER TABLE `tbl_containers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_id` (`invoices_id`);

--
-- Indexes for table `tbl_countries`
--
ALTER TABLE `tbl_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_currencies`
--
ALTER TABLE `tbl_currencies`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `tbl_custom_duties`
--
ALTER TABLE `tbl_custom_duties`
  ADD PRIMARY KEY (`custom_duty_id`),
  ADD KEY `invoices_id` (`requisition_id`),
  ADD KEY `commodity_id` (`commodity_id`);

--
-- Indexes for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  ADD PRIMARY KEY (`departments_id`);

--
-- Indexes for table `tbl_document_types`
--
ALTER TABLE `tbl_document_types`
  ADD PRIMARY KEY (`document_type_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_due_dates`
--
ALTER TABLE `tbl_due_dates`
  ADD PRIMARY KEY (`due_date_id`),
  ADD KEY `invoices_id` (`invoices_id`);

--
-- Indexes for table `tbl_exchange_rates`
--
ALTER TABLE `tbl_exchange_rates`
  ADD PRIMARY KEY (`exchange_rate_id`);

--
-- Indexes for table `tbl_financial_information`
--
ALTER TABLE `tbl_financial_information`
  ADD PRIMARY KEY (`financial_id`);

--
-- Indexes for table `tbl_gd_types`
--
ALTER TABLE `tbl_gd_types`
  ADD PRIMARY KEY (`gd_type_id`);

--
-- Indexes for table `tbl_incoterms`
--
ALTER TABLE `tbl_incoterms`
  ADD PRIMARY KEY (`incoterm_id`);

--
-- Indexes for table `tbl_invoices`
--
ALTER TABLE `tbl_invoices`
  ADD PRIMARY KEY (`invoices_id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `supplier_id` (`supplier_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_invoice_documents`
--
ALTER TABLE `tbl_invoice_documents`
  ADD PRIMARY KEY (`document_id`),
  ADD KEY `invoices_id` (`invoices_id`),
  ADD KEY `document_title` (`document_title`);

--
-- Indexes for table `tbl_job_activity`
--
ALTER TABLE `tbl_job_activity`
  ADD PRIMARY KEY (`job_activity_id`),
  ADD KEY `invoices_id` (`invoices_id`),
  ADD KEY `job_activity_status` (`job_activity_status`);

--
-- Indexes for table `tbl_job_expenses`
--
ALTER TABLE `tbl_job_expenses`
  ADD PRIMARY KEY (`job_expense_id`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `tbl_job_status`
--
ALTER TABLE `tbl_job_status`
  ADD PRIMARY KEY (`job_status_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_memos`
--
ALTER TABLE `tbl_memos`
  ADD PRIMARY KEY (`memo_id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `invoices_id` (`invoices_id`);

--
-- Indexes for table `tbl_memo_charges`
--
ALTER TABLE `tbl_memo_charges`
  ADD PRIMARY KEY (`mc_id`),
  ADD KEY `memo_id` (`memo_id`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `tbl_other_expenses`
--
ALTER TABLE `tbl_other_expenses`
  ADD PRIMARY KEY (`other_expense_id`),
  ADD KEY `invoices_id` (`invoices_id`),
  ADD KEY `requisition_id` (`requisition_id`),
  ADD KEY `requisition_expense_id` (`requisition_expense_id`);

--
-- Indexes for table `tbl_payments`
--
ALTER TABLE `tbl_payments`
  ADD PRIMARY KEY (`payments_id`),
  ADD KEY `invoices_id` (`invoices_id`),
  ADD KEY `paid_by` (`paid_by`);

--
-- Indexes for table `tbl_private_message_send`
--
ALTER TABLE `tbl_private_message_send`
  ADD PRIMARY KEY (`private_message_send_id`);

--
-- Indexes for table `tbl_requisitions`
--
ALTER TABLE `tbl_requisitions`
  ADD PRIMARY KEY (`requisition_id`),
  ADD KEY `invoices_id` (`invoices_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_requisition_expenses`
--
ALTER TABLE `tbl_requisition_expenses`
  ADD PRIMARY KEY (`requisition_expense_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_saved_commodities`
--
ALTER TABLE `tbl_saved_commodities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_id` (`invoices_id`),
  ADD KEY `commodity_type` (`commodity_type`);

--
-- Indexes for table `tbl_security_deposit`
--
ALTER TABLE `tbl_security_deposit`
  ADD PRIMARY KEY (`deposit_id`),
  ADD KEY `invoices_id` (`invoices_id`);

--
-- Indexes for table `tbl_sheds`
--
ALTER TABLE `tbl_sheds`
  ADD PRIMARY KEY (`shed_id`);

--
-- Indexes for table `tbl_shipping_calculator`
--
ALTER TABLE `tbl_shipping_calculator`
  ADD PRIMARY KEY (`sc_id`);

--
-- Indexes for table `tbl_shipping_line`
--
ALTER TABLE `tbl_shipping_line`
  ADD PRIMARY KEY (`shipping_id`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
  ADD PRIMARY KEY (`staff_id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_status`
--
ALTER TABLE `tbl_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  ADD PRIMARY KEY (`supplier_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_todo`
--
ALTER TABLE `tbl_todo`
  ADD PRIMARY KEY (`todo_id`);

--
-- Indexes for table `tbl_transfer_amount`
--
ALTER TABLE `tbl_transfer_amount`
  ADD PRIMARY KEY (`tb_id`),
  ADD KEY `transaction_id` (`transaction_id`),
  ADD KEY `advance_payment_id` (`advance_payment_id`),
  ADD KEY `credit_account` (`credit_account`),
  ADD KEY `debit_account` (`debit_account`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `tbl_user_role`
--
ALTER TABLE `tbl_user_role`
  ADD PRIMARY KEY (`user_role_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `menu_id` (`menu_id`);

--
-- Indexes for table `tbl_vouchers`
--
ALTER TABLE `tbl_vouchers`
  ADD PRIMARY KEY (`voucher_id`);

--
-- Indexes for table `tbl_voucher_details`
--
ALTER TABLE `tbl_voucher_details`
  ADD PRIMARY KEY (`voucher_detail_id`);

--
-- Indexes for table `tbl_weight_units`
--
ALTER TABLE `tbl_weight_units`
  ADD PRIMARY KEY (`weight_unit_id`);

--
-- Indexes for table `tbl_yards`
--
ALTER TABLE `tbl_yards`
  ADD PRIMARY KEY (`yard_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`T_ID`),
  ADD KEY `CREATED_BY` (`CREATED_BY`),
  ADD KEY `UPDATED_BY` (`UPDATED_BY`);

--
-- Indexes for table `transactions_meta`
--
ALTER TABLE `transactions_meta`
  ADD PRIMARY KEY (`TM_ID`),
  ADD KEY `transactions_meta_ibfk_1` (`T_ID`),
  ADD KEY `transactions_meta_ibfk_2` (`A_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `A_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `accounts_head`
--
ALTER TABLE `accounts_head`
  MODIFY `H_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `B_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `BR_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cash_and_bank`
--
ALTER TABLE `cash_and_bank`
  MODIFY `CB_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cheques`
--
ALTER TABLE `cheques`
  MODIFY `C_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expense`
--
ALTER TABLE `expense`
  MODIFY `E_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_vouchers`
--
ALTER TABLE `payment_vouchers`
  MODIFY `PV_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `petty_cash`
--
ALTER TABLE `petty_cash`
  MODIFY `PC_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `petty_cash_details`
--
ALTER TABLE `petty_cash_details`
  MODIFY `PCD_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reverse_entries`
--
ALTER TABLE `reverse_entries`
  MODIFY `reverse_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stbdeliverysection`
--
ALTER TABLE `stbdeliverysection`
  MODIFY `Iid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stbjobtype`
--
ALTER TABLE `stbjobtype`
  MODIFY `Iid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sub_heads`
--
ALTER TABLE `sub_heads`
  MODIFY `SUB_HEAD_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tbl_account_details`
--
ALTER TABLE `tbl_account_details`
  MODIFY `account_details_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_activities`
--
ALTER TABLE `tbl_activities`
  MODIFY `activities_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_advance_payments`
--
ALTER TABLE `tbl_advance_payments`
  MODIFY `ap_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_advance_payment_details`
--
ALTER TABLE `tbl_advance_payment_details`
  MODIFY `apd_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_bills`
--
ALTER TABLE `tbl_bills`
  MODIFY `bill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tbl_bill_details`
--
ALTER TABLE `tbl_bill_details`
  MODIFY `bill_detail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_cities`
--
ALTER TABLE `tbl_cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `tbl_client`
--
ALTER TABLE `tbl_client`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_client_documents`
--
ALTER TABLE `tbl_client_documents`
  MODIFY `client_document_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_commodity_types`
--
ALTER TABLE `tbl_commodity_types`
  MODIFY `commodity_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_consignors`
--
ALTER TABLE `tbl_consignors`
  MODIFY `consignor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_consignor_documents`
--
ALTER TABLE `tbl_consignor_documents`
  MODIFY `consignor_document_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_consignor_persons`
--
ALTER TABLE `tbl_consignor_persons`
  MODIFY `consignor_person_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_containers`
--
ALTER TABLE `tbl_containers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_countries`
--
ALTER TABLE `tbl_countries`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=242;

--
-- AUTO_INCREMENT for table `tbl_custom_duties`
--
ALTER TABLE `tbl_custom_duties`
  MODIFY `custom_duty_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  MODIFY `departments_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_document_types`
--
ALTER TABLE `tbl_document_types`
  MODIFY `document_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_due_dates`
--
ALTER TABLE `tbl_due_dates`
  MODIFY `due_date_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_exchange_rates`
--
ALTER TABLE `tbl_exchange_rates`
  MODIFY `exchange_rate_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_financial_information`
--
ALTER TABLE `tbl_financial_information`
  MODIFY `financial_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_gd_types`
--
ALTER TABLE `tbl_gd_types`
  MODIFY `gd_type_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_incoterms`
--
ALTER TABLE `tbl_incoterms`
  MODIFY `incoterm_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_invoices`
--
ALTER TABLE `tbl_invoices`
  MODIFY `invoices_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tbl_invoice_documents`
--
ALTER TABLE `tbl_invoice_documents`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_job_activity`
--
ALTER TABLE `tbl_job_activity`
  MODIFY `job_activity_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_job_expenses`
--
ALTER TABLE `tbl_job_expenses`
  MODIFY `job_expense_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_job_status`
--
ALTER TABLE `tbl_job_status`
  MODIFY `job_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_memos`
--
ALTER TABLE `tbl_memos`
  MODIFY `memo_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_memo_charges`
--
ALTER TABLE `tbl_memo_charges`
  MODIFY `mc_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT for table `tbl_other_expenses`
--
ALTER TABLE `tbl_other_expenses`
  MODIFY `other_expense_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_payments`
--
ALTER TABLE `tbl_payments`
  MODIFY `payments_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_private_message_send`
--
ALTER TABLE `tbl_private_message_send`
  MODIFY `private_message_send_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_requisitions`
--
ALTER TABLE `tbl_requisitions`
  MODIFY `requisition_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_requisition_expenses`
--
ALTER TABLE `tbl_requisition_expenses`
  MODIFY `requisition_expense_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_saved_commodities`
--
ALTER TABLE `tbl_saved_commodities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `tbl_security_deposit`
--
ALTER TABLE `tbl_security_deposit`
  MODIFY `deposit_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_sheds`
--
ALTER TABLE `tbl_sheds`
  MODIFY `shed_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_shipping_calculator`
--
ALTER TABLE `tbl_shipping_calculator`
  MODIFY `sc_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_shipping_line`
--
ALTER TABLE `tbl_shipping_line`
  MODIFY `shipping_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
  MODIFY `staff_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_status`
--
ALTER TABLE `tbl_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_todo`
--
ALTER TABLE `tbl_todo`
  MODIFY `todo_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_transfer_amount`
--
ALTER TABLE `tbl_transfer_amount`
  MODIFY `tb_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_user_role`
--
ALTER TABLE `tbl_user_role`
  MODIFY `user_role_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_vouchers`
--
ALTER TABLE `tbl_vouchers`
  MODIFY `voucher_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_voucher_details`
--
ALTER TABLE `tbl_voucher_details`
  MODIFY `voucher_detail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_weight_units`
--
ALTER TABLE `tbl_weight_units`
  MODIFY `weight_unit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_yards`
--
ALTER TABLE `tbl_yards`
  MODIFY `yard_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `T_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `transactions_meta`
--
ALTER TABLE `transactions_meta`
  MODIFY `TM_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`H_ID`) REFERENCES `accounts_head` (`H_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `accounts_ibfk_2` FOREIGN KEY (`SUB_HEAD_ID`) REFERENCES `sub_heads` (`SUB_HEAD_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `accounts_ibfk_3` FOREIGN KEY (`CREATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `accounts_ibfk_4` FOREIGN KEY (`UPDATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `accounts_ibfk_5` FOREIGN KEY (`CLIENT_ID`) REFERENCES `tbl_client` (`client_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `accounts_head`
--
ALTER TABLE `accounts_head`
  ADD CONSTRAINT `accounts_head_ibfk_1` FOREIGN KEY (`SUB_HEAD_ID`) REFERENCES `sub_heads` (`SUB_HEAD_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `accounts_head_ibfk_2` FOREIGN KEY (`CREATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `accounts_head_ibfk_3` FOREIGN KEY (`UPDATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `banks`
--
ALTER TABLE `banks`
  ADD CONSTRAINT `banks_ibfk_1` FOREIGN KEY (`A_ID`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `banks_ibfk_2` FOREIGN KEY (`CREATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `banks_ibfk_3` FOREIGN KEY (`UPDATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `branches`
--
ALTER TABLE `branches`
  ADD CONSTRAINT `branches_ibfk_1` FOREIGN KEY (`A_ID`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `branches_ibfk_2` FOREIGN KEY (`B_ID`) REFERENCES `banks` (`B_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `branches_ibfk_3` FOREIGN KEY (`BR_CITY`) REFERENCES `tbl_cities` (`id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `branches_ibfk_4` FOREIGN KEY (`CREATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `branches_ibfk_5` FOREIGN KEY (`UPDATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `cash_and_bank`
--
ALTER TABLE `cash_and_bank`
  ADD CONSTRAINT `cash_and_bank_ibfk_1` FOREIGN KEY (`T_ID`) REFERENCES `transactions` (`T_ID`),
  ADD CONSTRAINT `cash_and_bank_ibfk_2` FOREIGN KEY (`CB_CREDITACCOUNT`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `cash_and_bank_ibfk_3` FOREIGN KEY (`CB_DEBITACCOUNT`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `cash_and_bank_ibfk_4` FOREIGN KEY (`B_ID`) REFERENCES `banks` (`B_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `cash_and_bank_ibfk_5` FOREIGN KEY (`CREATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `cash_and_bank_ibfk_6` FOREIGN KEY (`UPDATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `cheques`
--
ALTER TABLE `cheques`
  ADD CONSTRAINT `cheques_ibfk_1` FOREIGN KEY (`BR_ID`) REFERENCES `branches` (`BR_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `cheques_ibfk_2` FOREIGN KEY (`DB_ACCOUNTID`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `cheques_ibfk_3` FOREIGN KEY (`T_ID`) REFERENCES `transactions` (`T_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `cheques_ibfk_4` FOREIGN KEY (`CREATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `cheques_ibfk_5` FOREIGN KEY (`UPDATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `expense`
--
ALTER TABLE `expense`
  ADD CONSTRAINT `expense_ibfk_1` FOREIGN KEY (`CREDIT_ACCOUNT`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `expense_ibfk_2` FOREIGN KEY (`DEBIT_ACCOUNT`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `expense_ibfk_3` FOREIGN KEY (`T_ID`) REFERENCES `transactions` (`T_ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `sub_heads`
--
ALTER TABLE `sub_heads`
  ADD CONSTRAINT `sub_heads_ibfk_1` FOREIGN KEY (`CREATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `sub_heads_ibfk_2` FOREIGN KEY (`UPDATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_account_details`
--
ALTER TABLE `tbl_account_details`
  ADD CONSTRAINT `tbl_account_details_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_bills`
--
ALTER TABLE `tbl_bills`
  ADD CONSTRAINT `tbl_bills_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`T_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_bills_ibfk_2` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_bills_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_bills_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_bill_details`
--
ALTER TABLE `tbl_bill_details`
  ADD CONSTRAINT `tbl_bill_details_ibfk_1` FOREIGN KEY (`bill_id`) REFERENCES `tbl_bills` (`bill_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_bill_details_ibfk_2` FOREIGN KEY (`job_expense_id`) REFERENCES `tbl_job_expenses` (`job_expense_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_client`
--
ALTER TABLE `tbl_client`
  ADD CONSTRAINT `tbl_client_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_client_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_client_ibfk_3` FOREIGN KEY (`refered_by`) REFERENCES `tbl_client` (`client_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_client_documents`
--
ALTER TABLE `tbl_client_documents`
  ADD CONSTRAINT `tbl_client_documents_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `tbl_client` (`client_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_commodity_types`
--
ALTER TABLE `tbl_commodity_types`
  ADD CONSTRAINT `tbl_commodity_types_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_commodity_types_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_consignors`
--
ALTER TABLE `tbl_consignors`
  ADD CONSTRAINT `tbl_consignors_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `tbl_client` (`client_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_consignors_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_consignors_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_consignor_documents`
--
ALTER TABLE `tbl_consignor_documents`
  ADD CONSTRAINT `tbl_consignor_documents_ibfk_1` FOREIGN KEY (`consignor_id`) REFERENCES `tbl_consignors` (`consignor_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_consignor_persons`
--
ALTER TABLE `tbl_consignor_persons`
  ADD CONSTRAINT `tbl_consignor_persons_ibfk_1` FOREIGN KEY (`consignor_id`) REFERENCES `tbl_consignors` (`consignor_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_containers`
--
ALTER TABLE `tbl_containers`
  ADD CONSTRAINT `tbl_containers_ibfk_1` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_custom_duties`
--
ALTER TABLE `tbl_custom_duties`
  ADD CONSTRAINT `tbl_custom_duties_ibfk_1` FOREIGN KEY (`requisition_id`) REFERENCES `tbl_requisitions` (`requisition_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_custom_duties_ibfk_2` FOREIGN KEY (`commodity_id`) REFERENCES `tbl_saved_commodities` (`id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_document_types`
--
ALTER TABLE `tbl_document_types`
  ADD CONSTRAINT `tbl_document_types_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_document_types_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_due_dates`
--
ALTER TABLE `tbl_due_dates`
  ADD CONSTRAINT `tbl_due_dates_ibfk_1` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_invoices`
--
ALTER TABLE `tbl_invoices`
  ADD CONSTRAINT `tbl_invoices_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `tbl_client` (`client_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_invoices_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `tbl_supplier` (`supplier_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_invoices_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_invoices_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_invoice_documents`
--
ALTER TABLE `tbl_invoice_documents`
  ADD CONSTRAINT `tbl_invoice_documents_ibfk_1` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_invoice_documents_ibfk_2` FOREIGN KEY (`document_title`) REFERENCES `tbl_document_types` (`document_type_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_job_activity`
--
ALTER TABLE `tbl_job_activity`
  ADD CONSTRAINT `tbl_job_activity_ibfk_1` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_job_activity_ibfk_2` FOREIGN KEY (`job_activity_status`) REFERENCES `tbl_job_status` (`job_status_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_job_expenses`
--
ALTER TABLE `tbl_job_expenses`
  ADD CONSTRAINT `tbl_job_expenses_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_job_expenses_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_job_status`
--
ALTER TABLE `tbl_job_status`
  ADD CONSTRAINT `tbl_job_status_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_job_status_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_memos`
--
ALTER TABLE `tbl_memos`
  ADD CONSTRAINT `tbl_memos_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `tbl_client` (`client_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_memos_ibfk_2` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_memo_charges`
--
ALTER TABLE `tbl_memo_charges`
  ADD CONSTRAINT `tbl_memo_charges_ibfk_1` FOREIGN KEY (`memo_id`) REFERENCES `tbl_memos` (`memo_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_other_expenses`
--
ALTER TABLE `tbl_other_expenses`
  ADD CONSTRAINT `tbl_other_expenses_ibfk_1` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_other_expenses_ibfk_2` FOREIGN KEY (`requisition_id`) REFERENCES `tbl_requisitions` (`requisition_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_other_expenses_ibfk_3` FOREIGN KEY (`requisition_expense_id`) REFERENCES `tbl_requisition_expenses` (`requisition_expense_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_payments`
--
ALTER TABLE `tbl_payments`
  ADD CONSTRAINT `tbl_payments_ibfk_1` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_requisitions`
--
ALTER TABLE `tbl_requisitions`
  ADD CONSTRAINT `tbl_requisitions_ibfk_1` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_requisitions_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_requisitions_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_requisition_expenses`
--
ALTER TABLE `tbl_requisition_expenses`
  ADD CONSTRAINT `tbl_requisition_expenses_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_requisition_expenses_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_saved_commodities`
--
ALTER TABLE `tbl_saved_commodities`
  ADD CONSTRAINT `tbl_saved_commodities_ibfk_1` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_saved_commodities_ibfk_2` FOREIGN KEY (`commodity_type`) REFERENCES `tbl_commodity_types` (`commodity_type_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_security_deposit`
--
ALTER TABLE `tbl_security_deposit`
  ADD CONSTRAINT `tbl_security_deposit_ibfk_1` FOREIGN KEY (`invoices_id`) REFERENCES `tbl_invoices` (`invoices_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_shipping_line`
--
ALTER TABLE `tbl_shipping_line`
  ADD CONSTRAINT `tbl_shipping_line_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_shipping_line_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
  ADD CONSTRAINT `tbl_staff_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_staff_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_staff_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  ADD CONSTRAINT `tbl_supplier_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_supplier_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_transfer_amount`
--
ALTER TABLE `tbl_transfer_amount`
  ADD CONSTRAINT `tbl_transfer_amount_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`T_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_transfer_amount_ibfk_3` FOREIGN KEY (`credit_account`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_transfer_amount_ibfk_4` FOREIGN KEY (`debit_account`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_user_role`
--
ALTER TABLE `tbl_user_role`
  ADD CONSTRAINT `tbl_user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_user_role_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `tbl_menu` (`menu_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`CREATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `transactions_ibfk_2` FOREIGN KEY (`UPDATED_BY`) REFERENCES `tbl_users` (`user_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `transactions_meta`
--
ALTER TABLE `transactions_meta`
  ADD CONSTRAINT `transactions_meta_ibfk_1` FOREIGN KEY (`T_ID`) REFERENCES `transactions` (`T_ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `transactions_meta_ibfk_2` FOREIGN KEY (`A_ID`) REFERENCES `accounts` (`A_ID`) ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
