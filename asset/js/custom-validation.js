$(document).ready(function() {

    $("#update_profile").validate({
        rules: {
            first_name: "required",
            last_name: "required",
            user_name: {
                required: true,
                noSpace: true,
                alphanumeric: true
            }
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        }

    });
    $("#change_password").validate({
        rules: {
            old_password: "required",
            new_password: "required",
            confirm_password: {
                required: true,
                equalTo: "#new_password",
            }
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        }

    });
    
    $("#form").validate({
        rules: {
        
        },
        highlight: function(element) {
            if($(element).parent('.input-group').length){
                $(element).parent().parent().addClass('has-error');
            }else if($(element).parent('.document_dropdown').length){
                $(element).parent().find('.select2-selection').css('border', '1px solid red');
            }else if($(element).parent('.iradio_square-blue').length){
                $(element).parent().parent().addClass('has-error');
            }
            else {
                $(element).parent().addClass('has-error');
            }
        },
        unhighlight: function(element) {
            if($(element).parent('.iradio_square-blue').length){
                $(element).parent().parent().removeClass('has-error');
            }else {
                $(element).parent().removeClass('has-error');
            }
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                /*error.insertAfter(element.parent());*/
            }else if(element.parent('.document_dropdown').length){
                error.insertAfter(element.parent());
            }
            else {
                /*error.insertAfter(element);*/
            }
        }
    });
    
    $(".form").validate({
        rules: {
        
        },
        highlight: function(element) {
            if($(element).parent('.input-group').length){
                $(element).parent().parent().addClass('has-error');
            }else {
                $(element).parent().addClass('has-error');
            }
        },
        unhighlight: function(element) {
            $(element).parent().removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                /*error.insertAfter(element.parent());*/
            } else {
                /*error.insertAfter(element);*/
            }
        }
    });
    
    $("#form_label").validate({
        highlight: function(element) {
            $(element).parent().addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).parent().removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
    
    $("#form_v").validate({
        rules: {
        
        },
        highlight: function(element) {
            if($(element).parent('.input-group').length){
                $(element).parent().parent().parent().addClass('has-error');
            }
            else {
                $(element).parent().parent().addClass('has-error');
            }
        },
        unhighlight: function(element) {
            if($(element).parent('.input-group').length){
                $(element).parent().parent().parent().removeClass('has-error');
            }else {
                $(element).parent().parent().removeClass('has-error');
            }
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                /*error.insertAfter(element.parent());*/
            }else if(element.parent('.document_dropdown').length){
                error.insertAfter(element.parent());
            }
            else {
                /*error.insertAfter(element);*/
            }
        }
    });

});