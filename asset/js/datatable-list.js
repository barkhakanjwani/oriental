var table = $('.data-table-list').DataTable({
    "processing": true,
    "responsive": true,
    "serverSide": true,
    "order": [],
    "ajax": {
        "url": data_url,
        "type": "POST"
    }/*,
    "columnDefs": [{
        "targets": -1,
        "data": null,
        "defaultContent": buttons
    }]*/
});

var datatable_body='.data-table-list tbody';
$(datatable_body).on('click','.fpcci',function () {
    var data = table.row($(this).parents('tr')).data();
    view_function(data[1]);
} );

var datatable_body='.data-table-list tbody';
$(datatable_body).on('click','.view_button',function () {
    var data = table.row($(this).parents('tr')).data();
    view_function(data[1]);
} );

$(datatable_body).on('click','.view_link',function () {
    var data = table.row($(this).parents('tr')).data();
    $(this).attr('href',view_url+data[1]);
} );

$(datatable_body).on('click','.edit_buttons_modal',function () {
    var data = table.row($(this).parents('tr')).data();
    edit_function(data[1]);
} );

$(datatable_body).on('click','.edit_button',function () {
    var data = table.row($(this).parents('tr')).data();
    $(this).attr('href',edit_url+data[1]);
} );

$(datatable_body).on('click','.delete_button',function () {
    var data = table.row($(this).parents('tr')).data();
    delete_function(data[1]);
} );